# Source on localhost: ... connected.
SET FOREIGN_KEY_CHECKS=0;
# Exporting metadata from alienspaces_dev
DROP DATABASE IF EXISTS `alienspaces_dev`;
CREATE DATABASE `alienspaces_dev`;
USE `alienspaces_dev`;
# TABLE: alienspaces_dev.as_business
CREATE TABLE `as_business` (
  `business_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_status_id` smallint(6) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `create_ts` timestamp NULL DEFAULT NULL,
  `update_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`business_id`),
  KEY `as_business_business_status_id_fk_idx` (`business_status_id`),
  CONSTRAINT `as_business_business_status_id_fk` FOREIGN KEY (`business_status_id`) REFERENCES `as_r_business_status` (`business_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_business_contact
CREATE TABLE `as_business_contact` (
  `business_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `business_contact_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`business_contact_id`),
  KEY `as_business_contact_business_id_fk_idx` (`business_id`),
  KEY `as_business_contact_contact_id_fk_idx` (`contact_id`),
  KEY `as_business_contact_business_contact_type_id_fk_idx` (`business_contact_type_id`),
  CONSTRAINT `as_business_contact_business_contact_type_id_fk` FOREIGN KEY (`business_contact_type_id`) REFERENCES `as_r_business_contact_type` (`business_contact_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_business_contact_business_id_fk` FOREIGN KEY (`business_id`) REFERENCES `as_business` (`business_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_business_contact_contact_id_fk` FOREIGN KEY (`contact_id`) REFERENCES `as_contact` (`contact_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_business_user
CREATE TABLE `as_business_user` (
  `business_user_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `business_user_type_id` smallint(6) NOT NULL,
  `business_user_status_id` smallint(6) NOT NULL,
  `create_ts` timestamp NULL DEFAULT NULL,
  `update_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`business_user_id`),
  KEY `as_business_user_business_id_fk_idx` (`business_id`),
  KEY `as_business_user_user_id_fk_idx` (`user_id`),
  KEY `as_business_user_business_user_status_id_fk_idx` (`business_user_status_id`),
  KEY `as_business_user_business_user_type_id_fk_idx` (`business_user_type_id`),
  CONSTRAINT `as_business_user_business_id_fk` FOREIGN KEY (`business_id`) REFERENCES `as_business` (`business_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_business_user_business_user_status_id_fk` FOREIGN KEY (`business_user_status_id`) REFERENCES `as_r_business_user_status` (`business_user_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_business_user_business_user_type_id_fk` FOREIGN KEY (`business_user_type_id`) REFERENCES `as_r_business_user_type` (`business_user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_business_user_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_business_user_contact
CREATE TABLE `as_business_user_contact` (
  `business_user_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `business_user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `business_user_contact_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`business_user_contact_id`),
  KEY `as_business_user_contact_business_user_id_fk_idx` (`business_user_id`),
  KEY `as_business_user_contact_contact_id_fk_idx` (`contact_id`),
  KEY `as_business_user_contact_business_user_contact_type_id_fk_idx` (`business_user_contact_type_id`),
  CONSTRAINT `as_business_user_contact_business_user_contact_type_id_fk` FOREIGN KEY (`business_user_contact_type_id`) REFERENCES `as_r_business_user_contact_type` (`business_user_contact_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_business_user_contact_business_user_id_fk` FOREIGN KEY (`business_user_id`) REFERENCES `as_business_user` (`business_user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_business_user_contact_contact_id_fk` FOREIGN KEY (`contact_id`) REFERENCES `as_contact` (`contact_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_contact
CREATE TABLE `as_contact` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `email_address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line_one` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_line_two` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` smallint(6) DEFAULT NULL,
  `state_province` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`contact_id`),
  KEY `as_contact_country_id_fk_idx` (`country_id`),
  CONSTRAINT `as_contact_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `as_r_country` (`country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_email
CREATE TABLE `as_email` (
  `email_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reply_to` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `email_source` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `purge_ts` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_email_attachment
CREATE TABLE `as_email_attachment` (
  `email_attachment_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_id` int(11) NOT NULL,
  `file_name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `file_path` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`email_attachment_id`),
  KEY `as_email_attachment_email_id_fk_idx` (`email_id`),
  CONSTRAINT `as_email_attachment_email_id_fk` FOREIGN KEY (`email_id`) REFERENCES `as_email` (`email_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_email_recipient
CREATE TABLE `as_email_recipient` (
  `email_recipient_id` int(11) NOT NULL AUTO_INCREMENT,
  `email_id` int(11) NOT NULL,
  `email_address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `recipient_type` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `sent_ts` timestamp NULL DEFAULT NULL,
  `send_fail_count` tinyint(4) DEFAULT NULL,
  `send_fail_ts` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email_recipient_id`),
  KEY `as_email_recipient_email_id_fk_idx` (`email_id`),
  KEY `as_email_recipient_recipient_type_fk_idx` (`recipient_type`),
  CONSTRAINT `as_email_recipient_email_id_fk` FOREIGN KEY (`email_id`) REFERENCES `as_email` (`email_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_email_recipient_recipient_type_fk` FOREIGN KEY (`recipient_type`) REFERENCES `as_r_email_recipient_type` (`recipient_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_feedback
CREATE TABLE `as_feedback` (
  `feedback_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `feedback_type_id` smallint(6) NOT NULL,
  `name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`feedback_id`),
  KEY `as_feedback_feedback_type_id_fk_idx` (`feedback_type_id`),
  KEY `as_feedback_user_id_fk_idx` (`user_id`),
  CONSTRAINT `as_feedback_feedback_type_id_fk` FOREIGN KEY (`feedback_type_id`) REFERENCES `as_r_feedback_type` (`feedback_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_feedback_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_phrase
CREATE TABLE `as_phrase` (
  `phrase_id` int(11) NOT NULL,
  `delete_ts` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`phrase_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_phrase_text
CREATE TABLE `as_phrase_text` (
  `phrase_text_id` int(11) NOT NULL AUTO_INCREMENT,
  `phrase_id` int(11) NOT NULL,
  `language_id` smallint(6) NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`phrase_text_id`),
  KEY `as_phrase_text_phrase_id_fk_idx` (`phrase_id`),
  KEY `as_phrase_text_language_id_fk_idx` (`language_id`),
  CONSTRAINT `as_phrase_text_language_id_fk` FOREIGN KEY (`language_id`) REFERENCES `as_r_language` (`language_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_phrase_text_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=100001 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_request
CREATE TABLE `as_request` (
  `request_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_type_id` smallint(6) NOT NULL,
  `user_id` int(11) NOT NULL,
  `request_status_id` smallint(6) NOT NULL,
  `request_key` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expires_ts` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `as_request_request_type_id_fk_idx` (`request_type_id`),
  KEY `as_request_request_status_id_fk_idx` (`request_status_id`),
  KEY `as_request_user_id_fk_idx` (`user_id`),
  CONSTRAINT `as_request_request_status_id_fk` FOREIGN KEY (`request_status_id`) REFERENCES `as_r_request_status` (`request_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_request_request_type_id_fk` FOREIGN KEY (`request_type_id`) REFERENCES `as_r_request_type` (`request_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_request_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_request_data
CREATE TABLE `as_request_data` (
  `request_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `table_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `column_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `pk_value` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `current_value` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `new_value` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`request_data_id`),
  KEY `as_request_data_request_id_fk_idx` (`request_id`),
  CONSTRAINT `as_request_data_request_id_fk` FOREIGN KEY (`request_id`) REFERENCES `as_request` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_request_status_history
CREATE TABLE `as_request_status_history` (
  `request_status_history_id` int(11) NOT NULL AUTO_INCREMENT,
  `request_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `old_request_status_id` smallint(6) DEFAULT NULL,
  `new_request_status_id` smallint(6) NOT NULL,
  `entry_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`request_status_history_id`),
  KEY `as_request_status_history_request_id_fk_idx` (`request_id`),
  KEY `as_request_status_history_old_request_status_id_fk_idx` (`old_request_status_id`),
  KEY `as_request_status_history_new_request_status_id_fk_idx` (`new_request_status_id`),
  KEY `as_request_status_history_user_id_fk_idx` (`user_id`),
  CONSTRAINT `as_request_status_history_new_request_status_id_fk` FOREIGN KEY (`new_request_status_id`) REFERENCES `as_r_request_status` (`request_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_request_status_history_old_request_status_id_fk` FOREIGN KEY (`old_request_status_id`) REFERENCES `as_r_request_status` (`request_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_request_status_history_request_id_fk` FOREIGN KEY (`request_id`) REFERENCES `as_request` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_request_status_history_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_business_contact_type
CREATE TABLE `as_r_business_contact_type` (
  `business_contact_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`business_contact_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_business_status
CREATE TABLE `as_r_business_status` (
  `business_status_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`business_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_business_user_contact_type
CREATE TABLE `as_r_business_user_contact_type` (
  `business_user_contact_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`business_user_contact_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_business_user_status
CREATE TABLE `as_r_business_user_status` (
  `business_user_status_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`business_user_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_business_user_type
CREATE TABLE `as_r_business_user_type` (
  `business_user_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `user_account_admin` tinyint(1) NOT NULL DEFAULT '0',
  `user_account_admin_rank` smallint(6) NOT NULL,
  PRIMARY KEY (`business_user_type_id`),
  KEY `as_r_business_user_type_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_business_user_type_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_business_user_type_task
CREATE TABLE `as_r_business_user_type_task` (
  `business_user_type_task_id` int(11) NOT NULL,
  `business_user_type_id` smallint(6) NOT NULL,
  `task_id` smallint(6) NOT NULL,
  PRIMARY KEY (`business_user_type_task_id`),
  KEY `as_r_bus_user_type_task_bus_user_type_id_fk_idx` (`business_user_type_id`),
  KEY `as_r_bus_user_type_task_task_id_fk_idx` (`task_id`),
  CONSTRAINT `as_r_bus_user_type_task_bus_user_type_id_fk` FOREIGN KEY (`business_user_type_id`) REFERENCES `as_r_business_user_type` (`business_user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_bus_user_type_task_task_id_fk` FOREIGN KEY (`task_id`) REFERENCES `as_r_task` (`task_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_country
CREATE TABLE `as_r_country` (
  `country_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `alpha_2_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `alpha_3_code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `numeric_code` smallint(6) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`country_id`),
  KEY `as_r_country_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_country_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_email_recipient_type
CREATE TABLE `as_r_email_recipient_type` (
  `recipient_type` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`recipient_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_feedback_type
CREATE TABLE `as_r_feedback_type` (
  `feedback_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`feedback_type_id`),
  KEY `as_r_feedback_type_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_feedback_type_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_language
CREATE TABLE `as_r_language` (
  `language_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `parent_language_id` smallint(6) DEFAULT NULL,
  `code` varchar(16) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`language_id`),
  KEY `as_r_language_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_language_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_message_type
CREATE TABLE `as_r_message_type` (
  `message_type_id` smallint(6) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`message_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_notification
CREATE TABLE `as_r_notification` (
  `notification_id` int(11) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `notification_interval_id` int(11) NOT NULL,
  `notification_method_id` int(11) NOT NULL,
  `notification_status_id` int(11) NOT NULL,
  PRIMARY KEY (`notification_id`),
  KEY `as_r_notification_notification_method_id_fk_idx` (`notification_method_id`),
  KEY `as_r_notification_notification_status_id_fk_idx` (`notification_status_id`),
  KEY `as_r_notification_phrase_id_fk_idx` (`phrase_id`),
  KEY `as_r_notification_notification_interval_id_fk_idx` (`notification_interval_id`),
  CONSTRAINT `as_r_notification_notification_interval_id_fk` FOREIGN KEY (`notification_interval_id`) REFERENCES `as_r_notification_interval` (`notification_interval_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_notification_notification_method_id_fk` FOREIGN KEY (`notification_method_id`) REFERENCES `as_r_notification_method` (`notification_method_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_notification_notification_status_id_fk` FOREIGN KEY (`notification_status_id`) REFERENCES `as_r_notification_status` (`notification_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_notification_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_notification_interval
CREATE TABLE `as_r_notification_interval` (
  `notification_interval_id` int(11) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`notification_interval_id`),
  KEY `as_r_notification_interval_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_notification_interval_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_notification_method
CREATE TABLE `as_r_notification_method` (
  `notification_method_id` int(11) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`notification_method_id`),
  KEY `as_r_notification_method_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_notification_method_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_notification_status
CREATE TABLE `as_r_notification_status` (
  `notification_status_id` int(11) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`notification_status_id`),
  KEY `as_r_notification_status_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_notification_status_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_oauth
CREATE TABLE `as_r_oauth` (
  `oauth_id` int(11) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `auth_uri` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token_uri` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  `api_uri` varchar(512) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`oauth_id`),
  KEY `as_r_oauth_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_oauth_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_oauth_config
CREATE TABLE `as_r_oauth_config` (
  `oauth_config_id` int(11) NOT NULL,
  `oauth_id` int(11) NOT NULL,
  `local_domain` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_app_id` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `oauth_app_secret` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`oauth_config_id`),
  KEY `as_r_oauth_config_oauth_id_fk_idx` (`oauth_id`),
  CONSTRAINT `as_r_oauth_config_oauth_id_fk` FOREIGN KEY (`oauth_id`) REFERENCES `as_r_oauth` (`oauth_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_request_business_contact
CREATE TABLE `as_r_request_business_contact` (
  `request_business_contact_id` int(11) NOT NULL,
  `request_type_id` int(11) NOT NULL,
  `business_contact_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`request_business_contact_id`),
  KEY `as_r_req_bus_contact_request_type_id_fk_idx` (`request_type_id`),
  KEY `as_r_request_bus_contact_bus_contact_type_id_fk_idx` (`business_contact_type_id`),
  CONSTRAINT `as_r_req_bus_contact_bus_contact_type_id_fk` FOREIGN KEY (`business_contact_type_id`) REFERENCES `as_r_business_contact_type` (`business_contact_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_req_bus_contact_req_type_id_fk` FOREIGN KEY (`request_type_id`) REFERENCES `as_r_request_type` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_request_message
CREATE TABLE `as_r_request_message` (
  `request_message_id` int(11) NOT NULL,
  `subject_phrase_id` int(11) NOT NULL,
  `template_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Developer reference',
  PRIMARY KEY (`request_message_id`),
  KEY `as_r_request_message_subject_phrase_id_fk_idx` (`subject_phrase_id`),
  CONSTRAINT `as_r_request_message_subject_phrase_id_fk` FOREIGN KEY (`subject_phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='System configured messages such as content for system emails and other system related notifications.';
# TABLE: alienspaces_dev.as_r_request_message_recipient
CREATE TABLE `as_r_request_message_recipient` (
  `request_message_recipient_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`request_message_recipient_id`),
  KEY `as_r_request_message_recipient_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_request_message_recipient_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_request_status
CREATE TABLE `as_r_request_status` (
  `request_status_id` smallint(6) NOT NULL,
  `phrase_id` int(11) DEFAULT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`request_status_id`),
  KEY `as_r_request_status_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_request_status_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_request_system_contact
CREATE TABLE `as_r_request_system_contact` (
  `request_system_contact_id` int(11) NOT NULL,
  `request_type_id` smallint(6) NOT NULL,
  `system_contact_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`request_system_contact_id`),
  KEY `as_r_request_system_contact_request_type_id_fk_idx` (`request_type_id`),
  KEY `as_r_request_system_contact_system_contact_type_id_fk_idx` (`system_contact_type_id`),
  CONSTRAINT `as_r_request_system_contact_request_type_id_fk` FOREIGN KEY (`request_type_id`) REFERENCES `as_r_request_type` (`request_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_request_system_contact_system_contact_type_id_fk` FOREIGN KEY (`system_contact_type_id`) REFERENCES `as_r_system_contact_type` (`system_contact_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_request_type
CREATE TABLE `as_r_request_type` (
  `request_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `validity_minutes` int(11) NOT NULL DEFAULT '0',
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`request_type_id`),
  KEY `as_r_request_type_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_request_type_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_request_type_message
CREATE TABLE `as_r_request_type_message` (
  `request_type_message_id` int(11) NOT NULL,
  `request_type_id` smallint(6) NOT NULL,
  `request_status_id` smallint(6) NOT NULL,
  `request_message_id` int(11) NOT NULL,
  `request_message_recipient_id` smallint(6) NOT NULL,
  PRIMARY KEY (`request_type_message_id`),
  KEY `as_r_request_type_message_request_status_id_fk_idx` (`request_status_id`),
  KEY `as_r_request_type_message_request_type_id_fk` (`request_type_id`),
  KEY `as_r_request_type_message_request_message_id_fk_idx` (`request_message_id`),
  KEY `as_r_request_type_message_request_message_recipient_id_fk_idx` (`request_message_recipient_id`),
  CONSTRAINT `as_r_request_type_message_request_id_fk` FOREIGN KEY (`request_type_id`) REFERENCES `as_r_request_type` (`request_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_request_type_message_request_message_id_fk` FOREIGN KEY (`request_message_id`) REFERENCES `as_r_request_message` (`request_message_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_request_type_message_request_message_recipient_id_fk` FOREIGN KEY (`request_message_recipient_id`) REFERENCES `as_r_request_message_recipient` (`request_message_recipient_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_request_type_message_request_status_id_fk` FOREIGN KEY (`request_status_id`) REFERENCES `as_r_request_status` (`request_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Recipient types in this table will receieve messages based on status changes to a request';
# TABLE: alienspaces_dev.as_r_system_contact_type
CREATE TABLE `as_r_system_contact_type` (
  `system_contact_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`system_contact_type_id`),
  KEY `as_r_system_contact_type_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_system_contact_type_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_task
CREATE TABLE `as_r_task` (
  `task_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`task_id`),
  KEY `as_r_task_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_task_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_time_zone
CREATE TABLE `as_r_time_zone` (
  `time_zone_id` smallint(6) NOT NULL,
  `phrase_id` int(11) DEFAULT NULL,
  `country_id` smallint(6) DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`time_zone_id`),
  KEY `as_r_time_zone_phrase_id_fk_idx` (`phrase_id`),
  KEY `as_r_time_zone_country_id_fk_idx` (`country_id`),
  CONSTRAINT `as_r_time_zone_country_id_fk` FOREIGN KEY (`country_id`) REFERENCES `as_r_country` (`country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_time_zone_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_user_config
CREATE TABLE `as_r_user_config` (
  `user_config_id` smallint(6) NOT NULL,
  `registration_confirmation_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Is user required to confirm registration via email to activate their account?',
  `email_change_confirmation_enabled` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Is user required to confirm via the new email address a change to their existing email address?',
  `cancel_account_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `delete_account_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `default` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_user_contact_type
CREATE TABLE `as_r_user_contact_type` (
  `user_contact_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_contact_type_id`),
  KEY `as_r_user_contact_type_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_user_contact_type_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_user_status
CREATE TABLE `as_r_user_status` (
  `user_status_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`user_status_id`),
  KEY `as_r_user_status_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_user_status_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_user_type
CREATE TABLE `as_r_user_type` (
  `user_type_id` smallint(6) NOT NULL,
  `phrase_id` int(11) NOT NULL,
  `description` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `user_account_admin` tinyint(1) NOT NULL DEFAULT '0',
  `user_account_admin_rank` smallint(6) NOT NULL,
  PRIMARY KEY (`user_type_id`),
  KEY `as_r_user_type_phrase_id_fk_idx` (`phrase_id`),
  CONSTRAINT `as_r_user_type_phrase_id_fk` FOREIGN KEY (`phrase_id`) REFERENCES `as_phrase` (`phrase_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_r_user_type_task
CREATE TABLE `as_r_user_type_task` (
  `user_type_task_id` int(11) NOT NULL,
  `user_type_id` smallint(6) NOT NULL,
  `task_id` smallint(6) NOT NULL,
  PRIMARY KEY (`user_type_task_id`),
  KEY `as_r_user_type_task_task_id_fk_idx` (`task_id`),
  KEY `as_r_user_type_task_user_type_id_fk_idx` (`user_type_id`),
  CONSTRAINT `as_r_user_type_task_task_id_fk` FOREIGN KEY (`task_id`) REFERENCES `as_r_task` (`task_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_r_user_type_task_user_type_id_fk` FOREIGN KEY (`user_type_id`) REFERENCES `as_r_user_type` (`user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_session
CREATE TABLE `as_session` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `key_UNIQUE` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_session_data
CREATE TABLE `as_session_data` (
  `session_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `key` mediumtext COLLATE utf8_unicode_ci,
  `data` longtext COLLATE utf8_unicode_ci,
  `update_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_data_id`),
  KEY `as_session_data_session_id_fk_idx` (`session_id`),
  CONSTRAINT `as_session_data_session_id_fk` FOREIGN KEY (`session_id`) REFERENCES `as_session` (`session_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_system_contact
CREATE TABLE `as_system_contact` (
  `system_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `system_contact_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`system_contact_id`),
  KEY `as_system_contact_contact_id_fk_idx` (`contact_id`),
  KEY `as_system_contact_system_contact_type_id_fk_idx` (`system_contact_type_id`),
  CONSTRAINT `as_system_contact_contact_id_fk` FOREIGN KEY (`contact_id`) REFERENCES `as_contact` (`contact_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_system_contact_system_contact_type_id_fk` FOREIGN KEY (`system_contact_type_id`) REFERENCES `as_r_system_contact_type` (`system_contact_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_user
CREATE TABLE `as_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` smallint(6) NOT NULL DEFAULT '0',
  `user_status_id` smallint(6) NOT NULL DEFAULT '0',
  `time_zone_id` smallint(6) DEFAULT NULL,
  `user_type_id` smallint(6) DEFAULT NULL,
  `handle` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email_address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `alternate_email_address` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `login_active` tinyint(4) NOT NULL DEFAULT '0',
  `activate_key` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `update_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `touch_ts` timestamp NULL DEFAULT NULL,
  `force_password_change` tinyint(4) DEFAULT NULL,
  `user_config_id` smallint(6) NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY `as_user_language_id_fk_idx` (`language_id`),
  KEY `as_user_user_status_id_fk_idx` (`user_status_id`),
  KEY `as_user_time_zone_id_fk_idx` (`time_zone_id`),
  KEY `as_user_user_config_id_fk_idx` (`user_config_id`),
  KEY `as_user_user_type_id_fk_idx` (`user_type_id`),
  CONSTRAINT `as_user_language_id_fk` FOREIGN KEY (`language_id`) REFERENCES `as_r_language` (`language_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_time_zone_id_fk` FOREIGN KEY (`time_zone_id`) REFERENCES `as_r_time_zone` (`time_zone_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_user_config_id_fk` FOREIGN KEY (`user_config_id`) REFERENCES `as_r_user_config` (`user_config_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_user_status_id_fk` FOREIGN KEY (`user_status_id`) REFERENCES `as_r_user_status` (`user_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_user_type_id_fk` FOREIGN KEY (`user_type_id`) REFERENCES `as_r_user_type` (`user_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_user_contact
CREATE TABLE `as_user_contact` (
  `user_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  `user_contact_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`user_contact_id`),
  KEY `as_user_contact_user_id_fk_idx` (`user_id`),
  KEY `as_user_contact_contact_id_fk_idx` (`contact_id`),
  KEY `as_user_contact_user_contact_type_id_fk_idx` (`user_contact_type_id`),
  CONSTRAINT `as_user_contact_contact_id_fk` FOREIGN KEY (`contact_id`) REFERENCES `as_contact` (`contact_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_contact_user_contact_type_id_fk` FOREIGN KEY (`user_contact_type_id`) REFERENCES `as_r_user_contact_type` (`user_contact_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_contact_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_user_message
CREATE TABLE `as_user_message` (
  `user_message_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `message_type_id` smallint(6) NOT NULL,
  `create_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `delete_ts` timestamp NULL DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_message_id`),
  KEY `as_user_message_message_type_id_fk_idx` (`message_type_id`),
  KEY `as_user_message_user_id_fk_idx` (`user_id`),
  CONSTRAINT `as_user_message_message_type_id_fk` FOREIGN KEY (`message_type_id`) REFERENCES `as_r_message_type` (`message_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_message_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Global messages to display to a user. ';
# TABLE: alienspaces_dev.as_user_notification
CREATE TABLE `as_user_notification` (
  `user_notification_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `entry_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_notification_id`),
  KEY `as_user_notification_user_id_fk_idx` (`user_id`),
  KEY `as_user_notification_notification_id_fk_idx` (`notification_id`),
  CONSTRAINT `as_user_notification_notification_id_fk` FOREIGN KEY (`notification_id`) REFERENCES `as_r_notification` (`notification_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_notification_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_user_notification_config
CREATE TABLE `as_user_notification_config` (
  `user_notification_config_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_id` int(11) NOT NULL,
  `notification_interval_id` int(11) NOT NULL,
  `notification_method_id` int(11) NOT NULL,
  `notification_status_id` int(11) NOT NULL,
  PRIMARY KEY (`user_notification_config_id`),
  KEY `as_user_notification_config_notification_id_fk_idx` (`notification_id`),
  KEY `as_user_notification_config_notification_interval_id_fk_idx` (`notification_interval_id`),
  KEY `as_user_notification_config_notification_method_id_fk_idx` (`notification_method_id`),
  KEY `as_user_notification_config_notification_status_id_fk_idx` (`notification_status_id`),
  KEY `as_user_notification_config_user_id_fk_idx` (`user_id`),
  CONSTRAINT `as_user_notification_config_notification_id_fk` FOREIGN KEY (`notification_id`) REFERENCES `as_r_notification` (`notification_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_notification_config_notification_interval_id_fk` FOREIGN KEY (`notification_interval_id`) REFERENCES `as_r_notification_interval` (`notification_interval_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_notification_config_notification_method_id_fk` FOREIGN KEY (`notification_method_id`) REFERENCES `as_r_notification_method` (`notification_method_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_notification_config_notification_status_id_fk` FOREIGN KEY (`notification_status_id`) REFERENCES `as_r_notification_status` (`notification_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_notification_config_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# TABLE: alienspaces_dev.as_user_oauth
CREATE TABLE `as_user_oauth` (
  `user_oauth_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `oauth_id` int(11) NOT NULL,
  `identifier` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_oauth_id`),
  KEY `as_user_oauth_user_id_fk_idx` (`user_id`),
  KEY `as_user_oauth_auth_id_fk_idx` (`oauth_id`),
  CONSTRAINT `as_user_oauth_auth_id_fk` FOREIGN KEY (`oauth_id`) REFERENCES `as_r_oauth_config` (`oauth_config_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `as_user_oauth_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `as_user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
# VIEW: alienspaces_dev.as_user_task_view
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `alienspaces_dev`.`as_user_task_view` AS select `u`.`user_id` AS `user_id`,`t`.`task_id` AS `task_id` from ((`alienspaces_dev`.`as_user` `u` join `alienspaces_dev`.`as_r_user_type_task` `utt` on((`utt`.`user_type_id` = `u`.`user_type_id`))) join `alienspaces_dev`.`as_r_task` `t` on((`t`.`task_id` = `utt`.`task_id`))) union all select `bu`.`user_id` AS `user_id`,`t`.`task_id` AS `task_id` from ((`alienspaces_dev`.`as_business_user` `bu` join `alienspaces_dev`.`as_r_business_user_type_task` `butt` on((`butt`.`business_user_type_id` = `bu`.`business_user_type_id`))) join `alienspaces_dev`.`as_r_task` `t` on((`t`.`task_id` = `butt`.`task_id`)));
# VIEW: alienspaces_dev.as_user_view
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `alienspaces_dev`.`as_user_view` AS select `u`.`user_id` AS `user_id`,`u`.`language_id` AS `language_id`,`u`.`user_status_id` AS `user_status_id`,`u`.`time_zone_id` AS `time_zone_id`,`u`.`user_type_id` AS `user_type_id`,`u`.`handle` AS `handle`,`u`.`first_name` AS `first_name`,`u`.`last_name` AS `last_name`,`u`.`email_address` AS `email_address`,`u`.`alternate_email_address` AS `alternate_email_address`,`u`.`password` AS `password`,`u`.`login_active` AS `login_active`,`u`.`activate_key` AS `activate_key`,`u`.`update_ts` AS `update_ts`,`u`.`touch_ts` AS `touch_ts`,`u`.`force_password_change` AS `force_password_change`,`u`.`user_config_id` AS `user_config_id`,`bu`.`business_user_id` AS `business_user_id`,`bu`.`business_id` AS `business_id`,`bu`.`business_user_type_id` AS `business_user_type_id`,`bu`.`business_user_status_id` AS `business_user_status_id` from (`alienspaces_dev`.`as_user` `u` left join `alienspaces_dev`.`as_business_user` `bu` on((`bu`.`user_id` = `u`.`user_id`)));
# Grant:
GRANT ALTER ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT ALTER ROUTINE ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT CREATE ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT CREATE ROUTINE ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT CREATE TEMPORARY TABLES ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT CREATE VIEW ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT DELETE ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT DROP ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT EVENT ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT EXECUTE ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT INDEX ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT INSERT ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT LOCK TABLES ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT REFERENCES ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT SELECT ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT SHOW VIEW ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT TRIGGER ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT UPDATE ON `alienspaces_dev`.* TO 'as_bin_dev_user'@'localhost';
# Grant:
GRANT DELETE ON `alienspaces_dev`.* TO 'as_web_dev_user'@'localhost';
# Grant:
GRANT INSERT ON `alienspaces_dev`.* TO 'as_web_dev_user'@'localhost';
# Grant:
GRANT SELECT ON `alienspaces_dev`.* TO 'as_web_dev_user'@'localhost';
# Grant:
GRANT UPDATE ON `alienspaces_dev`.* TO 'as_web_dev_user'@'localhost';
#...done.
SET FOREIGN_KEY_CHECKS=1;
