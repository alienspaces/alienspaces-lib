package XXXX::Util::T;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use base qw (XXXX::Util);

use XXXX::Constants qw(:utils);

our $VERSION = (qw$Revision:$)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{$self->SUPER::util_list},
    ];

    return $util_list;
}

sub new {
    my ($class, $args) = @_;
    
    my $self = $class->SUPER::new($args);
    
    return $self;
}

sub init {
    my ($self, $args) = @_;
    
    if (!$self->SUPER::init($args)) {
        $self->fatal({'-text' => 'Failed to initialise super class'});
    }
    
    return 1;
}
                    
1;
