package XXXX::Constants;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = (qw$Revision: 342 $)[1];

use AlienSpaces::Dev::Constants qw(:all);

use Exporter qw(import);
use Storable qw(dclone);

our @EXPORT_OK;
our %EXPORT_TAGS = %{ dclone( \%AlienSpaces::Dev::Constants::EXPORT_TAGS ) };

#   programs
use constant {

};

push @{$EXPORT_TAGS{'programs'}}, (

);

#   comp types
use constant {

};

push @{$EXPORT_TAGS{'comps'}}, (

);

#   utils
use constant {

};

push @{$EXPORT_TAGS{'utils'}}, (

);
    
#   export
foreach my $tag ( keys %EXPORT_TAGS ) {
    Exporter::export_ok_tags($tag);
}
@{ $EXPORT_TAGS{'all'} } = @EXPORT_OK;

1;

