package XXXX::Comp;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

our $VERSION = (qw$Revision: $)[1];

use base qw(AlienSpaces::Dev::Comp);

use XXXX::Constants qw(:program :comp :util);

use AlienSpaces::Dev::Properties {

};

#   util list
#   - can define a list of utilities that you know all
#     sub-classed components are going to need to use
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{$self->SUPER::util_list},
    ];

    return $util_list;
}

#   comp util new args
sub util_new_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   comp util init args
sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   implicit action list   
sub implicit_action_list { 
    my ($self, $args) = @_;
    
    my $implicit_action_list = [
        @{$self->SUPER::implicit_action_list},
    ];
    
    return $implicit_action_list;
}

#
#   base methods 
#   - sub-classes of this class should implement
#     the following methods
sub render {
    my ( $self, $args ) = @_;

    return '';
}

sub action {
    my ( $self, $args ) = @_;

    return COMP_ACTION_FAILURE();
}

1;

