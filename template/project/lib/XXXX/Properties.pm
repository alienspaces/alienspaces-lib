package XXXX::Properties;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = (qw$Revision: 342 $)[1];

use base 'AlienSpaces::Dev::Properties';

1;

