package XXXX::Comp::HTML::T;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

our $VERSION = (qw$Revision: $)[1];

use base qw(XXXX::Comp);

use XXXX::Constants qw(:program :comp :util :interface);

use XXXX::Properties {

};

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

    ];

    return $util_list;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    #   title
    my $title = 'Title';

    #   content
    my $content = $self->panel->render(
        {   '-name'    => $self->name,
            '-title'   => $title,
            '-content' => $cgi->div( { '-class' => 'XXXX' }, 'Content' ),
        } );

    return $content;
}

#   action
sub action {
    my ( $self, $args ) = @_;

    #   action
    #   - action submitted specifically to this component
    my $action = $args->{'-action'};
    if ( !defined $action ) {
        return COMP_ACTION_FAILURE();
    }

    #   data
    #   - post/get data accompanying the action
    my $data = $args->{'-data'};

    if ( $action eq 'xxx' ) {

        my $xxx = $data->{'xxx'};
        my $xxy = $data->{'xxy'};

        if ($self->xxxx(
                {   '-xxx' => $xxx,
                    '-xxy' => $xxy,
                } )
            ) {
            return COMP_ACTION_SUCCESS();
        }

    }

    return COMP_ACTION_FAILURE();
} ## end sub action

1;

