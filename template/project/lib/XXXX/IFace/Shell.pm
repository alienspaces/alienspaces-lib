package XXXX::IFace::Shell;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = (qw$Revision:$)[1];

use base qw (AlienSpaces::Dev::IFace::Shell);

use XXXX::Constants qw(:utils);

use AlienSpaces::Dev::Properties {

};

#   util list
#   - can define a list of utilities that you know all
#     sub-classed components are going to need to use
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ 
        @{ $self->SUPER::util_list }, 
    ];

    return $util_list;
}

#   util new args
sub util_new_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   util init args
sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

sub options_config {
    return 'x:';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-x - xxx
USAGE

    return;
}

sub main {
    my ($self, $args) = @_;

    return;
}

1;

