package XXXX::IFace::Web::HTML;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

our $VERSION = (qw$Revision: $)[1];

use base qw(AlienSpaces::Dev::IFace::Web::HTML);

use XXXX::Constants qw(:util :comp :program);

use AlienSpaces::Dev::Properties {

};

sub init {
    my ( $self, $args ) = @_;

    push @{ $self->css }, (

        # YUI
        '/yui_3.11/build/cssreset/reset-min.css',
        '/yui_3.11/build/cssfonts/fonts-min.css',
        '/yui_3.11/build/cssbase/base-min.css',
        '/yui_3.11/build/cssgrids/grids-min.css',

        # XXXX
        'xxxx.css',
    );

    push @{ $self->js }, (

        # YUI
        '/yui_3.11//build/yui/yui-min.js',
    );

    return $self->SUPER::init($args);
}

#   util list
#   - can define a list of utilities that you know all
#     sub-classed components are going to need to use
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [
        @{$self->SUPER::util_list},
    ];

    return $utility_list;
}

#   html util new args
sub util_new_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   html util init args
sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   html comp list
sub comp_list {
    my $self = shift;

    return [
    
        #   you components here
        
    ];
}

#   html comp new args
sub comp_new_args {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-class'], $args ) ) {
        return;
    }

    my $comp_args = $self->SUPER::comp_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

#   html comp init args
sub comp_init_args {
    my ( $self, $args ) = @_;

    my $comp_args = $self->SUPER::comp_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

#   authorize
sub authorize_user {
    my ( $self, $args ) = @_;

    return $self->AUTHORIZE_FAILURE;
}

sub authorize_redirect_url {
    my ( $self, $args ) = @_;

    return '/html/home';
}

#
#   page methods
#
sub page_css {
    return {};
}

sub page_js {
    return {};
}

#   render
#   - all of the following methods are implemented by
#     AlienSpaces::Dev::IFace::Web::HTML as default
#     placeholders
#   - It is your job to implement 
#     ->render_header
#     ->render_content
#     ->render_footer
sub render {
    my ( $self, $args ) = @_;

    my $ret =

        #   start html document
        $self->render_start($args) .

        #   header
        $self->render_header($args) .

        #   start content
        $self->render_content_start($args) .

        #   content
        $self->render_content($args) .

        #   end content
        $self->render_content_end($args) .

        #   footer
        $self->render_footer($args) .

        #   end html document
        $self->render_end($args);

    return $ret;
}

#   render header
#   - wrap header in a div with its own id of 'header'
#   - additionally add -class => 'yui3-g' for grouping of inner divs
sub render_header {
    my ($self, $args) = @_;
    
    my $cgi = $self->cgi;
    
    return $cgi->div({'-id' => 'header'}, '') . "\n";
}

#   render content
#   - methods render_content_start and render_content_end already look 
#     after outer div encapsulation here
sub render_content {
    my ($self, $args) = @_;
    
    my $cgi = $self->cgi;
    
    return $cgi->div({}, '') . "\n";
}
    
#   render footer
#   - wrap footer in a div with its own id of 'footer'
#   - additionally add -class => 'yui3-g' for grouping of inner divs
sub render_footer {
    my ($self, $args) = @_;
    
    my $cgi = $self->cgi;
    
    return $cgi->div({'-id' => 'footer'}, '') . "\n";
}

#
#   document description, keywords, title
#
sub document_description {
    my ( $self, $args ) = @_;

    return 'Description for the document';
}

sub document_keywords {
    my ( $self, $args ) = @_;

    return 'Keywords for the document';
}

sub document_title {
    my ( $self, $args ) = @_;

    return 'xxxx';
}
    
1;

