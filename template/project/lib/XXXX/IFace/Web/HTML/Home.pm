package XXXX::IFace::Web::HTML::Home;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(croak carp);

use base qw(XXXX::IFace::Web::HTML);

use XXXX::Constants qw(:comp);

our $VERSION = '$Revision: $';

#
#   user authentication
#
sub authorize_user {
    my ($self, $args) = @_;

    return $self->AUTHORIZE_SUCCESS;
}

#
#   panel list
#
sub comp_list {
    my $self = shift;
    
    return [
        @{$self->SUPER::comp_list},
        
        #   your components here
        
    ];
}

#   example of overloading super render content method
sub render_content {
    my ($self, $args) = @_;
    
    my $cgi = $self->cgi;
    
    return $cgi->div({}, 'It Works!') . "\n";
}

1;
