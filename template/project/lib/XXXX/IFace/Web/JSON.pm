package XXXX::IFace::Web::JSON;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

our $VERSION = (qw$Revision: $)[1];

use base qw(AlienSpaces::Dev::IFace::Web::JSON);

use XXXX::Constants qw(:util :comp :program);

use AlienSpaces::Dev::Properties {

};

#   util list
#   - can define a list of utilities that you know all
#     sub-classed components are going to need to use
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [
        @{$self->SUPER::util_list},
    ];

    return $utility_list;
}

#   html util new args
sub util_new_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   html util init args
sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   html comp list
sub comp_list {
    my $self = shift;

    return [];
}

#   html comp new args
sub comp_new_args {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-class'], $args ) ) {
        return;
    }

    my $comp_args = $self->SUPER::comp_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

#   html comp init args
sub comp_init_args {
    my ( $self, $args ) = @_;

    my $comp_args = $self->SUPER::comp_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

1;

