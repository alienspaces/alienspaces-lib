package XXXX::Util;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = (qw$Revision: $)[1];

use base qw (AlienSpaces::Dev::Util);

use XXXX::Constants qw(:utils);

use AlienSpaces::Dev::Properties {

};

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{$self->SUPER::util_list},
    ];

    return $util_list;
}

#   init
sub init {
    my ( $self, $args ) = @_;

    if (!$self->SUPER::init($args)) {
        $self->error({'-text' => 'Failed SUPER init'});
        return;
    }
    
    return 1;
}

1;
