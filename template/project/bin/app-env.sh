#/usr/bin/bash

export APP_HOSTNAME='xxxx.local'
export APP_URL='http://xxxx.local'
export APP_HOME='/project/xxxx/dev-repo'
export APP_WEB_HOME='/var/www/vhosts/xxxx.local/httpdocs'

#   project
export APP_NAME='XXXX'

#   perl
export APP_PERL_SUPER_NAMESPACE='AlienSpaces::Dev'
export APP_PERL_SUPER_LIBRARY_PATH='/project/alienspaces/dev-build/lib'
export APP_PERL_NAMESPACE='XXXX'
export APP_PERL_LIBRARY_PATH='/project/xxxx/dev-repo/lib'

#   cookie
export APP_COOKIE_NAME='xxxx-cookie'

export MYSQL_DB_USERNAME='xxxx_bin_dev_user'
export MYSQL_DB_PASSWORD='xxxx_bin_dev_user'
export MYSQL_DB='xxxx_dev'

#   contacts
export APP_NOREPLY_EMAIL='noreply@xxxx.local'

