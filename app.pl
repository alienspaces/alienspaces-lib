use Plack::Builder;

use strict;
use warnings;

use lib './lib';
use AlienSpaces::App::Web;
use Plack::App::File;

our $VERSION = (qw$Revision:$)[1];

my $project_root = '/project/alienspaces/dev-repo';

#   app configuration
my $local_env = {
    'APP_CONFIG_FILE' => $project_root . '/conf/app.json',
};

my $dyn_html = sub {
    AlienSpaces::App::Web->call(@_);
};

my $css    = Plack::App::File->new( root => $project_root . '/www/httpdocs/css' )->to_app;
my $js     = Plack::App::File->new( root => $project_root . '/www/httpdocs/js' )->to_app;
my $images = Plack::App::File->new( root => $project_root . '/www/httpdocs/img' )->to_app;
my $html   = Plack::App::File->new( root => $project_root . '/www/httpdocs' )->to_app;

builder {
    #   set environment here in middleware?
    enable sub {
        my $app = shift;
        sub {
            my $env = shift;

            while ( my ( $key, $value ) = each %{$local_env} ) {
                $env->{$key} = $value;
            }

            # do preprocessing
            my $res = $app->($env);

            # do postprocessing
            return $res;
        };
    };
    mount '/img'  => $images;
    mount '/css'  => $css;
    mount '/js'   => $js;
    mount '/'     => $html;
    mount '/html' => $dyn_html;
};

