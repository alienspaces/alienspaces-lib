package AlienSpaces::Constants;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = qw($Revision: $) [1];

use Exporter qw(import);

our @EXPORT_OK;
our %EXPORT_TAGS;

use constant {

    #   html components
    COMP_HTML_SITE_HEADER                => 'Site::Header',
    COMP_HTML_SITE_LOGO                  => 'Site::Logo',
    COMP_HTML_SITE_USER                  => 'Site::User',
    COMP_HTML_SITE_MESSAGE               => 'Site::Message',
    COMP_HTML_SITE_NAVIGATION            => 'Site::Navigation',
    COMP_HTML_SITE_NAVIGATION_BREADCRUMB => 'Site::Navigation::BreadCrumb',
    COMP_HTML_SITE_PROMOTION             => 'Site::Promotion',
    COMP_HTML_SITE_FOOTER                => 'Site::Footer',
    COMP_HTML_SITE_ABOUT                 => 'Site::About',
    COMP_HTML_SITE_CONTACT               => 'Site::Contact',
    COMP_HTML_SITE_NAVIGATION            => 'Site::Navigation',

    COMP_HTML_SITE_PROJECT => 'Site::Project',

    COMP_HTML_SITE_PROJECT_STUPID_STORY  => 'Site::Project::Stupidstory',
    COMP_HTML_SITE_PROJECT_ARENA         => 'Site::Project::Arena',
    COMP_HTML_SITE_PROJECT_DUNGEON_DOOM  => 'Site::Project::Dungeondoom',
    COMP_HTML_SITE_PROJECT_FART_LICENSE  => 'Site::Project::Fartlicense',
    COMP_HTML_SITE_PROJECT_TORVUS        => 'Site::Project::Torvus',
    COMP_HTML_SITE_PROJECT_BRAINS_BRAINS => 'Site::Project::Brainsbrains',
    COMP_HTML_SITE_PROJECT_LIST          => 'Site::Project::List',

    COMP_HTML_SITE_POLICY_PRIVACY          => 'Site::Policy::Privacy',
    COMP_HTML_SITE_POLICY_TERMS_CONDITIONS => 'Site::Policy::Termsconditions',
    COMP_HTML_SITE_POLICY_CONTACT          => 'Site::Policy::Contact',
    COMP_HTML_SITE_POLICY_COPYRIGHT        => 'Site::Policy::Copyright',
    COMP_HTML_SITE_POLICY_SECURITY         => 'Site::Policy::Security',

    COMP_HTML_SITE_ADSENSE_LEFT_SQUARE       => 'Site::Adsense::Leftsquare',
    COMP_HTML_SITE_ADSENSE_BOTTOM_HORIZONTAL => 'Site::Adsense::Bottomhorizontal',

    COMP_HTML_SITE_KRISKRINGLE_LOGO                      => 'Site::Kriskringle::Logo',
    COMP_HTML_SITE_KRISKRINGLE_TITLE                     => 'Site::Kriskringle::Title',
    COMP_HTML_SITE_KRISKRINGLE_PARTY                     => 'Site::Kriskringle::Party',
    COMP_HTML_SITE_KRISKRINGLE_GIFT                      => 'Site::Kriskringle::Gift',
    COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT               => 'Site::Kriskringle::Participant',
    COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT_WISHLIST      => 'Site::Kriskringle::Participant::Wishlist',
    COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT_WISHLIST_LIST => 'Site::Kriskringle::Participant::Wishlist::List',
    COMP_HTML_SITE_KRISKRINGLE_ASSIGNED                  => 'Site::Kriskringle::Assigned',
    COMP_HTML_SITE_KRISKRINGLE_ASSIGNED_WISHLIST_LIST    => 'Site::Kriskringle::Assigned::Wishlist::List',
    COMP_HTML_SITE_KRISKRINGLE_FOOTER                    => 'Site::Kriskringle::Footer',

    COMP_HTML_SOCIAL_FACEBOOK => 'Social::Facebook',

    COMP_HTML_USER_LOGIN          => 'User::Login',
    COMP_HTML_USER_LOGIN_TITLE    => 'User::Login::Title',
    COMP_HTML_USER_LOGOUT         => 'User::Logout',
    COMP_HTML_USER_REGISTER       => 'User::Register',
    COMP_HTML_USER_REGISTER_TITLE => 'User::Register::Title',

    COMP_HTML_USER_ACCOUNT_PROFILE                => 'User::Account::Profile',
    COMP_HTML_USER_ACCOUNT_PROFILE_UPDATE         => 'User::Account::Profile::Update',
    COMP_HTML_USER_ACCOUNT_EMAIL_PRIMARY          => 'User::Account::Email::Primary',
    COMP_HTML_USER_ACCOUNT_EMAIL_PRIMARY_UPDATE   => 'User::Account::Email::Primary::Update',
    COMP_HTML_USER_ACCOUNT_EMAIL_ALTERNATE        => 'User::Account::Email::Alternate',
    COMP_HTML_USER_ACCOUNT_EMAIL_ALTERNATE_UPDATE => 'User::Account::Email::Alternate::Update',
    COMP_HTML_USER_ACCOUNT_CONTACT                => 'User::Account::Contact',
    COMP_HTML_USER_ACCOUNT_CONTACT_UPDATE         => 'User::Account::Contact::Update',
    COMP_HTML_USER_ACCOUNT_CONTACT_ADD            => 'User::Account::Contact::Add',
    COMP_HTML_USER_ACCOUNT_NOTIFICATION           => 'User::Account::Notification',
    COMP_HTML_USER_ACCOUNT_PASSWORD               => 'User::Account::Password',
    COMP_HTML_USER_ACCOUNT_PASSWORD_UPDATE        => 'User::Account::Password::Update',
    COMP_HTML_USER_ACCOUNT_PASSWORD_RESET         => 'User::Account::Password::Reset',
    COMP_HTML_USER_ACCOUNT_PASSWORD_RESET_APPROVE => 'User::Account::Password::Reset::Approve',
    COMP_HTML_USER_ACCOUNT_PASSWORD_RESET_DENY    => 'User::Account::Password::Reset::Deny',

    COMP_HTML_USER_OAUTH_LOGIN             => 'User::Oauth::Login',
    COMP_HTML_USER_OAUTH_LOGIN_TITLE       => 'User::Oauth::Login::Title',
    COMP_HTML_USER_OAUTH_REGISTER          => 'User::Oauth::Register',
    COMP_HTML_USER_OAUTH_REGISTER_TITLE    => 'User::Oauth::Register::Title',
    COMP_HTML_USER_OAUTH_REGISTER_DIALOGUE => 'User::Oauth::Register::Dialogue',

    COMP_HTML_REQUEST_APPROVE => 'Request::Approve',
    COMP_HTML_REQUEST_DENY    => 'Request::Deny',

    COMP_HTML_BUSINESS_ACCOUNT_PROFILE        => 'Business::Account::Profile',
    COMP_HTML_BUSINESS_ACCOUNT_PROFILE_ADD    => 'Business::Account::Profile::Add',
    COMP_HTML_BUSINESS_ACCOUNT_PROFILE_UPDATE => 'Business::Account::Profile::Update',
    COMP_HTML_BUSINESS_ACCOUNT_CONTACT        => 'Business::Account::Contact',
    COMP_HTML_BUSINESS_ACCOUNT_CONTACT_ADD    => 'Business::Account::Contact::Add',
    COMP_HTML_BUSINESS_ACCOUNT_CONTACT_UPDATE => 'Business::Account::Contact::Update',

    COMP_HTML_BUSINESS_USER                         => 'Business::User',
    COMP_HTML_BUSINESS_USER_REGISTER                => 'Business::User::Register',
    COMP_HTML_BUSINESS_USER_REGISTER_APPROVE        => 'Business::User::Register::Approve',
    COMP_HTML_BUSINESS_USER_REGISTER_DENY           => 'Business::User::Register::Deny',
    COMP_HTML_BUSINESS_USER_ACCOUNT_PROFILE         => 'Business::User::Account::Profile',
    COMP_HTML_BUSINESS_USER_ACCOUNT_PROFILE_UPDATE  => 'Business::User::Account::Profile::Update',
    COMP_HTML_BUSINESS_USER_ACCOUNT_EMAIL_PRIMARY   => 'Business::User::Account::Email::Primary',
    COMP_HTML_BUSINESS_USER_ACCOUNT_EMAIL_ALTERNATE => 'Business::User::Account::Email::Alternate',
    COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT         => 'Business::User::Account::Contact',
    COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT_ADD     => 'Business::User::Account::Contact::Add',
    COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT_UPDATE  => 'Business::User::Account::Contact::Update',

    COMP_HTML_APPLICATION_USER                         => 'Application::User',
    COMP_HTML_APPLICATION_USER_ACCOUNT_PROFILE         => 'Application::User::Account::Profile',
    COMP_HTML_APPLICATION_USER_ACCOUNT_PROFILE_UPDATE  => 'Application::User::Account::Profile::Update',
    COMP_HTML_APPLICATION_USER_ACCOUNT_EMAIL_PRIMARY   => 'Application::User::Account::Email::Primary',
    COMP_HTML_APPLICATION_USER_ACCOUNT_EMAIL_ALTERNATE => 'Application::User::Account::Email::Alternate',
    COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT         => 'Application::User::Account::Contact',
    COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT_ADD     => 'Application::User::Account::Contact::Add',
    COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT_UPDATE  => 'Application::User::Account::Contact::Update',

    #   json components
    COMP_JSON_API_REMOTE_BUILD => 'API::Remote::Build',

    COMP_JSON_API_SESSION_UPDATE => 'API::Session::Update',

    #    COMP_JSON_API_USER_LOGIN    => 'API::User::Login',
    #    COMP_JSON_API_USER_LOGOUT   => 'API::User::Logout',
    #    COMP_JSON_API_USER_REGISTER => 'API::User::Register',

    #    COMP_JSON_API_USER_CANCEL      => 'API::User::Cancel',
    #    COMP_JSON_API_USER_DELETE      => 'API::User::Delete',
    #    COMP_JSON_API_USER_INFORMATION => 'API::User::Information',
    #    COMP_JSON_API_USER_UPDATE      => 'API::User::Update',

    COMP_JSON_API_USER_ACCOUNT => 'API::User::Account',

    #   test components
    COMP_TEST      => 'Test',
    COMP_HTML_TEST => 'HTML::Test',
    COMP_JSON_TEST => 'JSON::Test',

    #   comps action return codes
    COMP_ACTION_FAILURE          => 0,
    COMP_ACTION_VALIDATE_FAILURE => 4,
    COMP_ACTION_SUCCESS          => 1,
    COMP_ACTION_AUTHORIZE        => 2,
    COMP_ACTION_NONE             => 3,

    #   comp implicit actions (implied, rather than expressly stated: implicit agreement)
    IMPLICIT_ACTION_SET_LANGUAGE  => 'set_language',
    IMPLICIT_ACTION_SET_TIME_ZONE => 'set_time_zone',
};

$EXPORT_TAGS{'comp'} = [

    #   html components
    'COMP_HTML_SITE_HEADER',
    'COMP_HTML_SITE_LOGO',
    'COMP_HTML_SITE_USER',
    'COMP_HTML_SITE_MESSAGE',
    'COMP_HTML_SITE_NAVIGATION',
    'COMP_HTML_SITE_NAVIGATION_BREADCRUMB',
    'COMP_HTML_SITE_PROMOTION',
    'COMP_HTML_SITE_FOOTER',
    'COMP_HTML_SITE_ABOUT',
    'COMP_HTML_SITE_CONTACT',
    'COMP_HTML_SITE_NAVIGATION',

    'COMP_HTML_SITE_PROJECT',

    'COMP_HTML_SITE_PROJECT_STUPID_STORY',
    'COMP_HTML_SITE_PROJECT_ARENA',
    'COMP_HTML_SITE_PROJECT_DUNGEON_DOOM',
    'COMP_HTML_SITE_PROJECT_FART_LICENSE',
    'COMP_HTML_SITE_PROJECT_TORVUS',
    'COMP_HTML_SITE_PROJECT_BRAINS_BRAINS',

    'COMP_HTML_SITE_ADSENSE_LEFT_SQUARE',
    'COMP_HTML_SITE_ADSENSE_BOTTOM_HORIZONTAL',

    'COMP_HTML_SITE_PROJECT_LIST',
    'COMP_HTML_SITE_POLICY_PRIVACY',
    'COMP_HTML_SITE_POLICY_TERMS_CONDITIONS',
    'COMP_HTML_SITE_POLICY_CONTACT',
    'COMP_HTML_SITE_POLICY_COPYRIGHT',
    'COMP_HTML_SITE_POLICY_SECURITY',

    'COMP_HTML_SITE_KRISKRINGLE_LOGO',
    'COMP_HTML_SITE_KRISKRINGLE_TITLE',
    'COMP_HTML_SITE_KRISKRINGLE_PARTY',
    'COMP_HTML_SITE_KRISKRINGLE_GIFT',
    'COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT',
    'COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT_WISHLIST',
    'COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT_WISHLIST_LIST',
    'COMP_HTML_SITE_KRISKRINGLE_ASSIGNED',
    'COMP_HTML_SITE_KRISKRINGLE_ASSIGNED_WISHLIST_LIST',
    'COMP_HTML_SITE_KRISKRINGLE_FOOTER',

    'COMP_HTML_SOCIAL_FACEBOOK',

    'COMP_HTML_USER_LOGIN',
    'COMP_HTML_USER_LOGIN_TITLE',
    'COMP_HTML_USER_LOGOUT',
    'COMP_HTML_USER_REGISTER',
    'COMP_HTML_USER_REGISTER_TITLE',

    'COMP_HTML_USER_ACCOUNT_PROFILE',
    'COMP_HTML_USER_ACCOUNT_PROFILE_UPDATE',
    'COMP_HTML_USER_ACCOUNT_EMAIL_PRIMARY',
    'COMP_HTML_USER_ACCOUNT_EMAIL_PRIMARY_UPDATE',
    'COMP_HTML_USER_ACCOUNT_EMAIL_ALTERNATE',
    'COMP_HTML_USER_ACCOUNT_EMAIL_ALTERNATE_UPDATE',
    'COMP_HTML_USER_ACCOUNT_CONTACT',
    'COMP_HTML_USER_ACCOUNT_CONTACT_UPDATE',
    'COMP_HTML_USER_ACCOUNT_CONTACT_ADD',
    'COMP_HTML_USER_ACCOUNT_NOTIFICATION',
    'COMP_HTML_USER_ACCOUNT_PASSWORD',
    'COMP_HTML_USER_ACCOUNT_PASSWORD_UPDATE',
    'COMP_HTML_USER_ACCOUNT_PASSWORD_RESET',
    'COMP_HTML_USER_ACCOUNT_PASSWORD_RESET_APPROVE',
    'COMP_HTML_USER_ACCOUNT_PASSWORD_RESET_DENY',

    'COMP_HTML_USER_OAUTH_LOGIN',
    'COMP_HTML_USER_OAUTH_LOGIN_TITLE',
    'COMP_HTML_USER_OAUTH_REGISTER',
    'COMP_HTML_USER_OAUTH_REGISTER_TITLE',
    'COMP_HTML_USER_OAUTH_REGISTER_DIALOGUE',

    'COMP_HTML_REQUEST_APPROVE',
    'COMP_HTML_REQUEST_DENY',

    'COMP_HTML_BUSINESS_ACCOUNT_PROFILE',
    'COMP_HTML_BUSINESS_ACCOUNT_PROFILE_ADD',
    'COMP_HTML_BUSINESS_ACCOUNT_PROFILE_UPDATE',
    'COMP_HTML_BUSINESS_USER_ACCOUNT_EMAIL_PRIMARY',
    'COMP_HTML_BUSINESS_USER_ACCOUNT_EMAIL_ALTERNATE',
    'COMP_HTML_BUSINESS_ACCOUNT_CONTACT',
    'COMP_HTML_BUSINESS_ACCOUNT_CONTACT_ADD',
    'COMP_HTML_BUSINESS_ACCOUNT_CONTACT_UPDATE',

    'COMP_HTML_BUSINESS_USER',
    'COMP_HTML_BUSINESS_USER_REGISTER',
    'COMP_HTML_BUSINESS_USER_REGISTER_APPROVE',
    'COMP_HTML_BUSINESS_USER_REGISTER_DENY',
    'COMP_HTML_BUSINESS_USER_ACCOUNT_PROFILE',
    'COMP_HTML_BUSINESS_USER_ACCOUNT_PROFILE_UPDATE',
    'COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT',
    'COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT_ADD',
    'COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT_UPDATE',

    'COMP_HTML_APPLICATION_USER',
    'COMP_HTML_APPLICATION_USER_ACCOUNT_PROFILE',
    'COMP_HTML_APPLICATION_USER_ACCOUNT_PROFILE_UPDATE',
    'COMP_HTML_APPLICATION_USER_ACCOUNT_EMAIL_PRIMARY',
    'COMP_HTML_APPLICATION_USER_ACCOUNT_EMAIL_ALTERNATE',
    'COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT',
    'COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT_ADD',
    'COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT_UPDATE',

    #   json components
    'COMP_JSON_API_REMOTE_BUILD',

    'COMP_JSON_API_SESSION_UPDATE',

    #    'COMP_JSON_API_USER_LOGIN',
    #    'COMP_JSON_API_USER_LOGOUT',
    #    'COMP_JSON_API_USER_REGISTER',

    #    'COMP_JSON_API_USER_CANCEL',
    #    'COMP_JSON_API_USER_DELETE',
    #    'COMP_JSON_API_USER_INFORMATION',
    #    'COMP_JSON_API_USER_UPDATE',

    'COMP_JSON_API_USER_ACCOUNT',

    #   test components
    'COMP_TEST',
    'COMP_HTML_TEST',
    'COMP_JSON_TEST',

    #   comp action return codes
    'COMP_ACTION_SUCCESS',
    'COMP_ACTION_FAILURE',
    'COMP_ACTION_VALIDATE_FAILURE',
    'COMP_ACTION_AUTHORIZE',
    'COMP_ACTION_NONE',

    #   comp implicit actions
    'IMPLICIT_ACTION_SET_LANGUAGE',
    'IMPLICIT_ACTION_SET_TIME_ZONE',

];

$EXPORT_TAGS{'comps'} = $EXPORT_TAGS{'comp'};

#   comp actions
$EXPORT_TAGS{'comp-actions'} =
    [ 'COMP_ACTION_SUCCESS', 'COMP_ACTION_FAILURE', 'COMP_ACTION_VALIDATE_FAILURE', 'COMP_ACTION_AUTHORIZE', 'COMP_ACTION_NONE' ];

#   obsolete
$EXPORT_TAGS{'panel-actions'} =
    [ 'COMP_ACTION_SUCCESS', 'COMP_ACTION_FAILURE', 'COMP_ACTION_VALIDATE_FAILURE', 'COMP_ACTION_AUTHORIZE', 'COMP_ACTION_NONE' ];

#
#   programs
#
use constant {

    #   alienspaces application programs
    PROGRAM_SITE_HOME    => '/html/site/home',
    PROGRAM_SITE_CONTACT => '/html/site/contact',
    PROGRAM_SITE_POLICY  => '/html/site/policy',
    PROGRAM_SITE_PROJECT => '/html/site/project',

    #   alienspaces core programs
    PROGRAM_SITE_USER_LOGIN        => '/html/site/user/login',
    PROGRAM_SITE_USER_LOGOUT       => '/html/site/user/logout',
    PROGRAM_SITE_USER_REGISTER     => '/html/site/user/register',
    PROGRAM_SITE_USER_ACTIVATE     => '/html/site/user/activate',
    PROGRAM_SITE_USER_NOTIFICATION => '/html/site/user/notification',

    PROGRAM_SITE_USER_ACCOUNT                 => '/html/site/user/account',
    PROGRAM_SITE_USER_ACCOUNT_PROFILE         => '/html/site/user/account/profile',
    PROGRAM_SITE_USER_ACCOUNT_EMAIL_PRIMARY   => '/html/site/user/account/email/primary',
    PROGRAM_SITE_USER_ACCOUNT_EMAIL_ALTERNATE => '/html/site/user/account/email/alternate',
    PROGRAM_SITE_USER_ACCOUNT_CONTACT_UPDATE  => '/html/site/user/account/contact/update',
    PROGRAM_SITE_USER_ACCOUNT_CONTACT_ADD     => '/html/site/user/account/contact/add',
    PROGRAM_SITE_USER_ACCOUNT_PASSWORD        => '/html/site/user/account/password',
    PROGRAM_SITE_USER_ACCOUNT_PASSWORD_RESET  => '/html/site/user/account/password/reset',

    PROGRAM_SITE_PASSWORD_RESET_REQUEST_APPROVE => '/html/site/user/account/password/reset/approve',
    PROGRAM_SITE_PASSWORD_RESET_REQUEST_DENY    => '/html/site/user/account/password/reset/deny',

    PROGRAM_SITE_USER_OAUTH_LOGIN    => '/html/site/user/oauth/login',
    PROGRAM_SITE_USER_OAUTH_REGISTER => '/html/site/user/oauth/register',

    PROGRAM_SITE_REMOTE_BUILD => '/html/site/remote/build',

    PROGRAM_SITE_REQUEST_APPROVE => '/html/site/request/approve',
    PROGRAM_SITE_REQUEST_DENY    => '/html/site/request/deny',

    PROGRAM_SITE_BUSINESS_ACCOUNT                => '/html/site/business/account',
    PROGRAM_SITE_BUSINESS_ACCOUNT_PROFILE_ADD    => '/html/site/business/account/profile/add',
    PROGRAM_SITE_BUSINESS_ACCOUNT_PROFILE_UPDATE => '/html/site/business/account/profile/update',
    PROGRAM_SITE_BUSINESS_ACCOUNT_CONTACT_ADD    => '/html/site/business/account/contact/add',
    PROGRAM_SITE_BUSINESS_ACCOUNT_CONTACT_UPDATE => '/html/site/business/account/contact/update',

    PROGRAM_SITE_BUSINESS_USER                        => '/html/site/business/user',
    PROGRAM_SITE_BUSINESS_USER_REGISTER               => '/html/site/business/user/register',
    PROGRAM_SITE_BUSINESS_USER_REGISTER_APPROVE       => '/html/site/business/user/register/approve',
    PROGRAM_SITE_BUSINESS_USER_REGISTER_DENY          => '/html/site/business/user/register/deny',
    PROGRAM_SITE_BUSINESS_USER_ACCOUNT                => '/html/site/business/user/account',
    PROGRAM_SITE_BUSINESS_USER_ACCOUNT_PROFILE        => '/html/site/business/user/account/profile',
    PROGRAM_SITE_BUSINESS_USER_ACCOUNT_CONTACT_ADD    => '/html/site/business/user/account/contact/add',
    PROGRAM_SITE_BUSINESS_USER_ACCOUNT_CONTACT_UPDATE => '/html/site/business/user/account/contact/update',

    PROGRAM_SITE_APPLICATION_USER                        => '/html/site/application/user',
    PROGRAM_SITE_APPLICATION_USER_ACCOUNT                => '/html/site/application/user/account',
    PROGRAM_SITE_APPLICATION_USER_ACCOUNT_PROFILE        => '/html/site/application/user/account/profile',
    PROGRAM_SITE_APPLICATION_USER_ACCOUNT_CONTACT_ADD    => '/html/site/application/user/account/contact/add',
    PROGRAM_SITE_APPLICATION_USER_ACCOUNT_CONTACT_UPDATE => '/html/site/application/user/account/contact/update',

    #   alienspaces test programs
    PROGRAM_SITE_TEST => 'test',

    #   alienspaces kriskringle programs
    PROGRAM_SITE_KRISKRINGLE_HOME => '/html/site/kriskringle/home',
};

$EXPORT_TAGS{'program'} = [

    #   alienspaces application programs
    'PROGRAM_SITE_HOME',
    'PROGRAM_SITE_CONTACT',
    'PROGRAM_SITE_POLICY',
    'PROGRAM_SITE_PROJECT',

    #   alienspaces core programs
    'PROGRAM_SITE_USER_LOGIN',
    'PROGRAM_SITE_USER_LOGOUT',
    'PROGRAM_SITE_USER_REGISTER',
    'PROGRAM_SITE_USER_ACTIVATE',
    'PROGRAM_SITE_USER_NOTIFICATION',

    'PROGRAM_SITE_USER_ACCOUNT',
    'PROGRAM_SITE_USER_ACCOUNT_PROFILE',
    'PROGRAM_SITE_USER_ACCOUNT_EMAIL_PRIMARY',
    'PROGRAM_SITE_USER_ACCOUNT_EMAIL_ALTERNATE',
    'PROGRAM_SITE_USER_ACCOUNT_CONTACT_UPDATE',
    'PROGRAM_SITE_USER_ACCOUNT_CONTACT_ADD',
    'PROGRAM_SITE_USER_ACCOUNT_PASSWORD',
    'PROGRAM_SITE_USER_ACCOUNT_PASSWORD_RESET',

    'PROGRAM_SITE_PASSWORD_RESET_REQUEST_APPROVE',
    'PROGRAM_SITE_PASSWORD_RESET_REQUEST_DENY',

    'PROGRAM_SITE_USER_OAUTH_LOGIN',
    'PROGRAM_SITE_USER_OAUTH_REGISTER',

    'PROGRAM_SITE_REQUEST_APPROVE',
    'PROGRAM_SITE_REQUEST_DENY',

    'PROGRAM_SITE_REMOTE_BUILD',

    'PROGRAM_SITE_BUSINESS_ACCOUNT',
    'PROGRAM_SITE_BUSINESS_ACCOUNT_PROFILE_UPDATE',
    'PROGRAM_SITE_BUSINESS_ACCOUNT_PROFILE_ADD',
    'PROGRAM_SITE_BUSINESS_ACCOUNT_CONTACT_UPDATE',
    'PROGRAM_SITE_BUSINESS_ACCOUNT_CONTACT_ADD',

    'PROGRAM_SITE_BUSINESS_USER',
    'PROGRAM_SITE_BUSINESS_USER_REGISTER',
    'PROGRAM_SITE_BUSINESS_USER_REGISTER_APPROVE',
    'PROGRAM_SITE_BUSINESS_USER_REGISTER_DENY',
    'PROGRAM_SITE_BUSINESS_USER_ACCOUNT',
    'PROGRAM_SITE_BUSINESS_USER_ACCOUNT_PROFILE',
    'PROGRAM_SITE_BUSINESS_USER_ACCOUNT_CONTACT_ADD',
    'PROGRAM_SITE_BUSINESS_USER_ACCOUNT_CONTACT_UPDATE',

    'PROGRAM_SITE_APPLICATION_USER',
    'PROGRAM_SITE_APPLICATION_USER_ACCOUNT',
    'PROGRAM_SITE_APPLICATION_USER_ACCOUNT_PROFILE',
    'PROGRAM_SITE_APPLICATION_USER_ACCOUNT_CONTACT_ADD',
    'PROGRAM_SITE_APPLICATION_USER_ACCOUNT_CONTACT_UPDATE',

    #   alienspaces test programs
    'PROGRAM_SITE_TEST',

    #   alienspaces kriskringle programs
    'PROGRAM_SITE_KRISKRINGLE_HOME',
];

$EXPORT_TAGS{'programs'} = $EXPORT_TAGS{'program'};

#
#   class types
#
use constant {
    CLASS_TYPE_COMP  => 'Comp',
    CLASS_TYPE_UTIL  => 'Util',
    CLASS_TYPE_IFACE => 'IFace',
};

$EXPORT_TAGS{'loader'} = [ 'CLASS_TYPE_COMP', 'CLASS_TYPE_UTIL', 'CLASS_TYPE_IFACE', ];

#
#   interface types
#
use constant {
    INTERFACE_HTML  => 'HTML',
    INTERFACE_JSON  => 'JSON',
    INTERFACE_XML   => 'XML',
    INTERFACE_SHELL => 'Shell',
};

$EXPORT_TAGS{'interface'} = [ 'INTERFACE_HTML', 'INTERFACE_JSON', 'INTERFACE_XML', 'INTERFACE_SHELL' ];

#
#   auth
#
use constant {
    AUTHORIZE_SUCCESS => 1,
    AUTHORIZE_FAILURE => 2,
};

$EXPORT_TAGS{'auth'} = [ 'AUTHORIZE_SUCCESS', 'AUTHORIZE_FAILURE', ];

use constant {

    #   CGI.pm interface
    UTIL_CGI => 'CGI',

    #   Log::Log4perl.pm interface
    UTIL_LOG => 'Log',

    #   MySQL connection interface
    UTIL_DB_MYSQL => 'DB::MySQL',

    #   phrase methods
    UTIL_PHRASE => 'Phrase',

    #   error message registering
    UTIL_ERROR => 'Error',

    #   session handling
    UTIL_SESSION => 'Session',

    #   user methods
    UTIL_USER => 'User',

    #   business methods
    UTIL_BUSINESS => 'Business',

    #   time zone methods
    UTIL_TIME_ZONE => 'TimeZone',

    #   language methods
    UTIL_LANGUAGE => 'Language',

    #   country methods
    UTIL_COUNTRY => 'Country',

    #   message sub-system methods
    UTIL_MESSAGE => 'Message',

    #   request sub-system methods
    UTIL_REQUEST => 'Request',

    #   email sub-system methods
    UTIL_EMAIL => 'Email',

    #   feedback sub-system methods
    UTIL_FEEDBACK => 'Feedback',

    #   xml loading / parsing methods
    UTIL_XML_LIBXML => 'XML::LibXML',

    #   data loader map
    UTIL_DATA_LOADER_MAP => 'Data::Loader::Map',

    #   data config xml
    UTIL_DATA_CONFIG_XML => 'Data::Config::XML',

    #   image sub-system methods
    UTIL_IMAGE => 'Image',

    #   dictionary methods
    UTIL_DICTIONARY => 'Dictionary',

    #   notification
    UTIL_NOTIFICATION => 'Notification',

    #   db basic (deprecated)
    UTIL_DB_BASIC => 'DB::Basic',

    #   db table meta data methods
    UTIL_DB_TABLE => 'DB::Table',

    #   db view meta data methods
    UTIL_DB_VIEW => 'DB::View',

    #   table data methods
    UTIL_DB_DATA => 'DB::Data',

    #   db record data methods
    UTIL_DB_RECORD => 'DB::Record',

    #   util db record classes (user)
    UTIL_DB_RECORD_USER       => 'DB::Record::User',
    UTIL_DB_RECORD_USER_OAUTH => 'DB::Record::UserOauth',

    UTIL_DB_RECORD_USER_VIEW      => 'DB::Record::UserView',
    UTIL_DB_RECORD_USER_TASK_VIEW => 'DB::Record::UserTaskView',

    #   util db record classes (business)
    UTIL_DB_RECORD_BUSINESS      => 'DB::Record::Business',
    UTIL_DB_RECORD_BUSINESS_USER => 'DB::Record::BusinessUser',

    #   util db record classes (contact)
    UTIL_DB_RECORD_CONTACT               => 'DB::Record::Contact',
    UTIL_DB_RECORD_USER_CONTACT          => 'DB::Record::UserContact',
    UTIL_DB_RECORD_BUSINESS_CONTACT      => 'DB::Record::BusinessContact',
    UTIL_DB_RECORD_BUSINESS_USER_CONTACT => 'DB::Record::BusinessUserContact',
    UTIL_DB_RECORD_SYSTEM_CONTACT        => 'DB::Record::SystemContact',

    #   util db record classes (session)
    UTIL_DB_RECORD_SESSION      => 'DB::Record::Session',
    UTIL_DB_RECORD_SESSION_DATA => 'DB::Record::SessionData',

    #   util db record classes (notification)
    UTIL_DB_RECORD_USER_NOTIFICATION        => 'DB::Record::UserNotification',
    UTIL_DB_RECORD_USER_NOTIFICATION_CONFIG => 'DB::Record::UserNotificationConfig',

    #   util db record classes (reference)
    UTIL_DB_RECORD_R_USER_CONTACT_TYPE => 'DB::Record::R::UserContactType',
    UTIL_DB_RECORD_R_USER_CONFIG       => 'DB::Record::R::UserConfig',
    UTIL_DB_RECORD_R_USER_TYPE         => 'DB::Record::R::UserType',
    UTIL_DB_RECORD_R_USER_TYPE_TASK    => 'DB::Record::R::UserTypeTask',
    UTIL_DB_RECORD_R_USER_STATUS       => 'DB::Record::R::UserStatus',

    UTIL_DB_RECORD_R_BUSINESS_USER_TYPE      => 'DB::Record::R::BusinessUserType',
    UTIL_DB_RECORD_R_BUSINESS_USER_TYPE_TASK => 'DB::Record::R::BusinessUserTypeTask',
    UTIL_DB_RECORD_R_BUSINESS_USER_STATUS    => 'DB::Record::R::BusinessUserStatus',

    UTIL_DB_RECORD_R_COUNTRY               => 'DB::Record::R::Country',
    UTIL_DB_RECORD_R_LANGUAGE              => 'DB::Record::R::Language',
    UTIL_DB_RECORD_R_TIME_ZONE             => 'DB::Record::R::TimeZone',
    UTIL_DB_RECORD_R_OAUTH                 => 'DB::Record::R::Oauth',
    UTIL_DB_RECORD_R_OAUTH_CONFIG          => 'DB::Record::R::OauthConfig',
    UTIL_DB_RECORD_R_NOTIFICATION          => 'DB::Record::R::Notification',
    UTIL_DB_RECORD_R_NOTIFICATION_INTERVAL => 'DB::Record::R::NotificationInterval',

    UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE      => 'DB::Record::R::BusinessContactType',
    UTIL_DB_RECORD_R_BUSINESS_USER_CONTACT_TYPE => 'DB::Record::R::BusinessUserContactType',

    #   bugzilla
    UTIL_BUGZILLA => 'Bugzilla',

    #   bbcode
    UTIL_BBCODE => 'BBCode',

    #   oauth
    UTIL_OAUTH => 'Oauth',

    #   contact
    UTIL_CONTACT => 'Contact',

    #   template
    UTIL_TEMPLATE => 'Template',

    #   output html
    UTIL_OUTPUT_HTML_PAGE       => 'Output::HTML::Page',
    UTIL_OUTPUT_HTML_PANEL      => 'Output::HTML::Panel',
    UTIL_OUTPUT_HTML_TABLE      => 'Output::HTML::Table',
    UTIL_OUTPUT_HTML_NAVIGATION => 'Output::HTML::Navigation',
    UTIL_OUTPUT_HTML_DIALOGUE   => 'Output::HTML::Dialogue',

    #   output html / js
    UTIL_OUTPUT_HTML_JS_CONFIRM => 'Output::HTML::JS::Confirm',

    #   kris kringle
    UTIL_SITE_KRISKRINGLE => 'Site::Kriskringle',
};

$EXPORT_TAGS{'util'} = [
    'UTIL_CGI',
    'UTIL_LOG',
    'UTIL_DB_MYSQL',
    'UTIL_DATABASE',
    'UTIL_PHRASE',
    'UTIL_ERROR',
    'UTIL_MESSAGE',
    'UTIL_SESSION',
    'UTIL_USER',
    'UTIL_BUSINESS',
    'UTIL_REQUEST',
    'UTIL_EMAIL',
    'UTIL_FEEDBACK',
    'UTIL_XML_LIBXML',
    'UTIL_DATA_LOADER_MAP',
    'UTIL_DATA_CONFIG_XML',
    'UTIL_TIME_ZONE',
    'UTIL_LANGUAGE',
    'UTIL_COUNTRY',
    'UTIL_IMAGE',
    'UTIL_DICTIONARY',
    'UTIL_NOTIFICATION',

    #   util db classes
    'UTIL_DB_BASIC',
    'UTIL_DB_TABLE',
    'UTIL_DB_VIEW',
    'UTIL_DB_DATA',
    'UTIL_DB_RECORD',

    #   util db record classes (user)
    'UTIL_DB_RECORD_USER',
    'UTIL_DB_RECORD_USER_OAUTH',

    'UTIL_DB_RECORD_USER_VIEW',
    'UTIL_DB_RECORD_USER_TASK_VIEW',

    #   util db record classes (business)
    'UTIL_DB_RECORD_BUSINESS',
    'UTIL_DB_RECORD_BUSINESS_USER',

    #   util db record classes (contact)
    'UTIL_DB_RECORD_CONTACT',
    'UTIL_DB_RECORD_USER_CONTACT',
    'UTIL_DB_RECORD_BUSINESS_CONTACT',
    'UTIL_DB_RECORD_BUSINESS_USER_CONTACT',
    'UTIL_DB_RECORD_SYSTEM_CONTACT',

    #   util db record classes (session)
    'UTIL_DB_RECORD_SESSION',
    'UTIL_DB_RECORD_SESSION_DATA',

    #   util db record classes (notification)
    'UTIL_DB_RECORD_USER_NOTIFICATION',
    'UTIL_DB_RECORD_USER_NOTIFICATION_CONFIG',

    #   util db record classes (reference)
    'UTIL_DB_RECORD_R_USER_CONTACT_TYPE',
    'UTIL_DB_RECORD_R_USER_CONFIG',
    'UTIL_DB_RECORD_R_USER_TYPE',
    'UTIL_DB_RECORD_R_USER_TYPE_TASK',
    'UTIL_DB_RECORD_R_USER_STATUS',

    'UTIL_DB_RECORD_R_BUSINESS_USER_TYPE',
    'UTIL_DB_RECORD_R_BUSINESS_USER_TYPE_TASK',
    'UTIL_DB_RECORD_R_BUSINESS_USER_STATUS',

    'UTIL_DB_RECORD_R_COUNTRY',
    'UTIL_DB_RECORD_R_LANGUAGE',
    'UTIL_DB_RECORD_R_TIME_ZONE',
    'UTIL_DB_RECORD_R_OAUTH',
    'UTIL_DB_RECORD_R_OAUTH_CONFIG',
    'UTIL_DB_RECORD_R_NOTIFICATION',
    'UTIL_DB_RECORD_R_NOTIFICATION_INTERVAL',

    'UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE',
    'UTIL_DB_RECORD_R_BUSINESS_USER_CONTACT_TYPE',

    #   bugzilla
    'UTIL_BUGZILLA',

    #   bbcode
    'UTIL_BBCODE',

    #   oauth
    'UTIL_OAUTH',

    #   contact
    'UTIL_CONTACT',

    #   template
    'UTIL_TEMPLATE',

    #   output html
    'UTIL_OUTPUT_HTML_PAGE',
    'UTIL_OUTPUT_HTML_PANEL',
    'UTIL_OUTPUT_HTML_TABLE',
    'UTIL_OUTPUT_HTML_NAVIGATION',
    'UTIL_OUTPUT_HTML_DIALOGUE',

    #   output html / js
    'UTIL_OUTPUT_HTML_JS_CONFIRM',

    #   kris kringle
    'UTIL_SITE_KRISKRINGLE',

];

$EXPORT_TAGS{'utils'} = $EXPORT_TAGS{'util'};

#
#   mode
#   - used generically in multiple places to specify
#     a data interaction mode
#
use constant {
    MODE_INSERT => 1,
    MODE_UPDATE => 2,
    MODE_QUERY  => 3,
};

$EXPORT_TAGS{'mode'} = [ 'MODE_INSERT', 'MODE_UPDATE', 'MODE_QUERY', ];

#
#   Util::User
#
use constant {
    USER_STATUS_INACTIVE           => 0,
    USER_STATUS_ACTIVE             => 1,
    USER_STATUS_CANCELLED          => 2,
    USER_STATUS_BANNED             => 3,
    USER_STATUS_PENDING_ACTIVATION => 4,
    USER_STATUS_DELETED            => 5,
    USER_STATUS_LOCKED             => 6,

    BUSINESS_USER_STATUS_INACTIVE => 0,
    BUSINESS_USER_STATUS_ACTIVE   => 1,
    BUSINESS_USER_STATUS_DELETED  => 2,

    USER_LOGIN_ACTIVE   => 1,
    USER_LOGIN_INACTIVE => 0,

    USER_AUTHEN_EXPIRY_MINUTES => 15,

    USER_TYPE_BASIC   => 1,
    USER_TYPE_PREMIUM => 2,

    BUSINESS_USER_TYPE_ADMINISTRATOR           => 1,
    BUSINESS_USER_TYPE_USER_ADMINISTRATOR      => 2,
    BUSINESS_USER_TYPE_INVENTORY_ADMINISTRATOR => 3,
};

$EXPORT_TAGS{'user'} = [
    'USER_STATUS_INACTIVE',
    'USER_STATUS_ACTIVE',
    'USER_STATUS_CANCELLED',
    'USER_STATUS_BANNED',
    'USER_STATUS_PENDING_ACTIVATION',
    'USER_STATUS_DELETED',
    'USER_STATUS_LOCKED',

    'BUSINESS_USER_STATUS_INACTIVE',
    'BUSINESS_USER_STATUS_ACTIVE',
    'BUSINESS_USER_STATUS_DELETED',

    'USER_LOGIN_ACTIVE',
    'USER_LOGIN_INACTIVE',

    'USER_AUTHEN_EXPIRY_MINUTES',

    'USER_TYPE_BASIC',
    'USER_TYPE_PREMIUM',

    'BUSINESS_USER_TYPE_ADMINISTRATOR',
    'BUSINESS_USER_TYPE_USER_ADMINISTRATOR',
    'BUSINESS_USER_TYPE_INVENTORY_ADMINISTRATOR',
];

#
#   Util::Business
#
use constant {
    BUSINESS_STATUS_INACTIVE => 0,
    BUSINESS_STATUS_ACTIVE   => 1,
    BUSINESS_STATUS_DELETED  => 2,

    BUSINESS_USER_STATUS_INACTIVE => 0,
    BUSINESS_USER_STATUS_ACTIVE   => 1,
    BUSINESS_USER_STATUS_DELETED  => 2,
};

$EXPORT_TAGS{'business'} = [
    'BUSINESS_STATUS_INACTIVE',
    'BUSINESS_STATUS_ACTIVE',
    'BUSINESS_STATUS_DELETED',

    'BUSINESS_USER_STATUS_INACTIVE',
    'BUSINESS_USER_STATUS_ACTIVE',
    'BUSINESS_USER_STATUS_DELETED',
];

#
#   sections
#
use constant {
    SECTION_HEADER    => 'header',
    SECTION_PRIMARY   => 'primary',
    SECTION_SECONDARY => 'secondary',
    SECTION_TERTIARY  => 'tertiary',
    SECTION_FOOTER    => 'footer',
};

$EXPORT_TAGS{'section'} = [ 'SECTION_HEADER', 'SECTION_PRIMARY', 'SECTION_SECONDARY', 'SECTION_TERTIARY', 'SECTION_FOOTER', ];

#   tasks
#
use constant {
    TASK_PUBLIC             => 1,
    TASK_USER_ACCOUNT       => 2,
    TASK_BUSINESS_ACCOUNT   => 3,
    TASK_BUSINESS_USER      => 4,
    TASK_BUSINESS_INVENTORY => 5,

    TASK_APPLICATION_USER => 10,
};

$EXPORT_TAGS{'task'} = [
    'TASK_PUBLIC',
    'TASK_USER_ACCOUNT',
    'TASK_BUSINESS_ACCOUNT',
    'TASK_BUSINESS_USER',
    'TASK_BUSINESS_INVENTORY',

    'TASK_APPLICATION_USER',
];

$EXPORT_TAGS{'tasks'} = $EXPORT_TAGS{'task'};

#
#   Util::Output::HTML::Panel
#
use constant {
    PANEL_STYLE_BASIC    => 1,
    PANEL_STYLE_STANDARD => 2,
};

$EXPORT_TAGS{'panel-style'} = [ 'PANEL_STYLE_BASIC', 'PANEL_STYLE_STANDARD', ];

$EXPORT_TAGS{'panel-styles'} = $EXPORT_TAGS{'panel-style'};

#
#   Util::Output::HTML::Table
#
use constant {
    TABLE_STYLE_BASIC      => 1,
    TABLE_CLASS_VERTICAL   => 'TableVertical',
    TABLE_CLASS_HORIZONTAL => 'TableHorizontal',
    TABLE_ROW_CLASS_EVEN   => 'RowEven',
    TABLE_ROW_CLASS_ODD    => 'RowOdd',
};

$EXPORT_TAGS{'table-style'} =
    [ 'TABLE_STYLE_BASIC', 'TABLE_CLASS_VERTICAL', 'TABLE_CLASS_HORIZONTAL', 'TABLE_ROW_CLASS_EVEN', 'TABLE_ROW_CLASS_ODD', ];

$EXPORT_TAGS{'table-styles'} = $EXPORT_TAGS{'table-style'};

#
#   Util::Output::HTML::Navigation
#
use constant {

    #   orientation
    LINK_VERTICAL   => 1,
    LINK_HORIZONTAL => 2,

    #   align
    LINK_LEFT   => 10,
    LINK_RIGHT  => 11,
    LINK_CENTER => 12,
};

$EXPORT_TAGS{'navigation-style'} = [

    #   orientation
    'LINK_VERTICAL',
    'LINK_HORIZONTAL',

    #   align
    'LINK_LEFT',
    'LINK_RIGHT',
    'LINK_CENTER',
];

$EXPORT_TAGS{'link'} = $EXPORT_TAGS{'navigation-style'};

#
#   Util::Output::HTML::Navigation
#
use constant {
    LINK_VERTICAL   => 1,
    LINK_HORIZONTAL => 2,
    LINK_LEFT       => 10,
    LINK_RIGHT      => 11,
    LINK_CENTER     => 12,

    LINK_ORIENTATION_VERTICAL   => 1,
    LINK_ORIENTATION_HORIZONTAL => 2,

    LINK_ALIGN_LEFT   => 10,
    LINK_ALIGN_RIGHT  => 11,
    LINK_ALIGN_CENTER => 12,
};

$EXPORT_TAGS{'link'} = [
    'LINK_VERTICAL',    'LINK_HORIZONTAL',           'LINK_LEFT',                   'LINK_RIGHT',
    'LINK_CENTER',      'LINK_ORIENTATION_VERTICAL', 'LINK_ORIENTATION_HORIZONTAL', 'LINK_ALIGN_LEFT',
    'LINK_ALIGN_RIGHT', 'LINK_ALIGN_CENTER',
];

#
#   Util::Output::HTML::Page
#
use constant { DEFAULT_HTML_PAGE_TEMPLATE => 'html/shared/page', };

$EXPORT_TAGS{'html-page'} = [ 'DEFAULT_HTML_PAGE_TEMPLATE', ];

#
#   Util::Output::HTML::JS::Confirm
#
use constant { JS_CONFIRM_TYPE_DELETE => 1, };

$EXPORT_TAGS{'js-confirm'} = [ 'JS_CONFIRM_TYPE_DELETE', ];

#
#   Comp::HTML::Form
#   Comp::HTML::Form::Basic
#   Comp::HTML::Form::Tabular
#
use constant {
    FORM_INPUT_TYPE_TEXTFIELD => 1,
    FORM_INPUT_TYPE_PASSWORD  => 2,
    FORM_INPUT_TYPE_CHECKBOX  => 3,
    FORM_INPUT_TYPE_LIST      => 4,
    FORM_INPUT_TYPE_TEXTAREA  => 5,
    FORM_INPUT_TYPE_HIDDEN    => 6,
    FORM_INPUT_TYPE_TEXT      => 7,

    FORM_DATA_TYPE_NUMBER => 1,
    FORM_DATA_TYPE_TEXT   => 2,

    FORM_ACTION_SUBMIT   => 'form-submit',
    FORM_ACTION_POPULATE => 'form-populate',

    DEFAULT_VALIDATION_REQUIRED_PHRASE     => 30_500,
    DEFAULT_VALIDATION_TOO_LONG_PHRASE     => 30_510,
    DEFAULT_VALIDATION_TOO_SHORT_PHRASE    => 30_520,
    DEFAULT_VALIDATION_UNRECOGNIZED_PHRASE => 30_530,

    DEFAULT_SUBMIT_LABEL_PHRASE => 30_600,
    DEFAULT_NO_DATA_PHRASE      => 30_610,

    VALIDATION_ERROR_REQUIRED   => 30_000,
    VALIDATION_ERROR_MAX_LENGTH => 30_010,
    VALIDATION_ERROR_MIN_LENGTH => 30_020,
    VALIDATION_ERROR_DO_MATCH   => 30_030,
    VALIDATION_ERROR_DONT_MATCH => 30_040,

};

$EXPORT_TAGS{'form'} = [
    'FORM_INPUT_TYPE_TEXTFIELD',
    'FORM_INPUT_TYPE_PASSWORD',
    'FORM_INPUT_TYPE_CHECKBOX',
    'FORM_INPUT_TYPE_LIST',
    'FORM_INPUT_TYPE_TEXTAREA',
    'FORM_INPUT_TYPE_HIDDEN',
    'FORM_INPUT_TYPE_TEXT',

    'FORM_DATA_TYPE_NUMBER',
    'FORM_DATA_TYPE_TEXT',

    'FORM_ACTION_SUBMIT',
    'FORM_ACTION_POPULATE',

    'DEFAULT_VALIDATION_REQUIRED_PHRASE',
    'DEFAULT_VALIDATION_TOO_LONG_PHRASE',
    'DEFAULT_VALIDATION_TOO_SHORT_PHRASE',
    'DEFAULT_VALIDATION_UNRECOGNIZED_PHRASE',

    'DEFAULT_SUBMIT_LABEL_PHRASE',
    'DEFAULT_NO_DATA_PHRASE',

    'VALIDATION_ERROR_REQUIRED',
    'VALIDATION_ERROR_MAX_LENGTH',
    'VALIDATION_ERROR_MIN_LENGTH',
    'VALIDATION_ERROR_DO_MATCH',
    'VALIDATION_ERROR_DONT_MATCH',

];

use constant { DEFAULT_MAX_REFERENCE_DATA_ROWS => 300, };

$EXPORT_TAGS{'reference'} = [ 'DEFAULT_MAX_REFERENCE_DATA_ROWS', ];

#
#   Util::Message
#
use constant {

    #   message types
    MESSAGE_TYPE_SUCCESS => 1,
    MESSAGE_TYPE_ERROR   => 2,
    MESSAGE_TYPE_INFO    => 3,

    MESSAGE_DEFAULT_NAME => 'global',
    MESSAGE_DEFAULT_TYPE => 1,
};

$EXPORT_TAGS{'message-type'} =
    [ 'MESSAGE_TYPE_SUCCESS', 'MESSAGE_TYPE_ERROR', 'MESSAGE_TYPE_INFO', 'MESSAGE_DEFAULT_NAME', 'MESSAGE_DEFAULT_TYPE', ];

$EXPORT_TAGS{'message-types'} = $EXPORT_TAGS{'message-type'};
$EXPORT_TAGS{'message'}       = $EXPORT_TAGS{'message-type'};

#
#   Util::Oauth
#
use constant {
    OAUTH_FACEBOOK  => 1,
    OAUTH_GOOGLE    => 2,
    OAUTH_INSTAGRAM => 3,
};

$EXPORT_TAGS{'oauth'} = [ 'OAUTH_FACEBOOK', 'OAUTH_GOOGLE', 'OAUTH_INSTAGRAM', ];

#
#   Util::Contact
#   - as_r_contact_type
#
use constant {
    CONTACT_TYPE_USER_PHYSICAL => 1,
    CONTACT_TYPE_USER_BILLING  => 2,
    CONTACT_TYPE_USER_SHIPPING => 3,

    CONTACT_TYPE_BUSINESS_PHYSICAL          => 1,
    CONTACT_TYPE_BUSINESS_SUPPORT_GENERAL   => 2,
    CONTACT_TYPE_BUSINESS_SUPPORT_ACCOUNT   => 3,
    CONTACT_TYPE_BUSINESS_SUPPORT_TECHNICAL => 4,

    CONTACT_TYPE_SYSTEM_SUPPORT_GENERAL   => 0,
    CONTACT_TYPE_SYSTEM_SUPPORT_ACCOUNT   => 1,
    CONTACT_TYPE_SYSTEM_SUPPORT_TECHNICAL => 2,
};

$EXPORT_TAGS{'contact-type'} = [
    'CONTACT_TYPE_USER_PHYSICAL',
    'CONTACT_TYPE_USER_BILLING',
    'CONTACT_TYPE_USER_SHIPPING',

    'CONTACT_TYPE_BUSINESS_PHYSICAL',
    'CONTACT_TYPE_BUSINESS_SUPPORT_GENERAL',
    'CONTACT_TYPE_BUSINESS_SUPPORT_ACCOUNT',
    'CONTACT_TYPE_BUSINESS_SUPPORT_TECHNICAL',

    'CONTACT_TYPE_SYSTEM_SUPPORT_GENERAL',
    'CONTACT_TYPE_SYSTEM_SUPPORT_ACCOUNT',
    'CONTACT_TYPE_SYSTEM_SUPPORT_TECHNICAL',
];

#
#   Util::Request
#   - as_r_request_type
#     as_r_request_status
#     as_r_message_recipient
#
use constant {

    #   types
    REQUEST_TYPE_USER_REGISTER                       => 1,
    REQUEST_TYPE_USER_CHANGE_EMAIL_ADDRESS           => 2,
    REQUEST_TYPE_USER_CHANGE_ALTERNATE_EMAIL_ADDRESS => 3,
    REQUEST_TYPE_USER_PASSWORD_RESET                 => 4,

    REQUEST_TYPE_BUSINESS_USER_REGISTER => 10,

    #   status
    REQUEST_STATUS_NEW      => 1,
    REQUEST_STATUS_DENIED   => 2,
    REQUEST_STATUS_APPROVED => 3,
    REQUEST_STATUS_EXPIRED  => 4,

    #   request message recipient
    REQUEST_MESSAGE_RECIPIENT_REQUESTER => 1,
    REQUEST_MESSAGE_RECIPIENT_SYSTEM    => 2,
    REQUEST_MESSAGE_RECIPIENT_BUSINESS  => 3,

};

$EXPORT_TAGS{'request'} = [

    #   types
    'REQUEST_TYPE_USER_REGISTER',
    'REQUEST_TYPE_USER_CHANGE_EMAIL_ADDRESS',
    'REQUEST_TYPE_USER_CHANGE_ALTERNATE_EMAIL_ADDRESS',
    'REQUEST_TYPE_USER_PASSWORD_RESET',

    'REQUEST_TYPE_BUSINESS_USER_REGISTER',

    #   status
    'REQUEST_STATUS_NEW',
    'REQUEST_STATUS_DENIED',
    'REQUEST_STATUS_APPROVED',
    'REQUEST_STATUS_EXPIRED',

    #   request message recipient
    'REQUEST_MESSAGE_RECIPIENT_REQUESTER',
    'REQUEST_MESSAGE_RECIPIENT_SYSTEM',
    'REQUEST_MESSAGE_RECIPIENT_BUSINESS',
];

use constant {
    CACHE_CATEGORY_USER      => 1,
    CACHE_CATEGORY_USER_TASK => 2,
};

$EXPORT_TAGS{'cache'} = [ 'CACHE_CATEGORY_USER', 'CACHE_CATEGORY_USER_TASK', ];

#
#   error codes global
#
use constant {
    ERROR_CODE_UNAUTHORIZED => 1,

    ERROR_CODE_API_FAILED_INPUT_PARSE => 10,

    ERROR_CODE_API_MISSING_COMPONENT => 20,
    ERROR_CODE_API_MISSING_ACTION    => 21,

};

$EXPORT_TAGS{'error-codes-global'} = [

    #   authorization
    'ERROR_CODE_UNAUTHORIZED',

    #   api
    'ERROR_CODE_API_FAILED_INPUT_PARSE',
    'ERROR_CODE_API_MISSING_COMPONENT',
    'ERROR_CODE_API_MISSING_ACTION',
];

#
#   error codes user
#
use constant {
    ERROR_CODE_USER_NOT_LOGGED_IN            => 100,
    ERROR_CODE_USER_BANNED                   => 101,
    ERROR_CODE_USER_PENDING_ACTIVATION       => 102,
    ERROR_CODE_USER_STATUS_CHANGE_INVALID    => 103,
    ERROR_CODE_USER_STATUS_CHANGE_DISALLOWED => 104,

    ERROR_CODE_USER_HANDLE_NOT_UNIQUE        => 110,
    ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE => 111,
};

$EXPORT_TAGS{'error-codes-user'} = [
    'ERROR_CODE_USER_NOT_LOGGED_IN',            'ERROR_CODE_USER_BANNED',
    'ERROR_CODE_USER_PENDING_ACTIVATION',       'ERROR_CODE_USER_STATUS_CHANGE_INVALID',
    'ERROR_CODE_USER_STATUS_CHANGE_DISALLOWED', 'ERROR_CODE_USER_HANDLE_NOT_UNIQUE',
    'ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE',
];

#
#   error codes business
#
use constant { ERROR_CODE_BUSINESS_NAME_NOT_UNIQUE => 100, };

$EXPORT_TAGS{'error-codes-business'} = [ 'ERROR_CODE_BUSINESS_NAME_NOT_UNIQUE', ];

$EXPORT_TAGS{'error-codes'} =
    [ @{ $EXPORT_TAGS{'error-codes-global'} }, @{ $EXPORT_TAGS{'error-codes-user'} }, @{ $EXPORT_TAGS{'error-codes-business'} } ];

$EXPORT_TAGS{'error-code'} = $EXPORT_TAGS{'error-codes'};

#
#   notifications
#
use constant {
    NOTIFICATION_STATUS_ENABLED  => 1,
    NOTIFICATION_STATUS_DISABLED => 2,
};

$EXPORT_TAGS{'notification-statuses'} = [ 'NOTIFICATION_STATUS_ENABLED', 'NOTIFICATION_STATUS_DISABLED', ];

use constant {
    NOTIFICATION_METHOD_EMAIL   => 1,
    NOTIFICATION_METHOD_MESSAGE => 2,
};

$EXPORT_TAGS{'notification-methods'} = [ 'NOTIFICATION_METHOD_EMAIL', 'NOTIFICATION_METHOD_MESSAGE', ];

use constant {
    NOTIFICATION_INTERVAL_HOURLY        => 1,
    NOTIFICATION_INTERVAL_DAILY         => 2,
    NOTIFICATION_INTERVAL_TWICE_DAILY   => 3,
    NOTIFICATION_INTERVAL_WEEKLY        => 4,
    NOTIFICATION_INTERVAL_TWICE_WEEKLY  => 5,
    NOTIFICATION_INTERVAL_MONTHLY       => 6,
    NOTIFICATION_INTERVAL_TWICE_MONTHLY => 7,
};

$EXPORT_TAGS{'notification-intervals'} = [
    'NOTIFICATION_INTERVAL_HOURLY',       'NOTIFICATION_INTERVAL_DAILY',
    'NOTIFICATION_INTERVAL_TWICE_DAILY',  'NOTIFICATION_INTERVAL_WEEKLY',
    'NOTIFICATION_INTERVAL_TWICE_WEEKLY', 'NOTIFICATION_INTERVAL_MONTHLY',
    'NOTIFICATION_INTERVAL_TWICE_MONTHLY',
];

#
#   db record
#
use constant {
    SQL_OPERATOR_IN           => 'IN',
    SQL_OPERATOR_NOT_IN       => 'NOT IN',
    SQL_OPERATOR_EQUALS       => '=',
    SQL_OPERATOR_LIKE         => 'LIKE',
    SQL_OPERATOR_NOT_EQUALS   => '!=',
    SQL_OPERATOR_LESS_THAN    => '<',
    SQL_OPERATOR_GREATER_THAN => '>',
};

$EXPORT_TAGS{'db-record'} = [
    'SQL_OPERATOR_IN',         'SQL_OPERATOR_NOT_IN',    'SQL_OPERATOR_EQUALS', 'SQL_OPERATOR_LIKE',
    'SQL_OPERATOR_NOT_EQUALS', 'SQL_OPERATOR_LESS_THAN', 'SQL_OPERATOR_GREATER_THAN',
];

$EXPORT_TAGS{'links'} = $EXPORT_TAGS{'link'};

#
#   defaults
#
use constant {
    DEFAULT_LANGUAGE  => 1,      #   en
    DEFAULT_TIME_ZONE => 339,    #   melbourne australia
};

$EXPORT_TAGS{'defaults'} = [ 'DEFAULT_LANGUAGE', 'DEFAULT_TIME_ZONE', ];

#
#   languages
#   - these are not correct and should be removed once
#     i've worked out where they are used..
#
use constant {
    LANGUAGE_AMERICAN   => 1,
    LANGUAGE_ENGLISH    => 2,
    LANGUAGE_AUSTRALIAN => 3,
};

$EXPORT_TAGS{'language'} = [ 'LANGUAGE_AMERICAN', 'LANGUAGE_ENGLISH', 'LANGUAGE_AUSTRALIAN', ];

foreach my $tag ( keys %EXPORT_TAGS ) {
    Exporter::export_ok_tags($tag);
}
@{ $EXPORT_TAGS{'all'} } = @EXPORT_OK;

1;
