
=pod

=head1 NAME

AlienSpaces::Manual::IFace::Web::HTML

=head1 VERSION

This document describes AlienSpaces::Manual::IFace::Web::HTML version $Revision$

=head1 DESCRIPTION

This document describes the distributed HTML web interfaces.

=head1 SCREENS 

=head2 Public

=over

=item /html/site/home

The default entry point for an application.

=item /html/site/user/login

Log into the application authenticating against a local database.

=item /html/site/user/logout

Log a user out of the application.

=item /html/site/user/register

Register a new user account storing account authentication information in the local database.

=item /html/site/user/oauth/login

Log into the application authenticating against an oauth provider.

=item /html/site/user/oauth/register

Register a new user account using an en oauth provider for account authentication.

=item /html/site/user/account/password/reset

Reset an account password.

=item /html/site/user/account/password/reset/approve

Approve an account password reset request.

=item /html/site/user/account/password/reset/deny

Deny an account password reset request.

=back

=head2 User Account Management

=over

=item /html/site/user/account

=item /html/site/user/account/profile

=item /html/site/user/account/email/primary

=item /html/site/user/account/email/alternate

=item /html/site/user/account/contact/add

=item /html/site/user/account/contact/update

=item /html/site/user/account/notification

=item /html/site/user/account/password

=item /html/site/user/account/purchase/history

=item /html/site/user/account/purchase/download

=back

=head2 Business Account Management

=over

=item /html/site/business/account

=item /html/site/business/account/profile/add

=item /html/site/business/account/profile/update

=item /html/site/business/account/contact/add

=item /html/site/business/account/contact/update

=back

=head2 Business User Account Management

=over

=item /html/site/business/user

=item /html/site/business/user/account

=item /html/site/business/user/register

=item /html/site/business/user/account/profile/update

=item /html/site/business/user/account/contact/add

=item /html/site/business/user/account/contact/update

=back

=head2 Application User Account Management

=over

=item /html/site/application/user

=item /html/site/application/user/account

=item /html/site/application/user/account/profile/update

=item /html/site/application/user/account/contact/add

=item /html/site/application/user/account/contact/update

=back

=head2 Application Inventory Management

=over

=item /html/site/application/inventory/product

=item /html/site/application/inventory/product/add

=item /html/site/application/inventory/product/update

=item /html/site/application/inventory/product/delete

=item /html/site/application/inventory/product/category

=item /html/site/application/inventory/product/category/add

=item /html/site/application/inventory/product/category/update

=item /html/site/application/inventory/product/category/delete

=item /html/site/application/inventory/product/category/move

=back

=head2 Application Store

=over

=item /html/site/application/store/cart

=item /html/site/application/store/cart/shipping

=item /html/site/application/store/cart/checkout

=item /html/site/application/store/browse

=item /html/site/application/store/product

=back

=head1 SEE ALSO

See L<AlienSpaces::Manual/AlienSpaces Manual> for complete documentation reference.

=head1 AUTHOR

Benjamin Wallin (alienspaces) C<< <alienspaces@gmail.com> >>

=head1 LICENCE AND COPYRIGHT

Copyright (c) 20014, Benjamin Wallin (alienspaces) C<< <alienspaces@gmail.com> >>. All rights reserved.

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself.

=head1 DISCLAIMER OF WARRANTY

BECAUSE THIS SOFTWARE IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
FOR THE SOFTWARE, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
PROVIDE THE SOFTWARE "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE
ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE OF THE SOFTWARE IS WITH
YOU. SHOULD THE SOFTWARE PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL
NECESSARY SERVICING, REPAIR, OR CORRECTION.

IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
REDISTRIBUTE THE SOFTWARE AS PERMITTED BY THE ABOVE LICENCE, BE
LIABLE TO YOU FOR DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL,
OR CONSEQUENTIAL DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE
THE SOFTWARE (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING
RENDERED INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A
FAILURE OF THE SOFTWARE TO OPERATE WITH ANY OTHER SOFTWARE), EVEN IF
SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF
SUCH DAMAGES.

=cut


