package AlienSpaces::IFace::Shell::Builder;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use File::Path qw(make_path);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

#   private constants
use constant { MAX_RECURSE_DEPTH => 100, };

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_XML_LIBXML() ];

    return $util_list;
}

sub init_database_connection {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Not initialising database connection' } );

    return 1;
}

sub options_config {
    return 'c:';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-c - build configuration file
USAGE

    return;
}

#   file permissions to set files to after copy process
my $FILE_PERMISSIONS = {
    'pl'        => '0755',
    'sh'        => '0755',
    'pm'        => '0644',
    'conf'      => '0644',
    'css'       => '0644',
    'js'        => '0644',
    'gif'       => '0644',
    'jpg'       => '0644',
    'png'       => '0644',
    'html'      => '0644',
    'ods'       => '0644',
    'sql'       => '0644',
    'xml'       => '0644',
    'json'      => '0644',
    'txt'       => '0644',
    'tt'        => '0644',
    't'         => '0775',
    'gitignore' => '0644',
};

sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;
    if ( !defined $options ) {
        $self->error( { '-text' => 'No options provided on command line' } );
        return;
    }

    my $config_file = $options->{'c'};
    if ( !defined $config_file ) {
        $self->error( { '-text' => '-c Configuration file argument required' } );
        $self->options_usage();
        return;
    }

    #   config
    my $config = $self->get_config( { '-file' => $config_file } );

    #   process paths
    foreach my $path_config ( @{$config} ) {

        $self->debug( { '-text' => 'Have path -source ' . $path_config->{'-source'} . ' -target ' . $path_config->{'-target'} } );

        if ( !-d $path_config->{'-source'} ) {
            $self->debug(
                { '-text' => 'Cannot build from non existent source path ' . $self->dbg_value( $path_config->{'-source'} ) } );
            next;
        }

        if ( !-d $path_config->{'-target'} ) {
            $self->create_path( { '-path' => $path_config->{'-target'} } );
        }

        #   process includes
        foreach my $include ( @{ $path_config->{'-include'} } ) {

            #   source
            my $include_source = $path_config->{'-source'} . '/' . $include->{'-source'};
            if ( !-d $include_source ) {
                $self->debug(
                    { '-text' => 'Cannot build from non existent include source path ' . $self->dbg_value($include_source) } );
                next;
            }

            #   recurse source
            my $recurse = $include->{'-recurse'} || 0;

            #   target
            my $include_target = $path_config->{'-target'} . '/' . $include->{'-target'};

            #   clean target
            if ( -d $include_target && $include->{'-clean'} ) {
                $self->remove_path( { '-path' => $include_target } );
            }

            #   permissions
            my $include_permissions = $include->{'-permissions'};

            #   create target if not exists
            if ( !-d $include_target ) {
                $self->create_path( { '-path' => $include_target, '-permissions' => $include_permissions } );
            }

            #   process files
            foreach my $file ( @{ $include->{'-file'} } ) {

                my $match_file  = $file->{'-match_file'};
                my $parse       = $file->{'-parse'};
                my $permissions = $file->{'-permissions'};

                $self->debug(
                    {   '-text' => 'Search for files -match_file ' .
                            $match_file . ' -source_path ' . $include_source . ' -parse ' . $self->dbg_value($parse) } );

                $self->process_path(
                    {   '-source'           => $include_source,
                        '-target'           => $include_target,
                        '-permissions'      => $include_permissions,
                        '-file_permissions' => $permissions,
                        '-match_file'       => $match_file,
                        '-parse'            => $parse,
                        '-recurse'          => $recurse
                    } );
            }
        } ## end foreach my $include ( @{ $path_config...})
    } ## end foreach my $path_config ( @...)

    return 1;
} ## end sub main

sub remove_path {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-path', ], $args ) ) {
        return;
    }

    my $path = $args->{'-path'};

    if ( !-d $path ) {
        $self->fatal( { '-text' => 'Remove -path ' . $self->dbg_value($path) . ' does not exist' } );
    }

    $self->debug( { '-text' => 'Removing -path ' . $path } );

    my $success = eval {
        `rm -rf $path`;
        1;
    };

    if ( !$success ) {
        $self->error( { '-text' => 'Failed to remove -path ' . $path . ' :' . $EVAL_ERROR } );
    }

    return;
}

sub process_path {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-source', '-target', '-match_file', '-parse', '-recurse' ], $args ) ) {
        return;
    }

    my $source           = $args->{'-source'};
    my $target           = $args->{'-target'};
    my $match_file       = $args->{'-match_file'};
    my $parse            = $args->{'-parse'};
    my $recurse          = $args->{'-recurse'};
    my $permissions      = $args->{'-permissions'};
    my $file_permissions = $args->{'-file_permissions'};

    #   source paths
    my @source_paths = ($source);

    #   target paths
    my @target_paths = ($target);

    if ($recurse) {
        $self->get_paths(
            { '-source' => $source, '-source_paths' => \@source_paths, '-target' => $target, '-target_paths' => \@target_paths } );
    }

    $self->debug( { '-text' => 'Processing -source_paths ' . $self->dbg_value( \@source_paths ) } );
    $self->debug( { '-text' => 'Processing -target_paths ' . $self->dbg_value( \@target_paths ) } );

    foreach my $idx ( 0 .. scalar(@source_paths) - 1 ) {
        my $source_path = $source_paths[$idx];
        my $target_path = $target_paths[$idx];

        if ( !-d $target_path ) {
            $self->create_path( { '-path' => $target_path, '-permissions' => $permissions } );
        }

        my $dir_fh;
        opendir( $dir_fh, $source_path ) or croak 'Failed to open -source_path ' . $source_path . ' :' . $OS_ERROR;
        my @files = readdir($dir_fh);
        closedir($dir_fh) or croak 'Failed to close -source_path ' . $source_path . ' :' . $OS_ERROR;

    PROCESS_FILES:
        foreach my $file (@files) {
            if ( $file =~ /^\.$/sxmg ) {
                next PROCESS_FILES;
            }
            if ( $file =~ /^\.\.$/sxmg ) {
                next PROCESS_FILES;
            }
            if ( $file !~ /$match_file/sxmg ) {
                $self->debug( { '-text' => "Skipping -file $file -match_file $match_file" } );
                next PROCESS_FILES;
            }

            my $process_source_file = $source_path . '/' . $file;
            my $process_target_file = $target_path . '/' . $file;

            if ( -l $process_source_file ) {
                $self->debug( { '-text' => 'File -file ' . $file . ' is a symlink, copying' } );
                if (!$self->system_command(
                        { '-command' => 'cp --no-dereference ' . $process_source_file . ' ' . $process_target_file } )
                    ) {
                    $self->error( { '-text' => 'Failed executing system copy command ' . $OS_ERROR } );
                    return;
                }
                next PROCESS_FILES;
            }

            #   process file contents
            my $file_contents = $self->process_file( { '-file' => $process_source_file, '-parse' => $parse } );

            if ( !defined $file_contents ) {
                $self->debug( { '-text' => 'File contents empty' } );
                next PROCESS_FILES;
            }

            #   write out new file
            my $fh;
            open( $fh, '>', $process_target_file ) or
                croak 'Failed to open -target_file ' . $process_target_file . ' for writing :' . $OS_ERROR;
            print {$fh} $file_contents;
            close $fh or croak 'Failed to close -target_file ' . $process_target_file . ' :' . $OS_ERROR;

            #   set permissions
            $self->set_file_permissions( { '-file' => $process_target_file, '-permissions' => $file_permissions } );
        } ## end PROCESS_FILES: foreach my $file (@files)
    } ## end foreach my $idx ( 0 .. scalar...)

    return;
} ## end sub process_path

sub set_file_permissions {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-file'], $args ) ) {
        return;
    }

    my $file        = $args->{'-file'};
    my $permissions = $args->{'-permissions'};

    my $file_extension;
    if ( $file =~ /.*\.([a-z]+)/sxmg ) {
        $file_extension = $1;
    }

    #   permissions based on file extension
    if ( defined $file_extension && !defined $permissions ) {
        $permissions = $FILE_PERMISSIONS->{$file_extension};
    }

    if ( !defined $permissions ) {
        $self->fatal( { '-text' => 'Have no file permission configuration for -extension ' . $file_extension } );
    }

    chmod oct($permissions), $file;

    return 1;
}

sub process_file {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-file', '-parse' ], $args ) ) {
        return;
    }

    my $file  = $args->{'-file'};
    my $parse = $args->{'-parse'};

    #   read in file
    my $file_contents;
    my $fh;
    open( $fh, '<', $file ) or croak 'Failed to open -source_file ' . $file . ' for writing :' . $OS_ERROR;
    {
        local $INPUT_RECORD_SEPARATOR;
        $file_contents = <$fh>;
    }
    close $fh or croak 'Failed to close -source_file ' . $file . ' :' . $OS_ERROR;

    if ( !defined $file_contents ) {
        $self->debug( { '-text' => 'File -source_file ' . $file . ' is empty..' } );
        return;
    }

    if ( defined $parse && scalar @{$parse} ) {

    PROCESS_PARSE:
        foreach my $parse_rule ( @{$parse} ) {
            my $match_string   = $parse_rule->{'-match_string'};
            my $replace_string = $parse_rule->{'-replace_string'};

            if ( !defined $match_string || !defined $replace_string ) {
                $self->debug(
                    {   '-text' => 'Missing -match_string ' .
                            $self->dbg_value($match_string) . ' or -replace_string ' . $self->dbg_value($replace_string) } );
                next PROCESS_PARSE;
            }

            $self->debug(
                {   '-text' => 'Replacing contents -source_file ' .
                        $file . ' -match_string ' . $match_string . ' -replace_string ' . $replace_string
                } );
            $file_contents =~ s/$match_string/$replace_string/sxmg;
        }

    PROCESS_ENVIRONMENT:
        foreach my $environment_key ( keys %{ $self->environment } ) {
            my $match_string   = $environment_key;
            my $replace_string = $self->environment->{$environment_key};

            $file_contents =~ s/\[\%\s?$match_string\s?\%\]/$replace_string/sxmg;
        }
    }

    return $file_contents;
} ## end sub process_file

sub get_paths {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-source', '-source_paths', '-target', '-target_paths' ], $args ) ) {
        return;
    }

    my $source       = $args->{'-source'};
    my $source_paths = $args->{'-source_paths'};
    my $target       = $args->{'-target'};
    my $target_paths = $args->{'-target_paths'};

    $self->debug( { '-text' => 'Recursing' } );

    foreach my $source_path ( @{$source_paths} ) {

        my $dir_fh;
        opendir( $dir_fh, $source_path ) or croak 'Failed to open -source_path ' . $source_path . ' :' . $OS_ERROR;
        my @files = readdir($dir_fh);
        closedir($dir_fh) or croak 'Failed to close -source_path ' . $source_path . ' :' . $OS_ERROR;

    FIND_DIRECTORIES:
        foreach my $file (@files) {
            if ( $file =~ /^\./sxmg ) {
                next FIND_DIRECTORIES;
            }
            my $new_source_path = $source_path . '/' . $file;

            if ( !-d $new_source_path ) {
                next FIND_DIRECTORIES;
            }

            #   add to source paths
            push @{$source_paths}, $new_source_path;

            #   add to target paths
            my $new_target_path = $new_source_path;
            $new_target_path =~ s/^$source/$target/sxmg;
            push @{$target_paths}, $new_target_path;

        }
    }

    return;
} ## end sub get_paths

sub create_path {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-path'], $args ) ) {
        return;
    }

    my $path        = $args->{'-path'};
    my $permissions = $args->{'-permissions'};

    make_path($path);

    if ( !-d $path ) {
        $self->fatal( { '-text' => 'Failed to make path ' . $path } );
    }

    #   set permissions
    if ( defined $permissions ) {
        $self->debug( { '-text' => 'Setting file -permissions ' . $permissions . ' -path ' . $path } );
        $self->set_file_permissions( { '-file' => $path, '-permissions' => $permissions } );
    }

    return 1;
}

sub get_config {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-file'], $args ) ) {
        return;
    }

    #   xml libxml util
    my $xml_libxml_util = $self->utils->{ UTIL_XML_LIBXML() };

    #   file
    my $file = $args->{'-file'};

    $self->debug( { '-text' => 'Using configuration ' . $file } );

    #   document
    my $doc = $xml_libxml_util->load( { '-file' => $file } );

    #   config
    my $config = [];

    #   path
PATH_NODES:
    foreach my $path_node ( $doc->childNodes ) {

        if ( $path_node->nodeName ne 'Path' ) {
            next PATH_NODES;
        }

        $self->debug( { '-text' => 'Have path node' } );

        my $path_config = {};

        $path_config->{'-source'}  = $path_node->getAttribute('Source');
        $path_config->{'-target'}  = $path_node->getAttribute('Target');
        $path_config->{'-include'} = [];

        #   include
    INCLUDE_NODES:
        foreach my $include_node ( $path_node->childNodes ) {

            if ( $include_node->nodeName ne 'Include' ) {
                next INCLUDE_NODES;
            }

            $self->debug( { '-text' => 'Have include node' } );

            my $include_config = {};

            $include_config->{'-source'}      = $include_node->getAttribute('Source');
            $include_config->{'-target'}      = $include_node->getAttribute('Target');
            $include_config->{'-recurse'}     = $include_node->getAttribute('Recurse');
            $include_config->{'-clean'}       = $include_node->getAttribute('Clean');
            $include_config->{'-permissions'} = $include_node->getAttribute('Permissions');
            $include_config->{'-file'}        = [];

            #   file
        FILE_NODES:
            foreach my $file_node ( $include_node->childNodes ) {

                if ( $file_node->nodeName ne 'File' ) {
                    next FILE_NODES;
                }

                my $file_config = {};
                $file_config->{'-match_file'}  = $file_node->getAttribute('MatchFile');
                $file_config->{'-permissions'} = $file_node->getAttribute('Permissions');
                $file_config->{'-parse'}       = [];

                #   parse
            PARSE_NODES:
                foreach my $parse_node ( $file_node->childNodes ) {

                    if ( $parse_node->nodeName ne 'Parse' ) {
                        next PARSE_NODES;
                    }

                    my $parse_config = {};
                    $parse_config->{'-match_string'}   = $parse_node->getAttribute('MatchString');
                    $parse_config->{'-replace_string'} = $parse_node->getAttribute('ReplaceString');

                    #   add parse to file config
                    push @{ $file_config->{'-parse'} }, $parse_config;
                }

                #   add file to include config
                push @{ $include_config->{'-file'} }, $file_config;
            } ## end FILE_NODES: foreach my $file_node ( $include_node...)

            #   add include to path config
            push @{ $path_config->{'-include'} }, $include_config;
        } ## end INCLUDE_NODES: foreach my $include_node ( ...)

        #   add path config
        push @{$config}, $path_config;
    } ## end PATH_NODES: foreach my $path_node ( $doc...)

    return $config;
} ## end sub get_config

1;

