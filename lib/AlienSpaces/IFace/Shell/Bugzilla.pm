package AlienSpaces::IFace::Shell::Bugzilla;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

use AlienSpaces::Properties {

};

#   util list
#   - can define a list of utilities that you know all
#     sub-classed components are going to need to use
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, UTIL_BUGZILLA(), ];

    return $util_list;
}

sub options_config {
    return 'SLAUP:u:p:n:s';
}

sub options_usage {
    print <<'USAGE';

Actions
=======
-S - Search bugs
-L - Output local store
-A - Add a bug
-U - Update a bug

Parameters
==========
-P - Params (field=value&field=value&field=value)
-u - Bugzilla username
-p - Bugzilla password
-n - Bugzilla hostname
-s - Store search results locally as JSON data in /APP_HOME/data/bugzilla.json

Arguments -u, -p and -h are required for all actions other than -L

USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    #   options
    my $options = $self->options;

    if ( !defined $options->{'S'} && !defined $options->{'A'} && !defined $options->{'U'} && !defined $options->{'L'} ) {
        print 'Missing -S, -A, -U or -L' . "\n";
        $self->options_usage();
        return;
    }

    if ( !defined $options->{'L'} ) {
        if ( !defined $options->{'u'} || !defined $options->{'p'} || !defined $options->{'n'} ) {
            print 'Missing -u, -p or -h' . "\n";
            $self->options_usage();
            return;
        }
    }

    #   local
    if ( $options->{'L'} ) {

        if ( !$self->bugzilla_local_store() ) {
            $self->error( { '-text' => 'Failed to get bugs from local store' } );
            return;
        }
    }

    #   search
    elsif ( $options->{'S'} ) {

        if ( !$self->bugzilla_search() ) {
            $self->error( { '-text' => 'Failed to search for bugs' } );
            return;
        }
    }

    #   add
    elsif ( $options->{'A'} ) {

        if ( !$self->bugzilla_add() ) {
            $self->error( { '-text' => 'Failed to add a bug' } );
            return;
        }

    }

    #   update
    elsif ( $options->{'U'} ) {

        if ( !$self->bugzilla_update() ) {
            $self->error( { '-text' => 'Failed to update bug' } );
            return;
        }
    }

    return;
} ## end sub main

#   bugzilla search
sub bugzilla_search {
    my ( $self, $args ) = @_;

    #   bugzilla util
    my $bugzilla_util = $self->utils->{ UTIL_BUGZILLA() };

    #   options
    my $options = $self->options;

    #   params
    my $params = $self->get_params();

    #   store
    my $store = $self->options->{'s'};

    #   hostname
    my $hostname = $options->{'n'};

    #   login
    $self->bugzilla_login();

    #   bugs
    my $bugs = $bugzilla_util->fetch( { '-params' => $params, '-store' => $store, '-hostname' => $hostname } );

    print $self->dbg_value($bugs);

    return 1;
}

#   bugzilla local store
sub bugzilla_local_store {
    my ( $self, $args ) = @_;

    #   bugzilla util
    my $bugzilla_util = $self->utils->{ UTIL_BUGZILLA() };

    my $bugs = $bugzilla_util->fetch_local();

    print $self->dbg_value($bugs);

    return 1;
}

#   bugzilla login
sub bugzilla_login {
    my ( $self, $args ) = @_;

    #   bugzilla util
    my $bugzilla_util = $self->utils->{ UTIL_BUGZILLA() };

    #   options
    my $options = $self->options;

    #   username
    my $username = $options->{'u'};

    #   password
    my $password = $options->{'p'};

    #   hostname
    my $hostname = $options->{'n'};

    #   login
    $bugzilla_util->login( { '-username' => $username, '-password' => $password, '-hostname' => $hostname } );

    return;
}

#   get params
sub get_params {
    my ( $self, $args ) = @_;

    #   options
    my $options = $self->options;

    #   param pairs
    my @param_pairs = ();
    if ( defined $options->{'P'} ) {
        @param_pairs = split /\&/, $options->{'P'};
    }

    #   params
    my $params = {};
    if ( scalar @param_pairs ) {
        foreach my $param_pair (@param_pairs) {
            my ( $param, $value ) = split /\=/, $param_pair;

            if ( defined $param && defined $value ) {
                $params->{$param} = $value;
            }
        }
    }

    return $params;
}

1;

