package AlienSpaces::IFace::Shell::Notification;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(carp croak);

our $VERSION = '$Revision: 179 $';

use base qw(AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_NOTIFICATION(),
    ];
}

#   options
sub options_config {
    return 'QS';
}

sub options_usage {
    print <<'USAGE';
Actions
-------
Q   - Query notifications that would be sent
S   - Send notifications

Options
-------
None

Help
-------    
-h - show help
USAGE

    return;
}

#   main
sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;

    if ( $options->{'Q'} ) {

        #   query
        if ( !$self->query_notifications() ) {
            $self->error( { '-text' => 'Querying notifications failed' } );
            return;
        }
    }
    elsif ( $options->{'S'} ) {

        #   query
        if ( !$self->send_notifications() ) {
            $self->error( { '-text' => 'Sending notifications failed' } );
            return;
        }
    }
    else {
        $self->options_usage();
    }

    return 1;
}

sub query_notifications {
    my ( $self, $args ) = @_;

    my $notification_util = $self->utils->{ UTIL_NOTIFICATION() };

    my $notifications = $notification_util->fetch_notifications();

    $self->info( { '-text' => 'Have -notifications ' . $self->dbg_value($notifications) } );

    return 1;
}

sub send_notifications {
    my ( $self, $args ) = @_;

    my $notification_util = $self->utils->{ UTIL_NOTIFICATION() };

    my $notifications = $notification_util->send_notifications();

    $self->info( { '-text' => 'Have -notifications ' . $self->dbg_value($notifications) } );

    return 1;
}

1;

