package AlienSpaces::IFace::Shell::Builder::Remote;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use JSON;

use File::Path qw(make_path);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Builder);

use AlienSpaces::Constants qw(:util);

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

    ];

    return $util_list;
}

sub main {
    my ( $self, $args ) = @_;

    #   remote config
    my $remote_config = $self->get_remote_config();
    if ( !defined $remote_config ) {
        $self->debug( { '-text' => 'No remote config' } );
        return;
    }

    foreach my $config ( @{$remote_config} ) {

        #   update repository
        if ( !$self->update_repository( { '-config' => $config } ) ) {
            $self->error( { '-text' => 'Failed to update repository' } );
            return;
        }

        #   set options
        if ( !$self->set_options( { '-config' => $config } ) ) {
            $self->error( { '-text' => 'Failed to set options' } );
            return;
        }
    }

    #   clear remote config
    if ( !$self->clear_remote_config() ) {
        $self->error( { '-text' => 'Failed to clear remote config' } );
        return;
    }

    #   restart apache
    if ( !$self->restart_apache() ) {
        $self->error( { '-text' => 'Failed to restart apache' } );
        return;
    }

    return $self->SUPER::main($args);
} ## end sub main

#   update repository
sub update_repository {
    my ( $self, $args ) = @_;

    #   config
    my $config = $args->{'-config'};

    #   project
    my $project = $config->{'-project'};
    if ( !defined $project ) {
        $self->error( { '-text' => 'Missing -project configuration' } );
        return;
    }

    #   revision
    my $revision = $config->{'-revision'};

    #   path
    my $path = '/project/' . $project . '/dev-repo';

    if ( !chdir $path ) {
        $self->error( { '-text' => 'Failed to change directory to ' . $path } );
        return;
    }

    #   command
    my $command = 'svn update';

    if ( defined $revision ) {
        $command .= ' -r ' . $revision;
    }

    if ( !$self->system_command( { '-command' => $command } ) ) {
        $self->error( { '-text' => 'Failed to update repository -command ' . $command } );
        return;
    }

    return 1;
} ## end sub update_repository

#   set options
sub set_options {
    my ( $self, $args ) = @_;

    #   config
    my $config = $args->{'-config'};

    #   project
    my $project = $config->{'-project'};
    if ( !defined $project ) {
        $self->error( { '-text' => 'Missing -project configuration' } );
        return;
    }

    #   target
    my $target = $config->{'-target'};
    if ( !defined $target ) {
        $self->error( { '-text' => 'Missing -target configuration' } );
        return;
    }

    #   build config
    my $build_config = '/project/' . $project . '/dev-repo/conf/build/' . $target . '-build.xml';

    #   options
    my $options = $self->options();

    $options->{'c'} = $build_config;

    return 1;
} ## end sub set_options

#   restart apache
sub restart_apache {
    my ( $self, $args ) = @_;

    #   command
    my $command;

    foreach my $apache_p ( '/usr/sbin/apache2ctl', '/usr/sbin/apachectl' ) {
        if ( -e $apache_p ) {
            $command = $apache_p . ' restart';
        }
    }

    if ( !defined $command ) {
        $self->error( { '-text' => 'Failed to find apache command' } );
        return;
    }

    if ( !$self->system_command( { '-command' => $command } ) ) {
        $self->error( { '-text' => 'Failed to restart apache -command ' . $command } );
        return;
    }

    return 1;
}

#   clear remote config
sub clear_remote_config {
    my ( $self, $args ) = @_;

    #   application home
    my $application_home = $self->environment->{'APP_HOME'};

    #   build remote
    my $build_remote = $application_home . '/conf/build/remote.conf';

    my $fh;
    if ( !open $fh, '>', $build_remote ) {
        $self->error( { '-text' => 'Failed to open ' . $build_remote . ' :' . $OS_ERROR } );
        return;
    }

    if ( !close $fh ) {
        $self->error( { '-text' => 'Failed to close ' . $build_remote . ' :' . $OS_ERROR } );
        return;
    }

    return 1;
}

#   get remote config
sub get_remote_config {
    my ( $self, $args ) = @_;

    #   application home
    my $application_home = $self->environment->{'APP_HOME'};

    #   build remote
    my $build_remote = $application_home . '/conf/build/remote.conf';

    my $fh;
    if ( !open $fh, '<', $build_remote ) {
        $self->error( { '-text' => 'Failed to open ' . $build_remote . ' :' . $OS_ERROR } );
        return;
    }

    my @rules = <$fh>;

    if ( !close $fh ) {
        $self->error( { '-text' => 'Failed to close ' . $build_remote . ' :' . $OS_ERROR } );
        return;
    }

    #   remote config
    my $remote_config = ();

FIND_RULE:
    foreach my $rule (@rules) {

        chomp($rule);
        if ( !length $rule ) {
            next FIND_RULE;
        }

        my $rule = from_json($rule);
        if ( !defined $rule ) {
            next FIND_RULE;
        }

        push @{$remote_config}, $rule;
    }

    return $remote_config;
} ## end sub get_remote_config

1;

