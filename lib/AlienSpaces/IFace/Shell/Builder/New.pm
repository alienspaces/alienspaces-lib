package AlienSpaces::IFace::Shell::Builder::New;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use File::Path qw(make_path);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Builder);

use AlienSpaces::Constants qw(:util);

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

    ];

    return $util_list;
}

sub options_config {
    return 'c:p:n:';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-c - template configuration file

-p - project name           (example: myproject)
-n - project perl namespace (example: MyProject)

USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;
    if ( !defined $options ) {
        $self->error( { '-text' => 'No options provided on command line' } );
        return;
    }

    my $option = $options->{'p'};
    if ( !defined $option ) {
        $self->error( { '-text' => '-p project name argument required' } );
        $self->options_usage();
        return;
    }
    $option = $options->{'n'};
    if ( !defined $option ) {
        $self->error( { '-text' => '-n perl namespace argument required' } );
        $self->options_usage();
        return;
    }

    return $self->SUPER::main($args);
}

#   get config
#   - override base Builder config method to perform
#     interpolation of command line argument values into XML attributes
sub get_config {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-file'], $args ) ) {
        return;
    }

    my $config = $self->SUPER::get_config($args);
    if ( !defined $config ) {
        $self->error( { '-text' => 'Failed to load config -file ' . $args->{'-file'} } );
        return;
    }

    #   cycle through all nodes / attributes replacing tokens
    $self->replace_attributes( { '-ref' => $config } );

    return $config;
}

my $token_option_map = {
    'APP_PERL_NAMESPACE' => 'n',
    'APP_NAME'           => 'p',
};

sub replace_attributes {
    my ( $self, $args ) = @_;

    #   ref
    my $ref = $args->{'-ref'};
    if ( !defined $ref ) {
        $self->error( { '-text' => 'Missing argument -ref, cannot replace attributes' } );
        return;
    }

    if ( ref($ref) eq 'HASH' ) {

        while ( my ( $k, $v ) = each %{$ref} ) {

            if ( ( ref($v) eq 'HASH' ) || ( ref($v) eq 'ARRAY' ) ) {

                #   recurse
                $self->replace_attributes( { '-ref' => $ref->{$k} } );
            }
            else {

                #   replace
                if ( defined $v ) {
                    while ( my ( $mk, $mv ) = each %{$token_option_map} ) {
                        my $option = $self->options->{$mv};
                        $v =~ s/\{$mk\}/$option/sxmg;
                    }

                    #   assign back
                    $ref->{$k} = $v;
                }

            }
        }

    }
    elsif ( ref($ref) eq 'ARRAY' ) {

        foreach my $v ( @{$ref} ) {
            if ( ( ref($v) eq 'HASH' ) || ( ref($v) eq 'ARRAY' ) ) {

                #   recurse
                $self->replace_attributes( { '-ref' => $v } );
            }
            else {

                #   replace
                if ( defined $v ) {
                    while ( my ( $mk, $mv ) = each %{$token_option_map} ) {
                        my $option = $self->options->{$mv};
                        $v =~ s/\{$mk\}/$option/sxmg;
                    }
                }
            }
        }
    }

    return;
} ## end sub replace_attributes

1;

