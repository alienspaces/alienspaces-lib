package AlienSpaces::IFace::Shell::Builder::DB;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use IO::Prompter;

use File::Path qw(make_path);
use File::Temp qw(tempfile);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Builder);

use AlienSpaces::Constants qw(:util);

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

    ];

    return $util_list;
}

sub options_config {
    return 'IEc:p:';
}

sub options_usage {
    print <<'USAGE';

-I - Import objects
-E - Export objects

-h - show help
-c - configuration file

USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;
    if ( !defined $options ) {
        $self->error( { '-text' => 'No options provided on command line' } );
        $self->options_usage();
        return;
    }

    if ( !defined $options->{'c'} ) {
        $self->error( { '-text' => 'Missing -c option, cannot import / export database objects' } );
        $self->options_usage();
        return;
    }

    if ( $options->{'I'} ) {
        if ( !$self->import_objects() ) {
            $self->error( { '-text' => 'Failed to import database objects' } );
            return;
        }
    }
    elsif ( $options->{'E'} ) {
        if ( !$self->export_objects() ) {
            $self->error( { '-text' => 'Failed to export database objects' } );
            return;
        }
    }
    else {
        $self->error( { '-text' => '(I)mport or (E)xport not specified, dont know what to do' } );
        $self->options_usage();
        return;
    }

    return 1;
} ## end sub main

sub export_objects {
    my ( $self, $args ) = @_;

    my $config = $self->get_config();

    my $file = $config->{'-file'};
    if ( !defined $file ) {
        $self->error( { '-text' => 'Missing -file from config, cannot export objects' } );
        return;
    }

    my $name = $config->{'-name'};
    if ( !defined $name ) {
        $self->error( { '-text' => 'Missing -name from config, cannot export objects' } );
        return;
    }

    #   password
    my $password = prompt '[mysql] password for root:', -echo => '*';

    #   command
    my $command = 'mysqldbexport --server=root:' . $password . '@localhost ' . $name . ' > ./' . $file;

    if ( !$self->system_command( { '-command' => $command } ) ) {
        $self->error( { '-text' => 'Failed to export -name ' . $name . ' -command ' . $command } );
        return;
    }

    return 1;
} ## end sub export_objects

sub import_objects {
    my ( $self, $args ) = @_;

    my $config = $self->get_config();

    print 'Have -config ' . $self->dbg_value($config) . "\n";

    my $file = $config->{'-file'};
    if ( !defined $file ) {
        $self->error( { '-text' => 'Missing -file from config, cannot export objects' } );
        return;
    }

    my $name = $config->{'-name'};
    if ( !defined $name ) {
        $self->error( { '-text' => 'Missing -name from config, cannot export objects' } );
        return;
    }

    #   process
    #   - returns a processed file
    $file = $self->process_schema( { '-config' => $config } );

    #   password
    my $password = prompt '[mysql] password for root:', -echo => '*';

    #   command
    my $command = 'mysqldbimport -vvv --server=root:' . $password . '@localhost ' . $file;

    if ( !$self->system_command( { '-command' => $command } ) ) {
        $self->error( { '-text' => 'Failed to import -name ' . $name . ' -command ' . $command } );
        return;
    }

    return 1;
} ## end sub import_objects

sub process_schema {
    my ( $self, $args ) = @_;

    my $config = $args->{'-config'};

    my $name           = $config->{'-name'};
    my $file           = $config->{'-file'};
    my $replace_schema = $config->{'-replaceschema'};

    #   drop schema
    #   - defaults to 1
    my $drop_schema = $config->{'-dropschema'};
    if ( !defined $drop_schema || $drop_schema != 0 ) {
        $drop_schema = 1;
    }

    #   create schema
    #   - defaults to 1
    my $create_schema = $config->{'-createschema'};
    if ( !defined $create_schema || $create_schema != 0 ) {
        $create_schema = 1;
    }

    my @all_content;
    {
        my $fh;
        open $fh, '<', $file or croak 'Failed to open -file ' . $file . ' for read :' . $OS_ERROR;
        @all_content = <$fh>;
        close $fh or croak 'Failed to close -file ' . $file . ' for read :' . $OS_ERROR;
    };

    my $tmp = File::Temp->new();

    foreach my $content (@all_content) {

        #   drop schema
        #   - remove drop schema clause if we are not dropping the schema
        if ( !$drop_schema ) {
            $content =~ s/DROP\sDATABASE.*?[;]//sxmg;
        }

        #   create schema
        #   - remove create schema clause if we are not creating the schema
        if ( !$create_schema ) {
            $content =~ s/CREATE\sDATABASE.*?[;]//sxmg;
        }

        if ($replace_schema) {
            $content =~ s/CREATE\sTABLE\s([`].*?[`])/DROP TABLE IF EXISTS $1;\nCREATE TABLE $1/sxmg;
            $content =~ s/$replace_schema/$name/sxmg;

            if ( $content =~ /CREATE.*?VIEW.*?([`].*?[`][.][`].*?[`])/sxmg ) {
                my $view_name = $1;
                $content =~
                    s/(CREATE.*?VIEW.*?[`].*?[`][.][`].*?[`])/DROP TABLE IF EXISTS $view_name;\nDROP VIEW IF EXISTS $view_name;\n$1/sxmg;
            }
        }

        print {$tmp} $content;
    }

    print "Filename is $tmp\n";

    return $tmp;
} ## end sub process_schema

sub get_config {
    my ( $self, $args ) = @_;

    my $file = $self->options->{'c'};
    if ( !defined $file ) {
        $self->error( { '-text' => 'Missing -file, cannot get_config' } );
        return;
    }

    #   xml libxml util
    my $xml_libxml_util = $self->utils->{ UTIL_XML_LIBXML() };

    $self->debug( { '-text' => 'Using configuration ' . $file } );

    #   parsed
    my $parsed = $xml_libxml_util->parse( { '-file' => $file } );
    if ( !defined $parsed || !defined $parsed->{'-elements'} || !scalar @{ $parsed->{'-elements'} } ) {
        $self->error( { '-text' => 'Problem parsing configuration file, got -parsed ' . $self->dbg_value($parsed) } );
        return;
    }

    my $config;
    foreach my $element ( @{ $parsed->{'-elements'} } ) {
        if ( $element->{'-name'} ne 'Database' ) {
            next;
        }

        $config = $element->{'-attributes'};
        last;
    }

    $self->debug( { '-text' => 'Returning -config ' . $self->dbg_value($config) } );

    return $config;
} ## end sub get_config

1;

__END__

