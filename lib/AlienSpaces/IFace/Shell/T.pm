package AlienSpaces::IFace::Shell::T;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(carp croak);

our $VERSION = '$Revision: 179 $';

use base qw(AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local

    ];
}

#   options
sub options_config {
    return '';
}

sub options_usage {
    print <<'USAGE';
Actions
-------


Options
-------


Help
-------    
-h - show help
USAGE

    return;
}

#   main
sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;

    if ( $options->{''} ) {

        #   xxx

    }
    else {
        $self->options_usage();
    }

    return 1;
}

1;
