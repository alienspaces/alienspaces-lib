package AlienSpaces::IFace::Shell::Email;

#
#   $Revision: 179 $
#   $Author: stbaldwin $
#   $Date: 2010-08-17 19:42:44 +1000 (Tue, 17 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(carp croak);

our $VERSION = '$Revision: 179 $';

use base qw(AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

sub util_list {
    my $self = $_[0];
    return [ @{ $self->SUPER::util_list }, UTIL_EMAIL(), ];
}

sub options_config {

    #   -h - show help
    #   -m - mail transport agent host
    #   -p - purge emails
    #   -d - delete sent emails
    #   -f - fetch unsent email
    #   -t - test send email
    #   -s - send unsent email

    return 'pdft:s';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-p - purge sent emails
-d - delete sent emails
-f - fetch unsent email
-t - test send email
-s - send unsent email
USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;

    if ( $options->{'f'} ) {
        $self->fetch_unsent_emails();
    }
    elsif ( $options->{'p'} ) {
        $self->purge_sent_emails();
    }
    elsif ( $options->{'d'} ) {
        $self->delete_sent_emails();
    }
    elsif ( $options->{'s'} ) {
        $self->send_unsent_emails();
    }
    elsif ( $options->{'t'} ) {
        $self->send_test_email();
    }
    else {
        $self->options_usage();
    }

    return 1;
}

sub send_test_email {
    my ( $self, $args ) = @_;

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    $self->debug( { '-text' => 'Sending test email' } );

    #   options
    my $options = $self->options;

    #   test address
    my $address = $options->{'t'};

    if ( !defined $address ) {
        return $self->fatal( { '-text' => 'Missing test email address' } );
    }

    my $email = $email_util->create_email(
        {   '-sender'    => 'noreply@alienspaces.net',
            '-reply_to'  => 'noreply@alienspaces.net',
            '-subject'   => 'AlienSpaces Email Test',
            '-body'      => 'AlienSpaces Email Test',
            '-recipient' => {
                '-address' => $address,
                '-type'    => 'to',
            },
        } );

    $self->info( { '-text' => 'Have test -email ' . $self->dbg_value($email) } );

    $email_util->send_email( { '-email_id' => $email->{'-email_id'}, } );

    return;
} ## end sub send_test_email

sub fetch_unsent_emails {
    my ( $self, $args ) = @_;

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    $self->debug( { '-text' => 'Fetching unsent emails' } );

    my $unsent_email_list = $email_util->fetch_unsent_emails();

    foreach my $email_id ( @{$unsent_email_list} ) {
        my $email = $email_util->fetch_email( { '-email_id' => $email_id } );
        $self->info( { '-text' => 'Have unsent -email ' . $self->dbg_value($email) } );
    }

    return;
}

sub send_unsent_emails {
    my ( $self, $args ) = @_;

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    $self->debug( { '-text' => 'Sending unsent emails' } );

    #   options
    my $options = $self->options;

    my $unsent_email_list = $email_util->fetch_unsent_emails();

    foreach my $email_id ( @{$unsent_email_list} ) {
        $self->info( { '-text' => 'Sending -email_id ' . $email_id } );
        $email_util->send_email( { '-email_id' => $email_id, } );
    }

    return;
}

sub purge_sent_emails {
    my ( $self, $args ) = @_;

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    $self->info( { '-text' => 'Purging sent emails' } );

    my $purge_list = $email_util->fetch_purgeable_emails();

    foreach my $email_id ( @{$purge_list} ) {
        $self->info( { '-text' => 'Purging email_id ' . $email_id } );
        $email_util->purge_email( { '-email_id' => $email_id } );
    }

    return 1;
}

sub delete_sent_emails {
    my ( $self, $args ) = @_;

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    $self->info( { '-text' => 'Deleting sent emails' } );

    my $delete_list = $email_util->fetch_sent_emails();

    foreach my $email_id ( @{$delete_list} ) {
        $self->info( { '-text' => 'Deleting email_id ' . $email_id } );
        $email_util->delete_email( { '-email_id' => $email_id } );
    }

    return 1;
}

1;
