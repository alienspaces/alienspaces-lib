package AlienSpaces::IFace::Shell::User;

#
#   $Revision: 179 $
#   $Author: stbaldwin $
#   $Date: 2010-08-17 19:42:44 +1000 (Tue, 17 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(carp croak);

our $VERSION = '$Revision: 179 $';

use base qw(AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

sub util_list {
    my $self = $_[0];
    return [ @{ $self->SUPER::util_list }, UTIL_USER(), ];
}

sub options_config {

    #   -h - show help
    #   -Q - query users
    #   -A - add user
    #   -D - delete user
    #   -P - change password
    #   -E - change email
    #   -I - change handle
    #   -S - change status
    #   -T - change type
    #
    #   -u - user_id
    #   -p - password
    #   -e - email
    #   -i - handle
    #   -s - status
    #   -t - type

    return 'QADPEISTp:e:u:i:s:t:';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-Q - query users
-A - add user
-D - delete user record
-P - change user password
-E - change user email
-I - change user handle
-S - change user status
-T - change user type

-p - password
-e - email
-u - user id
-i - handle
-s - status [inactive, active, cancelled, banned, pending_activation, deleted, locked]
-t - type [basic, business, business_user_admin, business_inv_admin]

USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;

    if ( $options->{'Q'} ) {
        $self->query_users();
    }
    elsif ( $options->{'A'} ) {
        $self->insert_user();
    }
    elsif ( $options->{'D'} ) {
        $self->delete_user();
    }
    elsif ( $options->{'P'} ) {
        $self->change_user_password();
    }
    elsif ( $options->{'E'} ) {
        $self->change_user_email();
    }
    elsif ( $options->{'I'} ) {
        $self->change_user_handle();
    }
    elsif ( $options->{'S'} ) {
        $self->change_user_status();
    }
    elsif ( $options->{'T'} ) {
        $self->change_user_type();
    }
    else {
        $self->options_usage();
    }

    return 1;
} ## end sub main

sub query_users {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    $self->debug( { '-text' => 'Querying users' } );

    #   options
    my $options = $self->options;

    my $users = $user_util->fetch_users();

    $self->debug( { '-text' => 'Have -users - ' . $self->dbg_value($users) } );

    print 'Have -users - ' . $self->dbg_value($users) . "\n";

    return 1;
}

sub insert_user {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    $self->debug( { '-text' => 'Adding user' } );

    #   options
    my $options = $self->options;

    #   handle
    my $handle = $options->{'i'};

    #   email
    my $email_address = $options->{'e'};

    #   password
    my $password = $options->{'p'};

    #   add user
    my $user_id = $user_util->insert_user( { '-handle' => $handle, '-email_address' => $email_address, '-password' => $password } );
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Failed to add user' } );
        return;
    }

    $self->debug( { '-text' => 'Added new -user_id ' . $user_id } );

    print 'Added new -user_id ' . $user_id . "\n";

    $self->query_users();

    $self->dbh->commit();

    return 1;
} ## end sub insert_user

sub delete_user {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    $self->debug( { '-text' => 'Adding user' } );

    #   options
    my $options = $self->options;

    #   user id
    my $user_id = $options->{'u'};
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing -user_id, cannot delete user' } );
        return;
    }

    if ( !$user_util->delete_user( { '-user_id' => $user_id } ) ) {
        $self->error( { '-text' => 'Failed to delte user -user_id ' . $user_id } );
        return;
    }

    print 'Deleted user -user_id ' . $user_id . "\n";

    return 1;
}

my $user_status_map = {
    'inactive'           => 0,
    'active'             => 1,
    'cancelled'          => 2,
    'banned'             => 3,
    'pending_activation' => 4,
    'deleted'            => 5,
    'locked'             => 6,
};

sub change_user_status {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    $self->debug( { '-text' => 'Changing user status' } );

    #   options
    my $options = $self->options;

    #   user id
    my $user_id = $options->{'u'};
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing -user_id, cannot change user status' } );
        return;
    }

    #   status
    my $status = $options->{'s'};
    if ( !defined $status ) {
        $self->error( { '-text' => 'Missing -status, cannot change user status' } );
        return;
    }

    #   user status id
    my $user_status_id = $user_status_map->{$status};
    if ( !defined $user_status_id ) {
        $self->error( { '-text' => 'Unknown user -status ' . $status . ', cannot change user status' } );
        return;
    }

    #   change status
    if ( !$user_util->update_user( { '-user_id' => $user_id, '-user_status_id' => $user_status_id } ) ) {
        $self->error( { '-text' => 'Failed to update user status' } );
        return;
    }

    return 1;
} ## end sub change_user_status

my $user_type_map = {
    'basic'               => 1,
    'business'            => 2,
    'business_inv_admin'  => 3,
    'business_user_admin' => 4,
};

sub change_user_type {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    $self->debug( { '-text' => 'Changing user type' } );

    #   options
    my $options = $self->options;

    #   user id
    my $user_id = $options->{'u'};
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing -user_id, cannot change user type' } );
        return;
    }

    #   type
    my $type = $options->{'t'};
    if ( !defined $type ) {
        $self->error( { '-text' => 'Missing -type, cannot change user type' } );
        return;
    }

    #   user type id
    my $user_type_id = $user_type_map->{$type};
    if ( !defined $user_type_id ) {
        $self->error( { '-text' => 'Unknown user -type ' . $type . ', cannot change user type' } );
        return;
    }

    #   change type
    if ( !$user_util->update_user( { '-user_id' => $user_id, '-user_type_id' => $user_type_id } ) ) {
        $self->error( { '-text' => 'Failed to update user type' } );
        return;
    }

    return 1;
} ## end sub change_user_type

sub change_user_password {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   options
    my $options = $self->options;

    #   user id
    my $user_id = $options->{'u'};
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing -user_id, cannot change user password' } );
        return;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $user_id . ', cannot change user password' } );
        return;
    }

    #   password
    my $password = $options->{'p'};
    if ( !defined $password ) {
        $self->error( { '-text' => 'Missing password option, cannot change users password' } );
        return;
    }

    #   email address
    my $email_address = $user->{'-email_address'};

    $self->debug( { '-text' => 'Changeing -user_id ' . $user_id . ' password only' } );

    #   change password
    $user_util->update_user(
        {   '-user_id'       => $user_id,
            '-password'      => $password,
            '-email_address' => $email_address,
        } );

    return;
} ## end sub change_user_password

sub change_user_handle {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   options
    my $options = $self->options;

    #   user id
    my $user_id = $options->{'u'};
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing -user_id, cannot change user handle' } );
        return;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $user_id . ', cannot change user password' } );
        return;
    }

    #   handle
    my $handle = $options->{'i'};
    if ( !defined $handle ) {
        $self->error( { '-text' => 'Missing handle option, cannot change users handle' } );
        return;
    }

    $self->debug( { '-text' => 'Changeing -user_id ' . $user_id . ' handle only' } );

    #   change password
    $user_util->update_user(
        {   '-user_id' => $user_id,
            '-handle'  => $handle,
        } );

    return;
} ## end sub change_user_handle

sub change_user_email {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   options
    my $options = $self->options;

    #   user id
    my $user_id = $options->{'u'};
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing -user_id, cannot change user email' } );
        return;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $user_id . ', cannot change user email' } );
        return;
    }

    #   password
    my $password = $options->{'p'};
    if ( !defined $password ) {
        $self->error( { '-text' => 'Missing password option, cannot change users email' } );
        return;
    }

    #   email address
    my $email_address = $options->{'e'};
    if ( !defined $email_address ) {
        $self->error( { '-text' => 'Missing email option, cannot change users email' } );
        return;
    }

    $self->debug( { '-text' => 'Changeing -user_id ' . $user_id . ' email / password' } );

    #   change password
    $user_util->update_user(
        {   '-user_id'       => $user_id,
            '-password'      => $password,
            '-email_address' => $email_address,
        } );

    return;
} ## end sub change_user_email

1;
