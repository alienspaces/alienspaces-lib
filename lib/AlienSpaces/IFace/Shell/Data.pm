package AlienSpaces::IFace::Shell::Data;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use File::Path qw(make_path);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

#   private constants
use constant { 

};

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_XML_LIBXML() ];

    return $util_list;
}

sub options_config {
    return 'c:';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-c - build configuration file
USAGE

    return;
}

#   get auto increment
sub get_auto_increment {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-table_name'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   table name
    my $table_name = $args->{'-table_name'};

    my $current_value;

    my $statement = <<"STMT";
SELECT `auto_increment`
FROM   information_schema.tables
WHERE  table_schema = DATABASE()
AND    table_name   = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->bind_param(1, $table_name);
        $sth->execute();
        ($current_value) = $sth->fetchrow_array();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to get auto increment -table ' . $table_name . ' :' . $EVAL_ERROR } );
        return;
    }

    return $current_value;
} ## end sub get_auto_increment

#   set auto increment
sub set_auto_increment {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-table_name', '-value' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $table_name = $args->{'-table_name'};
    my $value      = $args->{'-value'};

    my $current_value = $self->get_auto_increment( { '-table_name' => $table_name } );
    if ( !defined $current_value ) {
        $self->error( { '-text' => 'Current auto increment value undefined for -table_name ' . $table_name } );
        return;
    }

    if ( $current_value >= $value ) {
        $self->debug(
            {   '-text' => 'Current auto increment -value ' .
                    $current_value . ' for -table_name ' . $table_name . ' is >= than reset -value ' . $value . ', wont reset'
            } );
        return 1;
    }

    my $statement = <<"STMT";
ALTER TABLE `$table_name` AUTO_INCREMENT=$value;
STMT

    my $success = eval {
        $self->dbh->do($statement);
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to set auto increment -table ' . $table_name . ' :' . $EVAL_ERROR } );
        return;
    }

    return 1;
}

1;

__END__
