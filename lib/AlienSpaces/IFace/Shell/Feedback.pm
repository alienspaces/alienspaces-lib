package AlienSpaces::IFace::Shell::Feedback;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #        @{ $self->SUPER::util_list },
    ];

    return $util_list;
}

sub options_config {

    #   e - email feedback to this address
    
    return 'e:';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-e - email to this address
USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Running' } );

    my $email_address      = $self->options->{'e'};

    return;
} ## end sub main

1;

