package AlienSpaces::IFace::Shell::Session;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(carp croak);

our $VERSION = '$Revision: 179 $';

use base qw(AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

sub util_list {
    my ( $self, $args ) = @_;
    return [

        #   super utils
        @{ $self->SUPER::util_list },

        #   local utils
        UTIL_SESSION(),
    ];
}

#   options
sub options_config {
    return 'Da';
}

sub options_usage {
    print <<'USAGE';

Actions
-------
D   -   delete expired sessions (> 2 days old)

Options
-------
a   -   all sessions, don't care how old, what colour, what flavour, all of them!

Help
-------    
-h - show help
USAGE

    return;
}

#   main
sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;

    if ( $options->{'D'} ) {
        if ( $options->{'a'} ) {
            if ( !$self->delete_all_sessions() ) {
                $self->error( { '-text' => 'Failed deleting all sessions' } );
                return;
            }
        }
        else {
            if ( !$self->delete_expired_sessions() ) {
                $self->error( { '-text' => 'Failed deleting expired sessions' } );
                return;
            }
        }

    }
    else {
        $self->options_usage();
    }

    return 1;
}

#   delete expired sessions
sub delete_expired_sessions {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    return $session_util->delete_expired_session_records($args);
}

#   delete all sessions
sub delete_all_sessions {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    return $session_util->delete_all_session_records($args);
}

1;
