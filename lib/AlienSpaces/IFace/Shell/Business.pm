package AlienSpaces::IFace::Shell::Business;

#
#   $Revision: 179 $
#   $Author: stbaldwin $
#   $Date: 2010-08-17 19:42:44 +1000 (Tue, 17 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(carp croak);

our $VERSION = (qw$Revision: 179 $)[1];

use base qw(AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

sub util_list {
    my $self = $_[0];
    return [ 
        
        #   super
        @{ $self->SUPER::util_list }, 
        
        #   local
        UTIL_BUSINESS(), 
    ];
}

sub options_config {

    #   -h - show help
    #   -Q - query business
    #   -A - add business
    #   -D - delete business
    #   -N - change name
    #   -S - change status
    #
    #   -b - business_id
    #   -s - status

    return 'QADNSb:n:s:';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-Q - query business
-A - add business
-D - delete business record
-N - change business name
-S - change business status

-b - business id
-n - name
-s - status [inactive, active, deleted]

USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    my $options = $self->options;

    if ( $options->{'Q'} ) {
        $self->query_business();
    }
    elsif ( $options->{'A'} ) {
        $self->insert_business();
    }
    elsif ( $options->{'D'} ) {
        $self->delete_business();
    }
    elsif ( $options->{'S'} ) {
        $self->change_business_status();
    }
    elsif ( $options->{'N'} ) {
        $self->change_business_name();
    }
    else {
        $self->options_usage();
    }

    return 1;
}

sub query_business {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    $self->debug( { '-text' => 'Querying business' } );

    #   options
    my $options = $self->options;

    my $business = $business_util->fetch_business();

    $self->debug( { '-text' => 'Have -business - ' . $self->dbg_value($business) } );

    print 'Have -business - ' . $self->dbg_value($business) . "\n";

    return;
}

sub insert_business {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    $self->debug( { '-text' => 'Adding business' } );

    #   options
    my $options = $self->options;

    #   name
    my $name = $options->{'n'};

    #   add business
    my $business_id = $business_util->insert_business( { '-name' => $name, } );
    if ( !defined $business_id ) {
        $self->error( { '-text' => 'Failed to add business' } );
        return;
    }

    $self->debug( { '-text' => 'Added new -business_id ' . $business_id } );

    print 'Added new -business_id ' . $business_id . "\n";

    $self->query_business();

    $self->dbh->commit();

    return 1;
} ## end sub insert_business

sub delete_business {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    $self->debug( { '-text' => 'Adding business' } );

    #   options
    my $options = $self->options;

    #   business id
    my $business_id = $options->{'b'};
    if ( !defined $business_id ) {
        $self->error( { '-text' => 'Missing -business_id, cannot delete business' } );
        return;
    }

    if ( !$business_util->delete_business( { '-business_id' => $business_id } ) ) {
        $self->error( { '-text' => 'Failed to delte business -business_id ' . $business_id } );
        return;
    }

    print 'Deleted business -business_id ' . $business_id . "\n";

    return 1;
}

my $business_status_map = {
    'inactive' => 0,
    'active'   => 1,
    'deleted'  => 2,
};

sub change_business_status {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    $self->debug( { '-text' => 'Adding business' } );

    #   options
    my $options = $self->options;

    #   business id
    my $business_id = $options->{'b'};
    if ( !defined $business_id ) {
        $self->error( { '-text' => 'Missing -business_id, cannot change business status' } );
        return;
    }

    #   status
    my $status = $options->{'s'};

    #   business status id
    my $business_status_id = $business_status_map->{$status};
    if ( !defined $business_status_id ) {
        $self->error( { '-text' => 'Unknown business -status ' . $status . ', cannot change business status' } );
        return;
    }

    #   change status
    if ( !$business_util->update_business( { '-business_id' => $business_id, '-business_status_id' => $business_status_id } ) ) {
        $self->error( { '-text' => 'Failed to update business status' } );
        return;
    }

    return 1;
} ## end sub change_business_status

sub change_business_name {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    $self->debug( { '-text' => 'Adding business' } );

    #   options
    my $options = $self->options;

    #   business id
    my $business_id = $options->{'b'};
    if ( !defined $business_id ) {
        $self->error( { '-text' => 'Missing -business_id, cannot change business name' } );
        return;
    }

    #   name
    my $name = $options->{'n'};

    #   change name
    if ( !$business_util->update_business( { '-business_id' => $business_id, '-name' => $name } ) ) {
        $self->error( { '-text' => 'Failed to update business name' } );
        return;
    }

    return 1;
} ## end sub change_business_name

1;

__END__
