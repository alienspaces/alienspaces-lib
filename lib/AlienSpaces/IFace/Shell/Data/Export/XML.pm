package AlienSpaces::IFace::Shell::Data::Export::XML;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Data::Export);

use AlienSpaces::Properties {};

use AlienSpaces::Constants qw(:util);

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ 
        
        #   super
        @{ $self->SUPER::util_list }, 
        
        #   local
        UTIL_XML_LIBXML(), 
        UTIL_DB_TABLE(), 
    ];

    return $util_list;
}

#   options config
sub options_config {
    return 'c:f:';
}

sub options_usage {
    print <<'USAGE';
    -h - show help
    -c [/config/file.xml] xml config file
    -f [/data/xml/file.xml]   xml data file
USAGE

    return;
}

