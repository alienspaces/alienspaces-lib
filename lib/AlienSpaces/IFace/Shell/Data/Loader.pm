package AlienSpaces::IFace::Shell::Data::Loader;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use Data::Dumper;

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Data);

use AlienSpaces::Properties {};

use constant { NOT_FOUND => '-999999', };

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

#   start transaction
sub start_transaction {
    my ( $self, $args ) = @_;

    #   foreign key checks
    if ( $self->disable_foreign_key_checks ) {
        my $success = eval {
            $self->dbh->do('SET FOREIGN_KEY_CHECKS=0');
            1;
        };
        if ( !$success ) {
            $self->fatal( { '-text' => 'Failed to set foreign key checks off :' . $EVAL_ERROR } );
            return;
        }
    }

    #   auto commit
    $self->dbh->{'AutoCommit'} = 0;
    if ( $self->dbh->{'AutoCommit'} ) {
        $self->fatal( { '-text' => 'Failed to turn auto commit off' } );
        return;
    }

    $self->debug( { '-text' => 'Transaction started' } );

    return 1;
}

#   commit
sub commit {
    my ( $self, $args ) = @_;

    #   foreign key checks
    if ( $self->disable_foreign_key_checks ) {
        my $success = eval {
            $self->dbh->do('SET FOREIGN_KEY_CHECKS=1');
            1;
        };
        if ( !$success ) {
            $self->fatal( { '-text' => 'Failed to set foreighn key checks on' } );
            return;
        }
    }

    #   commit
    $self->dbh->commit();

    #   auto commit
    $self->dbh->{'AutoCommit'} = 1;
    if ( !$self->dbh->{'AutoCommit'} ) {
        $self->fatal( { '-text' => 'Failed to turn auto commit on' } );
        return;
    }

    $self->debug( { '-text' => 'Transaction comitted' } );

    return 1;
}

#   rollback
sub rollback {
    my ( $self, $args ) = @_;

    my ( $package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash ) = caller(1);

    $self->debug( { '-text' => 'Called by ' . $subroutine } );

    #   foreign key checks
    if ( $self->disable_foreign_key_checks ) {
        my $success = eval {
            $self->dbh->do('SET FOREIGN_KEY_CHECKS=1');
            1;
        };
        if ( !$success ) {
            $self->fatal( { '-text' => 'Failed to set foreighn key checks on' } );
            return;
        }
    }

    #   rollback
    $self->dbh->rollback();

    #   auto commit
    $self->dbh->{'AutoCommit'} = 1;
    if ( !$self->dbh->{'AutoCommit'} ) {
        $self->fatal( { '-text' => 'Failed to turn auto commit on' } );
        return;
    }

    $self->debug( { '-text' => 'Transaction rolled back' } );

    return 1;
} ## end sub rollback

#   util  methods
sub check_defined {
    my ( $self, $args ) = @_;

    if ( !defined $args || !scalar @{$args} ) {
        return;
    }
    foreach my $arg ( @{$args} ) {
        if ( !defined $arg ) {
            $self->debug( { '-text' => 'Found undefined arguments' } );
            return;
        }
    }

    return 1;
}

#
#   configuration
#
#   further available configuration in file type specific sub class
#
sub reset_auto_increment {
    return 1;
}

sub truncate_data {
    return 1;
}

sub truncate_data_where_clause {
    return;
}

sub disable_foreign_key_checks {
    return 1;
}

sub file_field_trim_spaces {
    return 1;
}

1;

__END__
