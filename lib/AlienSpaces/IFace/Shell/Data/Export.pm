package AlienSpaces::IFace::Shell::Data::Export;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use Data::Dumper;

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Data);

use AlienSpaces::Properties {};

use constant { NOT_FOUND => '-999999', };

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

1;
