package AlienSpaces::IFace::Shell::Data::Loader::ODS;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use Archive::Zip;
use Encode;

use OpenOffice::OODoc;

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Data::Loader);

use AlienSpaces::Properties {
    'file_table_configuration'  => { '-type' => 's' },
    'file_column_configuration' => { '-type' => 's' },
    'file_column_required'      => { '-type' => 's' },
    'file_name'                 => { '-type' => 's' },
};

use AlienSpaces::Constants qw(:util);

#   Data dumper
use Data::Dumper;

use constant {
    MAX_COLUMNS    => 100,        #   maximum columns in a spreadsheet
    MAX_ROWS       => 1000000,    #   maximum records
    MAX_EMPTY_ROWS => 10,         #   maximum blank rows
};

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ 
        
        #   super
        @{ $self->SUPER::util_list }, 
        
        #   local
        UTIL_DATA_LOADER_MAP(), 
        UTIL_XML_LIBXML(), 
    ];

    return $util_list;
}

#   options config
sub options_config {
    return 'c:';
}

sub options_usage {
    print <<'USAGE';
    -h - show help
    -c - configuration XML
USAGE

    return;
}

my $column_alpha_map = {
    0  => 'A',
    1  => 'B',
    2  => 'C',
    3  => 'D',
    4  => 'E',
    5  => 'F',
    6  => 'G',
    7  => 'H',
    8  => 'I',
    9  => 'J',
    10 => 'K',
    11 => 'L',
    12 => 'M',
    13 => 'N',
    14 => 'O',
    15 => 'P',
    16 => 'Q',
    17 => 'R',
    18 => 'S',
    19 => 'T',
    20 => 'U',
    21 => 'V',
    22 => 'W',
    23 => 'X',
    24 => 'Y',
    25 => 'Z',
};

#   load
sub main {
    my ( $self, $args ) = @_;

    #   loader map util
    my $loader_map_util = $self->utils->{ UTIL_DATA_LOADER_MAP() };

    #   load configuration
    $self->load_configuration_xml();

    #   build insert statements
    $self->build_insert_statements();

    #   load file data
    my $file_data = $self->load_file_data();

    if ( !$self->start_transaction() ) {
        $self->debug( { '-text' => 'Failed to start transaction' } );
        return;
    }

    #   check table truncate
    if ( !$self->truncate_table_data() ) {
        $self->debug( { '-text' => 'Failed to truncate table data' } );
        return;
    }

    if ( !$self->reset_table_auto_increment() ) {
        $self->debug( { '-text' => 'Failed to reset auto increment' } );
        return;
    }

    my $file_column_configuration = $self->file_column_configuration();
    my $file_table_configuration  = $self->file_table_configuration();

    my $row_load_data = {};
    my $row_count     = 0;

PROCESS_ROW:
    foreach my $row_data ( @{$file_data} ) {
        my $idx = 0;
    PROCESS_COLUMN:
        foreach my $column_data ( @{$row_data} ) {

            my $column_configuration = $file_column_configuration->[$idx];

            my $table_name       = $column_configuration->{'-table_name'};
            my $column_name      = $column_configuration->{'-column_name'};
            my $value_map_method = $column_configuration->{'-value_map_method'};

            if ( !defined $table_name ) {
                $idx++;
                next PROCESS_COLUMN;
            }

            if ( defined $value_map_method ) {
                my $map_column_data = $loader_map_util->$value_map_method( { '-data' => $column_data } );
                if ( defined $map_column_data && $map_column_data == $self->NOT_FOUND ) {
                    $self->error(
                        {         '-text' => 'Map data not found -file_name '
                                . $self->file_name()
                                . ' -row '
                                . $row_count
                                . ' -column '
                                . $column_alpha_map->{$idx}
                                . ' -value '
                                . ( defined $column_data ? $column_data : 'undef' )
                                . ' -value_map '
                                . $value_map_method
                                . ' -data '
                                . Dumper($row_data) } );
                    $row_count++;
                    next PROCESS_ROW;
                }
                $column_data = $map_column_data;
            }

            if ( !defined $row_load_data->{$table_name} ) {
                $row_load_data->{$table_name} = {};
            }

            $row_load_data->{$table_name}->{$column_name} = $column_data;
            $idx++;
        } ## end PROCESS_COLUMN: foreach my $column_data ( @...)

        $self->debug( { '-text' => 'Have -row_data ' . $self->dbg_value($row_data) . ' -row_load_data ' . $self->dbg_value($row_load_data) } );

        my $table_insert_ids = {};
        foreach my $table_name (
            sort { $file_table_configuration->{$a}->{'-insert_order'} <=> $file_table_configuration->{$b}->{'-insert_order'} }
            keys %{$file_table_configuration}
            ) {

            my $log_values = '';
            foreach my $key ( keys %{ $row_load_data->{$table_name} } ) {
                $log_values .= (
                    defined $row_load_data->{$table_name}->{$key} ?
                        "\n\t $key - " . $row_load_data->{$table_name}->{$key}
                    : 'undef'
                ) . ' ';
            }
            $self->debug( { '-text' => "\n\t" . 'Insert -table ' . $table_name . ' -values ' . $log_values } );

            my $insert_id = $self->insert_data(
                {   '-data'             => $row_load_data->{$table_name},
                    '-table_name'       => $table_name,
                    '-table_insert_ids' => $table_insert_ids,
                } );
            $table_insert_ids->{$table_name} = $insert_id;
        }
        $row_count++;
        $row_load_data = {};

    } ## end PROCESS_ROW: foreach my $row_data ( @{$file_data...})

    $self->commit();

    return;
} ## end sub main

sub insert_data {
    my ( $self, $args ) = @_;

    my $data             = $args->{'-data'};
    my $table_name       = $args->{'-table_name'};
    my $table_insert_ids = $args->{'-table_insert_ids'};

    $self->debug( { '-text' => 'Have -table_insert_ids ' . $self->dbg_value($table_insert_ids) } );

    #   table configuration
    my $table_config = $self->file_table_configuration;

    my $insert_statement = $table_config->{$table_name}->{'-insert_statement'};
    my $insert_columns   = $table_config->{$table_name}->{'-insert_columns'};

    my $success = eval {
        my $sth = $self->dbh->prepare($insert_statement);

        my $bind = 0;
        foreach my $insert_column ( @{$insert_columns} ) {

            $self->debug( { '-text' => 'Sourcing -insert_column ' . $insert_column } );

            my $column_value = $data->{$insert_column};

            if ( !defined $column_value ) {

                #   add parent table primary key column if applicable
                my $parent_table_name = $table_config->{$table_name}->{'-parent_table_name'};
                if ( defined $parent_table_name ) {
                    my $parent_table_primary_key_column = $table_config->{$parent_table_name}->{'-primary_key_column'};
                    if ( $parent_table_primary_key_column eq $insert_column ) {
                        $column_value = $table_insert_ids->{$parent_table_name};
                        $self->debug( { '-text' => 'Binding -insert_column ' . $column_value . ' parent table primary key' } );
                    }
                }
            }
            else {
                $self->debug( { '-text' => 'Binding -insert_column ' . $column_value } );
            }

            $sth->bind_param( ++$bind, $column_value );
        }

        $sth->execute();
        1;
    };

    if ( !$success ) {
        $self->fatal( { '-text' => 'Failed to insert record :' . $EVAL_ERROR } );
    }

    my $return_value;
    if ( exists $data->{ $table_config->{$table_name}->{'-primary_key_column'} } ) {
        $return_value = $data->{ $table_config->{$table_name}->{'-primary_key_column'} };
    }
    else {
        $return_value = $self->dbh->last_insert_id();
    }
    $self->debug( { '-text' => 'Returning -primary_key_value ' . $return_value } );

    return $return_value;
} ## end sub insert_data

sub build_insert_statements {
    my ( $self, $args ) = @_;

    #   table configuration
    my $table_config = $self->file_table_configuration;

    #   column configuration
    my $column_config = $self->file_column_configuration;

    foreach my $table_name ( keys %{$table_config} ) {

        my @table_columns = ();

        #   add parent table primary key column if applicable
        my $parent_table_name = $table_config->{$table_name}->{'-parent_table_name'};
        if ( defined $parent_table_name ) {
            my $parent_table_primary_key_column = $table_config->{$parent_table_name}->{'-primary_key_column'};
            if ( !defined $parent_table_primary_key_column ) {
                $self->fatal( { '-text' => 'Parent -table ' . $parent_table_name . ' defined for -table ' . $table_name . ' but parent table has no primary key column defined..' } );
            }
            push @table_columns, $parent_table_primary_key_column;
        }

        #   add this tables columns
    FIND_TABLE_COLUMNS:
        foreach my $column ( @{$column_config} ) {
            my $column_table_name = $column->{'-table_name'};
            if ( !defined $column_table_name ) {
                next FIND_TABLE_COLUMNS;
            }
            if ( $column_table_name ne $table_name ) {
                next FIND_TABLE_COLUMNS;
            }

            push @table_columns, $column->{'-column_name'};

        }

        my $statement = <<"STMT";
INSERT INTO `$table_name` (
STMT

        foreach my $table_column (@table_columns) {
            $statement .= "`$table_column`,\n";
        }
        chomp $statement;
        chop $statement;

        $statement .= <<"STMT";
)
VALUES  (
STMT

        foreach my $table_column (@table_columns) {
            $statement .= "?,\n";
        }
        chomp $statement;
        chop $statement;

        $statement .= <<"STMT";
)
STMT

        $table_config->{$table_name}->{'-insert_statement'} = $statement;
        $table_config->{$table_name}->{'-insert_columns'}   = \@table_columns;
    } ## end foreach my $table_name ( keys...)

    #   table configuration
    $self->file_table_configuration($table_config);

    return;
} ## end sub build_insert_statements

#   load configuration xml
sub load_configuration_xml {
    my ( $self, $args ) = @_;

    #   xml libxml util
    my $xml_libxml_util = $self->utils->{ UTIL_XML_LIBXML() };

    #   config file
    my $config_file = $self->options->{'c'};

    if ( !defined $config_file ) {
        $self->fatal( { '-text' => 'Missing file configuration' } );
    }

    #   doc
    my $doc = $xml_libxml_util->load( { '-file' => $config_file } );

    #   data file_name
    my $file_name = $doc->getAttribute('Filename');
    $self->file_name($file_name);

    $self->debug( { '-text' => 'Parse config -data_file_name ' . $file_name } );

    my $table_config         = {};
    my $column_config        = [];
    my $file_column_required = [];

    foreach my $node ( $doc->childNodes ) {

        if ( $node->nodeName eq 'Table' ) {
            my $table_name           = $node->getAttribute('TableName');
            my $insert_order         = $node->getAttribute('InsertOrder');
            my $primary_key_column   = $node->getAttribute('PrimaryKeyColumn');
            my $auto_increment_value = $node->getAttribute('AutoIncrementValue');
            my $parent_table_name    = $node->getAttribute('ParentTableName');

            $table_config->{$table_name}                            = {};
            $table_config->{$table_name}->{'-insert_order'}         = $insert_order;
            $table_config->{$table_name}->{'-primary_key_column'}   = $primary_key_column;
            $table_config->{$table_name}->{'-auto_increment_value'} = $auto_increment_value;
            $table_config->{$table_name}->{'-parent_table_name'}    = $parent_table_name;

            foreach my $sub_node ( $node->childNodes ) {
                if ( $sub_node->nodeName eq 'Truncate' ) {
                    my $sql = $sub_node->string_value();
                    $table_config->{$table_name}->{'-truncate_sql'} = $sql;
                }
            }
        }
        elsif ( $node->nodeName eq 'Column' ) {

            my $table_name       = $node->getAttribute('TableName');
            my $column_name      = $node->getAttribute('ColumnName');
            my $required         = $node->getAttribute('Required');
            my $value_map_method = $node->getAttribute('ValueMapMethod');

            push @{$column_config},
                {
                '-table_name'       => $table_name,
                '-column_name'      => $column_name,
                '-value_map_method' => $value_map_method,
                };

            if ($required) {
                push @{$file_column_required}, 1;
            }
            else {
                push @{$file_column_required}, 0;
            }
        }
    } ## end foreach my $node ( $doc->childNodes)

    #   table configuration
    $self->file_table_configuration($table_config);

    #   column configuration
    $self->file_column_configuration($column_config);

    #   required columns
    $self->file_column_required($file_column_required);

    $self->debug(
        { '-text' => 'Loaded -table_config ' . $self->dbg_value($table_config) . ' -column_config ' . $self->dbg_value($column_config) . ' -required ' . $self->dbg_value($file_column_required) } );

    return;
} ## end sub load_configuration_xml

#   ods specific load file method
sub load_file_data {
    my ( $self, $args ) = @_;

    my $file_column_required = $self->file_column_required();

    my $data_file_name = $self->file_name();
    if ( !defined $data_file_name ) {
        $self->fatal( { '-text' => 'Missing -f argument' } );
    }

    if ( !-e $data_file_name ) {
        $self->fatal( { '-text' => 'No such file ' . $data_file_name . "\n" . $self->options_usage } );
    }

    #   parse ods
    my $file_data = $self->parse_ods( { '-file_name' => $data_file_name } );

    #   get first sheet
    my $sheet_data;
    if ( defined $file_data && scalar @{$file_data} ) {
        $sheet_data = $file_data->[0];
    }

    #   no sheet, no luck
    if ( !defined $sheet_data ) {
        $self->debug( { '-text' => 'Failed to fetch sheet data' } );
        return;
    }

    #   rows
    my $row = 0;

    #   temp data
    my @temp_data = ();

PARSE_DATA:
    foreach my $row_data ( @{$sheet_data} ) {

        $self->debug( { '-text' => 'Load -row ' . $row . ' data ' . Dumper($row_data) } );

        my $data_found    = 0;
        my @temp_row_data = ();

    FIND_DATA:
        foreach my $column_data ( @{$row_data} ) {
            if ( defined $column_data && length $column_data ) {
                $data_found = 1;
                if ( $self->file_field_trim_spaces ) {
                    $column_data =~ s/^\s+//sxm;
                    $column_data =~ s/(.*?)\s+$/$1/sxmg;
                }
            }
            push @temp_row_data, $column_data;
        }

        my $idx = 0;
    CHECK_REQUIRED:
        foreach my $required_column ( @{$file_column_required} ) {

            if ( !defined $temp_row_data[$idx] || !length $temp_row_data[$idx] && $required_column ) {
                $data_found = 0;
                $self->debug( { '-text' => 'WARN: Required column ' . $idx . ' missing, skipping row' } );
                last CHECK_REQUIRED;
            }
            $idx++;
        }

        my $add_data = 1;
        if ( $self->file_remove_empty_rows ) {
            if ( !$data_found ) {
                $add_data = 0;
            }
        }

        if ($add_data) {
            push @temp_data, \@temp_row_data;
        }

        #   increment row
        $row++;
    } ## end PARSE_DATA: foreach my $row_data ( @{$sheet_data...})

    my @file_contents = @temp_data;

    return \@file_contents;
} ## end sub load_file_data

sub parse_ods {
    my ( $self, $args ) = @_;

    my $file_name = $args->{'-file_name'};

    my $doc = odfDocument( 'file' => $file_name );

    #   columns in sheet
    my $columns = scalar @{ $self->file_column_configuration };
    $self->debug( { '-text' => 'Parsing -columns ' . $columns } );

    #   data
    my $data = [];

    my $empty_rows = 0;
    my $sheet      = 0;
    my $row        = 2;

PARSE_ROWS:
    while ( $empty_rows < MAX_EMPTY_ROWS ) {

        my $empty_cols = 0;

        #   col
        my $col = 0;

    PARSE_COLS:
        while ( $col < $columns ) {

            #            my $cell_code = $column_alpha_map->{$col} . $row;

            my $cell = $doc->getCell( $sheet, $row, $col );

            my $value = $doc->cellValue($cell);

            my $repeated = 0;
            if ( defined $cell ) {
                $repeated = $cell->getAttribute('table:number-columns-repeated') || 0;

                my %attributes = $cell->getAttributes();
                $self->debug( { '-text' => 'This cell has -attributes ' . $self->dbg_value( \%attributes ) } );
            }

            $self->debug(
                {         '-text' => 'Read -sheet '
                        . $self->dbg_value($sheet)
                        . ' -row '
                        . $self->dbg_value($row)
                        . ' -col '
                        . $self->dbg_value($col)
                        . ' -value '
                        . $self->dbg_value($value)
                        . ' -repeated '
                        . $self->dbg_value($repeated) } );

            #   repeated
            if ( $repeated && $repeated < 10 ) {
                foreach my $repeat ( 1 .. $repeated ) {
                    push @{ $data->[$sheet]->[$row] }, $value;
                    if ( !defined $value || $value eq '' ) {
                        if ( ++$empty_cols > 10 ) {
                            last PARSE_COLS;
                        }
                    }
                }
            }
            else {
                #   set value
                push @{ $data->[$sheet]->[$row] }, $value;
                if ( !defined $value || $value eq '' ) {
                    if ( ++$empty_cols > 10 ) {
                        last PARSE_COLS;
                    }
                }
            }
            $col++;
        } ## end PARSE_COLS: while ( $col < $columns )

        my $data_found = 0;
        foreach my $value ( @{ $data->[$sheet]->[$row] } ) {
            if ( defined $value && $value ne '' ) {
                $data_found = 1;
            }
        }
        if ( !$data_found ) {
            $empty_rows++;
            $self->debug( { '-text' => 'No data found -empty_rows ' . $empty_rows } );
        }
        else {
            $empty_rows = 0;
        }

        $row++;
    } ## end PARSE_ROWS: while ( $empty_rows < MAX_EMPTY_ROWS)

    return $data;
} ## end sub parse_ods

#   truncate
sub truncate_table_data {
    my ( $self, $args ) = @_;

    my $file_table_configuration = $self->file_table_configuration();

TRUNCATE_TABLE:
    foreach my $table_name (
        sort { $file_table_configuration->{$b}->{'-insert_order'} <=> $file_table_configuration->{$a}->{'-insert_order'} }
        keys %{$file_table_configuration}
        ) {

        #   truncate sql
        my $truncate_sql = $file_table_configuration->{$table_name}->{'-truncate_sql'};
        if ( !defined $truncate_sql ) {
            next TRUNCATE_TABLE;
        }

        $self->debug( { '-text' => 'Truncating data from -table ' . $table_name . ' -statement ' . $truncate_sql } );

        my $success = eval {
            my $sth = $self->dbh->prepare($truncate_sql);
            $sth->execute();
            $sth->finish();
            1;
        };
        if ( !$success ) {
            $self->debug( { '-text' => 'Failed to truncate data :' . $EVAL_ERROR } );
            return;
        }
    }

    return 1;
} ## end sub truncate_table_data

#   auto increment
sub reset_table_auto_increment {
    my ( $self, $args ) = @_;

    my $file_table_configuration = $self->file_table_configuration();

AUTO_INCREMENT_TABLE:
    foreach my $table_name ( keys %{$file_table_configuration} ) {

        #   autoincrement value
        my $auto_increment_value = $file_table_configuration->{$table_name}->{'-auto_increment_value'};
        if ( !defined $auto_increment_value ) {
            next AUTO_INCREMENT_TABLE;
        }

        $self->debug( { '-text' => 'Setting auto increment to ' . $auto_increment_value } );

        if ( !$self->set_auto_increment( { '-table_name' => $table_name, '-value' => $auto_increment_value } ) ) {
            $self->error( { '-text' => 'Failed to set auto increment' } );
            return;
        }
    }

    return 1;
}

#
#   configuration
#
sub file_remove_empty_rows {
    return 1;
}

sub file_field_separator {
    return ';';
}

sub file_field_quote_character {
    return '"';
}

1;
