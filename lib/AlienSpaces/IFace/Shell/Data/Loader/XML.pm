package AlienSpaces::IFace::Shell::Data::Loader::XML;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Data::Loader);

use AlienSpaces::Properties {};

use AlienSpaces::Constants qw(:util);

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_XML_LIBXML(),
        UTIL_DB_TABLE(),
        UTIL_DATA_CONFIG_XML(),
    ];

    return $util_list;
}

#   options config
sub options_config {
    return 'c:f:d:t';
}

sub options_usage {
    print <<'USAGE';
    -h - show help
    -c [/config/file.xml] xml config file
    -f [/data/file.xml]   xml data file
    -d [/data/directory]  xml data file directory
    -t                    force truncate
USAGE

    return;
}

#   truncated
my $truncated = {};

#   load
sub main {
    my ( $self, $args ) = @_;

    if ( !defined $self->options->{'f'} && !defined $self->options->{'d'} ) {
        $self->options_usage();
        return;
    }

    my $util_data_config_xml = $self->utils->{ UTIL_DATA_CONFIG_XML() };

    #   data files
    my $data_files = [];

    #   directory
    my $directory = $self->options->{'d'};
    if ( defined $directory && -d $directory ) {

        $data_files = $self->get_directory_files( { '-directory' => $directory } );

    }
    else {
        $data_files = [ $self->options->{'f'} ];
    }

    #   start transaction
    if ( !$self->start_transaction() ) {
        $self->error( { '-text' => 'Failed to start transaction' } );
        $self->rollback();
        return;
    }

LOAD_DATA_FILE:
    foreach my $data_file ( @{$data_files} ) {

        $self->info( { '-text' => 'Loading -data_file ' . $self->dbg_value($data_file) } );

        #   data
        my $data = $self->load_file_data( { '-file' => $data_file } );
        if ( !defined $data ) {
            $self->error( { '-text' => 'Failed to load data, cannot load XML data' } );
            next LOAD_DATA_FILE;
        }

        #    config
        my $config_file = $self->options->{'c'};
        if ( !defined $config_file ) {
            $config_file = $data_file;
            $config_file =~ s/[.]xml/.config.xml/sxmg;
        }

        $self->info( { '-text' => 'Using -config_file ' . $self->dbg_value($config_file) } );

        my $config;
        if ( -e $config_file ) {
            $config = $util_data_config_xml->load_file_config( { '-file' => $config_file } );
        }

        if (!$self->load(
                {   '-config' => $config,
                    '-data'   => $data,
                } )
            ) {
            $self->error( { '-text' => 'Failed to load data' } );

            #   roll everything back
            $self->rollback();

            last LOAD_DATA_FILE;
        }
    } ## end LOAD_DATA_FILE: foreach my $data_file ( @{$data_files...})

    #   commit
    $self->commit();

    return;
} ## end sub main

#   get directory files
sub get_directory_files {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-directory'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   directory
    my $directory = $args->{'-directory'};

    my $files = [];

    my $fh;
    opendir( $fh, $directory ) or croak 'Failed to open -directory ' . $self->dbg_value($directory) . ' :' . $OS_ERROR;
    my @dir_files = readdir($fh);
    closedir($fh) or croak 'Failed to close -directory ' . $self->dbg_value($directory) . ' :' . $OS_ERROR;

    foreach my $dir_file ( sort @dir_files ) {
        if ( $dir_file =~ /[.]config[.]xml$/sxmg ) {
            next;
        }
        if ( $dir_file =~ /[.]xml$/sxmg ) {
            push @{$files}, $directory . '/' . $dir_file;
        }
    }

    return $files;
}

#   load
sub load {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-data'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   data
    my $data = $args->{'-data'};

    #   config
    my $config = $args->{'-config'};
    if ( !defined $config ) {

        $config = [];

    GET_TABLE_CONFIG_DATA:
        foreach my $table_data ( @{$data} ) {

            #   table name
            my $table_name = $table_data->{'-table'};

            $self->info( { '-text' => 'Deriving config for -table_name ' . $self->dbg_value($table_name) } );

            #   columns
            my $columns = $db_table_util->fetch_columns( { '-table_name' => $table_name } );

            if ( !defined $columns || !scalar @{$columns} ) {
                next GET_TABLE_CONFIG_DATA;
            }

            #   table config
            my $table_config = {
                '-table'   => $table_name,
                '-columns' => [],
            };

            #   columns
            foreach my $column ( @{$columns} ) {
                push @{ $table_config->{'-columns'} }, { '-name' => $column->{'-column_name'} };
            }

            #   add table config
            push @{$config}, $table_config;
        }
    } ## end if ( !defined $config )

    $self->debug( { '-text' => 'Have -config ' . $self->dbg_value($config) . '-data ' . $self->dbg_value($data) } );

    #   total record count
    my $total_record_count = 0;

    foreach my $table_config ( @{$config} ) {

        $self->debug( { '-text' => 'Working -table_config ' . $self->dbg_value($table_config) } );

        my $table          = $table_config->{'-table'};
        my $columns        = $table_config->{'-columns'};
        my $truncate       = $table_config->{'-truncate'};
        my $auto_increment = $table_config->{'-auto_increment'};

        if ( !defined $truncate ) {

            #   add truncate if forced
            if ( $self->options->{'t'} ) {
                $self->info( { '-text' => 'Forcing truncate' } );
                $truncate = <<"STMT";
DELETE FROM `$table`
STMT
            }
        }

    GET_TABLE_DATA:
        foreach my $table_data ( @{$data} ) {

            my $record_data = $table_data->{'-data'};

            if ( defined $truncate && !$truncated->{$table} ) {
                if ( !$self->truncate_data( { '-statement' => $truncate } ) ) {
                    $self->error( { '-text' => 'Failed to truncate -table ' . $self->dbg_value($table) . ' data' } );
                    next GET_TABLE_DATA;
                }

                #   truncated
                $truncated->{$table} = 1;
            }

            #   config column count
            my $config_column_count = scalar @{$columns};

            #   statement
            my $statement = <<"STMT";
INSERT INTO `$table` (
STMT
            my $column_count = 0;
            foreach my $column ( @{$columns} ) {
                my $column_name = $column->{'-name'};
                if ( defined $column_name ) {
                    $statement .= <<"STMT";
`$column_name`,
STMT
                    $column_count++;
                }
            }
            chomp($statement);
            chop($statement);

            $statement .= <<"STMT";
) VALUES (
STMT
            $statement .= ' ?,' x $column_count;

            chop($statement);

            $statement .= ' )';

            $self->debug( { '-text' => 'Have -statement ' . $self->dbg_value($statement) } );

            my ( $sth, $record_count );
            my $success = eval {
                $sth = $self->dbh->prepare($statement);

                #   record count
                $record_count = 0;
                foreach my $record_values ( @{$record_data} ) {

                    $self->debug( { '-text' => 'Have -record_values ' . $self->dbg_value($record_values) } );

                    my $bind  = 1;
                    my $count = 0;
                BIND_VALUES:
                    foreach my $value ( @{$record_values} ) {

                        my $column = $columns->[$count];
                        if ( !defined $column->{'-name'} ) {
                            $self->debug( { '-text' => 'Skipping data -index ' . $self->dbg_value( $bind - 1 ) } );
                            $count++;
                            next BIND_VALUES;
                        }

                        $self->debug(
                            { '-text' => 'Binding data -bind ' . $self->dbg_value($bind) . ' -value ' . $self->dbg_value($value) }
                        );

                        $sth->bind_param( $bind, $value );
                        $bind++;
                        $count++;

                        if ( $bind > $column_count ) {
                            last BIND_VALUES;
                        }
                    }
                    $sth->execute();

                    $record_count++;
                } ## end foreach my $record_values (...)
                $sth->finish();
                1;
            };
            if ( !$success ) {
                $self->error( { '-text' => 'Failed to insert data into -table ' . $self->dbg_value($table) . ' :' . $EVAL_ERROR } );
                next GET_TABLE_DATA;
            }

            $total_record_count += $record_count;

            $self->debug( { '-text' => 'Inserted -record_count ' . $self->dbg_value($record_count) . ' records' } );

        } ## end GET_TABLE_DATA: foreach my $table_data ( @{...})

        #   auto increment
        if ( defined $auto_increment ) {
            if ( !$self->set_auto_increment( { '-table_name' => $table, '-value' => $auto_increment->{'-minimum'} } ) ) {
                $self->error( { '-text' => 'Failed to set auto increment' } );
                last;
            }
        }

    } ## end foreach my $table_config ( ...)

    $self->info( { '-text' => 'Inserted -total_record_count ' . $self->dbg_value($total_record_count) . ' records' } );

    return 1;
} ## end sub load

#   truncate data
sub truncate_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-statement'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   statement
    my $statement = $args->{'-statement'};

    $self->debug( { '-text' => 'Have truncate -statement ' . $self->dbg_value($statement) } );

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to truncate -statement ' . $self->dbg_value($statement) . ' :' . $EVAL_ERROR } );
        return;
    }

    return 1;
}

#   load file data
sub load_file_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-file'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   xml libxml util
    my $xml_libxml_util = $self->utils->{ UTIL_XML_LIBXML() };

    #   data file
    my $data_file = $args->{'-file'};
    if ( !defined $data_file ) {
        $self->error( { '-text' => 'Missing data file, cannot load XML data' } );
        return;
    }

    $self->debug( { '-text' => 'Loading -file ' . $self->dbg_value($data_file) } );

    #   raw data
    my $raw_data = $xml_libxml_util->parse( { '-file' => $data_file, '-validate' => 1 } );

    if ( !defined $raw_data->{'-elements'} || !scalar @{ $raw_data->{'-elements'} } ) {
        $self->error( { '-text' => 'Config appears to be empty' } );
        return;
    }

    #   data
    my $data = [];

GET_DATA_ELEMENTS:
    foreach my $element ( @{ $raw_data->{'-elements'} } ) {

        #   table data
        my $table_data = {};
        if ( $element->{'-name'} ne 'Data' ) {
            next GET_DATA_ELEMENTS;
        }

        #   table name
        $table_data->{'-table'} = $element->{'-attributes'}->{'-table'};

        #   table data
        $table_data->{'-data'} = [];

        #   elements
        my $elements = $element->{'-elements'};

    GET_RECORD_ELEMENTS:
        foreach my $rec_element ( @{$elements} ) {

            #   records
            if ( $rec_element->{'-name'} ne 'Record' ) {
                next GET_RECORD_ELEMENTS;
            }

            #   values
            my $values = [];

            #   rec_elements
            my $rec_elements = $rec_element->{'-elements'};

        GET_VALUE_ELEMENTS:
            foreach my $val_element ( @{$rec_elements} ) {

                #   values
                if ( $val_element->{'-name'} ne 'Value' ) {
                    next GET_VALUE_ELEMENTS;
                }

                push @{$values}, $val_element->{'-attributes'}->{'-data'};
            }

            push @{ $table_data->{'-data'} }, $values;
        }

        push @{$data}, $table_data;
    } ## end GET_DATA_ELEMENTS: foreach my $element ( @{ $raw_data...})

    return $data;
} ## end sub load_file_data

1;

__END__
