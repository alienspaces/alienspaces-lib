package AlienSpaces::IFace::Shell::Data::Loader::SQL;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Data::Loader);

use AlienSpaces::Properties {};

use constant {

};

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}


#   options config
sub options_config {
    return 'f:';
}

sub options_usage {
    print <<'USAGE';
    -h - show help
    -f - sql script file
USAGE
    
        return;
}

sub main {
    my ( $self, $args ) = @_;

    if ( !$self->start_transaction() ) {
        $self->debug( { '-text' => 'Failed to start transaction' } );
        return;
    }

    #   file 
    my $sql_script = $self->options->{'f'};
    if (!defined $sql_script) {
        $self->options_usage();
        return;
    }
        
    if (! -e $sql_script ) {
        $self->fatal({'-text' => 'Failed to find script ' . $sql_script . "\n" . $self->options_usage });
    }

    my $sql;
    {
        local $INPUT_RECORD_SEPARATOR = undef;
        my $fh;
        open $fh, '<', $sql_script or croak 'Failed to open -sql_script ' . $sql_script . ' for read :' . $OS_ERROR;
        $sql = <$fh>;
        close $fh or croak 'Failed to close -sql_script ' . $sql_script . ' :' . $OS_ERROR;
    }
    
    #   remove comments
    $sql =~ s/\-\-.*?\n//sxmg;
    
    #   remove blank lines
    my @temp_statements = split /\;/, $sql; #/
    
    my @statements = ();
    CLEAN_STATEMENTS:
    foreach my $statement (@temp_statements) {
        $statement =~ s/^\s+$//sxm;
        if (!length $statement) {
            next CLEAN_STATEMENTS;
        }
        push @statements, $statement;
    }

    $self->debug({'-text' => 'Executing ' . scalar @statements . ' statements from -sql_script ' . $sql_script });
            
    EXECUTE_STATEMENTS:
    foreach my $statement (@statements) {

        my $success = eval {
            my $sth = $self->dbh->prepare($statement);
            $sth->execute();
            $sth->finish();
            1;
        };
        if ( !$success ) {
            $self->debug( { '-text' => 'Failed to execute -sql_script ' . $statement . ' :' . $EVAL_ERROR } );
            $self->rollback();
            return;
        }
    }

    # commit
    $self->commit();

    return 1;
} ## end sub load

1;
