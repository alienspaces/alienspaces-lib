package AlienSpaces::IFace::Shell::Data::Loader::CSV;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell::Data::Loader);

use AlienSpaces::Properties {};

use AlienSpaces::Constants qw(:util);

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ 
        
        #   super
        @{ $self->SUPER::util_list }, 
        
        #   local
        UTIL_DATA_LOADER_MAP(), 
    ];

    return $util_list;
}

#   options config
sub options_config {
    return 'f:';
}

sub options_usage {
    print <<'USAGE';
    -h - show help
    -f - csv file config
USAGE

    return;
}

my $column_alpha_map = {
    0  => 'A',
    1  => 'B',
    2  => 'C',
    3  => 'D',
    4  => 'E',
    5  => 'F',
    6  => 'G',
    7  => 'H',
    8  => 'I',
    9  => 'J',
    10 => 'K',
    11 => 'L',
    12 => 'M',
    13 => 'N',
    14 => 'O',
    15 => 'P',
    16 => 'Q',
    17 => 'R',
    18 => 'S',
    19 => 'T',
    20 => 'U',
    21 => 'V',
    22 => 'W',
    23 => 'X',
    24 => 'Y',
    25 => 'Z',
};

#   load
sub main {
    my ( $self, $args ) = @_;

    #   loader map util
    my $loader_map_util = $self->utils->{ UTIL_DATA_LOADER_MAP() };

    my $file_data = $self->load_file_data();

    if ( !$self->start_transaction() ) {
        $self->debug( { '-text' => 'Failed to start transaction' } );
        return;
    }

    if ( $self->truncate_data ) {
        if ( !$self->truncate_table_data() ) {
            $self->debug( { '-text' => 'Failed to truncate table data' } );
            return;
        }
    }

    if ( $self->reset_auto_increment ) {
        if ( !$self->reset_table_auto_increment() ) {
            $self->debug( { '-text' => 'Failed to reset auto increment' } );
            return;
        }
    }

    my $file_column_configuration = $self->file_column_configuration();
    my $file_table_configuration  = $self->file_table_configuration();

    my $row_load_data = {};
    my $row_count     = 0;

PROCESS_ROW:
    foreach my $row_data ( @{$file_data} ) {
        my $idx = 0;
    PROCESS_COLUMN:
        foreach my $column_data ( @{$row_data} ) {
            my $column_configuration = $file_column_configuration->[$idx];
            my $table_name           = $column_configuration->{'-table_name'};
            my $data_key             = $column_configuration->{'-data_key'};
            my $value_map_method     = $column_configuration->{'-value_map_method'};

            if ( !defined $table_name ) {
                $idx++;
                next PROCESS_COLUMN;
            }

            if ( defined $value_map_method ) {
                my $map_column_data = $loader_map_util->$value_map_method( { '-data' => $column_data } );
                if ( defined $map_column_data && $map_column_data == $self->NOT_FOUND ) {
                    $self->rollback();
                    $self->debug(
                        {         '-text' => 'ERROR: -row '
                                . $row_count
                                . ' -column '
                                . $column_alpha_map->{$idx}
                                . ' -value '
                                . ( defined $column_data ? $column_data : 'undef' )
                                . ' -value_map '
                                . $value_map_method
                        } );
                    $self->debug( { '-text' => 'ERROR: -data ' . Dumper($row_data) } );
                    $row_count++;
                    next PROCESS_ROW;
                }
                $column_data = $map_column_data;
            }
            if ( !defined $row_load_data->{$table_name} ) {
                $row_load_data->{$table_name} = {};
            }
            $row_load_data->{$table_name}->{$data_key} = $column_data;
            $idx++;
        } ## end PROCESS_COLUMN: foreach my $column_data ( @...)

        my $table_insert_ids = {};
        foreach my $table_name (
            sort { $file_table_configuration->{$a}->{'-insert_order'} <=> $file_table_configuration->{$b}->{'-insert_order'} }
            keys %{$file_table_configuration}
            ) {

            my $table_method = $file_table_configuration->{$table_name}->{'-method'};

            my $log_values = '';
            foreach my $key ( keys %{ $row_load_data->{$table_name} } ) {
                $log_values .= (
                    defined $row_load_data->{$table_name}->{$key} ?
                        "\n\t $key - " . $row_load_data->{$table_name}->{$key}
                    : 'undef'
                ) . ' ';
            }
            $self->debug( { '-text' => "\n\t" . 'Insert -table ' . $table_name . ' -values ' . $log_values } );

            my $insert_id = $self->$table_method(
                {   '-data'             => $row_load_data->{$table_name},
                    '-table_name'       => $table_name,
                    '-table_insert_ids' => $table_insert_ids,
                } );
            $table_insert_ids->{$table_name} = $insert_id;
        }
        $row_count++;
        $row_load_data = {};

    } ## end PROCESS_ROW: foreach my $row_data ( @{$file_data...})

    $self->commit();

    return;
} ## end sub main

#   csv specific load file method
sub load_file_data {
    my ( $self, $args ) = @_;

    my $file_required_columns = $self->file_required_columns();

    my $data_file_name = $self->options->{'f'};

    if ( !-e $data_file_name ) {
        $self->fatal( { '-text' => 'No such file ' . $data_file_name . "\n" . $self->options_usage } );
    }

    my $fh;
    open $fh, '<', $data_file_name or croak 'Failed to open file for read -name ' . $data_file_name . ' :' . $OS_ERROR;
    my @file_contents = <$fh>;
    close $fh or croak 'Failed to close file :' . $OS_ERROR;

    if ( $self->file_remove_header ) {
        shift @file_contents;
    }

    my $field_separator       = $self->file_field_separator();
    my $field_quote_character = $self->file_field_quote_character();
    my @temp_data             = ();
    foreach my $data_row (@file_contents) {
        chomp $data_row;
        my @row_data = split /$field_separator/sxm, $data_row;

        my $data_found    = 0;
        my @temp_row_data = ();

    FIND_DATA:
        foreach my $column_data (@row_data) {

            #   remove quote characters
            if ( defined $field_quote_character ) {
                $column_data =~ s/$field_quote_character//sxmg;
            }
            if ( $self->file_field_trim_spaces ) {
                $column_data =~ s/^\s+//sxm;
                $column_data =~ s/(.*?)\s+$/$1/sxmg;
            }

            #   check for empty rows
            if ( length $column_data ) {
                $data_found = 1;
            }
            push @temp_row_data, $column_data;

        }

    CHECK_REQUIRED:
        foreach my $required_column ( @{$file_required_columns} ) {
            if ( !defined $temp_row_data[$required_column] ) {
                $data_found = 0;
                $self->debug( { '-text' => 'Required column ' . $required_column . ' missing, skipping row' } );
                last CHECK_REQUIRED;
            }
        }

        my $add_data = 1;
        if ( $self->file_remove_empty_rows ) {
            if ( !$data_found ) {
                $add_data = 0;
            }
        }
        if ($add_data) {
            push @temp_data, \@temp_row_data;
        }
    } ## end foreach my $data_row (@file_contents)
    @file_contents = @temp_data;

    return \@file_contents;
} ## end sub load_file_data

#   truncate
sub truncate_table_data {
    my ( $self, $args ) = @_;

    my $file_table_configuration = $self->file_table_configuration();

    my $truncate_where_clause = $self->truncate_data_where_clause;

    foreach my $table_name (
        sort { $file_table_configuration->{$b}->{'-insert_order'} <=> $file_table_configuration->{$a}->{'-insert_order'} }
        keys %{$file_table_configuration}
        ) {

        $self->debug( { '-text' => 'Truncating data from -table ' . $table_name } );

        my $statement = <<"STMT";
DELETE FROM `$table_name`
STMT
        if ( defined $truncate_where_clause ) {
            $statement .= <<"STMT";
$truncate_where_clause
STMT
        }

        my $success = eval {
            my $sth = $self->dbh->prepare($statement);
            $sth->execute();
            $sth->finish();
            1;
        };
        if ( !$success ) {
            $self->debug( { '-text' => 'Failed to truncate data :' . $EVAL_ERROR } );
            return;
        }
    }

    return 1;
} ## end sub truncate_table_data

#   auto increment
sub reset_table_auto_increment {
    my ( $self, $args ) = @_;

    my $file_table_configuration = $self->file_table_configuration();

    my $auto_increment_value = $self->reset_auto_increment();

    foreach my $table_name ( keys %{$file_table_configuration} ) {
        if ( !$self->set_auto_increment( { '-table_name' => $table_name, '-value' => $auto_increment_value } ) ) {
            $self->error( { '-text' => 'Failed to set auto increment' } );
            return;
        }
    }

    return 1;
}

#
#   configuration
#
sub file_required_columns {
    return [];
}

sub file_remove_header {
    return 1;
}

sub file_remove_empty_rows {
    return 1;
}

sub file_field_separator {
    return ';';
}

sub file_field_quote_character {
    return '"';
}

sub file_column_configuration {
    return [];
}

sub file_table_configuration {
    return {};
}

1;
