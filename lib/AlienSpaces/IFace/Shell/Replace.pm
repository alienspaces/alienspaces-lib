package AlienSpaces::IFace::Shell::Replace;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #        @{ $self->SUPER::util_list },
    ];

    return $util_list;
}

sub options_config {

    #   p - path
    #   m - match string
    #   r - replace string
    #   f - file type
    #   t - traverse

    return 'p:m:r:f:t';
}

sub options_usage {
    print <<'USAGE';
-h - show help
-p - path
-m - match 
-r - replace 
-f - file type
-t - traverse
USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Running' } );

    my $path      = $self->options->{'p'};
    my $match     = $self->options->{'m'};
    my $replace   = $self->options->{'r'};
    my $file_type = $self->options->{'f'};
    my $traverse  = $self->options->{'t'};

    $self->debug(
        {   '-text' => 'Working -path ' . $self->dbg_value($path) . ' -match ' . $self->dbg_value($match) . ' -replace ' .
                $self->dbg_value($replace) . ' -file ' . $self->dbg_value($file_type) . ' -traverse ' . $self->dbg_value($traverse)
        } );

    if ( !defined $path ) {
        croak options_usage();
    }
    if ( !defined $match ) {
        croak options_usage();
    }
    if ( !defined $replace ) {
        croak options_usage();
    }
    if ( !defined $file_type ) {
        croak options_usage();
    }

    #   get paths
    my @paths;
    if ($traverse) {
        push @paths, @{ $self->get_paths( { '-path' => $path } ) };
    }
    else {
        push @paths, $path;
    }

    #   process paths
    foreach my $process_path (@paths) {
        $self->process_path(
            {   '-path'      => $process_path,
                '-match'     => $match,
                '-replace'   => $replace,
                '-file_type' => $file_type,
            } );
    }

    return;
} ## end sub main

#   process paths
sub process_path {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-path', '-match', '-replace', '-file_type' ], $args ) ) {
        return;
    }

    my $path      = $args->{'-path'};
    my $match     = $args->{'-match'};
    my $replace   = $args->{'-replace'};
    my $file_type = $args->{'-file_type'};

    $self->debug( { '-text' => 'Processing -path ' . $path } );

    my $dirfh;
    opendir $dirfh, $path or croak 'Failed to open -path ' . $path . ' :' . $OS_ERROR;
    my @files = readdir $dirfh;
    closedir $dirfh or croak 'Failed to close -path ' . $path . ' :' . $OS_ERROR;

    #   process files
PROCESS_FILE:
    foreach my $file (@files) {
        if ( -d $file ) {
            next PROCESS_FILE;
        }

        if ( $file =~ /.*\.$file_type$/sxmg ) {
            $self->debug( { '-text' => 'Processing -file ' . $file } );

            my ( $fh, $content, $file_name );
            $file_name = $path . '/' . $file;
            open $fh, '<', $file_name or croak 'Failed to open -file ' . $file . ' for read :' . $OS_ERROR;
            {
                local $INPUT_RECORD_SEPARATOR;
                $content = <$fh>;
            }
            close $fh or croak 'Failed to close -file ' . $file . ' :' . $OS_ERROR;

            $content =~ s/$match/$replace/sxmg;

            open $fh, '>', $file_name or croak 'Failed to open -file ' . $file . ' for write :' . $OS_ERROR;
            print {$fh} $content;
            close $fh or croak 'Failed to close -file ' . $file . ' :' . $OS_ERROR;

        }
    }

    return;
} ## end sub process_path

#   fetch a path tree
sub get_paths {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-path'], $args ) ) {
        return;
    }

    my @paths = ( $args->{'-path'} );

    foreach my $path (@paths) {
        my $dfh;
        opendir $dfh, $path or croak 'Failed to open -path ' . $path . ' :' . $OS_ERROR;
        my @files = readdir $dfh;
        closedir $dfh or croak 'Failed to close -path ' . $path . ' :' . $OS_ERROR;

    FIND_FILE:
        foreach my $file (@files) {
            if ( $file =~ /^\./sxmg ) {
                next FIND_FILE;
            }
            my $new_path = $path . '/' . $file;

            if ( -d $new_path ) {
                push @paths, $new_path;
            }
        }
    }

    return \@paths;
} ## end sub get_paths

1;

