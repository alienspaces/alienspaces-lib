package AlienSpaces::IFace::Shell::Test::JSON;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace::Shell);

use AlienSpaces::Constants qw(:util);
use AlienSpaces::Properties {
    'test_count' => { 'type' => 's' },
    'test_pass'  => { 'type' => 's' },
    'test_fail'  => { 'type' => 's' },
};

use Time::HiRes qw(gettimeofday tv_interval);

use LWP::UserAgent;
use HTTP::Cookies;
use JSON;

use constant DEFAULT_METHOD        => 'POST';
use constant DEFAULT_COUNT         => 1;
use constant DEFAULT_SLEEP_SECONDS => 2;

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, UTIL_XML_LIBXML(), ];

    return $util_list;
}

sub options_config {
    return 'u:j:x:m:s:c:r';
}

sub options_usage {
    print <<'USAGE';

-h - show help
-u - url to test
-j - json string to test
-m - method 'POST' or 'GET'
-c - configuration XML (overrides -u, -j, -m, -x, -s)
-x - make request x times
-s - sleep seconds between requests
-r - show request

USAGE

    return;
}

sub main {
    my ( $self, $args ) = @_;

    $self->test_count(0);
    $self->test_pass(0);
    $self->test_fail(0);

    my $options     = $self->options;
    my $config_file = $options->{'c'};

    my $configs = [];
    if ( defined $config_file ) {

        #   xml configuration
        $configs = $self->get_xml_configs( { '-config_file' => $config_file } );

    }
    else {

        #   command line configuration

        my $url          = $options->{'u'};
        my $json         = $options->{'j'};
        my $method       = $options->{'m'};
        my $count        = $options->{'x'};
        my $sleep        = $options->{'s'};
        my $show_request = ( defined $options->{'r'} ? 1 : 0 );

        if ( !defined $url || !defined $json ) {
            options_usage();
            return;
        }

        #   default method
        if ( !defined $method ) {
            $method = DEFAULT_METHOD;
        }

        #   default count
        if ( !defined $count ) {
            $count = DEFAULT_COUNT;
        }

        #   default sleep
        if ( !defined $sleep ) {
            $sleep = DEFAULT_SLEEP_SECONDS;
        }
        push @{$configs},
            {
            '-url'          => $url,
            '-json'         => $json,
            '-method'       => $method,
            '-count'        => $count,
            '-sleep'        => $sleep,
            '-show_request' => $show_request,
            };
    } ## end else [ if ( defined $config_file)]

EXECUTE_TESTS:
    foreach my $config ( @{$configs} ) {
        if (!$self->test_json_post(
                {   '-url'          => $config->{'-url'},
                    '-json'         => $config->{'-json'},
                    '-method'       => $config->{'-method'},
                    '-count'        => $config->{'-count'},
                    '-sleep'        => $config->{'-sleep'},
                    '-show_request' => $config->{'-show_request'},
                } )
            ) {
            last EXECUTE_TESTS;
        }
    }

    $self->show_results();

    return;
} ## end sub main

sub show_results {
    my ( $self, $args ) = @_;

    print 'Tests ' . $self->test_count . ' Passed ' . $self->test_pass . ' Failed ' . $self->test_fail . "\n";
    return;
}

sub get_xml_configs {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-config_file'], $args ) ) {
        return;
    }

    #   xml libxml util
    my $xml_libxml_util = $self->utils->{ UTIL_XML_LIBXML() };

    my $config_file = $args->{'-config_file'};

    my $doc = $xml_libxml_util->parse( { '-file' => $config_file } );
    if ( !defined $doc ) {
        $self->fatal( { '-text' => 'Could not get configuration from -config_file ' . $config_file } );
    }

    my $configs = [];

FIND_REQUESTS:
    foreach my $element ( @{ $doc->{'-elements'} } ) {

        if ( $element->{'-name'} ne 'Request' ) {
            next FIND_REQUESTS;
        }

        #   attributes
        my $attributes = $element->{'-attributes'};
        if ( !defined $attributes ) {
            next FIND_REQUESTS;
        }

        #   url
        my $url = $attributes->{'-url'};

        #   method
        my $method = $attributes->{'-method'};

        #   count
        my $count = $attributes->{'-repeat'};

        #   sleep
        my $sleep = $attributes->{'-sleep'};

        #   show request
        my $show_request = ( defined $attributes->{'-showrequest'} ? $attributes->{'-showrequest'} : 1 );

        #   elements
        my $elements = $element->{'-elements'};
        if ( !defined $elements ) {
            next FIND_REQUESTS;
        }

        #   json
        my $json;

    FIND_JSON:
        foreach my $child_element ( @{$elements} ) {

            if ( $child_element->{'-name'} ne 'JSON' ) {
                next FIND_JSON;
            }

            #   attributes
            $attributes = $child_element->{'-attributes'};
            if ( !defined $attributes ) {
                next FIND_JSON;
            }

            $json = $attributes->{'-data'};
        }

        if ( !defined $url || !defined $json ) {
            $self->fatal( { '-text' => 'Missing -url ' . $self->dbg_value($url) . ' or -json ' . $self->dbg_value($json) . ', cannot continue' } );
        }

        #   default method
        if ( !defined $method ) {
            $method = DEFAULT_METHOD;
        }

        #   default count
        if ( !defined $count ) {
            $count = DEFAULT_COUNT;
        }

        #   default sleep
        if ( !defined $sleep ) {
            $sleep = DEFAULT_SLEEP_SECONDS;
        }
        push @{$configs},
            {
            '-url'          => $url,
            '-json'         => $json,
            '-method'       => $method,
            '-count'        => $count,
            '-sleep'        => $sleep,
            '-show_request' => $show_request,
            };

    } ## end FIND_REQUESTS: foreach my $element ( @{ $doc...})

    return $configs;
} ## end sub get_xml_configs

my $cookie_jar;

sub test_json_post {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-url', '-json' ], $args ) ) {
        return;
    }

    my $url    = $args->{'-url'};
    my $json   = $args->{'-json'};
    my $method = $args->{'-method'};
    my $count  = $args->{'-count'};
    my $sleep  = $args->{'-sleep'};

    if ( $args->{'-show_request'} ) {
        print 'Requesting: -url ' . $url . ' -json ' . $json . ' -x ' . $count . ' times' . "\n";
    }

    #   create cookie jar
    if ( !defined $cookie_jar ) {
        $cookie_jar = HTTP::Cookies->new();
    }

    #   create request
    my $request = HTTP::Request->new();

    #   method
    $request->method($method);

    #   uri
    $request->uri($url);

    #   content
    $request->content($json);

    #   create response
    my $ua = LWP::UserAgent->new();

    #   add cookie jar
    $ua->cookie_jar($cookie_jar);

    $self->test_count( $self->test_count + 1 );

    for ( 1 .. $count ) {

        #   time request
        my $time = [gettimeofday];

        #   make request
        my $response = $ua->request($request);

        #   time request
        my $elapsed = tv_interval($time);

        #   json
        my $json_parser = JSON->new->allow_nonref;

        #   struct
        my $struct;
        my $success = eval {
            $struct = $json_parser->decode( $response->content );
            1;
        };
        if ( !$success ) {
            $struct = { '-error_text' => $EVAL_ERROR . ' : ' . $response->content };
        }

        print STDOUT 'Response: -as_string ' . $self->dbg_value($struct) . "\n" . ' -time ' . $elapsed . "\n";

        if ( !defined $struct ) {
            print STDERR 'Test failed, component failed to return a struct' . "\n";

            $self->test_fail( $self->test_fail + 1 );

            return;
        }

        if ( !defined $struct->{'status'} ) {
            print STDERR 'Test failed, component failed to return a status' . "\n";

            $self->test_fail( $self->test_fail + 1 );

            return;
        }

        if ( $struct->{'status'} == 0 ) {
            print STDERR 'Test failed, component returned status 0' . "\n";

            $self->test_fail( $self->test_fail + 1 );

            return;
        }

        sleep $sleep;
    } ## end for ( 1 .. $count )

    $self->test_pass( $self->test_pass + 1 );

    return 1;
} ## end sub test_json_post

1;

