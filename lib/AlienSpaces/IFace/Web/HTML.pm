package AlienSpaces::IFace::Web::HTML;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(
    :loader
    :interface
    :util
    :program
    :comp
    :task
    :auth
    :error-codes-global
    :message-types
    :section
);

our @DYNISA = qw(IFace::Web);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {

    'css' => { '-type' => 'a', '-default' => [] },    #   list of style sheets for current program
    'js'  => { '-type' => 'a', '-default' => [] },    #   list of javascript files for current program

};

use Apache2::Const qw(OK REDIRECT);
use Time::HiRes qw(gettimeofday tv_interval);

#   html util object load list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_CGI(),        # html display / cgi parameter interface
        UTIL_MESSAGE(),    # messaging data interface

        UTIL_TEMPLATE(),   # template toolkit abstraction

        UTIL_OUTPUT_HTML_PAGE(),          # html page
        UTIL_OUTPUT_HTML_PANEL(),         # html panel
        UTIL_OUTPUT_HTML_TABLE(),         # html table
        UTIL_OUTPUT_HTML_NAVIGATION(),    # html navigation
        UTIL_OUTPUT_HTML_DIALOGUE(),      # html dialogue
    ];

    return $util_list;
}

#   html util object clear list
sub util_clear_list {
    my ( $self, $args ) = @_;

    my $clear_list = [

        #   super
        @{ $self->SUPER::util_clear_list },

        #   local
        UTIL_CGI(),
    ];

    return $clear_list;
}

#   html util new args
sub util_new_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   html util init args
sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   html comp list
sub comp_list {
    my $self = shift;

    return [];
}

#   html comp new args
sub comp_new_args {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-class'], $args ) ) {
        return;
    }

    my $comp_args = $self->SUPER::comp_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

#   html comp init args
sub comp_init_args {
    my ( $self, $args ) = @_;

    my $comp_args = $self->SUPER::comp_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

#   html init
sub init {
    my ( $self, $args ) = @_;

    #   SUPER init
    if ( !$self->SUPER::init($args) ) {
        $self->debug( { '-text' => 'Failed SUPER init' } );
        return;
    }

    $self->debug( { '-text' => 'Init' } );

    #   clear css
    $self->css( [] );

    #   clear js
    $self->js( [] );

    #   init user session
    $self->init_user_session();

    #   init user language
    $self->init_user_language();

    #   load comps
    $self->load_comps();

    return 1;
}

#   check interface authorize user
sub check_interface_authorize_user {
    my ( $self, $args ) = @_;

    #   actions
    my $actions = $self->actions();

    $self->debug( { '-text' => 'Checking interface authorize user' } );

    #   super check interface authorize user
    if ( !$self->SUPER::check_interface_authorize_user( { '-actions' => $actions } ) ) {

        $self->debug( { '-text' => 'User does not have authorization to this program, redirecting' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_UNAUTHORIZED } );

        #   redirect
        my $redirect_url = $self->get_redirect_url();
        if ( !defined $redirect_url ) {
            $self->debug( { '-text' => 'User does not have authorization to this program, redirecting' } );

            #   authorize redirect url
            $self->set_redirect_url( { '-url' => $self->authorize_redirect_url( { '-actions' => $actions } ) } );
        }
        return;
    }

    #   check user authorization
    if ( $self->authorize_user( { '-actions' => $actions } ) != AUTHORIZE_SUCCESS ) {

        $self->debug( { '-text' => 'User does not have authorization to this program, redirecting' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_UNAUTHORIZED } );

        #   redirect
        my $redirect_url = $self->get_redirect_url();
        if ( !defined $redirect_url ) {

            #   authorize redirect url
            $self->set_redirect_url( { '-url' => $self->authorize_redirect_url( { '-actions' => $actions } ) } );
        }

        return;
    }

    return 1;
} ## end sub check_interface_authorize_user

#   authorize redirect url
sub authorize_redirect_url {
    my ( $self, $args ) = @_;

    #   default redirect url for unauthorized access
    return PROGRAM_SITE_HOME();
}

#   parse input params
sub parse_input_params {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my @actions    = $cgi_util->param('action');
    my @components = $cgi_util->param('panel');
    if ( !scalar @components ) {
        @components = $cgi_util->param('component');
    }

    $self->debug( { '-text' => 'Have -panels ' . $self->dbg_value( \@components ) } );
    $self->debug( { '-text' => 'Have -actions ' . $self->dbg_value( \@actions ) } );

    #   missing action or missing component
    #   - give the default config a chance to define a default component OR a default action
    if ( !scalar @actions || !scalar @components ) {

        $self->debug( { '-text' => 'Missing a submitted action or component, looking for default action config' } );

        my $default_action_configs = $self->default_action_config();
        if ( defined $default_action_configs && scalar @{$default_action_configs} ) {
            foreach my $default_action_config ( @{$default_action_configs} ) {
                my $component = $default_action_config->{'-component'};
                my $action    = $default_action_config->{'-action'};
                if ( defined $component ) {
                    push @components, $component;
                }
                if ( defined $action ) {
                    push @actions, $action;
                }
            }
        }

        #   still missing something
        if ( !scalar @actions || !scalar @components ) {
            $self->debug( { '-text' => 'Still missing a submitted action or component, no need to continue parsing' } );
            return 1;
        }
    }

    #   package up component action requests
    my $component_actions = {};

    #   for every action param we will assume the associated panel is at the same index
    #   - otherwise we'll give the panel the previous panels action (/shrug)
    #   - for any other params we'll package them with every component / action package
    #     as we have no idea which are for what
    my $params = {};
FIND_PARAMS:
    foreach my $param ( $cgi_util->param ) {

        #   skip these
        if ( $param eq 'action' ||
            $param eq 'panel'           ||
            $param eq 'component'       ||
            $param eq '-apache_request' ||
            $param eq '-environment'    ||
            $param eq '-interface'      ||
            $param eq '-program_name' ) {
            next FIND_PARAMS;
        }

        #   (security) do not log params
        my $param_value = $cgi_util->param($param);
        $params->{ lc($param) } = $cgi_util->param($param);
    }

    #   total packages
    my $total_components = ( scalar @components ) - 1;
    my $total_actions    = ( scalar @actions ) - 1;

    my $total;
    if ( $total_components > $total_actions ) {
        $total = $total_components;
    }
    else {
        $total = $total_actions;
    }

    my $component_idx = 0;
    my $action_idx    = 0;
    foreach ( 0 .. $total ) {

        my $action    = $actions[$action_idx];
        my $component = $components[$component_idx];

        my $session_data = $self->get_comp_session_data( { '-component' => $component } );

        my $component_action_pack = {
            '-action'       => $action,
            '-panel'        => $component,
            '-component'    => $component,
            '-data'         => $params,
            '-session_data' => $session_data,
            '-interface'    => $self->interface,
        };

        if ( defined $actions[ $action_idx + 1 ] ) {
            $action_idx++;
        }
        if ( defined $components[ $component_idx + 1 ] ) {
            $component_idx++;
        }

        $component_actions->{$component} = $component_action_pack;

        #   (security) do not log params
        $self->debug(
            {   '-text' => 'Packing -name ' . $component . ' -action ' .
                    $self->dbg_value($action) . ' -component ' . $self->dbg_value($component) . ' -interface ' . $self->interface
            } );
    } ## end foreach ( 0 .. $total )

    #   actions
    $self->actions($component_actions);

    return 1;
} ## end sub parse_input_params

#   debug input params
#   - (security) this method should only be called by components that do not
#     work with any secure data such as passwords
sub debug_input_params {
    my ( $self, $args ) = @_;

    my $actions = $self->actions();
    if ( !defined $actions ) {
        $self->debug( { '-text' => 'No action parameters to debug' } );
        return;
    }

    foreach my $p_key ( keys %{$actions} ) {
        $self->debug( { '-text' => 'Param -key ' . $p_key . ' -value ' . $self->dbg_value( $actions->{$p_key} ) } );
    }

    return 1;
}

#   html get content type
sub get_content_type {
    my ( $self, $args ) = @_;

    return 'text/html';
}

#   document description, keywords, title
sub document_description {
    my ( $self, $args ) = @_;

    return 'Description for the document';
}

sub document_keywords {
    my ( $self, $args ) = @_;

    return 'Keywords for the document';
}

sub document_title {
    my ( $self, $args ) = @_;

    return 'Title for the document';
}

#   check interface render
sub check_interface_render {
    my ( $self, $args ) = @_;

    #   check redirected
    my $redirect_url = $self->get_redirect_url();
    if ( defined $redirect_url ) {
        $self->debug( { '-text' => 'Redirect notice given, returning' } );
        return;
    }

    return 1;
}

sub render {
    my ( $self, $args ) = @_;

    my $html_output_page_util = $self->utils->{ UTIL_OUTPUT_HTML_PAGE() };

    my $render_vars = $self->get_render_vars($args);

    my $ret = $html_output_page_util->render(
        {   '-vars'         => $render_vars,
            '-program_name' => $self->program_name,
        } );

    return $ret;
}

sub get_render_vars {
    my ( $self, $args ) = @_;

    my $actions = $args->{'-actions'};

    my $render_vars = {
        'css'         => $self->get_css,
        'js'          => $self->get_js,
        'keywords'    => $self->document_keywords,
        'description' => $self->document_description,
        'title'       => $self->document_title,
        'class'       => $self->make_css_class( { '-string' => $self->program_name } ),
        'section'     => $self->render_sections(
            {   '-sections' => [ SECTION_HEADER(), SECTION_PRIMARY(), SECTION_SECONDARY(), SECTION_TERTIARY(), SECTION_FOOTER(), ],
                '-actions'  => $actions,
            }
        ),
    };

    return $render_vars;
}

sub get_css {
    my ( $self, $args ) = @_;

    if ( !$self->css() ) {
        return;
    }

    my $css = [];

    #   add application
    push @{ $self->css() }, lc( $self->environment->{'APP_NAME'} ) . '.css';

    #   add current program
    my $program_name = $self->program_name;
    $program_name =~ s/\//-/sxmg;
    push @{ $self->css() }, $program_name . '.css';

    $self->debug( { '-text' => 'Looking for -css ' . $self->dbg_value( $self->css() ) } );

APP_CSS:
    foreach my $stylesheet ( @{ $self->css() } ) {

        #   external style sheet
        if ( $stylesheet =~ /http/sxm ) {
            push @{$css}, $stylesheet;
            next APP_CSS;
        }

        #   try in /css
        my $test_stylesheet = '/css/' . $stylesheet;
        if ( !-e $self->environment->{'APP_WEB_HOME'} . $test_stylesheet ) {
            $self->debug( { '-text' => 'Application -css ' . $self->environment->{'APP_WEB_HOME'} . $test_stylesheet . ' does not exist, not adding' } );

            #   try without /css
            $test_stylesheet = $stylesheet;
            if ( !-e $self->environment->{'APP_WEB_HOME'} . $test_stylesheet ) {
                $self->debug( { '-text' => 'Application -css ' . $self->environment->{'APP_WEB_HOME'} . $test_stylesheet . ' does not exist, not adding' } );
                next APP_CSS;
            }
        }
        push @{$css}, $test_stylesheet;
    }

    return $css;
} ## end sub get_css

sub get_js {
    my ( $self, $args ) = @_;

    my $js = [];

    #   add application
    push @{ $self->js() }, lc( $self->environment->{'APP_NAME'} ) . '.js';

    #   add current program
    my $program_name = $self->program_name;
    $program_name =~ s/\//-/sxmg;
    push @{ $self->js() }, $program_name . '.js';

    $self->debug( { '-text' => 'Looking for -js ' . $self->dbg_value( $self->js() ) } );

APP_JS:
    foreach my $javascript ( @{ $self->js() } ) {

        #   external javascript
        if ( $javascript =~ /http/sxm ) {
            push @{$js}, $javascript;
            next APP_JS;
        }

        #   try in /js
        my $app_js = '/js/' . $javascript;
        if ( !-e $self->environment->{'APP_WEB_HOME'} . $app_js ) {

            #   try without /js
            $app_js = $javascript;
            if ( !-e $self->environment->{'APP_WEB_HOME'} . $app_js ) {
                $self->debug( { '-text' => 'Application -javascript ' . $javascript . ' does not exist, not adding' } );
                next APP_JS;
            }
        }
        push @{$js}, $app_js;
    }

    #
    #   component js
    #
    $self->debug( { '-text' => 'Adding comps Javascript' } );

COMP_JS:
    foreach my $comp_name ( keys %{ $self->comps() } ) {
        my $comp_js = '/js/comp/' . $comp_name . '.js';
        if ( !-e $self->environment->{'APP_WEB_HOME'} . $comp_js ) {
            next COMP_JS;
        }
        $self->debug( { '-text' => 'Adding component -javascript ' . $comp_js } );
        push @{$js}, $comp_js;
    }

    return $js;
} ## end sub get_js

#   section text
#   - used for id / class names
my $section_text = {
    SECTION_HEADER()    => 'header',
    SECTION_PRIMARY()   => 'primary',
    SECTION_SECONDARY() => 'secondary',
    SECTION_TERTIARY()  => 'tertiary',
    SECTION_FOOTER()    => 'footer',
};

#   message type class map
my %message_type_class_map = (
    MESSAGE_TYPE_SUCCESS() => 'Success',
    MESSAGE_TYPE_ERROR()   => 'Error',
    MESSAGE_TYPE_INFO()    => 'Info',
);

#   component messages
sub render_messages {
    my ( $self, $args ) = @_;

    my $cgi = $self->cgi;

    #   names
    my @names = ( 'global', $self->program_name );

    if ( exists $args->{'-name'} ) {
        push @names, $args->{'-name'};
    }

    #   type
    my $type = $args->{'-type'};

    #   messages
    my @messages;

    if ( exists $args->{'-messages'} ) {
        push @messages, @{ $args->{'-messages'} };
    }

    foreach my $name (@names) {
        my $messages = $self->get_messages( { '-name' => $name, '-type' => $type } );
        if ( defined $messages && scalar @{$messages} ) {
            push @messages, @{$messages};
        }
    }

    #   content
    my $content = $cgi->start_div( { '-class' => 'Messages' } );

    #   message count
    my $message_count = 0;

    #   sort messages by type
RENDER_MESSAGES:
    foreach my $message ( sort { $a->{'-type'} <=> $b->{'-type'} } @messages ) {

        #   text
        my $message_text = $message->{'-text'};
        if ( !$message_text ) {
            next RENDER_MESSAGES;
        }

        #   type
        my $message_type = $message->{'-type'};

        $content .= $cgi->div( { '-class' => $message_type_class_map{$message_type} }, $message_text );

        $message_count++;
    }

    $content .= $cgi->end_div();

    if ( !$message_count ) {
        $self->debug( { '-text' => 'No messages rendered' } );
        return;
    }

    return $content;
} ## end sub render_messages

#   render message content
sub render_message_content {
    my ( $self, $args ) = @_;

    #   render messages
    my $ret = $self->render_messages($args);

    return ( defined $ret ? $ret : '' );
}

#   get sections config
sub section_config {
    my ( $self, $args ) = @_;

    my $section_config = {};

    #   header
    $section_config->{ SECTION_HEADER() }{'components'} = [];

    #   primary
    $section_config->{ SECTION_PRIMARY() }{'components'} = [];

    #   secondary
    $section_config->{ SECTION_SECONDARY() }{'components'} = [];

    #   tertiary
    $section_config->{ SECTION_TERTIARY() }{'components'} = [];

    #   footer
    $section_config->{ SECTION_FOOTER() }{'components'} = [];

    return $section_config;
}

#   render sections
sub render_sections {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-sections', '-actions' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   section
    my $sections = delete $args->{'-sections'};
    if ( !defined $sections ) {
        return;
    }
    if ( ref($sections) ne 'ARRAY' ) {
        $sections = [$sections];
    }

    #   cgi
    my $cgi = $self->cgi;

    #   actions
    my $actions = $args->{'-actions'};

    #   sections config
    my $sections_config = $self->section_config();
    if ( !defined $sections_config ) {
        $self->debug( { '-text' => 'Missing sections config, not rendering section content' } );
        return;
    }

    my $ret = {};

SECTIONS:
    foreach my $section ( @{$sections} ) {

        my $section_config = $sections_config->{$section};
        if ( !defined $section_config ) {
            $self->debug( { '-text' => 'Section config missing for -section ' . $section . ', skipping' } );
            next SECTIONS;
        }

        my $section_components_content =
            $self->render_section_components(
            { '-section_config' => $section_config, '-section' => $section, '-actions' => $actions } );

        $ret->{$section} = { 'components' => $section_components_content, };
    }

    return $ret;
} ## end sub render_sections

sub render_section_components {
    my ( $self, $args ) = @_;

    my $section_config = $args->{'-section_config'};
    my $section        = $args->{'-section'};
    my $actions        = $args->{'-actions'};

    my $comp_list = $section_config->{'components'};
    if ( ref($comp_list) ne 'ARRAY' ) {
        $comp_list = [$comp_list];
    }

    my $section_content = [];

    foreach my $comp_name ( @{$comp_list} ) {

        my $comp = $self->comps->{$comp_name};
        if ( !defined $comp ) {
            $self->error( { '-text' => 'No -component ' . $comp_name . ', cannot render component' } );
            next;
        }

        $self->debug( { '-text' => 'Rendering -component ' . $comp_name . ' in -section ' . $section } );

        my $action       = $actions->{$comp_name}->{'-action'};
        my $data         = $actions->{$comp_name}->{'-data'};
        my $session_data = $actions->{$comp_name}->{'-session_data'};
        my $config_data  = $actions->{$comp_name}->{'-config_data'};
        my $interface    = INTERFACE_HTML;

        #   get comp config data
        $config_data = $self->get_comp_config_data(
            {   '-action'       => $action,
                '-component'    => $comp_name,
                '-data'         => $data,
                '-session_data' => $session_data,
                '-config_data'  => $config_data,
                '-interface'    => $interface,
            } );

        $actions->{$comp_name}->{'-config_data'} = $config_data;

        #   comp content
        my $comp_content = $comp->render( { '-actions' => $actions } );

        push @{$section_content}, ( defined $comp_content ? $comp_content : ' ' );
    } ## end foreach my $comp_name ( @{$comp_list...})

    return $section_content;
} ## end sub render_section_components

#
#   short cuts
#
sub cgi {
    my ( $self, $args ) = @_;

    my $cgi = $self->utils->{ UTIL_CGI() };

    return $cgi;
}

#   finish
sub finish {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::finish($args) ) {
        $self->debug( { '-text' => 'Finish failed in super' } );
        return;
    }

    return 1;
}

1;

__END__

