package AlienSpaces::IFace::Web::JSON::T;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :comp :auth);

our @DYNISA = qw(IFace::Web::JSON);

our $VERSION = (qw$Revision: $)[1];

sub init {
    my ( $self, $args ) = @_;

    return $self->SUPER::init($args);
}

#
#   user authentication
#
sub authorize_user {
    my ( $self, $args ) = @_;

    return AUTHORIZE_SUCCESS();
}

#
#   component list
#
sub comp_list {
    my ($self, $args) = @_;

    return [ 
        
        #   super
        @{ $self->SUPER::comp_list }, 
        
        #   local

    ];
}

1;
