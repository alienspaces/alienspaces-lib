package AlienSpaces::IFace::Web::JSON::Session;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :comp);

our @DYNISA = qw(IFace::Web::JSON);

our $VERSION = (qw$Revision: $)[1];

sub init {
    my ( $self, $args ) = @_;

    return $self->SUPER::init($args);
}

#
#   user authentication
#
sub authorize_user {
    my ( $self, $args ) = @_;

    return $self->AUTHORIZE_SUCCESS;
}

#
#   component list
#
sub comp_list {
    my $self = shift;

    return [

        #   superclass components
        @{ $self->SUPER::comp_list },

        #   local components
        COMP_JSON_API_SESSION_UPDATE(),

    ];
}

1;
