package AlienSpaces::IFace::Web::JSON;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(
    :loader
    :util
    :program
    :task
    :auth
    :error-codes-global
);

our @DYNISA = qw(IFace::Web);

our $VERSION = (qw$Revision: $)[1];

use constant READ_LENGTH => 1024;

use Apache2::Const qw(OK REDIRECT);
use JSON;
use Data::Dumper;
use Time::HiRes qw(gettimeofday tv_interval);

#   json util object load list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

#   json util object clear list
sub util_clear_list {
    my ( $self, $args ) = @_;

    my $clear_list = [ @{ $self->SUPER::util_clear_list }, ];
    return $clear_list;
}

#   json util new args
sub util_new_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   json util init args
sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    if ( defined $args->{'-class'} ) {

    }
    return $util_args;
}

#   json comp list
sub comp_list {
    my $self = shift;

    return [];
}

#   json comp new args
sub comp_new_args {
    my ( $self, $args ) = @_;

    my $comp_args = $self->SUPER::comp_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

#   json comp init args
sub comp_init_args {
    my ( $self, $args ) = @_;

    my $comp_args = $self->SUPER::comp_init_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $comp_args;
}

#   json init
sub init {
    my ( $self, $args ) = @_;

    #   SUPER init
    if ( !$self->SUPER::init($args) ) {
        $self->debug( { '-text' => 'Failed SUPER init' } );
        return;
    }

    $self->debug( { '-text' => 'Init' } );

    #   init user session
    $self->init_user_session();

    #   init user language
    $self->init_user_language();

    #   load comps
    $self->load_comps();

    return 1;
}

#   check interface authorize user
sub check_interface_authorize_user {
    my ( $self, $args ) = @_;

    #   actions
    my $actions = $self->actions();

    #   super check interface authorize user
    if ( !$self->SUPER::check_interface_authorize_user( { '-actions' => $actions } ) ) {
        $self->debug( { '-text' => 'User does not have authorization to this program' } );
        return;
    }

    #   check user authorization
    if ( $self->authorize_user( { '-actions' => $actions } ) != AUTHORIZE_SUCCESS ) {

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_UNAUTHORIZED } );

        $self->debug( { '-text' => 'User does not have authorization to this program, redirection not supported' } );
        return;
    }

    return 1;
}

sub authorize_redirect_url {
    my ( $self, $args ) = @_;

    croak 'Interface does not support redirection';
}

sub verify_redirect_url {
    my ( $self, $args ) = @_;

    croak 'Interface does not support redirection';
}

#   json parse input
sub parse_input_params {
    my ( $self, $args ) = @_;

    #   apache request
    my $apache_request = $self->apache_request();

    $apache_request->subprocess_env;

    $self->debug(
        {   '-text' => 'Content MOD_PERL ' . $self->dbg_value( $ENV{'MOD_PERL_API_VERSION'} ) .
                ' -content_length ' . $self->dbg_value( $ENV{'CONTENT_LENGTH'} ) . ' -request_method ' .
                $self->dbg_value( $ENV{'REQUEST_METHOD'} ) . ' -query_string ' . $self->dbg_value( $ENV{'QUERY_STRING'} ) } );

    #   decode json string
    my $json;

    #   json string
    my $json_string;
    if ( $ENV{'REQUEST_METHOD'} eq 'GET' ) {
        $json_string = $ENV{'QUERY_STRING'};

        #   GET - key / value
        my @pairs = split /\&/sxm, $json_string;
        foreach my $pair (@pairs) {
            my ( $key, $value ) = split /\=/sxm, $pair;
            if ( defined $key && defined $value ) {
                $json->{$key} = $value;
            }
        }

    }
    else {

        #   POST - json input stream
        my ( $buffer, $length );

        $length = READ_LENGTH;

        while ( $apache_request->read( $buffer, $length, 0 ) ) {
            $self->debug( { '-text' => "Read -buffer $buffer" } );
            $json_string .= $buffer;
        }

        $self->trace( { '-text' => 'Full JSON -string ' . $self->dbg_value($json_string) } );

        if ( length $json_string ) {
            my $success = eval {
                $json = from_json($json_string);
                1;
            };
            if ( !$success ) {
                $self->error( { '-text' => 'Failed parsing -json_string ' . $json_string . ' :' . $EVAL_ERROR } );

                #   set error
                $self->set_error( { '-error_code' => ERROR_CODE_API_FAILED_INPUT_PARSE } );

                return;
            }
        }
    }

    $self->trace( { '-text' => 'Have new json structure ' . Dumper($json) } );

    my $component_actions = {};

    my $json_structures = [];
    if ( ref $json eq 'HASH' ) {
        push @{$json_structures}, $json;
    }
    elsif ( ref $json eq 'ARRAY' ) {
        $json_structures = $json;
    }

    if ( !scalar @{$json_structures} ) {

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_API_FAILED_INPUT_PARSE } );

        return;
    }

PARSE_JSON:
    foreach my $json_structure ( @{$json_structures} ) {
        my $action    = $json_structure->{'action'};
        my $component = $json_structure->{'component'};

        #   missing action or missing component
        #   - give the default config a chance to define a default component OR a default action
        if ( !defined $action || !defined $component ) {

            $self->debug(
                {   '-text' => 'Missing a submitted -action ' . $self->dbg_value($action) .
                        ' or -component ' . $self->dbg_value($component) . ', looking for default action config'
                } );

            my $default_action_configs = $self->default_action_config();
            if ( defined $default_action_configs && scalar @{$default_action_configs} ) {
            FIND_DEFAULT:
                foreach my $default_action_config ( @{$default_action_configs} ) {
                    my $check_component = $default_action_config->{'-component'};
                    my $check_action    = $default_action_config->{'-action'};

                    if ( defined $check_component || defined $check_action ) {
                        $self->debug(
                            {   '-text' => 'Testing default -action ' .
                                    $self->dbg_value($check_action) . ' and -component ' . $self->dbg_value($check_component) } );

                        #   can we fill in an action for a submitted component?
                        if ( defined $component && !$action && defined $check_action ) {
                            $action = $check_action;
                            last FIND_DEFAULT;
                        }

                        #   can we fill in a component for a submitted action?
                        if ( defined $action && !defined $component && defined $check_component ) {
                            $component = $check_component;
                            last FIND_DEFAULT;
                        }

                        #   how about we filling on both
                        if ( !defined $action && !defined $component && defined $check_action && defined $check_component ) {
                            $action    = $check_action;
                            $component = $check_component;
                            last FIND_DEFAULT;
                        }
                    }
                }
            } ## end if ( defined $default_action_configs...)
        } ## end if ( !defined $action ...)

        #   test action
        if ( !defined $action ) {
            $self->set_error( { '-error_code' => ERROR_CODE_API_MISSING_ACTION() } );
            last PARSE_JSON;
        }

        #   test component
        if ( !defined $component ) {
            $self->set_error( { '-error_code' => ERROR_CODE_API_MISSING_COMPONENT() } );
            last PARSE_JSON;
        }

        my $params = {};
    ADD_PARAMS:
        foreach my $key ( keys %{$json_structure} ) {
            if ( $key eq 'component' || $key eq 'action' || $key eq 'panel' ) {
                next ADD_PARAMS;
            }
            $params->{ lc($key) } = $json_structure->{$key};
        }

        #   session data
        my $session_data = $self->get_comp_session_data( { '-component' => $component } );

        my $component_action_pack = {
            '-action'       => $action,
            '-panel'        => $component,         #   compatible with old style naming of components (panels)
            '-component'    => $component,
            '-interface'    => $self->interface,
            '-data'         => $params,
            '-session_data' => $session_data,
        };

        $component_actions->{$component} = $component_action_pack;
    } ## end PARSE_JSON: foreach my $json_structure ...

    $self->debug( { '-text' => 'Have -component_actions ' . $self->dbg_value($component_actions) } );

    #   actions
    $self->actions($component_actions);

    return 1;
} ## end sub parse_input_params

#   debug input params
sub debug_input_params {
    my ( $self, $args ) = @_;

    my $actions = $self->actions();
    if ( !defined $actions ) {
        $self->debug( { '-text' => 'No action parameters to debug' } );
        return;
    }

    foreach my $p_key ( keys %{$actions} ) {
        $self->debug( { '-text' => 'Param -key ' . $p_key . ' -value ' . $self->dbg_value( $actions->{$p_key} ) } );
    }

    return;
}

#   json get content type
sub get_content_type {
    my ( $self, $args ) = @_;

    return 'application/json';
}

#   check interface render
sub check_interface_render {
    my ( $self, $args ) = @_;

    return 1;
}

#   render method
sub render {
    my ( $self, $args ) = @_;

    #   error util
    my $error_util = $self->utils->{ UTIL_ERROR() };

    #   actions
    my $actions = $args->{'-actions'};

    my $ret = '';
    my $data;

    #   program errors
    if ( $self->has_error() ) {
        my $program_errors = $error_util->get_errors();

        if ( defined $program_errors && scalar @{$program_errors} ) {

            push @{ $data->{'-errors'} }, @{$program_errors};
        }

        #   status
        $data->{'status'} = 0;
    }

    #   component data
    else {

        #   this really only supports returning data
        #   and a status for a single component

    PROCESS_COMPONENT_JSON:
        foreach my $component ( values %{ $self->comps } ) {
            if ( !defined $actions->{ $component->name } ) {
                $self->debug( { '-text' => 'Component -name ' . $component->name . ' handled no action, not returning data' } );
                next PROCESS_COMPONENT_JSON;
            }

            $data = $component->render($args);
            $self->debug( { '-text' => 'Component -name ' . $component->name . ' returned -data ' . $self->dbg_value($data) } );
            if ( defined $data && ref($data) ne 'HASH' ) {
                $data = undef;
            }

            #   add component name
            $data->{'component'} = $component->name;

            #   add status
            my $status = $actions->{ $component->name }->{'-status'};
            if ( !defined $status ) {
                $status = 0;
            }
            $data->{'status'} = $status;

            #   add action
            my $action = $actions->{ $component->name }->{'-action'};
            if ( !defined $action ) {
                $action = '';
            }
            $data->{'action'} = $action;

            #   component context errors
            my $component_errors = $error_util->get_errors( { '-value' => $component->name, } );

            if ( defined $component_errors && scalar @{$component_errors} ) {
                push @{ $data->{'-errors'} }, $component_errors;
            }

            last PROCESS_COMPONENT_JSON;

        } ## end PROCESS_COMPONENT_JSON: foreach my $component ( values...)
    } ## end else [ if ( $self->has_error(...))]

    #   convert to json
    $ret .= $self->render_json( { '-data' => $data } );

    return $ret;
} ## end sub render

#   convert hashref structure to a json string
sub render_json {
    my ( $self, $args ) = @_;

    my $data = $args->{'-data'};

    if ( defined $data ) {
        $data = $self->unprefix_keys($data);

        my $success = eval {
            $data = to_json($data);
            1;
        };
        if ( !$success ) {
            $self->error(
                { '-text' => 'Failed to convert return -data ' . $self->dbg_value($data) . ' to JSON string :' . $EVAL_ERROR } );
            $data = undef;
        }
    }

    return $data;
}

#
#   opposite of init
#
#   all util objects may implement a finish method
#
sub finish {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::finish($args) ) {
        $self->debug( { '-text' => 'Finish failed in super' } );
        return;
    }

    return 1;
}

1;

