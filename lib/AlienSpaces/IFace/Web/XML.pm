package AlienSpaces::IFace::Web::XML;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:util :loader :program);

our @DYNISA = qw(IFace::Web);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {

    'status'       => { '-type' => 's' },    #   Apache2::Const return value
    'redirect_url' => { '-type' => 's' },

    'comps' => { '-type' => 'h' },           # hash of APP_NAMEPSPACE::Comp::XX objects for current program

    'time'    => { '-type' => 's' },
    'user_id' => { '-type' => 's' },

    'finished' => { '-type' => 's', 'dflt' => 0 },
};

use Apache2::Const qw(OK REDIRECT);
use Time::HiRes qw(gettimeofday tv_interval);

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        UTIL_DB_MYSQL,    # default mysql database handle
        UTIL_SESSION,     # session data interface
        UTIL_USER,        # user account data interface
        UTIL_PHRASE,      # phrase data interface
    ];

    return $util_list;
}

#   util object clear list
sub util_clear_list {
    my ( $self, $args ) = @_;

    my $clear_list = [ @{ $self->SUPER::util_clear_list }, ];
    return $clear_list;
}

sub util_new_args {
    my ( $self, $args ) = @_;

    #   super args
    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = {};
    if ( defined $args->{'-class'} ) {

    }
    return $util_args;
}

sub load_utils {
    my ( $self, $args ) = @_;

    return $self->SUPER::load_utils( { '-scope' => 'global', } );
}

#   init
sub init {
    my ( $self, $args ) = @_;

    #   SUPER init
    if ( !$self->SUPER::init($args) ) {
        $self->debug( { '-text' => 'Failed SUPER init' } );
        return;
    }

    $self->debug( { '-text' => 'Init' } );

    #   init user session
    $self->init_user_session();

    #   init user language
    $self->init_user_language();

    #   load comps
    $self->load_comps();

    return 1;
}

#   user authentication
sub authorize_user {
    my ( $self, $args ) = @_;

    return $self->AUTHORIZE_FAILURE;
}

sub authorize_redirect_url {
    my ( $self, $args ) = @_;

    return;
}

#
#   user account verification
#
sub verify_user_account {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $user_id = $session_util->get_session( { '-key' => '-user_id' } );

    if ( defined $user_id ) {
        $self->debug( { '-text' => 'Have session -user_id ' . $user_id } );

        my $logged_in = $session_util->get_session( { '-key' => '-logged_in' } );

        if ($logged_in) {
            $self->debug( { '-text' => 'Verifying user login' } );

            if ( $self->account->verify_user_login( { '-user_id' => $user_id } ) ) {
                $self->debug( { '-text' => 'Verify user login passed' } );
                return 1;
            }
            else {
                $self->debug( { '-text' => 'Verify user login failed' } );
                $session_util->set_session( { '-key' => '-logged_in', '-value' => 0 } );
                return;
            }
        }
    }

    $self->debug( { '-text' => 'User not logged in' } );

    return 1;
} ## end sub verify_user_account

sub verify_redirect_url {
    my ( $self, $args ) = @_;

    return;
}

sub debug_param {
    my ( $self, $args ) = @_;

    return;
}

sub main {
    my ( $self, $args ) = @_;

    return;
}

sub comp_list {
    my $self = shift;

    return [];
}

sub comp_init_args {
    my $self = shift;

    return {};
}

#   panel methods
sub load_comps {
    my ( $self, $args ) = @_;

    #
    #   instantiate program comps
    #
    my $comps = {};

    foreach my $comp_name ( @{ $self->comp_list() } ) {
        my ( $panel_file, $comp_class ) = $self->make_class(
            {   '-name' => $comp_name,
                '-type' => CLASS_TYPE_COMP,
            } );

        if ( defined $comp_class ) {

            $self->debug( { '-text' => 'Adding panel -class ' . $comp_class } );

            require $panel_file;

            my $panel;
            my $ret = eval {

                #   panel
                $panel = $comp_class->new(
                    {   '-name' => $comp_name,
                        %{ $self->comp_new_args( { '-class' => $comp_class } ) },
                    } );

                $comps->{$comp_name} = $panel;
                $comps->{$comp_name}->init();
                1;
            };
            if ($ret) {
                $self->debug( { '-text' => 'Added panel -name ' . $comp_name } );
            }
            else {
                $self->debug(
                    { '-text' => 'Failed to add panel -name ' . $comp_name . ' -class ' . $comp_class . ' :' . $EVAL_ERROR } );
            }

        }
        else {
            $self->debug( { '-text' => 'Could not make panel class for panel -name ' . $comp_name } );
        }
    } ## end foreach my $comp_name ( @{ ...})
    if ( scalar( keys %{$comps} ) ) {
        $self->comps($comps);
    }

    return;
} ## end sub load_comps

sub comp_new_args {
    my ( $self, $args ) = @_;
    if ( !$self->check_arguments( ['-class'], $args ) ) {
        return;
    }
    return {
        '-environment'    => $self->environment,
        '-program_name'   => $self->program_name,
        '-apache_request' => $self->apache_request,
    };
}

#   render method
sub render {
    my ( $self, $args ) = @_;

    return;
}

#
#   opposite of init
#
#   all util objects may implement a finish method
#
sub finish {
    my ( $self, $args ) = @_;

    $self->finished(1);

    return 1;
}

#
#   cleanup methods
#
sub cleanup {
    my ( $self, $args ) = @_;

    #
    #   if we have never been finished finish us off
    #
    if ( !$self->finished() ) {
        $self->debug( { '-text' => 'Finishing myself off ' . ref($self) } );
        $self->finish();
    }

    #   profile end
    $self->profile_end( { '-tag' => 'Program' } );

    #   profile summary
    $self->profile_summary();

    #   db mysql
    my $db_mysql_util = $self->utils->{UTIL_DB_MYSQL};
    if ( defined $db_mysql_util ) {
        $self->debug( { '-text' => 'Committing database' } );
        $db_mysql_util->commit();
    }

    return 1;
}

sub DESTROY {
    my $self = $_[0];

    $self->cleanup();

    return;
}

1;

