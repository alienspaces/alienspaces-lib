package AlienSpaces::IFace::Web::HTML::Site;

#
#   Purpose:
#
#   This is the base configuration class for all Alienspaces screens on
#   the alienspaces.net application.
#
#   Do not sub-class this class with your own HTML::Site.pm class, instead
#   you should sub-class HTML.pm
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth);

our @DYNISA = qw(IFace::Web::HTML);

our $VERSION = (qw$Revision: $)[1];

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super init failed' } );
        return;
    }

    push @{ $self->css }, (

        # YUI
        '/yui_3.17/build/cssreset/cssreset-min.css',
        '/yui_3.17/build/cssfonts/cssfonts-min.css',
        '/yui_3.17/build/cssbase/cssbase-min.css',
        '/yui_3.17/build/cssgrids/cssgrids-min.css',
    );

    push @{ $self->js }, (

        # YUI
        '/yui_3.17/build/yui/yui-min.js',
    );

    return 1;
} ## end sub init

#   comp list
sub comp_list {
    my $self = shift;

    return [
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_SITE_LOGO(),
        COMP_HTML_SITE_USER(),
        COMP_HTML_SITE_ABOUT(),
        COMP_HTML_SITE_MESSAGE(),
        COMP_HTML_SITE_NAVIGATION(),
        COMP_HTML_SITE_PROMOTION(),
        COMP_HTML_SITE_FOOTER(),

    ];
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    #   header
    $section_config->{ SECTION_HEADER() }{'components'} = [
        
        #   Site::Logo
        COMP_HTML_SITE_LOGO(),
    ];

    #   primary
    $section_config->{ SECTION_PRIMARY() }{'components'} = [

        #   Site::Message
        COMP_HTML_SITE_MESSAGE(),
    ];

    #   secondary
    $section_config->{ SECTION_SECONDARY() }{'components'} = [

        #   Site::Navigation
        COMP_HTML_SITE_NAVIGATION(),
    ];

    #   tertiary
    $section_config->{ SECTION_TERTIARY() }{'components'} = [

        #   Site::Promotion
        COMP_HTML_SITE_PROMOTION(),
    ];

    #   footer
    $section_config->{ SECTION_FOOTER() }{'components'} = [
        
        #   Site::Footer
        COMP_HTML_SITE_FOOTER()
    ];

    return $section_config;
} ## end sub section_config

#   document description
sub document_description {
    my ( $self, $args ) = @_;

    return 'Web application design and development. Business systems, eCommerce, education and entertainment';
}

#   document keywords
sub document_keywords {
    my ( $self, $args ) = @_;

    return
        'business, games, puzzles, education, entertainment, client development, systems development, web application, web standards, internet, perl, javascript, CSS, HTML, apache, mod_perl, wc3, mvc, application development, web site development, Linux, systems developer, systems analysis, mysql, oracle, PLSQL, SQL, internet application architecture, internet systems architecture, custom code, custom programming, custom development';
}

#   document title
sub document_title {
    my ( $self, $args ) = @_;

    return 'Alienspaces - Web Development';
}

#   render header script
sub render_header_script {
    my ( $self, $args ) = @_;

    my $content = $self->google_analytics();

    return $content;
}

#   google analytics
sub google_analytics {
    my ( $self, $args ) = @_;

    #    var _gaq = _gaq || [];
    #    _gaq.push(['_setAccount', 'UA-35712854-1']);
    #    _gaq.push(['_setDomainName', 'alienspaces.net']);
    #    _gaq.push(['_trackPageview']);

    #    (function() {
    #        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    #        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    #        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    #    })();

    my $ret = <<'ANALYTICS';

ANALYTICS

    return $ret;
}

1;

__END__

