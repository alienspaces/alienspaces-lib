package AlienSpaces::IFace::Web::HTML::Site::Business::Account::Profile::Add;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth :task);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1]; 

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ 
        
        #   super
        @{ $self->SUPER::util_list }, 
        
        #   local
        UTIL_BUSINESS(), 
    ];

    return $utility_list;
}

#   comp list
sub comp_list {
    my $self = shift;

    return [

        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_BUSINESS_ACCOUNT_PROFILE_ADD(),
    ];
}

#   authorize task
sub authorize_task {
    my ($self, $args) = @_;
    
    return TASK_USER_ACCOUNT;
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    push @{$section_config->{ SECTION_PRIMARY() }{'components'}}, (
    
        #   Business::Account::Profile::Update::Add
        COMP_HTML_BUSINESS_ACCOUNT_PROFILE_ADD(), 
    );

    return $section_config;
}

1;

__END__

