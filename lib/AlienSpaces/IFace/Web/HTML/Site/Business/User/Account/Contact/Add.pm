package AlienSpaces::IFace::Web::HTML::Site::Business::User::Account::Contact::Add;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth :task);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_BUSINESS(),
        UTIL_USER(),
        UTIL_PHRASE(),
    ];

    return $utility_list;
}

#   comp list
sub comp_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_SITE_NAVIGATION_BREADCRUMB(),
        COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT_ADD(),
    ];
}

#   comp config data
sub comp_config_data {
    my ($self, $args) = @_;
    
    my $component = $args->{'-component'};
    my $config_data = $args->{'-config_data'};

    #   phrase util 
    my $phrase_util = $self->utils->{UTIL_PHRASE()};
    
    #   Site::Navigation::BreadCrumb
    if ($component eq COMP_HTML_SITE_NAVIGATION_BREADCRUMB()) {

        $config_data->{'-links'} = [

            #   site
            {   '-label_text' => $phrase_util->text({'-phrase_id' => 96_400}),  #   phrase id 96400 - Business
                '-text_only'  => 1,
            },
            {   '-name'    => $phrase_util->text({'-phrase_id' => 96_410}), #   phrase id 96410 - User
                '-program' => PROGRAM_SITE_BUSINESS_USER(),
            },
            {   '-name'    => $phrase_util->text({'-phrase_id' => 96_430}), #   phrase id 96430 - Account
                '-program' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT(),
            },
            {   '-name'    => $phrase_util->text({'-phrase_id' => 96_450}), #   phrase id 96450 - Add Contact
                '-program' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT_CONTACT_ADD(),
            },
        ];
    }

    return $config_data;
}

#   authorize task
sub authorize_task {
    my ($self, $args) = @_;
    
    return TASK_BUSINESS_USER;
}
    
#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    push @{$section_config->{ SECTION_PRIMARY() }{'components'}}, (
    
        #   Site::Navigation::BreadCrumb
        COMP_HTML_SITE_NAVIGATION_BREADCRUMB(),
    
        #   Business::User::Account::Contact::Add
        COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT_ADD(),
    );
    
    return $section_config;
}

1;

__END__
