package AlienSpaces::IFace::Web::HTML::Site::Policy;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:comp :program :util);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1];

#
#   panel list
#
sub comp_list {
    my $self = shift;

    return [

        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_SITE_POLICY_PRIVACY(),
        COMP_HTML_SITE_POLICY_TERMS_CONDITIONS(),
        COMP_HTML_SITE_POLICY_COPYRIGHT(),
        COMP_HTML_SITE_POLICY_SECURITY(),
        COMP_HTML_SITE_POLICY_CONTACT(),
    ];
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    #   primary
    push @{ $section_config->{ SECTION_PRIMARY() }{'components'} }, (

        COMP_HTML_SITE_POLICY_PRIVACY(),
        COMP_HTML_SITE_POLICY_TERMS_CONDITIONS(),
        COMP_HTML_SITE_POLICY_SECURITY(),
        COMP_HTML_SITE_POLICY_COPYRIGHT(),
        COMP_HTML_SITE_POLICY_CONTACT(),
    );

    return $section_config;
}

1;

__END__
