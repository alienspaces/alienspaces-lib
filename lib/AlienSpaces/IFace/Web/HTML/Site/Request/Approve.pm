package AlienSpaces::IFace::Web::HTML::Site::Request::Approve;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:comp :program :util :section);
 
our @DYNISA = qw(IFace::Web::HTML::Site);
  
our $VERSION = (qw$Revision: $)[1];

#   utils
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ @{ $self->SUPER::util_list }, UTIL_USER(), ];

    return $utility_list;
}

#   comps
sub comp_list {
    my ( $self, $args ) = @_;

    return [ 
        #   super
        @{ $self->SUPER::comp_list }, 
        
        #   local
        COMP_HTML_REQUEST_APPROVE(), ];
}

#   default action config
sub default_action_config {

    my $default_action_config = [

        {   '-component' => COMP_HTML_REQUEST_APPROVE(),
            '-action'    => 'approve',
        },
    ];

    return $default_action_config;
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);
    
    #   primary
    push @{$section_config->{ SECTION_PRIMARY() }{'components'}}, (
        
        #   Request::Approve
        COMP_HTML_REQUEST_APPROVE(),
    );

    return $section_config;
}

1;

__END__
