package AlienSpaces::IFace::Web::HTML::Site::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:comp :program :util :section);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [

        #   super
        @{ $self->SUPER::util_list },
    ];

    return $utility_list;
}

#   comp list
sub comp_list {
    my $self = shift;

    return [

        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_SITE_CONTACT(),
    ];
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    #   primary
    push @{ $section_config->{ SECTION_PRIMARY() }{'components'} }, (

        #   Site::Contact
        COMP_HTML_SITE_CONTACT(),
    );

    return $section_config;
}

1;

__END__
