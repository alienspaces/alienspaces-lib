package AlienSpaces::IFace::Web::HTML::Site::User::Logout;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth :task);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [

        #   super
        @{ $self->SUPER::util_list },
    ];

    return $utility_list;
}

#   comp list
sub comp_list {
    my $self = shift;

    return [

        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_USER_LOGOUT(),
    ];
}

#   authorize task
sub authorize_task {
    my ( $self, $args ) = @_;

    return TASK_USER_ACCOUNT;
}

#   default action config
sub default_action_config {

    my $default_action_config = [

        {   '-component' => COMP_HTML_USER_LOGOUT(),
            '-action'    => 'logout',
        },
    ];

    return $default_action_config;
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    push @{$section_config->{ SECTION_PRIMARY() }{'components'}}, (
            
        #   User::Logout
        COMP_HTML_USER_LOGOUT(),
    );

    return $section_config;
}

1;

__END__

