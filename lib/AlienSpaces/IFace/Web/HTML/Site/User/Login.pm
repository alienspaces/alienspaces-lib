package AlienSpaces::IFace::Web::HTML::Site::User::Login;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth :task);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
    ];

    return $utility_list;
}

#   comp list
sub comp_list {
    my $self = shift;

    return [

        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_USER_LOGIN(),
    ];
}

#   authorize user
#   - if logged in no access is granted
sub authorize_user {
    my ( $self, $args ) = @_;

    my $user_util = $self->utils->{ UTIL_USER() };

    if ( $user_util->check_user_logged_in() ) {
        $self->debug( { '-text' => 'User logged in, no access allowed' } );
        return AUTHORIZE_FAILURE;
    }

    return $self->SUPER::authorize_user($args);
}

#   authorize task
sub authorize_task {
    my ( $self, $args ) = @_;

    return TASK_PUBLIC;
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    push @{ $section_config->{ SECTION_PRIMARY() }{'components'} }, (

        #   User::Login
        COMP_HTML_USER_LOGIN(),
    );

    return $section_config;
}

1;

__END__

