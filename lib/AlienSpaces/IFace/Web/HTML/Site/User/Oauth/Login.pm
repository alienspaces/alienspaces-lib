package AlienSpaces::IFace::Web::HTML::Site::User::Oauth::Login;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1]; 

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ 
        
        #   super
        @{ $self->SUPER::util_list }, 
        
        #   local
        UTIL_USER(), 
    ];

    return $utility_list;
}

#   comp list
sub comp_list {
    my $self = shift;

    return [ 
        
        #   super
        @{ $self->SUPER::comp_list }, 
        
        #   local
        COMP_HTML_USER_OAUTH_LOGIN(), 
    ];
}

#   default action config
sub default_action_config {

    my $default_action_config = [

        {   '-component' => COMP_HTML_USER_OAUTH_LOGIN(),
            '-action'    => 'login',
        },
    ];

    return $default_action_config;
}

1;

__END__
