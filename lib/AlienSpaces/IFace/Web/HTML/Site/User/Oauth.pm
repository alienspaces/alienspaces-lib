package AlienSpaces::IFace::Web::HTML::Site::User::Oauth;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:comp :program :util :auth);
 
our @DYNISA = qw(IFace::Web::HTML::Site);
  
our $VERSION = (qw$Revision: $)[1];

1;
