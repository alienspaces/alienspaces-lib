package AlienSpaces::IFace::Web::HTML::Site::User::Account::Password::Reset::Deny;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1];

#   utils
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
    ];

    return $utility_list;
}

#   html comp list
sub comp_list {
    my $self = shift;

    return [
        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_USER_ACCOUNT_PASSWORD_RESET_DENY(),
    ];
}

#   default action config
sub default_action_config {

    my $default_action_config = [

        {   '-component' => COMP_HTML_USER_ACCOUNT_PASSWORD_RESET_DENY(),
            '-action'    => 'deny',
        },
    ];

    return $default_action_config;
}

#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    push @{$section_config->{ SECTION_PRIMARY() }{'components'}}, (
        
        #   User::Account::Password::Reset::Deny
        COMP_HTML_USER_ACCOUNT_PASSWORD_RESET_DENY(),
    );

    return $section_config;
}

1;

