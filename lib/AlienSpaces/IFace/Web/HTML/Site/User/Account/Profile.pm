package AlienSpaces::IFace::Web::HTML::Site::User::Account::Profile;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:comp :program :util :section :auth :task);

our @DYNISA = qw(IFace::Web::HTML::Site);

our $VERSION = (qw$Revision: $)[1]; 

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ 
        
        #   super
        @{ $self->SUPER::util_list }, 
        
        #   local
        UTIL_USER(), 
        UTIL_PHRASE(),
    ];

    return $utility_list;
}

#   comp list
sub comp_list {
    my $self = shift;

    return [

        #   super
        @{ $self->SUPER::comp_list },

        #   local
        COMP_HTML_SITE_NAVIGATION_BREADCRUMB(),
        COMP_HTML_USER_ACCOUNT_PROFILE_UPDATE(),
    ];
}

#   comp config data
sub comp_config_data {
    my ($self, $args) = @_;
    
    my $component = $args->{'-component'};
    my $config_data = $args->{'-config_data'};

    #   phrase util 
    my $phrase_util = $self->utils->{UTIL_PHRASE()};
    
    #   Site::Navigation::BreadCrumb
    if ($component eq COMP_HTML_SITE_NAVIGATION_BREADCRUMB()) {

        $config_data->{'-links'} = [

            #   site
            {   '-label_text' => $phrase_util->text({'-phrase_id' => 96_000}),  #   phrase id 96000 - User
                '-text_only'  => 1,
            },
            {   '-name'    => $phrase_util->text({'-phrase_id' => 96_010}), #   phrase id 96010 - Account
                '-program' => PROGRAM_SITE_USER_ACCOUNT(),
            },
            {   '-name'    => $phrase_util->text({'-phrase_id' => 96_020}), #   phrase id 96020 - Profile
                '-program' => PROGRAM_SITE_USER_ACCOUNT_PROFILE(),
            },
        ];
    }

    return $config_data;
}

#   authorize task
sub authorize_task {
    my ( $self, $args ) = @_;
    
    return TASK_USER_ACCOUNT;
}
        
#   section config
sub section_config {
    my ( $self, $args ) = @_;

    #   super
    my $section_config = $self->SUPER::section_config($args);

    push @{$section_config->{ SECTION_PRIMARY() }{'components'}}, (

        #   Site::Navigation::BreadCrumb
        COMP_HTML_SITE_NAVIGATION_BREADCRUMB(),

        #   User::Account::Profile::Update
        COMP_HTML_USER_ACCOUNT_PROFILE_UPDATE(), 
    );

    return $section_config;
}

1;

__END__

