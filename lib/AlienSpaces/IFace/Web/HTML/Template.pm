package AlienSpaces::IFace::Web::HTML::Test;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :comp);

our @DYNISA = qw(IFace::Web::HTML);

our $VERSION = (qw$Revision: $)[1];

sub init {
    my ( $self, $args ) = @_;

    push @{ $self->css }, (

        # test
        'test.css',
    );

    push @{ $self->js }, (

        # test
        'test.js',
    );

    return $self->SUPER::init($args);
}

#
#   user authentication
#
sub authorize_user {
    my ( $self, $args ) = @_;

    return $self->AUTHORIZE_SUCCESS;
}

#
#   panel list
#
sub comp_list {
    my $self = shift;

    return [ @{ $self->SUPER::comp_list }, COMP_TEST(), ];
}

#   render header
sub render_header {
    my ( $self, $args ) = @_;

    return 'test header' . "\n";
}

#   render content
sub render_content {
    my ( $self, $args ) = @_;
    my $ret = '';

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   standard alienspaces content layout
    $ret .= 
        $self->comps->{ COMP_TEST() }->render($args) . "\n";

    return $ret;
}

#   render footer
sub render_footer {
    my ( $self, $args ) = @_;

    return 'test footer' . "\n";
}

#   document description, keywords, title
sub document_description {
    my ( $self, $args ) = @_;

    return 'test description';
}

sub document_keywords {
    my ( $self, $args ) = @_;

    return 'test keywords';
}

sub document_title {
    my ( $self, $args ) = @_;

    return 'test page title';
}

1;
