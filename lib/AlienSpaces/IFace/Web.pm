package AlienSpaces::IFace::Web;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use Apache2::Const qw(OK REDIRECT);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace);

use AlienSpaces::Constants qw(:util :loader :comp :task :auth :language);

#   shared properties for all IFace::Web interfaces
use AlienSpaces::Properties {

    #   Apache2::Const return value
    'status' => { '-type' => 's', '-default' => undef },

    #   whether a redirect was issued
    'authorized' => { '-type' => 's', '-default' => 0 },

    # hash of APP_NAMEPSPACE::Comp::XX objects for current program
    'comps' => { '-type' => 'h', '-default' => {} },

    # hash of APP_NAMEPSPACE::Comp::XX objects for current program
    'actions' => { '-type' => 'h', '-default' => {} },

    'time'    => { '-type' => 's', '-default' => undef },
    'user_id' => { '-type' => 's', '-default' => undef },

    #   url to redirect to
    'redirect_url' => { '-type' => 's', '-default' => undef },

    'finished' => { '-type' => 's', '-default' => 0 },
};

#   init
sub init {
    my ( $self, $args ) = @_;

    #   clear comps
    $self->comps( {} );

    #   clear actions
    $self->actions( {} );

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Init failed in super' } );
        return;
    }

    $self->debug( { '-text' => 'Init' } );

    return 1;
}

#   init database connection
sub init_database_connection {
    my ( $self, $args ) = @_;

    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };
    if ( !defined $db_mysql_util ) {
        $self->error( { '-text' => 'Missing database utility, cannot init database connection' } );
        return;
    }

    #   username
    my $username = $self->environment->{'APP_WEB_DB_USERNAME'};
    if ( !defined $username ) {
        $self->error( { '-text' => 'Missing username, cannot init database connection' } );
        return;
    }

    #   password
    my $password = $self->environment->{'APP_WEB_DB_PASSWORD'};
    if ( !defined $password ) {
        $self->error( { '-text' => 'Missing password, cannot init database connection' } );
        return;
    }

    #   database
    my $database = $self->environment->{'APP_WEB_DB'};
    if ( !defined $database ) {
        $self->error( { '-text' => 'Missing database, cannot init database connection' } );
        return;
    }

    if (!$db_mysql_util->init_connection(
            {   '-username' => $username,
                '-password' => $password,
                '-database' => $database,
            } )
        ) {
        $self->error( { '-text' => 'Failed to init connection' } );
        return;
    }

    return 1;
} ## end sub init_database_connection

#   init user session
sub init_user_session {
    my ( $self, $args ) = @_;

    my $session_util = $self->utils->{ UTIL_SESSION() };
    if ( !defined $session_util ) {
        $self->error( { '-text' => 'Missing session util, cannot init user session' } );
        return;
    }

    if ( !$session_util->init_user_session() ) {
        $self->error( { '-text' => 'Failied to init user session' } );
        return;
    }

    return 1;
}

#   init user language
#   TODO: this needs to change, wont work with correct language support
sub init_user_language {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $language = $cgi_util->param( '-name' => 'language' );

    if ( defined $language ) {
        $session_util->set_session( { '-key' => '-language', '-value' => $language } );
    }
    else {
        $language = $session_util->get_session( { '-key' => '-language' } );
        if ( !defined $language ) {
            $session_util->set_session(
                {   '-key'   => '-language',
                    '-value' => LANGUAGE_AMERICAN,
                } );
            $language = LANGUAGE_AMERICAN;
        }
    }
    return $language;
}

#   shared util object load list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },
        UTIL_DB_MYSQL(),    # default mysql database handle
        UTIL_SESSION(),     # session data interface
        UTIL_USER(),        # user account data interface
        UTIL_PHRASE(),      # phrase data interface
        UTIL_MESSAGE(),     # standard messaging interface
        UTIL_ERROR(),       # error messaging interface
    ];

    return $util_list;
}

#   shared util object clear list
sub util_clear_list {
    my ( $self, $args ) = @_;

    my $clear_list = [ @{ $self->SUPER::util_clear_list }, UTIL_SESSION(), ];

    return $clear_list;
}

#   shared util new args
sub util_new_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   shared util init args
sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    #   global
    $util_args->{'-program_name'} = $self->program_name;

    #   specific
    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

#   shared load utils
sub load_utils {
    my ( $self, $args ) = @_;

    return $self->SUPER::load_utils( { '-scope' => 'global', } );
}

#   shared comp list
sub comp_list {
    my $self = shift;

    return [];
}

#   shared comp new args
sub comp_new_args {
    my ( $self, $args ) = @_;

    return {
        '-environment'          => $self->environment,
        '-program_name'         => $self->program_name,
        '-referer_program_name' => $self->referer_program_name,
        '-apache_request'       => $self->apache_request,
        '-interface'            => $self->interface,
    };
}

#   shared comp init args
sub comp_init_args {
    my $self = shift;

    return {};
}

#   compat config data
#   - method called to retrieve additional component data
sub comp_config_data {
    my ( $self, $args ) = @_;

    return {};
}

#   shared load comps
sub load_comps {
    my ( $self, $args ) = @_;

    #
    #   instantiate program comps
    #
    my $comps = {};

    foreach my $comp_name ( @{ $self->comp_list() } ) {
        my ( $component_file, $comp_class ) = $self->make_class(
            {   '-name'      => $comp_name,
                '-type'      => CLASS_TYPE_COMP,
                '-interface' => $self->interface,
            } );

        if ( !defined $comp_class ) {
            $self->fatal( { '-text' => 'Could not make comp class for component -name ' . $comp_name } );
        }

        $self->debug( { '-text' => 'Adding comp -class ' . $comp_class } );

        my $component;
        my $success = eval {

            #   load
            $self->load_class( { '-class_name' => $comp_class } );

            #   component
            $component = $comp_class->new(
                {   '-name' => $comp_name,
                    %{ $self->comp_new_args( { '-class' => $comp_class } ) },
                } );

            $comps->{$comp_name} = $component;
            $comps->{$comp_name}->init( $self->comp_init_args( { '-class' => $comp_class } ) );
            1;
        };
        if ( !$success ) {
            $self->fatal( { '-text' => 'Failed to add comp -name ' . $comp_name . ' -class ' . $comp_class . ' :' . $EVAL_ERROR } );
        }

        $self->debug( { '-text' => 'Added comp -name ' . $comp_name } );

    } ## end foreach my $comp_name ( @{ ...})

    if ( scalar( keys %{$comps} ) ) {
        $self->comps($comps);
    }

    return 1;
} ## end sub load_comps

#   check interface authorize user
sub check_interface_authorize_user {
    my ( $self, $args ) = @_;

    return 1;
}

#   authorize user
sub authorize_user {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };
    if ( !defined $user_util ) {
        $self->error( { '-text' => 'Missing user util, cannot authorize user' } );
        return AUTHORIZE_FAILURE;
    }

    #   task id
    #   - if defined user must have this task
    my $task_id = $self->get_program_authorize_task();
    if ( !defined $task_id ) {
        $self->error( { '-text' => 'Unauthorized -program ' . $self->program() . ' missing tasks' } );
        return AUTHORIZE_FAILURE;
    }

    $self->debug( { '-text' => 'Checking -task_id ' . $self->dbg_value($task_id) } );

    #   public
    if ( $task_id == TASK_PUBLIC ) {
        $self->debug( { '-text' => 'Program has public access' } );
        return AUTHORIZE_SUCCESS;
    }

    $self->debug( { '-text' => 'Program does not have public access' } );

    if ( !$user_util->check_user_logged_in() ) {
        $self->info( { '-text' => 'User is not logged in, authorize failure' } );
        return AUTHORIZE_FAILURE;
    }

    $self->debug( { '-text' => 'User is logged in, authorize success' } );

    if ( !$user_util->check_user_task( { '-task_id' => $task_id } ) ) {
        $self->info( { '-text' => 'User does not have -task_id ' . $self->dbg_value($task_id) . ', authorize failure' } );
        return AUTHORIZE_FAILURE;
    }

    $self->debug( { '-text' => 'User has -task_id ' . $self->dbg_value($task_id) . ', authorize success' } );

    return AUTHORIZE_SUCCESS;
} ## end sub authorize_user

#   authorize task
#   - when defined current user must have this task
#   for successful authorization
sub authorize_task {
    my ( $self, $args ) = @_;

    return TASK_PUBLIC;
}

#   check interface render
sub check_interface_render {
    my ( $self, $args ) = @_;

    return 1;
}

#   default action config
sub default_action_config {

    my $default_action_config = [

        #        {
        #           '-component' => COMP_XXXX(),
        #           '-action'    => 'xxxx',
        #        },
    ];

    return $default_action_config;
}

#   shared main method
sub main {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Component actions start' } );

    #   actions
    my $actions = $self->actions();

    #   action
HANDLE_ACTIONS:
    while ( my ( $component_name, $component_object ) = each %{ $self->comps } ) {

        my $component_action = $actions->{$component_name};

        #   TODO - consider moving this logic to earlier
        if ( !defined $component_action ) {
            $component_action = {
                '-component'    => $component_name,
                '-interface'    => $self->interface,
                '-data'         => {},
                '-session_data' => {},
                '-config_data'  => {},
                '-status'       => COMP_ACTION_NONE,
            };
            $actions->{$component_name} = $component_action;
        }

        my $action    = $component_action->{'-action'};
        my $interface = $component_action->{'-interface'};
        my $component = $component_action->{'-component'};
        my $data      = $component_action->{'-data'};

        #   get comp session data
        my $session_data = $self->get_comp_session_data( { '-component' => $component } );
        $component_action->{'-session_data'} = $session_data;

        #   get comp config data
        my $config_data = $self->get_comp_config_data(
            {   '-action'       => $action,
                '-component'    => $component,
                '-data'         => $data,
                '-session_data' => $session_data,
                '-interface'    => $interface,
            } );

        $component_action->{'-config_data'} = $config_data;

        if ( defined $action ) {

            #   handle comp action
            my $action_status = $self->handle_comp_action(
                {   '-action'       => $action,
                    '-component'    => $component,
                    '-data'         => $data,
                    '-session_data' => $session_data,
                    '-config_data'  => $config_data,
                    '-interface'    => $interface,
                } );

            $component_action->{'-status'} = $action_status;

            #   set comp session data
            $self->set_comp_session_data( { '-component' => $component, '-session_data' => $session_data } );

            #   re-authorize
            if ( $action_status == COMP_ACTION_AUTHORIZE ) {

                #   check interface authorize user
                $self->check_interface_authorize_user( { '-actions' => $actions, } );
            }

            if ( $self->has_error ) {
                last HANDLE_ACTIONS;
            }
        }

    } ## end HANDLE_ACTIONS: while ( my ( $component_name...))

    return 1;
} ## end sub main

#   get comp config data
sub get_comp_config_data {
    my ( $self, $args ) = @_;

    my $action       = $args->{'-action'};
    my $interface    = $args->{'-interface'};
    my $component    = $args->{'-component'};
    my $data         = $args->{'-data'};
    my $session_data = $args->{'-session_data'};
    my $config_data  = $args->{'-config_data'} || {};

    $self->debug(
        { '-text' => 'Fetching additional data for ' . $self->dbg_value($action) . ' to component -name ' . $component } );

    #   TODO - if we were to introduce file system based component
    #   configuration this is where we would load it prior to
    #   calling the iface comp_config_data method passing what we
    #   have. File system config would be implemented in JSON.

    $config_data = $self->comp_config_data(
        {   '-component'    => $component,
            '-action'       => $action,
            '-data'         => $data,
            '-session_data' => $session_data,
            '-config_data'  => $config_data,
            '-interface'    => $interface,
        }

    );

    return $config_data;
} ## end sub get_comp_config_data

#   get comp session data
#   - called from sub-class pinput parameters parsing routines
#     where the component action packs are created
#   - return component state stored session data
sub get_comp_session_data {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   component
    my $component = $args->{'-component'};

    #   session key
    my $session_key = $component . '::session_data';

    my $session_data = $session_util->get_session( { '-key' => $session_key } );
    if ( !defined $session_data ) {
        $session_data = {};
    }

    return $session_data;
}

#   set comp session data
#   - set component state stored session data
sub set_comp_session_data {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   component
    my $component = $args->{'-component'};

    #   session data
    my $session_data = $args->{'-session_data'};
    if ( !defined $session_data ) {
        $session_data = {};
    }

    #   session key
    my $session_key = $component . '::session_data';

    if ( !$session_util->set_session( { '-key' => $session_key, '-value' => $session_data } ) ) {
        $self->error( { '-text' => 'Failed setting form session data' } );
        return;
    }

    return 1;
}

#   main render
sub main_render {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    $self->debug( { '-text' => 'Render start' } );

    #   actions
    my $actions = $self->actions();

    #   render
    #   - render will vary depending on the sub-class
    my $content = $self->render( { '-actions' => $actions, } );

    #   apache
    if ( defined $self->apache_request ) {

        #   content type
        #   - content type will vary depending on the sub-class
        my $content_type = $self->get_content_type();

        #   set headers
        my $cookie_string = $session_util->get_cookie_string;

        if ( defined $cookie_string ) {
            $self->debug( { '-text' => 'Output -cookie_string ' . $cookie_string } );

            $self->apache_request->headers_out->add( 'Set-Cookie' => $cookie_string );
        }

        $self->apache_request->content_type($content_type);
    }

    return $content;
} ## end sub main_render

#   shared handle comp action
sub handle_comp_action {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-action', '-component' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $action       = $args->{'-action'};
    my $comp_name    = $args->{'-component'};
    my $data         = $args->{'-data'};
    my $session_data = $args->{'-session_data'};
    my $config_data  = $args->{'-config_data'};
    my $interface    = $args->{'-interface'};

    #   find component object
    my $component = $self->comps->{$comp_name};
    if ( !defined $component ) {
        $self->error( { '-text' => 'No such -component ' . $self->dbg_value($comp_name) } );
        return COMP_ACTION_FAILURE;
    }

    $self->debug( { '-text' => 'Sending action ' . $action . ' to component -name ' . $comp_name } );

    #   non consequential return status from implicit action handling
    my $implicit_return_status = $component->implicit_action(
        {   '-action'       => $action,
            '-data'         => $data,
            '-session_data' => $session_data,
            '-config_data'  => $config_data,
            '-interface'    => $interface,
        } );

    $self->debug( { '-text' => 'Return -implicit_return_status ' . $self->dbg_value($implicit_return_status) } );

    #   consequential return status from explicit action handling
    my $return_status = $component->action(
        {   '-action'       => $action,
            '-data'         => $data,
            '-session_data' => $session_data,
            '-config_data'  => $config_data,
            '-interface'    => $interface,
        } );
    if ( !defined $return_status ) {
        $return_status = COMP_ACTION_FAILURE;
    }

    $self->debug( { '-text' => 'Return -return_status ' . $return_status } );

    #   clear context
    $self->clear_context( { '-tag' => 'component', } );

    return $return_status;
} ## end sub handle_comp_action

#   get redirect url
sub get_redirect_url {
    my ( $self, $args ) = @_;

    #   redirect url
    my $redirect_url = $self->redirect_url();
    if ( defined $redirect_url ) {
        $self->debug( { '-text' => 'Interface redirecting -redirect_url ' . $self->dbg_value($redirect_url) } );
        return $redirect_url;
    }

    #   component redirect url
    foreach my $comp ( values %{ $self->comps } ) {
        $self->debug( { '-text' => 'Checking component -name ' . $comp->name } );
        $redirect_url = $comp->redirect_url();
        if ( defined $redirect_url ) {
            $self->debug(
                { '-text' => 'Component -name ' . $comp->name . ' redirecting -redirect_url ' . $self->dbg_value($redirect_url) } );
            return $redirect_url;
        }
    }

    return;
}

#   set redirect url
sub set_redirect_url {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-url'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   url
    my $url = $args->{'-url'};

    #   redirect url
    $self->redirect_url($url);

    return $url;
}

#   get session cookie string
sub get_session_cookie_string {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $cookie_string = $session_util->get_cookie_string;

    return $cookie_string;
}

#   get content type
sub get_content_type {
    my ( $self, $args ) = @_;

    return 'text/plain';
}

###########################################
#
#   finish
#   DESTROY
#
###########################################

#   finish
sub finish {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::finish($args) ) {
        $self->debug( { '-text' => 'Finish failed in super' } );
        return;
    }

    #   check for rollback needed
    my $actions = $self->actions();
    while ( my ( $component_name, $component_action ) = each %{$actions} ) {
        if ( !defined $component_action->{'-status'} ||
            $component_action->{'-status'} == COMP_ACTION_FAILURE ||
            $component_action->{'-status'} == COMP_ACTION_VALIDATE_FAILURE ) {
            $self->error(
                {   '-text' => 'Component -name ' .
                        $self->dbg_value($component_name) . ' returned failure, flagging rollback required'
                } );
            $args->{'-rollback'} = 1;
        }
    }

    my $rollback = $args->{'-rollback'};
    my $commit   = $args->{'-commit'};
    if ( !defined $commit ) {
        $commit = 1;
    }

    $self->debug( { '-text' => 'Finish -rollback ' . $self->dbg_value($rollback) . ' -commit ' . $self->dbg_value($commit) } );

    #   db mysql
    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };
    if ( defined $db_mysql_util ) {
        if ($rollback) {
            $self->debug( { '-text' => 'Rolling back database' } );
            $db_mysql_util->rollback();
        }
        elsif ($commit) {
            $self->debug( { '-text' => 'Committing database' } );
            $db_mysql_util->commit();
        }
    }

    $self->debug( { '-text' => 'Finish' } );

    return 1;
} ## end sub finish

sub DESTROY {
    my ( $self, $args ) = @_;

    if ($EVAL_ERROR) {
        carp 'Destroyed with error :' . $EVAL_ERROR;
    }

    #   cleanup
    if ( !$self->finished() ) {
        $self->finish( { '-rollback' => 1 } );
    }
    return;
}

1;

__END__

