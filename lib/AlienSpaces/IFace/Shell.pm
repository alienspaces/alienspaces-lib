package AlienSpaces::IFace::Shell;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use Getopt::Std;

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::IFace);

use AlienSpaces::Constants qw(:util);

use AlienSpaces::Properties {
    'options'  => { '-type' => 'h' },
    'finished' => { '-type' => 's', 'dflt' => 0 },
};

sub new {
    my ( $class, $args ) = @_;

    #   options
    my $options = delete $args->{'-options'};

    #   init environment
    #   - environment is initialised via a handler when running as a web interface
    $class->init_environment();

    #   init logging
    #   - logging is initialised via a handler when running as a web interface
    $class->init_logging();

    #   super new (does minimal stuff)
    my $self = $class->SUPER::new($args);
    if ( !defined $self ) {
        $self->error( { '-text' => 'Failed SUPER new' } );
        return;
    }

    #   init options
    $self->init_options( { '-options' => $options } );

    return $self;
} ## end sub new

sub init_options {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-options'], $args, { '-exists' => 1 } ) ) {
        return;
    }

    my $options = $args->{'-options'};

    #   options
    my $options_config = $self->options_config || 'h';
    if ( defined $options_config ) {
        $options_config .= 'h';
    }

    $self->debug( { '-text' => 'Fetching options ' . $options_config } );

    getopts( $options_config, $self->options );

    #   process passed in options
    if ( defined $options && ref($options) eq 'HASH' ) {
        foreach my $option_key ( keys %{$options} ) {

            #   option
            my $option = $option_key;
            $option =~ s/^\-//sxmg;

            #   option value
            my $option_value = $options->{$option_key};

            $self->debug( { '-text' => 'Setting -option ' . $option . ' -value ' . $option_value } );
            $self->options->{$option} = $option_value;
        }
    }

    if ( $self->options->{'h'} ) {
        $self->options_usage();
        exit(1);
    }

    if ( defined $self->options ) {
        foreach my $option ( keys %{ $self->options } ) {
            $self->debug(
                { '-text' => 'Option ' . $self->dbg_value($option) . ' -value ' . $self->dbg_value( $self->options->{$option} ) } );
        }
    }

    return 1;
} ## end sub init_options

#   options
sub options_config {
    return '';
}

sub options_usage {
    print <<'USAGE';
-h - show help
USAGE

    return;
}

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        UTIL_DB_MYSQL(),    # default mysql database handle
    ];

    return $util_list;
}

#   util object clear list
sub util_clear_list {
    my ( $self, $args ) = @_;

    my $clear_list = [ @{ $self->SUPER::util_clear_list }, ];

    return $clear_list;
}

sub util_new_args {
    my ( $self, $args ) = @_;

    #   super args
    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = {};
    if ( defined $args->{'-class'} ) {

    }
    return $util_args;
}

sub load_utils {
    my ( $self, $args ) = @_;

    return $self->SUPER::load_utils( { '-scope' => 'global', } );
}

#   init
sub init {
    my ( $self, $args ) = @_;

    #   SUPER init
    if ( !$self->SUPER::init($args) ) {
        $self->fatal( { '-text' => 'Failed SUPER init' } );
    }

    return 1;
}

#   init database connection
sub init_database_connection {
    my ( $self, $args ) = @_;

    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };
    if ( !defined $db_mysql_util ) {
        $self->error( { '-text' => 'Missing database utility, cannot init database connection' } );
        return;
    }

    #   username
    my $username = $self->environment->{'APP_SHELL_DB_USERNAME'};
    if ( !defined $username ) {
        $self->error( { '-text' => 'Missing username, cannot init database connection' } );
        return;
    }

    #   password
    my $password = $self->environment->{'APP_SHELL_DB_PASSWORD'};
    if ( !defined $password ) {
        $self->error( { '-text' => 'Missing password, cannot init database connection' } );
        return;
    }

    #   database
    my $database = $self->environment->{'APP_SHELL_DB'};
    if ( !defined $database ) {
        $self->error( { '-text' => 'Missing database, cannot init database connection' } );
        return;
    }

    if (!$db_mysql_util->init_connection(
            {   '-username' => $username,
                '-password' => $password,
                '-database' => $database,
            } )
        ) {
        $self->error( { '-text' => 'Failed to init connection' } );
        return;
    }

    return 1;
} ## end sub init_database_connection

#   dbh shortcut
sub dbh {
    my ( $self, $args ) = @_;

    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };

    return $db_mysql_util;
}

#   disabled auto commit
#   - disabling auto commit means you now must
#     explicity $self->dbh->commit with any
#     database changes
sub disable_auto_commit {
    my ( $self, $args ) = @_;

    #   auto commit
    $self->dbh->{'AutoCommit'} = 0;
    if ( $self->dbh->{'AutoCommit'} ) {
        $self->fatal( { '-text' => 'Failed to turn auto commit off' } );
        return;
    }

    return 1;
}

#   enable auto commit
#   - enabling auto commit means you do not
#     need to explicity $self->dbh->commit
#     with any database changes
sub enable_auto_commit {
    my ( $self, $args ) = @_;

    #   auto commit
    $self->dbh->{'AutoCommit'} = 1;
    if ( !$self->dbh->{'AutoCommit'} ) {
        $self->fatal( { '-text' => 'Failed to turn auto commit on' } );
        return;
    }

    $self->debug( { '-text' => 'Transaction comitted' } );

    return 1;
}

#   finish
sub finish {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::finish($args) ) {
        $self->debug( { '-text' => 'Finish failed in super' } );
        return;
    }

    my $rollback = $args->{'-rollback'};
    my $commit   = $args->{'-commit'};
    if ( !defined $commit ) {
        if ($rollback) {
            $commit = 0;
        }
        else {
            $commit = 1;
        }
    }

    $self->debug( { '-text' => 'Finish -rollback ' . $self->dbg_value($rollback) . ' -commit ' . $self->dbg_value($commit) } );

    #   db mysql
    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };
    if ( defined $db_mysql_util ) {
        if ($rollback) {
            $self->debug( { '-text' => 'Rolling back database' } );
            $db_mysql_util->rollback();
        }
        elsif ($commit) {
            $self->debug( { '-text' => 'Committing database' } );
            $db_mysql_util->commit();
        }
    }

    $self->debug( { '-text' => 'Finish' } );

    return 1;
} ## end sub finish

sub DESTROY {
    my ( $self, $args ) = @_;

    if ($EVAL_ERROR) {
        carp 'Destroyed with error :' . $EVAL_ERROR;
    }

    #   cleanup
    if ( !$self->finished() ) {
        $self->finish( { '-rollback' => 1, '-commit' => 0 } );
    }

    return;
}

1;

