package AlienSpaces::Environment;

use strict;
use warnings;
use Carp qw(carp croak);

use Config::JSON;

our $VERSION = (qw$Revision$)[1];

#   init environment
our $ENVIRONMENT = {};

#   token environment key list
our $ENVIRONMENT_TOKEN_KEY_LIST = [

    #   environment tokens that are ok to substitute in message strings
    'APP_URL',
    'APP_NAME',

    'APP_NOREPLY_EMAIL',

    'SECOND',
    'MINUTE',
    'HOUR',
    'MONTH_DAY',
    'MONTH',
    'YEAR',
    'WEEK_DAY',
    'YEAR_DAY',
    'DAYLIGHT_SAVING',

];

#   required environment key list
my $required_environment_key_list = [

    #   this is first so we can read in additional config from
    #   the JSON config file and transition any additional
    #   Apache configs into the config file.
    'APP_CONFIG_FILE',

    #   required environment keys
    'APP_NAME',
    'APP_HOSTNAME',
    'APP_URL',

    'APP_HOME',
    'APP_WEB_HOME',
    'APP_TEMPLATE_PATHS',

    'APP_PERL_NAMESPACE',
    'APP_PERL_LIBRARY_PATH',

    'APP_COOKIE_NAME',

    'APP_SHELL_DB_USERNAME',
    'APP_SHELL_DB_PASSWORD',
    'APP_SHELL_DB',

    'APP_WEB_DB_USERNAME',
    'APP_WEB_DB_PASSWORD',
    'APP_WEB_DB',

    'APP_NOREPLY_EMAIL',
];

#   environment key list
my $environment_key_list = [

    #   apache / http
    'REMOTE_ADDR',
    'REMOTE_PORT',
    'REMOTE_HOST',
    'LANG',

    'HTTP_ACCEPT',
    'HTTP_ACCEPT_CHARSET',
    'HTTP_ACCEPT_ENCODING',
    'HTTP_ACCEPT_LANGUAGE',
    'HTTP_HOST',
    'HTTP_REFERER',
    'HTTP_USER_AGENT',
    
    #   offline config
    'APP_OFFLINE_CONFIG_FILE',

    #   super namespaces and library
    'APP_PERL_SUPER_NAMESPACE',
    'APP_PERL_SUPER_LIBRARY_PATH',

    #   so important! :P
    'APP_ALIENSPACES_APPLICATION_URL',
];

sub init_environment {
    my ( $class, $args ) = @_;

    #   env (plack)
    my $env = $args->{'-env'};
    if ( defined $env ) {
        while ( my ( $k, $v ) = each %{$env} ) {
            carp 'Have -env -k ' . $k . ' -v ' . $v;
        }
    }

    #   directives (apache)
    my $directives = $args->{'-directives'};

    #   apache request (apache)
    my $apache_request = $args->{'-apache_request'};

    #   config
    my $config;

    $ENVIRONMENT = {};

    my %required_keys = map { $_ => 1 } @{$required_environment_key_list};

    #   required
    foreach my $key ( @{$required_environment_key_list}, @{$environment_key_list} ) {
        my $value;

        #   config
        if ( defined $config ) {
            my $config_key = lc $key;
#            carp 'init_environment: looking for config key ' . $config_key;
            $value = $config->get( $config_key );
        }

        #   apache directive
        if ( !defined $value && defined $directives && $directives->can("$key") ) {

#            carp 'init_environment: looking for directive key ' . $key;
            $value = $directives->$key;
        }

        #   apache dir config
        if ( !defined $value && defined $apache_request ) {

#            carp 'init_environment: looking for dir_config key ' . $key;
            $value = $apache_request->dir_config($key);
        }

        #   env (plack)
        if ( !defined $value && defined $env->{$key} ) {

#            carp 'init_environment: looking for $env key ' . $key;
            $value = $env->{$key};
        }

        #   $ENV
        if ( !defined $value ) {

#            carp 'init_environment: looking for $ENV key ' . $key;
            $value = $ENV{$key};
        }

        if ( !defined $value && $required_keys{$key} ) {
            croak 'Missing required environment value for key ' . $key;
        }

        if ( defined $value && $value =~ /[,]/sxm ) {
            $value = [ split /[,]/sxm, $value ];
        }

        $ENVIRONMENT->{$key} = $value;

        if ( $key eq 'APP_CONFIG_FILE' ) {
            $config = $class->_get_config();
        }

    } ## end foreach my $key ( @{$required_environment_key_list...})

    #   init date time
    _init_date_time();

    return 1;
} ## end sub init_environment

#   get environment string
#   - returns environment as a pipe separated
#     string of key=value pairs
sub get_environment_string {
    my ( $class, $args ) = @_;

    if ( !keys %{$ENVIRONMENT} ) {
        $class->init_environment();
    }

    my $return_string = '';
    foreach my $key ( keys %{$ENVIRONMENT} ) {
        my $export_key   = uc($key);
        my $export_value = $ENVIRONMENT->{$key};
        if ( ref($export_value) eq 'ARRAY' ) {
            $export_value = join ',', @{$export_value};
        }
        if ( !defined $export_value ) {
            next;
        }
        $return_string .= "$export_key=$export_value|";
    }

    chop $return_string;

    return $return_string;
}

#  init date time
sub _init_date_time {

    #   date
    my ( $sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst ) = localtime(time);

    my $date_time_environment = {
        'SECOND'          => $sec,
        'MINUTE'          => $min,
        'HOUR'            => $hour,
        'MONTH_DAY'       => $mday,
        'MONTH'           => $mon,
        'YEAR'            => ( $year += 1900 ),
        'WEEK_DAY'        => $wday,
        'YEAR_DAY'        => $yday,
        'DAYLIGHT_SAVING' => $isdst,
    };

    while ( my ( $key, $value ) = each %{$date_time_environment} ) {
        $ENVIRONMENT->{$key} = $value;
    }

    return 1;
}

#   get config
sub _get_config {

    my $app_config_file = $ENVIRONMENT->{'APP_CONFIG_FILE'};
    if ( !defined $app_config_file ) {
        croak 'Missing APP_CONFIG_FILE from environment';
    }

    my $config = Config::JSON->new($app_config_file);
    if ( !defined $config ) {
        croak 'Missing -app_config_file ' . $app_config_file;
    }

    return $config;
}

1;

__END__

