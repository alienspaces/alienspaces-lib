package AlienSpaces::Cache;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use Storable qw(dclone);
use Digest::MD5;

our $VERSION = qw($Revision: $) [1];

my $CACHE;
my $NAMESPACE;

sub init_cache {
    my ($self, $args) = @_;
    
    $NAMESPACE = $args->{'-namespace'};

    return 1;
}

#   set cached
sub set_cached {
    my ( $self, $args ) = @_;

    if ( !defined $NAMESPACE ) {
        croak 'Missing namespace, cannot clear cached';
        return;
    }

    my $category = $args->{'-category'};
    if ( !defined $category ) {
        $category = 'all';
    }

    if ( !defined $CACHE->{$NAMESPACE} ) {
        $CACHE->{$NAMESPACE} = {};
    }

    if ( !defined $CACHE->{$NAMESPACE}->{$category} ) {
        $CACHE->{$NAMESPACE}->{$category} = {};
    }

    my $key   = $args->{'-key'};
    my $value = $args->{'-value'};

    my $cache_value;
    if ( ref $value ) {
        $cache_value = dclone($value);
    }
    else {
        $cache_value = $value;
    }

    my $clear = $args->{'-clear'};

    if ( !defined $clear ) {
        $clear = 1;
    }

    $CACHE->{$NAMESPACE}->{$category}->{$key} = {
        '-data'  => $cache_value,
        '-clear' => $clear,
    };

    return;
} ## end sub set_cached

#   get cache key
sub get_cache_key {
    my ( $self, $args ) = @_;

    #   cache key args
    my $cache_key_args = $args->{'-cache_key_args'};
    if ( ref($cache_key_args) eq 'HASH' ) {
        return $self->encrypt_string( { '-string' => ( join '-', %{$cache_key_args} ) } );
    }

    return $self->encrypt_string( { '-string' => ( join '-', @{$cache_key_args} ) } );
}

#   get cached
sub get_cached {
    my ( $self, $args ) = @_;

    if ( !defined $NAMESPACE ) {
        croak 'Missing namespace, cannot clear cached';
        return;
    }

    my $category = $args->{'-category'};
    if ( !defined $category ) {
        $category = 'all';
    }

    if ( !defined $CACHE->{$NAMESPACE} ) {
        $CACHE->{$NAMESPACE} = {};
        return;
    }

    #   key
    my $key = $args->{'-key'};

    #   data
    my $data;

    #   category cached
    my $category_cached = $CACHE->{$NAMESPACE}->{$category};
    if ( defined $category_cached ) {

        #   cached
        my $cached = $category_cached->{$key};

        if ( defined $cached ) {
            if ( ref $cached->{'-data'} ) {
                $data = dclone( $cached->{'-data'} );
            }
            else {
                $data = $cached->{'-data'};
            }
        }
    }

    return $data;
} ## end sub get_cached

#   clear cached
sub clear_cached {
    my ( $self, $args ) = @_;

    if ( !defined $NAMESPACE ) {
        croak 'Missing namespace, cannot clear cached';
        return;
    }

    my $category = $args->{'-category'};

    #   key
    if ( defined $args->{'-key'} ) {

        if ( !defined $category ) {
            $category = 'all';
        }

        if ( defined $CACHE->{$NAMESPACE} && defined $CACHE->{$NAMESPACE}->{$category} ) {
            delete $CACHE->{$NAMESPACE}->{$category}->{ $args->{'-key'} };
        }
    }

    #   category
    elsif ( defined $category ) {
        if ( defined $CACHE->{$NAMESPACE} && defined $CACHE->{$NAMESPACE}->{$category} ) {
            delete $CACHE->{$NAMESPACE}->{$category};
        }
    }

    #   all
    else {
        delete $CACHE->{$NAMESPACE};
    }

    return;
} ## end sub clear_cached

#
#   encrypt a string
#
sub encrypt_string {
    my ( $self, $args ) = @_;

    my $string         = $args->{'-string'};
    my $encrypt_string = undef;

    if ( defined $string ) {
        my $context = Digest::MD5->new();
        $context->add($string);
        $encrypt_string = $context->b64digest();
    }

    return $encrypt_string;
}

1;
