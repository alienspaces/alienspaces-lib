package AlienSpaces::Base;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

our $VERSION = '$Revision:$';

use File::Basename;
use File::Temp;
use URI::Escape;
use Digest::MD5;
use Cwd;
use Sys::Hostname;
use Log::Log4perl;
use Log::Log4perl::Level;
use Apache2::Const qw(OK REDIRECT);
use IPC::Run qw(start);
use Module::Load;

use Time::HiRes qw(gettimeofday tv_interval);

use Data::Dumper;

use AlienSpaces::Properties {

    #   apache request object (http)
    'apache_request' => { '-type' => 's' },

    #   apache request object (plack)
    'request' => { '-type' => 's' },

    #   interface
    'interface' => { '-type' => 's' },

    #   hash of util objects
    'utils' => { '-type' => 'h' },

    #   program name
    'program_name'         => { '-type' => 's' },
    'referer_program_name' => { '-type' => 's' },

    #   logging
    'carp_enabled'  => { '-type' => 's' },    #   carp if no logger
    'debug_enabled' => { '-type' => 's' },    #   debug logging enabled
    'error_enabled' => { '-type' => 's' },    #   error logging enabled
    'trace_enabled' => { '-type' => 's' },    #   trace logging enabled
    'info_enabled'  => { '-type' => 's' },    #   info logging enabled
    'fatal_enabled' => { '-type' => 's' },    #   info logging enabled

};

use AlienSpaces::Environment;
use AlienSpaces::Constants qw(:interface :loader :util :error-codes-global :error-codes-user);

use constant TRUE                  => 1;
use constant FALSE                 => 0;
use constant DEBUG_VALUE_INDENT    => 4;
use constant MAX_DBG_RECURSE_COUNT => 1000;

use AlienSpaces::Cache;

#   new
sub new {
    my ( $class, $args ) = @_;

    my $self = {};
    bless $self, $class;

    #   init property values
    $self->_init_property_values($args);

    #   init logging flags
    $self->_init_logging_flags();

    #   program name
    if ( !defined $self->program_name ) {
        $self->program_name( $self->get_program_name() );
    }

    if ( !defined $self->interface ) {
        $self->interface(INTERFACE_SHELL);
    }

    #   referer program name
    if ( !defined $self->referer_program_name ) {
        $self->referer_program_name( $self->get_referer_program_name() );
    }

    return $self;
}

sub _init_property_values {
    my ( $self, $args ) = @_;

    my $success = eval {
        foreach my $key ( keys %{$args} ) {
            my $prop_key = $key;
            $prop_key =~ s/\-//sxmg;
            $self->$prop_key( $args->{$key} );
        }
        1;
    };
    if ( !$success ) {
        croak 'Failed to initialize all arguments as properties :' . $EVAL_ERROR;
    }

    return 1;
}

sub _init_logging_flags {
    my ( $self, $args ) = @_;

    #   default log levels enabled
    $self->trace_enabled(1);
    $self->carp_enabled(1);
    $self->debug_enabled(1);
    $self->info_enabled(1);

    $self->error_enabled(1);
    $self->fatal_enabled(1);

    return 1;
}

#   init
sub init {
    my ( $self, $args ) = @_;

    #   init property values
    $self->_init_property_values($args);

    #   program name
    if ( !defined $self->program_name ) {
        $self->program_name( $self->get_program_name() );
    }

    #   referer program name
    if ( !defined $self->referer_program_name ) {
        $self->referer_program_name( $self->get_referer_program_name() );
    }

    return 1;
}

#   environment
sub init_environment {
    my ( $class, $args ) = @_;
    return AlienSpaces::Environment->init_environment($args);
}

sub environment_token_key_list {
    return $AlienSpaces::Environment::ENVIRONMENT_TOKEN_KEY_LIST;
}

sub environment {
    return $AlienSpaces::Environment::ENVIRONMENT;
}

#   logging
sub init_logging {
    my ( $class, $args ) = @_;

    #   application home
    my $application_home = $class->environment->{'APP_HOME'};
    if ( !defined $application_home ) {
        croak 'Could not initialise no APP_HOME environment variable set';
    }

    #   log4perl configuration
    my $log4perl_conf = $application_home . '/conf/log4perl.conf';
    if ( !-e $log4perl_conf ) {
        croak 'Could not find log4perl conf ' . $log4perl_conf;
    }

    #   initialise
    Log::Log4perl->init($log4perl_conf);

    $Log::Log4perl::caller_depth = 2;

    return 1;
}

sub finish_logging {
    my ( $class, $args ) = @_;

    Log::Log4perl->reset();

    return 1;
}

#   profiling
my $PROFILE_TAGS    = [];
my $PROFILE_SUMMARY = {};

sub init_profile {
    my ( $self, $args ) = @_;

    $PROFILE_TAGS    = [];
    $PROFILE_SUMMARY = {};

    return 1;
}

sub profile_start {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-tag'], $args ) ) {
        return;
    }

    my $tag = $args->{'-tag'};

    #   set profile for tag
    my $time = [gettimeofday];
    if ( defined $time ) {
        push @{$PROFILE_TAGS},
            {
            '-tag'        => $tag,
            '-start_time' => $time,
            };

        if ( !exists $PROFILE_SUMMARY->{$tag} ) {
            $PROFILE_SUMMARY->{$tag} = {
                '-total_elapsed' => 0,
                '-total_calls'   => 0,
            };
        }

        $PROFILE_SUMMARY->{$tag}->{'-total_calls'}++;
    }

    return;
} ## end sub profile_start

sub profile_end {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-tag'], $args ) ) {
        return;
    }

    my $tag = $args->{'-tag'};

    my $profile = pop @{$PROFILE_TAGS};
    if ( !defined $profile ) {

        #   Program tag name may be called if we are destroyed which
        #   means it isnt really a 'tag' error in this instance
        if ( defined $tag && $tag ne 'Program' ) {
            $self->error( { '-text' => 'Profile not defined, cannot end profile -tag ' . $tag } );
        }
        return;
    }

    my $profile_tag        = $profile->{'-tag'};
    my $profile_start_time = $profile->{'-start_time'};

    if ( !defined $profile_tag || !defined $profile_start_time ) {
        $self->debug(
            {   '-text' => 'Profile -tag ' .
                    $self->dbg_value($profile_tag) . ' or -time ' . $self->dbg_value($profile_start_time) . ' not defined'
            } );
    }

    my $elapsed = tv_interval($profile_start_time);

    $PROFILE_SUMMARY->{$tag}->{'-total_elapsed'} += $elapsed;

    return 1;
} ## end sub profile_end

sub profile_summary {
    my ( $self, $args ) = @_;

    if ( !scalar keys %{$PROFILE_SUMMARY} ) {
        return;
    }

    $self->debug( { '-text' => '<== Profile Summary ==>' } );

PROFILE_TAGS:
    foreach my $profile_tag (
        sort { $PROFILE_SUMMARY->{$a}->{'-total_elapsed'} <=> $PROFILE_SUMMARY->{$b}->{'-total_elapsed'} }
        keys %{$PROFILE_SUMMARY}
        ) {

        my $profile_total_elapsed = $PROFILE_SUMMARY->{$profile_tag}->{'-total_elapsed'};
        my $profile_total_calls   = $PROFILE_SUMMARY->{$profile_tag}->{'-total_calls'};

        if ( !$profile_total_elapsed || !$profile_total_calls ) {
            $self->info( { '-text' => 'Profiling problem, skipping..' } );
            next PROFILE_TAGS;
        }
        my $profile_average = $profile_total_elapsed / $profile_total_calls;

        my $log = sprintf( 'Tag [%-20s] calls [%-3d] average [%-.3f] total [%-.3f]',
            $profile_tag, $profile_total_calls, $profile_average, $profile_total_elapsed );

        $self->debug( { '-text' => $log } );
    }

    $self->debug( { '-text' => '<== ------- ------- ==>' } );

    return;
} ## end sub profile_summary

#   context
my $CONTEXT_TAGS = {};

sub init_context {
    my ( $self, $args ) = @_;

    #   clear context tags
    $CONTEXT_TAGS = {};

    return 1;
}

sub set_context {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-tag', '-value' ], $args ) ) {
        return;
    }

    #   tag
    my $tag = $args->{'-tag'};

    #   value
    my $value = $args->{'-value'};

    $self->trace( { '-text' => 'Setting -tag ' . $tag . ' -value ' . $value } );

    #   set context
    $CONTEXT_TAGS->{$tag} = $value;

    return;
}

sub get_context {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-tag'], $args ) ) {
        return;
    }

    #   tag
    my $tag = $args->{'-tag'};

    #   value
    my $value = $CONTEXT_TAGS->{$tag};

    $self->trace( { '-text' => 'Returning -tag ' . $tag . ' -value ' . $self->dbg_value($value) } );

    return $value;
}

sub clear_context {
    my ( $self, $args ) = @_;

    #   tag
    my $tag = $args->{'-tag'};

    if ( defined $tag ) {
        delete $CONTEXT_TAGS->{$tag};
    }
    else {
        $CONTEXT_TAGS = {};
    }

    return 1;
}

#   util object load list (none by default)
sub util_list {
    my ( $self, $args ) = @_;

    return [];
}

#   util object clear list (none by default)
sub util_clear_list {
    my ( $self, $args ) = @_;

    return [

    ];
}

#   utiliy object new args
sub util_new_args {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-class'], $args ) ) {
        return;
    }

    return {
        '-program_name'         => $self->program_name,
        '-referer_program_name' => $self->referer_program_name,
        '-apache_request'       => $self->apache_request,
        '-interface'            => $self->interface,
    };
}

sub util_init_args {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-class'], $args ) ) {
        return;
    }

    return {};
}

#
#   util object loading
#
our $CLASS_UTILS      = {};
our $CLASS_UTILS_INIT = {};

sub load_utils {
    my ( $self, $args ) = @_;

    #   namespace
    my $namespace = $self->environment->{'APP_PERL_NAMESPACE'};

    $self->trace(
        {   '-text' => 'Load utils for ' .
                ref($self) . ' -scope ' . $self->dbg_value( $args->{'-scope'} ) . ' -namespace ' . $namespace
        } );

    #   assign previously instantiated global utils
    my $utils = {};

    #   util list
    my $util_list = $self->util_list;

    $self->trace( { '-text' => 'Class ' . ref($self) . ' -util_list ' . $self->dbg_value($util_list) } );

LOAD_UTIL:
    foreach my $util_name ( @{$util_list} ) {

        #   util file / class
        my ( $util_file, $util_class ) = $self->make_class(
            {   '-name' => $util_name,
                '-type' => CLASS_TYPE_UTIL
            } );

        #   didnt find one?
        if ( !defined $util_class ) {
            next LOAD_UTIL;
        }

        #   already have one?
        if ( defined $utils->{$util_name} ) {
            next LOAD_UTIL;
        }

        #   we dont need a util of ourselves
        if ( $util_class eq ref($self) ) {
            $self->trace( { '-text' => 'Match ' . $util_class . ', not adding' } );
            next LOAD_UTIL;
        }

        #   check for global utils we have already loaded
        if ( ( exists $CLASS_UTILS->{$namespace}->{$util_class} ) &&
            ( $CLASS_UTILS->{$namespace}->{$util_class} != -1 ) ) {

            $self->trace( { '-text' => 'Add global util -class ' . $util_class } );

            $utils->{$util_name} = $CLASS_UTILS->{$namespace}->{$util_class};

            if ( defined $CLASS_UTILS_INIT->{$namespace}->{$util_class} &&
                $CLASS_UTILS_INIT->{$namespace}->{$util_class} == 1 ) {
                next LOAD_UTIL;
            }

            #   mark as initialised
            $CLASS_UTILS_INIT->{$namespace}->{$util_class} = 1;

            #   initialise
            if ( !$utils->{$util_name}->init( $self->util_init_args( { '-class' => $util_name } ) ) ) {
                $self->error( { '-text' => 'Utility -util_name ' . $self->dbg_value($util_name) . ' failed initialization' } );
                return;
            }

            next LOAD_UTIL;
        }

        $self->trace( { '-text' => 'Added new util -class ' . $util_class } );

        my $util_object;
        my $success = eval {

            #   load
            $self->load_class( { '-class_name' => $util_class } );

            #   util new args
            my $util_new_args = $self->util_new_args( { '-class' => $util_name } );

            #   util name
            $util_new_args->{'-name'} = $util_name;

            #   util new
            $util_object = $util_class->new($util_new_args);
            1;
        };

        if ( !$success ) {
            $self->fatal( { '-text' => 'Failed util -class ' . $util_class . ' :' . $EVAL_ERROR } );
            next LOAD_UTIL;
        }

        $utils->{$util_name} = $util_object;

        #   if global assign to global hash
        if ( ( defined $args->{'-scope'} ) && ( $args->{'-scope'} eq 'global' ) ) {
            my $add_to_sort = 1;

            $self->trace( { '-text' => 'Register global util -name ' . $util_name } );

            $CLASS_UTILS->{$namespace}->{$util_class} = $util_object;

        }

        $self->trace( { '-text' => 'Init util -class ' . $util_class } );

        #   mark as initialised
        $CLASS_UTILS_INIT->{$namespace}->{$util_class} = 1;

        #   initialise
        if ( !$utils->{$util_name}->init( $self->util_init_args( { '-class' => $util_name } ) ) ) {
            $self->error( { '-text' => 'Utility -util_name ' . $self->dbg_value($util_name) . ' failed init' } );
            return;
        }

    } ## end LOAD_UTIL: foreach my $util_name ( @{$util_list...})

    if ( scalar keys %{$utils} ) {
        $self->utils($utils);
        $self->show_utils();
    }

    return 1;
} ## end sub load_utils

sub show_utils {
    my ( $self, $args ) = @_;

    my $utils = $self->utils;
    if ( !defined $utils || !scalar keys %{$utils} ) {
        $self->debug( { '-text' => 'Object ' . ref($self) . ' has no utils' } );
        return;
    }

    $self->debug( { '-text' => 'Object ' . ref($self) . ' has utils ' . ( join ', ', keys %{$utils} ) } );

    return 1;
}

#
#   environment set methods
#
sub set_hostname {
    my ( $self, $args ) = @_;

    if ( defined $self->hostname ) {

        #        carp 'Hostname already set as ' . $self->hostname . "\n";
    }
    elsif ( $ENV{'SERVER_NAME'} ) {
        $self->hostname( $ENV{'SERVER_NAME'} );

        #        carp 'Set hostname from $ENV as ' . $self->hostname . "\n";
    }
    return;
}

sub set_environment {
    my ( $self, $args ) = @_;

    return;
}

#
#   Execute an OS command.  Note that this method returns the OS return code
#   from the command - not the traditional 1 => OK, 0 => not OK of a perl
#   method.
#
sub system_command {
    my ( $self, $args ) = @_;

    my $command = $args->{'-command'};

    system($command) == 0 or croak 'System command failed :' . $CHILD_ERROR;

    return 1;
}

sub execute_shell_class {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-class'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   class
    my $class = $args->{'-class'};

    #   options
    my $options = $args->{'-options'};

    #   options_string
    my $options_string = '';
    if ( defined $options && ref($options) eq 'HASH' ) {
        foreach my $option ( keys %{$options} ) {

            if ( !defined $options->{$option} ) {
                $options_string .= ' ' . $option;
            }
            else {
                $options_string .= ' ' . $option . ' ' . $options->{$option};
            }
        }
    }

    my $application_home = $self->environment->{'APP_HOME'};

    my $script = <<"SCRIPT";
#!/usr/bin/env bash    

#   set environment
source $application_home/bin/app-env.sh

#   run class
/project/alienspaces/dev-build/bin/prun.pl $class $options_string

SCRIPT

## BWW

    File::Temp->safe_level(File::Temp::STANDARD);
    my $fh = File::Temp->new( UNLINK => 0 );
    my $filename = $fh->filename;

    $self->debug( { '-text' => 'Creating temporary script -filename ' . $filename } );

    print $fh $script;

    chmod 0755, $filename;

    close $fh;

    $self->system_command( { '-command' => $filename } );

    return 1;
} ## end sub execute_shell_class

#
#   html escape
#
sub html_escape {
    my ( $self, $string ) = @_;

    my $ret_string = uri_escape($string);
    $ret_string =~ s/\"/\%22/sxmg;

    return $ret_string;
}

#
#   html strip
#
sub html_strip {
    my ( $self, $string ) = @_;

    if ( defined $string ) {
        $string =~ s/\<.*?\>//sxmg;
        $string =~ s/[\<\>]//sxmg;
    }

    return $string;
}

#
#   html format
#
sub html_format {
    my ( $self, $string ) = @_;

    #   add line breaks instead of newlines
    $string =~ s/\n/\<br\/\>/sxmg;

    #   turn urls into anchors
    $string =~
        s/(http\:\/\/|www\.|http\:\/\/www\.)(.*?)(\s|\n)/\<a href\=\"http\:\/\/$2$3\" target\=\"new\"\>http\:\/\/$2$3\<\/a\>/sxmg;
    return $string;
}

#
#   current program name
#
sub get_program_name {
    my $self = shift;

    my $program_name = basename($PROGRAM_NAME);

    return $program_name;
}

#
#   get program authorize task
#   - given a program path fetches authorization task from iface class
my $AUTHORIZATION_TASK_CACHE = {};

sub get_program_authorize_task {
    my ( $self, $args ) = @_;

    my $program = $args->{'-program'};
    if ( !defined $program ) {
        $program = $self->program_name;
    }
    if ( !defined $program ) {
        $self->error( { '-text' => 'Missing -program, cannot get program authorization task' } );
        return;
    }

    #   namespaces
    my @namespaces = ( $self->environment->{'APP_PERL_NAMESPACE'}, $self->environment->{'APP_PERL_SUPER_NAMESPACE'}, );

FIND_CACHED:
    foreach my $namespace (@namespaces) {
        if ( !defined $namespace ) { next FIND_CACHED; }
        if ( !defined $AUTHORIZATION_TASK_CACHE->{$namespace} ) {
            $AUTHORIZATION_TASK_CACHE->{$namespace} = {};
        }

        #   cached
        if ( exists $AUTHORIZATION_TASK_CACHE->{$namespace}->{$program} ) {
            return $AUTHORIZATION_TASK_CACHE->{$namespace}->{$program};
        }
    }

    #   class name
    my $class_name = $self->_make_class_from_path( { '-path' => $program } );

    #   found namespace
    my $found_namespace;

    my $load_class_name;

FIND_CLASS:
    foreach my $namespace (@namespaces) {
        if ( !defined $namespace ) { next FIND_CLASS; }

        $self->debug( { '-text' => ' Program -namespace ' . $namespace . ' -class_name ' . $class_name } );

        #   load class name
        $load_class_name = $namespace . '::IFace::Web::' . $class_name;

        #   load class
        if ( $self->load_class( { '-class_name' => $load_class_name } ) ) {
            $self->debug( { '-text' => ' Success load class -load_class_name ' . $load_class_name } );

            $found_namespace = $namespace;

            last FIND_CLASS;
        }

        $self->debug( { '-text' => ' Failed load class -load_class_name ' . $load_class_name } );
        undef $load_class_name;
    }

    if ( !defined $found_namespace ) {
        croak $self->stack( { '-text' => 'Failed to find namespace for -program ' . $program } );
    }

    if ( !defined $load_class_name ) {
        croak 'Failed to load -class_name ' . $class_name;
    }

    my $task = $load_class_name->authorize_task();

    $self->debug(
        {   '-text' => 'Have -found_namespace ' . $found_namespace .
                ' -task ' . $self->dbg_value($task) . ' from -program ' . $program . ' -load_class_name ' . $load_class_name
        } );

    #   cache
    $AUTHORIZATION_TASK_CACHE->{$found_namespace}->{$program} = $task;

    return $task;
} ## end sub get_program_authorize_task

#
#   referer program name
#
sub get_referer_program_name {
    my ( $self, $args ) = @_;

    my $interface = $self->interface();
    if ( !defined $interface ) {
        $self->info( { '-text' => 'No interface defined, cannot derive referer program name' } );
        return;
    }

    $interface = lc($interface);

    my $referer_program_name = $ENV{'HTTP_REFERER'};

    if ( !defined $referer_program_name ) {
        $referer_program_name = $self->program_name;
        if ( !defined $referer_program_name ) {
            $referer_program_name = $self->get_program_name();
        }
    }

    $referer_program_name =~ s/\?.*$//sxmg;
    $referer_program_name =~ s/^.*$interface\///sxmg;

    $self->debug( { '-text' => 'Referer program name ' . $self->dbg_value($referer_program_name) . ' -interface ' . $interface } );

    return $referer_program_name;
}

#
#   http redirect
#
sub http_redirect {
    my ( $class, $args ) = @_;

    #   apache request
    my $apache_request = $args->{'-apache_request'};

    #   log
    my $log = $apache_request->log;

    #   url
    my $url = $args->{'-url'};
    if ( !defined $url ) {
        croak 'Missing argument -url, cannot perform redirection';

        #   i am considering defaulting this to $class->authorize_redirect_url
        #   - shortcut to redirect without arguments
        #   - catch a scenario where the argument -url was accidentally excluded
        #     or mistyped..
        #
        #   i will leave it for now..
    }

    #   interface
    my $interface = lc( $class->interface() );

    my $application_hostname = $class->environment->{'APP_HOSTNAME'};

    $application_hostname =~ s/^http\:\/\///sxmg;
    $application_hostname =~ s/^https\:\/\///sxmg;

    if ( ( $url =~ /^http\:\/\/$application_hostname/ ) ||
        ( $url =~ /^https\:\/\/$application_hostname/ ) ) {

        $log->info( __PACKAGE__,
            ' Redirecting local application -url ' . $url . ' -interface ' .
                $class->dbg_value($interface) . ' -application_hostname ' . $class->dbg_value($application_hostname) );

        if ( $url =~ /^http/sxmg ) {
            $url =~ s/http\:\/\/.*?(\/.*)$/$1/sxmg;
            $log->info( __PACKAGE__, ' Removing address -url ' . $url );
        }

        if ( defined $interface ) {
            if ( $url !~ /^\/$interface/sxmg ) {
                $log->info( __PACKAGE__, ' URL does not contain interface' );

                $url = '/' . $interface . '/' . $url;
            }
        }

        #   get rid of any double /
        $url =~ s/\/\//\//sxmg;
    }
    else {
        $log->info( __PACKAGE__,
            ' Redirecting external application -url ' . $url . ' -interface ' .
                $class->dbg_value($interface) . ' -application_hostname ' . $class->dbg_value($application_hostname) );
    }

    #   session util
    my $session_util = $class->utils->{ UTIL_SESSION() };

    #   set cookie
    if ( defined $session_util ) {
        $apache_request->err_headers_out->add( 'Set-Cookie' => $session_util->get_cookie );
    }

    #   set location
    $apache_request->headers_out->set( 'Location' => $url );

    #   set status
    $apache_request->status(Apache2::Const::REDIRECT);

    return 1;
} ## end sub http_redirect

#
#   util navigation shortcuts
#
sub make_program_link {
    my ( $self, $args ) = @_;

    my $navigation_util = $self->utils->{ UTIL_OUTPUT_HTML_NAVIGATION() };
    if ( !defined $navigation_util ) {
        $self->error( { '-text' => 'Missing navigation util, cannot make program link' } );
        return;
    }

    return $navigation_util->_make_program_link($args);
}

sub make_panel_link {
    my ( $self, $args ) = @_;

    my $navigation_util = $self->utils->{ UTIL_OUTPUT_HTML_NAVIGATION() };
    if ( !defined $navigation_util ) {
        $self->error( { '-text' => 'Missing navigation util, cannot make panel link' } );
        return;
    }

    return $navigation_util->_make_component_link($args);
}

sub make_component_link {
    my ( $self, $args ) = @_;

    my $navigation_util = $self->utils->{ UTIL_OUTPUT_HTML_NAVIGATION() };
    if ( !defined $navigation_util ) {
        $self->error( { '-text' => 'Missing navigation util, cannot make component link' } );
        return;
    }

    return $navigation_util->_make_component_link($args);
}

sub make_link_params {
    my ( $self, $args ) = @_;

    my $navigation_util = $self->utils->{ UTIL_OUTPUT_HTML_NAVIGATION() };
    if ( !defined $navigation_util ) {
        $self->error( { '-text' => 'Missing navigation util, cannot make link params' } );
        return;
    }

    return $navigation_util->_make_link_params($args);
}

#   make css class
sub make_css_class {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-string'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   string
    my $string = lc( $args->{'-string'} );

    #   normalise
    $string =~ s/^(.*)\..*$/$1/sxmg;
    $string =~ s/[\_\-\/]/\|/sxmg;

    #   pieces
    my @pieces = split /\|/sxm, $string;

    my $return_string = '';
    foreach (@pieces) {
        $return_string .= ucfirst($_);
    }

    return $return_string;
}

#   unique ids
our $UNIQUE_IDS = {};

#   make id
#   - generate a unique id
sub make_id {
    my ( $self, $args ) = @_;

    #   name
    my $name = $args->{'-name'};
    if ( !defined $name ) {
        if ( $self->can('name') ) {
            $name = $self->name();
        }
        if ( !defined $name ) {
            $name = ref($self);
        }
    }
    if ( !defined $name ) {
        $self->trace( { '-text' => 'Still could not make a name' } );
        return;
    }

    my @words;
    if ( $name =~ /\:\:/sxmg ) {
        @words = split /\:\:/sxm, $name;
    }
    else {
        @words = split /[\-\_]/sxm, $name;
    }

    #   id
    my $id = '';
    foreach my $word (@words) {
        $id .= ucfirst($word);
    }

    if (! exists $UNIQUE_IDS->{$id} ) {
        $UNIQUE_IDS->{$id} = 0;
    } else  {
        $id .= ++$UNIQUE_IDS->{$id};
    }

    return $id;
} ## end sub make_id

#   clear ids
#   - clear our id hash
sub clear_ids {
    my ( $self, $args ) = @_;

    $UNIQUE_IDS = {};

    return 1;
}

#   make class
#   - translate a constant into a Perl class name / file name
sub make_class {
    my ( $self, $args ) = @_;

    my $name      = $args->{'-name'};
    my $type      = $args->{'-type'};
    my $interface = $args->{'-interface'};

    my ( $file, $class, $namespace, $library_path );

    #   try local library first
    ( $namespace, $library_path ) = ( $self->environment->{'APP_PERL_NAMESPACE'}, $self->environment->{'APP_PERL_LIBRARY_PATH'}, );

    $self->trace( { '-text' => 'Looking for class -name ' . $name . ' -type ' . $type . ' -namespace ' . $namespace } );

    if ( !defined $namespace || !defined $library_path ) {
        $self->fatal(
            {   '-text' => 'Either -namespace ' . $self->dbg_value($namespace) .
                    ' or -library_path ' . $self->dbg_value($library_path) . ' undefined, cannot continue'
            } );
    }

    #   if -type component and we have an interface argument
    #   try to find a component specific to the interface as priority
    my @types = ($type);
    if ( defined $interface ) {
        push @types, $type . '::' . $interface;
    }

FIND_CLASS:
    foreach my $type ( reverse @types ) {
        ( $file, $class ) = $self->_make_class(
            {   '-name'         => $name,
                '-type'         => $type,
                '-namespace'    => $namespace,
                '-library_path' => $library_path,
            } );
        if ( defined $file && defined $class ) {
            last FIND_CLASS;
        }
    }

    if ( !defined $file || !defined $class ) {
        $self->trace(
            {   '-text' => 'Could not find source for -name ' . $name . ' -type ' .
                    $type . ' -namespace ' . $self->dbg_value($namespace) . ' -library_path ' . $self->dbg_value($library_path) } );

        #   try super library next
        ( $namespace, $library_path ) =
            ( $self->environment->{'APP_PERL_SUPER_NAMESPACE'}, $self->environment->{'APP_PERL_SUPER_LIBRARY_PATH'}, );
        if ( defined $namespace && defined $library_path ) {
        FIND_SUPER_CLASS:
            foreach my $type ( reverse @types ) {
                ( $file, $class ) = $self->_make_class(
                    {   '-name'         => $name,
                        '-type'         => $type,
                        '-namespace'    => $namespace,
                        '-library_path' => $library_path,
                    } );
                if ( defined $file && defined $class ) {
                    last FIND_SUPER_CLASS;
                }
            }

            if ( !defined $file || !defined $class ) {
                $self->trace(
                    {   '-text' => 'Could not find source for -name ' .
                            $name . ' -type ' . $type . ' -namespace ' . $namespace . ' -library_path ' . $library_path
                    } );
            }
        }
    } ## end if ( !defined $file ||...)

    if ( !defined $file || !defined $class ) {
        $self->fatal(
            {   '-text' => 'Could not find source -name ' . $name . ' -type ' .
                    $type . ' -namespace ' . $self->dbg_value($namespace) . ' -library_path ' . $self->dbg_value($library_path) } );
    }

    return ( $file, $class );
} ## end sub make_class

my %CLASS_DATA = ();

sub _make_class {
    my ( $self, $args ) = @_;

    my $name         = $args->{'-name'};
    my $type         = $args->{'-type'};
    my $namespace    = $args->{'-namespace'};
    my $library_path = $args->{'-library_path'};

    my ( $file, $class );

    #   returned cached namespace->name->type
    if ( defined $CLASS_DATA{$namespace} &&
        defined $CLASS_DATA{$namespace}->{$name} &&
        defined $CLASS_DATA{$namespace}->{$name}->{$type} ) {
        $file  = $CLASS_DATA{$namespace}->{$name}->{$type}->{'-file'};
        $class = $CLASS_DATA{$namespace}->{$name}->{$type}->{'-class'};
        if ( defined $file && defined $class ) {

            $self->trace(
                {   '-text' => 'Have cached -file ' .
                        $file . ' -class ' . $class . ' for -name ' . $name . ' -type ' . $type . ' -namespace ' . $namespace
                } );

            #   if both file and name or not 'true' they have been
            #   flagged as having been searched for in this namespace
            #   before but not found so dont fucking try again
            if ( !$file || !$class ) {
                return;
            }
            return ( $file, $class );
        }
    }

    #   name looks like a class
    #   ie: Log, Phrase, DB::MySQL
    else {
        ( $file, $class ) = $self->_make_class_from_class(
            {   '-name'         => $name,
                '-type'         => $type,
                '-namespace'    => $namespace,
                '-library_path' => $library_path,
            } );
    }

    if ( !defined $file || !defined $class ) {
        $self->trace( { '-text' => 'Marking class as unfindable' } );
        $CLASS_DATA{$namespace}->{$name}->{$type}->{'-file'}  = 0;
        $CLASS_DATA{$namespace}->{$name}->{$type}->{'-class'} = 0;

        return;
    }

    $CLASS_DATA{$namespace}->{$name}->{$type}->{'-file'}  = $file;
    $CLASS_DATA{$namespace}->{$name}->{$type}->{'-class'} = $class;

    return ( $file, $class );
} ## end sub _make_class

#   _make_class_from_class
sub _make_class_from_class {
    my ( $self, $args ) = @_;

    my $name         = $args->{'-name'};
    my $type         = $args->{'-type'};
    my $namespace    = $args->{'-namespace'};
    my $library_path = $args->{'-library_path'};

    #   class
    my $class = $namespace . '::' . $type . '::' . $name;

    #   file
    my $file = $class;
    $file =~ s/\:\:/\//sxmg;
    $file .= '.pm';

    $self->trace( { '-text' => 'Looking for class for -name ' . $class } );

    if ( -e ( $library_path . '/' . $file ) ) {
        $self->trace( { '-text' => 'Found -file ' . $file . ' -class ' . $class } );
        return ( $file, $class );
    }

    return;
}

#   make class from path
sub _make_class_from_path {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-path'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   path
    my $path = $args->{'-path'};

    #   interface
    my $interface = $args->{'-interface'} || $self->interface();

    my ( @class_bits, $class_name );
    if ( $path =~ /[\/]/sxmg ) {
        @class_bits = split /[\/]/sxm, $path;

        foreach my $class_bit (@class_bits) {
            if ( !$class_bit ) {
                next;
            }
            if ( uc($class_bit) eq $interface ) {
                $class_bit = uc $class_bit;
            }
            else {
                $class_bit = ucfirst $class_bit;
            }

            if ( defined $class_name ) {
                $class_name .= '::' . $class_bit;
            }
            else {
                $class_name = $class_bit;
            }
        }
    }
    else {
        $class_name = ucfirst $path;
    }

    if ( $class_name !~ /^$interface/sxmg ) {
        $self->debug( { '-text' => 'Adding -interface ' . $interface . ' to -class_name ' . $class_name } );
        $class_name = $interface . '::' . $class_name;
    }

    return $class_name;
} ## end sub _make_class_from_path

#   load class
sub load_class {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-class_name'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   class name
    my $class_name = $args->{'-class_name'};

    #    carp 'Loading class ' . $class_name;

    my $success = eval {

        #   load
        load $class_name;
        1;
    };

    if ( !$success ) {

        #        carp 'Failed loading -class_name ' . $class_name . ' :' . $EVAL_ERROR;
        undef $class_name;
        return;
    }

    #   process isa
    $self->process_dynisa( { '-class_name' => $class_name } );

    return 1;
} ## end sub load_class

#   process dynisa
my $DYNISA_CACHE = {};

sub process_dynisa {
    my ( $self, $args ) = @_;

    my $class_name = $args->{'-class_name'};

    #    carp 'Process dynisa ' . $class_name;

    #   namespace
    my $app_namespace = $self->environment->{'APP_PERL_NAMESPACE'};
    if ( !defined $app_namespace ) {
        carp 'Missing app namespace, cannot process dynisa';
        return;
    }

    my ( $isa_ref, $dynisa_ref );

    {
        no strict 'refs';
        $isa_ref    = \@{"${class_name}::ISA"};
        $dynisa_ref = \@{"${class_name}::DYNISA"};
        use strict 'refs';
    };

    #   cached
    my $isa_cached = $DYNISA_CACHE->{$app_namespace}->{$class_name};
    if ( defined $isa_cached ) {

#        $self->debug( { '-text' => 'Assigning -isa_cached ' . $self->dbg_value($isa_cached) . ' for -class_name ' . $class_name } );
        @{$isa_ref} = @{$isa_cached};
        return 1;
    }

    #    $self->debug( { '-text' => 'Have -class_name ' . $class_name } );
    #    $self->debug( { '-text' => 'Have -isa_ref ' . $self->dbg_value($isa_ref) } );
    #    $self->debug( { '-text' => 'Have isa_ref -count ' . ( scalar @{$isa_ref} ) } );
    #    $self->debug( { '-text' => 'Have -dynisa_ref ' . $self->dbg_value($dynisa_ref) } );
    #    $self->debug( { '-text' => 'Have dynisa_ref -count ' . ( scalar @{$dynisa_ref} ) } );

    #   if we have dynisa clear out existing isa
    if ( scalar @{$dynisa_ref} ) {
        @{$isa_ref} = ();
    }

    #   namespaces
    my @namespaces = ( $self->environment->{'APP_PERL_NAMESPACE'}, $self->environment->{'APP_PERL_SUPER_NAMESPACE'}, );

PROCESS_DYNISA:
    foreach my $dynisa_class ( @{$dynisa_ref} ) {
        if ( !defined $dynisa_class ) { next PROCESS_DYNISA; }

    PROCESS_DYNISA_NAMESPACE:
        foreach my $namespace (@namespaces) {
            if ( !defined $namespace ) { next PROCESS_DYNISA_NAMESPACE; }

            #   load class name
            my $load_class_name = $namespace . '::' . $dynisa_class;

            #   don't add ourself
            if ( $load_class_name eq $class_name ) {

                #                $self->debug( { '-text' => 'Not adding our self -load_class_name ' . $load_class_name } );
                next PROCESS_DYNISA_NAMESPACE;
            }

            #   load class
            if ( $self->load_class( { '-class_name' => $load_class_name } ) ) {

                #                $self->debug( { '-text' => 'Adding -load_class_name ' . $load_class_name } );
                push @{$isa_ref}, $load_class_name;

                last PROCESS_DYNISA_NAMESPACE;
            }
        }
    }

    #   cache
    $DYNISA_CACHE->{$app_namespace}->{$class_name} = \@{$isa_ref};

    return 1;
} ## end sub process_dynisa

#   debug isa
sub debug_isa {
    my ( $self, $args ) = @_;

    my $class_name = $args->{'-class_name'};

    my @isa;

    {
        no strict 'refs';
        @isa = @{"${class_name}::ISA"};
        use strict 'refs';
    };

    if ( scalar @isa ) {
        foreach my $isa_class_name (@isa) {
            {
                no strict 'refs';
                push @isa, @{"${isa_class_name}::ISA"};
                use strict 'refs';
            };
        }

        $self->debug( { '-text' => $class_name } );

        my $count = 1;
        foreach my $isa_class_name (@isa) {
            $self->debug( { '-text' => ( '-' x $count++ ) . ' ' . $isa_class_name } );
        }
    }

    return 1;
} ## end sub debug_isa

#
#   generic method argument checker
#
sub check_arguments {
    my ( $self, $check_args, $args, $options ) = @_;

    my $check_passed = 1;
    if ( !defined $check_args ) {
        return $check_passed;
    }

    #   check for options
    #   - default to checking exists
    #   - default to not checking defined
    if ( !defined $options ) {
        $options = {
            '-exists'   => 1,
            '-required' => 0,
        };
    }
    else {

        #   always check exists unless specified
        if ( !exists $options->{'-exists'} ) {
            $options->{'-exists'} = 1;
        }

        #   dont check defined unless specified
        if ( !exists $options->{'-defined'} ) {
            $options->{'-defined'} = 0;
        }
    }

    if ( ref $check_args ne 'ARRAY' ) {
        my ( $package, $filename, $line, $subroutine, $hasargs, $wantarray, $evaltext, $is_require, $hints, $bitmask, $hinthash ) =
            caller(1);

        $self->fatal( { '-text' => 'Check args call from ' . $subroutine . ' is not an array reference ' } );
        return;
    }

    foreach my $arg ( @{$check_args} ) {
        my $check_defined = $options->{'-defined'};
        if ( $options->{'-exists'} && !exists $args->{$arg} ) {

            $self->stack( { '-text' => 'Parameter ' . $arg . ' missing in method' } );
            $check_passed  = 0;
            $check_defined = 0;
        }
        if ( $check_defined && !defined $args->{$arg} ) {

            $self->stack( { '-text' => 'Parameter ' . $arg . ' not defined in method' } );
            $check_passed = 0;
        }
    }

    return $check_passed;
} ## end sub check_arguments

#   stack
#   - logs out calls stack at error log level
sub stack {
    my ( $self, $args ) = @_;

    my $text = $args->{'-text'};

    if ( defined $text ) {
        $self->error( { '-text' => $text } );
    }

    my $caller = 1;
    my ( $package, $filename, $line, $subroutine, @other ) = caller($caller);
    while ( $subroutine && $caller < 10 ) {
        $self->error( { '-text' => ( '+' x $caller ) . ' -line ' . $line . ' -method ' . $subroutine } );

        $caller++;
        ( $package, $filename, $line, $subroutine, @other ) = caller($caller);
    }

    return 1;
}

#   deprecate
#   - warns of method deprecation
sub deprecate {
    my ( $self, $args ) = @_;

    if ( $self->check_arguments( ['-method'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   method
    my $method = $args->{'-method'};

    my $caller = 1;
    my ( $package, $filename, $line, $subroutine, @other ) = caller($caller);

    $self->stack( { '-text' => 'Method ' . $subroutine . ' is deprecated, use method ' . $method . ' instead' } );

    return 1;
}

#   unprefix keys
#   - convert any hash key beginning with a hyphen to a
#     key without a hyphen
sub unprefix_keys {
    my ( $self, $data ) = @_;

    #   process hash
    if ( ref($data) eq 'HASH' ) {
        my @keys = ( keys %{$data} );
        foreach my $old_key (@keys) {
            my $new_key = $old_key;
            if ( $old_key =~ /^\-/sxmg ) {
                $new_key =~ s/^\-//sxmg;
                $data->{$new_key} = $data->{$old_key};
                delete $data->{$old_key};
            }

            if ( ref( $data->{$new_key} ) eq 'HASH' ) {

                #   process hash
                $data->{$new_key} = $self->unprefix_keys( $data->{$new_key} );

            }
            elsif ( ref( $data->{$new_key} ) eq 'ARRAY' ) {

                #   process array
                my $new_array = [];
                foreach my $temp_data ( @{ $data->{$new_key} } ) {
                    push @{$new_array}, $self->unprefix_keys($temp_data);
                }
                $data->{$new_key} = $new_array;
            }
        }
    }
    elsif ( ref($data) eq 'ARRAY' ) {

        #   process array
        my $new_array = [];
        foreach my $temp_data ( @{$data} ) {
            push @{$new_array}, $self->unprefix_keys($temp_data);
        }
        $data = $new_array;
    }
    return $data;
} ## end sub unprefix_keys

#   prefix keys
#   - convert any hash key without a hyphen prefix to
#     a key with a hyphen prefix
sub prefix_keys {
    my ( $self, $data ) = @_;

    $self->debug( { '-text' => 'Have -data ' . $self->dbg_value($data) } );

    #   process hash
    if ( ref($data) eq 'HASH' ) {
        my @keys = ( keys %{$data} );
        foreach my $old_key (@keys) {
            my $new_key = $old_key;
            if ( $old_key !~ /^\-/sxmg ) {
                $new_key = '-' . $old_key;
                $data->{$new_key} = $data->{$old_key};
                delete $data->{$old_key};
            }

            if ( ref( $data->{$new_key} ) eq 'HASH' ) {

                #   process hash
                $data->{$new_key} = $self->prefix_keys( $data->{$new_key} );

            }
            elsif ( ref( $data->{$new_key} ) eq 'ARRAY' ) {

                #   process array
                my $new_array = [];
                foreach my $temp_data ( @{ $data->{$new_key} } ) {
                    push @{$new_array}, $self->prefix_keys($temp_data);
                }
                $data->{$new_key} = $new_array;
            }
        }
    }
    elsif ( ref($data) eq 'ARRAY' ) {

        #   process array
        my $new_array = [];
        foreach my $temp_data ( @{$data} ) {
            push @{$new_array}, $self->prefix_keys($temp_data);
        }
        $data = $new_array;
    }

    return $data;
} ## end sub prefix_keys

#
#   encrypt a string
#
sub encrypt_string {
    my ( $self, $args ) = @_;

    my $string         = $args->{'-string'};
    my $encrypt_string = undef;

    if ( defined $string ) {
        my $context = Digest::MD5->new();
        $context->add($string);
        $encrypt_string = $context->b64digest();
    }

    return $encrypt_string;
}

#
#   create a random key for account activation / login purposes
#
sub random_key {
    my ( $self, $args ) = @_;

    my $string;

    my $string_length = $args->{'-length'};
    if ( !$string_length ) {
        $string_length = 24;
    }

    my $charset = <<'CHARSET';
a b c d e f g h i j k l m n o
p q r s t u v w x y z 
0 1 2 3 4 5 6 7 8 9 A B C D E F
G H I J K L M N O P Q R S T U V
W X Y Z
CHARSET

    if ( ( !defined $args->{'-special_characters'} ) ||
        ( $args->{'-special_characters'} == 1 ) ) {
        $charset .= <<'CHARSET';
| _ - $ ! .
CHARSET
    }

    my @chars = split( ' ', $charset );

    for ( 1 .. $string_length ) {
        my $idx = int( rand( ( scalar @chars ) - 1 ) );
        $string .= $chars[$idx];
    }

    return $string;
} ## end sub random_key

#
#   cache methods
#

#   init cache
sub init_cache {
    my ( $self, $args ) = @_;

    return AlienSpaces::Cache->init_cache( { '-namespace' => $self->environment->{'APP_PERL_NAMESPACE'} } );
}

#   set cached
sub set_cached {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-key', '-value' ], $args ) ) {
        return;
    }

    return AlienSpaces::Cache->set_cached($args);
}

#   get cache key
sub get_cache_key {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-cache_key_args'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    return AlienSpaces::Cache->get_cache_key($args);
}

#   get cached
sub get_cached {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-key'], $args ) ) {
        return;
    }

    return AlienSpaces::Cache->get_cached($args);
}

#   clear cached
sub clear_cached {
    my ( $self, $args ) = @_;

    return AlienSpaces::Cache->clear_cached($args);
}

###########################################
#
#   init_errors
#   set_error
#   get_errors
#
#   methods to set and get errors codes and
#   error text accessible at the global
#   level via the Util::Error class
#
###########################################

#   errors
my $HAS_ERROR = 0;

#   init errors
sub init_errors {
    my ( $self, $args ) = @_;

    $HAS_ERROR = 0;

    return 1;
}

#   error code text
sub error_code_text {
    my ( $self, $args ) = @_;

    return {

        #   global
        ERROR_CODE_UNAUTHORIZED() => 'Request requires authorization',

        ERROR_CODE_API_FAILED_INPUT_PARSE() => 'Input string invalid format',

        ERROR_CODE_API_MISSING_COMPONENT() => 'Request missing component name',
        ERROR_CODE_API_MISSING_ACTION()    => 'Request missing action',

        #   user
        ERROR_CODE_USER_NOT_LOGGED_IN()            => 'User not logged in',
        ERROR_CODE_USER_BANNED()                   => 'User account banned',
        ERROR_CODE_USER_PENDING_ACTIVATION()       => 'User account pending activation',
        ERROR_CODE_USER_STATUS_CHANGE_INVALID()    => 'User account status change invalid',
        ERROR_CODE_USER_STATUS_CHANGE_DISALLOWED() => 'User account status change not allowed',

        ERROR_CODE_USER_HANDLE_NOT_UNIQUE()        => 'User handle is not unique',
        ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE() => 'User email address is not unique',
    };
}

#   has error
sub has_error {
    my ( $self, $args ) = @_;

    return $HAS_ERROR;
}

#   set error
sub set_error {
    my ( $self, $args ) = @_;

    #   error text
    my $error_text = $args->{'-error_text'};
    if ( !defined $error_text ) {
        $args->{'-error_text'} = $self->get_error_code_text($args);
    }

    #   error util
    my $error_util = $self->utils->{ UTIL_ERROR() };
    if ( !defined $error_util ) {
        $self->debug( { '-text' => 'Class ' . ref($self) . ' does not have error util' } );
        return;
    }

    #   set error
    if ( !$error_util->set_error($args) ) {
        $self->error( { '-text' => 'Class ' . ref($self) . ' failed to set error -args ' . $self->dbg_value($args) } );
        return;
    }

    #   has errors
    $HAS_ERROR = 1;

    return 1;
}

#   get errors
sub get_errors {
    my ( $self, $args ) = @_;

    #   error util
    my $error_util = $self->utils->{ UTIL_ERROR() };
    if ( !defined $error_util ) {
        $self->debug( { '-text' => 'Class ' . ref($self) . ' does not have error util' } );
        return;
    }

    #   get errors
    return $error_util->get_errors($args);
}

#   get error code text
sub get_error_code_text {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-error_code'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   error code
    my $error_code = $args->{'-error_code'};

    #   error code text
    my $error_text = $self->error_code_text->{$error_code};
    if ( !defined $error_text ) {
        $error_text = 'Unknown error occurred';
    }

    foreach my $arg ( keys %{$args} ) {
        my $val = $args->{$arg};
        $arg =~ s/^\-//sxmg;
        $error_text =~ s/\[$arg\]/$val/sxmg;
    }

    return $error_text;
}

#
#   message methods
#

#   add program message
sub add_program_message {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   store for a specific user if we have an
    #   avaialble user id
    if ( defined $user_util ) {
        if ( !defined $args->{'-user_id'} ) {
            $args->{'-user_id'} = $user_util->user_id;
            $args->{'-store'}   = 1;
        }
    }

    #   name
    if ( !defined $args->{'-name'} ) {
        if ( $self->can('program_name') ) {
            $args->{'-name'} = $self->program_name;
        }
    }

    $self->add_message($args);

    return 1;
}

#   add component message
sub add_component_message {
    my ( $self, $args ) = @_;

    #   name
    if ( !defined $args->{'-name'} ) {
        if ( $self->can('name') ) {
            $args->{'-name'} = $self->name;
        }
    }

    $self->add_message($args);

    return 1;
}

#   add message
sub add_message {
    my ( $self, $args ) = @_;

    #   message util
    my $message_util = $self->utils->{ UTIL_MESSAGE() };
    if ( !defined $message_util ) {
        $self->debug( { '-text' => 'Class ' . ref($self) . ' does not have message util' } );
        return;
    }

    #   name
    my $name = $args->{'-name'};

    #   type
    my $type = $args->{'-type'};
    if ( !defined $type ) {
        $type = $self->MESSAGE_TYPE_INFO;
    }

    #   text
    my $text = $args->{'-text'};

    $message_util->add_message(
        {   '-name' => $name,
            '-type' => $type,
            '-text' => $text,
        } );

    return 1;
} ## end sub add_message

#   get program messages
sub get_program_messages {
    my ( $self, $args ) = @_;

    #   name (program name)
    if ( !defined $args->{'-name'} ) {
        if ( $self->can('program_name') ) {
            $args->{'-name'} = $self->program_name;
        }
    }

    return $self->get_messages($args);
}

#   get component messages
sub get_component_messages {
    my ( $self, $args ) = @_;

    #   name (component name)
    my $name = $args->{'-panel'} || $args->{'-component'} || $args->{'-name'};
    if ( !defined $name ) {
        if ( $self->can('name') ) {
            $name = $self->name;
        }
    }

    return $self->get_messages($args);
}

#   get messages
#   - fetches messages from both the message utility and the error utility bundling them altogether
#   - message utility simply provides the concept of type and also allows storage of messages
#     on the database
sub get_messages {
    my ( $self, $args ) = @_;

    #   message util
    my $message_util = $self->utils->{ UTIL_MESSAGE() };
    if ( !defined $message_util ) {
        $self->debug( { '-text' => 'Class ' . ref($self) . ' does not have message util' } );
        return;
    }

    #   type
    my $type = $args->{'-type'};

    #   name
    my $name = $args->{'-name'};

    #   messages
    my $messages = $message_util->get_messages(
        {   '-name' => $name,
            '-type' => $type,
        } );

    return $messages;
}

#
#   log methods
#
my $BACKLOG = [];

sub write_log {
    my ( $self, $args ) = @_;

    my $method = $args->{'-method'};
    my $text   = $args->{'-text'};

    my $class = ref($self);

    #   logger
    my $logger = Log::Log4perl->get_logger($class);
    if ( !defined $logger ) {

        #        print 'Logger not defined for -class ' . $class . "\n";

        push @{$BACKLOG},
            {
            '-method' => $method,
            '-text'   => $text,
            };
        return;
    }

    if ( !Log::Log4perl->initialized ) {

        #        print 'Logger not initialized for -class ' . $class . "\n";

        push @{$BACKLOG},
            {
            '-method' => $method,
            '-text'   => $text,
            };
        return;
    }

    if ( scalar @{$BACKLOG} ) {

        #        print 'Emptying back log now' . "\n";

        while ( my $backlog_line = shift @{$BACKLOG} ) {
            my $backlog_method = $backlog_line->{'-method'};
            my $backlog_text   = $backlog_line->{'-text'};

            $logger->$backlog_method($backlog_text);
        }
    }

    return $logger->$method($text);
} ## end sub write_log

sub debug {
    my ( $self, $args ) = @_;

    if ( !$self->debug_enabled ) {
        return;
    }

    return $self->write_log( { '-method' => 'debug', '-text' => $args->{'-text'} } );
}

sub error {
    my ( $self, $args ) = @_;

    if ( !$self->error_enabled ) {
        return;
    }

    $self->write_log( { '-method' => 'error', '-text' => $args->{'-text'} } );

    if ( $self->carp_enabled() ) {
        carp $args->{'-text'};
    }

    return;
}

sub trace {
    my ( $self, $args ) = @_;

    if ( !$self->trace_enabled ) { return; }

    return $self->write_log( { '-method' => 'trace', '-text' => $args->{'-text'} } );
}

sub info {
    my ( $self, $args ) = @_;

    if ( !$self->info_enabled ) { return; }

    return $self->write_log( { '-method' => 'info', '-text' => $args->{'-text'} } );
}

sub fatal {
    my ( $self, $args ) = @_;

    if ( !$self->fatal_enabled ) { return; }

    $self->write_log( { '-method' => 'fatal', '-text' => $args->{'-text'} } );

    croak $args->{'-text'};
}

#   debug value
my $DBG_RECURSE_COUNT = 0;

sub dbg_value {
    my ( $self, $value ) = @_;

    $Data::Dumper::Indent = 1;

    return Dumper($value);
}

#   cleanup methods
sub cleanup {
    my ( $self, $args ) = @_;

    #   unload utils
    $self->unload_utils();

    #   backlog
    if ( scalar @{$BACKLOG} ) {
        while ( my $backlog_line = shift @{$BACKLOG} ) {
            my $backlog_method = $backlog_line->{'-method'};
            my $backlog_text   = $backlog_line->{'-text'};

            carp "[$backlog_method] $backlog_text";
        }
    }

    return;
}

#   unload utils
sub unload_utils {
    my ( $self, $args ) = @_;

    $CLASS_UTILS_INIT = {};

    #   namespace
    my $namespace = $self->environment->{'APP_PERL_NAMESPACE'};

    #   call finish method on all util objects
    if ( !defined $CLASS_UTILS->{$namespace} ) {
        return;
    }

FINISH_UTILITIES:
    while ( my ( $util_key, $util_obj ) = each %{ $CLASS_UTILS->{$namespace} } ) {
        if ( !defined $util_obj ) {
            $self->trace( { '-text' => 'cannot finish -util_key ' . $util_key } );
            next FINISH_UTILITIES;
        }

        if ( $util_obj == -1 ) {
            $self->trace( { '-text' => 'cannot finish -util_key ' . $util_key . ' -object destroyed' } );
            next FINISH_UTILITIES;
        }

        if ( !$util_obj->finished() ) {
            $self->trace( { '-text' => 'finishing -util_key ' . $util_key } );
            $util_obj->finish();
        }
    }

    #
    #   fully destroy the util objects that have to be destroyed
    #
    my $util_clear_list = $self->util_clear_list();
    foreach my $util_name ( @{$util_clear_list} ) {

        $self->trace( { '-text' => 'Clearing ' . $util_name } );

        #   util file / class
        my ( $util_file, $util_class ) = $self->make_class(
            {   '-name' => $util_name,
                '-type' => CLASS_TYPE_UTIL
            } );

        if ( exists $CLASS_UTILS->{$namespace}->{$util_class} ) {
            $CLASS_UTILS->{$namespace}->{$util_class} = -1;
            $self->trace( { '-text' => 'Cleared ' . $CLASS_UTILS->{$namespace}->{$util_class} } );
        }
    }

    return;
} ## end sub unload_utils

sub finish {
    my ( $self, $args ) = @_;

    #   cleanup
    $self->cleanup();

    return 1;
}

1;

