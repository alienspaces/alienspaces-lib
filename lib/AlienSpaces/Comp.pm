package AlienSpaces::Comp;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use Apache2::Const qw(OK REDIRECT);

our $VERSION = '$Revision: $';

use base qw(AlienSpaces::Base);

use AlienSpaces::Constants qw(:util :interface :comp);

use AlienSpaces::Properties {
    'id'   => { '-type' => 's' },    #   component id
    'name' => { '-type' => 's' },    #   component name

    'redirect_url' => { '-type' => 's' },    #   url to redirect to

    'application_url'        => { '-type' => 's' },    #   application url (ie http://bridge2buy.com)
    'secure_application_url' => { '-type' => 's' },    #   secure application url
};

sub new {
    my ( $class, $args ) = @_;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super init failed' } );
        return;
    }

    #   load panel object utils
    $self->load_utils();

    return 1;
}

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = $self->SUPER::util_list();

    #   everyone
    push @{$util_list}, (
        UTIL_DB_MYSQL(),     # db mysql util
        UTIL_SESSION(),      # session util
        UTIL_USER(),         # user util
        UTIL_LANGUAGE(),     # language util
        UTIL_PHRASE(),       # phrase
        UTIL_MESSAGE(),      # standard messaging interface
        UTIL_ERROR(),        # error messaging interface
        UTIL_TIME_ZONE(),    # time zone util
    );

    #   html interface only
    if ( $self->interface eq INTERFACE_HTML() ) {
        push @{$util_list}, (
            UTIL_CGI(),         # html display / cgi parameter interface
            UTIL_MESSAGE(),     # messaging data interface
            UTIL_TEMPLATE(),    # html template toolkit abstraction

            UTIL_OUTPUT_HTML_PANEL(),         # html panel
            UTIL_OUTPUT_HTML_TABLE(),         # html table
            UTIL_OUTPUT_HTML_NAVIGATION(),    # html navigation
            UTIL_OUTPUT_HTML_DIALOGUE(),      # html dialogue
        );
    }

    return $util_list;
} ## end sub util_list

#   util object clear list
sub util_clear_list {
    my ( $self, $args ) = @_;

    my $clear_list = [];
    return $clear_list;
}

sub util_new_args {
    my ( $self, $args ) = @_;

    #   super args
    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    #   global
    $util_args->{'-program_name'} = $self->program_name;

    #   specific
    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

sub load_utils {
    my ( $self, $args ) = @_;

    return $self->SUPER::load_utils( { '-scope' => 'global', } );
}

#
#   util shortcut methods
#
sub dbh {
    my ( $self, $args ) = @_;

    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };

    return $db_mysql_util;
}

sub cgi {
    my ( $self, $args ) = @_;

    my $cgi_util = $self->utils->{ UTIL_CGI() };

    return $cgi_util;
}

sub template {
    my ( $self, $args ) = @_;

    my $template_util = $self->utils->{ UTIL_TEMPLATE() };

    return $template_util;
}

sub session {
    my ( $self, $args ) = @_;

    my $session_util = $self->utils->{ UTIL_SESSION() };

    return $session_util;
}

sub panel {
    my ( $self, $args ) = @_;

    my $panel_util = $self->utils->{ UTIL_OUTPUT_HTML_PANEL() };

    return $panel_util;
}

sub table {
    my ( $self, $args ) = @_;

    my $table_util = $self->utils->{ UTIL_OUTPUT_HTML_TABLE() };

    return $table_util;
}

sub navigation {
    my ( $self, $args ) = @_;

    my $navigation_util = $self->utils->{ UTIL_OUTPUT_HTML_NAVIGATION() };

    return $navigation_util;
}

sub dialogue {
    my ( $self, $args ) = @_;

    my $dialogue_util = $self->utils->{ UTIL_OUTPUT_HTML_DIALOGUE() };

    return $dialogue_util;
}

sub user {
    my ( $self, $args ) = @_;

    my $user_util = $self->utils->{ UTIL_USER() };

    return $user_util;
}

#
#   base methods panel sub-classes should implement
#
sub render {
    my ( $self, $args ) = @_;

    return '';
}

sub action {
    my ( $self, $args ) = @_;

    return COMP_ACTION_FAILURE;
}

sub data {
    my ( $self, $args ) = @_;

    return;
}

###########################################
#
#   process_meta_data
#
#   Remove any keys from a structure that cannot be found
#   in array 'meta'
#   The keys in array 'meta' may or may not have a leading
#   hyphen
#
###########################################

#   process meta data
sub process_meta_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-data', '-meta' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   data
    my $data = $args->{'-data'};

    #   data must be an array of structures
    if ( ref($data) ne 'ARRAY' ) {
        $data = [$data];
    }

    #   meta
    my $meta = $args->{'-meta'};

    #   meta must be an array of keys
    #   - we will turn it into a hash..
    if ( ref($meta) ne 'ARRAY' ) {
        $meta = [$meta];
    }

    my $keep_k = {};
    foreach my $meta_k ( @{$meta} ) {
        $keep_k->{$meta_k} = 1;
        if ( $meta_k =~ s/^\-//sxmg ) {
            $keep_k->{$meta_k} = 1;
        }
        else {
            $keep_k->{ '-' . $meta_k } = 1;
        }
    }

    $self->debug( { '-text' => 'Keeping ' . $self->dbg_value($keep_k) } );

PROCESS_DATA:
    foreach my $data_i ( @{$data} ) {
        if ( ref($data_i) ne 'HASH' ) {
            next PROCESS_DATA;
        }
        foreach my $k ( keys %{$data_i} ) {
            if ( !exists $keep_k->{$k} ) {
                $self->debug( { '-text' => 'Removing ' . $k } );
                delete $data_i->{$k};
            }
        }
    }

    return 1;
} ## end sub process_meta_data

###########################################
#
#   implicit_action_list
#   implicit_action
#   implicit_action_set_language
#   implicit_action_set_timezone
#
#   Implicit action methods that will be called
#   automatically
#   Actions return by method implicit_action_list
#   must have an accompanying method to support
#   the action.
#
#   Example: action 'set_language' must have a
#   method 'implicit_action_set_language'
#   implemented.
#
###########################################
sub implicit_action_list {
    my ( $self, $args ) = @_;

    #   return an array of implicit action constants (:comp constant tag)
    return [];
}

sub implicit_action {
    my ( $self, $args ) = @_;

    my $implicit_action_list = $self->implicit_action_list;
    if ( !defined $implicit_action_list || !scalar @{$implicit_action_list} ) {
        $self->debug( { '-text' => 'No implicit actions configured for component' } );
        return COMP_ACTION_FAILURE;
    }

    my $implicit_return_status = COMP_ACTION_FAILURE;

PROCESS_IMPLICIT_ACTIONS:
    foreach my $implicit_action ( @{$implicit_action_list} ) {
        my $method = 'implicit_action_' . $implicit_action;

        my $success = eval {
            $implicit_return_status = $self->$method($args);
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Error executing implicit action -method ' . $self->dbg_value($method) . ' :' . $EVAL_ERROR } );
            next PROCESS_IMPLICIT_ACTIONS;
        }
    }

    return $implicit_return_status;
}

sub implicit_action_set_language {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-data'], $args ) ) {
        return;
    }

    $self->dbg_value( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    #   language util
    my $language_util = $self->utils->{ UTIL_LANGUAGE() };
    if ( !defined $language_util ) {
        $self->error( { '-text' => 'Missing language util, cannot implicit action set language' } );
        return COMP_ACTION_FAILURE;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };
    if ( !defined $session_util ) {
        $self->error( { '-text' => 'Missing session util, cannot implicit action set language' } );
        return COMP_ACTION_FAILURE;
    }

    my $data = $args->{'-data'};

    my $language_id = $data->{'language_id'};
    my $language    = $data->{'language'};

    if ( !defined $language_id && !defined $language ) {
        $self->debug( { '-text' => 'Missing optional -language_id and -language, cannnot implicit action set language' } );
        return;
    }

    my $set_language = $language_util->fetch_language(
        {   '-language_id' => $language_id,
            '-code'        => $language,
            '-active'      => 1,
        } );
    if ( !defined $set_language ) {
        $self->error( { '-text' => 'Unable to find -langauge_id ' . $self->dbg_value($language_id) . ' -language ' . $self->dbg_value($language) . ', cannot implicit action set language' } );
        return COMP_ACTION_FAILURE;
    }

    #   set new langauge for this session
    $session_util->set_session( { '-key' => '-language_id', '-value' => $set_language->{'-language_id'} } );

    $self->debug( { '-text' => 'Set -language_id ' . $set_language->{'-language_id'} } );

    return COMP_ACTION_SUCCESS;
} ## end sub implicit_action_set_language

sub implicit_action_set_time_zone {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-data'], $args ) ) {
        return;
    }

    #   time zone util
    my $time_zone_util = $self->utils->{ UTIL_TIME_ZONE() };
    if ( !defined $time_zone_util ) {
        $self->error( { '-text' => 'Missing time_zone util, cannot implicit action set time zone' } );
        return COMP_ACTION_FAILURE;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };
    if ( !defined $session_util ) {
        $self->error( { '-text' => 'Missing session util, cannot implicit action set time zone' } );
        return COMP_ACTION_FAILURE;
    }

    my $data = $args->{'-data'};

    my $time_zone_id = $data->{'time_zone_id'};
    my $time_zone = $data->{'time_zone'} || $data->{'time_zone'};

    if ( !defined $time_zone_id && !defined $time_zone ) {
        $self->debug( { '-text' => 'Missing optional -time_zone_id and -time_zone, cannnot implicit action set language' } );
        return;
    }

    my $set_time_zone = $time_zone_util->fetch_time_zone(
        {   '-time_zone_id' => $time_zone_id,
            '-name'         => $time_zone,
        } );
    if ( !defined $set_time_zone ) {
        $self->error(
            { '-text' => 'Unable to find -time_zone_id ' . $self->dbg_value($time_zone_id) . ' -time_zone_name ' . $self->dbg_value($time_zone) . ', cannot implicit action set time_zone' } );
        return COMP_ACTION_FAILURE;
    }

    #   set new langauge for this session
    $session_util->set_session( { '-key' => '-time_zone_id', '-value' => $set_time_zone->{'-time_zone_id'} } );

    $self->debug( { '-text' => 'Set -time_zone_id ' . $set_time_zone->{'-time_zone_id'} } );

    return COMP_ACTION_SUCCESS;

} ## end sub implicit_action_set_time_zone

#   - deprecated
sub render_links {
    my ( $self, $args ) = @_;

    #   name
    if ( !defined $args->{'-name'} ) {
        if ( $self->can('name') ) {
            $args->{'-name'} = $self->name;
        }
    }

    return $self->navigation->render($args);
}

#   set redirect url
sub set_redirect_url {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-url'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   url
    my $url = $args->{'-url'};

    #   redirect url
    $self->redirect_url($url);

    return $url;
}

#   get redirect url
sub get_redirect_url {
    my ( $self, $args ) = @_;

    #   redirect url
    return $self->redirect_url();
}

#   redirect
sub redirect {
    my ( $self, $args ) = @_;

    $self->deprecate( { '-method' => 'set_redirect_url' } );

    return $self->set_redirect_url($args);
}

1;

__END__

