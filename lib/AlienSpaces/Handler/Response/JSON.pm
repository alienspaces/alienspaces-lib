package AlienSpaces::Handler::Response::JSON;

#
#   $Revision: 155 $
#   $Author: bwwallin $
#   $Date: 2010-07-01 18:43:31 +1000 (Thu, 01 Jul 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use base qw(AlienSpaces::Base);
use AlienSpaces::Handler::Directive;

use Apache2::Const qw(:http);
use Apache2::Response;
use Apache2::RequestRec;
use Apache2::RequestUtil;
use Apache2::RequestIO;
use Apache2::Log;
use APR::Table;
use APR::Pool;

use Readonly qw( );

Readonly::Array our @REQUIRED_DIRECTIVES => qw( );

our $VERSION = (qw$Revision: 155 $)[1];

sub handler : method {
    my ( $class, $apache_request ) = @_;

    #   log
    my $log = $apache_request->log;

    $log->info( __PACKAGE__, ' Running -path_info ' . $apache_request->path_info . ' -unparsed_uri ' . $apache_request->unparsed_uri );

    #   .offline
    my $offline_file = $class->environment->{'APP_WEB_HOME'} . '/.offline';
    if ( -e $offline_file ) {
        $log->info( __PACKAGE__, ' Site is offline' );
        $apache_request->content_type('text/html');
        $apache_request->print('<p>Site is currently offline</p>');
        return Apache2::Const::OK;
    }

    my $program_name = $apache_request->path_info;
    $program_name =~ s/^\///sxm;

    #   program interface / program class
    my ( $interface, $interface_class ) = $class->get_interface_class(
        {   '-unparsed_uri'   => $apache_request->unparsed_uri,
            '-apache_request' => $apache_request,
        } );

    if ( !defined $interface_class ) {
        $log->error( __PACKAGE__, ' Failed to identify -interface_class ' . ( defined $interface_class ? $interface_class : undef ) );
        return Apache2::Const::NOT_FOUND;
    }

    $log->info( __PACKAGE__, ' Executing -interface_class ' . $interface_class );

    #   instantiate program object
    my $interface_object;
    my $success = eval {
        $interface_object = $interface_class->new(
            {   '-apache_request' => $apache_request,
                '-program_name'   => $program_name,
                '-hostname'       => $apache_request->hostname,
                '-interface'      => $interface,
            } );
        1;
    };

    if ( !$success ) {
        $log->error( __PACKAGE__, ' Failed instantiating program class :' . $EVAL_ERROR );
        return Apache2::Const::NOT_FOUND;
    }

    if ( !defined $interface_object ) {
        $log->error( __PACKAGE__, '  Failed instantiating program class' );
        return Apache2::Const::NOT_FOUND;
    }

    #   initialise interface object
    $success = eval {

        #   init
        $interface_object->init();
        1;
    };

    if ( !$success ) {
        $log->error( __PACKAGE__, ' Failed initialising class :' . $EVAL_ERROR );
        return Apache2::Const::NOT_FOUND;
    }

    #   parse input params
    if ( $interface_object->parse_input_params() ) {
        $log->info( __PACKAGE__, ' Parsed input parameters' );

        #   check interface authorize user
        if ( $interface_object->check_interface_authorize_user() ) {

            $log->info( __PACKAGE__, ' User authorized' );

            #   execute interface object
            $success = eval {

                #   main
                $interface_object->main();
                1;
            };

            if ( !$success ) {
                $log->error( __PACKAGE__, ' Failed executing class :' . $EVAL_ERROR );
                return Apache2::Const::NOT_FOUND;
            }
        }
        else {
            $log->info( __PACKAGE__, ' User not authorized' );
        }
    }
    else {
        $log->info( __PACKAGE__, ' Failed to parse input parameters' );
    }

    #   check interface render
    if ( $interface_object->check_interface_render() ) {

        #   main render
        my $content = $interface_object->main_render();
        if ( defined $content ) {
            $apache_request->print($content);
        }
    }

    #   finish
    $interface_object->finish();

    #   redirect url
    my $redirect_url = $interface_object->get_redirect_url();
    if ( defined $redirect_url ) {
        $interface_object->http_redirect(
            {   '-url'            => $redirect_url,
                '-apache_request' => $apache_request,
            } );
    }

    return Apache2::Const::OK;
} ## end sub handler :

sub get_interface_class {
    my ( $class, $args ) = @_;

    #   apache request
    my $apache_request = $args->{'-apache_request'};

    #   log
    my $log = $apache_request->log;

    my $path = $args->{'-unparsed_uri'};
    $path =~ s/[?].*$//sxmg;

    my $interface;
    if ( $path =~ /\/(.*?)\/(.*)?$/sxmg ) {
        $interface = $1;
    }
    else {
        return;
    }

    $log->info( __PACKAGE__, ' Unparsed URI is ' . $args->{'-unparsed_uri'} );
    $log->info( __PACKAGE__, ' IFace is ' . $interface );
    $log->info( __PACKAGE__, ' Path is ' . $path );

    $interface = uc($interface);

    #   class name
    my $class_name = $class->_make_class_from_path( { '-path' => $path, '-interface' => $interface } );

    #   namespaces
    my @namespaces = ( $class->environment->{'APP_PERL_NAMESPACE'}, $class->environment->{'APP_SUPER_PERL_NAMESPACE'}, );

    my $load_class_name;

FIND_CLASS:
    foreach my $namespace (@namespaces) {
        if ( !defined $namespace ) { next FIND_CLASS; }

        $log->info( __PACKAGE__, ' Program namespace ' . $namespace );

        #   load class name
        $load_class_name = $namespace . '::IFace::Web::' . $class_name;

        #   load class
        if ( $class->load_class( { '-class_name' => $load_class_name } ) ) {
            $log->info( ' Success load class -interface ' . $interface . ' -load_class_name ' . $load_class_name );
            last FIND_CLASS;
        }

        $log->info( 'Failed load class -interface ' . $interface . ' -load_class_name ' . $load_class_name );
        undef $load_class_name;
    }

    return ( $interface, $load_class_name );
} ## end sub get_interface_class

1;

