package AlienSpaces::Handler::Cleanup::Log;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;

use Apache2::Const '-compile' => qw(:http);
use APR::Const '-compile' => qw( :common );

use Apache2::RequestRec qw( );
use Apache2::Module qw( );

use base qw( AlienSpaces::Base );

use Readonly qw( );

Readonly::Array our @REQUIRED_DIRECTIVES => qw( );

our $VERSION = (qw$Revision: 2167 $)[1];

sub handler : method {
    my ( $class, $apache_request ) = @_;

    #   log
    my $log = $apache_request->log;

    $log->info( __PACKAGE__, 'Initializing logging' );

    #   directives
    my $directives = AlienSpaces::Handler::Directive->new(
        '-apache_request'      => $apache_request,
        '-required_directives' => \@REQUIRED_DIRECTIVES,
    );

    if ( !$directives->configured ) {
        return Apache2::Const::HTTP_INTERNAL_SERVER_ERROR;
    }

    $class->finish_logging( { 
        '-apache_request' => $apache_request,
        '-directives' => $directives, 
    } );

    return Apache2::Const::OK;
}

1;

__END__

