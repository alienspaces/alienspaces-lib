package AlienSpaces::Handler::Access::Offline;

#
#   display a specific HTML page when detecting
#   that the site is offline
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use base qw(AlienSpaces::Base);
use AlienSpaces::Handler::Directive;

use Apache2::Const qw(:http);
use Apache2::Response;
use Apache2::RequestRec;
use Apache2::RequestUtil;
use Apache2::RequestIO;
use Apache2::Log;
use APR::Table;
use APR::Pool;

use Readonly qw( );
use Config::JSON;

Readonly::Array our @REQUIRED_DIRECTIVES => qw( );

our $VERSION = (qw$Revision: 1 $)[1];

sub handler : method {
    my ( $class, $apache_request ) = @_;

    #   log
    my $log = $apache_request->log;

    $log->info( __PACKAGE__, ' Checking offline' );

    #   directives
    my $directives = AlienSpaces::Handler::Directive->new(
        '-apache_request'      => $apache_request,
        '-required_directives' => \@REQUIRED_DIRECTIVES
    );

    if ( !$directives->configured ) {
        return Apache2::Const::HTTP_INTERNAL_SERVER_ERROR;
    }

    my $offline_config_file = $class->environment->{'APP_OFFLINE_CONFIG_FILE'};

    $log->info( __PACKAGE__, ' Checking -offline_config_file ' . (defined $offline_config_file ? $offline_config_file : ''));
    if ( !defined $offline_config_file || ! -e $offline_config_file ) {
        $log->info( __PACKAGE__, ' No config file found, not offline' );
        return Apache2::Const::OK;
    }

    my $config = Config::JSON->new($offline_config_file);
    if ( !defined $config ) {
        $log->info( __PACKAGE__, ' No config in file, not offline' );
        return Apache2::Const::OK;
    }

    my $offline     = $config->get('offline');
    my $offline_url = $config->get('offline_url');
    
    $log->info( __PACKAGE__, ' Site -offline ' . (defined $offline ? $offline : '') . ' -offline_url ' . (defined $offline_url ? $offline_url : ''));

    if ( defined $offline_url && ( $offline eq '1' || $offline eq 'yes' ) ) {
        $log->info( __PACKAGE__, ' Site is offline' );
        $apache_request->headers_out->set( 'Location' => $offline_url );
        return Apache2::Const::REDIRECT;
    }

    $log->info( __PACKAGE__, ' Site is not offline' );

    return Apache2::Const::OK;
} ## end sub handler :

1;

__END__
