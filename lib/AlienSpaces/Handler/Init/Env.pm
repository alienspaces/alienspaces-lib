package AlienSpaces::Handler::Init::Env;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use base qw(AlienSpaces::Base);
use AlienSpaces::Handler::Directive;

use Apache2::Const qw(:http);
use Apache2::Response;
use Apache2::RequestRec;
use Apache2::RequestUtil;
use Apache2::RequestIO;
use Apache2::Log;
use APR::Table;
use APR::Pool;

use Readonly qw( );

Readonly::Array our @REQUIRED_DIRECTIVES => qw( );

our $VERSION = (qw$Revision: 2167 $)[1];

sub handler : method {
    my ( $class, $apache_request ) = @_;

    #   log
    my $log = $apache_request->log;

    $log->info( __PACKAGE__, ' Initializing environment' );

    #   subprocess env
    $apache_request->subprocess_env();

    #   directives
    my $directives = AlienSpaces::Handler::Directive->new( 
        '-apache_request'      => $apache_request, 
        '-required_directives' => \@REQUIRED_DIRECTIVES 
    );

    if ( !$directives->configured ) {
        return Apache2::Const::HTTP_INTERNAL_SERVER_ERROR;
    }

    #   init environment
    $class->init_environment( { 
        '-apache_request' => $apache_request, 
        '-directives'     => $directives,
    } );

    return Apache2::Const::OK;
}

1;

__END__
