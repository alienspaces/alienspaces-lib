package AlienSpaces::Handler::Directive;

#
#   $Revision: 155 $
#   $Author: bwwallin $
#   $Date: 2010-07-01 18:43:31 +1000 (Thu, 01 Jul 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw( carp croak);

use Apache2::Log;
use Apache2::CmdParms qw( );
use Apache2::Const '-compile' => qw( :cmd_how :common :log :override );
use Apache2::Directive qw( );
use Apache2::RequestRec qw( );
use Apache2::Module qw( );
use Apache2::ServerUtil qw( );
use APR::Const '-compile' => qw( :common );

use IO::Handle qw( );
use Readonly qw( );

our $VERSION = (qw$Revision: 3907 $)[1];

use namespace::autoclean;

use Moose;

has 'configured' => ( 'is' => 'rw', 'isa' => 'Bool' );

has 'APP_HOSTNAME'    => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_URL'         => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_HOME'        => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_WEB_HOME'    => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_NAME'        => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_CONFIG_FILE' => ( 'is' => 'rw', 'isa' => 'Str' );

has 'APP_PERL_NAMESPACE'          => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_PERL_SUPER_NAMESPACE'    => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_PERL_LIBRARY_PATH'       => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_PERL_SUPER_LIBRARY_PATH' => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_COOKIE_NAME'             => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_NOREPLY_EMAIL'           => ( 'is' => 'rw', 'isa' => 'Str' );

has 'APP_OFFLINE_CONFIG_FILE' => ( 'is' => 'rw', 'isa' => 'Str' );

has 'APP_SHELL_DB_USERNAME' => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_SHELL_DB_PASSWORD' => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_SHELL_DB'          => ( 'is' => 'rw', 'isa' => 'Str' );

has 'APP_WEB_DB_USERNAME' => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_WEB_DB_PASSWORD' => ( 'is' => 'rw', 'isa' => 'Str' );
has 'APP_WEB_DB'          => ( 'is' => 'rw', 'isa' => 'Str' );

has 'APP_ALIENSPACES_APPLICATION_URL' => ( 'is' => 'rw', 'isa' => 'Str' );

around qr/^APP/ms => sub {

    my $orig = shift;
    my $self = shift;

    if (@_) {
        if ( eval { $_[0]->isa('Apache2::CmdParms') } ) {
            my $cmdparms = shift;

           #            carp 'self >' . $self . '< method >' . $cmdparms->directive->directive . '< args >' . join( ',', @_ ) . '<';
        }
    }
    return $self->$orig(@_);
};

#
# constants
#
Readonly::Scalar my $CALLER_FILENAME => 1;
Readonly::Scalar my $CALLER_LINE     => 2;

#   directives
my @DIRECTIVES = (
    {   'name'         => 'APP_HOSTNAME',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_URL',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_HOME',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_WEB_HOME',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_NAME',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_CONFIG_FILE',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },

    {   'name'         => 'APP_PERL_NAMESPACE',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_PERL_SUPER_NAMESPACE',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_PERL_LIBRARY_PATH',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_PERL_SUPER_LIBRARY_PATH',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },

    {   'name'         => 'APP_COOKIE_NAME',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_NOREPLY_EMAIL',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },

    #   offline functionality
    {   'name'         => 'APP_OFFLINE_CONFIG_FILE',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },

    #   database connectivity
    {   'name'         => 'APP_SHELL_DB_USERNAME',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_SHELL_DB_PASSWORD',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_SHELL_DB',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },

    {   'name'         => 'APP_WEB_DB_USERNAME',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_WEB_DB_PASSWORD',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
    {   'name'         => 'APP_WEB_DB',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },

    #   other
    {   'name'         => 'APP_ALIENSPACES_APPLICATION_URL',
        'req_override' => Apache2::Const::ACCESS_CONF,
        'args_how'     => Apache2::Const::TAKE1,
        'errmsg'       => '',
    },
);

sub BUILD {
    my ( $self, $args ) = @_;

    my $class = ref($self);

    my $apache_request = $args->{'-apache_request'};

    #   log
    my $log = $apache_request->log;

    #   server scope configuration
    my $server_cfg = Apache2::Module::get_config( $class, Apache2::ServerUtil->server );

    foreach my $key ( sort keys %{$server_cfg} ) {
        $log->debug( __PACKAGE__, 'server key >' . $key . '< value >' . $server_cfg->{$key} . '<' );
        $self->$key( $server_cfg->{$key} );
    }

    #   virtual host scope configuration
    my $vhost_cfg = Apache2::Module::get_config( $class, $apache_request->server );

    foreach my $key ( sort keys %{$vhost_cfg} ) {
        $log->debug( __PACKAGE__, 'vhost key >' . $key . '< value >' . $vhost_cfg->{$key} . '<' );
        $self->$key( $vhost_cfg->{$key} );
    }

    #   directory scope configuration
    my $dir_cfg = Apache2::Module::get_config( $class, $apache_request->server, $apache_request->per_dir_config );

    foreach my $key ( sort keys %{$dir_cfg} ) {
        $log->debug( __PACKAGE__, 'dir key >' . $key . '< value >' . $dir_cfg->{$key} . '<' );
        $self->$key( $dir_cfg->{$key} );
    }

    #   validated required
    my $errors = 0;

    foreach my $required ( @{ $args->{'-required_directives'} } ) {

        if ( defined $args->{'-quiet'} && $args->{'-quiet'} ) {
            next;
        }

        if ( defined $self->{$required} && $self->{$required} ) {
            $log->debug( __PACKAGE__,
                $required . ' >' .
                    ( ref $self->$required eq 'ARRAY' ? ( join ' ', @{ $self->$required } ) : $self->$required ) . '<' );
        }
        else {

            $log->warn( __PACKAGE__, 'missing required >' . $required . '<' );
            $errors++;
        }
    }

    if ( $errors == 0 ) {
        $self->configured(1);
    }
    else {
        croak 'Missing required configuration';
    }

    return;
} ## end sub BUILD

#
# dump configuration directive help
#
sub _help {

    STDERR->print("\n");

    foreach my $directive (@DIRECTIVES) {
        STDERR->print( $directive->{'name'} . "\n" );
        STDERR->print( '    scope = ' . _scope( $directive->{'req_override'} ) . "\n" );
        STDERR->print( '    usage = ' . $directive->{'errmsg'} . "\n\n" );
    }

    return;
}

#
# initialize class
#
sub _initialize {
    my ($class) = @_;

    eval {
        my $s = Apache2::ServerUtil->server;
        Apache2::Module::add( $class, \@DIRECTIVES );
        1;
    } or
        do {
        Carp::croak $class . '->initialize error >' . $EVAL_ERROR . '<';
        };

    return;
}

# merge apache configuration
#
sub _merge {

    my $base = shift;
    my $add  = shift;

    my %copy = %{$base};

    foreach my $key ( sort keys %{$add} ) {
        if ( exists( $add->{$key} ) ) {
            $copy{$key} = $add->{$key};
        }
    }

    return bless \%copy, ref($base);
}

#
# english decode of Apache configuration scope flags
#
sub _scope {

    my ($flags) = @_;

    my @result = ();

    if ( ( $flags & Apache2::Const::RSRC_CONF ) || ( $flags & Apache2::Const::OR_ALL ) ) {
        push( @result, 'Server' );
        push( @result, '<VirtualHost>' );
    }

    if ( ( $flags & Apache2::Const::ACCESS_CONF ) || ( $flags & Apache2::Const::OR_ALL ) ) {
        push( @result, '<Directory(Match)>' );
        push( @result, '<Location(Match)>' );
        push( @result, '<Files(Match)>' );
    }

    if ( ( $flags & Apache2::Const::OR_AUTHCFG ) ||
        ( $flags & Apache2::Const::OR_FILEINFO ) ||
        ( $flags & Apache2::Const::OR_INDEXES )  ||
        ( $flags & Apache2::Const::OR_LIMIT )    ||
        ( $flags & Apache2::Const::OR_OPTIONS ) ) {
        my @override = ();
        if ( $flags & Apache2::Const::OR_AUTHCFG ) {
            push( @override, 'AuthConfig' );
        }
        if ( $flags & Apache2::Const::OR_FILEINFO ) {
            push( @override, 'FileInfo' );
        }
        if ( $flags & Apache2::Const::OR_INDEXES ) {
            push( @override, 'Indexes' );
        }
        if ( $flags & Apache2::Const::OR_LIMIT ) {
            push( @override, 'Limit' );
        }
        if ( $flags & Apache2::Const::OR_OPTIONS ) {
            push( @override, 'Options' );
        }
        push( @result, '.htaccess AllowOverride (' . join( '|', @override ) . ')' );
    }

    return ( join( ', ', @result ) );

} ## end sub _scope

#
# Apache2::Module configuration
#
sub SERVER_MERGE { return _merge(@_) }
sub DIR_MERGE    { return _merge(@_) }

#
# initialize class data
#
if ( exists( $ENV{'MOD_PERL'} ) ) {
    __PACKAGE__->_initialize;
}

__PACKAGE__->meta->make_immutable;

1;

__END__

