package AlienSpaces::Comp::Test;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   Generic test component expected to be able to differentiate between all types of interfaces
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :interface);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    my $interface = $args->{'-interface'};
    
    my $content;
    if ($interface eq INTERFACE_JSON) {
        my $content = {
            'request_data' => $args,
            'response_data' => 'All Ok',
        };
    } elsif ($interface eq INTERFACE_HTML) {
        #   cgi util
        my $cgi_util = $self->utils->{ UTIL_CGI() };
        
        my $content;
        foreach my $key (keys %{$args}) {
            $content .= $cgi_util->div({}, 'Have args -key ' . $key . ' -value ' . $self->dbg_value($args->{'-key'}));
        }
    }
            
    return $content;
}

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $action = delete $args->{'-action'};
    my $data   = $args->{'-data'};
    
    #
    #   handle action
    #

    return $self->COMP_ACTION_SUCCESS;
}

1;

