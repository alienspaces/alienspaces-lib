package AlienSpaces::Comp::JSON::API;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use constant {
    DEFAULT_ACTION_METHOD         => 'api_action',
    DEFAULT_VALIDATE_METHOD       => 'api_validate',
    DEFAULT_VALIDATE_FIELD_METHOD => 'api_validate_param',
    DEFAULT_LIST_VALUES_METHOD    => 'api_list_values',

    ERROR_CODE_ACTION_UNSUPPORTED => 10,
    ERROR_CODE_VAL_REQUIRED       => 11,
    ERROR_CODE_VAL_MAX_LENGTH     => 12,
    ERROR_CODE_VAL_MIN_LENGTH     => 13,
    ERROR_CODE_VAL_MATCH          => 14,
    ERROR_CODE_VAL_VALUES         => 15,

    MAX_SANITIZE_DEPTH => 10,
};

use AlienSpaces::Properties { 'response_data' => { '-type' => 's' }, };

#   api config methods
sub api_config {
    my ( $self, $args ) = @_;

    #   api config
    my $api_config = {

        '-actions' => {
            'xxxx' => {

                #   params
                '-params' => [
                    {   '-name'            => 'xxx',    #   input param name
                        '-validate_method' => 'xxx',    #   validation method specifically for this param
                                                        #   - defaults to 'api_validate_param'
                                                        #   - argument -name of param is passed in
                                                        #   - return (truthy|falsey)
                                                        #   validation rules
                        '-validate_rules'  => {
                            '-required'          => '1|0',    #   required
                            '-max_length'        => 'xxx',    #   max length
                            '-min_length'        => 'xxx',    #   min length
                            '-do_match'          => 'xxx',    #   must match this regular expression
                            '-dont_match'        => 'xxx',    #   must not match this regular expression
                            '-values_method'     => 'xxx',    #   Source values using this method,
                                                              #   - defaults to 'api_list_values'
                                                              #   - argument -name of param is passed in
                            '-values_table_name' => 'xxx',    #   Source values from this table
                        },
                        '-description' => 'xxx',              #   human readable description of parameter
                    },
                ],
                '-action_method'   => 'xxx',                  #   defaults to 'api_action'
                '-validate_method' => 'xxx',                  #   defaults to 'api_validate'
                                                              #   - return (truthy|falsey)
                '-description'     => 'xxx',                  #   human readable description of action

                #   response params
                '-response_params' => {                       #   optional 'allowed' response structure definition
                    'xxx' => '',                              #   if this option is used anything in ->response_data
                    'xxx' => [                                #   will be stripped of anything not in this structure
                        { 'xxx' => '', }
                    ],
                },
            },
        },
        '-prefix_keys' => '1|0'                       #   defaults to 1 (ensures there is a hyphen prefixed on data keys)
    };

    return $api_config;
} ## end sub api_config

#   init
sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed super init' } );
        return;
    }

    #   set api config
    my $config = $self->api_config();
    $self->set_api_config($config);

    #   response data
    $self->response_data(undef);

    return 1;
}

#   utils
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ @{ $self->SUPER::util_list }, UTIL_ERROR(), UTIL_DB_TABLE(), ];

    return $utility_list;
}

#   default api config
my $default_api_config = {
    '-actions' => {
        '-api' => {

            #   params
            '-params' => [],

            #   action method
            '-action_method' => 'api_action_publish',

            #   action description
            '-description' => 'Publish the available request API from this interface',

            #   response params
            '-response_params' => undef,
        },
    },
    '-params' => [
        {   '-name'           => 'meta',
            '-validate_rules' => { '-required' => '0', },
            '-description'    => 'A list of desired response parameters.',
        },
    ],
    '-prefix_keys' => 1,
};

sub get_default_api_config {
    my ( $self, $args ) = @_;

    return $default_api_config;
}

sub set_api_config {
    my ( $self, $config ) = @_;

    #   prefix config
    $self->prefix_keys( $config );

    #   default config
    my $default_config = $self->get_default_api_config();
    if ( defined $default_config ) {

        #   default actions
        my $default_actions = $default_config->{'-actions'};
        if ( defined $default_actions ) {
            foreach my $default_action ( keys %{$default_actions} ) {
                $config->{'-actions'}->{$default_action} = $default_actions->{$default_action};
            }
        }

        #   default params
        my $default_params = $default_config->{'-params'};
        if ( defined $default_params ) {
            my $actions = $config->{'-actions'};
            if ( defined $actions ) {
                foreach my $action ( keys %{$actions} ) {
                    foreach my $default_param ( @{$default_params} ) {
                        my $found = 0;
                        foreach my $param ( @{ $actions->{$action}->{'-params'} } ) {
                            if ( $param->{'-name'} eq $default_param->{'-name'} ) {
                                $found = 1;
                            }
                        }
                        if ( !$found ) {
                            push @{ $actions->{$action}->{'-params'} }, $default_param;
                        }
                    }
                }
            }
        }

        #   prefix data keys
        if ( !exists $config->{'-prefix_keys'} ) {
            $config->{'-prefix_keys'} = $default_config->{'-prefix_keys'};
        }
    } ## end if ( defined $default_config)

    $self->{'-api_config'} = $config;

    return;
} ## end sub set_api_config

sub get_api_config {
    my ( $self, $args ) = @_;

    return $self->{'-api_config'};
}

#   error code text
sub error_code_text {
    my ( $self, $args ) = @_;

    return {
        %{ $self->SUPER::error_code_text() },
        ERROR_CODE_ACTION_UNSUPPORTED() => 'Action [action] not supported',
        ERROR_CODE_VAL_REQUIRED()       => 'Parameter [param] is required',
        ERROR_CODE_VAL_MAX_LENGTH()     => 'Parameter [param] too long, max length [length]',
        ERROR_CODE_VAL_MIN_LENGTH()     => 'Parameter [param] too short, min length [length]',
        ERROR_CODE_VAL_MATCH()          => 'Parameter [param] does not look like what I would expect',
        ERROR_CODE_VAL_VALUES()         => 'Parameter [param] not in valid values [expression]',
    };
}

#   api validate methods
sub api_validate {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->get_api_config();

    #   action
    my $action = $args->{'-action'};

    #   data
    my $data = $args->{'-data'};

    $self->debug( { '-text' => 'Validating -data ' . $self->dbg_value($data) } );

    #  action config
    my $action_config = ( defined $config->{'-actions'} ? $config->{'-actions'}->{ '-' . $action } : undef );
    if ( !defined $action_config ) {
        $self->error( { '-text' => 'Config missing -action ' . $self->dbg_value($action) . ' -config ' . $self->dbg_value($config) } );
        return COMP_ACTION_FAILURE;
    }

    #   valid
    my $valid = 1;

    foreach my $param ( @{ $action_config->{'-params'} } ) {
        my $validate_param_method = $param->{'-validate_method'};

        #   default validate param method
        if ( !defined $validate_param_method ) {
            $validate_param_method = DEFAULT_VALIDATE_FIELD_METHOD;
        }

        my $param_name = $param->{'-name'};

        my $value = $data->{$param_name};
        if ( !defined $value ) {
            $value = $data->{ '-' . $param_name };
        }

        if (!$self->$validate_param_method(
                {   '-param' => $param,
                    '-value' => $value,
                    '-data'  => $data,
                } )
            ) {
            $valid = 0;
            $param->{'-validation_result'} = 0;
        }
    }

    #   global validation result
    $config->{'-validation_result'} = $valid;

    return $valid;
} ## end sub api_validate

sub api_validate_param {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   param
    my $param = $args->{'-param'};

    #   value
    my $value = $args->{'-value'};

    #   validate rules
    my $validate_rules = $param->{'-validate_rules'};

    $self->debug( { '-text' => 'Validating -param ' . $self->dbg_value($param) . ' -value ' . $self->dbg_value($value) } );

    my $valid;
    if ( defined $validate_rules ) {

        #   required
        if (!$self->api_validate_param_required(
                {   '-value'          => $value,
                    '-validate_rules' => $validate_rules,
                } )
            ) {

            #   set error
            $self->set_error(
                {   '-error_code' => ERROR_CODE_VAL_REQUIRED(),
                    '-param'      => $param->{'-name'},
                } );

            return;
        }

        #   max length
        if (!$self->api_validate_param_max_length(
                {   '-value'          => $value,
                    '-validate_rules' => $validate_rules,
                } )
            ) {

            #   set error
            $self->set_error(
                {   '-error_code' => ERROR_CODE_VAL_MAX_LENGTH(),
                    '-param'      => $param->{'-name'},
                    '-length'     => $validate_rules->{'-max_length'},

                } );

            return;
        }

        #   min length
        if (!$self->api_validate_param_min_length(
                {   '-value'          => $value,
                    '-validate_rules' => $validate_rules,
                } )
            ) {

            #   set error
            $self->set_error(
                {   '-error_code' => ERROR_CODE_VAL_MIN_LENGTH(),
                    '-param'      => $param->{'-name'},
                    '-length'     => $validate_rules->{'-min_length'},

                } );

            return;
        }

        #   do match
        if (!$self->api_validate_param_do_match(
                {   '-value'          => $value,
                    '-validate_rules' => $validate_rules,
                } )
            ) {

            #   set error
            $self->set_error(
                {   '-error_code' => ERROR_CODE_VAL_MATCH(),
                    '-param'      => $param->{'-name'},
                    '-expression' => $validate_rules->{'-do_match'},

                } );

            return;
        }

        #   dont match
        if (!$self->api_validate_param_dont_match(
                {   '-value'          => $value,
                    '-validate_rules' => $validate_rules,
                } )
            ) {

            #   set error
            $self->set_error(
                {   '-error_code' => ERROR_CODE_VAL_MATCH(),
                    '-param'      => $param->{'-name'},
                    '-expression' => $validate_rules->{'-dont_match'},

                } );

            return;
        }
    } ## end if ( defined $validate_rules)

    return 1;
} ## end sub api_validate_param

sub api_validate_param_required {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $validate_rules = $args->{'-validate_rules'};

    #   required
    my $required = $validate_rules->{'-required'};
    if ( defined $required && $required ) {
        if ( !defined $value || !length($value) ) {
            return;
        }
    }

    return 1;
}

sub api_validate_param_max_length {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $validate_rules = $args->{'-validate_rules'};

    #   assume validate required got it first
    if ( !defined $value ) {
        return 1;
    }

    #   max length
    my $max_length = $validate_rules->{'-max_length'};
    if ( defined $max_length && $max_length ) {
        if ( length($value) > $max_length ) {
            return;
        }
    }

    return 1;
}

sub api_validate_param_min_length {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $validate_rules = $args->{'-validate_rules'};

    #   assume validate required got it first
    if ( !defined $value ) {
        return 1;
    }

    #   min length
    my $min_length = $validate_rules->{'-min_length'};
    if ( defined $min_length && $min_length ) {
        if ( length($value) < $min_length ) {
            return;
        }
    }

    return 1;
}

sub api_validate_param_do_match {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $validate_rules = $args->{'-validate_rules'};

    #   assume validate required got it first
    if ( !defined $value ) {
        return 1;
    }

    #   do match
    my $do_match = $validate_rules->{'-do_match'};
    if ( defined $do_match ) {
        if ( !( $value =~ /$do_match/sxmg ) ) {
            return;
        }
    }

    return 1;
}

sub api_validate_param_dont_match {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $validate_rules = $args->{'-validate_rules'};

    #   assume validate required got it first
    if ( !defined $value ) {
        return 1;
    }

    #   dont match
    my $dont_match = $validate_rules->{'-dont_match'};
    if ( defined $dont_match ) {
        if ( $value =~ /$dont_match/sxmg ) {
            return;
        }
    }

    return 1;
}

sub api_action {
    my ( $self, $args ) = @_;

    return COMP_ACTION_FAILURE;
}

sub api_list_values {
    my ( $self, $args ) = @_;

    return ( [], {} );
}

sub action {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->get_api_config();

    my $return_status = COMP_ACTION_FAILURE;

    #   action
    my $action = $args->{'-action'};

    #  action config
    my $action_config = ( defined $config->{'-actions'} ? $config->{'-actions'}->{ '-' . $action } : undef );
    if ( !defined $action_config ) {
        $self->error( { '-text' => 'Config missing -action ' . $self->dbg_value($action) . ' -config ' . $self->dbg_value($config) } );
        return COMP_ACTION_FAILURE;
    }

    #   action method
    my $action_method = $action_config->{'-action_method'};
    if ( !defined $action_method ) {
        $self->debug( { '-text' => 'Missing action method, using default action method' } );
        $action_method = DEFAULT_ACTION_METHOD;
    }

    #   validate method
    my $validate_method = $action_config->{'-validate_method'};
    if ( !defined $validate_method ) {
        $self->debug( { '-text' => 'Missing validate method, using default validate method' } );
        $validate_method = DEFAULT_VALIDATE_METHOD;
    }

    if ( $config->{'-prefix_keys'} ) {
        $self->prefix_keys( { '-data' => $args->{'-data'} } );
    }

    if ( defined $action_method ) {

        #   validate
        if ( !$self->$validate_method($args) ) {
            return COMP_ACTION_FAILURE;
        }

        my $success = eval {
            $return_status = $self->$action_method($args);
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to call action method :' . $EVAL_ERROR } );
        }
    }

    return $return_status;
} ## end sub action

#   render
sub render {
    my ( $self, $args ) = @_;

    #   actions
    my $actions = $args->{'-actions'};

    if ( !defined $actions->{ $self->name } ) {
        $self->debug( { '-text' => 'Component -name ' . $self->name . ' no action, not rendering' } );
        return;
    }

    #   config
    my $config = $self->get_api_config();

    #   action
    my $action = $actions->{ $self->name() }->{'-action'};

    #  action config
    my $action_config = ( defined $config->{'-actions'} ? $config->{'-actions'}->{ '-' . $action } : undef );
    if ( !defined $action_config ) {
        $self->error( { '-text' => 'Config missing -action ' . $self->dbg_value($action) . ', not rendering' } );
        return;
    }

    #   response params
    #   - Remove anything from response data that is not in response params
    #     This will dive deep into the response structure
    my $response_params = $action_config->{'-response_params'};

    #   response data
    my $response_data = $self->response_data();

    $self->debug( { '-text' => 'Have -response_params ' . $self->dbg_value($response_params) });

    if ( defined $response_params && ref($response_params) eq 'HASH' ) {
        $self->debug( { '-text' => 'Santizing response data' } );

        #   santize response data
        $self->sanitize_response_data( { '-params' => $response_params, '-data' => $response_data } );
    } else  {
        $self->debug( { '-text' => 'Not santizing response data' } );
    }
    
    #   data
    my $data = $actions->{ $self->name() }->{'-data'};

    #   meta
    #   - Remove anything from response data that is not in the requested meta
    #     This will dive deep into the response structure
    my $meta = ( defined $data ? $data->{'-meta'} : undef );
    if ( defined $meta && ref($meta) eq 'ARRAY' ) {

        my $keep_h = {};
        foreach my $m ( @{$meta} ) {
            $m =~ s/^\-//sxmg;
            $keep_h->{$m} = 1;
        }

        #   sanitize data
        $self->sanitize_data( { '-keep' => $keep_h, '-data' => $response_data, '-depth' => 1 } );
    }

    #   unprefix keys
    if ( defined $response_data ) {
        $self->unprefix_keys( $response_data );
    }

    return $response_data;
} ## end sub render

sub sanitize_response_data {
    my ( $self, $args ) = @_;

    #   response params
    my $response_params = $args->{'-params'};

    #   response data
    my $response_data = $args->{'-data'};

    if ( defined $response_params && ref($response_params) eq 'HASH' ) {

        $self->debug( { '-text' => 'Have hashref -response_params ' . $self->dbg_value($response_params) . ' -response_data  ' . $self->dbg_value($response_data) } );

        if ( !defined $response_data || ref($response_data) ne 'HASH' ) {
            $self->debug( { '-text' => 'Undefined data or type mismatch, expecting hashref' } );
            return;
        }

        my $keep_h = {};
        while ( my ( $k, $v ) = each %{$response_params} ) {
            $k =~ s/^\-//sxmg;
            $keep_h->{$k} = 1;
        }

        $self->debug({'-text' => 'Have (hashref) -keep_h ' . $self->dbg_value($keep_h) });

        #   sanitize data
        $self->sanitize_data( { '-keep' => $keep_h, '-data' => $response_data, '-depth' => 1 } );

        while ( my ( $k, $v ) = each %{$response_data} ) {
            if ( ref( $response_params->{$k} ) ) {
                $self->sanitize_response_data( { '-params' => $response_params->{$k}, '-data' => $response_data->{$k} } );
            }
        }
    }
    elsif ( defined $response_params && ref($response_params) eq 'ARRAY' ) {
        $self->debug( { '-text' => 'Have (arrayref) -response_params ' . $self->dbg_value($response_params) . ' -response_data  ' . $self->dbg_value($response_data) } );

        if ( !defined $response_data || ref($response_data) ne 'ARRAY' ) {
            $self->debug( { '-text' => ' Undefined data or type mismatch, expecting arrayref' } );
            return;
        }

        #   params
        my $params = $response_params->[0];

        my $keep_h = {};
        while ( my ( $k, $v ) = each %{$params} ) {
            $k =~ s/^\-//sxmg;
            $keep_h->{$k} = 1;
        }

        $self->debug({'-text' => 'Have (arrayref) -keep_h ' . $self->dbg_value($keep_h) });
        
        foreach my $data ( @{$response_data} ) {

            #   sanitize data
            $self->sanitize_data( { '-keep' => $keep_h, '-data' => $data, '-depth' => 1 } );

            while ( my ( $k, $v ) = each %{$data} ) {
                if ( ref( $params->{$k} ) ) {
                    $self->sanitize_response_data( { '-params' => $params->{$k}, '-data' => $data->{$k} } );
                }
            }
        }
    }

    return;
} ## end sub sanitize_response_data

#   sanitize data
my $SANITIZE_DEPTH  = undef;
my $SANITIZED_DEPTH = 0;

sub sanitize_data {
    my ( $self, $args ) = @_;

    #   meta
    my $keep = $args->{'-keep'};

    #   data
    my $data = $args->{'-data'};

    #   depth
    my $depth = $args->{'-depth'};
    if ( defined $depth ) {
        $SANITIZE_DEPTH = $depth;
    }
    else {
        $SANITIZE_DEPTH = MAX_SANITIZE_DEPTH();
    }

    $SANITIZED_DEPTH = 0;

    #   _santize_data
    return $self->_santize_data(
        {   '-keep' => $keep,
            '-data' => $data,
        } );
}

#   _sanitize_data
sub _santize_data {
    my ( $self, $args ) = @_;

    #   meta
    my $keep = $args->{'-keep'};

    #   data
    my $data = $args->{'-data'};

    #   data is a hashref
    if ( ref($data) eq 'HASH' ) {
    REMOVE_META:
        foreach my $param ( keys %{$data} ) {
            my $keep_param = $param;
            $keep_param =~ s/^\-//sxmg;
            if ( !exists $keep->{$keep_param} ) {
                $self->trace( { '-text' => 'Deleting -param ' . $param } );
                delete $data->{$param};
                next REMOVE_META;
            }

            if ( ref( $data->{$param} ) eq 'ARRAY' ) {
                if ( defined $SANITIZE_DEPTH && ++$SANITIZED_DEPTH < $SANITIZE_DEPTH ) {
                    foreach my $data_item ( @{ $data->{$param} } ) {
                        $self->_sanitize_data( { '-keep' => $keep, '-data' => $data_item } );
                    }
                }
            }
        }
    }

    #   data is an arrayref
    elsif ( ref($data) eq 'ARRAY' ) {
        if ( defined $SANITIZE_DEPTH && ++$SANITIZED_DEPTH < $SANITIZE_DEPTH ) {
            foreach my $data_item ( @{$data} ) {
                $self->_sanitize_data( { '-keep' => $keep, '-data' => $data_item } );
            }
        }
    }

    return 1;
} ## end sub _santize_data

#   api action publish
sub api_action_publish {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Component -name ' . $self->name . ' -args ' . $self->dbg_value($args) } );

    #   config
    my $config = $self->get_api_config();

    #  actions config
    my $actions_config = ( defined $config->{'-actions'} ? $config->{'-actions'} : undef );
    if ( !defined $actions_config ) {
        $self->error( { '-text' => 'Config actions config' } );
        return COMP_ACTION_FAILURE;
    }

    #   data
    my $response_data = { '-actions' => {}, };

ACTIONS:
    foreach my $action ( keys %{$actions_config} ) {

        #   action config
        my $action_config = $actions_config->{$action};

        #   description
        $response_data->{'-actions'}->{$action}->{'-description'} = $action_config->{'-description'};

        #   response params
        my $response_params = $action_config->{'-response_params'};
        if ( defined $response_params ) {
            $response_data->{'-actions'}->{$action}->{'-response_params'} = $response_params;
        }

        #   params
        my $params = $action_config->{'-params'};
        if ( !defined $params || !scalar @{$params} ) {
            next ACTIONS;
        }

        $response_data->{'-actions'}->{$action}->{'-params'} = [];

    PARAMS:
        foreach my $param ( @{$params} ) {

            #   validate rules
            my $validate_rules = $param->{'-validate_rules'};

            #   required
            my $required = ( defined $validate_rules ? $validate_rules->{'-required'} : 0 );

            push @{ $response_data->{'-actions'}->{$action}->{'-params'} },
                {
                '-name'        => $param->{'-name'},
                '-description' => $param->{'-description'},
                '-required'    => $required,
                };

        }
    } ## end foreach my $action ( keys %...)

    #   response data
    $self->response_data($response_data);

    return COMP_ACTION_SUCCESS;
} ## end sub api_action_publish

1;

__END__

=head1 NAME

AlienSpaces::Comp::JSON::API

=head1 VERSION

$Author: $
$Date: $
$Revision: $

=head1 SYNOPSIS

This class is intended to be sub-classed by an application component class.

  I<Example:>
  
  package XXXX::Comp::JSON::API::Test::EntityAttribute;

  use strict;
  use warnings;
  use English qw( -no_match_vars );
  use Carp qw(carp croak);

  our $VERSION = '$Revision: $';

  use base qw (XXXX::Comp::JSON::API);

  use XXXX::Constants qw(:util :program :comp);

  sub util_list {

  }

  sub api_config {

  }

  1;

=head1 DESCRIPTION

It is intended that an application sub-classes this class for each JSON::API::XXXX component they wish to implement.

This class provides a configurable component for action handling and data response via a JSON interface.

=head1 CONFIGURATION

The sub-classing component must provide a api_config method to define 
what actions, action methods, validation rules and parameters the
component will handle.

  sub api_config {
      my ( $self, $args ) = @_;

      #   api config
      my $api_config = {

          '-actions' => {
              'xxxx' => {

                  #   params
                  '-params' => [
                      {   
                          #   input param name
                          '-name'            => 'xxx',    

                          #   validation method specifically for this param                                  
                          #   - defaults to 'api_validate_param'
                          #   - argument -name of param is passed in             
                          #   - return (truthy|falsey)
                          '-validate_method' => 'xxx',      

                          #   validation rules                                                                   
                          '-validate_rules'  => {

                              #   required
                              '-required'          => '1|0',    

                              #   max length
                              '-max_length'        => 'xxx',    

                              #   min length
                              '-min_length'        => 'xxx',    

                              #   must match this regular expression
                              '-do_match'          => 'xxx',    

                              #   must not match this regular expression
                              '-dont_match'        => 'xxx',    

                              #   Source values using this method,
                              #   - defaults to 'api_list_values'
                              #   - argument -name of param is passed in
                              '-values_method'     => 'xxx',    

                              #   Source values from this table
                              '-values_table_name' => 'xxx',    
                          },
                          #   parameter description
                          '-description' => 'xxx',
                      },
                  ],

                  #   method to call to handle this action
                  #   - defaults to 'api_action'
                  '-action_method'   => 'xxx',                  

                  #   method to call for validation
                  #   - defaults to 'api_validate'
                  #   - return (truthy|falsey)
                  '-validate_method' => 'xxx',                  

                  #   description of action
                  '-description'     => 'xxx',                  

                  #   response params
                  #   - optional array of 'expected' parameters to return
                  #   - if this option is used anything in ->response_data
                  #     will be stripped of anything not in this array
                  '-response_params' => [                      
                      'xxx',                                   
                      'xxx',                                   
                  ],
              },
          },
          
          #   defaults to 1 (ensures there is a hyphen prefixed on data keys)
          '-prefix_keys' => '1|0'                       
      };

      return $api_config;
  } ## end sub api_config

  In addition to the configuration provided by the implementing sub-class the following 
  action configuration is added by default.
  
  '-actions' => {
      'api' => { 

          #   params
          '-params' => [],

          #   action method
          '-action_method' => 'api_action_publish',

          #   action description
          '-description' => 'Publish the available request API from this interface',

          #   response params
          '-response_params' => ['actions'],
      },
  }

  In addition to the configuration provided by the implementing sub-class the following 
  param configuration is added to every action by default.
  
  '-params' => [
      {   '-name'           => 'meta',
          '-validate_rules' => { '-required' => '0', },
          '-description'    => 'A list of desired response parameters.',
      },
  ],

    
