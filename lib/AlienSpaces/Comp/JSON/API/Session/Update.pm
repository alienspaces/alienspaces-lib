package AlienSpaces::Comp::JSON::API::Session::Update;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:util :program :comp :interface);

our @DYNISA = qw(Comp::JSON::API);

our $VERSION = (qw$Revision: $)[1];

#   api config methods
sub api_config {
    my ( $self, $args ) = @_;

    #   api config
    my $api_config = {

        '-actions' => {
            'update-language' => {

                #   action method
                '-action_method' => 'update_language',

                #   params
                '-params' => [
                    {   '-name' => 'language_id',

                        # '-validate_method' => 'xxx',
                        '-validate_rules' => {

                            '-required' => '1',

                            # '-max_length'        => 'xxx',
                            # '-min_length'        => 'xxx',
                            # '-do_match'          => 'xxx',
                            # '-dont_match'        => 'xxx',
                            # '-values_method'     => 'xxx',
                            '-values_table_name' => 'as_r_language',
                        },
                        '-description' => 'The new language_id, See r/data/[R::Data]',
                    },
                ],

                # '-action_method'   => 'xxx',
                # '-validate_method' => 'xxx',
                '-description' => 'Update current session time zone settings',
            },
            'update-time-zone' => {

                #   action method
                '-action_method' => 'update_language',

                #   params
                '-params' => [
                    {   '-name' => 'time_zone_id',

                        # '-validate_method' => 'xxx',
                        '-validate_rules' => {

                            '-required' => '1',

                            # '-max_length'        => 'xxx',
                            # '-min_length'        => 'xxx',
                            # '-do_match'          => 'xxx',
                            # '-dont_match'        => 'xxx',
                            # '-values_method'     => 'xxx',
                            '-values_table_name' => 'as_r_language',
                        },
                        '-description' => 'The new time_zone_id, See r/data/[R::Data]',
                    },
                ],

                # '-action_method'   => 'xxx',
                # '-validate_method' => 'xxx',
                '-description' => 'Update current session time zone settings',
            },
        },
    };

    return $api_config;
} ## end sub api_config

#   update language
sub update_language {
    my ( $self, $args ) = @_;

    my $data = $args->{'-data'};

    my $language_id = $data->{'language_id'};

    if ( !defined $language_id ) {
        $self->error( { '-text' => 'Missing -language_id, cannnot update language' } );
        return COMP_ACTION_FAILURE;
    }

    #   language util
    my $language_util = $self->utils->{ UTIL_LANGUAGE() };
    if ( !defined $language_util ) {
        $self->error( { '-text' => 'Missing language util, cannot update language' } );
        return COMP_ACTION_FAILURE;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };
    if ( !defined $session_util ) {
        $self->error( { '-text' => 'Missing session util, cannot update language' } );
        return COMP_ACTION_FAILURE;
    }

    my $set_language = $language_util->fetch_language(
        {   '-language_id' => $language_id,
            '-active'      => 1,
        } );
    if ( !defined $set_language ) {
        $self->error( { '-text' => 'Unable to find -langauge_id ' . $self->dbg_value($language_id) . ' cannot update language' } );
        return COMP_ACTION_FAILURE;
    }

    #   set new langauge for this session
    $session_util->set_session( { '-key' => '-language_id', '-value' => $set_language->{'-language_id'} } );

    $self->debug( { '-text' => 'Set -language_id ' . $set_language->{'-language_id'} } );

    return COMP_ACTION_SUCCESS;
} ## end sub update_language

sub update_time_zone {
    my ( $self, $args ) = @_;

    my $data = $args->{'-data'};

    my $time_zone_id = $data->{'time_zone_id'};

    #   time zone util
    my $time_zone_util = $self->utils->{ UTIL_TIME_ZONE() };
    if ( !defined $time_zone_util ) {
        $self->error( { '-text' => 'Missing time_zone util, cannot update time zone' } );
        return COMP_ACTION_FAILURE;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };
    if ( !defined $session_util ) {
        $self->error( { '-text' => 'Missing session util, cannot update time zone' } );
        return COMP_ACTION_FAILURE;
    }

    if ( !defined $time_zone_id ) {
        $self->debug( { '-text' => 'Missing -time_zone_id ,cannnot update time zone' } );
        return COMP_ACTION_FAILURE;
    }

    my $set_time_zone = $time_zone_util->fetch_time_zone( { '-time_zone_id' => $time_zone_id, } );
    if ( !defined $set_time_zone ) {
        $self->error( { '-text' => 'Unable to find -time_zone_id ' . $self->dbg_value($time_zone_id) . ' cannot update time_zone' } );
        return COMP_ACTION_FAILURE;
    }

    #   set new langauge for this session
    $session_util->set_session( { '-key' => '-time_zone_id', '-value' => $set_time_zone->{'-time_zone_id'} } );

    $self->debug( { '-text' => 'Set -time_zone_id ' . $set_time_zone->{'-time_zone_id'} } );

    return COMP_ACTION_SUCCESS;

} ## end sub update_time_zone

1;

