package AlienSpaces::Comp::JSON::API::User::Account;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:util :program :comp :error-codes-user :request :user);

our @DYNISA = qw(Comp::JSON::API);

our $VERSION = (qw$Revision: $)[1];

#   api config methods
sub api_config {
    my ( $self, $args ) = @_;

    #   api config
    my $api_config = {

        '-actions' => {

            #   login
            'login' => {

                '-params' => [
                    {   '-name'           => 'email_address',
                        '-validate_rules' => { '-required' => '1', },
                        '-description'    => 'The email address of the users account',
                    },
                    {   '-name'           => 'password',
                        '-validate_rules' => { '-required' => '1', },
                        '-description'    => 'The password of the users account',
                    },
                ],
                '-action_method' => 'api_action_login',
                '-description'   => 'Log a user into the application with this session',
            },

            #   logout
            'logout' => {

                '-params'        => [],
                '-action_method' => 'api_action_logout',
                '-description'   => 'Log a user our of the application for this session',
            },

            #   register
            'register' => {

                '-params' => [
                    {   '-name'           => 'handle',
                        '-validate_rules' => {
                            '-required'   => '1',
                            '-max_length' => '45',
                            '-min_length' => '2',
                        },
                        '-description' => 'The public name for this user account',
                    },
                    {   '-name'           => 'email_address',
                        '-validate_rules' => {
                            '-required'   => '1',
                            '-max_length' => '256',
                            '-min_length' => '5',
                            '-do_match'   => '.*\@.*\..*',
                        },
                        '-description' => 'The email address for this user account',
                    },
                    {   '-name'           => 'password',
                        '-validate_rules' => {
                            '-required'   => '1',
                            '-max_length' => '128',
                            '-min_length' => '6',
                        },
                        '-description' => 'The password for this user account',
                    },
                ],
                '-action_method'   => 'api_action_register',
                '-description'     => 'Register a new user account on the application',
                '-response_params' => [],
            },

            #   fetch
            'fetch' => {

                '-params'          => [],
                '-action_method'   => 'api_action_fetch',
                '-description'     => 'Fetch user account information about the current logged in user',
                '-response_params' => {
                    'user' => {
                        'first_name'    => '',
                        'last_name'     => '',
                        'handle'        => '',
                        'email_address' => '',
                        'language_id'   => '',
                        'time_zone_id'  => '',
                    }
                },
            },

            #   update
            'update' => {
                '-params' => [
                    {   '-name'           => 'handle',
                        '-validate_rules' => {
                            '-required'   => '0',
                            '-max_length' => '45',
                            '-min_length' => '2',
                        },
                        '-description' => 'The new public name for this user account',
                    },
                    {   '-name'           => 'first_name',
                        '-validate_rules' => {
                            '-required'   => '0',
                            '-max_length' => '128',
                            '-min_length' => '2',
                        },
                        '-description' => 'The new first name for this user account',
                    },
                    {   '-name'           => 'last_name',
                        '-validate_rules' => {
                            '-required'   => '0',
                            '-max_length' => '128',
                            '-min_length' => '2',
                        },
                        '-description' => 'The new last name for this user account',
                    },
                    {   '-name'           => 'email_address',
                        '-validate_rules' => {
                            '-required'   => '0',
                            '-max_length' => '256',
                            '-min_length' => '5',
                            '-do_match'   => '.*\@.*\..*',
                        },
                        '-description' => 'The new email address for this user account',
                    },
                    {   '-name'           => 'password',
                        '-validate_rules' => {
                            '-required'   => '0',
                            '-max_length' => '128',
                            '-min_length' => '6',
                        },
                        '-description' => 'The new password for this user account',
                    },
                    {   '-name'           => 'language_id',
                        '-validate_rules' => { '-required' => '0', },
                        '-description'    => 'The new language_id for this user account',
                    },
                    {   '-name'           => 'time_zone_id',
                        '-validate_rules' => { '-required' => '0', },
                        '-description'    => 'The new time_zone_id for this user account',
                    },
                ],
                '-action_method'   => 'api_action_update',
                '-description'     => 'Update user account information for the current logged in user',
                '-response_params' => [],
            },

            #   cancel
            'cancel' => {
                '-params'          => [],
                '-action_method'   => 'api_action_cancel',
                '-description'     => 'Cancel a users own account.',
                '-response_params' => [],
            },

            #   delete
            'delete' => {
                '-params'          => [],
                '-action_method'   => 'api_action_delete',
                '-description'     => 'Delete a users own account.',
                '-response_params' => [],
            },
        },
    };

    return $api_config;
} ## end sub api_config

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_BUSINESS(),
        UTIL_REQUEST(),
    ];

    return $utility_list;
}

#   api action login
sub api_action_login {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    my $email_address = $self->html_strip( $data->{'email_address'} );
    my $password      = $self->html_strip( $data->{'password'} );

    #   login
    my $user_id = $user_util->login_user(
        {   '-email_address' => $email_address,
            '-password'      => $password,
        } );
    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'Failed to login user' } );
        return COMP_ACTION_FAILURE;
    }

    #   set session values
    if ( !$self->set_session_values( { '-user_id' => $user_id } ) ) {
        $self->error( { '-text' => 'Failed to set session values' } );
        return COMP_ACTION_FAILURE;
    }

    return COMP_ACTION_SUCCESS;
} ## end sub api_action_login

#   set session values
sub set_session_values {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   user id
    my $user_id = $args->{'-user_id'};

    if ( !$user_util->set_user_session_values( { '-user_id' => $user_id } ) ) {
        $self->error( { '-text' => 'Failed to set user session values' } );
        return;
    }

    #   business user
    my $business_user = $business_util->fetch_one_business_user( { '-user_id' => $user_id } );
    if ( defined $business_user ) {
        if ( !$business_util->set_business_session_values( { '-business_id' => $business_user->{'-business_id'} } ) ) {
            $self->error( { '-text' => 'Failed to set business session values' } );
        }
    }

    return 1;
} ## end sub set_session_values

#   api action logout
sub api_action_logout {
    my ( $self, $args ) = @_;

    #   data
    my $data = $args->{'-data'};

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   logout
    if ( $user_util->logout_user() ) {
        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
}

#   api action register
sub api_action_register {
    my ( $self, $args ) = @_;

    #   data
    my $data = $args->{'-data'};

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   register
    my $user_id = $user_util->register_user($data);
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Failed to register new user -data ' . $self->dbg_value($data) } );
        return COMP_ACTION_FAILURE;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $user_id } );
        return COMP_ACTION_FAILURE;
    }

    #   fetch user configuration
    my $user_config = $user_util->fetch_user_config( { '-user_config_id' => $user->{'-user_config_id'} } );
    if ( !defined $user_config ) {
        $self->error( { '-text' => 'Failed to fetch user config for -user_id ' . $user_id } );
        return COMP_ACTION_FAILURE;
    }

    #   does registration require email activation?
    if ( $user_config->{'-registration_confirmation_enabled'} ) {

        if (!$request_util->add_request(
                {   '-request_type_id' => REQUEST_TYPE_USER_REGISTER,
                    '-user_id'         => $user_id,
                    '-request_data'    => [
                        {   '-table_name'  => 'as_user',
                            '-column_name' => 'user_status_id',
                            '-pk_value'    => $user_id,
                            '-new_value'   => USER_STATUS_ACTIVE,
                        } ] } )
            ) {
            $self->error( { '-text' => 'Failed to register user' } );
            return COMP_ACTION_FAILURE;
        }

        return COMP_ACTION_SUCCESS;
    }

    #   if registration does not require email activation
    #   go ahead and activate them straight away
    if ($user_util->activate_user(
            {   '-user_id'      => $user_id,
                '-activate_key' => $user->{'-activate_key'} } )
        ) {
        $self->debug( { '-text' => 'User -user_id ' . $user_id . ' registered and activate' } );
        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub api_action_register

#   api action fetch
sub api_action_fetch {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user id
    my $user_id = $user_util->user_id();
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Unable to identify -user_id. Cannot get user account information' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   user logged in
    if ( !$user_util->check_user_logged_in() ) {
        $self->error( { '-text' => 'User not logged in, cannot get user account information' } );

        #   set error
        $self->set_error( { '-error_id' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_util->user_id } );

    my $response_data = { '-user' => $user };

    #   response data
    $self->response_data($response_data);

    return COMP_ACTION_SUCCESS;
} ## end sub api_action_fetch

#   api action update
sub api_action_update {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    #   user id
    my $user_id = $user_util->user_id();
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Unable to identify -user_id. Cannot get user account information' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   user logged in
    if ( !$user_util->check_user_logged_in() ) {
        $self->error( { '-text' => 'User not logged in, cannot get user account information' } );

        #   set error
        $self->set_error( { '-error_id' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   user id
    $data->{'-user_id'} = $user_id;

    #   update
    if ( $user_util->update_user($data) ) {
        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub api_action_update

#   api action cancel
sub api_action_cancel {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user logged in
    if ( !$user_util->check_user_logged_in() ) {
        $self->error( { '-text' => 'User is not logged in, cannot cancel user account' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   user id
    my $user_id = $user_util->user_id();
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Could not source user id, cannot cancel user account' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   cancel
    if ( $user_util->cancel_user( { '-user_id' => $user_id } ) ) {
        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub api_action_cancel

#   api action delete
sub api_action_delete {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user logged in
    if ( !$user_util->check_user_logged_in() ) {
        $self->error( { '-text' => 'User is not logged in, cannot cancel user account' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   user id
    my $user_id = $user_util->user_id();
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Could not source user id, cannot cancel user account' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_NOT_LOGGED_IN } );

        return COMP_ACTION_FAILURE;
    }

    #   delete
    if ( $user_util->delete_user() ) {
        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub api_action_delete

1;

