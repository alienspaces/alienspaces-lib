package AlienSpaces::Comp::JSON::API::Remote::Build;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp);

our @DYNISA = qw(Comp::JSON::API);

our $VERSION = (qw$Revision: $)[1];

use JSON;

#   api config methods
sub api_config {
    my ( $self, $args ) = @_;

    #   api config
    my $api_config = {

        '-actions' => {
            'build' => {

                #   params
                '-params' => [
                    {

                        #   param name
                        '-name' => 'project',

                        #   param validation
                        '-validate_rules'  => {
                            '-required'          => '1',
                        },

                        #   param description
                        '-description' => 'The project you wish to build',
                    },
                    {

                        #   param name
                        '-name' => 'target',

                        #   param validation
                        '-validate_rules'  => {
                            '-required'          => '1',
                        },

                        #   param description
                        '-description' => 'The target environment you wish to build to. Can include dev, beta or prod.',
                    },
                    {

                        #   param name
                        '-name' => 'revision',

                        #   param validation
                        '-validate_rules'  => {
                            '-required'          => '0',
                        },

                        #   param description
                        '-description' => 'An optional revision number to update the remote dev-repo repository to. If excluded the repository will be update to the latest revision',
                    },
                    {

                        #   param name
                        '-name' => 'email',

                        #   param validation
                        '-validate_rules'  => {
                            '-required'          => '0',
                        },

                        #   param description
                        '-description' => 'An option email addess for build process output to be emailed to.',
                    },
                ],

                #   action method
                '-action_method' => 'build',

                #   action description
                '-description' => 'Trigger the remote build process for an application.',
            },
        },
    };

    return $api_config;
} ## end sub api_config

sub build {
    my ( $self, $args ) = @_;

    #   data
    my $data = $args->{'-data'};

    my $project  = $data->{'-project'};
    my $target   = $data->{'-target'};
    my $revision = $data->{'-revision'};
    my $email    = $data->{'-email'};

    #   response data
    my $response_data = {
        '-project'  => $project,
        '-target'   => $target,
        '-revision' => $revision,
        '-email'    => $email,
    };

    #   application home
    my $application_home = $self->environment->{'APP_HOME'};

    #   build remote
    my $build_remote = $application_home . '/conf/build/remote.conf';

    #   check build rule
    if ( !$self->check_build_rule( { '-build_remote' => $build_remote, '-project' => $project } ) ) {
        return COMP_ACTION_FAILURE;
    }

    #   add build rule
    if ( !$self->add_build_rule( { '-build_remote' => $build_remote } ) ) {
        return COMP_ACTION_FAILURE;
    }

    $response_data->{'-text'} = 'Build rules added to ' . $build_remote;

    # response data
    $self->response_data($response_data);
    
    return COMP_ACTION_SUCCESS;
} ## end sub action

#   check build rule
sub check_build_rule {
    my ( $self, $args ) = @_;

    #   build remote
    my $build_remote = $args->{'-build_remote'};

    #   project
    my $project = $args->{'-project'};

    my $fh;
    if ( !open $fh, '<', $build_remote ) {
        $self->{'-return_data'}->{'-text'} = 'Failed to open ' . $build_remote . ' :' . $OS_ERROR;
        return;
    }

    my @rules = <$fh>;

    if ( !close $fh ) {
        $self->{'-return_data'}->{'-text'} = 'Failed to close ' . $build_remote . ' :' . $OS_ERROR;
        return;
    }

FIND_RULE:
    foreach my $rule (@rules) {

        chomp($rule);
        if ( !length $rule ) {
            next FIND_RULE;
        }

        my $rule = from_json($rule);
        if ( !defined $rule ) {
            next FIND_RULE;
        }

        if ( $rule->{'-project'} eq $project ) {
            $self->{'-return_data'}->{'-text'} = 'Project already has a pending build rule';
            return;
        }
    }

    return 1;
} ## end sub check_build_rule

#   add build rule
sub add_build_rule {
    my ( $self, $args ) = @_;

    #   build remote
    my $build_remote = $args->{'-build_remote'};

    my $fh;
    if ( !open $fh, '>>', $build_remote ) {
        $self->{'-return_data'}->{'-text'} = 'Failed to open ' . $build_remote . ' :' . $OS_ERROR;
        return;
    }

    my $json_string = to_json( $self->{'-return_data'} );

    print {$fh} $json_string . "\n";

    if ( !close $fh ) {
        $self->{'-return_data'}->{'-text'} = 'Failed to close ' . $build_remote . ' :' . $OS_ERROR;
        return;
    }

    return 1;
}

1;

