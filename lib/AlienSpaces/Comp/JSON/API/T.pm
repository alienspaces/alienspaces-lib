package AlienSpaces::Comp::JSON::API::T;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp);

our @DYNISA = qw(Comp::JSON::API);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local

    ];
}

#   api config methods
sub api_config {
    my ( $self, $args ) = @_;

    #   api config
    my $api_config = {

        '-actions' => {
            'xxx' => {

                #   params
                '-params' => [
                    {

                        #   param name
                        '-name' => 'xxx',

                        #   param validation
                        '-validate_method' => 'xxx',
                        '-validate_rules'  => {
                            '-required'          => '1|0',
                            '-max_length'        => 'xxx',
                            '-min_length'        => 'xxx',
                            '-do_match'          => 'xxx',
                            '-dont_match'        => 'xxx',
                            '-values_method'     => 'xxx',
                            '-values_table_name' => 'xxx',
                        },

                        #   param description
                        '-description' => 'xxx',
                    },
                ],
                
                #   response params
                '-response_params'    => [
                    'xxx',
                ],

                #   action method
                '-action_method' => 'xxx',

                #   validate method
                '-validate_method' => 'xxx',

                #   action description
                '-description' => 'xxx',
            },
        },
        '-hyphenise_data_keys' => '1|0',
    };

    return $api_config;
} ## end sub api_config

#   api_action_xxxx
sub api_action_xxxx {
    my ( $self, $args ) = @_;

    #   data
    my $data = $args->{'-data'};

    #   response data
    my $response_data;
    
    #   action logic


    
    #   set response data
    $self->response_data($response_data);
    
    return COMP_ACTION_FAILURE;
}

1;

