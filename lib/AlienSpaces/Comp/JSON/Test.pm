package AlienSpaces::Comp::JSON::Test;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties { 'data' => { '-type' => 's' }, };

sub render {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Component -name ' . $self->name . ' -args ' . $self->dbg_value($args) } );

    my $data = {
        'request_data'  => $self->data,
        'response_data' => 'All Ok',
    };

    return $data;
}

sub action {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Component -name ' . $self->name . ' -args ' . $self->dbg_value($args) } );

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $action = delete $args->{'-action'};
    my $data   = $args->{'-data'};

    $self->data($data);

    #
    #   handle action
    #

    return $self->COMP_ACTION_SUCCESS;
}

1;

