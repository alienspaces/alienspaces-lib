package AlienSpaces::Comp::HTML::Request::Deny;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :form :comp :user :contact-type);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_USER(), UTIL_REQUEST(), ];
}

sub render {
    my ( $self, $args ) = @_;

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    my $actions = $args->{'-actions'};
    if ( !defined $actions ) {
        return;
    }

    #   content
    my $content;

    #   check for successful action
    my $action = $actions->{ $self->name };
    if ( defined $action && $action->{'-action'} eq 'deny' && $action->{'-status'} == COMP_ACTION_SUCCESS ) {
        $content = 'Request denial successful';
    }
    else {
        $content = 'Request denial failed.';
    }

    return $content;
}

sub action {
    my ( $self, $args ) = @_;

    my $action = $args->{'-action'};
    my $data   = $args->{'-data'};

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    if ( defined $action ) {
        if ( $action eq 'deny' ) {

            my $user_id     = $data->{'user_id'};
            my $request_id  = $data->{'request_id'};
            my $request_key = $data->{'request_key'};

            if ( !defined $request_key || !defined $user_id ) {

                $self->debug( { '-text' => 'Missing some -data ' . $self->dbg_value($data) . ', cannot deny request' } );

                return COMP_ACTION_FAILURE;
            }

            if ($request_util->deny_request(
                    {   '-user_id'     => $user_id,
                        '-request_id'  => $request_id,
                        '-request_key' => $request_key
                    } )
                ) {

                $self->debug( { '-text' => 'Request -request_id ' . $self->dbg_value($request_id) . ' denied' } );

                return COMP_ACTION_SUCCESS;
            }

            $self->debug( { '-text' => 'Request -request_id ' . $self->dbg_value($request_id) . ' failed' } );
        }
    }

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

