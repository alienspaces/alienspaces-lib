package AlienSpaces::Comp::HTML::Application::User;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   HTML component template class
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {

};

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),

        UTIL_DB_RECORD_R_USER_TYPE(),
        UTIL_DB_RECORD_R_USER_STATUS(),
    ];
}

#   render
sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $data = $self->get_table_data();

    #   title
    my $title = $phrase_util->text( { '-phrase_id' => 90_000 } );    #   phrase_id 90000 - Application Users

    my $column_titles;
    if ( defined $data && scalar @{$data} ) {
        $column_titles = [
            $phrase_util->text( { '-phrase_id' => 90_010 } ),        # phrase_id 90010 - Name
            $phrase_util->text( { '-phrase_id' => 90_020 } ),        # phrase_id 90020 - Email
            $phrase_util->text( { '-phrase_id' => 90_030 } ),        # phrase_id 90030 - User Type
            $phrase_util->text( { '-phrase_id' => 90_040 } ),        # phrase_id 90040 - User Status
            undef,
        ];
    }

    #   content
    my $content = $self->table->render(
        {   '-name'          => $self->name,
            '-title'         => $title,
            '-column_titles' => $column_titles,
            '-data'          => $data,
        } );

    return $content;
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   db user type util
    my $db_user_type_util = $self->utils->{ UTIL_DB_RECORD_R_USER_TYPE() };

    #   db user status util
    my $db_user_status_util = $self->utils->{ UTIL_DB_RECORD_R_USER_STATUS() };

    #   users
    #   - exclude current user and all business users
    my $users = $user_util->fetch_users(
        {   '-where' => {
                '-clause' => 'user_id != ? AND user_id NOT IN (SELECT user_id FROM as_business_user)',
                '-bind'   => [ $user_util->user_id ],
            },
        } );
    if ( !defined $users ) {
        $self->debug( { '-text' => 'No users found' } );
        return;
    }

    my $return_data = [];

USERS:
    foreach my $user ( @{$users} ) {

        #   check account admin
        if ( !$user_util->check_user_account_admin_rank( { '-check_user_id' => $user->{'-user_id'} } ) ) {
            $self->debug( { '-text' => 'Cannot admin user' } );
            next USERS;
        }

        #   name
        my $name = ( defined $user->{'-first_name'} ? $user->{'-first_name'} : '' ) . ( defined $user->{'-last_name'} ? ' ' . $user->{'-last_name'} : '' );

        #   user type
        my $user_type = $db_user_type_util->fetch_one( { '-user_type_id' => $user->{'-user_type_id'} } );

        #   user type text
        my $user_type_text;
        if ( defined $user_type ) {
            $user_type_text = $phrase_util->text( { '-phrase_id' => $user_type->{'-phrase_id'} } );
        }

        #   user status
        my $user_status = $db_user_status_util->fetch_one( { '-user_status_id' => $user->{'-user_status_id'} } );

        #   user status text
        my $user_status_text;
        if ( defined $user_status ) {
            $user_status_text = $phrase_util->text( { '-phrase_id' => $user_status->{'-phrase_id'} } );
        }

        #   view link
        my $view_link = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 90_050 } ),    # phrase_id 80820 - View
                        '-program'    => PROGRAM_SITE_APPLICATION_USER_ACCOUNT(),
                        '-component'  => COMP_HTML_APPLICATION_USER_ACCOUNT_PROFILE(),
                        '-action'     => 'view',
                        '-user_id'    => $user->{'-user_id'},
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        push @{$return_data}, [

            #   name
            $name,

            #   email address
            $user->{'-email_address'},

            #   user type text
            $user_type_text,

            #   user status text
            $user_status_text,

            #   view
            $view_link,
        ];
    } ## end USERS: foreach my $user ( @{$users...})

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    return COMP_ACTION_FAILURE();
}

1;

