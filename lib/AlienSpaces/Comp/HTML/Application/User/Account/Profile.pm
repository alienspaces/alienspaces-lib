package AlienSpaces::Comp::HTML::Application::User::Account::Profile;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :table-style :user :error-codes-user);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),

        UTIL_DB_RECORD_R_USER_TYPE(),
        UTIL_DB_RECORD_R_USER_STATUS(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $data = $self->get_table_data();
    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to fetch account profile data' } );
        return;
    }

    #   edit link
    my $edit_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-name' => $phrase_util->text( { '-phrase_id' => 90_210 } ),    # phrase_id 90210 - Edit
                    '-program' => PROGRAM_SITE_APPLICATION_USER_ACCOUNT_PROFILE(),
                },
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    return $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => 90_200 } ),    # phrase_id  90_200 - Application User Profile
            '-class'                => TABLE_CLASS_VERTICAL,
            '-extra_footer_content' => $edit_link,
            '-data'                 => $data,
        } );
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   db user type util
    my $db_user_type_util = $self->utils->{ UTIL_DB_RECORD_R_USER_TYPE() };

    #   db user status util
    my $db_user_status_util = $self->utils->{ UTIL_DB_RECORD_R_USER_STATUS() };

    #   user
    my $user = $user_util->fetch_current_application_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current user' } );
        return;
    }

    #   user type
    my $user_type = $db_user_type_util->fetch_one( { '-user_type_id' => $user->{'-user_type_id'} } );

    #   user type text
    my $user_type_text;
    if ( defined $user_type ) {
        $user_type_text = $phrase_util->text( { '-phrase_id' => $user_type->{'-phrase_id'} } );
    }

    #   user status
    my $user_status = $db_user_status_util->fetch_one( { '-user_status_id' => $user->{'-user_status_id'} } );

    #   user status text
    my $user_status_text;
    if ( defined $user_status ) {
        $user_status_text = $phrase_util->text( { '-phrase_id' => $user_status->{'-phrase_id'} } );
    }

    my $return_data = [];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_220 } ), $user->{'-handle'}, ];    # phrase_id 90220 - Handle

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_230 } ), $user->{'-first_name'}, ];    # phrase_id 90230 - First Name

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_240 } ), $user->{'-last_name'}, ];     # phrase_id 90240 - Last Name

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_250 } ), $user_type_text, ];           # phrase_id 90250 - User Type

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_260 } ), $user_status_text, ];         # phrase_id 90260 - User Status 

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    #   action
    my $action = $args->{'-action'};

    if ( defined $action && $action eq 'view' ) {

        #   user id
        my $user_id = $data->{'user_id'};
        if ( !defined $user_id ) {
            $self->error( { '-text' => 'Missing -user_id, cannot view user' } );
            return COMP_ACTION_FAILURE;
        }

        #   user
        my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
        if ( !defined $user ) {
            $self->error( { '-text' => 'Failed to fetch user for -user_id ' . $self->dbg_value($user_id) . ', cannot view user' } );
            return COMP_ACTION_FAILURE;
        }

        #   set current application user
        if ( !$user_util->set_current_application_user( { '-user_id' => $user_id } ) ) {
            $self->error( { '-text' => 'Failed to set current business user' } );
            return COMP_ACTION_FAILURE;
        }

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

__END__

