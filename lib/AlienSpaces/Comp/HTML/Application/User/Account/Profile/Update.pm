package AlienSpaces::Comp::HTML::Application::User::Account::Profile::Update;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_USER(), UTIL_REQUEST(), ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 91_000,

        '-fields' => [
            {   '-name'            => 'handle',
                '-clear'           => 0,
                '-label_phrase_id' => 91_010,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => '1',    #   required
                    '-min_length' => '3',    #   min length
                },
            },
            {   '-name'            => 'first_name',
                '-clear'           => 0,
                '-label_phrase_id' => 91_020,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'            => 'last_name',
                '-clear'           => 0,
                '-label_phrase_id' => 91_030,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'              => 'user_type_id',
                '-clear'             => 0,
                '-label_phrase_id'   => 91_040,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_user_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'              => 'user_status_id',
                '-clear'             => 0,
                '-label_phrase_id'   => 91_060,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_user_status',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
        ],

        #   auto complete
        '-auto_complete' => 0,             #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed phrase
        '-submit_failed_text_method' => 'submit_failed_text',

        #   submit success phrase_id
        '-submit_success_phrase_id' => 91_100,

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_APPLICATION_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_APPLICATION_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, $phrase_util->text( { '-phrase_id' => 91_140 } ) );

    return $content;
}

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    my $action = $args->{'-action'};
    my $data   = $args->{'-data'};

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   application user
    my $user = $user_util->fetch_current_application_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current application user, cannot populate form' } );
        return;
    }

    return $user;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   data
    my $data = $args->{'-data'};

    #   application user
    my $user = $user_util->fetch_current_application_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current application user, cannot updatye application user profile' } );
        return COMP_ACTION FAILURE;
    }

    $self->debug( { '-text' => 'Update application user profile' } );

    #   html strip
    my $handle         = $self->html_strip( $data->{'handle'} );
    my $first_name     = $self->html_strip( $data->{'first_name'} );
    my $last_name      = $self->html_strip( $data->{'last_name'} );
    my $user_type_id   = $self->html_strip( $data->{'user_type_id'} );
    my $user_status_id = $self->html_strip( $data->{'user_status_id'} );

    #   update user
    if (!$user_util->update_user(
            {   '-user_id'        => $user->{'-user_id'},
                '-handle'         => $handle,
                '-first_name'     => $first_name,
                '-last_name'      => $last_name,
                '-user_type_id'   => $user_type_id,
                '-user_status_id' => $user_status_id,
            } )
        ) {

        $self->debug( { '-text' => 'Failed to update user' } );

        return COMP_ACTION_FAILURE;
    }

    #   TODO - if we introduce an 'as_application_user' table
    #   here is where we would update the application user

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   text
    my $text = '';

    #   errors
    my $errors = $self->get_errors();
    if ( defined $errors ) {
        foreach my $error ( @{$errors} ) {
            if ( $error->{'-error_code'} == ERROR_CODE_USER_HANDLE_NOT_UNIQUE ) {

                $text .= $phrase_util->text( { '-phrase_id' => 91_110 } );
            }
            if ( $error->{'-error_code'} == ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE ) {

                $text .= $phrase_util->text( { '-phrase_id' => 91_120 } );
            }
        }
    }
    else {

        $text .= $phrase_util->text( { '-phrase_id' => 91_130 } );
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
} ## end sub submit_failed_text

1;

