package AlienSpaces::Comp::HTML::Application::User::Account::Contact::Add;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :contact-type :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_CONTACT(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 91_300,    #

        '-fields' => [
            {   '-name'              => 'user_contact_type_id',
                '-label_phrase_id'   => 91_310,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_user_contact_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'name',
                '-label_phrase_id' => 91_320,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_one',
                '-label_phrase_id' => 91_330,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_two',
                '-label_phrase_id' => 91_340,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'              => 'country_id',
                '-label_phrase_id'   => 91_350,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_country',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'state_province',
                '-label_phrase_id' => 91_360,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'postal_code',
                '-label_phrase_id' => 91_370,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'phone',
                '-label_phrase_id' => 91_380,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
        ],

        #   auto complete
        '-auto_complete' => 0,             #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed phrase if
        '-submit_failed_phrase_id' => 91_390,

        #   submit success phrase id
        '-submit_success_phrase_id' => 91_400,

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_APPLICATION_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_APPLICATION_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, $phrase_util->text( { '-phrase_id' => 91_410 } ), );

    return $content;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    $self->debug( { '-text' => 'Adding new business user contact' } );

    #   business user
    my $user = $user_util->fetch_current_application_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Missing current business user, cannot add contact' } );
        return;
    }

    #   business user id
    my $user_id = $user->{'-user_id'};

    if ( $self->add_contact( { '-user_id' => $user_id, '-data' => $data } ) ) {
        return COMP_ACTION_SUCCESS;
    }

    $self->error( { '-text' => 'Failed to add new business user contact' } );

    return COMP_ACTION_FAILURE;
}

#   add contact
sub add_contact {
    my ( $self, $args ) = @_;

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   data
    my $data = $args->{'-data'};

    #   business user id
    my $user_id = $args->{'-user_id'};

    #   html strip
    my $user_contact_type_id = $self->html_strip( $data->{'user_contact_type_id'} );
    my $name                 = $self->html_strip( $data->{'name'} );
    my $address_line_one     = $self->html_strip( $data->{'address_line_one'} );
    my $address_line_two     = $self->html_strip( $data->{'address_line_two'} );
    my $country_id           = $self->html_strip( $data->{'country_id'} );
    my $state_province       = $self->html_strip( $data->{'state_province'} );
    my $postal_code          = $self->html_strip( $data->{'postal_code'} );
    my $phone                = $self->html_strip( $data->{'phone'} );

    #   contact id
    my $contact_id = $contact_util->insert_user_contact(
        {   '-user_id'              => $user_id,
            '-user_contact_type_id' => $user_contact_type_id,
            '-name'                 => $name,
            '-address_line_one'     => $address_line_one,
            '-address_line_two'     => $address_line_two,
            '-country_id'           => $country_id,
            '-state_province'       => $state_province,
            '-postal_code'          => $postal_code,
            '-phone'                => $phone,
        } );

    if ( !defined $contact_id ) {
        $self->error( { '-text' => 'Failed to insert business user contact' } );
        return;
    }

    $self->debug( { '-text' => 'Inserted business user contact -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub add_contact

1;

