package AlienSpaces::Comp::HTML::Application::User::Account::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :form :user :table-style :contact-type :message-type :js-confirm);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),

        UTIL_DB_RECORD_R_COUNTRY(),
        UTIL_DB_RECORD_R_USER_CONTACT_TYPE(),

        UTIL_OUTPUT_HTML_JS_CONFIRM(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   js confirm util
    my $js_confirm_util = $self->utils->{ UTIL_OUTPUT_HTML_JS_CONFIRM() };

    #   user contact types
    my $user_contact_types = $contact_util->fetch_user_contact_types( { '-active' => 1 } );
    if ( !defined $user_contact_types ) {
        $self->debug( { '-text' => 'No user contact types active, not rendering' } );
        return;
    }

    #   user
    my $user = $user_util->fetch_current_application_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current application user' } );
        return;
    }

    #   user id
    my $user_id = $user->{'-user_id'};

    #   add link
    my $add_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-label_text' => $phrase_util->text( { '-phrase_id' => 90_810 } ),    # phrase_id 90810 - Add
                    '-program' => PROGRAM_SITE_APPLICATION_USER_ACCOUNT_CONTACT_ADD(),
                }
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    #   content
    my $content = $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => 90_800 } ),    # phrase_id 90800 - Application User Contacts
            '-show_content'         => 0,
            '-show_footer'          => 0,
            '-extra_header_content' => $add_link,
        } );

    #   user contacts
    my $user_contacts = $contact_util->fetch_user_contacts( { '-user_id' => $user_id, } );
    if ( !defined $user_contacts || !scalar @{$user_contacts} ) {
        $self->debug( { '-text' => 'Did not fetch any user contacts for -user_id ' . $self->dbg_value($user_id) } );
        return $content;
    }

    foreach my $user_contact ( @{$user_contacts} ) {

        my $data = $self->get_table_data( { '-user_contact' => $user_contact } );
        if ( !defined $data ) {
            $self->error( { '-text' => 'Failed to get table data' } );
            next;
        }

        #   links
        my $links = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 90_820 } ),    # phrase_id 90820 - Edit
                        '-program'    => PROGRAM_SITE_APPLICATION_USER_ACCOUNT_CONTACT_UPDATE(),
                        '-component'  => COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT_UPDATE(),
                        '-action'     => FORM_ACTION_POPULATE,
                        '-user_contact_id' => $user_contact->{'-user_contact_id'},
                    },
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 90_830 } ),    # phrase_id 90830 - Delete
                        '-program'    => PROGRAM_SITE_APPLICATION_USER_ACCOUNT(),
                        '-component'  => COMP_HTML_APPLICATION_USER_ACCOUNT_CONTACT(),
                        '-action'     => 'delete',
                        '-user_contact_id' => $user_contact->{'-user_contact_id'},
                        '-data'            => { $js_confirm_util->render( { '-type' => JS_CONFIRM_TYPE_DELETE() } ) },
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        $content .= $self->table->render(
            {   '-name'                 => $self->name . $user_contact->{'-user_contact_id'},
                '-class'                => TABLE_CLASS_VERTICAL,
                '-show_header'          => 0,
                '-extra_footer_content' => $links,
                '-data'                 => $data,
            } );
    } ## end foreach my $user_contact ( ...)

    return $content;
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_contact'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user contact
    my $user_contact = $args->{'-user_contact'};

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   db country util
    my $db_country_util = $self->utils->{ UTIL_DB_RECORD_R_COUNTRY() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   cgi
    my $cgi = $self->cgi;

    #   country
    my $country = $db_country_util->fetch_one( { '-country_id' => $user_contact->{'-country_id'} } );

    #   country text
    my $country_text;
    if ( defined $country ) {
        $country_text = $phrase_util->text( { '-phrase_id' => $country->{'-phrase_id'} } );
    }

    #   user contact type
    my $user_contact_type = $contact_util->fetch_one_user_contact_type( { '-user_contact_type_id' => $user_contact->{'-user_contact_type_id'}, } );

    #   contact type text
    my $user_contact_type_text;
    if ( defined $user_contact_type ) {
        $user_contact_type_text = $phrase_util->text( { '-phrase_id' => $user_contact_type->{'-phrase_id'}, } );
    }

    my $return_data = [];

    push @{$return_data}, [ $cgi->div( { '-class' => 'HeadingMedium' }, $user_contact_type_text ), undef, ];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_900 } ), $user_contact->{'-name'}, ];    # phrase_id 90900 - Attention

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_910 } ), $user_contact->{'-address_line_one'}, ];    # phrase_id 90910 - Address Line One

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_920 } ), $user_contact->{'-address_line_two'}, ];    # phrase_id 90920 - Address Line Two

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_930 } ), $country_text, ];                           # phrase_id 90930 - Country

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_940 } ), $user_contact->{'-state_province'}, ];      # phrase_id 90940 - State / Province

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_950 } ), $user_contact->{'-postal_code'}, ];         # phrase_id 90950 - Postal Code

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 90_960 } ), $user_contact->{'-phone'}, ];               # phrase_id 90960 - Phone

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   application user
    my $user = $user_util->fetch_current_application_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current application user' } );
        return;
    }

    #   user id
    my $user_id = $user->{'-user_id'};

    my $action = $args->{'-action'};

    my $data = $args->{'-data'};

    #   user contact id
    my $user_contact_id = $self->html_strip( $data->{'user_contact_id'} );

    if ( $action eq 'delete' ) {

        if (!$contact_util->delete_user_contact(
                {   '-user_contact_id' => $user_contact_id,
                    '-user_id'         => $user_id,
                } )
            ) {
            $self->error( { '-text' => 'Failed to delete -user_contact_id ' . $self->dbg_value($user_contact_id) } );
            return COMP_ACTION_FAILURE;
        }

        $self->debug( { '-text' => 'Deleted -user_contact_id ' . $self->dbg_value($user_contact_id) } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 90_970 } ),    # phrase_id 90970 - Application user contact has been deleted
                '-type' => MESSAGE_TYPE_SUCCESS,
            } );

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

__END__
