package AlienSpaces::Comp::HTML::User::Oauth;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   Facebook login
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [
        @{ $self->SUPER::util_list },

        #   util oauth
        UTIL_OAUTH(),

        #   util user
        UTIL_USER(),
    ];

    return $utility_list;
}

1;

