package AlienSpaces::Comp::HTML::User::Oauth::Login;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   Facebook login
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp);

our @DYNISA = qw(Comp::HTML::User::Oauth);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ 
        @{ $self->SUPER::util_list }, 
    ];

    return $utility_list;
}

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   oauth util
    my $oauth_util = $self->utils->{ UTIL_OAUTH() };

    #   oauth recs
    my $oauth_recs = $oauth_util->fetch_oauths( { '-local_domain' => $self->environment->{'APP_HOSTNAME'}, } );
    if ( !defined $oauth_recs || !scalar @{$oauth_recs} ) {
        $self->debug( { '-text' => 'No -oauth_recs, not displaying oauth options' } );
        return;
    }

    #   content
    my $content = '';

    if ( !$user_util->check_user_logged_in() ) {

        foreach my $oauth_rec ( @{$oauth_recs} ) {
            my $oauth_id = $oauth_rec->{'-oauth_id'};
            my $oauth_text = $phrase_util->text( { '-phrase_id' => $oauth_rec->{'-phrase_id'} } );

            my $oauth_login_url = $self->make_program_link(
                {   '-program'  => PROGRAM_SITE_USER_OAUTH_LOGIN(),
                    '-oauth_id' => $oauth_id,
                } );

            #   css class
            my $css_class = $self->make_css_class( { '-string' => $oauth_text } );

            $content .=

                #   div
                $cgi_util->div(
                { '-class' => 'OAuthLogin' },

                #   div
                $cgi_util->div(
                    { '-class' => $css_class },

                    #   a
                    $cgi_util->a( { '-href' => $oauth_login_url }, $oauth_text ) ) );
        }
    }

    return $self->panel->render(
        {   '-name'    => $self->name,
            '-content' => $content,
            '-title'   => 'Log in or register with',
        } );

} ## end sub render

sub action {
    my ( $self, $args ) = @_;

    #   action
    my $action = delete $args->{'-action'};

    #   data
    my $data = $args->{'-data'};

    #   login
    if ( $action eq 'login' ) {

        my $oauth_id = $data->{'oauth_id'};
        if (!$self->action_oauth_login(
                {   '-oauth_id'     => $oauth_id,
                    '-local_domain' => $self->environment->{'APP_HOSTNAME'},
                } )
            ) {
            $self->error( { '-text' => 'Failed to handle oauth login action' } );
            return COMP_ACTION_FAILURE();
        }
    }

    return COMP_ACTION_SUCCESS();
}

#   action oauth login
sub action_oauth_login {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-oauth_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   oauth util
    my $oauth_util = $self->utils->{ UTIL_OAUTH() };

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   oauth id
    my $oauth_id = $args->{'-oauth_id'};

    my $oauth_login_uri = $oauth_util->get_oauth_login_uri(
        {   '-oauth_id'     => $oauth_id,
            '-local_domain' => $self->environment->{'APP_HOSTNAME'},
        } );

    $self->debug( { '-text' => 'Issuing redirect to -oauth_login_url ' . $self->dbg_value($oauth_login_uri) } );

    #   set session login oauth id
    #   - this will be used when returning to /html/user/oauth/register
    #     to identify the oauth_id we should have just authenticated against
    $session_util->set_session( { '-key' => '-login_oauth_id', '-value' => $oauth_id } );

    $self->redirect( { '-url' => $oauth_login_uri } );

    return 1;
} ## end sub action_oauth_login

1;

