package AlienSpaces::Comp::HTML::User::Oauth::Register;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   Facebook return register
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::Comp::HTML::Form::Basic);

use AlienSpaces::Constants qw(:utils :programs :comp :form :error-codes-user :oauth :message-types);

use constant {
    REGISTERED_REQUIRES_ACTIVATION    => 1,
    REGISTERED_REQUIRES_NO_ACTIVATION => 2,
};

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_USER(), UTIL_OAUTH(), UTIL_SESSION() ];
}

#   form config
my $form_config = {

    #   fields
    '-fields' => [
        {   '-name'            => 'handle',
            '-label_phrase_id' => 20_110,
            '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
            '-data_type'       => FORM_DATA_TYPE_TEXT,
            '-values_method'   => undef,
            '-validate_rules'  => { '-required' => 1, },
        },
        {   '-name'            => 'email_address',
            '-label_phrase_id' => 20_005,
            '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
            '-data_type'       => FORM_DATA_TYPE_TEXT,
            '-values_method'   => undef,
            '-validate_rules'  => { '-required' => 1, },
        },

        #   optional possible fields / override this component with new configuration
        #            {   '-name'            => 'first_name',
        #                '-label_phrase_id' => 20_115,
        #                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
        #                '-data_type'       => FORM_DATA_TYPE_TEXT,
        #                '-values_method'   => undef,
        #            },
        #            {   '-name'            => 'last_name',
        #                '-label_phrase_id' => 20_120,
        #                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
        #                '-data_type'       => FORM_DATA_TYPE_TEXT,
        #                '-values_method'   => undef,
        #            },
    ],

    #   auto complete
    '-auto_complete' => 0,    #   turn off auto complete, is on by default

    #   submit button
    '-submit_label_phrase_id' => 20_125,

    '-show_reset' => 0,

    #   submit failed phrase
    '-submit_failed_text_method' => 'submit_failed_text',

    #   submit success phrase
    '-submit_success_text_method' => 'submit_success_text',

    #   display after submit
    '-display_after_submit' => '1',

    #  - defaults to 0 (overrides -display_after_submit)
};

sub form_config {
    my ( $self, $args ) = @_;

    return $form_config;
}

sub form_populate {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   oauth user
    my $oauth_user = $session_util->get_session( { '-key' => '-oauth_user' } );
    if ( !defined $oauth_user ) {
        $self->error( { '-text' => 'Missing oauth user from session, cannot populate form fields' } );
        return;
    }

    my $form_field_data = {
        '-handle'        => $oauth_user->{'-handle'},
        '-email_address' => $oauth_user->{'-email_address'},
        '-first_name'    => $oauth_user->{'-first_name'},
        '-last_name'     => $oauth_user->{'-last_name'},
    };

    $self->debug( { '-text' => 'Have -form_field_data ' . $self->dbg_value($form_field_data) } );

    return $form_field_data;
}

#   action
sub action {
    my ( $self, $args ) = @_;

    #   oauth util
    my $oauth_util = $self->utils->{ UTIL_OAUTH() };

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   action
    my $action = $args->{'-action'};

    #   data
    my $data = $args->{'-data'};

    #   get session login oauth id
    #   - this would have been set by /html/user/oauth/login
    #     when redirecting to oauth provider for authentication
    my $oauth_id = $session_util->get_session( { '-key' => '-login_oauth_id' } );

    #   code
    my $code = $data->{'code'};

    #   register-confirm
    if ( $action eq 'register-confirm' ) {
        $self->debug( { '-text' => 'Confirm register user -oauth_id ' . $self->dbg_value($oauth_id) . ' -code ' . $self->dbg_value($code) } );

        #   oauth user
        my $oauth_user = $oauth_util->get_user_data(
            {   '-oauth_id'     => $oauth_id,
                '-code'         => $code,
                '-local_domain' => $self->environment->{'APPLICATION_HOSTNAME'},
            } );

        if ( !defined $oauth_user ) {
            $self->error( { '-text' => 'Failed to get oauth user data, redirecting' } );

            #   redirect
            $self->redirect( { '-url' => '/' } );

            return COMP_ACTION_FAILURE();
        }

        $self->debug( { '-text' => 'Have -oauth_user ' . $self->dbg_value($oauth_user) } );

        #   continue with confirm register path
        my $user_data = $user_util->parse_oauth_user_data( { '-oauth_user' => $oauth_user } );
        if ( !defined $user_data ) {
            $self->error( { '-text' => 'Failed to fetch user data, cannot populate form fields' } );
            return;
        }

        while ( my ( $k, $v ) = each %{$user_data} ) {
            $self->debug( { '-text' => 'Setting oauth user ' . $k . ' to ' . $v } );
            $oauth_user->{$k} = $v;
        }

        $self->debug( { '-text' => 'Checking for already registered user' } );

        #   registered
        my $registered_oauth_user = $user_util->fetch_oauth_user( { '-oauth_user' => $oauth_user, '-oauth_id' => $oauth_id } );
        if ( defined $registered_oauth_user ) {
            if ( $user_util->login_oauth_user( { '-oauth_user' => $oauth_user, '-oauth_id' => $oauth_id } ) ) {
                $self->debug( { '-text' => 'Logging in previously registered user ' } );

                #   redirect
                $self->redirect( { '-url' => '/' } );

                return COMP_ACTION_SUCCESS();
            }
        }

        $self->debug( { '-text' => 'User not registered' } );

        #   set oauth user
        $session_util->set_session( { '-key' => '-oauth_user', '-value' => $oauth_user } );

        return COMP_ACTION_SUCCESS();
    } ## end if ( $action eq 'register-confirm')

    #   super
    return $self->SUPER::action($args);
} ## end sub action

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    #   data
    my $data = $args->{'-data'};

    #   oauth util
    my $oauth_util = $self->utils->{ UTIL_OAUTH() };

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   get session login oauth id
    #   - this would have been set by /html/user/oauth/login
    #     when redirecting to oauth provider for authentication
    my $oauth_id = $session_util->get_session( { '-key' => '-login_oauth_id' } );

    $self->debug( { '-text' => 'Registering user -oauth_id ' . $self->dbg_value($oauth_id) } );

    #   oauth user
    my $oauth_user = $session_util->get_session( { '-key' => '-oauth_user' } );

    #   handle
    $oauth_user->{'-handle'} = $data->{'handle'};

    #   email address
    $oauth_user->{'-email_address'} = $data->{'email_address'};

    if (!$self->register_oauth_user(
            {   '-oauth_user'   => $oauth_user,
                '-oauth_id'     => $oauth_id,
                '-local_domain' => $self->environment->{'APPLICATION_HOSTNAME'},
            } )
        ) {
        $self->error( { '-text' => 'Failed to register user, redirecting' } );

        #	add program message
        $self->add_program_message(
            {   '-text' => 'A problem was encountered when registering your account. Please contact support.',
                '-type' => MESSAGE_TYPE_ERROR,
            } );

        #   redirect
        $self->redirect( { '-url' => '/' } );

        return COMP_ACTION_FAILURE();
    }

    $self->add_program_message(
        {   '-text' => 'Thank you for registering.',
            '-type' => MESSAGE_TYPE_ERROR,
        } );

    #   redirect
    $self->redirect( { '-url' => '/' } );

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

#   submit success text
sub submit_success_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   registered status
    my $registered_status = $self->{'-registered_status'};
    if ( !defined $registered_status ) {
        $self->error( { '-text' => 'Apparently registered but have no registered status' } );
        return;
    }

    #   text
    my $text;
    if ( $registered_status == REGISTERED_REQUIRES_ACTIVATION ) {

        #   Your registration has been submitted. Please check your email for account
        #   activation instructions. If you have problems please contact <ENV:APPLICATION_SYSTEM_SUPPORT_EMAIL>,
        #   Thankyou for registering!
        $text = $phrase_util->text( { '-phrase_id' => '20135' } );

    }
    elsif ( $registered_status == REGISTERED_REQUIRES_NO_ACTIVATION ) {

        #   Your registration was successful. You may now log in.
        $text = $phrase_util->text( { '-phrase_id' => '20140' } );
    }

    return $text;
} ## end sub submit_success_text

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   text
    my $text = '';

    #   errors
    my $errors = $self->get_errors();
    if ( defined $errors ) {
        foreach my $error ( @{$errors} ) {
            if ( $error->{'-error_code'} == ERROR_CODE_USER_HANDLE_NOT_UNIQUE ) {

                #   An account with this handle already exists. Please try a different handle.
                $text .= $phrase_util->text( { '-phrase_id' => '20145' } );
            }
            if ( $error->{'-error_code'} == ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE ) {

                #   An account with this email address already exists. Please try a different email address.
                $text .= $phrase_util->text( { '-phrase_id' => '20150' } );
            }
        }
    }
    else {

        #   There has been a problem registering your account. Please try again shortly.
        #   If the problem persists please contact <ENV:APPLICATION_SYSTEM_SUPPORT_EMAIL>, Thankyou!
        $text .= $phrase_util->text( { '-phrase_id' => '20130' } );
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
} ## end sub submit_failed_text

#   register oauth user
sub register_oauth_user {
    my ( $self, $args ) = @_;

    #   ouath user
    my $oauth_user = $args->{'-oauth_user'};

    #   oauth id
    my $oauth_id = $args->{'-oauth_id'};

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    my $registered_oauth_user = $user_util->fetch_oauth_user( { '-oauth_user' => $oauth_user, '-oauth_id' => $oauth_id } );
    if ( !defined $registered_oauth_user ) {
        $self->debug( { '-text' => 'Could not find existing oauth user account, attempting to register new oauth user' } );
        $registered_oauth_user = $user_util->register_oauth_user( { '-oauth_user' => $oauth_user, '-oauth_id' => $oauth_id } );
        if ( !defined $registered_oauth_user ) {
            $self->error( { '-text' => 'Failed to register new oauth user' } );
            return;
        }
    }

    if ( !$user_util->login_oauth_user( { '-oauth_user' => $registered_oauth_user, '-oauth_id' => $oauth_id } ) ) {
        $self->error( { '-text' => 'Failed to login oauth user' } );
        return;
    }

    $self->debug( { '-text' => 'Logged in -oauth_user ' . $self->dbg_value($oauth_user) } );

    return 1;
} ## end sub register_oauth_user

1;

