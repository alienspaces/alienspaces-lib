package AlienSpaces::Comp::HTML::User::Oauth::Register::Dialogue;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:comp :util :oauth);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

my $oauth_phrase_map = {
    OAUTH_FACEBOOK()  => 60000,
    OAUTH_GOOGLE()    => 60010,
    OAUTH_INSTAGRAM() => 60020,
};

sub render {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   oauth user
    my $oauth_user = $session_util->get_session( { '-key' => '-oauth_user' } );
    if ( !defined $oauth_user ) {
        $self->error( { '-text' => 'Missing oauth user from session, cannot render dialogue' } );
        return;
    }

    $self->debug( { '-text' => 'Have -oauth_user ' . $self->dbg_value($oauth_user) } );

    #   phrase util
    my $phrase_util = $self->utils->{UTIL_PHRASE()};

    my $content = $phrase_util->text({
        '-phrase_id' => 20155, 
        '-args' => {
            '-oauth_account' => $phrase_util->text({'-phrase_id' => $oauth_phrase_map->{$oauth_user->{'-oauth_id'}}}),
        }
    });

    return $self->panel->render(
        {   '-name'    => $self->name,
            '-content' => $content,
            '-class'   => 'Dialogue',
        } );
}

sub action {
    my ( $self, $args ) = @_;

    #   action
    my $action = $args->{'-action'};
    if ( !defined $action ) {
        return COMP_ACTION_FAILURE();
    }

    #   data
    my $data = $args->{'-data'};

    return COMP_ACTION_FAILURE();
}

1;

