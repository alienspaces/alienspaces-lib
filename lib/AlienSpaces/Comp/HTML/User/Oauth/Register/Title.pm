package AlienSpaces::Comp::HTML::User::Oauth::Register::Title;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:comp :util);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{UTIL_PHRASE()};

    my $content = $phrase_util->text({'-phrase_id' => 20103});

    return $self->panel->render(
        {   '-name'    => $self->name,
            '-content' => $content,
            '-class' => 'PageTitle',
        } );
}

sub action {
    my ( $self, $args ) = @_;

    #   action
    my $action = $args->{'-action'};
    if ( !defined $action ) {
        return COMP_ACTION_FAILURE();
    }

    #   data
    my $data = $args->{'-data'};

    return COMP_ACTION_FAILURE();
}

1;

