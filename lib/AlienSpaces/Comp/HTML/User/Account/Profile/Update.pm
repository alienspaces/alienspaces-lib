package AlienSpaces::Comp::HTML::User::Account::Profile::Update;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_USER(), UTIL_REQUEST(), ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 71_100,

        '-fields' => [
            {   '-name'            => 'handle',
                '-clear'           => 0,
                '-label_phrase_id' => 71_110,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => '1',    #   required
                    '-min_length' => '3',    #   min length
                },
            },
            {   '-name'            => 'first_name',
                '-clear'           => 0,
                '-label_phrase_id' => 71_120,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'            => 'last_name',
                '-clear'           => 0,
                '-label_phrase_id' => 71_130,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
        ],

        #   auto complete
        '-auto_complete' => 0,    #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed phrase
        '-submit_failed_text_method' => 'submit_failed_text',

        #   submit success text
        '-submit_success_text' => 'Your personal information has been updated',

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, 'Return to account' );

    return $content;
}

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_util->user_id } );

    return $user;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    my $data = $args->{'-data'};

    my $handle     = $data->{'handle'};
    my $first_name = $data->{'first_name'};
    my $last_name  = $data->{'last_name'};

    $self->debug( { '-text' => 'Attempting to register user' } );

    #   html strip
    $handle     = $self->html_strip($handle);
    $first_name = $self->html_strip($first_name);
    $last_name  = $self->html_strip($last_name);

    if ($user_util->update_user(
            {   '-user_id'    => $user_util->user_id,
                '-handle'     => $handle,
                '-first_name' => $first_name,
                '-last_name'  => $last_name,
            } )
        ) {

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub form_action

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   text
    my $text = '';

    #   errors
    my $errors = $self->get_errors();
    if ( defined $errors ) {
        foreach my $error ( @{$errors} ) {
            if ( $error->{'-error_code'} == ERROR_CODE_USER_HANDLE_NOT_UNIQUE ) {

                #   An account with this handle already exists. Please try a different handle.
                $text .= $phrase_util->text( { '-phrase_id' => '20145' } );
            }
            if ( $error->{'-error_code'} == ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE ) {

                #   An account with this email address already exists. Please try a different email address.
                $text .= $phrase_util->text( { '-phrase_id' => '20150' } );
            }
        }
    }
    else {

        #   There has been a problem registering your account. Please try again shortly.
        #   If the problem persists please contact <ENV:APP_SYSTEM_SUPPORT_EMAIL>, Thankyou!
        $text .= $phrase_util->text( { '-phrase_id' => '20130' } );
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
} ## end sub submit_failed_text

1;

