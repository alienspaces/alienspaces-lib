package AlienSpaces::Comp::HTML::User::Account::Profile;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :table-style);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_OUTPUT_HTML_JS_CONFIRM(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   js confirm util
    my $js_confirm_util = $self->utils->{ UTIL_OUTPUT_HTML_JS_CONFIRM() };

    my $data = $self->get_table_data();
    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to fetch account profile data' } );
        return;
    }

    #   edit link
    my $edit_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-name' => $phrase_util->text( { '-phrase_id' => '71000' } ),    # phrase_id 71000 - Edit
                    '-program' => PROGRAM_SITE_USER_ACCOUNT_PROFILE(),
                },
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    return $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => '71010' } ),    # phrase_id 71010 - Edit
            '-class'                => TABLE_CLASS_VERTICAL,
            '-extra_footer_content' => $edit_link,
            '-data'                 => $data,
        } );
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_util->user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch user for -user_id ' . $self->dbg_value( $user_util->user_id ) } );
        return;
    }

    my $return_data = [];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => '71020' } ), $user->{'-handle'}, ];    # phrase_id 71020 - Handle

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => '71030' } ), $user->{'-first_name'}, ];    # phrase_id 71030 - First Name

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => '71040' } ), $user->{'-last_name'}, ];     # phrase_id 71040 - Last Name

    return $return_data;
}

1;

__END__
