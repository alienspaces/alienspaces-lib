package AlienSpaces::Comp::HTML::User::Account::Email::Primary::Update;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :error-codes);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

use constant { REQUIRES_CONFIRMATION => 1, };

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_REQUEST(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 73_500,

        '-auto_complete' => 0,

        '-fields' => [
            {   '-name'            => 'password',
                '-clear'           => 1,
                '-label_phrase_id' => 73_510,
                '-input_type'      => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => 1,
                    '-min_length' => 6,
                },
            },
            {   '-name'            => 'email_address',
                '-label_phrase_id' => 73_520,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required'   => '1',         #   required
                    '-min_length' => '3',         #   min length
                    '-do_match'   => '.+\@.+',    #   must match this regular expression
                },
            },
            {   '-name'            => 'confirm_email_address',
                '-label_phrase_id' => 73_530,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_method' => 'validate_confirm_email_address',
                '-validate_rules'  => {
                    '-required'   => '1',         #   required
                    '-min_length' => '3',         #   min length
                    '-do_match'   => '.+\@.+',    #   must match this regular expression
                },
                '-validate_text_method' => 'validate_confirm_email_address_text_method',
            },
        ],

        #   auto complete
        '-auto_complete' => 0,                    #   turn off auto complete, is on by default

        #   submit button
        '-submit_label_text' => 'Update primary email',

        '-show_reset' => 0,

        #   submit failed phrase
        '-submit_failed_text_method' => 'submit_failed_text',

        #   submit success phrase
        '-submit_success_text_method' => 'submit_success_text',

        #   display after submit failure
        '-display_after_submit_failure' => 1,

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, 'Return to account' );

    return $content;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super failed init' } );
        return;
    }

    #   confirmation status
    $self->{'-confirmation_status'} = undef;

    return 1;
}

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_util->user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current user' } );
        return;
    }

    return { '-current_email_address' => $user->{'-email_address'} };
}

sub validate_confirm_email_address {
    my ( $self, $args ) = @_;

    my $data  = $args->{'-data'};
    my $field = $args->{'-field'};

    my $email_address         = $data->{'email_address'};
    my $confirm_email_address = $data->{'confirm_email_address'};

    if ( !defined $confirm_email_address || $confirm_email_address eq '' ) {
        $self->debug( { '-text' => 'Failed required' } );
        $field->{'-validation_error'} = VALIDATION_ERROR_REQUIRED;
        return;
    }

    if ( !defined $confirm_email_address || $confirm_email_address ne $email_address ) {
        $self->debug( { '-text' => 'Failed match' } );
        return;
    }

    $self->debug( { '-text' => 'Passed validation' } );

    return 1;
}

sub validate_confirm_email_address_text_method {
    my ( $self, $args ) = @_;

    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $field = $args->{'-field'};

    if ( defined $field->{'-validation_error'} && $field->{'-validation_error'} == VALIDATION_ERROR_REQUIRED ) {
        return $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_REQUIRED_PHRASE() } );
    }

    return $phrase_util->text( { '-phrase_id' => 73_540 } );
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    my $data = $args->{'-data'};

    my $password              = $data->{'password'};
    my $email_address         = $data->{'email_address'};
    my $confirm_email_address = $data->{'confirm_email_address'};

    #   html strip
    $password              = $self->html_strip($password);
    $email_address         = $self->html_strip($email_address);
    $confirm_email_address = $self->html_strip($confirm_email_address);

    my $user = $user_util->fetch_current_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current user, cannot update email address' } );
        return COMP_ACTION_FAILURE;
    }

    #   authenticate
    #   - we need to be sure this person is who they say
    my ( $user_id, $user_status_id ) = $user_util->authenticate_local_user(
        {   '-email_address' => $user->{'-email_address'},
            '-password'      => $password,
        } );

    if ( !defined $user_id || $user_id != $user_util->user_id ) {
        $self->error( { '-text' => 'Authenticating for -email_address ' . $user->{'-email_address'} . ' failed' } );

        $self->set_error( { '-error_code' => ERROR_CODE_UNAUTHORIZED() } );
        return COMP_ACTION_FAILURE;
    }

    #   user
    $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $self->dbg_value($user_id) } );
        return COMP_ACTION_FAILURE;
    }

    #   fetch default user configuration
    my $user_config = $user_util->fetch_user_config( { '-user_config_id' => $user->{'-user_config_id'} } );
    if ( !defined $user_config ) {
        $self->error( { '-text' => 'Failed to fetch user configuration for -user_id ' . $user_id } );
        return COMP_ACTION_FAILURE;
    }

    #   does email change require email confirmation
    if ( $user_config->{'-email_change_confirmation_enabled'} ) {

        $password = $user_util->encrypt_user_password(
            {   '-email_address' => $email_address,
                '-password'      => $password,
            } );

        if (!$request_util->add_request(
                {   '-request_type_id' => REQUEST_TYPE_USER_CHANGE_EMAIL_ADDRESS,
                    '-user_id'         => $user_id,
                    '-email_address'   => $email_address,
                    '-request_data'    => [
                        {   '-table_name'  => 'as_user',
                            '-column_name' => 'email_address',
                            '-pk_value'    => $user_id,
                            '-new_value'   => $email_address,
                        },
                        {   '-table_name'  => 'as_user',
                            '-column_name' => 'password',
                            '-pk_value'    => $user_id,
                            '-new_value'   => $password,
                        },
                    ] } )
            ) {
            $self->error( { '-text' => 'Failed to add request for user change email' } );
            return COMP_ACTION_FAILURE;
        }

        #   confirmation status
        $self->{'-confirmation_status'} = REQUIRES_CONFIRMATION;

        return COMP_ACTION_SUCCESS;

    } ## end if ( $user_config->{'-email_change_confirmation_enabled'...})

    $self->debug( { '-text' => 'Update user email' } );

    #   update user
    if ($user_util->update_user(
            {   '-user_id'       => $user_util->user_id,
                '-email_address' => $email_address,
                '-password'      => $password,
            } )
        ) {
        $self->debug( { '-text' => 'User -user_id ' . $user_id . ' email address and password updated' } );

        #   confirmation status
        $self->{'-confirmation_status'} = undef;

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub form_action

#   submit success text
sub submit_success_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   confirmation status
    my $confirmation_status = $self->{'-confirmation_status'};

    #   text
    my $text;
    if ( defined $confirmation_status && $confirmation_status == REQUIRES_CONFIRMATION ) {

        $text = $phrase_util->text( { '-phrase_id' => 73_570 } );
    }
    else {

        $text = $phrase_util->text( { '-phrase_id' => 73_580 } );
    }

    return $text;
}

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   text
    my $text;

    #   errors
    my $errors = $self->get_errors();
    if ( defined $errors ) {
        foreach my $error ( @{$errors} ) {
            if ( $error->{'-error_code'} == ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE ) {

                $text = $phrase_util->text( { '-phrase_id' => 50_195 } );
            }
            elsif ( $error->{'-error_code'} == ERROR_CODE_UNAUTHORIZED() ) {

                $text = $phrase_util->text({'-phrase_id' => 73_550});
            }
        }
    }
    else {

        $text = $phrase_util->text( { '-phrase_id' => 73_560 } );
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
} ## end sub submit_failed_text

1;

