package AlienSpaces::Comp::HTML::User::Account::Email::Alternate;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :table-style :request :user :error-codes-user);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $data = $self->get_table_data();
    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to fetch account profile data' } );
        return;
    }

    #   edit link
    my $edit_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-name' => $phrase_util->text( { '-phrase_id' => '72000' } ),    # phrase_id 72000 - Edit
                    '-program' => PROGRAM_SITE_USER_ACCOUNT_EMAIL_ALTERNATE(),
                },
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    return $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => '72010' } ),    # phrase_id 72010 - Alternate Email
            '-class'                => TABLE_CLASS_VERTICAL,
            '-extra_footer_content' => $edit_link,
            '-data'                 => $data,
        } );
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    my $user = $user_util->fetch_current_user();

    my $return_data = [];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 72_020 } ), $user->{'-alternate_email_address'}, ];

    return $return_data;
}

1;

__END__
