package AlienSpaces::Comp::HTML::User::Account::Contact::Update;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :contact-type :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),

        #   db record
        UTIL_DB_RECORD_R_COUNTRY(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 70_200,
        '-session'          => [ { '-name' => 'user_contact_id', }, ],
        '-fields'           => [
            {   '-name'              => 'user_contact_type_id',
                '-label_phrase_id'   => 70_280,
                '-input_type'        => FORM_INPUT_TYPE_TEXT,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_user_contact_type',
                '-clear'             => 0,
            },
            {   '-name'            => 'name',
                '-label_phrase_id' => 70_210,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
                '-clear' => 0,
            },
            {   '-name'            => 'address_line_one',
                '-label_phrase_id' => 70_220,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
                '-clear' => 0,
            },
            {   '-name'            => 'address_line_two',
                '-label_phrase_id' => 70_230,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-clear'           => 0,
            },
            {   '-name'              => 'country_id',
                '-label_phrase_id'   => 70_240,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_country',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
                '-clear' => 0,
            },
            {   '-name'            => 'state_province',
                '-label_phrase_id' => 70_250,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
                '-clear' => 0,
            },
            {   '-name'            => 'postal_code',
                '-label_phrase_id' => 70_260,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
                '-clear' => 0,
            },
            {   '-name'            => 'phone',
                '-label_phrase_id' => 70_270,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-clear'           => 0,
            },
        ],

        #   auto complete
        '-auto_complete' => 0,    #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed text
        '-submit_failed_text' => 'A problem was encountered while updating your contact information',

        #   submit success text
        '-submit_success_text' => 'Your contact information has been updated',

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, 'Return to account' );

    return $content;
}

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    my $session_data = $args->{'-session_data'};
    if ( !defined $session_data ) {
        $self->error( { '-text' => 'Missing session data, cannot populate' } );
        return;
    }

    my $user_contact_id = $session_data->{'user_contact_id'};
    if ( !defined $user_contact_id ) {
        $self->error( { '-text' => 'Missing user contact id, cannot populate' } );
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   contact
    my $contact = $contact_util->fetch_one_user_contact(
        {   '-user_contact_id' => $user_contact_id,
            '-user_id'         => $user_util->user_id,
        } );

    return $contact;
} ## end sub form_populate

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    #   data
    my $data = $args->{'-data'};

    #   session data
    my $session_data = $args->{'-session_data'};

    my $user_contact_id = $session_data->{'user_contact_id'};
    if ( !defined $user_contact_id ) {
        $self->error( { '-text' => 'Missing -user_contact_id in session data, cannot update contact' } );
        return COMP_ACTION_FAILURE;
    }

    if ( $self->update_contact( { '-user_contact_id' => $user_contact_id, '-data' => $data } ) ) {

        return COMP_ACTION_SUCCESS;
    }

    $self->error( { '-text' => 'Failed to update user contact' } );

    return COMP_ACTION_FAILURE;
}

#   update contact
sub update_contact {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   user contact id
    my $user_contact_id = $args->{'-user_contact_id'};

    #   data
    my $data = $args->{'-data'};

    my $name             = $data->{'name'};
    my $address_line_one = $data->{'address_line_one'};
    my $address_line_two = $data->{'address_line_two'};
    my $country_id       = $data->{'country_id'};
    my $state_province   = $data->{'state_province'};
    my $postal_code      = $data->{'postal_code'};
    my $phone            = $data->{'phone'};

    #   html strip
    $name             = $self->html_strip($name);
    $address_line_one = $self->html_strip($address_line_one);
    $address_line_two = $self->html_strip($address_line_two);
    $country_id       = $self->html_strip($country_id);
    $state_province   = $self->html_strip($state_province);
    $postal_code      = $self->html_strip($postal_code);
    $phone            = $self->html_strip($phone);

    #   update contact
    if (!$contact_util->update_user_contact(
            {   '-user_contact_id'  => $user_contact_id,
                '-user_id'          => $user_util->user_id,
                '-name'             => $name,
                '-address_line_one' => $address_line_one,
                '-address_line_two' => $address_line_two,
                '-country_id'       => $country_id,
                '-state_province'   => $state_province,
                '-postal_code'      => $postal_code,
                '-phone'            => $phone,
            } )
        ) {
        $self->error( { '-text' => 'Failed to update contact' } );
        return;
    }

    $self->debug( { '-text' => 'Contact updated' } );

    return 1;
} ## end sub update_contact

1;

