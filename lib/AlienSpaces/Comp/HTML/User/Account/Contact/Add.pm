package AlienSpaces::Comp::HTML::User::Account::Contact::Add;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :contact-type :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),

        #   db record
        UTIL_DB_RECORD_R_COUNTRY(),
        UTIL_DB_RECORD_R_USER_CONTACT_TYPE(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 70_300,    #

        '-fields' => [
            {   '-name'              => 'user_contact_type_id',
                '-label_phrase_id'   => 70_380,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_user_contact_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'name',
                '-label_phrase_id' => 70_310,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_one',
                '-label_phrase_id' => 70_320,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_two',
                '-label_phrase_id' => 70_330,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'              => 'country_id',
                '-label_phrase_id'   => 70_340,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_country',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'state_province',
                '-label_phrase_id' => 70_350,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'postal_code',
                '-label_phrase_id' => 70_360,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'phone',
                '-label_phrase_id' => 70_370,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
        ],

        #   auto complete
        '-auto_complete' => 0,             #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed text
        '-submit_failed_text' => 'A problem was encountered while adding new contact',

        #   submit success text
        '-submit_success_text' => 'New contact has been added',

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, 'Return to account' );

    return $content;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   data
    my $data = $args->{'-data'};

    $self->error( { '-text' => 'Adding new user contact' } );

    if ( $self->add_contact( { '-data' => $data } ) ) {
        return COMP_ACTION_SUCCESS;
    }

    $self->error( { '-text' => 'Failed to add new user contact' } );

    return COMP_ACTION_FAILURE;
}

#   add contact
sub add_contact {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   data
    my $data = $args->{'-data'};

    my $user_contact_type_id = $data->{'user_contact_type_id'};
    my $name                 = $data->{'name'};
    my $address_line_one     = $data->{'address_line_one'};
    my $address_line_two     = $data->{'address_line_two'};
    my $country_id           = $data->{'country_id'};
    my $state_province       = $data->{'state_province'};
    my $postal_code          = $data->{'postal_code'};
    my $phone                = $data->{'phone'};

    #   html strip
    $user_contact_type_id = $self->html_strip($user_contact_type_id);
    $name                 = $self->html_strip($name);
    $address_line_one     = $self->html_strip($address_line_one);
    $address_line_two     = $self->html_strip($address_line_two);
    $country_id           = $self->html_strip($country_id);
    $state_province       = $self->html_strip($state_province);
    $postal_code          = $self->html_strip($postal_code);
    $phone                = $self->html_strip($phone);

    #   contact id
    my $contact_id = $contact_util->insert_user_contact(
        {   '-user_id'              => $user_util->user_id,
            '-user_contact_type_id' => $user_contact_type_id,
            '-name'                 => $name,
            '-address_line_one'     => $address_line_one,
            '-address_line_two'     => $address_line_two,
            '-country_id'           => $country_id,
            '-state_province'       => $state_province,
            '-postal_code'          => $postal_code,
            '-phone'                => $phone,
        } );

    if ( !defined $contact_id ) {
        $self->error( { '-text' => 'Failed to insert contact' } );
        return;
    }

    $self->debug( { '-text' => 'Inserted -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub add_contact

1;

