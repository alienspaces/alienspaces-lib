package AlienSpaces::Comp::HTML::User::Account::Password::Reset;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(
    :util
    :program
    :form :comp
    :request
    :user
    :contact-type
    :error-codes-user
);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),
        UTIL_REQUEST(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 77_000,

        '-fields'           => [
            {   '-name'            => 'email_address',
                '-label_phrase_id' => 77_010,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {                            #   Validation rules
                    '-required'   => '1',                          #   required
                    '-min_length' => '3',                          #   min length
                    '-do_match'   => '.+\@.+',                     #   must not match this regular expression
                },
            },
        ],

        #   auto complete
        '-auto_complete' => 0,

        #   submit label phrase id
        '-submit_label_phrase_id' => 77_020,

        '-show_reset' => 0,

        #   submit success phrase id
        '-submit_success_phrase_id' => 77_030,

        #   submit failed phrase id
        '-submit_failed_phrase_id' => 77_040,

        #   display after submit failure
        '-display_after_submit_failure' => 1,

        #   display after submit success
        '-display_after_submit_success' => 0,

    };
} ## end sub form_config

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   data
    my $data = $args->{'-data'};

    #   email address
    my $email_address = $data->{'email_address'};

    #   html strip
    $email_address = $self->html_strip($email_address);

    #   found
    my $found = 0;

    #   user
    my $user;

    #   users
    my $users = $user_util->fetch_users(
        {   '-email_address'  => $email_address,
            '-user_status_id' => USER_STATUS_ACTIVE,
        } );
    if ( defined $users && scalar @{$users} == 1 ) {
        $self->debug( { '-text' => 'Found -user ' . $self->dbg_value( $users->[0] ) . ' with -email_address ' . $email_address } );

        #   user
        $user = $users->[0];

        #   found
        $found = 1;
    }

    if ( !$found ) {
        my $contacts = $contact_util->fetch( { '-email_address' => $email_address, } );
        if ( defined $contacts && scalar @{$contacts} == 1 ) {
            $self->debug( { '-text' => 'Found -contact ' . $contacts->[0] . ' with -email_address ' . $email_address } );

            #   user
            $user = $contact_util->fetch_user_for_contact(
                {   '-contact_id'     => $contacts->[0]->{'-contact_id'},
                    '-user_status_id' => USER_STATUS_ACTIVE,
                } );

            #   found
            $found = 1;
        }
    }

    if ( !$found ) {
        $self->debug( { '-text' => 'Could not find user account or contact with -email_address ' . $self->dbg_value($email_address) } );
        return COMP_ACTION_FAILURE;
    }

    if ( !defined $user ) {
        $self->debug( { '-text' => 'Failed to find user, cannot send password reset request' } );
        return COMP_ACTION_FAILURE;
    }

    #   user id
    my $user_id = $user->{'-user_id'};

    $self->debug( { '-text' => 'Sending reset password request for -user_id ' . $self->dbg_value($user_id) } );

    if (!$request_util->add_request(
            {   '-request_type_id' => REQUEST_TYPE_USER_PASSWORD_RESET,
                '-user_id'         => $user_id,
                '-email_address'   => $email_address,
                '-approve_url'     => PROGRAM_SITE_PASSWORD_RESET_REQUEST_APPROVE,
                '-deny_url'        => PROGRAM_SITE_PASSWORD_RESET_REQUEST_DENY,
            } )
        ) {
        $self->error( { '-text' => 'Failed to add request for reset password -user_id ' . $self->dbg_value($user_id) } );
        return COMP_ACTION_FAILURE;
    }

    $self->debug( { '-text' => 'Added request for password reset -user_id ' . $self->dbg_value($user_id) } );

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

1;

