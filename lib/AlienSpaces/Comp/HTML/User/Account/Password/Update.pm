package AlienSpaces::Comp::HTML::User::Account::Password::Update;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :contact-type :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 74_100,

        '-fields'          => [
            {   '-name'           => 'current_password',
                '-clear'          => 1,
                '-label_phrase_id'     => 74_110,
                '-input_type'     => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'      => FORM_DATA_TYPE_TEXT,
                '-validate_rules' => {
                    '-required'   => '1',    #   required
                    '-min_length' => '6',    #   min length
                },
            },
            {   '-name'           => 'password',
                '-clear'          => 1,
                '-label_phrase_id'     => 74_120,
                '-input_type'     => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'      => FORM_DATA_TYPE_TEXT,
                '-validate_rules' => {
                    '-required'   => '1',    #   required
                    '-min_length' => '6',    #   min length
                },
            },
            {   '-name'            => 'password_confirm',
                '-clear'           => 1,
                '-label_phrase_id'      => 74_130,
                '-input_type'      => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_method' => 'validate_password_confirm',
                '-validate_rules'  => {
                    '-required'   => 1,      #   required
                    '-min_length' => 6,      #   min length
                },
                '-validate_text_method' => 'validate_password_text_method',
            },
        ],
        
        #   auto complete
        '-auto_complete' => 0,               #   turn off auto complete, is on by default

        #   submit button
        '-submit_label_text' => 'Change Password',

        '-show_reset' => 0,

        #   submit failed text
        '-submit_failed_text' => 'A problem was encountered while updating your password',

        #   submit success text
        '-submit_success_text' => 'Your password has been updated',

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, 'Return to account' );

    return $content;
}

sub validate_password_confirm {
    my ( $self, $args ) = @_;

    my $data  = $args->{'-data'};
    my $field = $args->{'-field'};

    my $password         = $data->{'password'};
    my $password_confirm = $data->{'password_confirm'};

    if ( !defined $password_confirm || $password_confirm eq '' ) {
        $self->debug( { '-text' => 'Failed required' } );
        $field->{'-validation_error'} = VALIDATION_ERROR_REQUIRED;
        return;
    }

    if ( !defined $password_confirm || $password_confirm ne $password ) {
        $self->debug( { '-text' => 'Failed match' } );
        return;
    }

    $self->debug( { '-text' => 'Passed validation' } );

    return 1;
}

sub validate_password_text_method {
    my ( $self, $args ) = @_;

    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $field = $args->{'-field'};

    if ( defined $field->{'-validation_error'} && $field->{'-validation_error'} == VALIDATION_ERROR_REQUIRED ) {
        return $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_REQUIRED_PHRASE() } );
    }

    return 'Your passwords do not match';
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   data
    my $data = $args->{'-data'};

    if ( !$self->update_password( { '-data' => $data } ) ) {
        $self->error( { '-text' => 'Failed to update user password' } );
        return COMP_ACTION_FAILURE;
    }

    return COMP_ACTION_SUCCESS;
}

#   update password
sub update_password {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    my $password         = $data->{'password'};
    my $password_confirm = $data->{'password_confirm'};

    #   html strip
    $password         = $self->html_strip($password);
    $password_confirm = $self->html_strip($password_confirm);

    if ( !defined $password || !defined $password_confirm || ( $password ne $password_confirm ) ) {
        $self->error(
            { '-text' => 'Missing -password ' . $self->dbg_value($password) . ' or -password_confirm ' . $self->dbg_value($password_confirm) . ' or they dont match, cannot update user password' } );
        return;
    }

    $self->debug( { '-text' => 'Updating user password' } );

    #   update
    if (!$user_util->update_user_password(
            {   '-user_id'  => $user_util->user_id,
                '-password' => $password,
            } )
        ) {
        $self->error( { '-text' => 'Failed to update user password' } );
        return;
    }

    $self->debug( { '-text' => 'Password updated' } );

    return 1;
} ## end sub update_password

1;

