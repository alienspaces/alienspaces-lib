package AlienSpaces::Comp::HTML::User::Account::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :table-style :form :user :js-confirm :message-type);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),

        UTIL_DB_RECORD_R_COUNTRY(),
        UTIL_DB_RECORD_R_USER_CONTACT_TYPE(),

        UTIL_OUTPUT_HTML_JS_CONFIRM(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   js confirm util
    my $js_confirm_util = $self->utils->{ UTIL_OUTPUT_HTML_JS_CONFIRM() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   cgi
    my $cgi = $self->cgi;

    $self->debug( { '-text' => 'Rendering' } );

    #   contact types
    my $user_contact_types = $contact_util->fetch_user_contact_types( { '-active' => 1 } );
    if ( !defined $user_contact_types ) {
        $self->debug( { '-text' => 'No user contact types active, not rendering' } );
        return;
    }

    #   add link
    my $add_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-label_text' => $phrase_util->text( { '-phrase_id' => 70_090 } ),    # phrase_id 70090 - Add
                    '-program' => PROGRAM_SITE_USER_ACCOUNT_CONTACT_ADD(),
                }
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    #   content
    my $content = $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => 70_010 } ),    # phrase_id 70010 - Contact
            '-show_content'         => 0,
            '-show_footer'          => 0,
            '-extra_header_content' => $add_link,
        } );

    #   user contacts
    my $user_contacts = $contact_util->fetch_user_contacts( { '-user_id' => $user_util->user_id, } );
    if ( !defined $user_contacts || !scalar @{$user_contacts} ) {
        $self->debug( { '-text' => 'Did not fetch any user contacts' } );
        return $content;
    }

    foreach my $user_contact ( @{$user_contacts} ) {

        my $data = $self->get_table_data( { '-user_contact' => $user_contact } );
        if ( !defined $data ) {
            $self->error( { '-text' => 'Failed to get table data' } );
            next;
        }

        #   links
        my $links = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 70_000 } ),    # phrase_id 70000 - Edit
                        '-program'    => PROGRAM_SITE_USER_ACCOUNT_CONTACT_UPDATE(),
                        '-component'  => COMP_HTML_USER_ACCOUNT_CONTACT_UPDATE(),
                        '-action'     => FORM_ACTION_POPULATE,
                        '-user_contact_id' => $user_contact->{'-user_contact_id'},
                    },
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 70_100 } ),    # phrase_id 70100 - Delete
                        '-program'    => PROGRAM_SITE_USER_ACCOUNT(),
                        '-component'  => COMP_HTML_USER_ACCOUNT_CONTACT(),
                        '-action'     => 'delete',
                        '-user_contact_id' => $user_contact->{'-user_contact_id'},
                        '-data'            => { $js_confirm_util->render( { '-type' => JS_CONFIRM_TYPE_DELETE() } ) },
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        $content .= $self->table->render(
            {   '-name'                 => $self->name . $user_contact->{'-user_contact_id'},
                '-class'                => TABLE_CLASS_VERTICAL,
                '-show_header'          => 0,
                '-extra_footer_content' => $links,
                '-data'                 => $data,
            } );
    } ## end foreach my $user_contact ( ...)

    return $content;
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_contact'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user contact
    my $user_contact = $args->{'-user_contact'};

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   db country util
    my $db_country_util = $self->utils->{ UTIL_DB_RECORD_R_COUNTRY() };

    #   db user contact type util
    my $db_user_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_USER_CONTACT_TYPE() };

    #   cgi
    my $cgi = $self->cgi;

    #   country
    my $country = $db_country_util->fetch_one( { '-country_id' => $user_contact->{'-country_id'} } );

    #   country text
    my $country_text;
    if ( defined $country ) {
        $country_text = $phrase_util->text( { '-phrase_id' => $country->{'-phrase_id'} } );
    }

    #   user contact type
    my $user_contact_type = $db_user_contact_type_util->fetch_one( { '-user_contact_type_id' => $user_contact->{'-user_contact_type_id'} } );

    #   user contact type text
    my $user_contact_type_text;
    if ( defined $user_contact_type ) {
        $user_contact_type_text = $phrase_util->text( { '-phrase_id' => $user_contact_type->{'-phrase_id'} } );
    }

    my $return_data = [];

    push @{$return_data}, [ $cgi->div( { '-class' => 'HeadingMedium' }, $user_contact_type_text ), undef, ];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 70_020 } ), $user_contact->{'-name'}, ];    # phrase_id 70020 - Attention

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 70_030 } ), $user_contact->{'-address_line_one'}, ];    # phrase_id 70030 - Address Line One

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 70_040 } ), $user_contact->{'-address_line_two'}, ];    # phrase_id 70040 - Address Line Two

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 70_050 } ), $country_text, ];                           # phrase_id 70050 - Country

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 70_060 } ), $user_contact->{'-state_province'}, ];      # phrase_id 70060 - State / Province

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 70_070 } ), $user_contact->{'-postal_code'}, ];         # phrase_id 70070 - Postal Code

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 70_080 } ), $user_contact->{'-phone'}, ];               # phrase_id 70080 - Phone

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $action = $args->{'-action'};

    my $data = $args->{'-data'};

    #   user contact id
    my $user_contact_id = $self->html_strip( $data->{'user_contact_id'} );

    if ( $action eq 'delete' ) {

        if ( !$contact_util->delete_user_contact( { '-user_contact_id' => $user_contact_id, '-user_id' => $user_util->user_id } ) ) {
            $self->error( { '-text' => 'Failed to delete -user_contact_id ' . $self->dbg_value($user_contact_id) } );
            return COMP_ACTION_FAILURE;
        }

        $self->debug( { '-text' => 'Deleted -user_contact_id ' . $self->dbg_value($user_contact_id) } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 70_110 } ),    # phrase_id 70110 - Contact has been deleted
                '-type' => MESSAGE_TYPE_SUCCESS,
            } );

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

__END__

