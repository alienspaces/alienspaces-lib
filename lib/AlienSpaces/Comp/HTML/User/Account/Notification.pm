package AlienSpaces::Comp::HTML::User::Account::Notification;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   HTML component template class
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :form :notification-methods :notification-statuses);

our @DYNISA = qw(Comp::HTML::Form::Tabular);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),

        UTIL_DB_RECORD_USER_NOTIFICATION_CONFIG(),
        UTIL_DB_RECORD_R_NOTIFICATION(),
        UTIL_DB_RECORD_R_NOTIFICATION_INTERVAL(),
    ];
}

#   form config
my $form_config = {

    #   fields
    '-fields' => [
        {   '-name'       => 'notification_id',
            '-input_type' => FORM_INPUT_TYPE_HIDDEN,
            '-clear'      => 1,
        },
        {   '-name'       => 'phrase_text',
            '-label_text' => 'Type',
            '-input_type' => FORM_INPUT_TYPE_TEXT,
            '-clear'      => 1,
        },
        {   '-name'              => 'notification_interval_id',
            '-label_text'        => 'Send Email',
            '-input_type'        => FORM_INPUT_TYPE_LIST,
            '-values_table_name' => 'as_r_notification_interval',
            '-validate_rules'    => { '-required' => '1', },
            '-clear'             => 1,
        },
    ],

    #   columns
    '-columns' => '2',

    #   header
    '-header_phrase_id' => 76_000,    #   phrase text: Notification Settings

    #   submit failed phrase
    '-submit_failed_text' => 'There was a problem updating your notification settings',

    #   submit success phrase
    '-submit_success_text' => 'Your notification settings have been updated',

    '-display_after_submit'         => '1',
    '-display_after_submit_success' => '1',
    '-display_after_submit_failure' => '1',
};

#   form config
#   - method to return config, do not forget to add this method
sub form_config {
    my ( $self, $args ) = @_;

    return $form_config;
}

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    if ( !defined $self->{'-form_data'} ) {
        $self->{'-form_data'} = $self->fetch_form_data();
    }

    #   row
    my $row = $args->{'-row'};

    #   row data
    my $row_data = {};
    if ( defined $self->{'-form_data'} && scalar @{ $self->{'-form_data'} } ) {
        $row_data = $self->{'-form_data'}->[$row];
    }

    $self->debug( { '-text' => 'Returning -row_data ' . $self->dbg_value($row_data) . ' for -row ' . $self->dbg_value($row) } );

    return $row_data;
}

#   form row count
sub form_row_count {
    my ( $self, $args ) = @_;

    my $row_count = ( defined $self->{'-form_data'} ? scalar @{ $self->{'-form_data'} } : 0 );

    $self->debug( { '-text' => 'Returning -row_count ' . $self->dbg_value($row_count) } );

    return $row_count;
}

#   fetch form data
#   - return arrayref of form row data
sub fetch_form_data {
    my ( $self, $args ) = @_;

    my $notification_db_rec_util             = $self->utils->{ UTIL_DB_RECORD_R_NOTIFICATION() };
    my $user_notification_config_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER_NOTIFICATION_CONFIG() };
    my $user_util                            = $self->utils->{ UTIL_USER() };
    my $phrase_util                          = $self->utils->{ UTIL_PHRASE() };

    my $notifications = $notification_db_rec_util->fetch();
    my $user_notification_configs = $user_notification_config_db_rec_util->fetch( { '-user_id' => $user_util->user_id } );

    my $form_data;

NOTIFICATIONS:
    foreach my $notification ( @{$notifications} ) {

        #   user notification configs
        #   - look for user speific config
        if ( defined $user_notification_configs && scalar @{$user_notification_configs} ) {

            foreach my $user_notification_config ( @{$user_notification_configs} ) {
                if ( $user_notification_config->{'-notification_id'} == $notification->{'-notification_id'} ) {
                    push @{$form_data},
                        {
                        '-notification_id'             => $notification->{'-notification_id'},
                        '-phrase_id'                   => $notification->{'-phrase_id'},
                        '-phrase_text'                 => $phrase_util->text( { '-phrase_id' => $notification->{'-phrase_id'} } ),
                        '-description'                 => $notification->{'-description'},
                        '-notification_interval_id'    => $user_notification_config->{'-notification_interval_id'},
                        '-notification_method_id'      => $user_notification_config->{'-notification_method_id'},
                        '-notification_status_id'      => $user_notification_config->{'-notification_status_id'},
                        '-user_notification_config_id' => $user_notification_config->{'-user_notification_config_id'},
                        };
                    next NOTIFICATIONS;
                }
            }
        }

        #   no user specific config
        push @{$form_data},
            {
            '-notification_id'          => $notification->{'-notification_id'},
            '-phrase_id'                => $notification->{'-phrase_id'},
            '-phrase_text'              => $phrase_util->text( { '-phrase_id' => $notification->{'-phrase_id'} } ),
            '-description'              => $notification->{'-description'},
            '-notification_interval_id' => $notification->{'-notification_interval_id'},
            '-notification_method_id'   => $notification->{'-notification_method_id'},
            '-notification_status_id'   => $notification->{'-notification_status_id'},
            };
    } ## end NOTIFICATIONS: foreach my $notification ( ...)

    $self->debug( { '-text' => 'Have -form_data ' . $self->dbg_value($form_data) } );

    return $form_data;
} ## end sub fetch_form_data

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    my $notification_db_rec_util             = $self->utils->{ UTIL_DB_RECORD_R_NOTIFICATION() };
    my $user_notification_config_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER_NOTIFICATION_CONFIG() };
    my $user_util                            = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    #   notification id
    my $notification_id = $data->{'notification_id'};

    #   notification interval id
    my $notification_interval_id = $data->{'notification_interval_id'};

    my $user_notification_config = $user_notification_config_db_rec_util->fetch_one(
        {   '-user_id'         => $user_util->user_id,
            '-notification_id' => $notification_id,
        } );

    $self->debug( { '-text' => 'Have record -user_notification_config ' . $self->dbg_value($user_notification_config) } );

    #   update
    if ( defined $user_notification_config ) {
        if (!$user_notification_config_db_rec_util->update(
                {   '-user_id'                     => $user_util->user_id,
                    '-notification_id'             => $notification_id,
                    '-notification_interval_id'    => $notification_interval_id,
                    '-user_notification_config_id' => $user_notification_config->{'-user_notification_config_id'},
                } )
            ) {
            $self->error( { '-text' => 'Failed to update user notification record -notification_id ' . $self->dbg_value($notification_id) } );
            return COMP_ACTION_FAILURE;
        }

        $self->debug( { '-text' => 'Updated user notification record -notification_id ' . $self->dbg_value($notification_id) } );
        return COMP_ACTION_SUCCESS;
    }

    #   insert
    my $user_notification_id = $user_notification_config_db_rec_util->insert(
        {   '-user_id'                  => $user_util->user_id,
            '-notification_id'          => $notification_id,
            '-notification_interval_id' => $notification_interval_id,
            '-notification_method_id'   => NOTIFICATION_METHOD_EMAIL,
            '-notification_status_id'   => NOTIFICATION_STATUS_ENABLED,
        } );
    if ( !defined $user_notification_id ) {
        $self->error( { '-text' => 'Failed to insert user notification record -notification_id ' . $self->dbg_value($notification_id) } );
        return COMP_ACTION_FAILURE;
    }

    $self->debug( { '-text' => 'Inserted user notification record -notification_id ' . $self->dbg_value($notification_id) } );

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

1;

