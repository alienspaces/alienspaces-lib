package AlienSpaces::Comp::HTML::User::Register;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :error-codes-user :contact-type);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

use constant { REQUIRES_CONFIRMATION => 1, };

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_REQUEST(),
        UTIL_CONTACT(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 50_100,

        '-fields' => [
            {   '-name'            => 'handle',
                '-label_phrase_id' => 50_110,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required'   => '1',    #   required
                    '-min_length' => '3',    #   min length
                },
            },
            {   '-name'            => 'email_address',
                '-label_phrase_id' => 50_120,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => '1',         #   required
                    '-min_length' => '3',         #   min length
                    '-do_match'   => '.+\@.+',    #   must not match this regular expression
                },
            },
            {   '-name'            => 'password',
                '-clear'           => 1,
                '-label_phrase_id' => 50_130,
                '-input_type'      => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => 1,           #   required
                    '-min_length' => 6,           #   min length
                },
            },
            {   '-name'            => 'confirm_password',
                '-clear'           => 1,
                '-label_phrase_id' => 50_140,
                '-input_type'      => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_method' => 'validate_confirm_password',
                '-validate_rules'  => {
                    '-required'   => 1,           #   required
                    '-min_length' => 6,           #   min length
                },
                '-validate_text_method' => 'validate_confirm_password_text_method',
            },

            #   optional possible fields / override this component with new configuration
            #            {   '-name'            => 'first_name',
            #                '-label_phrase_id' => 20_115,
            #                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
            #                '-data_type'       => FORM_DATA_TYPE_TEXT,
            #                '-values_method'   => undef,
            #            },
            #            {   '-name'            => 'last_name',
            #                '-label_phrase_id' => 20_120,
            #                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
            #                '-data_type'       => FORM_DATA_TYPE_TEXT,
            #                '-values_method'   => undef,
            #            },
        ],

        #   auto complete
        '-auto_complete' => 0,    #   turn off auto complete, is on by default

        #   submit button
        '-submit_label_phrase_id' => 50_150,

        '-show_reset' => 0,

        #   submit failed phrase
        '-submit_failed_text_method' => 'submit_failed_text',

        #   submit success phrase
        '-submit_success_text_method' => 'submit_success_text',

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   display after submit success
        '-display_after_submit_success' => '0',

    };
} ## end sub form_config

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super failed init' } );
        return;
    }

    #   confirmation status
    $self->{'-confirmation_status'} = undef;

    return 1;
}

sub validate_confirm_password {
    my ( $self, $args ) = @_;

    my $data  = $args->{'-data'};
    my $field = $args->{'-field'};

    my $password         = $data->{'password'};
    my $confirm_password = $data->{'confirm_password'};

    if ( !defined $confirm_password || $confirm_password eq '' ) {
        $self->debug( { '-text' => 'Failed required' } );
        $field->{'-validation_error'} = VALIDATION_ERROR_REQUIRED;
        return;
    }

    if ( !defined $confirm_password || $confirm_password ne $password ) {
        $self->debug( { '-text' => 'Failed match' } );
        return;
    }

    $self->debug( { '-text' => 'Passed validation' } );

    return 1;
}

sub validate_confirm_password_text_method {
    my ( $self, $args ) = @_;

    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $field = $args->{'-field'};

    if ( defined $field->{'-validation_error'} && $field->{'-validation_error'} == VALIDATION_ERROR_REQUIRED ) {
        return $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_REQUIRED_PHRASE() } );
    }

    return 'Your passwords do not match';
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    my $data = $args->{'-data'};

    my $email_address = $data->{'email_address'};
    my $password      = $data->{'password'};

    my $handle     = $data->{'handle'};
    my $first_name = $data->{'first_name'};
    my $last_name  = $data->{'last_name'};

    $self->debug( { '-text' => 'Attempting to register user' } );

    #   html strip
    $handle        = $self->html_strip($handle);
    $first_name    = $self->html_strip($first_name);
    $last_name     = $self->html_strip($last_name);
    $email_address = $self->html_strip($email_address);
    $password      = $self->html_strip($password);

    my $user_id = $user_util->register_user(
        {   '-email_address' => $email_address,
            '-password'      => $password,
            '-handle'        => $handle,
            '-first_name'    => $first_name,
            '-last_name'     => $last_name,
        } );

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $self->dbg_value($user_id) } );
        return COMP_ACTION_FAILURE;
    }

    #   fetch default user configuration
    my $user_config = $user_util->fetch_user_config( { '-user_config_id' => $user->{'-user_config_id'} } );
    if ( !defined $user_config ) {
        $self->error( { '-text' => 'Failed to fetch user configuration for -user_id ' . $user_id } );
        return COMP_ACTION_FAILURE;
    }

    #   does registration require email confirmation
    if ( $user_config->{'-registration_confirmation_enabled'} ) {

        if (!$request_util->add_request(
                {   '-request_type_id' => REQUEST_TYPE_USER_REGISTER,
                    '-user_id'         => $user_id,
                    '-request_data'    => [
                        {   '-table_name'  => 'as_user',
                            '-column_name' => 'user_status_id',
                            '-pk_value'    => $user_id,
                            '-new_value'   => USER_STATUS_ACTIVE,
                        } ] } )
            ) {
            $self->error( { '-text' => 'Failed add request for new registered user' } );
            return COMP_ACTION_FAILURE;
        }

        #   confirmation status
        $self->{'-confirmation_status'} = REQUIRES_CONFIRMATION;

        return COMP_ACTION_SUCCESS;
    }

    #   if registration does not require email activation go ahead and activate them straight away
    if ($user_util->activate_user(
            {   '-user_id'      => $user_id,
                '-activate_key' => $user->{'-activate_key'} } )
        ) {
        $self->debug( { '-text' => 'User -user_id ' . $user_id . ' registered and activate' } );

        #   confirmation status
        $self->{'-confirmation_status'} = undef;

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub form_action

#   submit success text
sub submit_success_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   confirmation status
    my $confirmation_status = $self->{'-confirmation_status'};

    #   phrase args
    my $phrase_args = {};

    #   system contacts
    my $system_contacts = $contact_util->fetch_system_contacts( { '-system_contact_type_id' => CONTACT_TYPE_SYSTEM_SUPPORT_ACCOUNT } );
    if ( defined $system_contacts && scalar @{$system_contacts} == 1 ) {
        my $system_support_email = $system_contacts->[0]->{'-email_address'};
        $phrase_args->{'-system_support_email'} = $system_support_email;
    }

    #   text
    my $text;
    if ( defined $confirmation_status && $confirmation_status == REQUIRES_CONFIRMATION ) {

        #   Your registration has been submitted. Please check your email for account
        #   activation instructions. If you have problems please contact <ENV:APP_SYSTEM_SUPPORT_EMAIL>,
        #   Thankyou for registering!
        $text = $phrase_util->text( { '-phrase_id' => 50_170, '-args' => $phrase_args } );

    }
    else {

        #   Your registration was successful. You may now log in.
        $text = $phrase_util->text( { '-phrase_id' => 50_180, '-args' => $phrase_args } );
    }

    return $text;
} ## end sub submit_success_text

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   text
    my $text = '';

    #   phrase args
    my $phrase_args = {};

    #   system contacts
    my $system_contacts = $contact_util->fetch_system_contacts( { '-system_contact_type_id' => CONTACT_TYPE_SYSTEM_SUPPORT_ACCOUNT } );
    if ( defined $system_contacts && scalar @{$system_contacts} == 1 ) {
        my $system_support_email = $system_contacts->[0]->{'-email_address'};
        $phrase_args->{'-system_support_email'} = $system_support_email;
    }

    #   errors
    my $errors = $self->get_errors();
    if ( defined $errors ) {
        foreach my $error ( @{$errors} ) {
            if ( $error->{'-error_code'} == ERROR_CODE_USER_HANDLE_NOT_UNIQUE ) {

                #   An account with this handle already exists. Please try a different handle.
                $text .= $phrase_util->text( { '-phrase_id' => 50_190, '-args' => $phrase_args } );
            }
            if ( $error->{'-error_code'} == ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE ) {

                #   An account with this email address already exists. Please try a different email address.
                $text .= $phrase_util->text( { '-phrase_id' => 50_195, '-args' => $phrase_args } );
            }
        }
    }
    else {

        #   There has been a problem registering your account. Please try again shortly.
        #   If the problem persists please contact <ENV:APP_SYSTEM_SUPPORT_EMAIL>, Thankyou!
        $text .= $phrase_util->text( { '-phrase_id' => 50_160, '-args' => $phrase_args } );
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
} ## end sub submit_failed_text

1;

