package AlienSpaces::Comp::HTML::User::Login;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :form :comp :program);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_BUSINESS(),
    ];

    return $utility_list;
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {
        '-fields' => [
            {   '-name'            => 'email_address',
                '-label_phrase_id' => 50_010,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required'   => '1',         #   required
                    '-min_length' => '3',         #   min length
                    '-do_match'   => '.+\@.+',    #   must not match this regular expression
                },
            },
            {   '-name'            => 'password',
                '-clear'           => 1,
                '-label_phrase_id' => 50_020,
                '-input_type'      => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required'   => '1',         #   required
                    '-min_length' => '6',         #   min length
                },
            },
        ],

        #   header text
        '-header_phrase_id' => 50_000,

        #   submit button
        '-submit_label_phrase_id' => 50_030,
        
        #   allow value logging
        '-allow_value_logging' => 0,

        #   submit failed text
        '-submit_failed_text' => 'Email address and password combination not recognized',

        #   show reset
        '-show_reset' => 0,

        '-display_after_submit_failure' => '1',
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_ACCOUNT_PASSWORD_RESET } );

    my $content = $cgi->a( { '-href' => $href }, 'Forgot your account information?' );

    return $content;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    my $email_address = $self->html_strip( $data->{'email_address'} );
    my $password      = $self->html_strip( $data->{'password'} );

    $self->debug( { '-text' => 'Attempting to login user' } );

    #   login
    my $user_id = $user_util->login_user(
        {   '-email_address' => $email_address,
            '-password'      => $password,
        } );
    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'Failed to login user' } );
        return COMP_ACTION_FAILURE;
    }

    #   set session values
    if ( !$self->set_session_values( { '-user_id' => $user_id } ) ) {
        $self->debug( { '-text' => 'Failed to set user session values' } );
        return COMP_ACTION_FAILURE;
    }

    return COMP_ACTION_AUTHORIZE;
} ## end sub form_action

#   set session values
sub set_session_values {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   user id
    my $user_id = $args->{'-user_id'};

    if ( !$user_util->set_user_session_values( { '-user_id' => $user_id } ) ) {
        $self->error( { '-text' => 'Failed to set user session values' } );
        return;
    }

    #   business user
    my $business_user = $user_util->fetch_one_business_user( { '-user_id' => $user_id } );
    if ( defined $business_user ) {
    
        #   set business session values
        if ( !$business_util->set_business_session_values( { '-business_id' => $business_user->{'-business_id'} } ) ) {
            $self->error( { '-text' => 'Failed to set business session values' } );
        }
    }

    return 1;
} ## end sub set_session_values

1;

