package AlienSpaces::Comp::HTML::User::Logout;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :comp);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub action {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    my $action = delete $args->{'-action'};

    if ( defined $action ) {
        if ( $action eq 'logout' ) {
            my $logged_in = $session_util->get_session( { '-key' => '-logged_in' } );
            my $user_id   = $session_util->get_session( { '-key' => '-user_id' } );

            $self->debug(
                { '-text' => 'User -user_id ' . $self->dbg_value($user_id) . ' -logged_in ' . $self->dbg_value($logged_in) } );

            if ($logged_in) {
                $user_util->logout_user( { '-user_id' => $user_id, } );
                return COMP_ACTION_AUTHORIZE;
            }
        }
    }

    return COMP_ACTION_FAILURE;
}

1;

