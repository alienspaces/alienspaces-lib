package AlienSpaces::Comp::HTML::Form::DB;

#
#   $Revision: 184 $
#   $Author: bwwallin $
#   $Date: 2010-08-20 17:39:03 +1000 (Fri, 20 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(croak carp);

use AlienSpaces::Constants qw(:util :form :comp);

our @DYNISA = qw(Comp::HTML::Form);

our $VERSION = (qw$Revision: $)[1];

#   utils
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ @{ $self->SUPER::util_list }, 
        UTIL_CGI(), 
        UTIL_PHRASE(), 
        UTIL_DB_TABLE(), 
    ];

    return $utility_list;
}

1;
