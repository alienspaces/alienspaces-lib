package AlienSpaces::Comp::HTML::Form::Basic;

#
#   $Revision: 184 $
#   $Author: bwwallin $
#   $Date: 2010-08-20 17:39:03 +1000 (Fri, 20 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(croak carp);

use AlienSpaces::Constants qw(:util :form :comp :message-types :reference);

our @DYNISA = qw(Comp::HTML::Form);

our $VERSION = (qw$Revision: $)[1];

#   render form
sub render_form {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   config
    my $config = $self->get_form_config();

    #   populate method
    my $populate_method = $config->{'-populate_method'};

    #   form args
    my $form_args = {};

    #   auto complete
    if ( defined $config->{'-auto_complete'} && !$config->{'-auto_complete'} ) {
        $form_args->{'-autocomplete'} = 'off';
    }

    #   submit url
    if ( defined $config->{'-submit_url'} ) {
        if ( $config->{'-submit_url'} !~ /^\/html\//sxmg ) {
            $form_args->{'-action'} = '/html/' . $config->{'-submit_url'};
        }
        else {
            $form_args->{'-action'} = $config->{'-submit_url'};
        }
    }

    #<<<
    my $content = $cgi->start_form($form_args) . $cgi->start_table( { '-class' => 'FormBasic' } );

    #>>>

    #   form header
    $content .= $self->render_form_header($args);

    #   field values
    my $field_values;
    my $success = eval {
        $field_values = $self->$populate_method($args);
        1;
    };
    if ( !$success ) {
        $self->error(
            {   '-text' => 'Failed to populate field values from -populate_method ' .
                    $self->dbg_value($populate_method) . ' :' . $EVAL_ERROR
            } );
    }
    $args->{'-field_values'} = $field_values;

    #   form fields
    $content .= $self->render_form_fields($args);

    #   form footer
    $content .= $self->render_form_footer($args);

    #<<<
    $content .= $cgi->end_table() . $cgi->end_form();

    #>>>

    return $content;
} ## end sub render_form

#   render form fields
sub render_form_fields {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    #   allow value logging
    my $allow_value_logging = $config->{'-allow_value_logging'};

    #   valid
    my $valid = ( defined $config->{'-validation_result'} ? $config->{'-validation_result'} : 1 );

    my $content = '';

    #   fields
    my $fields = $config->{'-fields'};

    if ( !defined $fields || !scalar @{$fields} ) {
        $self->error( { '-text' => 'No form fields defined, not rendering fields' } );
        return $content;
    }

    #   field values
    my $field_values = $args->{'-field_values'};

    #   tbody
    $content .= $cgi->start_tbody();

RENDER_FIELDS:
    foreach my $field_config ( @{$fields} ) {

        $self->debug( { '-text' => 'Have field -config ' . $self->dbg_value($field_config) } );

        #   name
        my $name = $field_config->{'-name'};
        if ( !defined $name ) {
            $self->error( { '-text' => 'Field name not defined, cannot render field' } );
            next RENDER_FIELDS;
        }

        #   input type
        my $input_type = $field_config->{'-input_type'};
        if ( !defined $input_type ) {
            $self->error( { '-text' => 'Field input type not defined, cannot render field' } );
            next RENDER_FIELDS;
        }

        #   data type
        my $data_type = $field_config->{'-data_type'};

        #   label
        my $label_phrase_id = $field_config->{'-label_phrase_id'};
        my $label_text      = $field_config->{'-label_text'};

        #   label args
        my $label_args = { '-for' => 'field-' . $name };
        if ( defined $label_phrase_id ) {
            $label_text = $phrase_util->text( { '-phrase_id' => $label_phrase_id, } );
        }
        if ( !defined $label_text ) {
            $label_text = ':' . $name . ':';
        }

        #   value
        my $value;

        #   clear
        my $clear;
        if ($valid) {
            $clear = $field_config->{'-clear'};
        }
        else {
            $clear = 0;
        }

        #   clear value
        if ($clear) {
            $self->debug( { '-text' => 'Clearing field ' . $name } );
            $cgi->param( '-name' => $name, '-value' => '' );
        }

        #   populate value
        elsif ( defined $field_values && ref($field_values) eq 'HASH' ) {
            $value = $field_values->{ '-' . $name };
            if ( !defined $value ) {
                $value = $field_values->{$name};
            }
            if ( defined $value ) {
                $self->debug(
                    {   '-text' => 'Assigning -value ' .
                            ( $allow_value_logging ? $self->dbg_value($value) : '*****' ) . ' to field ' . $name
                    } );
                $cgi->param( '-name' => $name, '-value' => $value );
            }
        }

        #   field
        my $field;

        #   field class
        my $field_class;

        if ( $input_type == FORM_INPUT_TYPE_TEXTFIELD ) {

            $field = $self->_render_textfield(
                {   '-name'  => $name,
                    '-id'    => 'field-' . $name,
                    '-value' => $value,
                } );

            $field_class = 'TextField';
        }
        elsif ( $input_type == FORM_INPUT_TYPE_HIDDEN ) {

            $field = $self->_render_hidden(
                {   '-name'  => $name,
                    '-id'    => 'field-' . $name,
                    '-value' => $value,
                } );

            $field_class = 'Hidden';
        }
        elsif ( $input_type == FORM_INPUT_TYPE_TEXTAREA ) {

            $field = $self->_render_textarea(
                {   '-name'  => $name,
                    '-id'    => 'field-' . $name,
                    '-value' => $value,
                } );

            $field_class = 'TextArea';
        }
        elsif ( $input_type == FORM_INPUT_TYPE_PASSWORD ) {

            $field = $self->_render_password(
                {   '-name'  => $name,
                    '-id'    => 'field-' . $name,
                    '-value' => $value,
                } );

            $field_class = 'Password';
        }
        elsif ( $input_type == FORM_INPUT_TYPE_LIST ) {

            my $values_table_name = $field_config->{'-values_table_name'};
            my $values_method     = $field_config->{'-values_method'};

            $field = $self->_render_list(
                {   '-name'              => $name,
                    '-id'                => 'field-' . $name,
                    '-value'             => $value,
                    '-values_table_name' => $values_table_name,
                    '-values_method'     => $values_method,
                } );

            $field_class = 'List';

        }
        elsif ( $input_type == FORM_INPUT_TYPE_CHECKBOX ) {

            $field = $self->_render_checkbox(
                {   '-name'  => $name,
                    '-id'    => 'field-' . $name,
                    '-value' => $value,
                } );

            $field_class = 'CheckBox';
        }
        elsif ( $input_type == FORM_INPUT_TYPE_TEXT ) {

            my $values_table_name = $field_config->{'-values_table_name'};
            my $values_method     = $field_config->{'-values_method'};

            $field = $self->_render_text(
                {   '-name'              => $name,
                    '-id'                => 'field-' . $name,
                    '-value'             => $value,
                    '-values_table_name' => $values_table_name,
                    '-values_method'     => $values_method,
                } );

            $field_class = 'Text';
        }

        #   validation
        my $validation_text;
        if ( defined $field_config->{'-validation_result'} && !$field_config->{'-validation_result'} ) {
            $validation_text = $field_config->{'-validation_message'};
        }

        #   hidden field
        if ( defined $field && $input_type == FORM_INPUT_TYPE_HIDDEN ) {
            $content .= $cgi->Tr( {}, $cgi->td( { '-class' => 'Hidden', '-colspan' => 2 }, ( defined $field ? $field : '' ) ) );

        }

        #   enterable field
        else {
            #<<<
            $content .= $cgi->Tr(
                {},
                $cgi->td(
                    { '-colspan' => 2 },
                    $cgi->div(
                        { '-class' => 'Widget' },
                        $cgi->div( { '-class' => 'Message' }, ( defined $validation_text ? $validation_text : ' &nbsp;' ) ) .
                            $cgi->div( { '-class' => 'Label' },
                            ( defined $label_text ? $cgi->label( $label_args, $label_text ) : ' &nbsp; ' ) ) .
                            $cgi->div(
                            { '-class' => 'Field' },
                            $cgi->div( { '-class' => $field_class }, ( defined $field ? $field : ' &nbsp; ' ) ) ) ) ) );

            #>>>

        }
    } ## end RENDER_FIELDS: foreach my $field_config ( ...)

    $content .= $cgi->end_tbody();

    return $content;
} ## end sub render_form_fields

sub _render_textfield {
    my ( $self, $args ) = @_;

    my $name  = $args->{'-name'};
    my $id    = $args->{'-id'};
    my $value = $args->{'-value'};

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $field = $cgi->textfield(
        {   '-name'  => $name,
            '-id'    => $id,
            '-value' => $value
        } );

    return $field;
}

sub _render_text {
    my ( $self, $args ) = @_;

    my $name              = $args->{'-name'};
    my $id                = $args->{'-id'};
    my $value             = $args->{'-value'};
    my $values_table_name = $args->{'-values_table_name'};
    my $values_method     = $args->{'-values_mthod'};

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util db table
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    my $field;

    #   values table name
    if ( defined $values_table_name ) {
        my ( $values, $labels ) = $db_table_util->fetch_reference_table_data(
            {   '-table_name' => $values_table_name,
                '-max_rows'   => DEFAULT_MAX_REFERENCE_DATA_ROWS,
            } );

        $field = $labels->{$value};
    }

    #   values method
    elsif ( defined $values_method ) {
        my ( $values, $labels ) = $self->$values_method( { '-name' => $name } );

        $field = $labels->{$value};
    }

    #   value
    else {
        $field = $value;
    }

    return $field;
} ## end sub _render_text

sub _render_checkbox {
    my ( $self, $args ) = @_;

    my $name  = $args->{'-name'};
    my $id    = $args->{'-id'};
    my $value = $args->{'-value'};

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $field = $cgi->checkbox(
        {   '-name'  => $name,
            '-id'    => $id,
            '-value' => $value
        } );

    return $field;
}

sub _render_hidden {
    my ( $self, $args ) = @_;

    my $name  = $args->{'-name'};
    my $id    = $args->{'-id'};
    my $value = $args->{'-value'};

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $field = $cgi->hidden(
        {   '-name'  => $name,
            '-id'    => $id,
            '-value' => $value
        } );

    return $field;
}

sub _render_textarea {
    my ( $self, $args ) = @_;

    my $name  = $args->{'-name'};
    my $id    = $args->{'-id'};
    my $value = $args->{'-value'};

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $field = $cgi->textarea(
        {   '-name'  => $name,
            '-id'    => $id,
            '-value' => $value
        } );

    return $field;
}

sub _render_password {
    my ( $self, $args ) = @_;

    my $name  = $args->{'-name'};
    my $id    = $args->{'-id'};
    my $value = $args->{'-value'};

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $field = $cgi->password_field(
        {   '-name'  => $name,
            '-id'    => $id,
            '-value' => $value
        } );

    return $field;
}

sub _render_list {
    my ( $self, $args ) = @_;

    my $name              = $args->{'-name'};
    my $id                = $args->{'-id'};
    my $value             = $args->{'-value'};
    my $values_table_name = $args->{'-values_table_name'};
    my $values_method     = $args->{'-values_method'};

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util db table
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   list values / labels (values = arrayref, labels = hashref)
    my ( $values, $labels );
    if ( defined $values_table_name ) {
        ( $values, $labels ) = $db_table_util->fetch_reference_table_data(
            {   '-table_name' => $values_table_name,
                '-max_rows'   => DEFAULT_MAX_REFERENCE_DATA_ROWS,
            } );
    }
    else {
        ( $values, $labels ) = $self->$values_method( { '-name' => $name } );
    }

    #   add -- select one --
    #   - this should be a phrase
    if ( defined $values && scalar @{$values} ) {
        $self->debug( { '-text' => 'Have list -values ' . $self->dbg_value($values) } );
        my $found = 0;
        foreach my $value ( @{$values} ) {
            if ( $value eq '' ) {
                $found = 1;
                last;
            }
        }
        if ( !$found ) {
            $self->debug( { '-text' => 'Adding empty entry' } );
            $values = [ '', @{$values} ];
        }
    }
    else {
        $self->debug( { '-text' => 'Have empty list -values ' . $self->dbg_value($values) } );
        $values = [''];
    }

    if ( !exists $labels->{''} ) {
        $self->debug( { '-text' => 'Adding empty label' } );
        $labels->{''} = ' -- Select One -- ';
    }

    my $field = $cgi->popup_menu(
        {   '-name'   => $name,
            '-id'     => 'field-' . $name,
            '-values' => $values,
            '-labels' => $labels,
        } );

    $self->debug( { '-text' => 'Have -field ' . $self->dbg_value($field) } );

    return $field;
} ## end sub _render_list

#   action
sub action {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->get_form_config();

    my $return_status = COMP_ACTION_FAILURE;

    #   submit method
    my $submit_method = $config->{'-submit_method'};

    #   validate method
    my $validate_method = $config->{'-validate_method'};

    #   form process data
    $self->form_process_data($args);

    #   action
    my $action = $args->{'-action'};

    #   submit
    if ( defined $action && $action eq FORM_ACTION_SUBMIT ) {

        $self->debug( { '-text' => 'Form submitting' } );

        #   validate
        if ( !$self->$validate_method($args) ) {
            return COMP_ACTION_VALIDATE_FAILURE;
        }

        my $success = eval {
            $return_status = $self->$submit_method($args);
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to call submit method :' . $EVAL_ERROR } );
        }

        #   check validation
        if ( !defined $config->{'-validation_result'} || $config->{'-validation_result'} ) {

            $self->debug( { '-text' => 'Form is valid' } );

            if ( $return_status == COMP_ACTION_SUCCESS ) {

                $self->debug( { '-text' => 'Action successful' } );

                #   success message
                $self->add_success_message();

                #   redirect url
                my $redirect_url = $config->{'-redirect_after_submit_url'} || $config->{'-redirect_after_submit_success_url'};
                if ( defined $redirect_url ) {
                    $self->set_redirect_url( { '-url' => $redirect_url } );
                }
            }
            elsif ( $return_status == COMP_ACTION_FAILURE ) {

                $self->debug( { '-text' => 'Action failed' } );

                #   failure message
                $self->add_failure_message();

                #   redirect url
                my $redirect_url = $config->{'-redirect_after_submit_url'} || $config->{'-redirect_after_submit_failure_url'};
                if ( defined $redirect_url ) {
                    $self->set_redirect_url( { '-url' => $redirect_url } );
                }
            }
        } ## end if ( !defined $config->...)

    } ## end if ( defined $action &&...)

    #   populate
    elsif ( defined $action && $action eq FORM_ACTION_POPULATE ) {

        $self->debug( { '-text' => 'Form populating' } );

        $return_status = COMP_ACTION_SUCCESS;
    }

    return $return_status;
} ## end sub action

1;
