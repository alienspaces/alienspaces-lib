package AlienSpaces::Comp::Form::Basic;

#
#   $Revision: 184 $
#   $Author: bwwallin $
#   $Date: 2010-08-20 17:39:03 +1000 (Fri, 20 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(croak carp);

our $VERSION = '$Revision: 184 $';

use base qw(AlienSpaces::Comp::Form);

use AlienSpaces::Constants qw(:utils :form :comp);

use constant FORM_HEADER_COLSPAN => 3;
use constant FORM_FOOTER_COLSPAN => 3;

use constant DEFAULT_SUBMIT_METHOD   => 'form_action';
use constant DEFAULT_VALIDATE_METHOD => 'form_validate';
use constant DEFAULT_POPULATE_METHOD => 'form_populate';

use constant DEFAULT_LIST_VALUES_METHOD => 'form_list_values';

#   utils
sub util_list {
    my ( $self, $args ) = @_;

    my $utility_list = [ @{ $self->SUPER::util_list }, UTIL_CGI(), UTIL_PHRASE(), UTIL_DB_TABLE(), ];

    return $utility_list;
}

sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   fields
        '-fields' => [
            {   '-name'              => 'xxx',    #   input field name
                '-clear'             => 'xxx',    #   1|0 clear before render
                '-label_phrase_id'   => 'xxx',    #   phrase is for label
                '-input_type'        => 'xxx',    #   FORM_INPUT_TYPE_XXX
                '-data_type'         => 'xxx',    #   FORM_DATA_TYPE_XXX
                '-values_method'     => 'xxx',    #   Source list values using this method,
                                                  #   - defaults to 'form_list_values'
                                                  #   - argument -name of field is passed in
                '-values_table_name' => 'xxx',    #   Source list values from this table
            },
        ],

        #   submit button
        '-submit_label_phrase_id' => 'xxx',
        '-submit_method'          => 'xxx',       #   defaults to 'form_action'
        '-validate_method'        => 'xxx',       #   defaults to 'form_validate'
        '-populate_method'        => 'xxx',       #   defaults to 'form_populate'
        '-show_reset'             => 'xxx',

        #   title
        '-title_phrase_id' => 'xxx',

        #   submit success phrase
        '-submit_success_phrase_id' => 'xxx',     #   phrase to display if form action is successful

        #   submit failed phrase
        '-submit_failed_phrase_id' => 'xxx',      #   phrase to display if form action is not successful
    };
} ## end sub form_config

sub form_populate {
    my ( $self, $args ) = @_;

    return 1;
}

sub form_validate {
    my ( $self, $args ) = @_;

    return 1;
}

sub form_action {
    my ( $self, $args ) = @_;

    return COMP_ACTION_FAILURE;
}

sub form_list_values {
    my ( $self, $args ) = @_;

    return ( [], {} );
}

sub action {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->form_config;

    my $return_status = COMP_ACTION_FAILURE;

    #   submit method
    my $submit_method = $config->{'-submit_method'};
    if ( !defined $submit_method ) {
        $self->debug( { '-text' => 'Missing submit method, using default submit method' } );
        $submit_method = DEFAULT_SUBMIT_METHOD;
    }

    my $action = $args->{'-action'};
    if ( defined $action && $action eq 'submit-form' ) {
        my $success = eval {
            $return_status = $self->$submit_method($args);
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to call submit method :' . $EVAL_ERROR } );
        }
    }

    return $return_status;
}

sub render {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Component -args ' . $self->dbg_value($args) } );

    #   config
    my $config = $self->form_config;

    #   submit success phrase
    my $success_phrase_id = $config->{'-submit_success_phrase_id'};

    #   submit failed phrase
    my $failed_phrase_id = $config->{'-submit_failed_phrase_id'};

    #   content
    my $content;

    #   success / failure
    my $actions = $args->{'-actions'};
    if ( defined $actions ) {
        my $action = $actions->{ $self->name };
        if ( defined $action &&
            defined $action->{'-status'} ) {

            if ( $action->{'-status'} == COMP_ACTION_SUCCESS ) {
                $content = $self->render_form_success();
            }
            elsif ( $action->{'-status'} == COMP_ACTION_FAILURE ) {
                $content = $self->render_form_failure();
            }
        }
    }

    #   form
    if ( !defined $content ) {
        $content = $self->render_form($args);
    }

    return $content;
} ## end sub render

sub render_form_success {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->form_config;

    #   submit success phrase
    my $success_phrase_id = $config->{'-submit_success_phrase_id'};

    my $text;
    if ( defined $success_phrase_id ) {
        $text = $phrase_util->text( { '-phrase_id' => $success_phrase_id } );
    }
    else {
        $text = ':success:';
    }

    return $text;
}

sub render_form_failure {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->form_config;

    #   submit failed phrase
    my $failed_phrase_id = $config->{'-submit_failed_phrase_id'};

    my $text;
    if ( defined $failed_phrase_id ) {
        $text = $phrase_util->text( { '-phrase_id' => $failed_phrase_id } );
    }
    else {
        $text = ':failed:';
    }

    return $text;
}

sub render_form {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $content = $cgi_util->start_form() . $cgi_util->start_table( { '-class' => 'Form' } );

    #   form header
    $content .= $self->render_form_header();

    #   form fields
    $content .= $self->render_form_fields();

    #   form footer
    $content .= $self->render_form_footer();

    $content .= $cgi_util->end_table() . $cgi_util->end_form();

    return $content;
}

sub render_form_header {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->form_config;

    my $content = '';

    #   title phrase
    my $title_phrase_id = $config->{'-title_phrase_id'};
    my $title_text;
    if ( defined $title_phrase_id ) {
        $title_text = $phrase_util->text( { '-phrase_id' => $title_phrase_id, } );
    }
    if ( !defined $title_text ) {
        $title_text = ':title:';
    }

    $content = $cgi_util->thead( {}, $cgi_util->Tr( {}, $cgi_util->th( { '-colspan' => FORM_HEADER_COLSPAN }, $title_text, ) ) );

    return $content;
}

sub render_form_fields {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   util db table
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   config
    my $config = $self->form_config;

    my $content = '';

    #   fields
    my $fields = $config->{'-fields'};

    if ( !defined $fields || !scalar @{$fields} ) {
        $self->error( { '-text' => 'No form fields defined, not rendering fields' } );
        return $content;
    }

    $content = $cgi_util->start_tbody();

RENDER_FIELDS:
    foreach my $field ( @{$fields} ) {
        my $name              = $field->{'-name'};
        my $clear             = $field->{'-clear'};
        my $label_phrase_id   = $field->{'-label_phrase_id'};
        my $input_type        = $field->{'-input_type'};
        my $data_type         = $field->{'-data_type'};
        my $values_table_name = $field->{'-values_table_name'};
        my $values_method     = $field->{'-values_method'};
        if ( !defined $values_method ) {
            $values_method = DEFAULT_LIST_VALUES_METHOD;
        }

        if ( !defined $name ) {
            $self->error( { '-text' => 'Field name not defined, cannot render field' } );
            next RENDER_FIELDS;
        }
        if ( !defined $input_type ) {
            $self->error( { '-text' => 'Field input type not defined, cannot render field' } );
            next RENDER_FIELDS;
        }

        #   label
        my $label_text;

        #   label args
        my $label_args = { '-for' => 'field-' . $name };
        if ( defined $label_phrase_id ) {
            $label_text = $phrase_util->text( { '-phrase_id' => $label_phrase_id, } );
        }
        if ( !defined $label_text ) {
            $label_text = ':' . $name . ':';
        }

        #   clear ?
        if ($clear) {
            $cgi_util->param( '-name' => $name, '-value' => '' );
        }

        #   field
        my $field;

        #   field args
        my $field_args = { '-name' => $name, '-id' => 'field-' . $name };

        if ( $input_type == FORM_INPUT_TYPE_TEXTFIELD ) {
            if ($clear) {
                $field_args->{'-value'} = '';
            }
            $field = $cgi_util->textfield($field_args);

        }
        elsif ( $input_type == FORM_INPUT_TYPE_TEXTAREA ) {
            if ($clear) {
                $field_args->{'-value'} = '';
            }
            $field = $cgi_util->textarea($field_args);

        }
        elsif ( $input_type == FORM_INPUT_TYPE_PASSWORD ) {
            if ($clear) {
                $field_args->{'-value'} = '';
            }
            $field = $cgi_util->password_field($field_args);

        }
        elsif ( $input_type == FORM_INPUT_TYPE_LIST ) {
            if ($clear) {
                $field_args->{'-value'} = '';
            }

            #   list values / labels (values = arrayref, labels = hashref)
            my ( $values, $labels );
            if ( defined $values_table_name ) {
                ( $values, $labels ) = $db_table_util->fetch_reference_table_data( { '-table_name' => $values_table_name } );
            }
            else {
                ( $values, $labels ) = $self->$values_method( { '-name' => $name } );
            }
            $field_args->{'-values'} = $values;
            $field_args->{'-labels'} = $labels;

            $field = $cgi_util->popup_menu($field_args);
        }
        elsif ( $input_type == FORM_INPUT_TYPE_CHECKBOX ) {
            $field = '';
        }

        #   validation
        my $validation;

        #   content
        $content .= $cgi_util->Tr( {},
            $cgi_util->td( { '-class' => 'Label' }, ( defined $label_text ? $cgi_util->label( $label_args, $label_text ) : '' ) ) .
                $cgi_util->td( { '-class' => 'Field' },      ( defined $field      ? $field      : '' ) ) .
                $cgi_util->td( { '-class' => 'Validation' }, ( defined $validation ? $validation : '' ) ) );
    } ## end foreach my $field ( @{$fields...})

    $content .= $cgi_util->end_tbody();

    return $content;
} ## end sub render_form_fields

sub render_form_footer {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->form_config;

    #   submit args
    my $submit_args = {};

    #   label
    my $submit_label_phrase_id = $config->{'-submit_label_phrase_id'};
    my $label_text;
    if ( defined $submit_label_phrase_id ) {
        $label_text = $phrase_util->text( { '-phrase_id' => $submit_label_phrase_id, } );
    }
    if ( !defined $label_text ) {
        $label_text = ':submit:';
    }
    $submit_args->{'-value'} = $label_text;

    #   reset button
    my $show_reset = $config->{'-show_reset'};

    #   comp
    my $comp_args = {
        '-name'  => 'component',
        '-value' => $self->name,
    };
    $cgi_util->param( '-name' => 'component', '-value' => $self->name );

    #   action
    my $action_args = {
        '-name'  => 'action',
        '-value' => 'submit-form',
    };
    $cgi_util->param( '-name' => 'action', '-value' => 'submit-form' );

    my $content = $cgi_util->tfoot(
        {},
        $cgi_util->Tr(
            {},
            $cgi_util->td(
                { '-colspan' => FORM_FOOTER_COLSPAN },
                $cgi_util->submit($submit_args) . $cgi_util->hidden($action_args),
                $cgi_util->hidden($comp_args),
            ) ) );

    return $content;
} ## end sub render_form_footer

1;
