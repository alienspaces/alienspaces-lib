package AlienSpaces::Comp::HTML::Form::Tabular;

#
#   $Revision: 184 $
#   $Author: bwwallin $
#   $Date: 2010-08-20 17:39:03 +1000 (Fri, 20 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(croak carp);

use AlienSpaces::Constants qw(:util :form :comp :message-types :reference);

our @DYNISA = qw(Comp::HTML::Form);

our $VERSION = (qw$Revision: $)[1];

use constant {
    MIN_ROWS                      => 1,
    MAX_ROWS                      => 100,
    DEFAULT_FORM_ROW_COUNT_METHOD => 'form_row_count',
    DEFAULT_FORM_ROW_COUNT        => 1,
};

#   hidden field content
#   - cleared out at the start of rendering
my $HIDDEN_FIELD_CONTENT = [];

#   set form config
sub set_form_config {
    my ( $self, $config ) = @_;

    #   min rows
    if ( !exists $config->{'-min_rows'} ) {
        $config->{'-min_rows'} = MIN_ROWS;
    }

    #   max rows
    if ( !exists $config->{'-max_rows'} ) {
        $config->{'-max_rows'} = MAX_ROWS;
    }

    #   row count method
    if ( !exists $config->{'-row_count_method'} ) {
        $config->{'-row_count_method'} = DEFAULT_FORM_ROW_COUNT_METHOD;
    }

    return $self->SUPER::set_form_config($config);
}

#   form row count
sub form_row_count {
    my ( $self, $args ) = @_;

    return DEFAULT_FORM_ROW_COUNT;
}

#   form validate
sub form_validate {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->get_form_config();

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   data
    my $data = $args->{'-data'};

    #   valid
    my $valid = 1;

VALIDATE_FIELDS:
    foreach my $field ( @{ $config->{'-fields'} } ) {

        #   validate method
        my $validate_method = $field->{'-validate_method'};

        $self->debug( { '-text' => 'Validating -field ' . $self->dbg_value($field) . ' with -validate_method ' . $self->dbg_value($validate_method) } );

        my $field_name = $field->{'-name'};
        my $result     = $self->$validate_method(
            {   '-field' => $field,
                '-value' => $data->{$field_name},
                '-data'  => $data,
            } );

        if ( !defined $result ) { $result = 0; }

        $self->debug( { '-text' => 'Validated -field  ' . $self->dbg_value($field) . ' -result ' . $self->dbg_value($result) } );

        $field->{'-validation_result'} = $result;

        if ($result) {
            next VALIDATE_FIELDS;
        }

        #   valid
        #   - the entire form is now not valid
        $valid = 0;

        #   validate phrase id
        my $validate_phrase_id = $field->{'-validate_phrase_id'};
        if ( defined $validate_phrase_id ) {
            $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => $validate_phrase_id } );
            $self->debug( { '-text' => 'Validation message set with -validate_phrase_id ' . $validate_phrase_id } );
            next VALIDATE_FIELDS;
        }

        #   validate text
        my $validate_text = $field->{'-validate_text'};
        if ( defined $validate_text ) {
            $field->{'-validation_message'} = $validate_text;
            $self->debug( { '-text' => 'Validation message set with -validate_text ' . $validate_text } );
            next VALIDATE_FIELDS;
        }

        #   validate text method
        my $validate_text_method = $field->{'-validate_text_method'};
        if ( defined $validate_text_method ) {
            $field->{'-validation_message'} = $self->$validate_text_method(
                {   '-field' => $field,
                    '-value' => $data->{$field_name},
                    '-data'  => $data,
                } );
            $self->debug( { '-text' => 'Validation message set with -validate_text_method ' . $validate_text_method } );
            next VALIDATE_FIELDS;
        }

        my $validation_error = $field->{'-validation_error'};
        if ( defined $validation_error ) {

            if ( $validation_error == VALIDATION_ERROR_REQUIRED ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_REQUIRED_PHRASE() } );
                $self->debug( { '-text' => 'Validation error required' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_MAX_LENGTH ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_TOO_LONG_PHRASE() } );
                $self->debug( { '-text' => 'Validation error max length' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_MIN_LENGTH ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_TOO_SHORT_PHRASE() } );
                $self->debug( { '-text' => 'Validation error min length' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_DO_MATCH ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_UNRECOGNIZED_PHRASE() } );
                $self->debug( { '-text' => 'Validation error do match' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_DONT_MATCH ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_UNRECOGNIZED_PHRASE() } );
                $self->debug( { '-text' => 'Validation error dont match' } );

            }
        } ## end if ( defined $validation_error)
    } ## end VALIDATE_FIELDS: foreach my $field ( @{ $config...})

    #   global validation result
    $config->{'-validation_result'} = $valid;

    return $valid;
} ## end sub form_validate

#   render form
sub render_form {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    #   min rows
    my $min_rows = $config->{'-min_rows'};

    #   max rows
    my $max_rows = $config->{'-max_rows'};

    #   populate method
    my $populate_method = $config->{'-populate_method'};

    #   row count method
    my $row_count_method = $config->{'-row_count_method'};

    my $row_count = $self->$row_count_method;
    if ( !$row_count ) {
        $self->debug( { '-text' => 'Have -row_count ' . $self->dbg_value($row_count) . ', not displaying rows' } );

        $self->add_message( { '-type' => MESSAGE_TYPE_INFO, '-text' => $phrase_util->text( { '-phrase_id' => DEFAULT_NO_DATA_PHRASE } ) } );
        return;
    }

    #   form args
    my $form_args = {};

    #   auto complete
    if ( defined $config->{'-auto_complete'} && !$config->{'-auto_complete'} ) {
        $form_args->{'-autocomplete'} = 'off';
    }

    #   submit url
    if ( defined $config->{'-submit_url'} ) {
        $form_args->{'-action'} = '/html/' . $config->{'-submit_url'};
    }

    #   validation messages
    #   - not implemented at the moment...
    #$cgi->div( { '-class' => 'Message' }, ( defined $validation_text ? $validation_text : ' &nbsp;' ) ) .

    #   hidden field content
    $HIDDEN_FIELD_CONTENT = [];

#<<<
    my $content = 
        $cgi->start_form($form_args) .
        $cgi->start_table({'-class' => 'FormTabular'});
#>>>

    $content .=

        #   form header
        $self->render_form_header($args) .

        #   form footer
        $self->render_form_footer($args) .

        #   form body
        $cgi->start_tbody();

    #   row
    my $row = 0;

    #   render rows
    #   - rows start at row 0 to suite using row as
    #     an equivalent array index
RENDER_ROWS:
    while ( ( $row < $max_rows ) && ( $row < $row_count ) ) {

        $args->{'-row'} = $row;

        #   field values
        my $field_values;
        my $success = eval {
            $field_values = $self->$populate_method($args);
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to populate field values from -populate_method ' . $self->dbg_value($populate_method) . ' :' . $EVAL_ERROR } );

            $row++;
            next RENDER_ROWS;
        }

        if ( !defined $field_values && $row >= $min_rows ) {
            $self->debug( { '-text' => 'No field values and -min_rows ' . $self->dbg_value($min_rows) . ' exceeded' } );
            last RENDER_ROWS;
        }

        $args->{'-field_values'} = $field_values;

        #   form fields
        $content .= $self->render_form_fields($args);

        $row++;
    }

    $content .=

        #   form body
        $cgi->end_tbody();

#<<<
    $content .= 
        $cgi->end_table();
        
    if (scalar @{$HIDDEN_FIELD_CONTENT}) {        
        $content .= $cgi->div({'-class' => 'Hidden'}, (join '', @{$HIDDEN_FIELD_CONTENT}));
    }
    
    $content .= 
        $cgi->end_form();
#>>>

    return $content;
} ## end sub render_form

#   render form header
sub render_form_header {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    my $content = '';

    #   field count
    my $field_count = $config->{'-field_count'};

    #   column header
    my $column_header = '';

RENDER_FIELD_HEADER:
    foreach my $field_config ( @{ $config->{'-fields'} } ) {

        #   name
        my $name = $field_config->{'-name'};
        if ( !defined $name ) {
            $self->error( { '-text' => 'Field name not defined, cannot render field header' } );
            next RENDER_FIELD_HEADER;
        }

        #   label
        my $label_phrase_id = $field_config->{'-label_phrase_id'};
        my $label_text      = $field_config->{'-label_text'};
        if ( defined $label_phrase_id ) {
            $label_text = $phrase_util->text( { '-phrase_id' => $label_phrase_id, } );
        }
        if ( !defined $label_text ) {
            $label_text = ':' . $name . ':';
        }

        if ( $field_config->{'-input_type'} == FORM_INPUT_TYPE_HIDDEN ) {
            next RENDER_FIELD_HEADER;
        }
#<<<
        $column_header .= 
            $cgi->th({},

                #   label
                $cgi->div( { '-class' => 'Label' }, 
                    ( defined $label_text ? $cgi->label( {}, $label_text ) : ' &nbsp; ' ) 
                ) 
            );
#>>>
    } ## end RENDER_FIELD_HEADER: foreach my $field_config ( ...)

    $self->debug( { '-text' => 'Rendering -column_header ' . $self->dbg_value($column_header) } );

#<<<    
    $content =

        #   thead
        $cgi->thead( {},

            #   Tr
            $cgi->Tr( {}, $column_header ) 
        );
#>>>

    return $content;
} ## end sub render_form_header

#   render form fields
sub render_form_fields {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   util db table
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   config
    my $config = $self->get_form_config();

    #   allow value logging
    my $allow_value_logging = $config->{'-allow_value_logging'};

    #   valid
    my $valid = ( defined $config->{'-validation_result'} ? $config->{'-validation_result'} : 1 );

    my $content = '';

    #   fields
    my $fields = $config->{'-fields'};

    if ( !defined $fields || !scalar @{$fields} ) {
        $self->error( { '-text' => 'No form fields defined, not rendering fields' } );
        return $content;
    }

    #   field values
    my $field_values = $args->{'-field_values'};

    #   Tr
    $content .= $cgi->start_Tr( {} );

RENDER_FIELDS:
    foreach my $field_config ( @{$fields} ) {

        $self->debug( { '-text' => 'Have field -config ' . $self->dbg_value($field_config) } );

        #   name
        my $name = $field_config->{'-name'} . '.' . $args->{'-row'};
        if ( !defined $name ) {
            $self->error( { '-text' => 'Field name not defined, cannot render field header' } );
            next RENDER_FIELDS;
        }

        #   clear
        my $clear;
        if ($valid) {
            $clear = $field_config->{'-clear'};
        }
        else {
            $clear = 0;
        }

        #   input type
        my $input_type = $field_config->{'-input_type'};
        if ( !defined $input_type ) {
            $self->error( { '-text' => 'Field input type not defined, cannot render field' } );
            next RENDER_FIELDS;
        }

        #   data type
        my $data_type = $field_config->{'-data_type'};

        #   values table name
        my $values_table_name = $field_config->{'-values_table_name'};

        #   values method
        my $values_method = $field_config->{'-values_method'};

        #   field args
        my $field_args = { '-name' => $name, '-id' => 'field-' . $name };

        #   clear
        if ($clear) {
            $self->debug( { '-text' => 'Clearing field ' . $name } );
            $cgi->param( '-name' => $name, '-value' => '' );
            $field_args->{'-value'} = '';
        }

        #   populate value
        my $value;
        if ( defined $field_values && ref($field_values) eq 'HASH' ) {
            $value = $field_values->{ '-' . $field_config->{'-name'} };
            if ( !defined $value ) {
                $value = $field_values->{ $field_config->{'-name'} };
            }
        }

        if ( defined $value ) {
            $self->debug( { '-text' => 'Assigning -value ' . ( $allow_value_logging ? $self->dbg_value($value) : '*****' ) . ' to field ' . $name } );
            $cgi->param( '-name' => $name, '-value' => $value );
            $field_args->{'-value'} = $value;
        }

        #   field
        my $field;

        #   field class
        my $field_class;

        if ( $input_type == FORM_INPUT_TYPE_TEXTFIELD ) {
            $field       = $cgi->textfield($field_args);
            $field_class = 'TextField';
        }
        elsif ( $input_type == FORM_INPUT_TYPE_HIDDEN ) {
            $field       = $cgi->hidden($field_args);
            $field_class = 'Hidden';
        }
        elsif ( $input_type == FORM_INPUT_TYPE_TEXTAREA ) {
            $field       = $cgi->textarea($field_args);
            $field_class = 'TextArea';

        }
        elsif ( $input_type == FORM_INPUT_TYPE_PASSWORD ) {
            $field       = $cgi->password_field($field_args);
            $field_class = 'Password';

        }
        elsif ( $input_type == FORM_INPUT_TYPE_LIST ) {

            #   list values / labels (values = arrayref, labels = hashref)
            my ( $values, $labels );
            if ( defined $values_table_name ) {
                ( $values, $labels ) = $db_table_util->fetch_reference_table_data(
                    {   '-table_name' => $values_table_name,
                        '-max_rows'   => DEFAULT_MAX_REFERENCE_DATA_ROWS,
                    } );
            }
            else {
                ( $values, $labels ) = $self->$values_method( { '-name' => $field_config->{'-name'}, '-field_values' => $field_values } );
            }

            #   add -- select one --
            #   - this should be a phrase
            if ( defined $values && scalar @{$values} ) {
                $values = [ '', @{$values} ];
            }
            else {
                $values = [''];
            }
            $labels->{''} = ' -- Select One -- ';

            $field_args->{'-values'}  = $values;
            $field_args->{'-default'} = $field_args->{'-value'};
            $field_args->{'-labels'}  = $labels;
            $field_class              = 'List';

            $self->debug( { '-text' => 'Have -field_args ' . $self->dbg_value($field_args) } );

            $field       = $cgi->popup_menu($field_args);
            $field_class = 'Popmenu';

        } ## end elsif ( $input_type == FORM_INPUT_TYPE_LIST)
        elsif ( $input_type == FORM_INPUT_TYPE_CHECKBOX ) {

            $field_args->{'-label'} = '';

            if ( $field_args->{'-value'} == 1 || $field_args->{'-value'} eq 'Y' ) {
                $field_args->{'-checked'} = 1;
            }
            else {
                delete $field_args->{'-checked'};
            }

            $field       = $cgi->checkbox($field_args);
            $field_class = 'CheckBox';

        }
        elsif ( $input_type == FORM_INPUT_TYPE_TEXT ) {
            $field       = $value;
            $field_class = 'Text';

        }

        #   validation
        my $validation_text;
        if ( defined $field_config->{'-validation_result'} && !$field_config->{'-validation_result'} ) {
            $validation_text = $field_config->{'-validation_message'};
        }

        #   hidden field
        if ( defined $field && $input_type == FORM_INPUT_TYPE_HIDDEN ) {
            if ( defined $field ) {
                push @{$HIDDEN_FIELD_CONTENT}, $field;
            }
        }

        #   enterable field
        else {

            #   td
            $content .= $cgi->td(
                {},

                #   div
                $cgi->div(
                    { '-class' => 'Widget' },

                    #   field
                    $cgi->div( { '-class' => 'Field' }, $cgi->div( { '-class' => $field_class }, ( defined $field ? $field : ' &nbsp; ' ) ) )

                ) );
        }
    } ## end RENDER_FIELDS: foreach my $field_config ( ...)

    $content .= $cgi->end_Tr();

    return $content;
} ## end sub render_form_fields

#   render form footer
sub render_form_footer {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    #   submit args
    my $submit_args = {};

    #   label
    my $submit_label_phrase_id = $config->{'-submit_label_phrase_id'};
    my $submit_label_text      = $config->{'-submit_label_text'};

    if ( defined $submit_label_phrase_id ) {
        $submit_label_text = $phrase_util->text( { '-phrase_id' => $submit_label_phrase_id, } );
    }
    if ( !defined $submit_label_text ) {
        $submit_label_text = ':submit:';
    }
    $submit_args->{'-value'} = $submit_label_text;

    #   reset button
    my $show_reset = $config->{'-show_reset'};

    #   comp
    my $comp_args = {
        '-name'  => 'component',
        '-value' => $self->name,
    };
    $cgi->param( '-name' => 'component', '-value' => $self->name );

    #   action
    my $action_args = {
        '-name'  => 'action',
        '-value' => FORM_ACTION_SUBMIT,
    };
    $cgi->param( '-name' => 'action', '-value' => FORM_ACTION_SUBMIT );

    #   extra footer content
    my $extra_footer_content = $self->render_extra_footer_content();

#<<<
    my $content = 
        $cgi->tfoot({},
        
            #   tr
            $cgi->Tr({},

                #   td
                $cgi->td({ },

                    #   div
                    $cgi->div({ '-class' => 'Submit' },

                        #   submit
                        $cgi->submit($submit_args) )
                        .

                        #   action
                        $cgi->hidden($action_args) .

                        #   comp
                        $cgi->hidden($comp_args),
                ) .
                (defined $extra_footer_content ?
                $cgi->td({ '-colspan' => 1 },
                    
                    #   div
                    $cgi->div({ '-class' => 'Extra' },
                        
                        #   extra footer content
                        $extra_footer_content
                    ) 
                ) : '' )

            ) 
        );
#>>>

    return $content;
} ## end sub render_form_footer

#   action
sub action {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->get_form_config();

    my $return_status = COMP_ACTION_FAILURE;

    #   submit method
    my $submit_method = $config->{'-submit_method'};

    #   validate method
    my $validate_method = $config->{'-validate_method'};

    #   form process data
    $self->form_process_data($args);

    #   action
    my $action = $args->{'-action'};

    #   data
    my $data = $args->{'-data'};

    #   submit
    if ( defined $action && $action eq FORM_ACTION_SUBMIT ) {

        $self->debug( { '-text' => 'Form submitting' } );

        #   all row data
        my $all_row_data = [];

    PARSE_ROW_DATA:
        while ( my ( $key, $value ) = each %{$data} ) {
            my $row;
            if ( $key =~ s/\.(.*)$//sxm ) {
                $row = $1;
            }
            if ( !defined $row ) {
                $self->error( { '-text' => 'Could not identify row for data -key ' . $self->dbg_value($key) } );
                next PARSE_ROW_DATA;
            }
            if ( !defined $all_row_data->[$row] ) {
                $all_row_data->[$row] = {};
            }
            $all_row_data->[$row]->{$key} = $value;
        }

        my $row = 0;
        foreach my $row_data ( @{$all_row_data} ) {

            #   data
            $args->{'-data'} = $row_data;

            #   row
            $args->{'-row'} = $row;

            $self->debug( { '-text' => 'Have -row_data ' . $self->dbg_value($row_data) . ' for -row ' . $self->dbg_value($row) } );

            #   validate
            if ( !$self->$validate_method($args) ) {
                return COMP_ACTION_FAILURE;
            }

            my $success = eval {
                $return_status = $self->$submit_method($args);
                1;
            };
            if ( !$success ) {
                $self->error( { '-text' => 'Failed to call submit method :' . $EVAL_ERROR } );
            }

            $row++;
        }

        #   check validation
        if ( !defined $config->{'-validation_result'} || $config->{'-validation_result'} ) {

            $self->debug( { '-text' => 'Form is valid' } );

            if ( $return_status == COMP_ACTION_SUCCESS ) {

                $self->debug( { '-text' => 'Action successful' } );

                #   success message
                $self->add_success_message();

                #   redirect url
                my $redirect_url = $config->{'-redirect_after_submit_url'} || $config->{'-redirect_after_submit_success_url'};
                if ( defined $redirect_url ) {
                    $self->set_redirect_url( { '-url' => $redirect_url } );
                }
            }
            elsif ( $return_status == COMP_ACTION_FAILURE ) {

                $self->debug( { '-text' => 'Action failed' } );

                #   failure message
                $self->add_failure_message();

                #   redirect url
                my $redirect_url = $config->{'-redirect_after_submit_url'} || $config->{'-redirect_after_submit_failure_url'};
                if ( defined $redirect_url ) {
                    $self->set_redirect_url( { '-url' => $redirect_url } );
                }
            }
        } ## end if ( !defined $config->...)
    } ## end if ( defined $action &&...)

    #   populate
    if ( defined $action && $action eq FORM_ACTION_POPULATE ) {

        $self->debug( { '-text' => 'Form populating' } );

        $return_status = COMP_ACTION_SUCCESS;
    }

    return $return_status;
} ## end sub action

1;
