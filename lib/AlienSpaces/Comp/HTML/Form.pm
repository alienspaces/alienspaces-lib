package AlienSpaces::Comp::HTML::Form;

#
#   $Revision: 184 $
#   $Author: bwwallin $
#   $Date: 2010-08-20 17:39:03 +1000 (Fri, 20 Aug 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(croak carp);

use AlienSpaces::Constants qw(:util :form :comp :message-types :reference);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use constant {
    DEFAULT_SUBMIT_METHOD         => 'form_action',
    DEFAULT_VALIDATE_METHOD       => 'form_validate',
    DEFAULT_VALIDATE_FIELD_METHOD => 'form_validate_field',
    DEFAULT_POPULATE_METHOD       => 'form_populate',
};

use constant DEFAULT_LIST_VALUES_METHOD => 'form_list_values';

#   form config
my $form_config = {

    #   fields
    '-fields' => [
        {   '-name'    => 'xxx',    #   input field name
            '-clear'   => '1|0',    #   1|0 clear before render
                                    #   - defaults to 1
                                    #   - value will still be populated with any data returned
                                    #     from form_populate
            '-session' => '1|0',    #   1|0 clear set and retain as '-session_data'
                                    #   - defaults to 0
                                    #   - value will be populated when submitted by a defined parameter
                                    #   - value will not be populated with data returned
                                    #     from form_populate

            '-label_phrase_id'   => 'xxx',    #   phrase id for label
            '-label_text'        => 'xxx',    #   (or) dont use phrases, just use this text
            '-input_type'        => 'xxx',    #   FORM_INPUT_TYPE_XXX
            '-data_type'         => 'xxx',    #   FORM_DATA_TYPE_XXX
            '-values_method'     => 'xxx',    #   Source list values using this method,
                                              #   - defaults to 'form_list_values'
                                              #   - argument -name of field is passed in
            '-values_table_name' => 'xxx',    #   Source list values from this table
            '-validate_method'   => 'xxx',    #   Validation method specifically for this field
                                              #   - defaults to form_validate_field
                                              #   - return boolean 1|0
            '-validate_rules'    => {         #   Validation rules
                '-required'   => '1|0',       #   required
                '-max_length' => 'xxx',       #   max length
                '-min_length' => 'xxx',       #   min length
                '-do_match'   => 'xxx',       #   must match this regular expression
                '-dont_match' => 'xxx',       #   must not match this regular expression
            },
            '-validate_phrase_id'    => 'xxx',    #    phrase id for failed validation message
            '-validate_text'         => 'xxx',    #    (or) text for failed validation message
            '-validate_text_method'  => 'xxx',    #    (or) call this method for failed validation message
            '-information_phrase_id' => 'xxx',    #    phrase id for information message
            '-information_text'      => 'xxx',    #    (or) text for information message
        },
    ],

    #   allow value logging
    '-allow_value_logging' => '1|0',              #   any logging of values by this class will not happen by default
                                                  #   - defaults to 0
                                                  #   auto complete
    '-auto_complete'       => '1|0',              #   turn off auto complete, is on by default

    #   submit button
    '-submit_label_phrase_id' => 'xxx',           #   submit button phrase id
    '-submit_label_text'      => 'xxx',           #   (or) dont use phrases, just use this text
    '-submit_method'          => 'xxx',           #   defaults to 'form_action'
    '-validate_method'        => 'xxx',           #   defaults to 'form_validate'
                                                  #   - return boolean 1|0
    '-populate_method'        => 'xxx',           #   defaults to 'form_populate'
                                                  #   - return HASHREF or field 'name' => 'value'
                                                  #     to populate fields
    '-show_reset'             => 'xxx',

    #   header
    '-header_phrase_id'     => 'xxx',             #   header phrase id
    '-header_text'          => 'xxx',             #   (or) dont use phrases, just use this text
    '-header_text_method'   => 'xxx',             #   (or) call this method to generate the header

    #   submit url
    '-submit_url' => 'xxx',                       #   defaults to $self->program_name

    #   submit failed phrase
    '-submit_failed_phrase_id'   => 'xxx',        #   phrase to display if form action is not successful
    '-submit_failed_text'        => 'xxx',        #   (or) dont use phrase, just use this text
    '-submit_failed_text_method' => 'xxx',        #   (or) call this method to return the text

    #   submit success phrase
    '-submit_success_phrase_id'   => 'xxx',       #   phrase to display if form action is successful
    '-submit_success_text'        => 'xxx',       #   (or) dont use phrase, just use this text
    '-submit_success_text_method' => 'xxx',       #   (or) call this method to return the text

    #   global, program or component level messages
    '-messages' => 'global|program|component',    #   register messages as global, program or component
                                                  #   level messages
                                                  #   - defaults to global

    '-display_after_submit'                => '1|0',     #   display the form again after submitting
                                                         #   - defaults to 0
    '-display_after_submit_success'        => '1|0',     #   display the form again after submitting with success
                                                         #   - defaults to 0 (overrides -display_after_submit)
    '-display_after_submit_success_method' => 'xxxx',    #   (or) call this method to return 0|1
    '-display_after_submit_failure'        => '1|0',     #   display the form again after submitting with failure
                                                         #   - defaults to 0 (overrides -display_after_submit)
    '-display_after_submit_failure_method' => 'xxxx',    #   (or) call this method to return 0|1

    '-redirect_after_submit_url'         => 'xxx',       #   redirect to URL after submitting
    '-redirect_after_submit_success_url' => 'xxx',       #   redirect to URL after submitting with success
    '-redirect_after_submit_failure_url' => 'xxx',       #   redirect to URL after submitting with failure

};

#   form config
#   - method to return config, do not forget to add this method
sub form_config {
    my ( $self, $args ) = @_;

    return $form_config;
}

#   utils
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_CGI(),
        UTIL_PHRASE(),
        UTIL_SESSION(),
        UTIL_DB_TABLE(),
    ];
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed super init' } );
        return;
    }

    #   set form config
    $self->set_form_config( $self->form_config() );

    return 1;
}

sub set_form_config {
    my ( $self, $config ) = @_;

    #   program messages default
    if ( !exists $config->{'-messages'} ) {
        $config->{'-messages'} = 'global';
    }

    #   allow value logging
    if ( !exists $config->{'-allow_value_logging'} ) {
        $config->{'-allow_value_logging'} = 1;
    }

    #   submit url
    if ( !exists $config->{'-submit_url'} ) {
        $config->{'-submit_url'} = $self->program_name;
    }

    #   populate method
    if ( !exists $config->{'-populate_method'} ) {
        $config->{'-populate_method'} = DEFAULT_POPULATE_METHOD;
    }

    #   submit method
    if ( !exists $config->{'-submit_method'} ) {
        $config->{'-submit_method'} = DEFAULT_SUBMIT_METHOD;
    }

    #   validate method
    if ( !exists $config->{'-validate_method'} ) {
        $config->{'-validate_method'} = DEFAULT_VALIDATE_METHOD;
    }

    #   validate method
    if ( !exists $config->{'-submit_label_phrase_id'} ) {
        $config->{'-submit_label_phrase_id'} = DEFAULT_SUBMIT_LABEL_PHRASE;
    }

    #   fields
    my $fields = $config->{'-fields'};
    if ( !defined $fields || !scalar @{$fields} ) {
        $self->error( { '-text' => 'Missing -fields config' } );
        return;
    }

    #   field count
    $config->{'-field_count'} = scalar @{ $config->{'-fields'} };

    foreach my $field ( @{ $config->{'-fields'} } ) {

        #   values method
        if ( !exists $field->{'-values_method'} ) {
            $field->{'-values_method'} = DEFAULT_LIST_VALUES_METHOD;
        }

        #   validate method
        if ( !exists $field->{'-validate_method'} ) {
            $field->{'-validate_method'} = DEFAULT_VALIDATE_FIELD_METHOD;
        }

        #   clear
        if ( !exists $field->{'-clear'} ) {
            $field->{'-clear'} = 1;
        }
    }

    $self->{'-form_config'} = $config;

    return;
} ## end sub set_form_config

sub get_form_config {
    my ( $self, $args ) = @_;

    return $self->{'-form_config'};
}

sub form_populate {
    my ( $self, $args ) = @_;

    return {};
}

sub form_clear {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   config
    my $config = $self->get_form_config();

    #   fields
    my $fields = $config->{'-fields'};

    if ( !defined $fields || !scalar @{$fields} ) {
        $self->error( { '-text' => 'No form fields defined, not clearing fields' } );
        return;
    }

CLEAR_FIELDS:
    foreach my $field_config ( @{$fields} ) {

        #   name
        my $name = $field_config->{'-name'};

        $self->debug( { '-text' => 'Clearing field ' . $name } );
        $cgi->param( '-name' => $name, '-value' => '' );
    }

    return 1;
}

sub form_validate {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->get_form_config();

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   data
    my $data = $args->{'-data'};

    #   valid
    my $valid = 1;

VALIDATE_FIELDS:
    foreach my $field ( @{ $config->{'-fields'} } ) {
        my $validate_field_method = $field->{'-validate_method'};

        #   default validate field method
        if ( !defined $validate_field_method ) {
            $validate_field_method = DEFAULT_VALIDATE_FIELD_METHOD;
        }

        $self->debug(
            {   '-text' => 'Validating -field ' .
                    $self->dbg_value($field) . ' with method ' . $self->dbg_value($validate_field_method) } );

        my $field_name = $field->{'-name'};
        my $result     = $self->$validate_field_method(
            {   '-field' => $field,
                '-value' => $data->{$field_name},
                '-data'  => $data,
            } );

        if ( !defined $result ) { $result = 0; }

        $self->debug( { '-text' => 'Validated -field  ' . $self->dbg_value($field) . ' -result ' . $self->dbg_value($result) } );

        $field->{'-validation_result'} = $result;

        if ($result) {
            next VALIDATE_FIELDS;
        }

        #   valid
        #   - the entire form is now not valid
        $valid = 0;

        #   validate phrase id
        my $validate_phrase_id = $field->{'-validate_phrase_id'};
        if ( defined $validate_phrase_id ) {
            $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => $validate_phrase_id } );
            $self->debug( { '-text' => 'Validation message set with -validate_phrase_id ' . $validate_phrase_id } );
            next VALIDATE_FIELDS;
        }

        #   validate text
        my $validate_text = $field->{'-validate_text'};
        if ( defined $validate_text ) {
            $field->{'-validation_message'} = $validate_text;
            $self->debug( { '-text' => 'Validation message set with -validate_text ' . $validate_text } );
            next VALIDATE_FIELDS;
        }

        #   validate text method
        my $validate_text_method = $field->{'-validate_text_method'};
        if ( defined $validate_text_method ) {
            $field->{'-validation_message'} = $self->$validate_text_method(
                {   '-field' => $field,
                    '-value' => $data->{$field_name},
                    '-data'  => $data,
                } );
            $self->debug( { '-text' => 'Validation message set with -validate_text_method ' . $validate_text_method } );
            next VALIDATE_FIELDS;
        }

        my $validation_error = $field->{'-validation_error'};
        if ( defined $validation_error ) {

            if ( $validation_error == VALIDATION_ERROR_REQUIRED ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_REQUIRED_PHRASE() } );
                $self->debug( { '-text' => 'Validation error required' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_MAX_LENGTH ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_TOO_LONG_PHRASE() } );
                $self->debug( { '-text' => 'Validation error max length' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_MIN_LENGTH ) {

                $field->{'-validation_message'} = $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_TOO_SHORT_PHRASE() } );
                $self->debug( { '-text' => 'Validation error min length' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_DO_MATCH ) {

                $field->{'-validation_message'} =
                    $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_UNRECOGNIZED_PHRASE() } );
                $self->debug( { '-text' => 'Validation error do match' } );

            }
            elsif ( $validation_error == VALIDATION_ERROR_DONT_MATCH ) {

                $field->{'-validation_message'} =
                    $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_UNRECOGNIZED_PHRASE() } );
                $self->debug( { '-text' => 'Validation error dont match' } );

            }
        } ## end if ( defined $validation_error)
    } ## end VALIDATE_FIELDS: foreach my $field ( @{ $config...})

    #   global validation result
    $config->{'-validation_result'} = $valid;

    return $valid;
} ## end sub form_validate

sub form_validate_field {
    my ( $self, $args ) = @_;

    #   field
    my $field = $args->{'-field'};

    #   value
    my $value = $args->{'-value'};

    #   config
    my $config = $self->get_form_config();

    #   allow value logging
    my $allow_value_logging = $config->{'-allow_value_logging'};

    #   validate rules
    my $validate_rules = $field->{'-validate_rules'};

    $self->debug(
        {   '-text' => 'Validating -field ' .
                $self->dbg_value($field) . ' -value ' . ( $allow_value_logging ? $self->dbg_value($value) : '*****' ) } );

    my $valid;
    if ( defined $validate_rules ) {

        #   required
        $valid = $self->form_validate_field_required(
            {   '-value'          => $value,
                '-field'          => $field,
                '-validate_rules' => $validate_rules,
            } );

        if ( !$valid ) {
            return;
        }

        #   max length
        $valid = $self->form_validate_field_max_length(
            {   '-value'          => $value,
                '-field'          => $field,
                '-validate_rules' => $validate_rules,
            } );

        if ( !$valid ) {
            return;
        }

        #   min length
        $valid = $self->form_validate_field_min_length(
            {   '-value'          => $value,
                '-field'          => $field,
                '-validate_rules' => $validate_rules,
            } );

        if ( !$valid ) {
            return;
        }

        #   do match
        $valid = $self->form_validate_field_do_match(
            {   '-value'          => $value,
                '-field'          => $field,
                '-validate_rules' => $validate_rules,
            } );

        if ( !$valid ) {
            return;
        }

        #   dont match
        $valid = $self->form_validate_field_dont_match(
            {   '-value'          => $value,
                '-field'          => $field,
                '-validate_rules' => $validate_rules,
            } );

        if ( !$valid ) {
            return;
        }
    } ## end if ( defined $validate_rules)

    return 1;
} ## end sub form_validate_field

sub form_validate_field_required {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $field          = $args->{'-field'};
    my $validate_rules = $args->{'-validate_rules'};

    #   required
    my $required = $validate_rules->{'-required'};
    if ( defined $required && $required ) {
        if ( !defined $value || !length($value) ) {

            #   validation error required
            $field->{'-validation_error'} = VALIDATION_ERROR_REQUIRED;

            $self->debug( { '-text' => 'Failed validation' } );
            return;
        }
    }

    $self->debug( { '-text' => 'Passed validation' } );
    return 1;
}

sub form_validate_field_max_length {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $field          = $args->{'-field'};
    my $validate_rules = $args->{'-validate_rules'};

    #   max length
    my $max_length = $validate_rules->{'-max_length'};
    if ( defined $max_length && $max_length ) {
        if ( length($value) > $max_length ) {

            #   validation error max length
            $field->{'-validation_error'} = VALIDATION_ERROR_MAX_LENGTH;

            $self->debug( { '-text' => 'Failed validation' } );
            return;
        }
    }

    $self->debug( { '-text' => 'Passed validation' } );
    return 1;
}

sub form_validate_field_min_length {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $field          = $args->{'-field'};
    my $validate_rules = $args->{'-validate_rules'};

    #   min length
    my $min_length = $validate_rules->{'-min_length'};
    if ( defined $min_length && $min_length ) {
        if ( length($value) < $min_length ) {

            #   validation error min length
            $field->{'-validation_error'} = VALIDATION_ERROR_MIN_LENGTH;

            $self->debug( { '-text' => 'Failed validation' } );
            return;
        }
    }

    $self->debug( { '-text' => 'Passed validation' } );
    return 1;
}

sub form_validate_field_do_match {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $field          = $args->{'-field'};
    my $validate_rules = $args->{'-validate_rules'};

    #   do match
    my $do_match = $validate_rules->{'-do_match'};
    if ( defined $do_match ) {
        if ( !( $value =~ /$do_match/sxmg ) ) {

            #   validation error do match
            $field->{'-validation_error'} = VALIDATION_ERROR_DO_MATCH;

            $self->debug( { '-text' => 'Failed validation' } );
            return;
        }
    }

    $self->debug( { '-text' => 'Passed validation' } );
    return 1;
}

sub form_validate_field_dont_match {
    my ( $self, $args ) = @_;

    my $value          = $args->{'-value'};
    my $field          = $args->{'-field'};
    my $validate_rules = $args->{'-validate_rules'};

    #   dont match
    my $dont_match = $validate_rules->{'-dont_match'};
    if ( defined $dont_match ) {
        if ( $value =~ /$dont_match/sxmg ) {

            #   validation error dont match
            $field->{'-validation_error'} = VALIDATION_ERROR_DONT_MATCH;

            $self->debug( { '-text' => 'Failed validation' } );
            return;
        }
    }

    $self->debug( { '-text' => 'Passed validation' } );

    return 1;
}

#   form process data
#   - separates out session stored data
#   - this would be the place to automatically html strip
sub form_process_data {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->get_form_config();

    #   data
    my $data = $args->{'-data'};

    #   session data
    my $session_data = $args->{'-session_data'};

    if ( !defined $config->{'-session'} || !scalar @{ $config->{'-session'} } ) {
        $self->debug( { '-text' => 'No session storage configured' } );
        return 1;
    }

    $self->debug( { '-text' => 'Processing form session storage' } );

    foreach my $session ( @{ $config->{'-session'} } ) {

        #   name
        my $name = $session->{'-name'};

        if ( exists $data->{$name} ) {

            my $value = delete $data->{$name};

            $session_data->{$name} = $value;

            $self->debug( { '-text' => 'Setting session data -name ' . $name . ' -value ' . $self->dbg_value($value) } );
        }
    }

    $self->debug( { '-text' => 'Form -name ' . $self->name . ' -session_data ' . $self->dbg_value($session_data) } );

    return 1;
} ## end sub form_process_data

sub form_action {
    my ( $self, $args ) = @_;

    return COMP_ACTION_FAILURE;
}

sub form_list_values {
    my ( $self, $args ) = @_;

    return ( [], {} );
}

#   render
sub render {
    my ( $self, $args ) = @_;

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    #   content
    my $content;

    #   header phrase id
    my $header_phrase_id = $config->{'-header_phrase_id'};

    #   header text
    my $header_text = $config->{'-header_text'};

    #   header text method
    my $header_text_method = $config->{'-header_text_method'};

    #   display header text
    my $display_header_text;
    if ( defined $header_phrase_id ) {
        $display_header_text = $phrase_util->text( { '-phrase_id' => $header_phrase_id, } );
    }
    elsif ( defined $header_text ) {
        $display_header_text = $header_text;
    }
    elsif ( defined $header_text_method ) {
        $display_header_text = $self->$header_text_method($args);
    }

    #   messages
    my $messages = $config->{'-messages'};

    #   display after submit
    my $display_after_submit = $config->{'-display_after_submit'};
    if ( !defined $display_after_submit ) {
        $display_after_submit = 1;
    }

    my $display_after_submit_success        = $config->{'-display_after_submit_success'};
    my $display_after_submit_success_method = $config->{'-display_after_submit_success_method'};
    my $display_after_submit_failure        = $config->{'-display_after_submit_failure'};
    my $display_after_submit_failure_method = $config->{'-display_after_submit_failure_method'};

    #   comp args
    my $comp_args;
    if ( defined $args->{'-actions'} ) {
        $comp_args = $args->{'-actions'}->{ $self->name };
    }

    #   display after submit failure
    if ( defined $comp_args->{'-status'} && $comp_args->{'-status'} == COMP_ACTION_FAILURE ) {

        if ( defined $display_after_submit_failure ) {
            $display_after_submit = $display_after_submit_failure;
        }
        elsif ( defined $display_after_submit_failure_method ) {
            $display_after_submit = $self->$display_after_submit_failure_method($args);
        }
    }

    #   display after submit success
    elsif ( defined $comp_args->{'-status'} && $comp_args->{'-status'} == COMP_ACTION_SUCCESS ) {

        if ( defined $display_after_submit_success ) {
            $display_after_submit = $display_after_submit_success;
        }
        elsif ( defined $display_after_submit_success_method ) {
            $display_after_submit = $self->$display_after_submit_success_method($args);
        }
    }

    #   if we are not displaying the form after submission and we
    #   are showing messages at the program level then we dont want to
    #   display anything at all.
    if ( !$display_after_submit && ( $messages eq 'global' || $messages eq 'program' ) ) {
        $self->debug(
            {   '-text' => 'Have -display_after_submit ' .
                    $self->dbg_value($display_after_submit) . ' -messages ' . $self->dbg_value($messages) . ', not displaying form'
            } );
        return;
    }

    #   if we are not displaying the form after submission and we
    #   are not showing messages at the program level then we
    #   display the panel with no form content
    if ($display_after_submit) {
        $content = $self->render_form($comp_args);
    }

    if ( !defined $content ) {
        $self->debug( { '-text' => 'Have no content, not displaying form' } );
        return;
    }

    return $self->panel->render(
        {   '-name'    => $self->name,
            '-title'   => $display_header_text,
            '-content' => $content,
            '-class'   => 'Form',
        } );
} ## end sub render

sub add_success_message {
    my ( $self, $args ) = @_;

    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    #   submit success phrase
    my $success_phrase_id   = $config->{'-submit_success_phrase_id'};
    my $success_text        = $config->{'-submit_success_text'};
    my $success_text_method = $config->{'-submit_success_text_method'};

    if ( defined $success_phrase_id ) {
        $self->debug( { '-text' => 'Fetching phrase for success text' } );
        $success_text = $phrase_util->text( { '-phrase_id' => $success_phrase_id } );
    }

    if ( !defined $success_text ) {
        if ( defined $success_text_method ) {
            $self->debug( { '-text' => 'Calling method for success text' } );
            $success_text = $self->$success_text_method($args);
        }
    }

    if ( !defined $success_text ) {
        $self->debug( { '-text' => 'No success text, not rendering success messages' } );
        return;
    }

    #   component message
    if ( $config->{'-messages'} eq 'component' ) {
        $self->add_component_message(
            {   '-type' => MESSAGE_TYPE_SUCCESS(),
                '-text' => $success_text,
            } );
    }

    #   program message
    elsif ( $config->{'-messages'} eq 'program' ) {
        $self->add_program_message(
            {   '-type' => MESSAGE_TYPE_SUCCESS(),
                '-text' => $success_text,
            } );
    }

    #   global message
    else {
        $self->add_message(
            {   '-type' => MESSAGE_TYPE_SUCCESS(),
                '-text' => $success_text,
            } );
    }

    return 1;
} ## end sub add_success_message

sub add_failure_message {
    my ( $self, $args ) = @_;

    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    #   submit failed phrase
    my $failed_phrase_id   = $config->{'-submit_failed_phrase_id'};
    my $failed_text        = $config->{'-submit_failed_text'};
    my $failed_text_method = $config->{'-submit_failed_text_method'};

    if ( defined $failed_phrase_id ) {
        $self->debug( { '-text' => 'Fetching phrase for failed text' } );
        $failed_text = $phrase_util->text( { '-phrase_id' => $failed_phrase_id } );
    }

    if ( !defined $failed_text ) {
        if ( defined $failed_text_method ) {
            $self->debug( { '-text' => 'Calling method for failed text' } );
            $failed_text = $self->$failed_text_method($args);
        }
    }

    if ( !defined $failed_text ) {
        $self->debug( { '-text' => 'No failure text, not rendering failure messages' } );
        return;
    }

    #   component message
    if ( $config->{'-component_messages'} ) {
        $self->add_component_message(
            {   '-type' => MESSAGE_TYPE_ERROR(),
                '-text' => $failed_text,
            } );
    }

    #   program message
    elsif ( $config->{'-program_messages'} ) {
        $self->add_program_message(
            {   '-type' => MESSAGE_TYPE_ERROR(),
                '-text' => $failed_text,
            } );
    }

    #   global message
    else {
        $self->add_message(
            {   '-type' => MESSAGE_TYPE_ERROR(),
                '-text' => $failed_text,
            } );
    }

    return 1;
} ## end sub add_failure_message

sub render_form_header {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    my $content = '';

    #   extra header content
    my $extra_header_content = $self->render_extra_header_content();
    if ( !defined $extra_header_content ) {
        return $content;
    }

    #<<<
    $content =

        #   thead
        $cgi->thead(
        {},

        #   Tr
        $cgi->Tr(
            {},

            #   th
            $cgi->th( { '-colspan' => 2, '-class' => 'Extra' }, $extra_header_content ) )

        );

    #>>>

    return $content;
} ## end sub render_form_header

#   render extra header content
#   - placeholder for sub-class to implement additional content
#     in the header cell to the right
sub render_extra_header_content {
    my ( $self, $args ) = @_;

    return;
}

sub render_form_footer {
    my ( $self, $args ) = @_;

    #   util cgi
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   util phrase
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   config
    my $config = $self->get_form_config();

    #   submit args
    my $submit_args = {};

    #   label
    my $submit_label_phrase_id = $config->{'-submit_label_phrase_id'};
    my $submit_label_text      = $config->{'-submit_label_text'};

    if ( defined $submit_label_phrase_id ) {
        $submit_label_text = $phrase_util->text( { '-phrase_id' => $submit_label_phrase_id, } );
    }
    if ( !defined $submit_label_text ) {
        $submit_label_text = ':submit:';
    }
    $submit_args->{'-value'} = $submit_label_text;

    #   reset button
    my $show_reset = $config->{'-show_reset'};

    #   comp
    my $comp_args = {
        '-name'  => 'component',
        '-value' => $self->name,
    };
    $cgi->param( '-name' => 'component', '-value' => $self->name );

    #   action
    my $action_args = {
        '-name'  => 'action',
        '-value' => FORM_ACTION_SUBMIT,
    };
    $cgi->param( '-name' => 'action', '-value' => FORM_ACTION_SUBMIT );

    #   extra footer content
    my $extra_footer_content = $self->render_extra_footer_content();

    my $content = $cgi->tfoot(
        {},

        #   tr
        $cgi->Tr(
            {},

            #   td
            $cgi->td(
                { '-colspan' => ( defined $extra_footer_content ? 1 : 2 ) },

                #   div
                $cgi->div(
                    { '-class' => 'Submit' },

                    #   submit
                    $cgi->submit($submit_args)
                    ) .

                    #   action
                    $cgi->hidden($action_args) .

                    #   comp
                    $cgi->hidden($comp_args),
                ) .

                (
                defined $extra_footer_content ?
                    $cgi->td(
                    { '-colspan' => 1 },

                    #   div
                    $cgi->div(
                        { '-class' => 'Extra' },

                        #   extra footer content
                        $extra_footer_content
                    )
                    ) :
                    ''
                )

        ) );

    return $content;
} ## end sub render_form_footer

#   render extra footer content
#   - placeholder for sub-class to implement additional content
#     in the footer cell to the right
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    return;
}

#   render form
#   - implemented in sub-class
sub render_form {
    my ( $self, $args ) = @_;

    $self->error( { '-text' => 'Implement in sub-class' } );

    return;
}

#   render form fields
#   - implemented in sub-class
sub render_form_fields {
    my ( $self, $args ) = @_;

    $self->error( { '-text' => 'Implement in sub-class' } );

    return;
}

#   action
#   - implemented in sub-class
sub action {
    my ( $self, $args ) = @_;

    $self->error( { '-text' => 'Implement in sub-class' } );

    return;
}

1;
