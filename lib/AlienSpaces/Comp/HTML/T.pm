package AlienSpaces::Comp::HTML::T;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   HTML component template class
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties { 'xxx' => { '-type' => 's' }, };

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_XXXX(),
    ];
}

#   render
sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   title
    my $title = 'Xxxx';

    #   content
    my $content = '';

    return $self->panel->render(
        {   '-name'    => $self->name,
            '-title'   => $title,
            '-content' => $content,
        } );
}

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $action = $args->{'-action'};
    my $data   = $args->{'-data'};

    #   handle action
    if ( $action eq 'xxx' ) {

        return COMP_ACTION_SUCCESS();
    }

    return COMP_ACTION_FAILURE();
}

1;

