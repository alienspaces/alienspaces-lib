package AlienSpaces::Comp::HTML::Site::Footer;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use AlienSpaces::Constants qw(:util :program :panel-styles);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   render
sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   content
    my $content;

    #   alienspaces application url
    my $alienspaces_application_url = $self->environment->{'ALIENSPACES_APP_URL'};
    if ( defined $alienspaces_application_url ) {

        $content = $cgi_util->div(
            { '-class' => 'yui3-u-1' },

            #   phrase 100000: Copyright <ENV:YEAR> All Rights Reserved
            $phrase_util->text( { '-phrase_id' => '100000' } )
                . $cgi_util->div( { '-class' => 'AlienSpacesLink' }, $cgi_util->a( { '-href' => $alienspaces_application_url }, $alienspaces_application_url ) ) );

    }
    else {

        $content = $cgi_util->div(
            { '-class' => 'yui3-u-1' },

            #   phrase 100000: Copyright <ENV:YEAR> All Rights Reserved
            $phrase_util->text( { '-phrase_id' => '100000' } ) );

    }

    my $ret = $self->panel->render(
        {   '-name'        => $self->name,
            '-content'     => $content,
            '-style'       => PANEL_STYLE_BASIC,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );

    return $ret;
} ## end sub render

1;
