package AlienSpaces::Comp::HTML::Site::Logo;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :panel-styles :link);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   home href
    my $home_href = $self->make_program_link( { '-program' => PROGRAM_SITE_HOME } );

    my $content = '';

#<<<
    $content .= 
        $cgi->div( { '-class' => 'yui3-g' },
            $cgi->div( { '-class' => 'yui3-u-1' }, 
                $cgi->div( { '-class' => 'Logo' }, 
                    $cgi->a( { '-href' => $home_href, '-title' => $home_href },
                        $cgi->img( { '-src' => '/img/site/alienspaces-logo-white.png', '-alt' => 'AlienSpaces' } ) 
                    )
                ) 
            )
        );
#>>>    

    return $self->panel->render(
        {   '-name'        => $self->name,
            '-content'     => $content,
            '-style'       => PANEL_STYLE_BASIC,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
} ## end sub render

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #

    return $self->COMP_ACTION_SUCCESS;
}

1;

