package AlienSpaces::Comp::HTML::Site::Message;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :panel-styles :message-types);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $content = $self->render_message_content();

    return $self->panel->render(
        {   '-name'        => $self->name,
            '-content'     => $content,
            '-style'       => PANEL_STYLE_BASIC,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
}

#   message type class map
my %message_type_class_map = (
    MESSAGE_TYPE_SUCCESS() => 'Success',
    MESSAGE_TYPE_ERROR()   => 'Error',
    MESSAGE_TYPE_INFO()    => 'Info',
);

sub render_message_content {
    my ( $self, $args ) = @_;

    my $cgi = $self->cgi;

    #   messages
    my @messages = ();

    #   names
    my @names = ( 'global', $self->program_name );

    foreach my $name (@names) {

        #   messages
        #   - all types
        my $messages = $self->get_messages( { '-name' => $name } );
        if ( defined $messages && scalar @{$messages} ) {
            push @messages, @{$messages};
        }
    }

    #   content
    my $content = $cgi->start_div( { '-class' => 'Messages' } );

    #   message count
    my $message_count = 0;

    #   sort messages by type
RENDER_MESSAGES:
    foreach my $message ( sort { $a->{'-type'} <=> $b->{'-type'} } @messages ) {

        #   text
        my $message_text = $message->{'-text'};
        if ( !$message_text ) {
            next RENDER_MESSAGES;
        }

        #   type
        my $message_type = $message->{'-type'};

        $content .= $cgi->div( { '-class' => $message_type_class_map{$message_type} }, $message_text );

        $message_count++;
    }

    $content .= $cgi->end_div();

    if ( !$message_count ) {
        $self->debug( { '-text' => 'No messages rendered' } );
        return;
    }

    return $content;
} ## end sub render_message_content

1;

__END__


