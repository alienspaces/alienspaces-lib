package AlienSpaces::Comp::HTML::Site::Policy;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(croak carp);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub init {
    my ( $self, $args ) = @_;

    return $self->SUPER::init($args);
}

1;
