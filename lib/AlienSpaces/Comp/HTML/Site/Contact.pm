package AlienSpaces::Comp::HTML::Site::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :form :comp :contact-type);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_FEEDBACK(),
        UTIL_USER(),
        UTIL_EMAIL(),

        #   db interfaces
        UTIL_DB_RECORD_CONTACT(),
        UTIL_DB_RECORD_SYSTEM_CONTACT(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {
        '-header_phrase_id' => 50500,        
        '-fields' => [
            {   '-name'            => 'name',
                '-label_phrase_id' => 50510,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => { '-required' => 1, },
            },
            {   '-name'            => 'email',
                '-clear'           => 1,
                '-label_phrase_id' => 50520,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required' => 1,
                    '-do_match' => '^.*\@.*?\..*$'
                },
            },
            {   '-name'            => 'text',
                '-clear'           => 1,
                '-label_phrase_id' => 50540,
                '-input_type'      => FORM_INPUT_TYPE_TEXTAREA,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required'   => 1,
                    '-min_length' => 10,
                },
            },
        ],

        '-submit_label_phrase_id' => 50550,

        '-show_reset'             => 0,

        #   submit failed phrase
        '-submit_failed_phrase_id' => 50560,

        #   submit success phrase
        '-submit_success_phrase_id' => 50570,

        #   display after submit success
        '-display_after_submit_success' => 0,
    };
} ## end sub form_config

#   form list values
sub form_list_values {
    my ( $self, $args ) = @_;

    my $values = [];
    my $labels = {};

    my $field_name = $args->{'-name'};
    if ( $field_name eq 'feedback_type' ) {

    }

    return ( $values, $labels );
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   feedback util
    my $feedback_util = $self->utils->{ UTIL_FEEDBACK() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user id (if available)
    my $user_id = $user_util->user_id();

    my $data = $args->{'-data'};

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    my $name  = $data->{'name'};
    my $email = $data->{'email'};
    my $text  = $data->{'text'};

    #   html strip
    $name  = $self->html_strip($name);
    $email = $self->html_strip($email);
    $text  = $self->html_strip($text);

    if ($feedback_util->add_feedback(
            {   '-user_id' => $user_id,
                '-name'    => $name,
                '-email'   => $email,
                '-text'    => $text,
            } )
        ) {

        if ($self->email_feedback(
                {   '-user_id' => $user_id,
                    '-name'    => $name,
                    '-email'   => $email,
                    '-text'    => $text,
                } )
            ) {
            return COMP_ACTION_SUCCESS;
        }
    }

    return COMP_ACTION_FAILURE;
} ## end sub form_action

#   email feedback
sub email_feedback {
    my ( $self, $args ) = @_;

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec system contact
    my $db_rec_system_contact = $self->utils->{ UTIL_DB_RECORD_SYSTEM_CONTACT() };

    my $name  = $args->{'-name'}  || 'Not provided';
    my $email = $args->{'-email'} || 'Not provided';
    my $text  = $args->{'-text'};

    #   system contact recs
    my $system_contact_recs = $db_rec_system_contact->fetch( { '-user_contact_type_id' => CONTACT_TYPE_SYSTEM_SUPPORT_GENERAL, } );
    if ( !defined $system_contact_recs || !scalar @{$system_contact_recs} ) {
        $self->debug( { '-text' => 'No system contact records found for -system_contact_type_id ' . CONTACT_TYPE_SYSTEM_SUPPORT_GENERAL } );
        return 1;
    }

    #   contacts
    my $contacts = [];
    foreach my $system_contact_rec ( @{$system_contact_recs} ) {

        #   contact rec
        my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $system_contact_rec->{'-contact_id'}, } );
        if ( defined $contact_rec ) {
            $self->debug( { '-text' => 'Adding -contact ' . $self->dbg_value($contact_rec) } );
            push @{$contacts}, $contact_rec;
        }
    }

    if ( !defined $contacts || !scalar @{$contacts} ) {
        $self->debug( { '-text' => 'No contact records found for -system_contact_type_id ' . CONTACT_TYPE_SYSTEM_SUPPORT_GENERAL } );
        return 1;
    }

    #   hostname
    my $hostname = $self->environment->{'APP_HOSTNAME'};

    #   subject
    my $subject = $hostname . ' contact submission';

    #   need to template this stuff
    my $body = <<"EMAIL";

The following message was submitted for $hostname

-----------------------
Name  : $name
Email  : $email
-----------------------

Message: $text

-----------------------

EMAIL

EMAIL_CONTACTS:
    foreach my $contact ( @{$contacts} ) {

        if ( !defined $contact->{'-email_address'} ) {
            $self->error( { '-text' => 'Feedback -contact ' . $self->dbg_value($contact) . ' missing email address, cannot send email' } );
            next EMAIL_CONTACTS;
        }

        if (!$email_util->create_email(
                {   '-sender'    => $self->environment->{'APP_NOREPLY_EMAIL'},
                    '-reply_to'  => $self->environment->{'APP_NOREPLY_EMAIL'},
                    '-subject'   => $subject,
                    '-body'      => $body,
                    '-recipient' => {
                        '-type'    => 'to',
                        '-address' => $contact->{'-email_address'},
                    } } )
            ) {
            $self->error( { '-text' => 'Failed creating email' } );
            next EMAIL_CONTACTS;
        }

        $self->debug( { '-text' => 'Feedback emailed to feedback contact -email_address ' . $contact->{'-email_address'} } );
    }

    return 1;
} ## end sub email_feedback

1;

