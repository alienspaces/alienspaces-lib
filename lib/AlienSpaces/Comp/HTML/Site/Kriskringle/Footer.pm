package AlienSpaces::Comp::HTML::Site::Kriskringle::Footer;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :panel-styles);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

    ];

    return $util_list;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   content
    my $content = $cgi_util->div(
        { '-class' => 'yui3-u-1' },

        #   phrase 100000: Copyright <ENV:YEAR> All Rights Reserved
        $phrase_util->text( { '-phrase_id' => '100000' } ) );

    my $ret = $self->panel->render(
        {   '-name'        => $self->name,
            '-content'     => $content,
            '-style'       => PANEL_STYLE_BASIC,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );

    return $ret;
}

#   action
sub action {
    my ( $self, $args ) = @_;

    #   interface
    my $interface = $args->{'-interface'};

    #   action
    #   - action submitted specifically to this component
    my $action = $args->{'-action'};
    if ( !defined $action ) {
        return COMP_ACTION_FAILURE();
    }

    #   data
    #   - post/get data accompanying the action
    my $data = $args->{'-data'};

    if ( $action eq 'xxx' ) {

        my $xxx = $data->{'xxx'};
        my $xxy = $data->{'xxy'};

        if ($self->xxxx(
                {   '-xxx' => $xxx,
                    '-xxy' => $xxy,
                } )
            ) {
            return COMP_ACTION_SUCCESS();
        }

    }

    return COMP_ACTION_FAILURE();
} ## end sub action

1;

