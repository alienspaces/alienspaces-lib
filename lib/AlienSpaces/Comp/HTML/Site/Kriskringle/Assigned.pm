package AlienSpaces::Comp::HTML::Site::Kriskringle::Assigned;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:program :comp :util);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_SITE_KRISKRINGLE(),
    ];

    return $util_list;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    my $cgi = $self->utils->{ UTIL_CGI() };

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    #   participant
    my $participant = $kriskringle_util->get_session_participant();
    if ( !defined $participant ) {
        $self->error( { '-text' => 'Missing session participant, cannot render' } );
        return;
    }

    #   assigned participant
    my $assigned_participant = $kriskringle_util->get_assigned_participant( { '-digest' => $participant->{'digest'} } );
    if ( !defined $assigned_participant ) {
        $self->error( { '-text' => 'Failed to assign participant' } );
        return;
    }

    return $self->panel->render(
        {   '-name'    => $self->name(),
            '-content' => $cgi->div(
                { '-class' => 'Assigned' },
                'Hi ' . $participant->{'name'} .
                    ', Your Kris Kringle is ' . $cgi->span( { '-class' => 'AssignedName' }, $assigned_participant->{'name'} )
            ),
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
} ## end sub render

#   action
sub action {
    my ( $self, $args ) = @_;

    #   interface
    my $interface = $args->{'-interface'};

    #   action
    #   - action submitted specifically to this component
    my $action = $args->{'-action'};
    if ( !defined $action ) {
        return COMP_ACTION_FAILURE();
    }

    #   data
    #   - post/get data accompanying the action
    my $data = $args->{'-data'};

    if ( $action eq 'select' ) {

        my $digest = $data->{'digest'};

        if ( $self->set_participant( { '-digest' => $digest, } ) ) {
            return COMP_ACTION_SUCCESS();
        }

    }

    return COMP_ACTION_FAILURE();
}

sub set_participant {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-digest'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $digest = $args->{'-digest'};

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    if ( !$kriskringle_util->set_session_participant( { '-digest' => $digest } ) ) {
        $self->error( { '-text' => 'Failed to set session participant given -digest ' . $self->dbg_value($digest) } );
        return;
    }

    $self->debug( { '-text' => 'Set session participant given -digest ' . $self->dbg_value($digest) } );

    return 1;
}

1;

