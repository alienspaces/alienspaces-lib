package AlienSpaces::Comp::HTML::Site::Kriskringle::Participant::Wishlist::List;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:programs :comps :utils :links);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use constant { DEFAULT_WISHES_COUNT => 10, };

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_SITE_KRISKRINGLE(),
    ];

    return $util_list;
}

sub render {
    my ( $self, $args ) = @_;

    #   title
    my $title = 'Your wish list';

    #   data
    my $data = $self->get_table_data();
    if ( !defined $data || !scalar @{$data} ) {
        return;
    }

    #   column titles
    my $column_titles = [ 'Wish', 'Web Link', undef, ];

    return $self->table->render(
        {   '-name'          => $self->name,
            '-title'         => $title,
            '-header_class'  => 'HeadingMedium',
            '-column_titles' => $column_titles,
            '-data'          => $data,
        } );
}

sub get_table_data {
    my ( $self, $args ) = @_;

    my $cgi = $self->cgi;

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    #   participant
    my $participant = $kriskringle_util->get_session_participant();
    if ( !defined $participant ) {
        $self->error( { '-text' => 'Missing session participant, cannot perform action' } );
        return;
    }

    #   wishes
    my $wishes = $kriskringle_util->get_participant_wishes(
        {   '-count'  => DEFAULT_WISHES_COUNT,
            '-digest' => $participant->{'digest'},
        } );

    if ( !defined $wishes ) {
        $self->debug( { '-text' => 'No wishes returned, not returning data' } );
        return;
    }

    #   return data
    my $return_data = [];

    foreach my $wish ( @{$wishes} ) {

        my $links = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-name'      => 'Edit',
                        '-component' => COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT_WISHLIST,
                        '-program'   => PROGRAM_SITE_KRISKRINGLE_HOME(),
                        '-action'    => 'edit',
                        '-id'        => $wish->{'id'},
                    },
                    {   '-name'      => 'Delete',
                        '-component' => COMP_HTML_SITE_KRISKRINGLE_PARTICIPANT_WISHLIST_LIST,
                        '-program'   => PROGRAM_SITE_KRISKRINGLE_HOME(),
                        '-action'    => 'delete',
                        '-id'        => $wish->{'id'},
                    },
                ],
                '-align'       => LINK_RIGHT(),
                '-orientation' => LINK_HORIZONTAL(),
            } );

        push @{$return_data}, [

            #   text
            $wish->{'text'},

            #   url
            (   length $wish->{'url'} ?
                    $cgi->a( { '-href' => $wish->{'url'}, '-target' => 'ext' }, $self->html_format( $wish->{'url'} ) ) :
                    $cgi->span({'-class' => 'None' }, 'None')
            ),
            $links,
        ];
    } ## end foreach my $wish ( @{$wishes...})

    return $return_data;
} ## end sub get_table_data

#   action
sub action {
    my ( $self, $args ) = @_;

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    #   participant
    my $participant = $kriskringle_util->get_session_participant();
    if ( !defined $participant ) {
        $self->error( { '-text' => 'Missing session participant, cannot perform action' } );
        return COMP_ACTION_FAILURE;
    }

    #   action
    my $action = $args->{'-action'};

    #   data
    my $data = $args->{'-data'};

    if ( defined $action && $action eq 'delete' ) {

        #   id
        my $id = $data->{'id'};

        if ($kriskringle_util->delete_participant_wish(
                {   '-id'     => $id,
                    '-digest' => $participant->{'digest'},
                } )
            ) {

            #   delete cgi parameters
            #   - this action should really be posted to Comp/HTML/Site/Kriskringle/Participant/Wishlist
            #     so it can clear its own form parameters.. :/
            my $cgi = $self->utils->{ UTIL_CGI() };

            $cgi->param( '-name' => 'id', '-value' => '' );
            $cgi->param( '-name' => 'text', '-value' => '' );
            $cgi->param( '-name' => 'url', '-value' => '' );

            return COMP_ACTION_SUCCESS;
        }
    }

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

