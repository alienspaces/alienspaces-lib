package AlienSpaces::Comp::HTML::Site::Kriskringle::Participant::Wishlist;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:program :comp :util :form :links);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_SITE_KRISKRINGLE(),
        UTIL_DB_TABLE(),    #   Alienspaces Util::DB::Table class
    ];

    return $util_list;
}

#   edit wish
my $edit_wish = {};

#   init
sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super init failed' } );
        return;
    }

    #   reset edit wish
    $edit_wish = {};

    return 1;
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   fields
        '-fields' => [
            {   '-name'       => 'text',
                '-clear'      => 0,
                '-label_text' => 'Wish',
                '-input_type' => FORM_INPUT_TYPE_TEXTAREA(),
                '-data_type'  => FORM_DATA_TYPE_TEXT(),

                '-validate_rules' => {
                    '-required'   => '1',
                    '-min_length' => '5',
                    '-max_length' => '1000',
                },
            },
            {   '-name'       => 'url',
                '-clear'      => 0,
                '-label_text' => 'Web Link',
                '-input_type' => FORM_INPUT_TYPE_TEXTFIELD(),
                '-data_type'  => FORM_DATA_TYPE_TEXT(),

                '-validate_rules' => {
                    '-required'   => '0',
                    '-min_length' => '0',
                    '-max_length' => '1000',
                },
            },
            {   '-name'       => 'id',
                '-clear'      => 0,
                '-input_type' => FORM_INPUT_TYPE_HIDDEN(),
                '-data_type'  => FORM_DATA_TYPE_NUMBER(),
            },
        ],

        #   header text method
        '-header_text_method' => 'get_header_text',

        #   submit button
        '-submit_label_text' => 'Save',

        #   submit failed phrase
        '-submit_failed_text' => 'Sorry, something went wrong with saving your data! Get a techie to look in the logs...',

        #   submit success text method
        '-submit_success_text_method' => 'submit_success_text_method',

        #   display the form after subitting
        '-display_after_submit' => 1,
    };

} ## end sub form_config

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    return $edit_wish;
}

sub get_header_text {
    my ( $self, $args ) = @_;

    my $id;
    if ( defined $edit_wish ) {
        $id = $edit_wish->{'-id'};
    }

    if ( defined $id ) {
        return 'Update this wish';
    }

    return 'Add to your wish list';
}

#   action
sub action {
    my ( $self, $args ) = @_;

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    #   participant
    my $participant = $kriskringle_util->get_session_participant();
    if ( !defined $participant ) {
        $self->error( { '-text' => 'Missing session participant, cannot perform action' } );
        return COMP_ACTION_FAILURE;
    }

    #   action
    my $action = $args->{'-action'};

    #   data
    my $data = $args->{'-data'};

    if ( $action eq 'edit' ) {

        my $id = $data->{'id'};

        #   wish
        my $wish = $kriskringle_util->get_participant_wish( { '-id' => $id, '-digest' => $participant->{'digest'} } );

        if ( defined $wish ) {

            $edit_wish->{'-id'}   = $wish->{'id'};
            $edit_wish->{'-text'} = $wish->{'text'};
            $edit_wish->{'-url'}  = $wish->{'url'};

            return COMP_ACTION_SUCCESS;
        }

    }

    return $self->SUPER::action($args);
} ## end sub action

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    #   participant
    my $participant = $kriskringle_util->get_session_participant();
    if ( !defined $participant ) {
        $self->error( { '-text' => 'Missing session participant, cannot perform action' } );
        return COMP_ACTION_FAILURE;
    }

    #   data
    my $data = $args->{'-data'};

    #   wish id
    my $id = $data->{'id'};

    #   text
    my $text = $data->{'text'};

    #   url
    my $url = $data->{'url'};

    #   remove any html
    $text = $self->html_strip($text);
    $url  = $self->html_strip($url);

    #   update wish
    if ( defined $id &&
        $id ne '' ) {
        $self->debug( { '-text' => 'Updating wish...' } );
        if ($kriskringle_util->update_participant_wish(
                {   '-id'     => $id,
                    '-digest' => $participant->{'digest'},
                    '-text'   => $text,
                    '-url'    => $url,
                } )
            ) {
            $self->debug( { '-text' => 'Updated wish!' } );

            #   form clear
            $self->form_clear();

            return COMP_ACTION_SUCCESS;
        }
    }

    #   add wish
    else {
        $self->debug( { '-text' => 'Adding wish...' } );
        if ($kriskringle_util->add_participant_wish(
                {   '-digest' => $participant->{'digest'},
                    '-text'   => $text,
                    '-url'    => $url,
                } )
            ) {

            $self->debug( { '-text' => 'Added wish!' } );

            #   form clear
            $self->form_clear();

            return COMP_ACTION_SUCCESS;
        }
    }

    return COMP_ACTION_FAILURE;
} ## end sub form_action

#   submit success text method
sub submit_success_text_method {
    my ( $self, $args ) = @_;

    #   action
    my $action = $args->{'-action'};

    if ( defined $action && $action eq 'edit' ) {
        return;
    }

    return 'Link saved successfully';
}

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    my $id;
    if ( defined $edit_wish ) {
        $id = $edit_wish->{'-id'};
    }

    if ( !defined $id ) {
        return;
    }

    my $content = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-name'    => 'Cancel',
                    '-program' => PROGRAM_SITE_KRISKRINGLE_HOME(),
                },
            ],
            '-align'       => LINK_RIGHT(),
            '-orientation' => LINK_HORIZONTAL(),
        } );

    return $content;
}

1;

