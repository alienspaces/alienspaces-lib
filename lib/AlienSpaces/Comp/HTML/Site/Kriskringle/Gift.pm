package AlienSpaces::Comp::HTML::Site::Kriskringle::Gift;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:program :comp :util);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_SITE_KRISKRINGLE(),
    ];

    return $util_list;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    my $cgi = $self->utils->{ UTIL_CGI() };

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    my $gift = $kriskringle_util->get_gift();

#<<<
    my $content = $cgi->div({'-class' => 'Gift' },
        $cgi->div({'-class' => 'Gift' },
            $cgi->div({'-class' => 'GiftTitle' }, 'Your Gift') .
            $cgi->div({'-class' => 'GiftLimit' }, 'You should not spend more than about ' . $gift->{'limit'} . ' dollars')
        )
    );
#>>>

    return $self->panel->render(
        {   '-name'        => $self->name(),
            '-content'     => $content,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
}

1;

