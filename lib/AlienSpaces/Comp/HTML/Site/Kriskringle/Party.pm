package AlienSpaces::Comp::HTML::Site::Kriskringle::Party;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:program :comp :util);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_SITE_KRISKRINGLE(),
    ];

    return $util_list;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    my $cgi = $self->utils->{ UTIL_CGI() };

    my $kriskringle_util = $self->utils->{ UTIL_SITE_KRISKRINGLE() };

    my $party = $kriskringle_util->get_party();

#<<<
    my $content = $cgi->div({'-class' => 'Party' },
        $cgi->div({'-class' => 'Where' },
            $cgi->div({'-class' => 'WhereTitle' }, 'Where') .
            $cgi->div({'-class' => 'WherePlaceName' }, $party->{'where'}->{'name'}) .
            $cgi->div({'-class' => 'WherePlaceAddress' }, $party->{'where'}->{'address'})
        ) .
        $cgi->div({'-class' => 'When' },
            $cgi->div({'-class' => 'WhenTitle' }, 'When') .
            $cgi->div({'-class' => 'WhenTime' },  'The gift opening will start at ' . $party->{'when'}->{'time'} . ' on ' . $party->{'when'}->{'date'})
        )
    );
#>>>

    return $self->panel->render(
        {   '-name'        => $self->name(),
            '-content'     => $content,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
}

1;

