package AlienSpaces::Comp::HTML::Site::Kriskringle::Logo;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :panel-styles);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

    ];

    return $util_list;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    my $cgi = $self->utils->{ UTIL_CGI() };

    return $self->panel->render(
        {   '-name'        => $self->name(),
            '-content'     => $cgi->div( { '-class' => 'Logo' }, ' ' ),
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
}

#   action
sub action {
    my ( $self, $args ) = @_;

    #   interface
    my $interface = $args->{'-interface'};

    #   action
    #   - action submitted specifically to this component
    my $action = $args->{'-action'};
    if ( !defined $action ) {
        return COMP_ACTION_FAILURE();
    }

    #   data
    #   - post/get data accompanying the action
    my $data = $args->{'-data'};

    if ( $action eq 'xxx' ) {

        my $xxx = $data->{'xxx'};
        my $xxy = $data->{'xxy'};

        if ($self->xxxx(
                {   '-xxx' => $xxx,
                    '-xxy' => $xxy,
                } )
            ) {
            return COMP_ACTION_SUCCESS();
        }

    }

    return COMP_ACTION_FAILURE();
} ## end sub action

1;

