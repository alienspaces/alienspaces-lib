package AlienSpaces::Comp::HTML::Site::Navigation::BreadCrumb;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :panel-styles :link);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    #   cgi
    my $cgi = $self->cgi;

    #   actions
    my $actions = $args->{'-actions'};
    if ( !defined $actions ) {
        $self->debug( { '-text' => 'Missing -actions, cannot render bread crumb' } );
        return;
    }

    #   component
    my $component = $actions->{ $self->name };
    if ( !defined $component ) {
        $self->debug( { '-text' => 'Missing -component ' . $self->name . ', cannot render bread crumb' } );
        return;
    }

    #   config data
    my $config_data = $component->{'-config_data'};
    if ( !defined $config_data ) {
        $self->error( { '-text' => 'Missing -config_data, cannot render bread crumb' } );
        return;
    }

    #   links
    my $links = $config_data->{'-links'};
    if ( !defined $links || !scalar @{$links} ) {
        $self->error( { '-text' => 'Missing -links, cannot render bread crumb' } );
        return;
    }

    my $total_link_count = scalar @{$links};
    my $link_count       = 1;

    my $breadcrumb_links = [];
    foreach my $link ( @{$links} ) {

        push @{$breadcrumb_links}, $link;

        if ( $link_count++ == $total_link_count ) {
            last;
        }

        #   divider
        push @{$breadcrumb_links},
            {
            '-label_text' => $cgi->span( { '-class' => 'BreadCrumbDivider' }, '->' ),
            '-text_only'  => 1,
            };
    }

    my $content = $self->navigation->render(
        {   '-name'        => $self->name,
            '-links'       => $breadcrumb_links,
            '-orientation' => LINK_HORIZONTAL(),
            '-align'       => LINK_LEFT(),
        } );

    return $content;
} ## end sub render

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #

    return $self->COMP_ACTION_SUCCESS;
}

1;

