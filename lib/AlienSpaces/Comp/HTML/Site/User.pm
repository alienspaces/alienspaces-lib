package AlienSpaces::Comp::HTML::Site::User;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user
    my $user = $user_util->fetch_current_user();

    #   logout href
    my $logout_href = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_LOGOUT } );

    my $content;
    
    if (defined $user) {

#<<<
        $content = 
            $cgi->div( { '-class' => 'yui3-g' },
                $cgi->div( { '-class' => 'yui3-u-1' }, 
                    $cgi->div( { '-class' => 'User' }, 
                        'Hello ' . $user->{'-handle'}
                    ) .
                    $cgi->a( { '-href' => $logout_href, '-title' => $logout_href },
                        'Logout'
                    )
                )
            );
#>>>    
    }
    
    return $self->panel->render(
        {   '-name'        => $self->name,
            '-content'     => $content,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
} ## end sub render

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #

    return $self->COMP_ACTION_SUCCESS;
}

1;

