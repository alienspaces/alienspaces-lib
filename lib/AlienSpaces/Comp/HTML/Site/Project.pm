package AlienSpaces::Comp::HTML::Site::Project;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :panel-styles);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    $self->debug( { '-text' => 'Have -environment ' . $self->dbg_value( $self->environment ) } );

    my $config = [
        {   '-application_url'  => 'http://arena.local',
            '-application_logo' => '/img/site/arena-logo.png',
            '-title_text'       => $phrase_util->text( { '-phrase_id' => '901040' } ),
            '-description_text' => 'Battle your way to glory!',
        },
        {   '-application_url'  => 'http://torvusart.local',
            '-application_logo' => '/img/site/torvus-logo.png',
            '-title_text'       => $phrase_util->text( { '-phrase_id' => '901020' } ),
            '-description_text' => $phrase_util->text( { '-phrase_id' => '901025' } ),
        },
        {   '-application_url'  => 'http://story.local',
            '-application_logo' => '/img/site/stupid-story-logo.gif',
            '-title_text'       => $phrase_util->text( { '-phrase_id' => '901040' } ),
            '-description_text' => $phrase_util->text( { '-phrase_id' => '901045' } ),
        },
    ];

    my $content;
    foreach my $project ( @{$config} ) {
        $content .= $self->render_project($project);
    }

    return $self->panel->render(
        {   '-name'    => $self->name,
            '-content' => $content,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );
} ## end sub render

sub render_project {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   application url
    my $application_url = $args->{'-application_url'};

    #   application logo
    my $application_logo = $args->{'-application_logo'};

    #   title
    my $title_text = $args->{'-title_text'};

    #   description text
    my $description_text = $args->{'-description_text'};

    #   content
    my $content =

        #   logo
        $cgi->div( { '-class' => 'ProjectLogo' }, $cgi->a( { '-href' => $application_url }, $cgi->img( { '-src' => $application_logo, '-alt' => $title_text } ) ) ) .

        #   description
        $cgi->div( { '-class' => 'ProjectDescription' }, $description_text ) .

        #   link
        $cgi->div( { '-class' => 'ProjectLink' }, $cgi->a( { '-href' => $application_url }, $application_url ) );

    return $cgi->div( { '-class' => 'Project' }, $content );
} ## end sub render_project

1;

