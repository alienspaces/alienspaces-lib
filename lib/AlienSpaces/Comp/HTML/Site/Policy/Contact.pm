package AlienSpaces::Comp::HTML::Site::Policy::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program);

our @DYNISA = qw(Comp::HTML::Site::Policy);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $ret = '';

    my $application_url = $self->environment->{'APP_URL'} . PROGRAM_SITE_CONTACT;

    my $content = <<"CONTENT";
All questions and concerns regarding these policies or problems with AlienSpaces applications 
can be submitted through our <a href="$application_url">contact</a> 
form.  
CONTENT

    $ret = $self->panel->render({   
            '-name'   => $self->name,
        '-title'   => 'Contact',
        '-content' => $content,
    });

    return $ret;
}

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #
    if ( defined $action ) {

    }

    return $self->COMP_ACTION_FAILURE;
}

1;

