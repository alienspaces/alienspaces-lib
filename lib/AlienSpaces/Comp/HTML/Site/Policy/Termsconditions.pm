package AlienSpaces::Comp::HTML::Site::Policy::Termsconditions;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Comp::HTML::Site::Policy);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $ret = '';

    my $content = <<'CONTENT';
<p>All AlienSpaces application content, including all written and graphical content, is provided on an as is, 
where is basis. Application development is ongoing with development efforts prioritised on applications
of our choice.</p>
<p>All AlienSpaces applications will be clearly marked with either "Development", "Alpha" or "Beta" status
for applications that are yet to be deemed as production ready. 
</p>
<h3>"Development"</h3>
<ul>
<li>All application content and features may be removed or changed at any time</li>
<li>The application may not remain under long term development and may be removed from public view at any time</li>
<li>User account information and generated user content, if applicable, is not to be seen as permanent.  AlienSpaces 
reserves the right to remove all user account data and generated content prior to the application moving to either 
an "Alpha" or "Beta" status.</li>
</ul>
<h3>"Alpha"</h3>
<ul>
<li>All application content and features may be removed or changed at any time</li>
<li>The application has been deemed as having good potential for longer term development however may still be removed 
from public view at any time</li>
<li>User account information and generated user content, if applicable, is not to be seen as permanent.  AlienSpaces 
reserves the right to remove all user account data and generated content prior to the application moving to a "Beta" 
status</li>
</ul>
<h3>"Beta"</h3>
<ul>
<li>Application features and functionality are stable however larger scale changes to individual features is still to
be expected</li>
<li>We are more confident that the application has long life potential</li>
<li>User account information and generated user content, if applicable, is not to be seen as permanent. AlienSpaces 
reserves the right to remove all user account data and generated content prior to the "Beta" status being removed
from the application. There is however a greater chance at the "Beta" stage of an application that this may not
have to occur.</li>
</ul>
<p>AlienSpaces will not be held responsible for any loss, damage or injury caused by, or related to, 
the use of any information contained AlienSpaces applications at any time and provides no warranty of any kind.</p>
<p>AlienSpaces applications do not wish to be associated with pornography, drug related activities, promotion
of firearms or other dangerous goods, religious, political, sexual or social discrimination or any other 
criminal activity.</p>
<p>Any content uploaded by users that AlienSpaces deems to breach these guidelines will be removed immediately 
with related user accounts immediatley suspended and blocked indefinitely.</p>
CONTENT

    $ret = $self->panel->render({   
            '-name'   => $self->name,
        '-title'   => 'Terms and Conditions Policy',
        '-content' => $content,
    });

    return $ret;
} ## end sub render

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #
    if ( defined $action ) {

    }

    return $self->COMP_ACTION_FAILURE;
}

1;

