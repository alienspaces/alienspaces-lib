package AlienSpaces::Comp::HTML::Site::Policy::Copyright;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Comp::HTML::Site::Policy);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $ret = '';

    my $content = <<'CONTENT';
<p>Copyright &copy; 2010 AlienSpaces. All rights reserved.</p>
<p>AlienSpaces applications may contain information, photographs and graphics that are
protected by Australian copyright laws, trademark or other proprietary rights of 
AlienSpaces including but not limited to product names, logos and designs. The 
compilation, organization and display of the content are the exclusive property of 
AlienSpaces.</p>
<p>Reproduction of any part of an AlienSpaces application in any format requires explicit
written permission from AlienSpaces.</p>
<p>Content uploaded and shared by individual users of AlienSpaces applications remains 
the property of the uploading user and AlienSpaces claims no rights to such material.</p>
CONTENT

    $ret = $self->panel->render({   
            '-name'   => $self->name,
        '-title'   => 'Copyright Policy',
            '-content' => $content,
        } );

    return $ret;
}

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #
    if ( defined $action ) {

    }

    return $self->COMP_ACTION_FAILURE;
}

1;

