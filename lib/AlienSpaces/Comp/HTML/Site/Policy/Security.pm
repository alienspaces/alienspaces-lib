package AlienSpaces::Comp::HTML::Site::Policy::Security;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Comp::HTML::Site::Policy);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $ret = '';

    my $content = <<'CONTENT';
<p>AlienSpaces applications contain secure areas as indicated by a padlock symbol in the browser and 
a https:// web address.</p>
<p>AlienSpaces uses SSL (Secure Sockets Layer) for transmitting private information via the Internet. 
SSL uses a private key to encrypt data that is transferred over the connection. This protocol is a standard 
used by many web sites when you submit confidential information, such as credit card numbers and other personal 
data. SSL creates a secure connection between your browser and our server.</p>
<p>AlienSpaces applications make use of web browser cookies to manage your personal session and preferences 
while using our applications. No information of a personal nature is stored as data in these cookies. Disabling
cookies for AlienSpaces applications will in most cases render the application unusable.</p>
<p>If you have any concerns or enquiries about your security while using our website, please contact
us for more information.</p>
CONTENT

    $ret = $self->panel->render({   
            '-name'   => $self->name,
        '-title'   => 'Security Policy',
        '-content' => $content,
    });

    return $ret;
}

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #
    if ( defined $action ) {

    }

    return $self->COMP_ACTION_FAILURE;
}

1;

