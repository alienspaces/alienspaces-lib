package AlienSpaces::Comp::HTML::Site::Policy::Privacy;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Comp::HTML::Site::Policy);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $ret = '';

    my $content = <<'CONTENT';
<p>We recognize the importance of protecting your privacy.</p>
<p>When visiting and using our applications, information is recorded (such as your browser, date, time and pages accessed) 
for statistical purposes and stricly remains anonymous.</p>
<p>Information you provide through the different AlienSpaces applications such as email contact information is used only 
by AlienSpaces for the function of the application and is not sold or given to third-parties under any circumstances.</p>
CONTENT

    $ret = $self->panel->render({   
            '-name'   => $self->name,
        '-title'   => 'Privacy Policy',
        '-content' => $content,
    });

    return $ret;
}

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #
    if ( defined $action ) {

    }

    return $self->COMP_ACTION_FAILURE;
}

1;

