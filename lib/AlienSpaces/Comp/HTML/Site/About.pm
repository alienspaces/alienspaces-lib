package AlienSpaces::Comp::HTML::Site::About;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :panel-styles);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $content = $phrase_util->text( { '-phrase_id' => 900_000 } );

    my $ret = $self->panel->render(
        {   '-name'        => $self->name,
            '-content'     => $content,
            '-style'       => PANEL_STYLE_BASIC,
            '-show_header' => 0,
            '-show_footer' => 0,
        } );

    return $ret;
}

1;

