package AlienSpaces::Comp::HTML::Adsense::Bottomhorizontal;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $adsense_code_left = <<'ADSENSE';
<script type="text/javascript"><!--
google_ad_client = "pub-8760957171255753";
/* AlienSpaces - Bottom Horizontal */
google_ad_slot = "4951510358";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
ADSENSE

    my $adsense_code_right = <<'ADSENSE';
<script type="text/javascript"><!--
google_ad_client = "pub-8760957171255753";
/* AlienSpaces - Bottom Links */
google_ad_slot = "6871095533";
google_ad_width = 180;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
ADSENSE

    my $content = 
        $cgi_util->div({ '-class' => 'AdSenseBottomLeft' },     
            $adsense_code_left
        ) .
        $cgi_util->div({ '-class' => 'AdSenseBottomRight' },
            $adsense_code_right
        );
                    
    my $ret = $self->panel->render({
            '-name'   => $self->name,
        '-content' => $content,
    });
    
    return $ret;
}

1;
