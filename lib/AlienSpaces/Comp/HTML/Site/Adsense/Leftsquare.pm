package AlienSpaces::Comp::HTML::Adsense::Leftsquare;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $content = <<'ADSENSE';
<script type="text/javascript"><!--
google_ad_client = "pub-8760957171255753";
/* Alienspaces - Left Square */
google_ad_slot = "1928411727";
google_ad_width = 300;
google_ad_height = 250;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
ADSENSE

    my $ret = $self->panel->render(
        {   '-name'    => $self->name,
            '-content' => $content,
        } );

    return $ret;
}

1;
