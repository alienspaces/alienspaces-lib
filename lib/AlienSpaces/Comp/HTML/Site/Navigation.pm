package AlienSpaces::Comp::HTML::Site::Navigation;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :panel-styles :link);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $ret;

    #   logged in
    if ( $user_util->check_user_logged_in() ) {

        $ret = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [

                    #   application
                    {   '-label_text' => $self->environment->{'APP_NAME'},
                        '-text_only'  => 1,
                        '-sub_links'  => {
                            '-links' => [
                                {   '-name'    => 'Home',
                                    '-program' => PROGRAM_SITE_HOME(),
                                },
                                {   '-name'    => 'Contact',
                                    '-program' => PROGRAM_SITE_CONTACT(),
                                },
                            ],
                            '-orientation' => LINK_VERTICAL(),
                            '-align'       => LINK_LEFT(),
                        },
                    },

                    #   user account
                    {   '-label_text' => 'User',
                        '-text_only'  => 1,
                        '-sub_links'  => {
                            '-links' => [
                                {   '-label_text' => 'Account',
                                    '-program'    => PROGRAM_SITE_USER_ACCOUNT(),
                                },
                            ],
                            '-orientation' => LINK_VERTICAL(),
                            '-align'       => LINK_LEFT(),
                        },
                    },

                    #   business account
                    {   '-label_text' => 'Business',
                        '-text_only'  => 1,
                        '-sub_links'  => {
                            '-links' => [
                                {   '-label_text' => 'Account',
                                    '-program'    => PROGRAM_SITE_BUSINESS_ACCOUNT(),
                                },
                                {   '-label_text' => 'User',
                                    '-program'    => PROGRAM_SITE_BUSINESS_USER(),
                                },
                            ],
                            '-orientation' => LINK_VERTICAL(),
                            '-align'       => LINK_LEFT(),
                        },
                    },

                    #   application
                    {   '-label_text' => 'Application',
                        '-text_only'  => 1,
                        '-sub_links'  => {
                            '-links' => [
                                {   '-label_text' => 'User',
                                    '-program'    => PROGRAM_SITE_APPLICATION_USER(),
                                },
                            ],
                            '-orientation' => LINK_VERTICAL(),
                            '-align'       => LINK_LEFT(),
                        },
                    },
                ],
                '-orientation' => LINK_VERTICAL(),
                '-align'       => LINK_LEFT(),
            } );
    } 
    #   not logged in
    else {

        $ret = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-label_text' => $self->environment->{'APP_NAME'},
                        '-text_only'  => 1,
                        '-sub_links'  => {
                            '-links' => [
                                {   '-label_text' => 'Home',
                                    '-program'    => PROGRAM_SITE_HOME(),
                                },
                                {   '-label_text' => 'Contact',
                                    '-program'    => PROGRAM_SITE_CONTACT(),
                                },
                            ],
                            '-orientation' => LINK_VERTICAL(),
                            '-align'       => LINK_LEFT(),
                        },
                    },
                    {   '-label_text' => 'User Account',
                        '-text_only'  => 1,
                        '-sub_links'  => {
                            '-links' => [
                                {   '-label_text' => 'Log In',
                                    '-program'    => PROGRAM_SITE_USER_LOGIN(),
                                },
                                {   '-label_text' => 'Register',
                                    '-program'    => PROGRAM_SITE_USER_REGISTER(),
                                },
                            ],
                            '-orientation' => LINK_VERTICAL(),
                            '-align'       => LINK_LEFT(),
                        },
                    },
                ],
                '-orientation' => LINK_VERTICAL(),
                '-align'       => LINK_LEFT(),
            } );
    } 

    return $ret;
} ## end sub render

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi = $self->utils->{ UTIL_CGI() };

    my $action = delete $args->{'-action'};

    #
    #   handle action
    #

    return $self->COMP_ACTION_SUCCESS;
}

1;

