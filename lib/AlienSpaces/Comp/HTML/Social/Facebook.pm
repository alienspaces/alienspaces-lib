package AlienSpaces::Comp::HTML::Social::Facebook;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :panel-styles);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};

    my $ret = <<'HTML';
<div id="fb-root"> </div>
<script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script>
<fb:like-box 
    width="292" 
    href="http://www.facebook.com/pages/AlienSpaces/237128179658464"
    show_faces="false" 
    border_color="" 
    stream="false" 
    header="true">
</fb:like-box>
HTML

    return $ret;
}

1;

