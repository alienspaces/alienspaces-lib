package AlienSpaces::Comp::HTML::Business::User::Account::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :form :user :table-style :contact-type :message-type :js-confirm);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),

        UTIL_DB_RECORD_R_COUNTRY(),
        UTIL_DB_RECORD_R_BUSINESS_USER_CONTACT_TYPE(),

        UTIL_OUTPUT_HTML_JS_CONFIRM(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   js confirm util
    my $js_confirm_util = $self->utils->{ UTIL_OUTPUT_HTML_JS_CONFIRM() };

    #   business user contact types
    my $business_user_contact_types = $contact_util->fetch_business_user_contact_types( { '-active' => 1 } );
    if ( !defined $business_user_contact_types ) {
        $self->debug( { '-text' => 'No business user contact types active, not rendering' } );
        return;
    }

    #   business user
    my $business_user = $user_util->fetch_current_business_user();
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Failed to fetch current business user' } );
        return;
    }

    #   business user id
    my $business_user_id = $business_user->{'-business_user_id'};

    #   add link
    my $add_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-label_text' => $phrase_util->text( { '-phrase_id' => 82_210 } ),    # phrase_id 82210 - Add
                    '-program' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT_CONTACT_ADD(),
                }
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    #   content
    my $content = $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => 82_200 } ),    # phrase_id 82200 - Business User Contacts
            '-show_content'         => 0,
            '-show_footer'          => 0,
            '-extra_header_content' => $add_link,
        } );

    #   business user contacts
    my $business_user_contacts = $contact_util->fetch_business_user_contacts( { '-business_user_id' => $business_user_id, } );
    if ( !defined $business_user_contacts || !scalar @{$business_user_contacts} ) {
        $self->debug( { '-text' => 'Did not fetch any user contacts for -business_user_id ' . $self->dbg_value($business_user_id) } );
        return $content;
    }

    foreach my $business_user_contact ( @{$business_user_contacts} ) {

        my $data = $self->get_table_data( { '-business_user_contact' => $business_user_contact } );
        if ( !defined $data ) {
            $self->error( { '-text' => 'Failed to get table data' } );
            next;
        }

        #   links
        my $links = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 82_220 } ),    # phrase_id 82220 - Edit
                        '-program'    => PROGRAM_SITE_BUSINESS_USER_ACCOUNT_CONTACT_UPDATE(),
                        '-component'  => COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT_UPDATE(),
                        '-action'     => FORM_ACTION_POPULATE,
                        '-business_user_contact_id' => $business_user_contact->{'-business_user_contact_id'},
                    },
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 82_230 } ),    # phrase_id 82230 - Delete
                        '-program'    => PROGRAM_SITE_BUSINESS_USER_ACCOUNT(),
                        '-component'  => COMP_HTML_BUSINESS_USER_ACCOUNT_CONTACT(),
                        '-action'     => 'delete',
                        '-business_user_contact_id' => $business_user_contact->{'-business_user_contact_id'},
                        '-data'                     => { $js_confirm_util->render( { '-type' => JS_CONFIRM_TYPE_DELETE() } ) },
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        $content .= $self->table->render(
            {   '-name'                 => $self->name . $business_user_contact->{'-business_user_contact_id'},
                '-class'                => TABLE_CLASS_VERTICAL,
                '-show_header'          => 0,
                '-extra_footer_content' => $links,
                '-data'                 => $data,
            } );
    } ## end foreach my $business_user_contact...

    return $content;
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_user_contact'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business_user contact
    my $business_user_contact = $args->{'-business_user_contact'};

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   db country util
    my $db_country_util = $self->utils->{ UTIL_DB_RECORD_R_COUNTRY() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   cgi
    my $cgi = $self->cgi;

    #   country
    my $country = $db_country_util->fetch_one( { '-country_id' => $business_user_contact->{'-country_id'} } );

    #   country text
    my $country_text;
    if ( defined $country ) {
        $country_text = $phrase_util->text( { '-phrase_id' => $country->{'-phrase_id'} } );
    }

    #   business contact type
    my $business_user_contact_type = $contact_util->fetch_one_business_user_contact_type( { '-business_user_contact_type_id' => $business_user_contact->{'-business_user_contact_type_id'}, } );

    #   business contact type text
    my $business_user_contact_type_text;
    if ( defined $business_user_contact_type ) {
        $business_user_contact_type_text = $phrase_util->text( { '-phrase_id' => $business_user_contact_type->{'-phrase_id'}, } );
    }

    my $return_data = [];

    push @{$return_data}, [ $cgi->div( { '-class' => 'HeadingMedium' }, $business_user_contact_type_text ), undef, ];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 82_300 } ), $business_user_contact->{'-name'}, ];    # phrase_id 82300 - Attention

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 82_310 } ), $business_user_contact->{'-address_line_one'}, ];    # phrase_id 82310 - Address Line One

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 82_320 } ), $business_user_contact->{'-address_line_two'}, ];    # phrase_id 82320 - Address Line Two

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 82_330 } ), $country_text, ];                                    # phrase_id 82330 - Country

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 82_340 } ), $business_user_contact->{'-state_province'}, ];      # phrase_id 82340 - State / Province

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 82_350 } ), $business_user_contact->{'-postal_code'}, ];         # phrase_id 82350 - Postal Code

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 82_360 } ), $business_user_contact->{'-phone'}, ];               # phrase_id 82360 - Phone

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   business user
    my $business_user = $user_util->fetch_current_business_user();
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Failed to fetch current business user' } );
        return;
    }

    #   business user id
    my $business_user_id = $business_user->{'-business_user_id'};

    my $action = $args->{'-action'};

    my $data = $args->{'-data'};

    #   business user contact id
    my $business_user_contact_id = $self->html_strip( $data->{'business_user_contact_id'} );

    if ( $action eq 'delete' ) {

        if (!$contact_util->delete_business_user_contact(
                {   '-business_user_contact_id' => $business_user_contact_id,
                    '-business_user_id'         => $business_user_id,
                } )
            ) {
            $self->error( { '-text' => 'Failed to delete -business_user_contact_id ' . $self->dbg_value($business_user_contact_id) } );
            return COMP_ACTION_FAILURE;
        }

        $self->debug( { '-text' => 'Deleted -business_user_contact_id ' . $self->dbg_value($business_user_contact_id) } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 82_370 } ),    # phrase_id 82370 - Business user contact has been deleted
                '-type' => MESSAGE_TYPE_SUCCESS,
            } );

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

__END__
