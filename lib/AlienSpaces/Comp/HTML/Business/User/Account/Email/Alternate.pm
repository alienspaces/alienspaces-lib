package AlienSpaces::Comp::HTML::Business::User::Account::Email::Alternate;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :table-style :request :user :error-codes-user);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_CONTACT(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $data = $self->get_table_data();
    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to fetch account alternate email' } );
        return;
    }

    return $self->table->render(
        {   '-name'  => $self->name,
            '-title' => $phrase_util->text( { '-phrase_id' => 83_300 } ),    # phrase_id 83300 - Business User Alternate Email
            '-class' => TABLE_CLASS_VERTICAL,
            '-data'  => $data,
        } );
}

sub get_table_data {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    my $business_user = $user_util->fetch_current_business_user();
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Failed to fetch current business user' } );
        return;
    }

    my $user = $user_util->fetch_one_user( { '-user_id' => $business_user->{'-user_id'} } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $business_user->{'-user_id'} } );
        return;
    }

    my $return_data = [];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 83_310 } ), $user->{'-alternate_email_address'}, ];

    return $return_data;
}

1;

__END__
