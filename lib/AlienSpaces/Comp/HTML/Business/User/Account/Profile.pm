package AlienSpaces::Comp::HTML::Business::User::Account::Profile;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link :table-style :user :message-type :error-codes-user);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),

        UTIL_DB_RECORD_R_USER_TYPE(),
        UTIL_DB_RECORD_R_BUSINESS_USER_TYPE(),
        UTIL_DB_RECORD_R_USER_STATUS(),
        UTIL_DB_RECORD_R_BUSINESS_USER_STATUS(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $data = $self->get_table_data();
    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to fetch account profile data' } );
        return;
    }

    #   edit link
    my $edit_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-name' => $phrase_util->text( { '-phrase_id' => 81_600 } ),    # phrase_id 81500 - Edit
                    '-program' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT_PROFILE(),
                },
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    return $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => 81_610 } ),    # phrase_id 81510 - Edit
            '-class'                => TABLE_CLASS_VERTICAL,
            '-extra_footer_content' => $edit_link,
            '-data'                 => $data,
        } );
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   db user type util
    my $db_user_type_util = $self->utils->{ UTIL_DB_RECORD_R_USER_TYPE() };

    #   db business user type util
    my $db_business_user_type_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_USER_TYPE() };

    #   db user status util
    my $db_user_status_util = $self->utils->{ UTIL_DB_RECORD_R_USER_STATUS() };

    #   db business user status util
    my $db_business_user_status_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_USER_STATUS() };

    #   business user
    my $business_user = $user_util->fetch_current_business_user();
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Failed to fetch current business user' } );
        return;
    }

    #   check user account admin
    if ( !$user_util->check_user_account_admin_rank( { '-check_user_id' => $business_user->{'-user_id'} } ) ) {
        $self->debug( { '-text' => 'Cannot admin user' } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 81_700 } ),
                '-type' => MESSAGE_TYPE_ERROR,
            } );

        #   set redirect url
        $self->set_redirect_url( { '-url' => PROGRAM_SITE_BUSINESS_USER } );

        return;
    }

    #   user type
    my $user_type = $db_user_type_util->fetch_one( { '-user_type_id' => $business_user->{'-user_type_id'} } );

    #   user type text
    my $user_type_text;
    if ( defined $user_type ) {
        $user_type_text = $phrase_util->text( { '-phrase_id' => $user_type->{'-phrase_id'} } );
    }

    #   business user type
    my $business_user_type = $db_business_user_type_util->fetch_one( { '-business_user_type_id' => $business_user->{'-business_user_type_id'} } );

    #   business user type text
    my $business_user_type_text;
    if ( defined $business_user_type ) {
        $business_user_type_text = $phrase_util->text( { '-phrase_id' => $business_user_type->{'-phrase_id'} } );
    }

    #   user status
    my $user_status = $db_user_status_util->fetch_one( { '-user_status_id' => $business_user->{'-user_status_id'} } );

    #   user status text
    my $user_status_text;
    if ( defined $user_status ) {
        $user_status_text = $phrase_util->text( { '-phrase_id' => $user_status->{'-phrase_id'} } );
    }

    #   business user status
    my $business_user_status = $db_business_user_status_util->fetch_one( { '-business_user_status_id' => $business_user->{'-business_user_status_id'} } );

    #   business user status text
    my $business_user_status_text;
    if ( defined $business_user_status ) {
        $business_user_status_text = $phrase_util->text( { '-phrase_id' => $business_user_status->{'-phrase_id'} } );
    }

    my $return_data = [];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 81_620 } ), $business_user->{'-handle'}, ];    # phrase_id 81520 - Handle

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 81_630 } ), $business_user->{'-first_name'}, ];    # phrase_id 81530 - First Name

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 81_640 } ), $business_user->{'-last_name'}, ];     # phrase_id 81540 - Last Name

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 81_660 } ), $user_type_text, ];                    # phrase_id 81560 - User Type

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 81_670 } ), $business_user_type_text, ];           # phrase_id 81570 - Business User Type

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 81_680 } ), $user_status_text, ];                  # phrase_id 81580 - User Status Text

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 81_690 } ), $business_user_status_text, ];         # phrase_id 81590 - Business User Status Text

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   data
    my $data = $args->{'-data'};

    #   action
    my $action = $args->{'-action'};

    if ( defined $action && $action eq 'view' ) {

        #   business user id
        my $business_user_id = $data->{'business_user_id'};
        if ( !defined $business_user_id ) {
            $self->error( { '-text' => 'Missing -business_user_id, cannot view business user' } );
            return COMP_ACTION_FAILURE;
        }

        #   business user
        my $business_user = $user_util->fetch_one_business_user( { '-business_user_id' => $business_user_id } );
        if ( !defined $business_user ) {
            $self->error( { '-text' => 'Failed to fetch business user for -business_user_id ' . $self->dbg_value($business_user_id) . ', cannot view business user' } );
            return COMP_ACTION_FAILURE;
        }

        #   check user account admin
        if ( !$user_util->check_user_account_admin_rank( { '-check_user_id' => $business_user->{'-user_id'} } ) ) {
            $self->debug( { '-text' => 'Cannot admin user' } );

            #   add message
            $self->add_message(
                {   '-text' => $phrase_util->text( { '-phrase_id' => 81_700 } ),
                    '-type' => MESSAGE_TYPE_ERROR,
                } );

            #   set redirect url
            $self->set_redirect_url( { '-url' => PROGRAM_SITE_BUSINESS_USER } );

            return COMP_ACTION_FAILURE;
        }

        #   set current business user
        if ( !$user_util->set_current_business_user( { '-business_user_id' => $business_user_id } ) ) {
            $self->error( { '-text' => 'Failed to set current business user' } );
            return COMP_ACTION_FAILURE;
        }

        return COMP_ACTION_SUCCESS;
    } ## end if ( defined $action &&...)

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

__END__

