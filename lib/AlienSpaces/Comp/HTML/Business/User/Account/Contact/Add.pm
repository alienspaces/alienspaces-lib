
package AlienSpaces::Comp::HTML::Business::User::Account::Contact::Add;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :business :contact-type :message-type :error-codes-business);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_BUSINESS(),
        UTIL_CONTACT(),

        #   db record
        UTIL_DB_RECORD_R_COUNTRY(),
        UTIL_DB_RECORD_R_BUSINESS_USER_CONTACT_TYPE(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 82_500,    #

        '-fields' => [
            {   '-name'              => 'business_user_contact_type_id',
                '-label_phrase_id'   => 82_510,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_business_user_contact_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'name',
                '-label_phrase_id' => 82_520,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_one',
                '-label_phrase_id' => 82_530,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_two',
                '-label_phrase_id' => 82_540,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'              => 'country_id',
                '-label_phrase_id'   => 82_550,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_country',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'state_province',
                '-label_phrase_id' => 82_560,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'postal_code',
                '-label_phrase_id' => 82_570,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'phone',
                '-label_phrase_id' => 82_580,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
        ],

        #   auto complete
        '-auto_complete' => 0,             #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed phrase if
        '-submit_failed_phrase_id' => 82_590,

        #   submit success phrase id
        '-submit_success_phrase_id' => 82_600,

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, $phrase_util->text( { '-phrase_id' => 82_610 } ), );

    return $content;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   data
    my $data = $args->{'-data'};

    $self->debug( { '-text' => 'Adding new business user contact' } );

    #   business user
    my $business_user = $user_util->fetch_current_business_user();
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Missing current business user, cannot add contact' } );
        return;
    }

    #   check user account admin
    if ( !$user_util->check_user_account_admin_rank( { '-check_user_id' => $business_user->{'-user_id'} } ) ) {
        $self->debug( { '-text' => 'Cannot admin user' } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 82_620 } ),
                '-type' => MESSAGE_TYPE_ERROR,
            } );

        #   set redirect url
        $self->set_redirect_url( { '-url' => PROGRAM_SITE_BUSINESS_USER } );

        return COMP_ACTION_FAILURE;
    }

    #   business user id
    my $business_user_id = $business_user->{'-business_user_id'};

    if ( $self->add_contact( { '-business_user_id' => $business_user_id, '-data' => $data } ) ) {
        return COMP_ACTION_SUCCESS;
    }

    $self->error( { '-text' => 'Failed to add new business user contact' } );

    return COMP_ACTION_FAILURE;
} ## end sub form_action

#   add contact
sub add_contact {
    my ( $self, $args ) = @_;

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   data
    my $data = $args->{'-data'};

    #   business user id
    my $business_user_id = $args->{'-business_user_id'};

    #   html strip
    my $business_user_contact_type_id = $self->html_strip( $data->{'business_user_contact_type_id'} );
    my $name                          = $self->html_strip( $data->{'name'} );
    my $address_line_one              = $self->html_strip( $data->{'address_line_one'} );
    my $address_line_two              = $self->html_strip( $data->{'address_line_two'} );
    my $country_id                    = $self->html_strip( $data->{'country_id'} );
    my $state_province                = $self->html_strip( $data->{'state_province'} );
    my $postal_code                   = $self->html_strip( $data->{'postal_code'} );
    my $phone                         = $self->html_strip( $data->{'phone'} );

    #   contact id
    my $contact_id = $contact_util->insert_business_user_contact(
        {   '-business_user_id'              => $business_user_id,
            '-business_user_contact_type_id' => $business_user_contact_type_id,
            '-name'                          => $name,
            '-address_line_one'              => $address_line_one,
            '-address_line_two'              => $address_line_two,
            '-country_id'                    => $country_id,
            '-state_province'                => $state_province,
            '-postal_code'                   => $postal_code,
            '-phone'                         => $phone,
        } );

    if ( !defined $contact_id ) {
        $self->error( { '-text' => 'Failed to insert business user contact' } );
        return;
    }

    $self->debug( { '-text' => 'Inserted business user contact -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub add_contact

1;

