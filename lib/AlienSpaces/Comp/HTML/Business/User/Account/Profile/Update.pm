package AlienSpaces::Comp::HTML::Business::User::Account::Profile::Update;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :message-type :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_USER(), UTIL_REQUEST(), ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 83_000,

        '-fields' => [
            {   '-name'            => 'handle',
                '-clear'           => 0,
                '-label_phrase_id' => 83_010,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => '1',    #   required
                    '-min_length' => '3',    #   min length
                },
            },
            {   '-name'            => 'first_name',
                '-clear'           => 0,
                '-label_phrase_id' => 83_020,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'            => 'last_name',
                '-clear'           => 0,
                '-label_phrase_id' => 83_030,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'              => 'user_type_id',
                '-clear'             => 0,
                '-label_phrase_id'   => 83_040,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_user_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'              => 'business_user_type_id',
                '-clear'             => 0,
                '-label_phrase_id'   => 83_050,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_business_user_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'              => 'user_status_id',
                '-clear'             => 0,
                '-label_phrase_id'   => 83_060,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_user_status',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'              => 'business_user_status_id',
                '-clear'             => 0,
                '-label_phrase_id'   => 83_070,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_business_user_status',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
        ],

        #   auto complete
        '-auto_complete' => 0,             #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed phrase
        '-submit_failed_text_method' => 'submit_failed_text',

        #   submit success phrase_id
        '-submit_success_phrase_id' => 83_100,

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_BUSINESS_USER_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, $phrase_util->text( { '-phrase_id' => 83_140 } ) );

    return $content;
}

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    my $action = $args->{'-action'};
    my $data   = $args->{'-data'};

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   business user
    my $business_user = $user_util->fetch_current_business_user();
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Failed to fetch current business user, cannot populate form' } );
        return;
    }

    #   check user account admin
    if ( !$user_util->check_user_account_admin_rank( { '-check_user_id' => $business_user->{'-user_id'} } ) ) {
        $self->debug( { '-text' => 'Cannot admin user' } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 83_150 } ),
                '-type' => MESSAGE_TYPE_ERROR,
            } );

        #   set redirect url
        $self->set_redirect_url( { '-url' => PROGRAM_SITE_BUSINESS_USER } );

        return;
    }

    return $business_user;
} ## end sub form_populate

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   data
    my $data = $args->{'-data'};

    $self->debug( { '-text' => 'Update business user profile' } );

    #   html strip
    my $handle                  = $self->html_strip( $data->{'handle'} );
    my $first_name              = $self->html_strip( $data->{'first_name'} );
    my $last_name               = $self->html_strip( $data->{'last_name'} );
    my $user_type_id            = $self->html_strip( $data->{'user_type_id'} );
    my $business_user_type_id   = $self->html_strip( $data->{'business_user_type_id'} );
    my $user_status_id          = $self->html_strip( $data->{'user_status_id'} );
    my $business_user_status_id = $self->html_strip( $data->{'business_user_status_id'} );

    #   business user
    my $business_user = $user_util->fetch_current_business_user();
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Failed to fetch current business user, cannot updatye business user profile' } );
        return COMP_ACTION FAILURE;
    }

    #   check user account admin
    if ( !$user_util->check_user_account_admin_rank( { '-check_user_id' => $business_user->{'-user_id'} } ) ) {
        $self->debug( { '-text' => 'Cannot admin user' } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 83_150 } ),
                '-type' => MESSAGE_TYPE_ERROR,
            } );

        #   set redirect url
        $self->set_redirect_url( { '-url' => PROGRAM_SITE_BUSINESS_USER } );

        return COMP_ACTION_FAILURE;
    }

    #   update user
    if (!$user_util->update_user(
            {   '-user_id'        => $business_user->{'-user_id'},
                '-handle'         => $handle,
                '-first_name'     => $first_name,
                '-last_name'      => $last_name,
                '-user_type_id'   => $user_type_id,
                '-user_status_id' => $user_status_id,
            } )
        ) {

        $self->debug( { '-text' => 'Failed to update user' } );

        return COMP_ACTION_FAILURE;
    }

    #   update business user
    if (!$user_util->update_business_user(
            {   '-business_user_id'        => $business_user->{'-business_user_id'},
                '-business_user_type_id'   => $business_user_type_id,
                '-business_user_status_id' => $business_user_status_id,
            } )
        ) {

        $self->debug( { '-text' => 'Failed to update business user' } );

        return COMP_ACTION_FAILURE;
    }

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   text
    my $text = '';

    #   errors
    my $errors = $self->get_errors();
    if ( defined $errors ) {
        foreach my $error ( @{$errors} ) {
            if ( $error->{'-error_code'} == ERROR_CODE_USER_HANDLE_NOT_UNIQUE ) {

                $text .= $phrase_util->text( { '-phrase_id' => 83_110 } );
            }
            if ( $error->{'-error_code'} == ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE ) {

                $text .= $phrase_util->text( { '-phrase_id' => 83_120 } );
            }
        }
    }
    else {

        $text .= $phrase_util->text( { '-phrase_id' => 83_130 } );
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
} ## end sub submit_failed_text

1;

