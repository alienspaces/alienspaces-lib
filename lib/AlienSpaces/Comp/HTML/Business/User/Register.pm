package AlienSpaces::Comp::HTML::Business::User::Register;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :error-codes-user :contact-type);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

use constant { REQUIRES_CONFIRMATION => 1, };

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_BUSINESS(),
        UTIL_CONTACT(),
        UTIL_REQUEST(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 81_400,

        '-fields' => [
            {   '-name'            => 'business_user_type_id',
                '-label_phrase_id' => 81_410,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_business_user_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'handle',
                '-label_phrase_id' => 81_420,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required'   => '1',    #   required
                    '-min_length' => '3',    #   min length
                },
            },
            {   '-name'            => 'email_address',
                '-label_phrase_id' => 81_430,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => '1',         #   required
                    '-min_length' => '3',         #   min length
                    '-do_match'   => '.+\@.+',    #   must not match this regular expression
                },
            },
            {   '-name'            => 'first_name',
                '-label_phrase_id' => 81_440,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
            },
            {   '-name'            => 'last_name',
                '-label_phrase_id' => 81_450,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
            },
        ],

        #   auto complete
        '-auto_complete' => 0,    #   turn off auto complete, is on by default

        #   submit button
        '-submit_label_phrase_id' => 81_460,

        '-show_reset' => 0,

        #   submit failed phrase
        '-submit_failed_text_method' => 'submit_failed_text',

        #   submit success phrase
        '-submit_success_phrase_id' => 81_490,

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   display after submit success
        '-display_after_submit_success' => '0',

    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_BUSINESS_USER } );

    my $content = $cgi->a( { '-href' => $href }, $phrase_util->text( { '-phrase_id' => 81_500 } ) );

    return $content;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    my $data = $args->{'-data'};

    my $email_address = $self->html_strip( $data->{'email_address'} );
    my $handle        = $self->html_strip( $data->{'handle'} );
    my $first_name    = $self->html_strip( $data->{'first_name'} );
    my $last_name     = $self->html_strip( $data->{'last_name'} );

    $self->debug( { '-text' => 'Attempting to register user' } );

    my $user_id = $user_util->register_business_user(
        {   '-business_id'   => $business_util->business_id,
            '-email_address' => $email_address,
            '-handle'        => $handle,
            '-first_name'    => $first_name,
            '-last_name'     => $last_name,
        } );

    #   user
    my $user = $user_util->fetch_one_business_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $self->dbg_value($user_id) } );
        return COMP_ACTION_FAILURE;
    }

    if (!$request_util->add_request(
            {   '-request_type_id' => REQUEST_TYPE_BUSINESS_USER_REGISTER,
                '-user_id'         => $user_id,
                '-approve_url'     => PROGRAM_SITE_BUSINESS_USER_REGISTER_APPROVE,
                '-deny_url'        => PROGRAM_SITE_BUSINESS_USER_REGISTER_DENY,
                '-request_data'    => [
                    {   '-table_name'  => 'as_user',
                        '-column_name' => 'user_status_id',
                        '-pk_value'    => $user_id,
                        '-new_value'   => USER_STATUS_ACTIVE,
                    } ] } )
        ) {
        $self->error( { '-text' => 'Failed add request for new registered user' } );
        return COMP_ACTION_FAILURE;
    }

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   text
    my $text = '';

    #   phrase args
    my $phrase_args = {};

    my $errors = $self->get_errors();
    if ( !defined $errors ) {
        $self->error( { '-text' => 'No error text' } );
        return $text;
    }

    foreach my $error ( @{$errors} ) {
        if ( $error->{'-error_code'} == ERROR_CODE_USER_HANDLE_NOT_UNIQUE ) {

            $text .= $phrase_util->text( { '-phrase_id' => 81_470, '-args' => $phrase_args } );
        }
        if ( $error->{'-error_code'} == ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE ) {

            $text .= $phrase_util->text( { '-phrase_id' => 81_480, '-args' => $phrase_args } );
        }
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
} ## end sub submit_failed_text

1;

