package AlienSpaces::Comp::HTML::Business::User::Register::Approve;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :user :message-type :error-codes-user);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

use constant { BUSINESS_USER_REGISTER_APPROVE_KEY => '-business_user_register_approve_key', };

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_REQUEST(),
        UTIL_PHRASE(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 81_800,

        '-fields' => [
            {   '-name'            => 'handle',
                '-clear'           => 0,
                '-label_phrase_id' => 81_810,
                '-input_type'      => FORM_INPUT_TYPE_TEXT,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
                '-validate_rules'  => {
                    '-required'   => '1',    #   required
                    '-min_length' => '3',    #   min length
                },
            },
            {   '-name'            => 'email_address',
                '-clear'           => 0,
                '-label_phrase_id' => 81_820,
                '-input_type'      => FORM_INPUT_TYPE_TEXT,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => '1',         #   required
                    '-min_length' => '3',         #   min length
                    '-do_match'   => '.+\@.+',    #   must not match this regular expression
                },
            },

            {   '-name'            => 'first_name',
                '-clear'           => 0,
                '-label_phrase_id' => 81_830,
                '-input_type'      => FORM_INPUT_TYPE_TEXT,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
            },
            {   '-name'            => 'last_name',
                '-clear'           => 0,
                '-label_phrase_id' => 81_840,
                '-input_type'      => FORM_INPUT_TYPE_TEXT,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-values_method'   => undef,
            },
            {   '-name'            => 'password',
                '-clear'           => 1,
                '-label_phrase_id' => 81_850,
                '-input_type'      => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {                           #   Validation rules
                    '-required'   => '1',                         #   required
                    '-min_length' => '6',                         #   min length
                },
            },
            {   '-name'            => 'password_confirm',
                '-clear'           => 1,
                '-label_phrase_id' => 81_860,
                '-input_type'      => FORM_INPUT_TYPE_PASSWORD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_method' => 'validate_password_confirm',
                '-validate_rules'  => {
                    '-required'   => 1,                           #   required
                    '-min_length' => 6,                           #   min length
                },
                '-validate_text_method' => 'validate_password_text_method',
            },
        ],

        #   auto complete
        '-auto_complete' => 0,

        #   submit button
        '-submit_label_phrase_id' => 81_870,

        '-show_reset' => 0,

        #   submit failed phrase_id
        '-submit_failed_phrase_id' => 81_880,

        #   submit success text
        '-submit_success_text_method' => 'submit_success_text',

        #   display after submit failure
        '-display_after_submit_failure' => 0,

        #   display after submit success
        '-display_after_submit_success' => 1,
    };
} ## end sub form_config

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   password request data
    my $request_data = $session_util->get_session( { '-key' => BUSINESS_USER_REGISTER_APPROVE_KEY } );
    if ( !defined $request_data ) {
        $self->error( { '-text' => 'Missing request data, cannot form populate' } );
        return;
    }

    $self->debug( { '-text' => 'Have -request_data ' . $self->dbg_value($request_data) } );

    #   user id
    my $user_id = $request_data->{'-user_id'};
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing user id, cannot form populae' } );
        return;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );

    return $user;
} ## end sub form_populate

sub validate_password_confirm {
    my ( $self, $args ) = @_;

    my $data  = $args->{'-data'};
    my $field = $args->{'-field'};

    my $password         = $data->{'password'};
    my $password_confirm = $data->{'password_confirm'};

    if ( !defined $password_confirm || $password_confirm eq '' ) {
        $self->debug( { '-text' => 'Failed required' } );
        $field->{'-validation_error'} = VALIDATION_ERROR_REQUIRED;
        return;
    }

    if ( !defined $password_confirm || $password_confirm ne $password ) {
        $self->debug( { '-text' => 'Failed match' } );
        return;
    }

    $self->debug( { '-text' => 'Passed validation' } );

    return 1;
}

sub validate_password_text_method {
    my ( $self, $args ) = @_;

    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $field = $args->{'-field'};

    if ( defined $field->{'-validation_error'} && $field->{'-validation_error'} == VALIDATION_ERROR_REQUIRED ) {
        return $phrase_util->text( { '-phrase_id' => DEFAULT_VALIDATION_REQUIRED_PHRASE() } );
    }

    return 'Your passwords do not match';
}

#   action
sub action {
    my ( $self, $args ) = @_;

    my $action = $args->{'-action'};
    my $data   = $args->{'-data'};

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    if ( !defined $action ) {
        return $self->SUPER::action($args);
    }

    #   approve
    #   - action submitted by email, not submitted by the form
    #   - if this action is successful
    if ( $action eq 'approve' ) {

        my $user_id     = $self->html_strip( $data->{'user_id'} );
        my $request_id  = $self->html_strip( $data->{'request_id'} );
        my $request_key = $self->html_strip( $data->{'request_key'} );

        if ( !defined $request_key || !defined $user_id ) {
            return COMP_ACTION_FAILURE;
        }

        #   authenticate request
        #   - we want to see if the request is valid but not actually approve it yet
        #   - we will want to authenticate multiple times throughout this process
        #   - we will call approve_request later on once registration has been completed
        if (!$request_util->authenticate_request(
                {   '-user_id'           => $user_id,
                    '-request_id'        => $request_id,
                    '-request_key'       => $request_key,
                    '-request_status_id' => REQUEST_STATUS_NEW,
                } )
            ) {

            $self->error( { '-text' => 'Failed to authenticate request, cannot change password' } );

            #   delete password request data
            $session_util->delete_session( { '-key' => BUSINESS_USER_REGISTER_APPROVE_KEY } );

            $self->add_program_message(
                {   '-text' => $phrase_util->text( { '-phrase_id' => 81_890 } ),
                    '-type' => MESSAGE_TYPE_ERROR,
                } );

            return COMP_ACTION_FAILURE;
        }

        #   user
        my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
        if ( !defined $user ) {
            $self->error( { '-text' => 'Missing user for -user_id ' . $self->dbg_value($user_id) } );
            return;
        }

        #   authenticated request
        #   - request has been authenticated so it is now okay to display the complete registration form
        my $request_data = {
            '-user_id'     => $user_id,
            '-request_id'  => $request_id,
            '-request_key' => $request_key,
        };

        #   set registration request data
        $session_util->set_session( { '-key' => BUSINESS_USER_REGISTER_APPROVE_KEY, '-value' => $request_data } );

        $self->add_program_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 81_900, '-args' => { '-handle' => $user->{'-handle'} } } ),
                '-type' => MESSAGE_TYPE_SUCCESS,
            } );

        return COMP_ACTION_SUCCESS;
    } ## end if ( $action eq 'approve')

    return $self->SUPER::action($args);
} ## end sub action

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   data
    my $data = $args->{'-data'};

    #   password request data
    my $request_data = $session_util->get_session( { '-key' => BUSINESS_USER_REGISTER_APPROVE_KEY } );
    if ( !defined $request_data ) {
        $self->error( { '-text' => 'Missing request data, cannot reset password' } );
        return COMP_ACTION_FAILURE;
    }

    $self->debug( { '-text' => 'Have -request_data ' . $self->dbg_value($request_data) } );

    #   approve request
    if (!$request_util->approve_request(
            {   '-user_id'     => $request_data->{'-user_id'},
                '-request_id'  => $request_data->{'-request_id'},
                '-request_key' => $request_data->{'-request_key'},
            } )
        ) {

        $self->error( { '-text' => 'Failed to approve request, cannot change password' } );

        #   delete password request data
        $session_util->delete_session( { '-key' => BUSINESS_USER_REGISTER_APPROVE_KEY } );

        return COMP_ACTION_FAILURE;
    }

    $self->debug( { '-text' => 'Request approved' } );

    #   update password
    if ( !$self->update_password( { '-data' => $data, '-user_id' => $request_data->{'-user_id'} } ) ) {
        $self->error( { '-text' => 'Failed to update user password' } );
        return COMP_ACTION_FAILURE;
    }

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

#   update password
sub update_password {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   data
    my $data = $args->{'-data'};

    #   user_id
    my $user_id = $args->{'-user_id'};

    my $password         = $data->{'password'};
    my $password_confirm = $data->{'password_confirm'};

    #   html strip
    $password         = $self->html_strip($password);
    $password_confirm = $self->html_strip($password_confirm);

    if ( !defined $password || !defined $password_confirm || ( $password ne $password_confirm ) ) {
        $self->error(
            { '-text' => 'Missing -password ' . $self->dbg_value($password) . ' or -password_confirm ' . $self->dbg_value($password_confirm) . ' or they dont match, cannot update user password' } );
        return;
    }

    $self->debug( { '-text' => 'Updating user password' } );

    #   update
    if (!$user_util->update_user_password(
            {   '-user_id'  => $user_id,
                '-password' => $password,
            } )
        ) {
        $self->error( { '-text' => 'Failed to update user password' } );
        return;
    }

    $self->debug( { '-text' => 'Password updated' } );

    return 1;
} ## end sub update_password

#   render form
#   - override method to control suppressing the form after
#     they have successfully reset their password
sub render_form {
    my ( $self, $args ) = @_;

    #   action
    my $action = $args->{'-action'};

    #   status
    my $status = $args->{'-status'};

    #   if we have the default form action and submission
    #   has been successful then we wont render the form
    if (   ( defined $action && $action eq 'submit-form' )
        && ( defined $status && $status == COMP_ACTION_SUCCESS ) ) {
        $self->debug( { '-text' => 'User changed password, not rendering form' } );
        return;
    }

    return $self->SUPER::render_form($args);
}

#   submit success text
#   - provide different success messages for initially approving
#     the request and then actually resetting the password
sub submit_success_text {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   password reset request data
    my $request_data = $session_util->get_session( { '-key' => BUSINESS_USER_REGISTER_APPROVE_KEY } );
    if ( !defined $request_data ) {
        $self->error( { '-text' => 'Have no password reset request data' } );
        return;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $request_data->{'-user_id'} } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Missing user for -user_id ' . $self->dbg_value( $request_data->{'-user_id'} ) } );
        return;
    }

    #   text
    my $text;

    #   action
    my $action = $args->{'-action'};

    #   about to enter in a new password
    if ($request_util->authenticate_request(
            {   '-user_id'           => $request_data->{'-user_id'},
                '-request_id'        => $request_data->{'-request_id'},
                '-request_key'       => $request_data->{'-request_key'},
                '-request_status_id' => REQUEST_STATUS_NEW,
            } )
        ) {

        #   request authenticated
        $text = $phrase_util->text( { '-phrase_id' => 77_150, '-args' => { '-handle' => $user->{'-handle'} } } );
    }

    #   password has been changed
    elsif (
        $request_util->authenticate_request(
            {   '-user_id'           => $request_data->{'-user_id'},
                '-request_id'        => $request_data->{'-request_id'},
                '-request_key'       => $request_data->{'-request_key'},
                '-request_status_id' => REQUEST_STATUS_APPROVED,
            } )
        ) {

        #   request authenticated
        $text = $phrase_util->text( { '-phrase_id' => 77_160, '-args' => { '-handle' => $user->{'-handle'} } } );
    }

    return $text;
} ## end sub submit_success_text

1;
