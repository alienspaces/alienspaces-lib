package AlienSpaces::Comp::HTML::Business::User::Register::Deny;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :request :message-type :error-codes-user);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_REQUEST(),
        UTIL_PHRASE(),
    ];
}

sub action {
    my ( $self, $args ) = @_;

    my $action = $args->{'-action'};
    my $data   = $args->{'-data'};

    #   request util
    my $request_util = $self->utils->{ UTIL_REQUEST() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    if ( $action eq 'deny' ) {

        my $user_id     = $self->html_strip( $data->{'user_id'} );
        my $request_id  = $self->html_strip( $data->{'request_id'} );
        my $request_key = $self->html_strip( $data->{'request_key'} );

        if ( !defined $request_key || !defined $user_id ) {
            $self->debug( { '-text' => 'Missing some -data ' . $self->dbg_value($data) . ', cannot deny request' } );
            return COMP_ACTION_FAILURE;
        }

        #   user
        my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
        if ( !defined $user ) {
            $self->error( { '-text' => 'Missing user for -user_id ' . $self->dbg_value($user_id) } );
            return COMP_ACTION_FAILURE;
        }

        #   deny / report request
        if ($request_util->deny_request(
                {   '-user_id'     => $user_id,
                    '-request_id'  => $request_id,
                    '-request_key' => $request_key
                } )
            ) {

            $self->debug( { '-text' => 'Request -request_id ' . $self->dbg_value($request_id) . ' deny / report success' } );

            #   message
            $self->add_program_message(
                {   '-text' => $phrase_util->text( { '-phrase_id' => 82_000 } ),
                    '-type' => MESSAGE_TYPE_SUCCESS,
                } );

            return COMP_ACTION_SUCCESS;
        }

        $self->debug( { '-text' => 'Request -request_id ' . $self->dbg_value($request_id) . ' deny / report failed' } );
    } ## end if ( $action eq 'deny')

    #   message
    $self->add_program_message(
        {   '-text' => $phrase_util->text( { '-phrase_id' => 82_010 } ),
            '-type' => MESSAGE_TYPE_ERROR,
        } );

    return COMP_ACTION_FAILURE;
} ## end sub action

1;
