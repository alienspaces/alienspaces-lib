package AlienSpaces::Comp::HTML::Business::Account::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :link :table-style :business :contact-type :message-type :js-confirm);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_BUSINESS(),
        UTIL_CONTACT(),
        UTIL_DB_RECORD_R_COUNTRY(),
        UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE(),

        UTIL_OUTPUT_HTML_JS_CONFIRM(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   js confirm util
    my $js_confirm_util = $self->utils->{ UTIL_OUTPUT_HTML_JS_CONFIRM() };

    #   business
    my $business = $business_util->fetch_current_business();
    if ( !defined $business ) {
        $self->debug( { '-text' => 'No current business, not rendering' } );
        return;
    }

    #   contact types
    my $business_contact_types = $contact_util->fetch_business_contact_types( { '-active' => 1 } );
    if ( !defined $business_contact_types ) {
        $self->debug( { '-text' => 'No business contact types active, not rendering' } );
        return;
    }

    #   add link
    my $add_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-label_text' => $phrase_util->text( { '-phrase_id' => 80_610 } ),    # phrase_id 80610 - Add
                    '-program' => PROGRAM_SITE_BUSINESS_ACCOUNT_CONTACT_ADD(),
                }
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    #   content
    my $content = $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $phrase_util->text( { '-phrase_id' => 80_600 } ),    # phrase_id 80600 - Business Account Contact
            '-show_content'         => 0,
            '-show_footer'          => 0,
            '-extra_header_content' => $add_link,
        } );

    #   business contacts
    my $business_contacts = $contact_util->fetch_business_contacts( { '-business_id' => $business_util->business_id, } );
    if ( !defined $business_contacts || !scalar @{$business_contacts} ) {
        $self->debug( { '-text' => 'Did not fetch any business contacts' } );
        return $content;
    }

    foreach my $business_contact ( @{$business_contacts} ) {

        my $data = $self->get_table_data( { '-business_contact' => $business_contact } );
        if ( !defined $data ) {
            $self->error( { '-text' => 'Failed to get table data' } );
            next;
        }

        #   links
        my $links = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 80_620 } ),    # phrase_id 80620 - Edit
                        '-program'    => PROGRAM_SITE_BUSINESS_ACCOUNT_CONTACT_UPDATE(),
                        '-component'  => COMP_HTML_BUSINESS_ACCOUNT_CONTACT_UPDATE(),
                        '-action'     => FORM_ACTION_POPULATE,
                        '-business_contact_id' => $business_contact->{'-business_contact_id'},
                    },
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 80_630 } ),    # phrase_id 80630 - Delete
                        '-program'    => PROGRAM_SITE_BUSINESS_ACCOUNT(),
                        '-component'  => COMP_HTML_BUSINESS_ACCOUNT_CONTACT(),
                        '-action'     => 'delete',
                        '-business_contact_id' => $business_contact->{'-business_contact_id'},
                        '-data'                => { $js_confirm_util->render( { '-type' => JS_CONFIRM_TYPE_DELETE() } ) },
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        $content .= $self->table->render(
            {   '-name'                 => $self->name . $business_contact->{'-business_contact_id'},
                '-class'                => TABLE_CLASS_VERTICAL,
                '-show_header'          => 0,
                '-extra_footer_content' => $links,
                '-data'                 => $data,
            } );
    } ## end foreach my $business_contact...

    return $content;
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_contact'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business contact
    my $business_contact = $args->{'-business_contact'};

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   db country util
    my $db_country_util = $self->utils->{ UTIL_DB_RECORD_R_COUNTRY() };

    #   db business contact type util
    my $db_business_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE() };

    #   cgi
    my $cgi = $self->cgi;

    #   country
    my $country = $db_country_util->fetch_one( { '-country_id' => $business_contact->{'-country_id'} } );

    #   country text
    my $country_text;
    if ( defined $country ) {
        $country_text = $phrase_util->text( { '-phrase_id' => $country->{'-phrase_id'} } );
    }

    #   business contact type
    my $business_contact_type = $db_business_contact_type_util->fetch_one( { '-business_contact_type_id' => $business_contact->{'-business_contact_type_id'} } );

    #   business contact type text
    my $business_contact_type_text;
    if ( defined $business_contact_type ) {
        $business_contact_type_text = $phrase_util->text( { '-phrase_id' => $business_contact_type->{'-phrase_id'} } );
    }

    my $return_data = [];

    push @{$return_data}, [ $cgi->div( { '-class' => 'HeadingMedium' }, $business_contact_type_text ), undef, ];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_640 } ), $business_contact->{'-name'}, ];    # phrase_id 80640 - Attention

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_650 } ), $business_contact->{'-address_line_one'}, ];    # phrase_id 80650 - Address Line One

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_660 } ), $business_contact->{'-address_line_two'}, ];    # phrase_id 80660 - Address Line Two

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_670 } ), $country_text, ];                               # phrase_id 80670 - Country

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_680 } ), $business_contact->{'-state_province'}, ];      # phrase_id 80680 - State / Province

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_690 } ), $business_contact->{'-postal_code'}, ];         # phrase_id 80690 - Postal Code

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_700 } ), $business_contact->{'-phone'}, ];               # phrase_id 80700 - Phone

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $action = $args->{'-action'};

    my $data = $args->{'-data'};

    #   business contact id
    my $business_contact_id = $self->html_strip( $data->{'business_contact_id'} );

    if ( $action eq 'delete' ) {

        if ( !$contact_util->delete_business_contact( { '-business_contact_id' => $business_contact_id, '-business_id' => $business_util->business_id } ) ) {
            $self->error( { '-text' => 'Failed to delete -business_contact_id ' . $self->dbg_value($business_contact_id) } );
            return COMP_ACTION_FAILURE;
        }

        $self->debug( { '-text' => 'Deleted -business_contact_id ' . $self->dbg_value($business_contact_id) } );

        #   add message
        $self->add_message(
            {   '-text' => $phrase_util->text( { '-phrase_id' => 80_710 } ),    # phrase_id 80710 - Business user contact has been deleted
                '-type' => MESSAGE_TYPE_SUCCESS,
            } );

        return COMP_ACTION_SUCCESS;
    }

    return COMP_ACTION_FAILURE;
} ## end sub action

1;

__END__
