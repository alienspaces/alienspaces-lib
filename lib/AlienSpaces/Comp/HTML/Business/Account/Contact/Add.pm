package AlienSpaces::Comp::HTML::Business::Account::Contact::Add;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :business :contact-type :error-codes-business);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_BUSINESS(),
        UTIL_CONTACT(),

        #   db record
        UTIL_DB_RECORD_R_COUNTRY(),
        UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE(),
    ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 81_000,

        '-fields' => [
            {   '-name'              => 'business_contact_type_id',
                '-label_phrase_id'   => 81_010,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_business_contact_type',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'name',
                '-label_phrase_id' => 81_020,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_one',
                '-label_phrase_id' => 81_030,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'address_line_two',
                '-label_phrase_id' => 81_040,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
            {   '-name'              => 'country_id',
                '-label_phrase_id'   => 81_050,
                '-input_type'        => FORM_INPUT_TYPE_LIST,
                '-data_type'         => FORM_DATA_TYPE_NUMBER,
                '-values_table_name' => 'as_r_country',
                '-validate_rules'    => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'state_province',
                '-label_phrase_id' => 81_060,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'postal_code',
                '-label_phrase_id' => 81_070,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required' => '1',    #   required
                },
            },
            {   '-name'            => 'phone',
                '-label_phrase_id' => 81_080,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
            },
        ],

        #   auto complete
        '-auto_complete' => 0,             #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed phrase id
        '-submit_failed_phrase_id' => 81_090,

        #   submit success phrase id
        '-submit_success_phrase_id' => 81_100,

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_BUSINESS_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_BUSINESS_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, $phrase_util->text( { '-phrase_id' => 81_110 } ), );

    return $content;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   data
    my $data = $args->{'-data'};

    $self->error( { '-text' => 'Adding new business contact' } );

    if ( $self->add_contact( { '-data' => $data } ) ) {
        return COMP_ACTION_SUCCESS;
    }

    $self->error( { '-text' => 'Failed to add new business contact' } );

    return COMP_ACTION_FAILURE;
}

#   add contact
sub add_contact {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    #   data
    my $data = $args->{'-data'};

    my $business_contact_type_id = $data->{'business_contact_type_id'};
    my $name                     = $data->{'name'};
    my $address_line_one         = $data->{'address_line_one'};
    my $address_line_two         = $data->{'address_line_two'};
    my $country_id               = $data->{'country_id'};
    my $state_province           = $data->{'state_province'};
    my $postal_code              = $data->{'postal_code'};
    my $phone                    = $data->{'phone'};

    #   html strip
    $business_contact_type_id = $self->html_strip($business_contact_type_id);
    $name                     = $self->html_strip($name);
    $address_line_one         = $self->html_strip($address_line_one);
    $address_line_two         = $self->html_strip($address_line_two);
    $country_id               = $self->html_strip($country_id);
    $state_province           = $self->html_strip($state_province);
    $postal_code              = $self->html_strip($postal_code);
    $phone                    = $self->html_strip($phone);

    #   contact id
    my $contact_id = $contact_util->insert_business_contact(
        {   '-business_id'              => $business_util->business_id,
            '-business_contact_type_id' => $business_contact_type_id,
            '-name'                     => $name,
            '-address_line_one'         => $address_line_one,
            '-address_line_two'         => $address_line_two,
            '-country_id'               => $country_id,
            '-state_province'           => $state_province,
            '-postal_code'              => $postal_code,
            '-phone'                    => $phone,
        } );

    if ( !defined $contact_id ) {
        $self->error( { '-text' => 'Failed to insert contact' } );
        return;
    }

    $self->debug( { '-text' => 'Inserted -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub add_contact

1;

