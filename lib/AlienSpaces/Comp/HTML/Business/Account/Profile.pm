package AlienSpaces::Comp::HTML::Business::Account::Profile;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :link :table-style :business :error-codes-business);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_BUSINESS(),
    ];
}

sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   content
    my $content;

    my $data = $self->get_table_data();
    if ( !defined $data ) {

        #   add link
        my $add_link = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-name'    => $phrase_util->text( { '-phrase_id' => 80_010 } ),
                        '-program' => PROGRAM_SITE_BUSINESS_ACCOUNT_PROFILE_ADD(),
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        $content = $self->table->render(
            {   '-name'                 => $self->name,
                '-title'                => $phrase_util->text( { '-phrase_id' => 80_000 } ),
                '-show_content'         => 0,
                '-show_footer'          => 0,
                '-extra_header_content' => $add_link,
            } );

    }
    else {

        #   edit link
        my $edit_link = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-name'    => $phrase_util->text( { '-phrase_id' => 80_020 } ),
                        '-program' => PROGRAM_SITE_BUSINESS_ACCOUNT_PROFILE_UPDATE(),
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        $content = $self->table->render(
            {   '-name'                 => $self->name,
                '-title'                => $phrase_util->text( { '-phrase_id' => 80_000 } ),
                '-class'                => TABLE_CLASS_VERTICAL,
                '-extra_footer_content' => $edit_link,
                '-data'                 => $data,
            } );
    }

    return $content;
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   business
    my $business = $business_util->fetch_current_business();
    if ( !defined $business ) {
        $self->debug( { '-text' => 'No current business' } );
        return;
    }

    my $return_data = [];

    push @{$return_data}, [ $phrase_util->text( { '-phrase_id' => 80_030 } ), $business->{'-name'}, ];

    return $return_data;
}

1;

__END__
