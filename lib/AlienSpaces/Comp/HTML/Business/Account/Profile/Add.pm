package AlienSpaces::Comp::HTML::Business::Account::Profile::Add;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :form :comp :request :business :error-codes-business);

our @DYNISA = qw(Comp::HTML::Form::Basic);

our $VERSION = (qw$Revision: $)[1];

#   util object load list
sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_BUSINESS(), UTIL_REQUEST(), ];
}

#   form config
sub form_config {
    my ( $self, $args ) = @_;

    return {

        #   header phrase id
        '-header_phrase_id' => 80_200,

        '-fields' => [
            {   '-name'            => 'name',
                '-label_phrase_id' => 80_210,
                '-input_type'      => FORM_INPUT_TYPE_TEXTFIELD,
                '-data_type'       => FORM_DATA_TYPE_TEXT,
                '-validate_rules'  => {
                    '-required'   => '1',    #   required
                    '-min_length' => '3',    #   min length
                },
            },
        ],

        #   auto complete
        '-auto_complete' => 0,    #   turn off auto complete, is on by default

        '-show_reset' => 0,

        #   submit failed phrase
        '-submit_failed_text_method' => 'submit_failed_text',

        #   submit success text
        '-submit_success_text' => 'Business account created',

        #   display after submit failure
        '-display_after_submit_failure' => '1',

        #   redirect after submit success
        '-redirect_after_submit_success_url' => PROGRAM_SITE_BUSINESS_ACCOUNT,
    };
} ## end sub form_config

#   render extra footer content
sub render_extra_footer_content {
    my ( $self, $args ) = @_;

    #   cgi
    my $cgi = $self->cgi;

    my $href = $self->make_program_link( { '-program' => PROGRAM_SITE_BUSINESS_ACCOUNT } );

    my $content = $cgi->a( { '-href' => $href }, 'Return to account' );

    return $content;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super failed init' } );
        return;
    }

    #   registered status
    $self->{'-registered_status'} = undef;

    return 1;
}

#   form populate
sub form_populate {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   business
    my $business = $business_util->fetch_business( { '-business_id' => $business_util->business_id } );

    return $business;
}

#   form action
sub form_action {
    my ( $self, $args ) = @_;

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    my $data = $args->{'-data'};

    my $name = $data->{'name'};

    $self->debug( { '-text' => 'Inserting business' } );

    #   html strip
    $name = $self->html_strip($name);

    my $business_id = $business_util->insert_business( { '-name' => $name, } );
    if ( !defined $business_id ) {
        $self->error( { '-text' => 'Failed to insert business' } );
        return COMP_ACTION_FAILURE;
    }

    my $business_user_id = $user_util->insert_business_user(
        {   '-business_id' => $business_id,
            '-user_id'     => $user_util->user_id,
        } );
    if ( !defined $business_user_id ) {
        $self->error( { '-text' => 'Failed to insert business user' } );
        return COMP_ACTION_FAILURE;
    }

    if ( !$business_util->set_business_session_values( { '-business_id' => $business_id } ) ) {
        $self->error( { '-text' => 'Failed to set business session values' } );
        return COMP_ACTION_FAILURE;
    }

    return COMP_ACTION_SUCCESS;
} ## end sub form_action

#   submit failed text
sub submit_failed_text {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   text
    my $text = '';

    #   errors
    my $errors = $self->get_errors();
    if ( defined $errors ) {
        foreach my $error ( @{$errors} ) {

        }
    }
    else {

        #   There has been a problem registering your account. Please try again shortly.
        #   If the problem persists please contact <ENV:APP_SYSTEM_SUPPORT_EMAIL>, Thankyou!
        $text .= $phrase_util->text( { '-phrase_id' => '20130' } );
    }

    $self->debug( { '-text' => 'Have -errors ' . $self->dbg_value($errors) } );

    return $text;
}

1;

