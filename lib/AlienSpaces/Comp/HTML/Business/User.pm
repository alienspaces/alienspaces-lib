package AlienSpaces::Comp::HTML::Business::User;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   HTML component template class
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :comp :link);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {

};

#   util list
sub util_list {
    my ( $self, $args ) = @_;

    return [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_BUSINESS(),
        UTIL_USER(),

        UTIL_DB_RECORD_R_USER_TYPE(),
        UTIL_DB_RECORD_R_BUSINESS_USER_TYPE(),
        UTIL_DB_RECORD_R_USER_STATUS(),
        UTIL_DB_RECORD_R_BUSINESS_USER_STATUS(),
    ];
}

#   render
sub render {
    my ( $self, $args ) = @_;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    my $data = $self->get_table_data();

    my $column_titles;
    if ( defined $data && scalar @{$data} ) {
        $column_titles = [
            $phrase_util->text( { '-phrase_id' => 80_850 } ),    # phrase_id 80850 - Name
            $phrase_util->text( { '-phrase_id' => 80_840 } ),    # phrase_id 80840 - Email
            $phrase_util->text( { '-phrase_id' => 80_870 } ),    # phrase_id 80870 - Business User Type
            $phrase_util->text( { '-phrase_id' => 80_890 } ),    # phrase_id 80890 - Business User Status
            undef,
        ];
    }

    #   add link
    my $add_link = $self->navigation->render(
        {   '-name'  => $self->name,
            '-links' => [
                {   '-label_text' => $phrase_util->text( { '-phrase_id' => 80_810 } ),    # phrase_id 80810 - Add
                    '-program' => PROGRAM_SITE_BUSINESS_USER_REGISTER(),
                }
            ],
            '-align'       => LINK_ALIGN_RIGHT,
            '-orientation' => LINK_ORIENTATION_HORIZONTAL,
        } );

    #   title
    my $title = $phrase_util->text( { '-phrase_id' => 80_800 } );   #   phrase_id 80800 - Business Users

    #   content
    my $content = $self->table->render(
        {   '-name'                 => $self->name,
            '-title'                => $title,
            '-extra_header_content' => $add_link,
            '-column_titles'        => $column_titles,
            '-data'                 => $data,
        } );

    return $content;
} ## end sub render

sub get_table_data {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   db user type util
    my $db_user_type_util = $self->utils->{ UTIL_DB_RECORD_R_USER_TYPE() };

    #   db business user type util
    my $db_business_user_type_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_USER_TYPE() };

    #   db user status util
    my $db_user_status_util = $self->utils->{ UTIL_DB_RECORD_R_USER_STATUS() };

    #   db business user status util
    my $db_business_user_status_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_USER_STATUS() };

    my $business_users = $user_util->fetch_business_user(
        {   '-business_id' => $business_util->business_id,
            '-where'       => {
                '-clause' => 'user_id != ?',
                '-bind'   => [ $user_util->user_id ],
            },
        } );
    if ( !defined $business_users ) {
        $self->debug( { '-text' => 'No business users found for -business_id ' . $self->dbg_value( $business_util->business_id ) } );
        return;
    }

    my $return_data = [];

BUSINESS_USERS:
    foreach my $user ( @{$business_users} ) {

        #   check user account admin
        if ( !$user_util->check_user_account_admin_rank( { '-check_user_id' => $user->{'-user_id'} } ) ) {
            $self->debug( { '-text' => 'Cannot admin user' } );
            next BUSINESS_USERS;
        }

        #   name
        my $name = ( defined $user->{'-first_name'} ? $user->{'-first_name'} : '' ) . ( defined $user->{'-last_name'} ? ' ' . $user->{'-last_name'} : '' );

        #   user type
        my $user_type = $db_user_type_util->fetch_one( { '-user_type_id' => $user->{'-user_type_id'} } );

        #   user type text
        my $user_type_text;
        if ( defined $user_type ) {
            $user_type_text = $phrase_util->text( { '-phrase_id' => $user_type->{'-phrase_id'} } );
        }

        #   business user type
        my $business_user_type = $db_business_user_type_util->fetch_one( { '-business_user_type_id' => $user->{'-business_user_type_id'} } );

        #   business user type text
        my $business_user_type_text;
        if ( defined $business_user_type ) {
            $business_user_type_text = $phrase_util->text( { '-phrase_id' => $business_user_type->{'-phrase_id'} } );
        }

        #   user status
        my $user_status = $db_user_status_util->fetch_one( { '-user_status_id' => $user->{'-user_status_id'} } );

        #   user status text
        my $user_status_text;
        if ( defined $user_status ) {
            $user_status_text = $phrase_util->text( { '-phrase_id' => $user_status->{'-phrase_id'} } );
        }

        #   business user status
        my $business_user_status = $db_business_user_status_util->fetch_one( { '-business_user_status_id' => $user->{'-business_user_status_id'} } );

        #   business user status text
        my $business_user_status_text;
        if ( defined $business_user_status ) {
            $business_user_status_text = $phrase_util->text( { '-phrase_id' => $business_user_status->{'-phrase_id'} } );
        }

        #   view link
        my $view_link = $self->navigation->render(
            {   '-name'  => $self->name,
                '-links' => [
                    {   '-label_text' => $phrase_util->text( { '-phrase_id' => 80_820 } ),    # phrase_id 80820 - View
                        '-program'    => PROGRAM_SITE_BUSINESS_USER_ACCOUNT(),
                        '-component'  => COMP_HTML_BUSINESS_USER_ACCOUNT_PROFILE(),
                        '-action'     => 'view',
                        '-business_user_id' => $user->{'-business_user_id'},
                    }
                ],
                '-align'       => LINK_ALIGN_RIGHT,
                '-orientation' => LINK_ORIENTATION_HORIZONTAL,
            } );

        push @{$return_data}, [

            #   name
            $name,

            #   email address
            $user->{'-email_address'},

            #   business user type text
            $business_user_type_text,

            #   business user status text
            $business_user_status_text,

            #   view
            $view_link,
        ];
    } ## end BUSINESS_USERS: foreach my $business_user (...)

    return $return_data;
} ## end sub get_table_data

sub action {
    my ( $self, $args ) = @_;

    return COMP_ACTION_FAILURE();
}

1;

