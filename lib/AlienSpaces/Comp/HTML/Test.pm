package AlienSpaces::Comp::HTML::Test;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   HTML specific test component which purposefully does not support
#   any other interface other than HTML
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :program :message-types);

our @DYNISA = qw(Comp);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {
    'data' => {'-type' => 's' },
};
    
sub render {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   title
    my $title = 'Test component header';
    
    #   content    
    my $content = $cgi_util->div({}, 'Have -args -key ' . $self->dbg_value($args) );

    #   href
    my $href = $self->make_component_link({'-component' => $self->name, '-action' => 'test'});
    
    #   link
    $content .= $cgi_util->div({}, $cgi_util->a({'-href' => $href}, 'Test action') );

    #   message
#    my $messages = $self->get_messages();
    
#    $content .= $cgi_util->div({}, 'Have -messages ' . $self->dbg_value($messages) );
                       
    return $self->panel->render({
        '-name'    => $self->name,
        '-title'   => $title,
        '-content' => $content,
    });
}

sub action {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   action
    my $action = $args->{'-action'};

    #   data
    my $data   = $args->{'-data'};
    
    #   handle action
    if ($action eq 'test') {
    
    
    }
    
    #   success message
    $self->add_message({'-text' => 'Here is a success message', '-type' => MESSAGE_TYPE_SUCCESS });

    #   error message
    $self->add_message({'-text' => 'Here is an error message', '-type' => MESSAGE_TYPE_ERROR });

    #   info message
    $self->add_message({'-text' => 'Here is an info message', '-type' => MESSAGE_TYPE_INFO });
    
    return $self->COMP_ACTION_SUCCESS;
}

1;

