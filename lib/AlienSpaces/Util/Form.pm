package AlienSpaces::Util::Form;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use Carp qw (carp croak );
use English qw(-no_match_vars);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{$self->SUPER::util_list},
        UTIL_CGI(),
    ];

    return $util_list;
}

#
#   form methods
#
sub validate_fields {
    my ($self, $args) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};
        
    my $valid = 1;    
    foreach my $field_name (keys %{$args->{'-fields'}}) {
        my $field_label  = $args->{'-fields'}->{$field_name}->{'-label'} || $field_name;
        my $field_config = $args->{'-fields'}->{$field_name};
        
        my $field_value  = $cgi_util->param( -name => $field_name );
        
        #
        #   check required
        #    
        if (($field_config->{'-required'} == 1) && (length $field_value == 0)) {
            $args->{'-fields'}->{$field_name}->{'-valid'}   = 0;
            if (defined $field_config->{'-message'}) {
                $args->{'-fields'}->{$field_name}->{'-message'} = $field_config->{'-message'};
            } else  {
                $args->{'-fields'}->{$field_name}->{'-message'} = "Field '$field_label' is required";
            }
            $self->debug({ -text => "Field $field_name failed required validation"});
            $valid = 0;
            next;
        }
        
        if (defined $field_config->{'-match_field'}) {
            my $match_field_value = $cgi_util->param( -name => $field_config->{'-match_field'});
            if ($field_value ne $match_field_value) {
                $self->debug({ -text => "Field $field_name failed field match " . $field_config->{'-match_field'} . " $match_field_value"});
                $args->{'-fields'}->{$field_name}->{'-valid'} = 0;
                $args->{'-fields'}->{$field_name}->{'-message'} = "Field '$field_label' does not match the field '" . $field_config->{'-match_field'} . "'";
                $valid = 0;
                next;
            }
        }

        if (defined $field_config->{'-match_exp'}) {
            my $match_exp = $field_config->{'-match_exp'};
            if ($field_value !~ /$match_exp/g) {
                $self->debug({ -text => "Field $field_name failed regular expression validation"});
                $args->{'-fields'}->{$field_name}->{'-valid'} = 0;
                
                if (defined $field_config->{'-message'}) {
                    $args->{'-fields'}->{$field_name}->{'-message'} = $field_config->{'-message'};
                } else  {
                    $args->{'-fields'}->{$field_name}->{'-message'} = "Field '$field_label' does not match the expression " . $field_config->{'-match_exp'};
                }                    
                $valid = 0;
                next;
            }
        }

        if (defined $field_config->{'-match_value'}) {
            $self->debug({ -text => "Match value is [" . $field_config->{'-match_value'} . "]"});
            my $match_value = $field_config->{'-match_value'};
            if (lc($field_value) ne lc($match_value)) {
                $self->debug({ -text => "Field $field_name failed value match $match_value"});
                $args->{'-fields'}->{$field_name}->{'-valid'} = 0;
                $args->{'-fields'}->{$field_name}->{'-message'} = $field_config->{'-message'};
                $valid = 0;
                next;
            }
        }

        if (defined $field_config->{'-min_length'}) {
            if (length $field_value < $field_config->{'-min_length'}) {
                $self->debug({ -text => "Field $field_name failed min length validation"});
                $args->{'-fields'}->{$field_name}->{'-valid'} = 0;
                $args->{'-fields'}->{$field_name}->{'-message'} = "Field '$field_label' must have at least " . $field_config->{'-min_length'} . ' characters';
                $valid = 0;
                next;
            }        
        }

        if (defined $field_config->{'-max_length'}) {
            if (length $field_value > $field_config->{'-max_length'}) {
                $self->debug({ -text => "Field $field_name failed max length validation"});
                $args->{'-fields'}->{$field_name}->{'-valid'} = 0;
                $args->{'-fields'}->{$field_name}->{'-message'} = "Field '$field_label' is larger than the allowed " . $field_config->{'-max_length'} . ' character limit';
                $valid = 0;
                next;
            }        
        }

    }
    return $valid;
}

sub validate_field_check {
    my ($self, $args) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{UTIL_CGI()};
    
    my $fields      = $args->{'-fields'};
    my $check_field = $args->{'-check_field'};

    $self->debug({ -text => "Check field $check_field -valid " . $fields->{$check_field}->{'-valid'}});    
    if ($fields->{$check_field}->{'-valid'} != 1) {
        return $cgi_util->div({ -class => 'Validation' }, $fields->{$check_field}->{'-message'});
    } else  {
        return '';
    }
}

1;
