package AlienSpaces::Util::XML::LibXML;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use XML::LibXML qw(:libxml);

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

sub new {
    my ( $class, $args ) = @_;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->fatal( { '-text' => 'Failed to initialise super class' } );
    }

    return 1;
}

sub load {
    my ( $self, $args ) = @_;

    #   file
    my $file = $args->{'-file'};

    #   html
    my $html = $args->{'-html'};

    #   validate
    my $validate = $args->{'-validate'} || 0;

    if ( !defined $file && !defined $html ) {
        $self->error( { '-text' => 'Missing required argument -file or -html, cannot load XML' } );
        return;
    }

    #   dom
    my $dom;
    if ( defined $file ) {
        $dom = XML::LibXML->load_xml( 'location' => $file );
    }
    elsif ( defined $html ) {

        #   will only work if the HTML is strict (mostly does not work..)
        my $parser = XML::LibXML->new( { 'expand_entities' => 0, 'suppress_errors' => 1 } );
        $dom = $parser->parse_html_string($html);
    }

    #   validate
    if ($validate) {
        my $success = eval {
            $dom->validate();
            1;
        };
        if (!$success) {
            $self->error({'-text' => 'Failed to validate -args ' . $self->dbg_value($args) . ' :' . $EVAL_ERROR});
            return;
        }
        $self->debug({'-text' => 'XML -file ' . $self->dbg_value($file) . ' -html ' . $self->dbg_value($html) . ' validated'});
    }
    
    #   root
    my $root;
    if ($dom) {
        $root = $dom->documentElement();
    }

    return $root;
} ## end sub load

#   parse
#   - parses an XML::LibXML nest of objects
#   into a single Perl structure
sub parse {
    my ( $self, $args ) = @_;

    #   file
    my $file = $args->{'-file'};

    #   html
    my $html = $args->{'-html'};

    #   xml
    my $xml = $args->{'-xml'};
    if ( !defined $xml && ( defined $html || defined $file ) ) {
        $xml = $self->load($args);
    }
    if ( !defined $xml ) {
        $self->error( { '-text' => 'No -xml, cannot parse' } );
        return;
    }

    #   data
    my $data;

    #   elements
    my $elements = $self->parse_node_elements(   { '-node' => $xml } );
    if (!defined $elements) {
        $elements = [];
    }
    
    #   attributes
    my $attributes = $self->parse_node_attributes( { '-node' => $xml } );
    if (!defined $attributes) {
        $attributes = {};
    }

    #   node name
    my $node_name = $xml->nodeName;
    $data = 
        {   
            '-name'       => $node_name,
            '-elements'   => $elements,
            '-attributes' => $attributes,
        };

    return $data;
} ## end sub parse

#   parse node attributes
sub parse_node_attributes {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-node'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   node
    my $node = $args->{'-node'};

    #   attributes
    my $attributes = {};
        
    #   data
    my $data = $self->parse_node_data({'-node' => $node});
    if (defined $data) {
        $attributes->{'-data'} = $data;
    }
                
    #   attributes
    GET_ATTRIBUTES:
    foreach my $attribute ( $node->attributes() ) {

        if (!defined $attribute) {
            next GET_ATTRIBUTES;
        }
        
        #   attribute name
        my $attribute_name = '-' . lc($attribute->name);

        $attributes->{$attribute_name} = $attribute->value();
    }

    return $attributes;
}

#   parse node data
sub parse_node_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-node' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   node
    my $node = $args->{'-node'};
    
    #   node type
    my $node_type = $node->nodeType;
    
    #   node name
    my $node_name = $node->nodeName;

    my $data;
    if ($node->hasChildNodes) {
        my @child_nodes = $node->childNodes;
        FIND_DATA:
        foreach my $child_node (@child_nodes) {
            if ($child_node->nodeType == XML_CDATA_SECTION_NODE) {
                $data = $child_node->nodeValue;
            } elsif ($child_node->nodeType == XML_TEXT_NODE) {
                $data = $child_node->nodeValue;
            }
            if (defined $data) {
                $data =~ s/^\s+//sxmg;
                $data =~ s/\s+$//sxmg;
            }
            if (length $data) {
                last FIND_DATA;
            }
        }
    }

    return $data;
}
        
#   parse node
sub parse_node_elements {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-node' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   node
    my $node = $args->{'-node'};

    #   child nodes
    my @child_nodes = $node->childNodes();

    #   data
    my $data = [];

    GET_CHILD_NODES:    
    foreach my $child_node (@child_nodes) {

        #   node name
        my $node_name = $child_node->nodeName;

        #   node type
        my $node_type = $child_node->nodeType;
        if ($node_type ne XML_ELEMENT_NODE) {
            next GET_CHILD_NODES;
        }
            
        #   elements
        my $elements = $self->parse_node_elements(   { '-node' => $child_node } );
        if (!defined $elements) {
            $elements = [];
        }
        
        #   attributes
        my $attributes = $self->parse_node_attributes( { '-node' => $child_node } );
        if (!defined $attributes) {
            $attributes = {};
        }
        
        push @{ $data }, 
            {
            '-name'       => $node_name,
            '-elements'   => $elements,
            '-attributes' => $attributes,
            };

    }

    return $data;
} ## end sub parse_node_elements

1;
