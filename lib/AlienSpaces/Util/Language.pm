package AlienSpaces::Util::Language;

#
#   $Revision: 157 $
#   $Author: bwwallin $
#   $Date: 2010-07-05 20:51:40 +1000 (Mon, 05 Jul 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :defaults);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_DB_MYSQL(), UTIL_SESSION(), ];
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed init super class' } );
        return;
    }

    return 1;
}

sub fetch_languages {
    my ( $self, $args ) = @_;

    my $statement = <<'STMT';
SELECT `language_id`,
       `phrase_id`,
       `parent_language_id`,
       `code`,
       `active`
FROM  `as_r_language`
WHERE 1 = 1
STMT

    my @bind;
    if ( defined $args->{'-code'} ) {
        $statement .= <<'STMT';
AND LOWER(`code`) = LOWER(?)
STMT
        push @bind, $args->{'-code'};
    }
    if ( defined $args->{'-language_id'} ) {
        $statement .= <<'STMT';
AND `language_id` = ?
STMT
        push @bind, $args->{'-language_id'};
    }
    if ( defined $args->{'-active'} ) {
        $statement .= <<'STMT';
AND `active` = ?
STMT
        push @bind, $args->{'-active'};
    }

    my $data = $self->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    return $data;
} ## end sub fetch_languages

sub fetch_language {
    my ( $self, $args ) = @_;

    my $language_id = $args->{'-language_id'};
    my $code        = $args->{'-code'};
    my $active        = $args->{'-active'};

    if ( !defined $language_id && !defined $code ) {
        $self->error(
            {   '-text' => 'Required optional arguments -language_id ' . $self->dbg_value($language_id) . ' and -code ' .
                    $self->dbg_value($code) . ' missing, cannot fetch language'
            } );
        return;
    }

    my $data = $self->fetch_languages(
        {   '-language_id' => $language_id,
            '-code'        => $code,
            '-active'      => $active,
        } );

    my $return_data;
    if ( defined $data && scalar @{$data} == 1 ) {
        $return_data = $data->[0];
    }

    return $return_data;
}

1;
