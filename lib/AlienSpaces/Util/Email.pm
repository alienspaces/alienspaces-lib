package AlienSpaces::Util::Email;

#
#   $Revision: 182 $
#   $Author: bwwallin $
#   $Date: 2010-08-17 20:45:49 +1000 (Tue, 17 Aug 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use MIME::Entity;

use constant { MAX_N_FAIL => 5, };

sub util_list {
    my $self = $_[0];
    return [
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_DB_MYSQL(),
    ];
}

sub create_email {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-sender', '-reply_to', '-subject', '-body' ], $args ) ) {
        return;
    }

    $self->debug( { '-text' => 'Creating email -args ' . $self->dbg_value($args) } );

    #   store ?
    my $store = $args->{'-store'};
    if ( !defined $store ) {
        $store = 1;
    }

    #   Set up a new email 'object'
    my $email = {
        '-email_id'     => $args->{'-email_id'},
        '-sender'       => $args->{'-sender'},
        '-reply_to'     => $args->{'-reply_to'},
        '-subject'      => $args->{'-subject'},
        '-body'         => $args->{'-body'},
        '-email_source' => $args->{'-email_source'},
        '-recipients'   => [],
        '-attachments'  => [],
    };

    #   store
    if ( !defined $email->{'-email_id'} && $store ) {

        my $keep_days = $args->{'-keep_days'};
        if ( !defined $keep_days ) {
            $keep_days = 1;
        }

        my $statement = <<'STMT';
INSERT INTO as_email (
    sender,
    reply_to,
    subject,
    body,
    email_source,
    create_ts,
    purge_ts
)
VALUES  (
    ?, 
    ?, 
    ?, 
    ?, 
    ?, 
    CURRENT_TIMESTAMP, 
    DATE_ADD(CURRENT_TIMESTAMP, INTERVAL ? DAY) 
)
STMT

        my $success = eval {
            my $sth = $self->dbh->prepare($statement);

            my $bind = 0;
            $sth->bind_param( ++$bind, $email->{'-sender'} );
            $sth->bind_param( ++$bind, $email->{'-reply_to'} );
            $sth->bind_param( ++$bind, $email->{'-subject'} );
            $sth->bind_param( ++$bind, $email->{'-body'} );
            $sth->bind_param( ++$bind, $email->{'-email_source'} );
            $sth->bind_param( ++$bind, $keep_days );

            $sth->execute();

            $email->{'-email_id'} = $self->dbh->last_insert_id( undef, undef, undef, undef );

            $sth->finish();

            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to create email :' . $EVAL_ERROR } );
            return;
        }
        $self->info( { '-text' => 'Stored email -email_id ' . $email->{'-email_id'} } );
    } ## end if ( !defined $email->...)

    #   Add any recipient(s)
    if ( defined $args->{'-recipient'} ) {
        $self->add_email_recipient(
            {   '-email'     => $email,
                '-recipient' => $args->{'-recipient'},
                '-store'     => $store,
            } );
    }

    #   Add any attachment(s)
    if ( defined $args->{'-attachment'} ) {
        $self->add_email_attachment(
            {   '-email'      => $email,
                '-attachment' => $args->{'-attachment'},
                '-store'      => $store,
            } );
    }

    return $email;
} ## end sub create_email

#   add a recipient given an email_id
sub add_recipient {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-email_id', '-recipient' ], $args ) ) {
        return;
    }

    my $email = $self->fetch_email( { '-email_id' => $args->{'-email_id'} } );

    if ( defined $email ) {
        $self->add_email_recipient(
            {   '-email'     => $email,
                '-recipient' => $args->{'-recipient'},
                '-store'     => 1,
            } );
    }
    return;
}

sub add_email_recipient {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-email', '-recipient' ], $args ) ) {
        return;
    }

    #   recipient
    my $recipient = $args->{'-recipient'};

    #   email id
    my $email_id = $args->{'-email'}->{'-email_id'};

    if ( ref($recipient) eq 'HASH' ) {
        if ( !$self->check_arguments( [ '-address', '-type' ], $recipient ) ) {
            return;
        }

        $self->debug( { '-text' => 'Adding single recipient ' . $self->dbg_value($recipient) } );
    }
    elsif ( ref($recipient) eq 'ARRAY' ) {

        #   Call myself recursively for each element in the array.
        foreach my $r ( @{$recipient} ) {
            $self->add_email_recipient( { '-email' => $args->{'-email'}, '-recipient' => $r, '-store' => $args->{'-store'} } );
        }
        return;
    }
    else {
        $recipient = {
            '-address' => $recipient,
            '-type'    => 'to',
        };
    }

    #   recipient ?
    if ( !defined $recipient ) {
        $self->error(
            { '-text' => 'Missing recipient -args ' . $self->dbg_value($args) . ' -recipient ' . $self->dbg_value($recipient) } );
        return;
    }

    if ( !defined $recipient->{'-recipient_id'} ) {

        #
        #   Are we storing this recipient in the database?
        #
        if ( $args->{'-store'} && $email_id ) {
            my $statement = <<'STMT';
INSERT INTO  as_email_recipient (
    email_id,
    email_address,
    recipient_type
)
VALUES  (
    ?, 
    ?, 
    ?
)
STMT

            my $success = eval {
                my $sth = $self->dbh->prepare($statement);

                my $bind = 0;
                $sth->bind_param( ++$bind, $email_id );
                $sth->bind_param( ++$bind, $recipient->{'-address'} );
                $sth->bind_param( ++$bind, $recipient->{'-type'} );
                $sth->execute();

                $recipient->{'-recipient_id'} = $self->dbh->last_insert_id( undef, undef, undef, undef );

                $sth->finish();
                1;
            };

            if ( !$success ) {
                $self->error(
                    {   '-text' => 'Failed to add email recipient to -email_id ' .
                            $args->{'-email'}->{'-email_id'} . ' :' . $EVAL_ERROR
                    } );
                return;
            }

            $self->debug( { '-text' => 'Added recipient ' . $self->dbg_value($recipient) } );

        } ## end if ( $args->{'-store'}...)

    } ## end if ( !defined $recipient...)

    push @{ $args->{'-email'}->{'-recipients'} }, $recipient;

    return 1;
} ## end sub add_email_recipient

sub add_email_attachment {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-email', '-attachment' ], $args ) ) {
        return;
    }

    #   attachment
    my $attachment = $args->{'-attachment'};

    #   email id
    my $email_id = $args->{'-email'}->{'-email_id'};

    if ( ref($attachment) eq 'HASH' ) {
        if ( !$self->check_arguments( [ '-file_name', '-file_path' ], $attachment ) ) {
            return;
        }
    }
    elsif ( ref($attachment) eq 'ARRAY' ) {

        #
        #   Call myself recursively for each element in the array.
        #
        foreach my $a ( @{$attachment} ) {
            $self->add_attachment( { '-email' => $args->{'-email'}, '-attachment' => $a, '-store' => $args->{'-store'} } );
        }
        return;
    }
    else {
        $attachment = { '-file_path' => $attachment, };
    }

    #   attachment ?
    if ( !defined $attachment ) {
        $self->error( { '-text' => 'Missing attachment' } );
        return;
    }

    if ( !defined $attachment->{'-attachment_id'} ) {

        $attachment->{'-attachment_id'} = undef;

        #
        #   Are we storing this attachment in the database?
        #
        if ( $args->{'-store'} && defined $args->{'-email'}->{'id'} ) {
            my $statement = <<'STMT';
INSERT INTO as_email_attachment (
    email_id,
    file_name,
    file_path
)
VALUES  (
    ?, 
    ?,
    ?
)
STMT

            my $success = eval {
                my $sth = $self->dbh->prepare($statement);

                my $bind = 0;
                $sth->bind_param( ++$bind, $email_id );
                $sth->bind_param( ++$bind, $attachment->{'-file_name'} );
                $sth->bind_param( ++$bind, $attachment->{'-file_path'} );
                $sth->execute();

                $attachment->{'-attachment_id'} = $self->dbh->last_insert_id( undef, undef, undef, undef );

                $sth->finish();
                1;
            };
            if ( !$success ) {
                $self->error(
                    { '-text' => 'Failed to add attachment to -email_id ' . $args->{'-email'}->{'-email_id'} . ' :' . $EVAL_ERROR }
                );
                return;
            }
        } ## end if ( $args->{'-store'}...)
        push @{ $args->{'-email'}->{'-attachments'} }, $attachment;
    } ## end if ( !defined $attachment...)
    return;
} ## end sub add_email_attachment

sub fetch_email {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-email_id'], $args ) ) {
        return;
    }

    #   email id
    my $email_id = $args->{'-email_id'};

    my $sql_email = <<'STMT';
SELECT  sender,
        reply_to,
        subject,
        body,
        create_ts,
        email_source,
        purge_ts
FROM    as_email
WHERE   email_id = ?
STMT

    my $sql_recipient = <<'STMT';
SELECT  email_recipient_id,
        email_address,
        recipient_type,
        sent_ts,
        send_fail_count,
        send_fail_ts
FROM    as_email_recipient
WHERE   email_id = ?
STMT

    my $sql_attachment = <<'STMT';
SELECT  email_attachment_id,
        file_path
FROM    as_email_attachment
WHERE   email_id = ?
STMT

    #   Fetch email
    my ( $sender, $reply_to, $subject, $body, $create_ts, $email_source, $purge_ts );
    my $success = eval {
        my $sth = $self->dbh->prepare($sql_email);
        $sth->execute($email_id);
        $sth->bind_columns( \( $sender, $reply_to, $subject, $body, $create_ts, $email_source, $purge_ts ) );
        if ( !$sth->fetch ) {
            $self->fatal( { '-text' => 'Email -email_id ' . $email_id . ' not found' } );
        }
        $sth->finish;
        1;
    };

    if ( !$success ) {
        $self->fatal( { '-text' => 'Failed to fetch email :' . $EVAL_ERROR } );
    }

    #   fetch recipients
    my $recip_data;
    $success = eval {
        my $sth = $self->dbh->prepare($sql_recipient);
        $sth->bind_param( 1, $email_id );
        $sth->execute();
        $recip_data = $sth->fetchall_arrayref();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->fatal( { '-text' => 'Failed to fetch recipients :' . $EVAL_ERROR } );
    }

    my $recipients = [];
    foreach ( @{$recip_data} ) {
        push @{$recipients},
            {
            '-recipient_id'    => $_->['0'],
            '-address'         => $_->['1'],
            '-type'            => $_->['2'],
            '-sent_ts'         => $_->['3'],
            '-send_fail_count' => $_->['4'],
            '-send_fail_ts'    => $_->['5'],
            };
    }

    #   fetch attachments
    my $attach_data;
    $success = eval {
        my $sth = $self->dbh->prepare($sql_attachment);
        $sth->bind_param( 1, $email_id );
        $sth->execute();
        $attach_data = $sth->fetchall_arrayref();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->fatal( { '-text' => 'Failed to fetch attachments :' . $EVAL_ERROR } );
    }

    my $attachments = [];
    foreach ( @{$attach_data} ) {
        push @{$attachments},
            {
            '-attachment_id' => $_->['0'],
            '-file_path'     => $_->['1'],
            };
    }

    #   email
    my $email = $self->create_email(
        {   '-store'        => 0,
            '-email_id'     => $email_id,
            '-sender'       => $sender,
            '-reply_to'     => $reply_to,
            '-subject'      => $subject,
            '-body'         => $body,
            '-create_ts'    => $create_ts,
            '-email_source' => $email_source,
            '-purge_ts'     => $purge_ts,
            '-recipient'    => $recipients,
            '-attachment'   => $attachments,
        } );

    $self->info( { '-text' => 'Fetched email -email_id ' . $email_id } );

    return $email;
} ## end sub fetch_email

sub send_email {
    my ( $self, $args ) = @_;

    if ( !defined $args->{'-email'} && !defined $args->{'-email_id'} ) {
        return $self->fatal( { '-message' => 'Either -email or -email_id is required' } );
    }

    my $email;
    if ( defined $args->{'-email'} ) {
        $email = $args->{'-email'};
    }
    elsif ( defined $args->{'-email_id'} ) {
        $email = $self->fetch_email( { '-email_id' => $args->{'-email_id'} } );
    }

    if ( !defined $email ) {
        $self->error( { '-text' => 'No email specified to send' } );
        return;
    }

    my $recip = { 'to' => [], 'cc' => [], 'bcc' => [], 'all' => [] };

    my $idx = 0;
    foreach my $recipient ( @{ $email->{'-recipients'} } ) {
        if ( !defined $recipient->{'-sent_ts'} ) {
            push @{ $recip->{ $recipient->{'-type'} } }, $recipient->{'-address'};

            #
            #   The 'all' array ref
            #   [0] = address,
            #   [1] = index to $recipients
            #
            push @{ $recip->{'all'} }, [ $recipient->{'-address'}, $idx ];

            $self->debug( { '-text' => 'Added recipient -address ' . $recipient->{'-address'} . ' -idx ' . $idx } );

        }
        ++$idx;
    }

    if ( !scalar @{ $recip->{'all'} } ) {
        $self->info( { '-text' => 'No (unsent) recipients for email (ID=' . $email->{'-email_id'} . ')' } );
        return;
    }

    $self->debug( { '-text' => 'Sending -email ' . $self->dbg_value($email) . ' -recip ' . $self->dbg_value($recip) } );

    #   data
    my $data = $email->{'-body'};

    #   type
    my $type = 'text/plain';
    if ( $data =~ /\<html/sxmg ) {
        $type = 'text/html';
    }

    my $mime = MIME::Entity->build(
        'Type'     => $type,
        'To'       => $recip->{'to'},
        'Cc'       => $recip->{'cc'},
        'Bcc'      => $recip->{'bcc'},
        'From'     => $email->{'-sender'},
        'Sender'   => $email->{'-sender'},
        'Subject'  => $email->{'-subject'},
        'Data'     => $data,
        'Charset'  => 'UTF-8',
        'Encoding' => '-SUGGEST',
        'Reply-To' => $email->{'-reply_to'},
    );

    my $success = eval {
        open MAIL, '| /usr/sbin/sendmail -t -oi -oem' or croak 'Failed opening mail handle :' . $OS_ERROR;
        $mime->print( \*MAIL );
        close MAIL or croak 'Failed closing mail handle :' . $OS_ERROR;
        1;
    };
    if ( !$success ) {

        $self->error( { '-text' => 'Sending email failed ' . $EVAL_ERROR } );

        my $statement = <<'STMT';
UPDATE  as_email_recipient
SET     send_fail_ts        = CURRENT_TIMESTAMP,
        send_fail_count     = send_fail_count + 1
WHERE   email_recipient_id  = ?
STMT

        my $sth = $self->dbh->prepare($statement);

    RECIPIENTS_FAIL:
        foreach my $recipient ( @{ $recip->{'all'} } ) {

            $idx = $recipient->[1];

            $success = eval {
                $sth->execute( $email->{'-recipients'}->[$idx]->{'-recipient_id'} );
                1;
            };
            if ( !$success ) {
                $self->error(
                    {   '-text' => 'Failed to update fail count for -recipient_id ' .
                            $email->{'-recipients'}->[$idx]->{'-recipient_id'} } );
                next RECIPIENTS_FAIL;
            }

            $self->debug(
                { '-text' => 'Updated fail count for -recipient_id ' . $email->{'-recipients'}->[$idx]->{'-recipient_id'} } );
            next RECIPIENTS_FAIL;

        }

        return;
    } ## end if ( !$success )

    my $statement = <<'STMT';
UPDATE  as_email_recipient
SET     sent_ts             = CURRENT_TIMESTAMP
WHERE   email_recipient_id  = ?
STMT

    my $sth = $self->dbh->prepare($statement);

RECIPIENTS_OK:
    foreach my $recipient ( @{ $recip->{'all'} } ) {

        $idx = $recipient->[1];

        $success = eval {
            $sth->execute( $email->{'-recipients'}->[$idx]->{'-recipient_id'} );
            1;
        };
        if ( !$success ) {
            $self->error(
                {   '-text' => 'Failed to update sent timestamp for -recipient_id ' .
                        $email->{'-recipients'}->[$idx]->{'-recipient_id'} } );
            next RECIPIENTS_OK;
        }

        $self->debug(
            { '-text' => 'Updated sent timestamp for -recipient_id ' . $email->{'-recipients'}->[$idx]->{'-recipient_id'} } );
        next RECIPIENTS_OK;

    }

    #
    #   Commit changes to the database
    #
    $self->dbh->commit;

    return;
} ## end sub send_email

sub fetch_unsent_emails {
    my ( $self, $args ) = @_;

    my $statement = <<'STMT';
SELECT  DISTINCT email_id
FROM    as_email_recipient
WHERE   sent_ts    IS NULL
AND     (send_fail_count < ? OR send_fail_count IS NULL)
ORDER
BY      email_id
STMT

    my $unsent_emails;
    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute(MAX_N_FAIL);
        $unsent_emails = $sth->fetchall_arrayref();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to fetch unsent emails :' . $EVAL_ERROR } );
        return;
    }

    my $unsent_id_list = [];
    foreach my $unsent_email ( @{$unsent_emails} ) {
        my ($email_id) = @{$unsent_email};
        push @{$unsent_id_list}, $email_id;
    }
    $self->info( { '-text' => 'Returning list of ' . ( scalar @{$unsent_id_list} ) . ' email ID\'s' } );

    return $unsent_id_list;
} ## end sub fetch_unsent_emails

sub delete_email {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-email_id'], $args ) ) {
        return;
    }

    #   email id
    my $email_id = $args->{'-email_id'};

    my $sql_delete_recipient = <<'STMT';
DELETE
FROM    as_email_recipient
WHERE   email_id = ?
STMT

    my $sql_delete_email = <<'STMT';
DELETE
FROM    as_email
WHERE   email_id = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($sql_delete_recipient);

        my $bind = 0;
        $sth->bind_param( ++$bind, $email_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete recipients for -email_id ' . $email_id . ' :' . $EVAL_ERROR } );
        return;
    }

    $success = eval {
        my $sth = $self->dbh->prepare($sql_delete_email);

        my $bind = 0;
        $sth->bind_param( ++$bind, $email_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete email -email_id ' . $email_id . ' :' . $EVAL_ERROR } );
        return;
    }

    return 1;
} ## end sub delete_email

sub fetch_purgeable_emails {
    my ( $self, $args ) = @_;

    my $statement = <<'STMT';
SELECT  email_id
FROM    as_email e
WHERE   GREATEST(purge_ts, create_ts + interval 1 day) < current_timestamp()
AND     EXISTS (
    SELECT  0
    FROM    as_email_recipient r
    WHERE   r.email_id = e.email_id
    AND     sent_ts IS NOT NULL
)
STMT

    my $purge_data;
    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute();
        $purge_data = $sth->fetchall_arrayref();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to fetch purgeable emails :' . $EVAL_ERROR } );
        return;
    }

    my $purge_ret = [];
    foreach my $purge_email ( @{$purge_data} ) {
        push @{$purge_ret}, $purge_email->[0];
    }
    $self->info( { '-text' => 'Found ' . ( scalar @{$purge_ret} ) . ' email(s) eligible for purging' } );

    return $purge_ret;
} ## end sub fetch_purgeable_emails

sub fetch_sent_emails {
    my ( $self, $args ) = @_;

    my $statement = <<'STMT';
SELECT  email_id
FROM    as_email e
WHERE EXISTS (
    SELECT  1
    FROM    as_email_recipient r
    WHERE   r.email_id = e.email_id
    AND     sent_ts IS NOT NULL
)
STMT

    my $sent_data;
    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute();
        $sent_data = $sth->fetchall_arrayref();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to fetch sent emails :' . $EVAL_ERROR } );
        return;
    }

    my $sent_ret = [];
    foreach my $sent_email ( @{$sent_data} ) {
        push @{$sent_ret}, $sent_email->[0];
    }
    $self->info( { '-text' => 'Found ' . ( scalar @{$sent_ret} ) . ' emails' } );

    return $sent_ret;
} ## end sub fetch_sent_emails

1;
