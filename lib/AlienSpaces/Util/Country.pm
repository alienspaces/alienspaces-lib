package AlienSpaces::Util::Country;

#
#   $Revision: 157 $
#   $Author: bwwallin $
#   $Date: 2010-07-05 20:51:40 +1000 (Mon, 05 Jul 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :defaults);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_DB_MYSQL(), UTIL_SESSION(), ];
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed init super class' } );
        return;
    }

    return 1;
}

1;
