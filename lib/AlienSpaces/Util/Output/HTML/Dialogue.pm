package AlienSpaces::Util::Output::HTML::Dialogue;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_CGI(),

    ];

    return $util_list;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-name', '-content' ], $args ) ) {
        return;
    }

    #   cgi
    my $cgi = $self->cgi;

    #   id
    my $id = $self->make_id($args);

    #   class
    my $class = $args->{'-class'} || '';

    #   title
    my $title = $args->{'-title'};

    #   header content
    my $header_content;
    if ( defined $title ) {
        $header_content = $cgi->div( { '-class' => 'DialogueHeader' }, $cgi->div( { '-class' => 'DialogueInnerHeader' }, $title, ) );
    }

#<<<
    #   content
    my $content = 
        $cgi->div({ '-id' => $id . 'Dialogue' },
            $cgi->div({ '-class' => 'Dialogue ' . $class },
                ( defined $header_content ? $header_content : '' ) . 
                $cgi->div( { '-class' => 'DialogueContent' }, 
                    $cgi->div( { '-class' => 'DialogueInnerContent' }, 
                        $args->{'-content'} 
                    ) 
                ) 
            ) 
        );
#>>>

    return $content;
} ## end sub render

1;
