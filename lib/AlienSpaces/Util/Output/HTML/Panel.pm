package AlienSpaces::Util::Output::HTML::Panel;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :panel-styles :message-types);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

        #   we are html centric
        UTIL_CGI(),

        #   error support
        UTIL_ERROR(),

        #   messaging support
        UTIL_MESSAGE(),
    ];

    return $util_list;
}

sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args();
    if ( defined $args->{'-class'} ) {

    }
    return $util_args;
}

sub render {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-name'], $args ) ) {
        return;
    }

    #   name
    my $name = $args->{'-name'};

    #   id
    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id($args);
    }

    #   style
    if ( !defined $args->{'-style'} ) {
        $args->{'-style'} = PANEL_STYLE_BASIC;
    }

    #   content
    my $content = '';

    #   messages
    my $messages = $args->{'-messages'};
    if ( !defined $messages && defined $name ) {
        $messages = $self->get_messages( { '-name' => $name } );
    }

    #   render messages
    my $message_content = $self->render_messages(
        {   '-messages' => $messages,
            '-name'     => $name,
        } );
    if ( defined $message_content ) {
        $args->{'-content'} = $message_content . ( defined $args->{'-content'} ? $args->{'-content'} : '' );
    }

    $self->debug( { '-text' => 'Rendering panel -name ' . ( defined $args->{'-name'} ? $args->{'-name'} : 'undef' ) . ' -id ' . $args->{'-id'} } );

    $content .=

        #   start panel
        $self->start_panel($args) .

        #   panel header
        $self->panel_header($args) .

        #   panel body
        $self->panel_body($args) .

        #   panel footer
        $self->panel_footer($args) .

        #   end panel
        $self->end_panel($args);

    return $content;
} ## end sub render

#   message type CSS classes
my %message_type_class_map = (
    MESSAGE_TYPE_SUCCESS() => 'Success',
    MESSAGE_TYPE_ERROR()   => 'Error',
    MESSAGE_TYPE_INFO()    => 'Info',
);

#   panel messages
sub render_messages {
    my ( $self, $args ) = @_;

    my $cgi = $self->cgi;

    #   type
    my $type = $args->{'-type'};

    #   messages
    my $messages = $args->{'-messages'};
    if ( !defined $messages || !scalar @{$messages} ) {
        return;
    }

    #   content
    my $content = $cgi->start_div( { '-class' => 'PanelMessages' } );

    #   sort messages by type
    foreach my $message ( sort { $a->{'-type'} <=> $b->{'-type'} } @{$messages} ) {

        #   text
        my $message_text = $message->{'-text'};

        #   type
        my $message_type = $message->{'-type'};

        $content .= $cgi->div( { '-class' => $message_type_class_map{$message_type} }, $message_text );
    }

    $content .= $cgi->end_div();

    return $content;
} ## end sub render_messages

#   start panel
sub start_panel {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $ret = '';

    my $style_class_map = {
        PANEL_STYLE_BASIC()    => 'PanelBasic',
        PANEL_STYLE_STANDARD() => 'PanelStandard',
    };

    my $content_only = $args->{'-content_only'} || 0;
    my $class        = $args->{'-class'}        || '';

    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id();
    }

    $ret .= '<!-- Start Panel ' . $args->{'-id'} . ' -->' . "\n";
    $ret .= (
        $content_only ?
            ''
        : $cgi_util->start_div(
            {   '-id'    => $args->{'-id'} . 'Panel',
                '-class' => 'Panel ' . $style_class_map->{ $args->{'-style'} } . ' ' . $class
            } ) ) . "\n";

    #
    #   standard style
    #
    if ( $args->{'-style'} == PANEL_STYLE_STANDARD ) {
        $ret .= $cgi_util->div( { '-class' => 'PanelHead1' }, '' ) . "\n";
        $ret .= $cgi_util->div( { '-class' => 'PanelHead2' }, '' ) . "\n";
        $ret .= $cgi_util->div( { '-class' => 'PanelHead3' }, '' ) . "\n";
    }

    return $ret;
} ## end sub start_panel

sub end_panel {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $ret = '';

    my $content_only = $args->{'-content_only'} || 0;

    if ( $args->{'-style'} == PANEL_STYLE_STANDARD ) {
        $ret .= $cgi_util->div( { '-class' => 'PanelFoot3' }, '' ) . "\n";
        $ret .= $cgi_util->div( { '-class' => 'PanelFoot2' }, '' ) . "\n";
        $ret .= $cgi_util->div( { '-class' => 'PanelFoot1' }, '' ) . "\n";
    }

    $ret .= ( $content_only ? '' : $cgi_util->end_div() ) . "\n";

    return $ret;
}

sub panel_header {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $title = $args->{'-title'};

    #   show header
    if ( exists $args->{'-show_header'} && !$args->{'-show_header'} ) {
        return '';
    }

    #   show empty header
    if ( !defined $title && exists $args->{'-show_empty_header'} && !$args->{'-show_empty_header'} ) {
        return '';
    }
    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id();
    }

    #   header class
    my $header_class = 'PanelHeader' . ( defined $args->{'-header_class'} ? ' ' . $args->{'-header_class'} : '' );

    return $cgi_util->div(
        {   '-id'    => $args->{'-id'} . 'PanelHeader',
            '-class' => $header_class,
        },
        ( defined $title ? $cgi_util->div( { '-class' => 'PanelInnerHeader' }, $title ) : ' ' ) ) . "\n";
} ## end sub panel_header

sub panel_body {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $content = $args->{'-content'};
    if ( !defined $content && exists $args->{'-show_empty_content'} && !$args->{'-show_empty_content'} ) {
        return '';
    }

    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id();
    }

    #   content class
    my $content_class = 'PanelContent' . ( defined $args->{'-content_class'} ? ' ' . $args->{'-content_class'} : '' );

    return $cgi_util->div(
        {   '-id'    => $args->{'-id'} . 'PanelContent',
            '-class' => $content_class,
        },
        $cgi_util->div(
            {   '-id'    => $args->{'-id'} . 'PanelInnerContent',
                '-class' => 'PanelInnerContent'
            },
            ( defined $content ? $content : ' ' ) )
            . "\n"
    ) . "\n";
} ## end sub panel_body

sub panel_footer {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $footer = $args->{'-footer'};

    #   show footer
    if ( exists $args->{'-show_footer'} && !$args->{'-show_footer'} ) {
        return '';
    }

    #   show empty footer
    if ( !defined $footer && exists $args->{'-show_empty_footer'} && !$args->{'-show_empty_footer'} ) {
        return '';
    }

    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id();
    }

    #   footer class
    my $footer_class = 'PanelFooter' . ( defined $args->{'-footer_class'} ? ' ' . $args->{'-footer_class'} : '' );

    return $cgi_util->div(
        {   '-id'    => $args->{'-id'} . 'PanelFooter',
            '-class' => $footer_class,
        },
        ( defined $footer ? $cgi_util->div( { '-class' => 'PanelInnerFooter' }, $footer ) : ' ' ) ) . "\n";
} ## end sub panel_footer

1;
