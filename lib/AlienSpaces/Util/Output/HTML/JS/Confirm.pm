package AlienSpaces::Util::Output::HTML::JS::Confirm;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :js-confirm);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_PHRASE(),

    ];

    return $util_list;
}

#   type phrase id map
my $type_phrase_id_map = { JS_CONFIRM_TYPE_DELETE() => 95_000, };

sub render {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-type'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   type
    my $type = $args->{'-type'};

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   confirm phrase id
    my $confirm_phrase_id = $type_phrase_id_map->{$type};
    if ( !defined $confirm_phrase_id ) {
        $self->error( { '-text' => 'Missing phrase for js confirm -type ' . $self->dbg_value($type) } );
        return;
    }

    #   confirm phrase text
    my $confirm_phrase_text = $phrase_util->text( { '-phrase_id' => $confirm_phrase_id } );
    if ( !defined $confirm_phrase_text ) {
        $self->error( { '-text' => 'Missing phrase text for -phrase_id ' . $self->dbg_value($confirm_phrase_id) } );
        return;
    }

    my %content = ( '-data-js-confirm' => $confirm_phrase_text );

    return %content;
} ## end sub render

1;

__END__
