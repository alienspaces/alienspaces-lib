package AlienSpaces::Util::Output::HTML::Table;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :table-styles);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_CGI(),

    ];

    return $util_list;
}

#   render
#   - Takes arguments
#       '-column_titles' => \@array_of_column_titles
#       '-data'          => \@array_of_rows_of_array_of_columns
sub render {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-name', ], $args ) ) {
        return;
    }

    #   name
    my $name = $args->{'-name'};

    #   id
    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id($args);
    }

    $self->debug( { '-text' => 'Rendering table -name ' . ( defined $args->{'-name'} ? $args->{'-name'} : 'undef' ) . ' -id ' . $args->{'-id'} } );

    my $content =

        #   start table
        $self->start_table($args) .

        #   table header
        $self->table_header($args) .

        #   table body
        $self->table_body($args) .

        #   table footer
        $self->table_footer($args) .

        #   end table
        $self->end_table($args);

    return $content;
} ## end sub render

#   start table
sub start_table {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $ret = '';

    my $style_class_map = { TABLE_STYLE_BASIC() => 'TableBasic', };

    #   class
    my $class = $args->{'-class'} || '';

    #   style
    my $style = $args->{'-style'} || TABLE_STYLE_BASIC;

    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id($args);
    }

    $ret .= '<!-- Start Table ' . $args->{'-id'} . ' -->' . "\n";
    $ret .= $cgi_util->start_div(
        {   '-id'    => $args->{'-id'} . 'Table',
            '-class' => 'Table ' . $style_class_map->{$style} . ' ' . $class
        } ) . "\n";

    return $ret;
}

sub end_table {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    my $ret = '';

    $ret .= $cgi_util->end_div() . "\n";

    return $ret;
}

sub table_header {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   title
    my $title = $args->{'-title'};
    if ( !defined $title && exists $args->{'-show_empty_header'} && !$args->{'-show_empty_header'} ) {
        return '';
    }

    #   extra header content
    my $extra_header_content = $args->{'-extra_header_content'};

    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id($args);
    }

    #   header class
    my $header_class = 'TableHeader' . ( defined $args->{'-header_class'} ? ' ' . $args->{'-header_class'} : '' );

    my $content = '';

    #   show header
    if ( !exists $args->{'-show_header'} || $args->{'-show_header'} ) {
        $content .= $cgi_util->div(
            {   '-id'    => $args->{'-id'} . 'TableHeader',
                '-class' => $header_class,
            },

            #   title
            ( ( defined $title || defined $extra_header_content ) ? $cgi_util->div( { '-class' => 'TableInnerHeader' }, $title ) : ' ' ) .

                #   extra header content
                ( defined $extra_header_content ? $cgi_util->div( { '-class' => 'TableInnerExtraHeader' }, $extra_header_content ) : ' ' ) );
    }

    #   show content
    if ( exists $args->{'-show_content'} && !$args->{'-show_content'} ) {
        $self->debug( { '-text' => 'Not showing content' } );
        return $content;
    }

    $content .=

        #   table content
        $cgi_util->start_div( { '-id' => $args->{'-id'} . 'TableContent', '-class' => 'TableContent' } ) .

        #   start table
        $cgi_util->start_table( { '-id' => $args->{'-id'} . 'TableInnerContent', '-class' => 'TableInnerContent' } );

    #   column title
    my $column_titles = $args->{'-column_titles'};
    if ( !defined $column_titles || !scalar @{$column_titles} ) {
        return $content;
    }

    $content .= $cgi_util->start_thead() . $cgi_util->start_Tr();

    #   column total
    my $column_total = scalar @{$column_titles};

    #   column count
    my $column_count = 0;
    foreach my $column_title ( @{$column_titles} ) {

        my $class;
        if ( $column_count == 0 ) {
            $class = 'First';
        }
        elsif ( $column_count == ( $column_total - 1 ) ) {
            $class = 'Last';
        }

        $class .= ' Col' . $column_count;

        $content .= $cgi_util->th( { '-class' => $class }, $column_title );
        $column_count++;
    }

    $content .= $cgi_util->end_Tr() . $cgi_util->end_thead();

    return $content;
} ## end sub table_header

sub table_body {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    #   show content
    if ( exists $args->{'-show_content'} && !$args->{'-show_content'} ) {
        $self->debug( { '-text' => 'Not showing content' } );
        return '';
    }

    #   data
    my $data = $args->{'-data'};
    if ( !defined $data || !scalar @{$data} ) {
        return '';
    }

    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id($args);
    }

    my $content = $cgi_util->start_tbody();

    #   row total
    my $row_total = scalar @{$data};

    #   row count
    my $row_count = 0;

    #   odd even
    my $odd_even;

    foreach my $row ( @{$data} ) {

        if ( ref($row) ne 'ARRAY' ) {
            $self->error( { '-text' => 'Row data must be an arrayref have -row ' . $self->dbg_value($row) } );
            return;
        }

        #   row class
        my $row_class;

        if ( $row_count == 0 ) {
            $row_class = 'First ';
        }
        elsif ( $row_count == ( $row_total - 1 ) ) {
            $row_class = 'Last ';
        }
        else {
            $row_class = '';
        }

        if ( !defined $odd_even ) {
            $row_class .= TABLE_ROW_CLASS_ODD;
            $odd_even = TABLE_ROW_CLASS_ODD;
        }
        elsif ( $odd_even eq TABLE_ROW_CLASS_EVEN ) {
            $row_class .= TABLE_ROW_CLASS_ODD;
            $odd_even = TABLE_ROW_CLASS_ODD;
        }
        elsif ( $odd_even eq TABLE_ROW_CLASS_ODD ) {
            $row_class .= TABLE_ROW_CLASS_EVEN;
            $odd_even = TABLE_ROW_CLASS_EVEN;
        }

        $content .= $cgi_util->start_Tr( { '-class' => $row_class } );

        #   column total
        my $column_total = scalar @{$row};

        #   column count
        my $column_count = 0;
        foreach my $col ( @{$row} ) {

            my $class;
            if ( $column_count == 0 ) {
                $class = 'First ';
            }
            elsif ( $column_count == ( $column_total - 1 ) ) {
                $class = 'Last ';
            }
            else {
                $class = '';
            }

            $class .= 'Col' . $column_count;

            $content .= $cgi_util->td( { '-class' => $class }, $col );
            $column_count++;
        }

        $content .= $cgi_util->end_Tr();

        $row_count++;
    } ## end foreach my $row ( @{$data} )

    $content .= $cgi_util->end_tbody();

    return $content;
} ## end sub table_body

sub table_footer {
    my ( $self, $args ) = @_;

    #   cgi util
    my $cgi_util = $self->utils->{ UTIL_CGI() };

    if ( !defined $args->{'-id'} ) {
        $args->{'-id'} = $self->make_id($args);
    }

    #   content
    my $content = '';

    #   show content
    if ( !exists $args->{'-show_content'} || $args->{'-show_content'} ) {

        $content .=

            #   end table
            $cgi_util->end_table() .

            #   end div
            $cgi_util->end_div();
    }

    #   extra footer content
    my $extra_footer_content = $args->{'-extra_footer_content'};

    #   show footer
    if ( exists $args->{'-show_footer'} && !$args->{'-show_footer'} ) {
        $self->debug( { '-text' => 'Not showing footer' } );
        return $content;
    }

    $content .=

        #   div
        $cgi_util->div(
        { '-id' => $args->{'-id'} . 'TableFooter', '-class' => 'TableFooter' },

        #   extra footer content
        ( defined $extra_footer_content ? $cgi_util->div( { '-class' => 'TableInnerExtraFooter' }, $extra_footer_content ) : ' ' ) );

    return $content;
} ## end sub table_footer

1;
