package AlienSpaces::Util::Output::HTML::Page;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :html-page);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

        #   we are html centric
        UTIL_CGI(),

        #   error support
        UTIL_ERROR(),

        #   messaging support
        UTIL_MESSAGE(),

        #   template
        UTIL_TEMPLATE(),
    ];

    return $util_list;
}

sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args();
    if ( defined $args->{'-class'} ) {

    }
    return $util_args;
}

sub render {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-vars', '-program_name' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $vars = { 'document' => $args->{'-vars'} };
    my $program_name = $args->{'-program_name'};

    my $template_util = $self->utils->{ UTIL_TEMPLATE() };

    # TODO: can we speed up this template search
    my $content = $template_util->process(
        {   '-file' => [ 'html/' . $program_name, DEFAULT_HTML_PAGE_TEMPLATE ],
            '-vars' => $vars,
        } );

    return $content;
}

1;

__END__


