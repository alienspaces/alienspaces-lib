package AlienSpaces::Util::Output::HTML::Navigation;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :task :link);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_CGI(),
        UTIL_USER(),
        UTIL_PHRASE(),

    ];

    return $util_list;
}

my %link_css = (

    #   orientation
    LINK_ORIENTATION_VERTICAL()   => 'Vertical',
    LINK_ORIENTATION_HORIZONTAL() => 'Horizontal',

    #   align
    LINK_ALIGN_LEFT()   => 'Left',
    LINK_ALIGN_RIGHT()  => 'Right',
    LINK_ALIGN_CENTER() => 'Center',
);

#   render
#   - Accepts the following arguments
#       '-links'    => \@array_of_hashes_containing
#           {
#             '-name'               => $name,   #   deprecated
#             '-label_text'         => "Label Text",
#             '-label_phrase_id'    => $label_phrase_id,
#             '-program'            => PROGRAM_SITE_NAME,
#             '-component'          => COMPONENT_NAME,
#             '-other'              => 'xxxx',
#             '-class'              => $css_class,
#             '-related_programs'   => \@array_of_program_names,
#             '-links'              => \@another_array_of_this_same_stuff
#           }
#       '-orientation' => LINK_ALIGN_HORIZONTAl | LINK_ORIENTATION_VERTICAL
#       '-align'       => LINK_ALIGN_LEFT | LINK_ALIGN_CENTER | LINK_ALIGN_RIGHT
sub render {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-links', '-name' ], $args ) ) {
        return;
    }

    #   links
    my $links = $args->{'-links'};

    #   orientation
    my $orientation = $args->{'-orientation'};
    if ( !defined $orientation ) {
        $orientation = LINK_ORIENTATION_VERTICAL();
    }

    #   class
    my $class = $args->{'-class'} || '';

    #   align
    my $align = $args->{'-align'};
    if ( !defined $align ) {
        $align = LINK_ALIGN_CENTER();
    }

    #   name
    my $name = $args->{'-name'};

    #   id
    my $id = $self->make_id({'-name' => $name . 'Navigation'});

    #   cgi
    my $cgi = $self->cgi;

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   css
    my $orientation_class = $link_css{$orientation};
    my $align_class       = $link_css{$align};

    #   content
    my $content = <<"HTML";

<!--   Start Navigation $name -->
<div id="$id" class="Links $class">
<div class="$orientation_class">
<ul class="$align_class">
HTML

    foreach my $link ( @{$links} ) {

        #   link args
        my $list_args = {};

        #   link content
        my $link_content;
        if ( ref($link) eq 'HASH' ) {

            #   data
            my $data = delete $link->{'-data'};

            #   sub links
            my $sub_links = delete $link->{'-sub_links'};

            #   program task
            if ( exists $link->{'-program'} ) {
                my $task_id = $self->get_program_authorize_task( { '-program' => $link->{'-program'} } );
                if ( !defined $task_id ) {
                    $self->error( { '-text' => 'Unauthorized -program ' . $link->{'-program'} . ' missing tasks' } );
                    return;
                }
                $self->debug(
                    { '-text' => 'Program ' . $self->dbg_value( $link->{'-program'} ) . ' requires -task_id ' . $task_id } );

                if ( $task_id != TASK_PUBLIC ) {

                    $self->debug( { '-text' => 'Program does not have public access' } );

                    if ( !$user_util->check_user_task( { '-task_id' => $task_id } ) ) {
                        $self->info(
                            { '-text' => 'User does not have -task_id ' . $self->dbg_value($task_id) . ', authorize failure' } );
                        return;
                    }
                }
            }

            #   related programs
            my $related_programs = delete $link->{'-related_programs'};
            if ( !defined $related_programs ) {
                $related_programs = [];
            }
            $self->debug(
                {   '-text' => 'Comparing -program_name ' .
                        $self->dbg_value( $self->program_name ) . ' with link -program ' . $self->dbg_value( $link->{'-program'} ) }
            );
            if ( exists $link->{'-program'} && $link->{'-program'} eq $self->program_name ) {
                push @{$related_programs}, $link->{'-program'};
            }

            if ( defined $related_programs && scalar @{$related_programs} ) {
                foreach my $program_name ( @{$related_programs} ) {
                    $self->debug(
                        {   '-text' => 'Comparing -program_name ' . $self->dbg_value( $self->program_name ) .
                                ' with related -program_name ' . $self->dbg_value($program_name) } );
                    if ( $self->program_name eq $program_name ) {
                        $list_args->{'-class'} = 'Current';
                    }
                }
            }

            if ( defined $link->{'-class'} ) {
                $list_args->{'-class'} =
                    ( defined $list_args->{'-class'} ? $list_args->{'-class'} . ' ' . $link->{'-class'} : $link->{'-class'} );
            }

            #   link name
            my $label_text = delete $link->{'-name'};
            if ( !defined $label_text ) {
                $label_text = delete $link->{'-label_text'};
            }
            if ( !defined $label_text ) {
                my $label_phrase_id = $link->{'-label_phrase_id'};
                if ( defined $label_phrase_id ) {
                    $label_text = $phrase_util->text( { '-phrase_id' => $label_phrase_id } );
                }
            }

            #   link content
            $link_content = $link->{'-content'} || '';
            if ( !$link->{'-text_only'} ) {
                my $href;
                if ( exists $link->{'-panel'} || exists $link->{'-component'} ) {
                    $href = $self->_make_component_link($link);
                }
                else {
                    $href = $self->_make_program_link($link);
                }

                my $link_args = {
                    '-href'  => $href,
                    '-title' => $href,
                };

                if ( defined $data && ref($data) eq 'HASH' ) {
                    while ( my ( $k, $v ) = each %{$data} ) {
                        $link_args->{$k} = $v;
                    }
                }

                $link_content .= $cgi->a( $link_args, ( defined $label_text ? $label_text : $href ) );
            }
            else {
                $link_content .= $cgi->span( {}, ( defined $label_text ? $label_text : '' ) );
            }

            #   sub
            if ( defined $sub_links ) {
                $self->debug( { '-text' => 'Have sub navigation' } );
                $link_content .= $self->render(
                    {   '-links'       => $sub_links->{'-links'},
                        '-orientation' => $sub_links->{'-orientation'},
                        '-align'       => $sub_links->{'-align'},
                        '-class'       => $sub_links->{'-class'},
                        '-name'        => $name . 'Sub',
                    } );
            }
        } ## end if ( ref($link) eq 'HASH')
        else {
            $link_content = $link;
        }

        $content .= $cgi->li( $list_args, $link_content );

    } ## end foreach my $link ( @{$links...})

    $content .= <<"HTML";
</ul>
</div>
</div>
<!--   End Navigation $name -->
HTML

    return $content;
} ## end sub render

sub _make_program_link {
    my ( $self, $args ) = @_;

    #   program
    my $program = delete $args->{'-program'};
    if ( !defined $program && ( ( ref($self) =~ /\:IFace\:/sxmg ) || ( ref($self) =~ /\:Comp\:/sxmg ) ) ) {
        $program = $self->program_name;
    }

    #   need a way to abstract "iface" here
    #   -   hard "html" for now
    my $link;

    #   url
    my $url = $args->{'-url'};
    if ( !defined $url ) {
        if ( !defined $program ) {
            $self->error( { '-text' => 'Unable to determine program name, cannot make program_link' } );
            return;
        }

        #   application url
        my $application_url = $self->environment->{'APP_URL'};

        if ( $program =~ /^\/html/sxmg ) {
            $link = $application_url . $program;
        }
        else {
            $link = $application_url . '/html/' . $program;
        }
    }
    else {
        $link = $url;
    }

    #   additional params
    my $additional_params = $self->_make_link_params($args);
    if ( defined $additional_params ) {
        $link .= $additional_params;
    }

    return $link;
} ## end sub _make_program_link

sub _make_panel_link {
    my ( $self, $args ) = @_;

    return $self->_make_component_link($args);
}

sub _make_component_link {
    my ( $self, $args ) = @_;

    #   panel link
    if ( exists $args->{'-panel'} ) {
        if ( !exists $args->{'-component'} ) {
            $args->{'-component'} = $args->{'-panel'};
        }
        delete $args->{'-panel'};
    }
    if ( !defined $args->{'-component'} && ref($self) =~ /\/Comp\//sxmg ) {
        $args->{'-component'} = $self->name;
    }

    if ( !defined $args->{'-component'} ) {
        $self->error( { '-text' => 'Unable to determine component name, returning program link' } );
        return;
    }

    #   program link
    my $link = $self->_make_program_link($args);

    return $link;
}

#   make link params
sub _make_link_params {
    my ( $self, $args ) = @_;

    my $link = '';

    #   count
    my $count = 0;

    #   additional args
    foreach my $key ( keys %{$args} ) {
        my $param_key;
        if ( $key =~ /^\-(.*?)$/sxmg ) {
            $param_key = $1;
        }
        else {
            $param_key = $key;
        }

        if ( $count == 0 ) {
            $link .= '?' . $param_key . '=' . ( defined $args->{$key} ? $args->{$key} : '' );
        }
        else {
            $link .= '&' . $param_key . '=' . ( defined $args->{$key} ? $args->{$key} : '' );
        }
        $count++;
    }

    return $link;
}

1;
