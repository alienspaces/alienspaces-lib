package AlienSpaces::Util::TimeZone;

#
#   $Revision: 157 $
#   $Author: bwwallin $
#   $Date: 2010-07-05 20:51:40 +1000 (Mon, 05 Jul 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :defaults);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, UTIL_DB_MYSQL(), UTIL_SESSION(), ];
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed init super class' } );
        return;
    }

    return 1;
}

sub fetch_time_zones {
    my ( $self, $args ) = @_;

    my $statement = <<'STMT';
SELECT `time_zone_id`,
       `phrase_id`,
       `country_id`,
       `name`
FROM  `as_r_time_zone`
WHERE 1 = 1
STMT

    my @bind;
    if ( defined $args->{'-country_id'} ) {
        $statement .= <<'STMT';
AND `country_id` = ?
STMT
        push @bind, $args->{'-country_id'};
    }
    if ( defined $args->{'-time_zone_id'} ) {
        $statement .= <<'STMT';
AND `time_zone_id` = ?
STMT
        push @bind, $args->{'-time_zone_id'};
    }
    if ( defined $args->{'-name'} ) {
        $statement .= <<'STMT';
AND LOWER(`name`) = LOWER(?)
STMT
        push @bind, $args->{'-name'};
    }

    my $data = $self->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    return $data;
} ## end sub fetch_time_zones

sub fetch_time_zone {
    my ( $self, $args ) = @_;

    my $time_zone_id = $args->{'-time_zone_id'};
    my $name         = $args->{'-name'};

    if ( !defined $time_zone_id && !defined $name ) {
        $self->error(
            {   '-text' => 'Required optional arguments -time_zone_id ' . $self->dbg_value($time_zone_id) . ' and -name ' .
                    $self->dbg_value($name) . ' missing, cannot fetch time zone'
            } );
        return;
    }

    my $data = $self->fetch_time_zones(
        {   '-time_zone_id' => $time_zone_id,
            '-name'         => $name,
        } );

    my $return_data;
    if ( defined $data && scalar @{$data} == 1 ) {
        $return_data = $data->[0];
    }

    return $return_data;
}

sub convert_time_zone_time {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-from_time_zone_id', '-to_time_zone_id', '-time' ], $args ) ) {
        return;
    }

    #   bind
    my @bind;

    #   time
    push @bind, $args->{'-time'};

    #   from
    my $from_time_zone = $self->fetch_time_zone( { '-time_zone_id' => $args->{'-from_time_zone_id'} } );
    if ( !defined $from_time_zone ) {
        $self->error(
            { '-text' => 'Failed to fetch -from_zone_zone_id ' . $args->{'-from_time_zone_id'} . ', cannot convert time' } );
        return;
    }
    push @bind, $from_time_zone->{'-name'};

    #   to
    my $to_time_zone = $self->fetch_time_zone( { '-time_zone_id' => $args->{'-from_time_zone_id'} } );
    if ( !defined $to_time_zone ) {
        $self->error( { '-text' => 'Failed to fetch -to_zone_zone_id ' . $args->{'-to_time_zone_id'} . ', cannot convert time' } );
        return;
    }
    push @bind, $to_time_zone->{'-name'};

    #   epoch
    my $convert_epoch = $args->{'-convert_epoch'};

    my $statement;

    if ($convert_epoch) {
        $statement = <<'STMT';
SELECT UNIX_TIMESTAMP(CONVERT_TZ(?, ?, ?)) AS convert_time
STMT
    }
    else {
        $statement = <<'STMT';
SELECT CONVERT_TZ(?, ?, ?) AS convert_time
STMT
    }

    my $data = $self->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    $self->debug(
        {   '-text' => 'Converting -from_time_zone_name ' . $from_time_zone->{'-name'} . ' to_time_zone_name ' .
                $to_time_zone->{'-name'} . ' -time ' . $args->{'-time'} . ' -data ' . $self->dbg_value($data) } );

    my $return_time;
    if ( defined $data && scalar @{$data} == 1 ) {
        $return_time = $data->[0]->{'-convert_time'};
    }
    return $return_time;
} ## end sub convert_time_zone_time

1;
