package AlienSpaces::Util::Database;
#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {
    'username' => { '-type' => 's' },
    'database' => { '-type' => 's' },
    'password' => { '-type' => 's' },
};

use DBI;

sub new {
    my ( $class, $args ) = @_;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub init {
    my ( $self, $args ) = @_;

    $self->debug( { -text => "Initialising database" } );

    #
    #   connect
    #
    $self->connect();

    return $self->SUPER::init($args);
}

sub connect {
    my $self = $_[0];

    my $dbh = undef;
    if (   ( defined $self->database )
        && ( defined $self->username )
        && ( defined $self->password ) ) {
        $self->debug( { -text => "Connecting to " . $self->database } );

        eval { $dbh = DBI->connect( 'DBI:mysql:database=' . $self->database . ';host=localhost', $self->username, $self->password, { 'RaiseError' => 1, 'AutoCommit' => 0 } ); };
        if ($@) {
            $self->debug( { -text => "No database available $@" } );
            return 0;
        }
        $dbh->{mysql_auto_reconnect} = 1;

        $self->dbh($dbh);

        $self->debug( { -text => 'Connected to database' } );
        return 1;

    }
    else {
        $self->debug( { -text => 'Missing connection parameters' } );
        return 0;
    }
}

sub prepare {
    my ( $self, $statement ) = @_;

    my $sth = $self->dbh->prepare($statement);

    return $sth;
}

sub do {
    my ( $self, $statement ) = @_;

    return $self->dbh->do($statement);
}

sub last_insert_id {
    my $self = $_[0];
    return $self->dbh->{q{mysql_insertid}};
}

sub disconnect {
    my $self = $_[0];

    $self->dbh->disconnect();
}

sub commit {
    my $self = $_[0];

    $self->dbh->commit();
}

sub rollback {
    my $self = $_[0];

    $self->dbh->rollback();
}

1;
