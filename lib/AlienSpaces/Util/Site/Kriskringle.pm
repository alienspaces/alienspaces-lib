package AlienSpaces::Util::Site::Kriskringle;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);
use Config::JSON;
use Digest::SHA1;

use AlienSpaces::Constants qw(
    :util
    :comp
    :program
    :defaults
    :mode
    :error-codes-business
);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use constant {
    KRISKRINGLE_PARTICIPANT_SESSION_KEY => '-kriskringle_participant_digest',
    KRISKRINGLE_MAX_ASSIGN_ATTEMPTS     => 100,
};

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        return;
    }

    #   process config
    if ( !$self->process_config() ) {
        $self->error( { '-text' => 'Failed to process config' } );
        return;
    }

    return 1;
}

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_EMAIL(),      # email
        UTIL_SESSION(),    # session
        UTIL_MESSAGE(),    # message
    ];

    return $util_list;
}

#   process config
sub process_config {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Processing config' } );

    #   load config
    my $config = $self->load_config();

    my $participants = $config->get('participants');

    #   verify config participants
    $participants = $self->verify_config_participants( { '-participants' => $participants } );
    if ( !defined $participants ) {
        $self->error( { '-text' => 'Failed to verify participants' } );
        return;
    }

    #   reset config participants
    my $reset_participants = $config->get('reset_participants');
    if ($reset_participants) {
        $participants = $self->reset_config_participants( { '-participants' => $participants } );
        if ( !defined $participants ) {
            $self->error( { '-text' => 'Failed to reset participants' } );
            return;
        }
        $config->set( 'reset_participants', 0 );
    }

    $config->set( 'participants', $participants );

    $self->debug( { '-text' => 'Processed config' } );

    return 1;
} ## end sub process_config

#   load config
sub load_config {
    my ( $self, $args ) = @_;

    my $app_home    = $self->environment->{'APP_HOME'};
    my $config_file = $app_home . '/conf/site/kriskringle.json';
    if ( !-f $config_file ) {
        $self->error( { '-text' => 'Config file ' . $config_file . ' does not exist, cannot process config' } );
        return;
    }

    $self->debug( { '-text' => 'Loading config' } );

    my $config = Config::JSON->new($config_file);
    if ( !defined $config ) {
        $self->error( { '-text' => 'Failed to parse config, cannot process config' } );
        return;
    }

    return $config;
}

#   verify config participants
sub verify_config_participants {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-participants'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $participants = $args->{'-participants'};

    foreach my $participant ( @{$participants} ) {

        #   digest
        if ( !defined $participant->{'digest'} ) {

            my $sha1 = Digest::SHA1->new;
            $sha1->add( $participant->{'name'} );
            my $digest = $sha1->hexdigest;
            $participant->{'digest'} = $digest;
            $participant->{'link'} =
                'http://dev.alienspaces.net/html/site/kriskringle/home?component=Site::Kriskringle::Participant&action=select&digest='
                . $digest;
        }
    }

    return $participants;
}

sub reset_config_participants {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-participants'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $participants = $args->{'-participants'};

    $self->debug( { '-text' => 'Resetting config' } );

    foreach my $participant ( @{$participants} ) {

        #   delete assigned
        delete $participant->{'assigned'};

        #   delete wishlist
        delete $participant->{'wishes'};
    }

    #   assign participants
    $participants = $self->assign_participants( { '-participants' => $participants } );

    $self->debug( { '-text' => 'Reset config' } );

    return $participants;
}

sub assign_participants {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-participants'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $participants = $args->{'-participants'};

    my $participant_count = scalar @{$participants};
    my $all_assigned      = 0;
    my $attempts          = 0;

    my $assigned            = {};
    my $has_assigned_digest = {};
    my $has_assigned_name   = {};

ASSIGN_ATTEMPT:
    while ( !$all_assigned && ( $attempts < KRISKRINGLE_MAX_ASSIGN_ATTEMPTS ) ) {

        $assigned            = {};
        $has_assigned_digest = {};
        $has_assigned_name   = {};

    ASSIGN_PARTICIPANT:
        foreach my $participant ( @{$participants} ) {
            $self->debug( { '-text' => 'Looking for assigment for -name ' . $participant->{'name'} } );

            my $exclude = $participant->{'exclude'};

            my @available = ();

        POSSIBLE_PARTICIPANT:
            foreach my $possible_participant ( @{$participants} ) {
                $self->debug( { '-text' => 'Testing -name ' . $possible_participant->{'name'} } );

                #   is me
                if ( $possible_participant->{'name'} eq $participant->{'name'} ) {
                    $self->debug( { '-text' => 'Same -name ' . $possible_participant->{'name'} } );
                    next POSSIBLE_PARTICIPANT;
                }

                #   already assigned
                if ( exists $assigned->{ $possible_participant->{'name'} } ) {
                    $self->debug( { '-text' => 'Already assigned -name ' . $possible_participant->{'name'} } );
                    next POSSIBLE_PARTICIPANT;
                }

                #   exclude
                foreach my $exclude_participant ( @{$exclude} ) {
                    if ( $possible_participant->{'name'} eq $exclude_participant ) {
                        $self->debug( { '-text' => 'In exclude list -name ' . $possible_participant->{'name'} } );
                        next POSSIBLE_PARTICIPANT;
                    }
                }

                push @available, $possible_participant;
            }

            #   none available
            if ( !scalar @available ) {
                $self->debug( { '-text' => 'None available, next attempts' } );
                $attempts++;
                last ASSIGN_PARTICIPANT;
            }

            my $number = int( rand( scalar @available ) );
            $assigned->{ $available[$number]->{'name'} } = $participant->{'name'};

            $has_assigned_digest->{ $participant->{'digest'} } = $available[$number]->{'digest'};
            $has_assigned_name->{ $participant->{'name'} }     = $available[$number]->{'name'};

            $self->debug( { '-text' => 'Assigned -number ' . $number . ' -name ' . $participants->[$number]->{'name'} } );
        } ## end ASSIGN_PARTICIPANT: foreach my $participant ( @...)

        $self->debug(
            {   '-text' => 'Testing -assigned ' .
                    ( scalar( keys %{$assigned} ) ) . ' against -participant_count ' . $participant_count
            } );
        if ( scalar( keys %{$assigned} ) == $participant_count ) {
            $all_assigned = 1;
        }

        $attempts++;

        $self->debug( { '-text' => 'Have -attempts ' . $attempts . ' -all_assigned ' . $all_assigned } );

    } ## end ASSIGN_ATTEMPT: while ( !$all_assigned &&...)

    if ( !$all_assigned ) {
        $self->error( { '-text' => 'All not assigned ' . $self->dbg_value($has_assigned_name) } );
        return;
    }

    $self->debug(
        {   '-text' => 'All assigned -names ' .
                $self->dbg_value($has_assigned_name) . ' -digests ' . $self->dbg_value($has_assigned_digest) } );

    foreach my $participant ( @{$participants} ) {
        $participant->{'assigned'} = $has_assigned_digest->{ $participant->{'digest'} };
    }

    return $participants;
} ## end sub assign_participants

sub get_participant {
    my ( $self, $args ) = @_;

    my $name   = $args->{'-name'};
    my $digest = $args->{'-digest'};

    if ( !defined $name && !defined $digest ) {
        $self->error( { 'text' => 'Missing argument -name or -digest, cannot get participant' } );
        return;
    }

    #   load config
    my $config = $self->load_config();

    my $participants = $config->get('participants');

    foreach my $participant ( @{$participants} ) {
        if ( defined $name && $participant->{'name'} eq $name ) {
            $self->debug(
                {   '-text' => 'Returning -participant ' .
                        $self->dbg_value($participant) . ', given -args ' . $self->dbg_value($args) } );
            return $participant;
        }
        if ( defined $digest && $participant->{'digest'} eq $digest ) {
            $self->debug(
                {   '-text' => 'Returning -participant ' .
                        $self->dbg_value($participant) . ', given -args ' . $self->dbg_value($args) } );
            return $participant;
        }
    }

    $self->error( { '-text' => 'Failed to find participant given -args ' . $self->dbg_value($args) } );

    return;
} ## end sub get_participant

sub update_participant {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-participant'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $participant = $args->{'-participant'};

    #   load config
    my $config = $self->load_config();
    if ( !defined $config ) {
        $self->error( { '-text' => 'Unable to load config, cannot update recipient' } );
        return;
    }

    my $config_participants = $config->get('participants');

    my $found = 0;
    foreach my $config_participant ( @{$config_participants} ) {
        if ( $config_participant->{'digest'} eq $participant->{'digest'} ) {
            $self->debug( { '-text' => 'Found participant, updating' } );
            %{$config_participant} = %{$participant};
            $found = 1;
            last;
        }
    }

    if ($found) {
        $self->debug( { '-text' => 'Updating participants' } );
        $config->set( 'participants', $config_participants );
    }

    return 1;
} ## end sub update_participant

sub get_session_participant {
    my ( $self, $args ) = @_;

    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $participant_digest = $session_util->get_session( { '-key' => KRISKRINGLE_PARTICIPANT_SESSION_KEY } );
    if ( !defined $participant_digest ) {
        $self->error( { '-text' => 'Failed to get session stored participant digest, cannnot get session participant' } );
        return;
    }

    my $participant = $self->get_participant( { '-digest' => $participant_digest } );
    if ( !defined $participant ) {
        $self->error(
            { '-text' => 'No participant found with session stored participant digest ' . $self->dbg_value($participant_digest) } );
        return;
    }

    return $participant;
}

sub set_session_participant {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-digest'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $digest = $args->{'-digest'};

    my $participant = $self->get_participant( { '-digest' => $digest } );
    if ( !defined $participant ) {
        $self->error(
            { '-text' => 'No participant found with digest ' . $self->dbg_value($digest) . ', cannot set session participant' } );
        return;
    }

    $session_util->set_session( { '-key' => KRISKRINGLE_PARTICIPANT_SESSION_KEY, '-value' => $digest } );

    return 1;
}

sub get_assigned_participant {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-digest'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $digest = $args->{'-digest'};

    my $participant = $self->get_participant( { '-digest' => $digest } );
    if ( !defined $participant ) {
        $self->error(
            { '-text' => 'No participant found with digest ' . $self->dbg_value($digest) . ', cannot set session participant' } );
        return;
    }

    my $assigned_digest = $participant->{'assigned'};
    if ( !defined $assigned_digest ) {
        $self->debug( { '-text' => 'No one assigned to participant with -digest ' . $digest } );
        return;
    }

    my $assigned_participant = $self->get_participant( { '-digest' => $assigned_digest } );
    if ( !defined $assigned_participant ) {
        $self->error( { '-text' => 'No participant found for assigned partipant -digest' . $digest } );
        return;
    }

    return $assigned_participant;
} ## end sub get_assigned_participant

sub get_participant_wishes {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-digest'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $digest = $args->{'-digest'};

    my $participant = $self->get_participant( { '-digest' => $digest } );
    if ( !defined $participant ) {
        $self->error( { '-text' => 'No participant found with digest ' . $self->dbg_value($digest) } );
        return;
    }

    return $participant->{'wishes'} || [];
}

sub get_participant_wish {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-digest', '-id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $digest = $args->{'-digest'};
    my $id     = $args->{'-id'};

    my $participant = $self->get_participant( { '-digest' => $digest } );
    if ( !defined $participant ) {
        $self->error( { '-text' => 'No participant found with digest ' . $self->dbg_value($digest) } );
        return;
    }

    my $wishes = $participant->{'wishes'} || [];
    foreach my $wish ( @{$wishes} ) {
        if ( $wish->{'id'} eq $id ) {
            return $wish;
        }
    }

    return;
}

sub add_participant_wish {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-digest', '-text' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $digest = $args->{'-digest'};
    my $text   = $args->{'-text'};
    my $url    = $args->{'-url'};

    my $participant = $self->get_participant( { '-digest' => $digest } );
    if ( !defined $participant ) {
        $self->error( { '-text' => 'No participant found with digest ' . $self->dbg_value($digest) } );
        return;
    }

    #   id
    my $sha1 = Digest::SHA1->new;
    $sha1->add($text);
    my $id = $sha1->hexdigest;

    my $wishes = $participant->{'wishes'} || [];
    push @{$wishes},
        {
        'text' => $text,
        'url'  => $url,
        'id'   => $id,
        };

    $participant->{'wishes'} = $wishes;

    $self->update_participant( { '-participant' => $participant } );

    return 1;
} ## end sub add_participant_wish

sub update_participant_wish {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-digest', '-id', '-text' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $digest = $args->{'-digest'};
    my $id     = $args->{'-id'};
    my $text   = $args->{'-text'};
    my $url    = $args->{'-url'};

    my $participant = $self->get_participant( { '-digest' => $digest } );
    if ( !defined $participant ) {
        $self->error( { '-text' => 'No participant found with digest ' . $self->dbg_value($digest) } );
        return;
    }

    my $new_wishes = [];
    my $wishes = $participant->{'wishes'} || [];

    foreach my $current_wish ( @{$wishes} ) {
        if ( $current_wish->{'id'} eq $id ) {
            $current_wish->{'text'} = $text;
            $current_wish->{'url'}  = $url;
        }

        push @{$new_wishes}, $current_wish;
    }

    $participant->{'wishes'} = $new_wishes;

    $self->update_participant( { '-participant' => $participant } );

    return 1;
} ## end sub update_participant_wish

sub delete_participant_wish {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-digest', '-id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $digest = $args->{'-digest'};
    my $id     = $args->{'-id'};

    $self->debug( { '-text' => 'Have -args ' . $self->dbg_value($args) } );

    my $participant = $self->get_participant( { '-digest' => $digest } );
    if ( !defined $participant ) {
        $self->error( { '-text' => 'No participant found with digest ' . $self->dbg_value($digest) } );
        return;
    }

    my $new_wishes = [];
    my $wishes = $participant->{'wishes'} || [];

    foreach my $current_wish ( @{$wishes} ) {
        if ( $current_wish->{'id'} eq $id ) {
            $self->debug( { '-text' => 'Deleting wish -id ' . $id } );
            next;
        }

        push @{$new_wishes}, $current_wish;
    }

    $participant->{'wishes'} = $new_wishes;

    $self->update_participant( { '-participant' => $participant } );

    return 1;
} ## end sub delete_participant_wish

sub get_party {
    my ( $self, $args ) = @_;

    #   load config
    my $config = $self->load_config();
    if ( !defined $config ) {
        $self->error( { '-text' => 'Unable to load config, cannot update recipient' } );
        return;
    }

    my $party = $config->get('party');

    return $party;
}

sub get_gift {
    my ( $self, $args ) = @_;

    #   load config
    my $config = $self->load_config();
    if ( !defined $config ) {
        $self->error( { '-text' => 'Unable to load config, cannot update recipient' } );
        return;
    }

    my $gift = $config->get('gift');

    return $gift;
}

sub get_title {
    my ( $self, $args ) = @_;

    #   load config
    my $config = $self->load_config();
    if ( !defined $config ) {
        $self->error( { '-text' => 'Unable to load config, cannot update recipient' } );
        return;
    }

    my $gift = $config->get('title');

    return $gift;
}

1;
