package AlienSpaces::Util::Template;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties { 'template' => { '-type' => 's' }, };

use constant { DEFAULT_LANGUAGE_CODE => 'en', };

#   Template Toolkit
use Template;

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_DB_RECORD_R_LANGUAGE(),
    ];

    return $util_list;
}

#   config
sub config {
    my ( $self, $args ) = @_;

    #   namespaces
    my $template_paths = $self->environment->{'APP_TEMPLATE_PATHS'};
    if ( !defined $template_paths ) {
        $self->error( { '-text' => 'Missing template paths, cannot configure' } );
        return;
    }

    my $include_path = [];

    foreach my $template_path ( @{$template_paths} ) {

        push @{$include_path}, (

            #   emails
            $template_path . '/email',

            #   iface
            $template_path . '/iface',

            #   comp
            $template_path . '/comp',

            #   util
            $template_path . '/util',

        );

    }

    my $config = {

        #   include path
        'INCLUDE_PATH' => $include_path,
    };

    return $config;
} ## end sub config

#   language codes
my $language_codes = { 'en' => 1, };

sub language_codes {
    return $language_codes;
}

#   init
sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->stack( { '-text' => 'Failed to initialise super class' } );
        return;
    }

    #   config
    my $config = $self->config;
    $self->debug( { '-text' => 'Creating template object with -config ' . $self->dbg_value($config) } );

    #   template
    my $template = Template->new($config);
    if ( !defined $template ) {
        $self->error( { '-text' => 'Failed to create template toolkit object :' . $Template::ERROR } );
        return;
    }

    $self->template($template);

    return 1;
}

#   process
sub process {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-file'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $file = $args->{'-file'};
    my $vars = $args->{'-vars'};

    if ( ref $file ne 'ARRAY' ) {
        $file = [$file];
    }

    #   language
    my $language = $self->get_template_language();

    #   template
    my $template = $self->template();
    if ( !defined $template ) {
        $self->error( { '-text' => 'Missing template object, cannot process template' } );
        return;
    }

    #   vars
    $vars = $self->get_template_vars( { '-vars' => $vars } );

    $self->debug( { '-text' => 'Processing template -file ' . $self->dbg_value($file) . ' -vars ' . $self->dbg_value($vars) } );

    my $content;

PROCESS_TEMPLATE:
    foreach my $tt_file ( @{$file} ) {

        #   file
        $tt_file = $tt_file . '.' . $language . '.tt';

        if ( !$template->process( $tt_file, $vars, \$content ) ) {
            $self->debug( { '-text' => 'Failed processing template :' . $template->error } );
            next PROCESS_TEMPLATE;
        }
        
        last PROCESS_TEMPLATE;
    }

    return $content;
} ## end sub process

#   get template language
sub get_template_language {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   language dn rec util
    my $language_db_rec_util = $self->utils->{ UTIL_DB_RECORD_R_LANGUAGE() };

    #   language id
    my $language_id = $user_util->user_language();
    if ( !defined $language_id ) {
        $self->debug( { '-text' => 'No language id defined, returning default language' } );
        return DEFAULT_LANGUAGE_CODE;
    }

    $self->debug( { '-text' => 'Fetching language for -language_id ' . $self->dbg_value($language_id) } );

    #   language
    my $language = $language_db_rec_util->fetch_one( { '-language_id' => $language_id, } );

    if ( !defined $language ) {
        $self->debug( { '-text' => 'No language defined, returning default language' } );
        return DEFAULT_LANGUAGE_CODE;
    }

    $self->debug( { '-text' => 'Have -language ' . $self->dbg_value($language) } );

    if ( !$self->language_codes->{ $language->{'-code'} } ) {
        $self->debug(
            {   '-text' => 'Language -code ' .
                    $self->dbg_value( $language->{'-code'} ) . ' not supported, returning default language'
            } );
        return DEFAULT_LANGUAGE_CODE;
    }

    return $language->{'-code'};
} ## end sub get_template_language

#   get template vars
sub get_template_vars {
    my ( $self, $args ) = @_;

    #   vars
    my $vars = $args->{'-vars'};
    if ( !defined $vars ) {
        $vars = {};
    }

    #   environment
    $vars = $self->get_template_env_vars( { '-vars' => $vars } );

    #   user
    $vars = $self->get_template_user_vars( { '-vars' => $vars } );

    return $vars;
}

sub get_template_env_vars {
    my ( $self, $args ) = @_;

    #   vars
    my $vars = $args->{'-vars'};
    if ( !defined $vars ) {
        $vars = {};
    }

    #   environment
    #   - add available environment variables
    my $environment = $self->environment();
    while ( my ( $k, $v ) = each %{$environment} ) {
        if ( lc($k) =~ /db/sxmg ) {
            next;
        }
        my $var_key = lc($k);
        $var_key =~ s/^\-//sxm;
        $vars->{$var_key} = $v;
    }

    return $vars;
}

sub get_template_user_vars {
    my ( $self, $args ) = @_;

    #   vars
    my $vars = $args->{'-vars'};
    if ( !defined $vars ) {
        $vars = {};
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user id
    my $user_id = $args->{'-user_id'} || $user_util->user_id();
    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'No available user id, not returning user vars' } );
        return $vars;
    }

    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( defined $user ) {
        $vars->{'user'} = {};
        while ( my ( $k, $v ) = each %{$user} ) {
            my $var_key = lc($k);
            $var_key =~ s/^\-//sxm;
            $vars->{'user'}{$var_key} = $v;
        }
    }

    return $vars;
} ## end sub get_template_user_vars

1;
