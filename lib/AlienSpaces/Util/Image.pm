package AlienSpaces::Util::Image;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#
#   provides an interface for storing and retrieving images
#
#   this abstraction allows us to build in remote image storage if need be
#   and will most likely be back engineered into AlienSpaces
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use MIME::Base64 qw(encode_base64 decode_base64);
use Digest::MD5 qw(md5_hex);
use File::Path qw(make_path);

use constant {
    DEFAULT_IMAGE_PATH => 'img',
    DEFAULT_CATEGORY   => 'default',
};

sub util_list {
    my ( $self, $args ) = @_;

    return [
        @{ $self->SUPER::util_list },

    ];
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed init super class' } );
        return;
    }

    return 1;
}

#   store image content
sub store_image {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-id', '-image', '-type' ], $args ) ) {
        return;
    }

    my $id       = $args->{'-id'};          #   an identifier to encode for the file name
    my $category = $args->{'-category'};    #   array of categories to index / store the image
    my $image    = $args->{'-image'};       #   the data
    my $encoding = $args->{'-encoding'};    #   the encoding of the data
    my $type     = $args->{'-type'};        #   the file type

    if (!defined $type) {
        $self->error({'-text' => 'Missing parameter -type, cannot store image '});
        return;
    }
    
    my $path = $args->{'-path'};
    if (!defined $path) {
        $path = $self->environment->{'APP_WEB_HOME'} . '/' . DEFAULT_IMAGE_PATH;
    }
    
    if ( !defined $category ) {
        $category = DEFAULT_CATEGORY;
    }

    #   decode if encoded
    if ( defined $encoding ) {
        $image = $self->decode_image(
            {   '-image'    => $image,
                '-encoding' => $encoding,
            } );
    }

    #   create digest of image id portion
    my $filename = $self->make_filename(
        {   '-path'     => $path,
            '-category' => $category,
            '-id'       => $id,
            '-type'     => $type,

        } );

    #   store (all stored locally for now)
    if (!$self->store_image_local(
            {   '-filename' => $filename,
                '-image'    => $image,
            } )
        ) {
        $self->error( { '-text' => 'Failed to store image -filename ' . $filename } );
        return;
    }

    return 1;
} ## end sub store_image

#   store image on local filesytem
sub store_image_local {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-image', '-filename' ], $args ) ) {
        return;
    }

    #   image
    my $image = $args->{'-image'};

    #   filename
    my $filename = $args->{'-filename'};

    #   verify local path
    if ( !$self->verify_local_path( { '-filename' => $filename } ) ) {
        $self->error( { '-text' => 'Failed to verify local path, cannot store image locally' } );
        return;
    }

    my $fh;
    open $fh, '>', $filename or croak 'Failed to open -filename ' . $filename . ' for writing :' . $OS_ERROR;
    print {$fh} $image;
    close $fh or croak 'Failed to close -filename ' . $filename . ' :' . $OS_ERROR;

    return 1;
}

#   store image on remote filesystem
sub store_image_remote {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-image', '-filename' ], $args ) ) {
        return;
    }

    return 1;
}

sub verify_local_path {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-filename'], $args ) ) {
        return;
    }

    #   filename
    my $filename = $args->{'-filename'};

    #   must start from root
    if ( !$filename =~ /^\//sxmg ) {
        $self->error( { '-text' => 'Filename -filename ' . $filename . ' does not start at root directory' } );
        return;
    }

    #   remove actual filename from path
    $filename =~ s/^(.*\/).*$/$1/sxmg;

    $self->debug( { '-text' => 'Testing -path ' . $filename } );

    if ( !-d $filename ) {
        $self->debug( { '-text' => 'Creating path' } );
        make_path($filename) or croak 'Failed to create path :' . $OS_ERROR;
    }

    return 1;
}

sub make_filename {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-path', '-id', '-type' ], $args ) ) {
        return;
    }

    my $category = $args->{'-category'};
    if (!ref($category)) {
        $category = [$category];
    }
    my $id       = $args->{'-id'};
    my $type     = $args->{'-type'};

    my $filename = $args->{'-path'} || '';

    #   directory path
    if ( defined $category && scalar @{$category} ) {
        foreach my $c ( @{$category} ) {
            $filename .= '/' . md5_hex($c);
        }
    }

    #   image filename
    $filename .= '/' . md5_hex($id) . '.' . $type;

    return $filename;
}

sub decode_image {
    my ( $self, $args ) = @_;

    my $image    = $args->{'-image'};
    my $encoding = $args->{'-encoding'};

    if ( $encoding eq 'base64' ) {
        $image = decode_base64($image);
    }

    return $image;
}

sub encode_image {
    my ( $self, $args ) = @_;

    my $image    = $args->{'-image'};
    my $encoding = $args->{'-encoding'};

    if ( $encoding eq 'base64' ) {
        $image = encode_base64($image);
    }

    return $image;
}

#   get digest
sub get_digest {
    my ( $self, $args ) = @_;

    my $string = $args->{'-string'};
    my $digest;

    return $digest;
}

#   fetch image content
sub fetch_image {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-id'], $args ) ) {
        return;
    }

    my $image = {};

    return $image;
}

#   return image uri
sub fetch_image_uri {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-id', '-type' ], $args ) ) {
        return;
    }

    #   path
    if ( !defined $args->{'-path'} ) {
        $args->{'-path'} = '/' . DEFAULT_IMAGE_PATH;
    }

    my $image_uri = $self->make_filename($args);

    return $image_uri;
}

1;

