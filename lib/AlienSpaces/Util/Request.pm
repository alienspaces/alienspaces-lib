package AlienSpaces::Util::Request;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :request :program);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_PHRASE(),
        UTIL_EMAIL(),
        UTIL_TEMPLATE(),
        UTIL_CONTACT(),

        #   db interfaces
        UTIL_DB_TABLE(),

        #   output
        UTIL_OUTPUT_HTML_NAVIGATION(),
    ];

    return $util_list;
}

sub fetch_requests {
    my ( $self, $args ) = @_;

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    my $statement = <<'STMT';
SELECT 
    `request_id`,
    `request_type_id`,
    `user_id`,
    `request_status_id`,
    `request_key`,
    `entry_ts`,
    `expires_ts`,
    CASE WHEN (CURRENT_TIMESTAMP - `expires_ts`) > 0 THEN 1 ELSE 0 END AS expired 
FROM `as_request`
WHERE 1 = 1
STMT

    my @bind       = ();
    my $request_id = $args->{'-request_id'};
    if ( defined $request_id ) {
        $statement .= <<'STMT';
AND `request_id` = ?
STMT
        push @bind, $request_id;
    }

    my $data = $db_table_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    return $data;
} ## end sub fetch_requests

sub fetch_one_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_id'], $args ) ) {
        return;
    }

    my $data = $self->fetch_requests($args);

    my $return_data;
    if ( defined $data && ref($data) eq 'ARRAY' ) {
        $return_data = $data->[0];
    }

    return $return_data;
}

sub fetch_request_type {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_type_id'], $args ) ) {
        return;
    }

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   request type id
    my $request_type_id = $args->{'-request_type_id'};

    my $statement = <<'STMT';
SELECT `request_type_id`, `phrase_id`, `validity_minutes`, `description`
FROM   `as_r_request_type`
WHERE  `request_type_id` = ?
STMT

    my @bind = ($request_type_id);
    my $data = $db_table_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    my $return_data;
    if ( defined $data && scalar @{$data} == 1 ) {
        $return_data = $data->[0];

        my $phrase_text = $phrase_util->text( { '-phrase_id' => $return_data->{'-phrase_id'} } );
        $return_data->{'-request_type'} = $phrase_text;
    }

    return $return_data;
} ## end sub fetch_request_type

sub add_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_type_id', '-user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    my $user_id         = delete $args->{'-user_id'};
    my $request_type_id = delete $args->{'-request_type_id'};
    my $request_data    = delete $args->{'-request_data'};

    #   approve url
    my $approve_url = delete $args->{'-approve_url'};
    if ( !defined $approve_url ) {
        $approve_url = PROGRAM_SITE_REQUEST_APPROVE;
    }

    #   deny url
    my $deny_url = delete $args->{'-deny_url'};
    if ( !defined $deny_url ) {
        $deny_url = PROGRAM_SITE_REQUEST_DENY;
    }

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $user_id } );
        return;
    }

    #   accept alternative email address for initial request submission
    my $email_address = delete $args->{'-email_address'};
    if ( defined $email_address ) {
        $self->debug(
            { '-text' => 'Using alternative requestor recipient -user_id ' . $user_id . ' -email_address ' . $email_address } );
        $user->{'-email_address'} = $email_address;
    }

    #   request type
    my $request_type = $self->fetch_request_type( { '-request_type_id' => $request_type_id, } );
    if ( !defined $request_type ) {
        $self->error( { '-text' => 'Failed to fetch -request_type_id ' . $request_type_id } );
        return;
    }

    #   validity minutes
    my $validity_minutes = $request_type->{'-validity_minutes'};
    if ( !defined $validity_minutes ) {
        $validity_minutes = 0;
    }

    #   request key
    my $request_key = $self->random_key();

    #   insert request
    my $statement = <<'STMT';
INSERT INTO `as_request` (
    `request_type_id`,
    `user_id`,
    `request_status_id`,
    `request_key`,
    `expires_ts`
) VALUES (
    ?,
    ?,
    ?,
    ?,
    DATE_ADD(CURRENT_TIMESTAMP, INTERVAL ? MINUTE)
)
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $request_type_id );
        $sth->bind_param( ++$bind, $user_id );
        $sth->bind_param( ++$bind, REQUEST_STATUS_NEW );
        $sth->bind_param( ++$bind, $request_key );
        $sth->bind_param( ++$bind, $validity_minutes );

        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to insert request record :' . $EVAL_ERROR } );
        return;
    }

    #   request_id
    my $request_id = $self->dbh->last_insert_id();
    if ( !defined $request_id ) {
        $self->error( { '-text' => 'Failed to return request id from last insert id' } );
        return;
    }

    #   request data
    #   - Not all requests have to specify data changes through the generic method
    #     For instance the password reset request handles the return approve
    #     and deny url and new password setting itself
    if ( defined $request_data ) {

        #   insert request details
        $statement = <<'STMT';
INSERT INTO `as_request_data` (
    `request_id`,
    `table_name`,
    `column_name`,
    `pk_value`,
    `current_value`,
    `new_value`
) VALUES (
    ?,
    ?,
    ?,
    ?,
    ?,
    ?
)        
STMT

        #   current data values
        my $current_data = $self->fetch_request_current_data( { '-request_data' => $request_data, } );

        $success = eval {

            my $sth = $self->dbh->prepare($statement);

        PROCESS_REQUEST_RECORDS:
            foreach my $request_data_record ( @{$request_data} ) {

                my $table_name  = $request_data_record->{'-table_name'};
                my $column_name = $request_data_record->{'-column_name'};
                my $pk_value    = $request_data_record->{'-pk_value'};
                my $new_value   = $request_data_record->{'-new_value'};

                my $current_value;
                if ( defined $pk_value &&
                    defined $current_data &&
                    exists $current_data->{$table_name} &&
                    exists $current_data->{$table_name}->{$pk_value} ) {
                    $current_value = $current_data->{$table_name}->{$pk_value}->{ '-' . $column_name };
                }

                #   required insert
                if ( !defined $table_name || !defined $column_name || !defined $new_value ) {
                    $self->error(
                        { '-text' => 'Request data record missing information ' . $self->dbg_value($request_data_record) } );
                    next PROCESS_REQUEST_RECORDS;
                }

                #   required update
                if ( defined $pk_value ) {
                    if ( !exists $current_data->{$table_name} || !exists $current_data->{$table_name}->{$pk_value} ) {
                        $self->error(
                            { '-text' => 'Was not able to find current record ' . $self->dbg_value($request_data_record) } );
                        next PROCESS_REQUEST_RECORDS;
                    }
                }

                my $bind = 0;
                $sth->bind_param( ++$bind, $request_id );
                $sth->bind_param( ++$bind, $table_name );
                $sth->bind_param( ++$bind, $column_name );
                $sth->bind_param( ++$bind, $pk_value );
                $sth->bind_param( ++$bind, $current_value );
                $sth->bind_param( ++$bind, $new_value );

                $sth->execute();
            } ## end PROCESS_REQUEST_RECORDS: foreach my $request_data_record...

            $sth->finish();

            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to insert request data records :' . $EVAL_ERROR } );
            return;
        }
    } ## end if ( defined $request_data)

    #   insert status history record
    if (!$self->add_request_status_history_record(
            {   '-request_id'            => $request_id,
                '-user_id'               => $user_id,
                '-new_request_status_id' => REQUEST_STATUS_NEW,
            } )
        ) {
        $self->error( { '-text' => 'Failed to add request status history record' } );
        return;
    }

    #   send request messages
    if (!$self->add_request_messages(
            {   '-request_id'  => $request_id,
                '-user'        => $user,
                '-approve_url' => $approve_url,
                '-deny_url'    => $deny_url,
            } )
        ) {
        $self->error( { '-text' => 'Failed to send request messages' } );
        return;
    }

    return 1;
} ## end sub add_request

sub key_deny_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_id', '-request_key', '-user_id', ], $args ) ) {
        return;
    }

    return $self->deny_request($args);
}

sub deny_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_id', '-user_id', ], $args ) ) {
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user id
    my $user_id = $args->{'-user_id'};

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $self->dbg_value($user_id) } );
        return;
    }

    #   request id
    my $request_id = $args->{'-request_id'};

    #   request key
    my $request_key = $args->{'-request_key'};

    #   authenticate request
    #   - we are repeating some checks here but needed to break out
    #     request authentication so external programs could authenticate
    #     a request without approving it yet
    if (!$self->authenticate_request(
            {   '-user_id'           => $user_id,
                '-request_id'        => $request_id,
                '-request_key'       => $request_key,
                '-request_status_id' => REQUEST_STATUS_NEW
            } )
        ) {
        $self->error( { '-text' => 'Failed to authenticate -request_id ' . $self->dbg_value($request_id) } );
        return;
    }

    #   request
    my $request = $self->fetch_one_request( { '-request_id' => $request_id } );
    if ( !defined $request ) {
        $self->debug( { '-text' => 'Failed to fetch -request_id ' . $self->dbg_value($request_id) } );
        return;
    }

    #   update request
    my $statement = <<'STMT';
UPDATE `as_request` 
SET    `request_status_id` = ?
WHERE  `request_id` = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, REQUEST_STATUS_DENIED );
        $sth->bind_param( ++$bind, $request_id );

        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to deny -request_id ' . $request_id . ' :' . $EVAL_ERROR } );
        return;
    }

    #   insert status history record
    if (!$self->add_request_status_history_record(
            {   '-request_id'            => $request_id,
                '-user_id'               => $user_id,
                '-new_request_status_id' => REQUEST_STATUS_DENIED,
                '-old_request_status_id' => REQUEST_STATUS_NEW,
            } )
        ) {
        $self->error( { '-text' => 'Failed to add request status history record' } );
        return;
    }

    #   send request messages
    if (!$self->add_request_messages(
            {   '-request_id' => $request_id,
                '-user'       => $user,
            } )
        ) {
        $self->error( { '-text' => 'Failed to send request messages' } );
        return;
    }

    return 1;
} ## end sub deny_request

sub expire_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_id'], $args ) ) {
        return;
    }

    #   request id
    my $request_id = $args->{'-request_id'};

    #   update request
    my $statement = <<'STMT';
UPDATE `as_request` 
SET    `request_status_id` = ?
WHERE  `request_id` = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, REQUEST_STATUS_EXPIRED );
        $sth->bind_param( ++$bind, $request_id );

        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to expire -request_id ' . $request_id . ' :' . $EVAL_ERROR } );
        return;
    }

    return 1;
} ## end sub expire_request

sub key_approve_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_id', '-request_key', '-user_id', ], $args ) ) {
        return;
    }

    return $self->approve_request($args);
}

#   authenticate request
#   - ensures a request is known and valid to be approved
sub authenticate_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_id', '-user_id', ], $args ) ) {
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user id
    my $user_id = $args->{'-user_id'};

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $self->dbg_value($user_id) } );
        return;
    }

    #   request id
    my $request_id = $args->{'-request_id'};
    if ( !defined $request_id ) {
        $self->error( { '-text' => 'Missing request id, cannot deny request' } );
        return;
    }

    #   request
    my $request = $self->fetch_one_request( { '-request_id' => $request_id } );
    if ( !defined $request ) {
        $self->error( { '-text' => 'Failed to fetch -request_id ' . $self->dbg_value($request_id) } );
        return;
    }

    #   verify request key if it exists
    my $request_key = $args->{'-request_key'};
    if ( defined $request_key && $request_key ne $request->{'-request_key'} ) {
        $self->error(
            {   '-text' => 'Submitted -request_key ' .
                    $request_key . ' does not match request -request_key ' . $request->{'-request_key'} } );
        return;
    }

    #   request status id
    #   - can pass in an expected request status otherwise will default
    #     to ensuring the request status is a 'new' request
    my $request_status_id = $args->{'-request_status_id'};
    if ( !defined $request_status_id ) {
        $request_status_id = REQUEST_STATUS_NEW;
    }

    if ( $request_status_id != $request->{'-request_status_id'} ) {
        $self->error( { '-text' => 'Incorrect request status ' . $request_status_id } );
        return;
    }

    #   verify expired
    my $expired = $request->{'-expired'};
    if ($expired) {
        $self->error( { '-text' => 'Request expired -request ' . $self->dbg_value($request) } );

        #   expire request
        $self->expire_request( { '-request_id' => $request_id } );
        return;
    }

    $self->debug( { '-text' => 'Request -args ' . $self->dbg_value($args) . ' authenticated' } );

    return 1;
} ## end sub authenticate_request

#   approve request
#   - authenticates and processes a request
sub approve_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_id', '-request_key', '-user_id', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user id
    my $user_id = $args->{'-user_id'};

    #   user
    my $user = $user_util->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $self->dbg_value($user_id) } );
        return;
    }

    #   request id
    my $request_id = $args->{'-request_id'};

    #   request key
    my $request_key = $args->{'-request_key'};

    #   authenticate request
    #   - we are repeating some checks here but needed to break out
    #     request authentication so external programs could authenticate
    #     a request without approving it yet
    if (!$self->authenticate_request(
            {   '-user_id'           => $user_id,
                '-request_id'        => $request_id,
                '-request_key'       => $request_key,
                '-request_status_id' => REQUEST_STATUS_NEW
            } )
        ) {
        $self->error( { '-text' => 'Failed to authenticate -request_id ' . $self->dbg_value($request_id) } );
        return;
    }

    #   request
    my $request = $self->fetch_one_request( { '-request_id' => $request_id } );
    if ( !defined $request ) {
        $self->error( { '-text' => 'Failed to fetch -request_id ' . $self->dbg_value($request_id) } );
        return;
    }

    #   process request
    if ( !$self->process_request( { '-request_id' => $request_id, } ) ) {
        $self->error( { '-text' => 'Failed to process -request_id ' . $self->dbg_value($request_id) } );
        return;
    }

    #   update request
    my $statement = <<'STMT';
UPDATE `as_request` 
SET    `request_status_id` = ?
WHERE  `request_id` = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, REQUEST_STATUS_APPROVED );
        $sth->bind_param( ++$bind, $request_id );

        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to approve -request_id ' . $request_id . ' :' . $EVAL_ERROR } );
        return;
    }

    #   insert status history record
    if (!$self->add_request_status_history_record(
            {   '-request_id'            => $request_id,
                '-user_id'               => $user_id,
                '-new_request_status_id' => REQUEST_STATUS_APPROVED,
                '-old_request_status_id' => REQUEST_STATUS_NEW,
            } )
        ) {
        $self->error( { '-text' => 'Failed to add request status history record' } );
        return;
    }

    #   send request messages
    if (!$self->add_request_messages(
            {   '-request_id' => $request_id,
                '-user'       => $user,
            } )
        ) {
        $self->error( { '-text' => 'Failed to send request messages' } );
        return;
    }

    return 1;
} ## end sub approve_request

sub add_request_status_history_record {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_id', '-user_id', '-new_request_status_id' ], $args ) ) {
        return;
    }

    my $request_id            = $args->{'-request_id'};
    my $user_id               = $args->{'-user_id'};
    my $new_request_status_id = $args->{'-new_request_status_id'};
    my $old_request_status_id = $args->{'-old_request_status_id'};

    my $statement = <<'STMT';
INSERT INTO `as_request_status_history` (
    `request_id`,
    `user_id`,
    `old_request_status_id`,
    `new_request_status_id`
) VALUES (
    ?,
    ?,
    ?,
    ?
)
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $request_id );
        $sth->bind_param( ++$bind, $user_id );
        $sth->bind_param( ++$bind, $old_request_status_id );
        $sth->bind_param( ++$bind, $new_request_status_id );

        $sth->execute();
        $sth->finish();
        1;
    };

    if ( !$success ) {
        $self->error( { '-text' => 'Failed to to add request status history record :' . $EVAL_ERROR } );
        return;
    }

    return 1;
} ## end sub add_request_status_history_record

sub add_request_messages {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_id', '-user' ], $args ) ) {
        return;
    }

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    #   template util
    my $template_util = $self->utils->{ UTIL_TEMPLATE() };

    #   template vars
    my $template_vars = {};

    #   request id
    my $request_id = $args->{'-request_id'};

    #   approve url
    my $approve_url = $args->{'-approve_url'};
    if ( !defined $approve_url ) {
        $approve_url = PROGRAM_SITE_REQUEST_APPROVE;
    }

    #   deny url
    my $deny_url = $args->{'-deny_url'};
    if ( !defined $deny_url ) {
        $deny_url = PROGRAM_SITE_REQUEST_DENY;
    }

    #   request
    my $request = $self->fetch_one_request( { '-request_id' => $request_id } );
    if ( !defined $request ) {
        $self->error( { '-text' => 'Failed to fetch -request_id :' . $request_id } );
        return;
    }

    #   user
    my $user = $args->{'-user'};
    if ( !defined $user ) {
        $self->error( { '-text' => 'Missing -user, cannot send request messages' } );
        return;
    }

    #   request type id
    my $request_type_id = $request->{'-request_type_id'};

    #   request type
    my $request_type = $self->fetch_request_type( { '-request_type_id' => $request_type_id, } );
    if ( !defined $request_type ) {
        $self->error( { '-text' => 'Failed to fetch -request_type_id ' . $request_type_id } );
        return;
    }

    #   request status id
    my $request_status_id = $request->{'-request_status_id'};

    #   add user to template vars
    while ( my ( $k, $v ) = each %{$user} ) {
        my $var_key = lc($k);
        $var_key =~ s/^\-//sxm;
        $template_vars->{$var_key} = $user->{$k};
    }

    #   add request id to template vars
    $template_vars->{'request_id'} = $request_id;

    #   add request type to template vars
    $template_vars->{'request_type'} = $request_type->{'-request_type'};

    my $statement = <<'STMT';
SELECT 
    `request_type_message_id`,
    `request_message_id`,
    `request_message_recipient_id`
FROM `as_r_request_type_message`
WHERE `request_type_id` = ?
AND   `request_status_id` = ?        
STMT

    my @bind = ( $request_type_id, $request_status_id );
    my $request_type_messages = $db_table_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    if ( !defined $request_type_messages || !scalar @{$request_type_messages} ) {
        $self->debug(
            {   '-text' => 'No request type messages for -request_type_id ' . $self->dbg_value($request_type_id) .
                    ' -request_status_id ' . $self->dbg_value($request_status_id) . ', not sending messages'
            } );
        return 1;
    }

    #   request reply email address
    my $request_reply_email = $self->environment->{'APP_NOREPLY_EMAIL'};

    #   system contacts
    my $system_contacts = $self->fetch_request_system_contacts( { '-request_type_id' => $request_type_id, } );

    #   business contacts
    my $business_contacts = $self->fetch_request_business_contacts( { '-request_type_id' => $request_type_id, } );

    #   approve url
    my $request_approve_url = $self->make_program_link(
        {   '-program'     => $approve_url,
            '-request_id'  => $request->{'-request_id'},
            '-user_id'     => $user->{'-user_id'},
            '-request_key' => $request->{'-request_key'},
        } );
    $template_vars->{'request_approve_url'} = $request_approve_url;

    #   deny url
    my $request_deny_url = $self->make_program_link(
        {   '-program'     => $deny_url,
            '-request_id'  => $request->{'-request_id'},
            '-user_id'     => $user->{'-user_id'},
            '-request_key' => $request->{'-request_key'},
        } );
    $template_vars->{'request_deny_url'} = $request_deny_url;

    $self->debug( { '-text' => 'Have -template_vars ' . $self->dbg_value($template_vars) } );

    #   send system contact emails
    foreach my $request_type_message ( @{$request_type_messages} ) {

        #   request message subject / body
        my $request_message_id = $request_type_message->{'-request_message_id'};
        my ( $subject_text, $template_name ) = $self->fetch_request_message(
            {   '-request_message_id' => $request_message_id,
                '-request'            => $request,
            } );

        #   subject text
        $subject_text .= ' (ID ' . $request_id . ') ';

        #   template
        #   - use Util::Template to form email body with vars
        my $body_text = $template_util->process(
            {   '-file' => 'request/' . $template_name,
                '-vars' => $template_vars,
            } );

        my $request_message_recipient_id = $request_type_message->{'-request_message_recipient_id'};

        #   requester email
        if ( $request_message_recipient_id == REQUEST_MESSAGE_RECIPIENT_REQUESTER ) {

            my $email = $email_util->create_email(
                {   '-sender'    => $request_reply_email,
                    '-reply_to'  => $request_reply_email,
                    '-subject'   => $subject_text,
                    '-body'      => $body_text,
                    '-recipient' => {
                        '-type'    => 'to',
                        '-address' => $user->{'-email_address'},
                    } } );

            if ( !defined $email ) {
                $self->error( { '-text' => 'Failed creating email' } );
                return;
            }

        }

        #   system email
        elsif ( $request_message_recipient_id == REQUEST_MESSAGE_RECIPIENT_SYSTEM ) {
            foreach my $system_contact ( @{$system_contacts} ) {
                $self->debug( { '-text' => 'Sending message to -system_contact ' . $self->dbg_value($system_contact) } );

                my $email = $email_util->create_email(
                    {   '-sender'    => $request_reply_email,
                        '-reply_to'  => $request_reply_email,
                        '-subject'   => $subject_text,
                        '-body'      => $body_text,
                        '-recipient' => {
                            '-type'    => 'to',
                            '-address' => $system_contact->{'-email_address'},
                        } } );

                if ( !defined $email ) {
                    $self->error( { '-text' => 'Failed creating system contact email' } );
                    return;
                }
            }
        }

        #   business email
        elsif ( $request_message_recipient_id == REQUEST_MESSAGE_RECIPIENT_BUSINESS ) {
            foreach my $business_contact ( @{$business_contacts} ) {
                $self->debug( { '-text' => 'Sending message to -business_contact ' . $self->dbg_value($business_contact) } );

                my $email = $email_util->create_email(
                    {   '-sender'    => $request_reply_email,
                        '-reply_to'  => $request_reply_email,
                        '-subject'   => $subject_text,
                        '-body'      => $body_text,
                        '-recipient' => {
                            '-type'    => 'to',
                            '-address' => $business_contact->{'-email_address'},
                        } } );

                if ( !defined $email ) {
                    $self->error( { '-text' => 'Failed creating business contact email' } );
                    return;
                }
            }
        }

        #   some other email
        else {

            #   some other request message type id i dont support yet :D
        }
    } ## end foreach my $request_type_message...

    return 1;
} ## end sub add_request_messages

sub fetch_request_system_contacts {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_type_id'], $args ) ) {
        return;
    }

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   request type id
    my $request_type_id = $args->{'-request_type_id'};

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    my $statement = <<'STMT';
SELECT `request_system_contact_id`,
       `request_type_id`,
       `system_contact_type_id`
FROM   `as_r_request_system_contact`
WHERE  `request_type_id` = ?
STMT

    my @bind = ($request_type_id);
    my $request_system_contacts = $db_table_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    my $contacts;

FIND_SYSTEM_CONTACTS:
    foreach my $request_system_contact ( @{$request_system_contacts} ) {

        #   system contact recs
        my $system_contacts = $contact_util->fetch_system_contacts(
            { '-system_contact_type_id' => $request_system_contact->{'-system_contact_type_id'}, } );
        if ( defined $system_contacts && scalar @{$system_contacts} ) {
            $self->debug( { '-text' => 'Adding -system_contacts ' . $self->dbg_value($system_contacts) } );
            push @{$contacts}, @{$system_contacts};
        }
    }

    return $contacts;
} ## end sub fetch_request_system_contacts

sub fetch_request_business_contacts {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_type_id'], $args ) ) {
        return;
    }

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   request type id
    my $request_type_id = $args->{'-request_type_id'};

    #   contact util
    my $contact_util = $self->utils->{ UTIL_CONTACT() };

    my $statement = <<'STMT';
SELECT `request_business_contact_id`,
       `request_type_id`,
       `business_contact_type_id`
FROM   `as_r_request_business_contact`
WHERE  `request_type_id` = ?
STMT

    my @bind = ($request_type_id);
    my $request_business_contacts = $db_table_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    my $contacts;

FIND_SYSTEM_CONTACTS:
    foreach my $request_business_contact ( @{$request_business_contacts} ) {

        #   business contact recs
        my $business_contacts = $contact_util->fetch_business_contacts(
            { '-business_contact_type_id' => $request_business_contact->{'-business_contact_type_id'}, } );
        if ( defined $business_contacts && scalar @{$business_contacts} ) {
            $self->debug( { '-text' => 'Adding -business_contacts ' . $self->dbg_value($business_contacts) } );
            push @{$contacts}, @{$business_contacts};
        }
    }

    return $contacts;
} ## end sub fetch_request_business_contacts

sub fetch_request_message {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-request_message_id', '-request' ], $args ) ) {
        return;
    }

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   request message id
    my $request_message_id = $args->{'-request_message_id'};

    #   request
    my $request = $args->{'-request'};

    my $statement = <<'STMT';
SELECT `subject_phrase_id`,
       `template_name`
FROM   `as_r_request_message`
WHERE  `request_message_id` = ?
STMT

    my ( $subject_phrase_id, $template_name );
    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->bind_param( 1, $request_message_id );
        $sth->execute();
        ( $subject_phrase_id, $template_name ) = $sth->fetchrow_array();
        $sth->finish();
        1;
    };

    if ( !$success ) {
        $self->error( { '-text' => 'Failed to fetch -request_message_id ' . $request_message_id . ' :' . $EVAL_ERROR } );
        return;
    }

    if ( !defined $subject_phrase_id && !defined $template_name ) {
        $self->error(
            {   '-text' => 'Failed to fetch -subject_phrase_id ' . $self->dbg_value($subject_phrase_id) .
                    ' and -template_name ' . $self->dbg_value($template_name) . ' for -request_message_id ' . $request_message_id
            } );
        return;
    }

    #   subject text
    my $subject_text = $phrase_util->text( { '-phrase_id' => $subject_phrase_id } );

    return ( $subject_text, $template_name );
} ## end sub fetch_request_message

#
#   expecting [{'-table_name' => xx, '-column_name' => xx, '-pk_value' => xx}]
#
sub fetch_request_current_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_data'], $args ) ) {
        return;
    }

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   request data
    my $request_data = $args->{'-request_data'};
    if ( !defined $request_data ) {
        $self->error( { '-text' => 'Missing request data, cannot identify current data values' } );
        return;
    }

    #   table primary key columns
    my $table_pk_columns = {};

FIND_PK_COLUMN:
    foreach my $request_data_record ( @{$request_data} ) {
        my $table_name = $request_data_record->{'-table_name'};
        if ( !exists $table_pk_columns->{$table_name} ) {
            my $table_constraints = $db_table_util->fetch_constraints( { '-table_name' => $table_name, } );
            if ( !defined $table_constraints || !keys %{$table_constraints} ) {
                next FIND_PK_COLUMN;
            }
            foreach my $table_column ( keys %{$table_constraints} ) {
                if ( $table_constraints->{$table_column}->{'-constraint_type'} eq 'PRIMARY KEY' ) {
                    $table_pk_columns->{$table_name} = $table_column;
                    next FIND_PK_COLUMN;
                }
            }
        }
    }

    $self->debug( { '-text' => 'Have table primary keys ' . $self->dbg_value($table_pk_columns) } );

    #   fetch each request data record
    my $current_data = {};

FETCH_CURRENT:
    foreach my $request_data_record ( @{$request_data} ) {

        my $table_name  = $request_data_record->{'-table_name'};
        my $column_name = $request_data_record->{'-column_name'};
        my $pk_value    = $request_data_record->{'-pk_value'};

        if ( !defined $table_name || !defined $column_name || !defined $pk_value ) {
            $self->debug( { '-text' => 'Not fetching current' } );
            next FETCH_CURRENT;
        }

        if ( !exists $current_data->{$table_name} || !exists $current_data->{$table_name}->{$pk_value} ) {
            my $statement = 'SELECT * FROM `' . $table_name . '` WHERE `' . $table_pk_columns->{$table_name} . '` = ?';

            my @bind = ($pk_value);
            my $data = $db_table_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

            if ( defined $data && scalar @{$data} == 1 ) {
                $current_data->{$table_name}->{$pk_value} = $data->[0];
            }
        }
    }

    $self->debug( { '-text' => 'Have current data ' . $self->dbg_value($current_data) } );

    return $current_data;
} ## end sub fetch_request_current_data

sub fetch_request_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_id'], $args ) ) {
        return;
    }

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   request id
    my $request_id = $args->{'-request_id'};

    my $statement = <<'STMT';
SELECT *
FROM   `as_request_data`
WHERE  `request_id` = ?
STMT

    my @bind = ($request_id);

    my $data = $db_table_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    return $data;
}

sub process_request {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_id'], $args ) ) {
        return;
    }

    #   table util
    my $table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   request id
    my $request_id = $args->{'-request_id'};

    #   request
    my $request = $self->fetch_one_request( { '-request_id' => $request_id } );
    if ( !defined $request ) {
        $self->error( { '-text' => 'Failed to fetch request -request_id ' . $self->dbg_value($request_id) } );
        return;
    }

    #   request status id
    my $request_status_id = $request->{'-request_status_id'};
    if ( $request_status_id != REQUEST_STATUS_NEW ) {
        $self->error( { '-text' => 'Request ' . $self->dbg_value($request) . ' not approved, not processing' } );
        return;
    }

    #   request data
    my $request_data = $self->fetch_request_data( { '-request_id' => $request_id } );
    if ( !defined $request_data ) {
        $self->debug( { '-text' => 'No request data to process for -request ' . $self->dbg_value($request) } );
        return 1;
    }

    #   current data values
    my $current_data = $self->fetch_request_current_data( { '-request_data' => $request_data, } );

    #   build update record
    my $update_data;
    my $update_error = 0;

CHECK_UPDATE_DATA:
    foreach my $request_data_record ( @{$request_data} ) {
        my $update_table_name    = $request_data_record->{'-table_name'};
        my $update_column_name   = $request_data_record->{'-column_name'};
        my $update_pk_value      = $request_data_record->{'-pk_value'};
        my $update_current_value = $request_data_record->{'-current_value'};
        my $update_new_value     = $request_data_record->{'-new_value'};

        my $current_value = $current_data->{$update_table_name}->{$update_pk_value}->{ '-' . $update_column_name };
        if ( $update_current_value ne $current_value ) {
            $self->error(
                {   '-text' => 'Problem -update_current_value ' .
                        $update_current_value . ' does not match -current_value ' . $current_value . ', wont update record'
                } );
            $update_error = 1;
            next CHECK_UPDATE_DATA;
        }

        #   organise data to build an SQL statement
        $update_data->{$update_table_name}->{$update_pk_value}->{$update_column_name}->{'-current_value'} = $update_current_value;
        $update_data->{$update_table_name}->{$update_pk_value}->{$update_column_name}->{'-new_value'}     = $update_new_value;

    }

    if ($update_error) {
        $self->error( { '-text' => 'Update errors found, wont process data' } );
        return;
    }

    #   process table rows
PROCESS_TABLE_ROWS:
    foreach my $update_table_name ( keys %{$update_data} ) {

        #   primary key column
        my $table_constraints = $table_util->fetch_constraints( { '-table_name' => $update_table_name, } );
        if ( !defined $table_constraints || !keys %{$table_constraints} ) {
            $self->error( { '-text' => 'Failed to identify pk column for -update_table_name ' . $update_table_name } );
            $update_error = 1;
            last PROCESS_TABLE_ROWS;
        }

        my $primary_key_column;
        foreach my $table_column ( keys %{$table_constraints} ) {
            if ( $table_constraints->{$table_column}->{'-constraint_type'} eq 'PRIMARY KEY' ) {
                $primary_key_column = $table_column;
            }
        }

        if ( !defined $primary_key_column ) {
            $self->error( { '-text' => 'Failed to identify pk column for -update_table_name ' . $update_table_name } );
            $update_error = 1;
            last PROCESS_TABLE_ROWS;
        }

        #   update table data
        my $update_table_data = $update_data->{$update_table_name};

        foreach my $update_pk_value ( keys %{$update_table_data} ) {

            my @bind      = ();
            my $statement = 'UPDATE `' . $update_table_name . '` SET';

            #   update pk data
            my $update_pk_data = $update_table_data->{$update_pk_value};

            foreach my $update_column_name ( keys %{$update_pk_data} ) {

                #   new value
                my $new_value = $update_pk_data->{$update_column_name}->{'-new_value'};

                #   bind value
                push @bind, $new_value;

                $statement .= ' `' . $update_column_name . '` = ?,';
            }
            chop $statement;

            $statement .= ' WHERE `' . $primary_key_column . '` = ?';

            push @bind, $update_pk_value;

            $self->debug( { '-text' => 'Update -statement ' . $self->dbg_value($statement) } );

            my $success = eval {
                my $sth = $self->dbh->prepare($statement);

                my $idx = 0;
                foreach my $bind_value (@bind) {
                    $sth->bind_param( ++$idx, $bind_value );
                }

                $sth->execute();
                $sth->finish();
                1;
            };
            if ( !$success ) {
                $self->error( { '-text' => 'Failed to update request data :' . $EVAL_ERROR } );
                $update_error = 1;
                last PROCESS_TABLE_ROWS;
            }
        } ## end foreach my $update_pk_value...
    } ## end PROCESS_TABLE_ROWS: foreach my $update_table_name...

    if ($update_error) {
        $self->error( { '-text' => 'Update errors found, wont process data' } );
        return;
    }

    return 1;
} ## end sub process_request

sub get_request_status {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-request_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $request_id = $args->{'-request_id'};

    my $request = $self->fetch_one_request( { '-request_id' => $request_id } );
    if ( !defined $request ) {
        $self->error( { '-text' => 'No request found for -request_id ' . $request_id . ', not returning request status' } );
        return;
    }

    return $request->{'-request_status_id'};
}

1;
