package AlienSpaces::Util::Dictionary;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use Text::Aspell;

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

sub new {
    my ( $class, $args ) = @_;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->fatal( { '-text' => 'Failed to initialise super class' } );
    }

    #   set up speller
    my $speller = Text::Aspell->new();

    $self->{'-speller'} = $speller;

    return 1;
}

#   search
#   - given a word returns 1 or undef if the word can be found
sub search {
    my ( $self, $args ) = @_;

    #   word
    my $word = $args->{'-word'};

    #   case insensitive
    my $case_insensitive = $args->{'-case_insensitive'};
    if ( !defined $case_insensitive ) {
        $case_insensitive = 1;
    }

    my $speller = $self->{'-speller'};
    if ( !defined $speller ) {
        $self->error( { '-text' => 'Missing speller, cannnot search dictioary' } );
        return;
    }

    my $found = $speller->check($word);

    $self->debug( { '-text' => 'Testing -word ' . $word . ' -result ' . $found } );

    return $found;
}

#   suggest
sub suggest {
    my ( $self, $args ) = @_;

    #   word
    my $word = $args->{'-word'};

    my $speller = $self->{'-speller'};
    if ( !defined $speller ) {
        $self->error( { '-text' => 'Missing speller, cannnot suggest words from dictioary' } );
        return;
    }

    return $speller->suggest($word);
}

#   speller
#   - return the aspell dictionary object
sub speller {
    my ( $self, $args ) = @_;

    return $self->{'-speller'};
}

1;
