package AlienSpaces::Util::Session;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :defaults);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use constant {
    MAX_SESSION_CREATE_ATTEMPTS => 5,

    #   status
    SESSION_STATUS_UNCHANGED => 1,
    SESSION_STATUS_CHANGED   => 2,
    SESSION_STATUS_NEW       => 3,
    SESSION_STATUS_DELETE    => 4,
};

use Storable qw(freeze thaw dclone);
use Digest::MD5;
use CGI::Cookie;

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_DB_BASIC(),
        UTIL_DB_RECORD_SESSION(),
        UTIL_DB_RECORD_SESSION_DATA(),
    ];

    return $util_list;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super init failed' } );
        return;
    }

    return 1;
}

sub init_user_session {
    my ( $self, $args ) = @_;

    #   clear session
    $self->clear_user_session();

    #   cookie name
    my $cookie_name = $self->get_cookie_name();

    #   cookie
    my $cookie;

    #   request (Plack)
    if ( $self->request ) {
        my $cookies = $self->request->cookies;
        $cookie = $cookies->{$cookie_name};
        $self->debug(
            {   '-text' => 'Fetched -cookie ' .
                    $self->dbg_value($cookie) . ' from Plack::Request cookies ' . $self->dbg_value($cookies) } );
    }
    else {
        my %cookies = CGI::Cookie->fetch();
        $cookie = $cookies{$cookie_name};
        $self->debug(
            {   '-text' => 'Fetched -cookie ' .
                    $self->dbg_value($cookie) . ' from CGI::Cookie cookies ' . $self->dbg_value( \%cookies ) } );
    }

    my $key;
    if ( defined $cookie ) {
        $key = $cookie->value;
    }

    $self->debug( { '-text' => 'Fetched -key ' . $self->dbg_value($key) } );

    #   create session
    if ( !defined $key ) {
        $key = $self->create_user_session();
    }

    if ( !defined $key ) {
        $self->error( { '-text' => 'Could not get or create a -session_id ' } );
        return;
    }

    #   load session
    if ( !$self->load_session( { '-key' => $key } ) ) {
        $self->debug( { '-text' => 'Missing session record re-creating session' } );

        #   create session
        $key = $self->create_user_session();

        $self->debug( { '-text' => 'New session -key ' . $self->dbg_value($key) } );

        #   load session
        if ( !$self->load_session( { '-key' => $key } ) ) {
            $self->error( { '-text' => 'Still failed to load user session, no session....' } );
            return;
        }
    }

    #   dump session
    $self->dump_user_session();

    $cookie = CGI::Cookie->new(
        '-name'    => $cookie_name,
        '-value'   => $key,
        '-expires' => '+1d'
    );

    $self->{'-cookie'} = $cookie;

    return 1;
} ## end sub init_user_session

#   init new user session
#   - doeesn't bother trying to find an existing
#     session but just clears the existing session
#     and creates a new one.
sub init_new_user_session {
    my ( $self, $args ) = @_;

    #   delete session
    $self->delete_user_session();

    #   clear user session
    $self->clear_user_session();

    #   cookie name
    my $cookie_name = $self->get_cookie_name();

    #   create session
    my $key = $self->create_user_session();

    $self->debug( { '-text' => 'New session -key ' . $self->dbg_value($key) } );

    #   load session
    if ( !$self->load_session( { '-key' => $key } ) ) {
        $self->error( { '-text' => 'Still failed to load user session, no session....' } );
        return;
    }

    #   dump session
    $self->dump_user_session();

    my $cookie = CGI::Cookie->new(
        '-name'    => $cookie_name,
        '-value'   => $key,
        '-expires' => '+1d'
    );

    $self->{'-cookie'} = $cookie;

    return 1;
} ## end sub init_new_user_session

sub get_cookie_name {
    my ( $self, $args ) = @_;

    #   cookie name
    my $cookie_name = $self->environment->{'APP_COOKIE_NAME'};

    return $cookie_name;
}

sub get_cookie_string {
    my ( $self, $args ) = @_;

    my $cookie = $self->get_cookie();
    if ( !defined $cookie ) {
        return;
    }

    return $cookie->as_string;
}

#   delete user session
sub delete_user_session {
    my ( $self, $args ) = @_;

    my $session_id   = $self->{'-session_id'};
    my $session      = $self->{'-session'};
    my $session_data = $self->{'-session_data'};

    foreach my $key ( keys %{$session_data} ) {

        my $session_data_rec = $session_data->{$key};
        my $session_data_id  = $session_data_rec->{'-session_data_id'};
        if ( !defined $session_data_id ) {
            next DELETE_SESSION;
        }

        if ( !$self->delete_session_data_record( { '-session_data_id' => $session_data_id } ) ) {
            $self->error( { '-text' => 'Failed to delete -session_data_record ' . $self->dbg_value($session_data_rec) } );
        }

        $self->debug( { '-text' => 'Deleted -session_data_id ' . $self->dbg_value($session_data_id) } );
    }

    if ( !$self->delete_session_record( { '-session_id' => $session_id } ) ) {
        $self->error( { '-text' => 'Failed to delete session record -session_id ' . $session_id } );
        return;
    }

    $self->debug( { '-text' => 'Session -session_id ' . $session_id . ' deleted' } );

    return 1;
} ## end sub delete_user_session

#   create user session
sub create_user_session {
    my ( $self, $args ) = @_;

    my $attempts = 0;
    my $key;

CREATE_USER_SESSION_RECORD:
    while ( $attempts++ < MAX_SESSION_CREATE_ATTEMPTS ) {
        $key = $self->insert_session_record();
        if ( defined $key ) {
            last CREATE_USER_SESSION_RECORD;
        }
    }

    return $key;
}

#   insert session record
sub insert_session_record {
    my ( $self, $args ) = @_;

    #   session db rec util
    my $session_db_rec_util = $self->utils->{ UTIL_DB_RECORD_SESSION() };

    my $key = $self->random_key( { '-length' => 32 } );

    #   session id
    my $session_id = $session_db_rec_util->insert( { '-key' => $key, '-children' => 0 } );
    if ( !defined $session_id ) {
        $self->error( { '-text' => 'Failed to create new session record' } );
        return;
    }

    $self->{'-session_id'} = $session_id;

    $self->debug( { '-text' => 'Created -session_id ' . $self->dbg_value($session_id) . ' -key ' . $self->dbg_value($key) } );

    return $key;
}

#   load session
sub load_session {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-key'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session db rec util
    my $session_db_rec_util = $self->utils->{ UTIL_DB_RECORD_SESSION() };

    #   session data db rec util
    my $session_data_db_rec_util = $self->utils->{ UTIL_DB_RECORD_SESSION_DATA() };

    #   key
    my $key = $args->{'-key'};

    #   session
    my $session = $session_db_rec_util->fetch_one( { '-key' => $key } );
    if ( !defined $session ) {
        $self->error( { '-text' => 'Failed to fetch session record for -key ' . $self->dbg_value($key) } );
        return;
    }

    #   session id
    my $session_id = $session->{'-session_id'};

    #   session data arr
    my $session_data_arr = $session_data_db_rec_util->fetch( { '-session_id' => $session_id } );

    #   session data
    my $session_data = {};
    if ( defined $session_data_arr && scalar @{$session_data_arr} ) {
        foreach my $session_data_hash ( @{$session_data_arr} ) {

            $self->debug( { '-text' => 'Have -session_data_hash ' . $self->dbg_value($session_data_hash) } );

            my $data_key        = $session_data_hash->{'-key'};
            my $data            = $session_data_hash->{'-data'};
            my $session_data_id = $session_data_hash->{'-session_data_id'};

            my $thawed_data = thaw($data);

            $session_data->{$data_key} = {
                '-session_data_id' => $session_data_id,
                '-data'            => $thawed_data,
                '-status'          => SESSION_STATUS_UNCHANGED,
            };
        }
    }

    $self->{'-session_id'}   = $session_id;
    $self->{'-session'}      = $session;
    $self->{'-session_data'} = $session_data;

    #   session id
    $self->set_session( { '-key' => '-session_id', '-value' => $session_id } );

    #   check defaults
    my $logged_in = $self->get_session( { '-key' => '-logged_in' } );
    if ( !defined $logged_in ) {

        #   set not logged in
        $self->set_session( { '-key' => '-logged_in', '-value' => 0 } );
    }

    my $language = $self->get_session( { '-key' => '-language' } );
    if ( !defined $language ) {

        #   set defaults
        $self->set_session( { '-key' => '-language', '-value' => DEFAULT_LANGUAGE } );
    }

    $self->debug( { '-text' => 'Loaded session' } );

    return 1;
} ## end sub load_session

#   set session
sub set_session {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-key', '-value' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   key
    my $key = $args->{'-key'};

    #   value
    my $value = $args->{'-value'};

    my $session_data = $self->{'-session_data'};

    my $session_data_rec = $session_data->{$key};

    if ( defined $session_data_rec && defined $session_data_rec->{'-session_data_id'} ) {
        $self->debug(
            {   '-text' => 'Setting -key ' .
                    $key . ' -session_data_id ' . $self->dbg_value( $session_data_rec->{'-session_data_id'} ) . ' as changed'
            } );

        $session_data_rec->{'-data'} = { '-value' => $value };
        $session_data_rec->{'-status'} = SESSION_STATUS_CHANGED;
    }
    else {
        $self->debug( { '-text' => 'Setting -key ' . $key . ' as new' } );

        $session_data->{$key} = {
            '-data'   => { '-value' => $value },
            '-status' => SESSION_STATUS_NEW,
        };
    }

    $self->{'-session_data'} = $session_data;

    return 1;
} ## end sub set_session

#   delete session
sub delete_session {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-key'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $session_data = $self->{'-session_data'};
    if ( !exists $session_data->{ $args->{'-key'} } ) {
        return;
    }

    if ( defined $session_data->{ $args->{'-key'} }->{'-session_data_id'} ) {
        $session_data->{ $args->{'-key'} }->{'-status'} = SESSION_STATUS_DELETE;
    }
    else {
        delete $session_data->{ $args->{'-key'} };
    }

    $self->{'-session_data'} = $session_data;

    return 1;
}

#   get session
sub get_session {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-key'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $session_data = $self->{'-session_data'};
    if ( !exists $session_data->{ $args->{'-key'} } ) {
        return;
    }

    #   deleted
    if ( $session_data->{ $args->{'-key'} }->{'-status'} == SESSION_STATUS_DELETE ) {
        return;
    }

    my $value = $session_data->{ $args->{'-key'} }{'-data'}{'-value'};
    if ( ref($value) ) {
        $value = dclone($value);
    }

    return $value;
}

#   update session
sub update_session {
    my $self = shift;

    #   session db rec util
    my $session_db_rec_util = $self->utils->{ UTIL_DB_RECORD_SESSION() };

    #   session data db rec util
    my $session_data_db_rec_util = $self->utils->{ UTIL_DB_RECORD_SESSION_DATA() };

    my $session_id   = $self->{'-session_id'};
    my $session      = $self->{'-session'};
    my $session_data = $self->{'-session_data'};

    #   session db rec util
    $session_db_rec_util->update( { '-session_id' => $session_id } );

SESSION_DATA:
    foreach my $key ( keys %{$session_data} ) {

        my $session_data_rec = $session_data->{$key};

        $self->debug(
            { '-text' => 'Have -key ' . $self->dbg_value($key) . ' -session_data ' . $self->dbg_value($session_data_rec) } );

        my $session_data_id = $session_data_rec->{'-session_data_id'};

        my $frozen_data = freeze( $session_data_rec->{'-data'} );

        my $status = $session_data_rec->{'-status'};

        #   changed
        if ( $status == SESSION_STATUS_CHANGED ) {
            if (!$session_data_db_rec_util->update(
                    {   '-session_data_id' => $session_data_id,
                        '-session_id'      => $session_id,
                        '-key'             => $key,
                        '-data'            => $frozen_data,
                    } )
                ) {
                $self->error(
                    {   '-text' => 'Failed to update -key ' .
                            $self->dbg_value($key) . ' -session_data_id ' . $self->dbg_value($session_data_id) } );
                next SESSION_DATA;
            }
            $self->debug(
                { '-text' => 'Updated -key ' . $self->dbg_value($key) . ' -session_data_id ' . $self->dbg_value($session_data_id) }
            );
        }

        #   new
        elsif ( $status == SESSION_STATUS_NEW ) {
            $session_data_id = $session_data_db_rec_util->insert(
                {   '-session_id' => $session_id,
                    '-key'        => $key,
                    '-data'       => $frozen_data,
                    '-children'   => 0,
                } );
            if ( !defined $session_data_id ) {
                $self->error( { '-text' => 'Failed to insert -key ' . $self->dbg_value($key) } );
                next SESSION_DATA;
            }
            $self->debug(
                {   '-text' => 'Inserted -key ' . $self->dbg_value($key) . ' -session_data_id ' . $self->dbg_value($session_data_id)
                } );
        }

        #   delete
        elsif ( $status == SESSION_STATUS_DELETE ) {
            if ( !$session_data_db_rec_util->delete( { '-session_data_id' => $session_data_id, } ) ) {
                $self->error(
                    {   '-text' => 'Failed to delete -key ' .
                            $self->dbg_value($key) . ' -session_data_id ' . $self->dbg_value($session_data_id) } );
                next SESSION_DATA;
            }
            $self->debug(
                { '-text' => 'Deleted -key ' . $self->dbg_value($key) . ' -session_data_id ' . $self->dbg_value($session_data_id) }
            );
        }
    } ## end SESSION_DATA: foreach my $key ( keys %{$session_data...})

    return 1;
} ## end sub update_session

#   clear user session
sub clear_user_session {
    my $self = shift;

    delete $self->{'-session_id'};
    delete $self->{'-session'};

    my $session_data = $self->{'-session_data'};
    foreach my $key ( keys %{$session_data} ) {
        if ( $key eq '-session_id' ) { next; }
        $self->debug( { '-text' => "Session (clear) -key [$key]" } );
        delete $self->{'-session_data'}->{$key};
    }

    delete $self->{'-session_data'};

    $self->debug( { '-text' => 'Cleared user session' } );

    return 1;
}

#   dump user session
sub dump_user_session {
    my $self = shift;

    my $session      = $self->{'-session'};
    my $session_data = $self->{'-session_data'};

    $self->debug( { '-text' => 'Session (dump) -session ' . $self->dbg_value($session) } );
    $self->debug( { '-text' => 'Session (dump) -session_data ' . $self->dbg_value($session_data) } );

    return 1;
}

#   delete expired session records
sub delete_expired_session_records {
    my ( $self, $args ) = @_;

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };

    my $stmt = <<'STMT';
SELECT 
    `s`.`session_id`, 
    `sd`.`session_data_id`
FROM 
    `as_session` s
LEFT OUTER JOIN `as_session_data` sd ON `sd`.`session_id` = `s`.`session_id`
WHERE 
    DATEDIFF(CURRENT_DATE, `s`.`update_ts`) > 2
ORDER BY `s`.`session_id` DESC
STMT

    my $data = $db_basic_util->fetch_data( { '-statement' => $stmt } );
    if ( !defined $data ) {
        $self->debug( { '-text' => 'No sessions to delete' } );
        return 1;
    }

    my %session_ids = ();
DELETE_SESSION:
    foreach my $session_data_record ( @{$data} ) {
        my $session_data_id = $session_data_record->{'-session_data_id'};
        if ( !defined $session_data_id ) {
            next DELETE_SESSION;
        }

        if ( !$self->delete_session_data_record( { '-session_data_id' => $session_data_id } ) ) {
            $self->error( { '-text' => 'Failed to delete -session_data_record ' . $self->dbg_value($session_data_record) } );
        }

        $self->debug( { '-text' => 'Deleted -session_data_id ' . $self->dbg_value($session_data_id) } );

        $session_ids{ $session_data_record->{'-session_id'} } = 1;
    }

    foreach my $session_id ( keys %session_ids ) {
        if ( !$self->delete_session_record( { '-session_id' => $session_id } ) ) {
            $self->error( { '-text' => 'Failed to delete session record -session_id ' . $self->dbg_value($session_id) } );
        }

        $self->debug( { '-text' => 'Deleted session record -session_id ' . $self->dbg_value($session_id) } );
    }

    return 1;
} ## end sub delete_expired_session_records

#   delete all session records
sub delete_all_session_records {
    my ( $self, $args ) = @_;

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };

    my $stmt = <<'STMT';
SELECT 
    `s`.`session_id`, 
    `sd`.`session_data_id`
FROM 
    `as_session` s
LEFT OUTER JOIN `as_session_data` sd ON `sd`.`session_id` = `s`.`session_id`
STMT

    my $data = $db_basic_util->fetch_data( { '-statement' => $stmt } );
    if ( !defined $data ) {
        $self->debug( { '-text' => 'No sessions to delete' } );
        return 1;
    }

    my %session_ids = ();
DELETE_SESSION:
    foreach my $session_data_record ( @{$data} ) {
        my $session_data_id = $session_data_record->{'-session_data_id'};
        if ( !defined $session_data_id ) {
            next DELETE_SESSION;
        }

        if ( !$self->delete_session_data_record( { '-session_data_id' => $session_data_id } ) ) {
            $self->error( { '-text' => 'Failed to delete -session_data_record ' . $self->dbg_value($session_data_record) } );
        }

        $self->debug( { '-text' => 'Deleted -session_data_id ' . $self->dbg_value($session_data_id) } );

        $session_ids{ $session_data_record->{'-session_id'} } = 1;
    }

    foreach my $session_id ( keys %session_ids ) {
        if ( !$self->delete_session_record( { '-session_id' => $session_id } ) ) {
            $self->error( { '-text' => 'Failed to delete session record -session_id ' . $self->dbg_value($session_id) } );
        }

        $self->debug( { '-text' => 'Deleted session record -session_id ' . $self->dbg_value($session_id) } );
    }

    return 1;
} ## end sub delete_all_session_records

#   delete session record
sub delete_session_record {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-session_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session db rec util
    my $session_db_rec_util = $self->utils->{ UTIL_DB_RECORD_SESSION() };

    #   session id
    my $session_id = $args->{'-session_id'};

    if ( !$session_db_rec_util->delete( { '-session_id' => $session_id } ) ) {
        $self->error( { '-text' => 'Failed to delete session record' } );
        return;
    }

    return 1;
}

#   delete session data record
sub delete_session_data_record {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-session_data_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session data db rec util
    my $session_data_db_rec_util = $self->utils->{ UTIL_DB_RECORD_SESSION_DATA() };

    #   session data id
    my $session_data_id = $args->{'-session_data_id'};

    if ( !$session_data_db_rec_util->delete( { '-session_data_id' => $session_data_id } ) ) {
        $self->error( { '-text' => 'Failed to delete session data records' } );
        return;
    }

    return 1;
}

sub get_session_id {
    my $self = shift;

    return $self->{'-session_id'};
}

sub get_cookie {
    my $self = shift;

    return $self->{'-cookie'};
}

sub finish {
    my ( $self, $args ) = @_;

    $self->debug( { '-text' => 'Updating user session' } );

    #   update session
    $self->update_session();

    #   remove session data
    delete $self->{'-session_data'};

    #   remove cookie
    delete $self->{'-cookie'};

    $self->finished(1);

    return 1;
}

1;
