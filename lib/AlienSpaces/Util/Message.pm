package AlienSpaces::Util::Message;

#
#   $Revision: 126 $
#   $Author: bwwallin $
#   $Date: 2010-05-29 16:41:02 +1000 (Sat, 29 May 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :language :message);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {

};

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },
        UTIL_PHRASE(),     # phrase
        UTIL_USER(),       # user
        UTIL_SESSION(),    # session
        UTIL_DB_BASIC(),   
    ];

    return $util_list;
}

#
#   add message
#   -text    - text for the message
#   -type    - type of message
#
sub add_message {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-text'], $args ) ) {
        return;
    }

    #   name
    my $name = $args->{'-name'};
    if ( !defined $name ) {
        $name = MESSAGE_DEFAULT_NAME;
    }

    #   type
    my $type = $args->{'-type'};
    if ( !defined $type ) {
        $type = MESSAGE_DEFAULT_TYPE;
    }

    #   text
    my $text = $args->{'-text'};
    if ( ref($text) ne 'ARRAY' ) {
        $text = [$text];
    }

    $self->debug( { '-text' => 'Adding messages with -args ' . $self->dbg_value($args) } );

    #   store in db or one hit temporary to be displayed immediately
    if ( $args->{'-store'} ) {

        #   add user message
        $self->_add_user_message($args);
    }
    else {

        $self->_add_session_message(
            {   '-name' => $name,
                '-type' => $type,
                '-text' => $text,
            } );

    }

    return 1;
} ## end sub add_message

sub get_messages {
    my ( $self, $args ) = @_;

    #   name
    my $name = $args->{'-name'};
    if ( !defined $name ) {
        $name = 'global';
    }

    #   type
    my $type = $args->{'-type'};

    #   return messages
    my $return_messages = [];

    #   stored messages
    my $user_messages = $self->_get_user_messages($args);
    if ( defined $user_messages && scalar @{$user_messages} ) {
        push @{$return_messages}, @{$user_messages};
    }

    #   session messages
    my $session_messages = $self->_get_session_messages($args);
    if ( defined $session_messages && scalar @{$session_messages} ) {
        push @{$return_messages}, @{$session_messages};
    }

    $self->debug( { '-text' => 'Have -return_messages ' . $self->dbg_value($return_messages) } );

    return $return_messages;
} ## end sub get_messages

#   get session messages
sub _get_session_messages {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $name = $args->{'-name'};
    my $type = $args->{'-type'};
    my $text = $args->{'-text'};

    my $messages = [];

    #   get session messages
    my $session_messages = $session_util->get_session( { '-key' => '-session_messages' } );
    if ( !defined $session_messages ) {
        $self->debug( { '-text' => 'No session messages for -name ' . $name } );
        return;
    }

    $self->debug( { '-text' => 'Have -session_messages ' . $self->dbg_value($session_messages) } );

    #   specific type for name
    if ( defined $type && defined $session_messages->{$name}->{$type} ) {

        while ( my $message = shift @{ $session_messages->{$name}->{$type} } ) {
            push @{$messages}, { '-type' => $type, '-text' => $message };
        }
        delete $session_messages->{$name}->{$type};
    }

    #   all types for name
    else {

        my $type_messages = $session_messages->{$name};

        foreach my $message_type ( sort keys %{$type_messages} ) {
            while ( my $message = shift @{ $type_messages->{$message_type} } ) {
                push @{$messages}, { '-type' => $message_type, '-text' => $message };
            }
        }
        delete $session_messages->{$name};
    }

    #   set session messages
    $session_util->set_session( { '-key' => '-session_messages', '-value' => $session_messages } );

    $self->debug( { '-text' => 'Returning -messages ' . $self->dbg_value($messages) } );

    return $messages;
} ## end sub _get_session_messages

#   get user messages
sub _get_user_messages {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   db basic util
    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };

    #   delete
    my $delete = $args->{'-delete'};
    if ( !defined $delete ) {
        $delete = 1;
    }

    #   user id
    my $user_id = $args->{'-user_id'};
    if ( !defined $user_id ) {
        $user_id = $user_util->user_id;
    }

    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'Missing -user_id, cannot fetch user messages' } );
        return;
    }

    $self->debug( { '-text' => 'Fetching messages for -user_id ' . $user_id . ' -delete ' . $delete } );

    my $statement = <<'STMT';
SELECT 
    `user_message_id`, 
    `message_type_id` type, 
    `text`,
    `create_ts`
FROM   
    `as_user_message`
WHERE  
    `user_id` = ?
STMT

    my @binds = ($user_id);

    my $messages = $db_basic_util->fetch_data( { '-statement' => $statement, '-bind' => \@binds } );
    if ( defined $messages && scalar @{$messages} && $delete ) {
        foreach my $message ( @{$messages} ) {
            $self->_delete_user_message($message);
        }
    }

    $self->debug( { '-text' => 'Returning -messages ' . $self->dbg_value($messages) } );

    return $messages;
} ## end sub _get_user_messages

#   add session message
sub _add_session_message {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $name = $args->{'-name'};
    my $type = $args->{'-type'};
    my $text = $args->{'-text'};

    $self->debug( { '-text' => 'Adding -name ' . $name . ' -type ' . $type . ' -text ' . $self->dbg_value($text) } );

    #   get session messages
    my $session_messages = $session_util->get_session( { '-key' => '-session_messages' } );
    if ( !defined $session_messages ) {
        $session_messages = {};
    }

    if ( !defined $session_messages->{$name} ) {
        $session_messages->{$name} = {};
    }
    if ( !defined $session_messages->{$name}{$type} ) {
        $session_messages->{$name}{$type} = [];
    }

    push @{ $session_messages->{$name}->{$type} }, @{$text};

    #   set session messages
    $session_util->set_session( { '-key' => '-session_messages', '-value' => $session_messages } );

    return 1;
} ## end sub _add_session_message

#   add user message
sub _add_user_message {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user id
    my $user_id = $args->{'-user_id'};
    if ( !defined $user_id ) {
        $user_id = $user_util->user_id;
    }

    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Missing -user_id, cannot add user messages' } );
        return;
    }

    #   message type
    my $message_type = $args->{'-type'};
    if ( !defined $message_type ) {
        $message_type = MESSAGE_TYPE_INFO;
    }

    #   text
    my $text = $args->{'-text'};
    if ( !defined $text ) {
        $self->error( { '-text' => 'Missing text, cannot add user message' } );
        return;
    }

    my $statement = <<'STMT';
INSERT INTO `as_user_message` (
    `user_id`,
    `message_type_id`,
    `text`
)
VALUES (
    ?,
    ?,
    ?
)
STMT

    my $success = eval {
        my $sth  = $self->dbh->prepare($statement);
        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->bind_param( ++$bind, $message_type );
        $sth->bind_param( ++$bind, $text );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to add user message :' . $EVAL_ERROR } );
        return;
    }

    $self->debug( { '-text' => 'Stored user message ' . $self->dbg_value($args) } );

    return 1;
} ## end sub _add_user_message

#   delete user message
sub _delete_user_message {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_message_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user message id
    my $user_message_id = $args->{'-user_message_id'};

    my $stmt = <<'STMT';
DELETE FROM `as_user_message`
WHERE  `user_message_id` = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($stmt);
        $sth->bind_param( 1, $user_message_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete -user_message_id ' . $self->dbg_value($user_message_id) . ' :' . $EVAL_ERROR } );
        return;
    }

    $self->debug( { '-text' => 'Deleted -user_message_id ' . $self->dbg_value($user_message_id) } );

    return 1;
} ## end sub _delete_user_message

1;
