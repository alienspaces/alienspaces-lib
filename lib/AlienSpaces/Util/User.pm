package AlienSpaces::Util::User;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(
    :util
    :comp
    :program
    :cache
    :defaults
    :user
    :contact-type
    :mode
    :error-codes-user
);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use constant {
    ACTIVATE_KEY_LENGTH => 32,
    MIN_PASSWORD_RATING => 4,
};

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        
        #   super
        @{ $self->SUPER::util_list },
        
        #   local
        UTIL_EMAIL(),      # email
        UTIL_SESSION(),    # session
        UTIL_MESSAGE(),    # message
        UTIL_PHRASE(),     # phrase
        UTIL_ERROR(),      # error

        #   util db record
        UTIL_DB_RECORD_USER(),
        UTIL_DB_RECORD_USER_OAUTH(),

        UTIL_DB_RECORD_USER_VIEW(),
        UTIL_DB_RECORD_USER_TASK_VIEW(),

        UTIL_DB_RECORD_BUSINESS_USER(),

        UTIL_DB_RECORD_R_USER_CONFIG(),
        UTIL_DB_RECORD_R_USER_TYPE(),
        UTIL_DB_RECORD_R_USER_TYPE_TASK(),

        UTIL_DB_RECORD_R_BUSINESS_USER_TYPE(),
        UTIL_DB_RECORD_R_BUSINESS_USER_TYPE_TASK(),
    ];

    return $util_list;
} ## end sub util_list

#   lc user args
sub lc_user_args {
    my ( $self, $args ) = @_;

    my $to_lower_param_map = {
        '-handle'        => 'handle',
        '-email_address' => 'email_address',
    };

TO_LOWER_PARAM:
    foreach my $key ( keys %{$args} ) {
        if ( !defined $args->{key} ) {
            next TO_LOWER_PARAM;
        }
        $self->trace( { '-text' => 'Have param ' . $key . ' value ' . $args->{$key} } );
        $args->{$key} = lc( $args->{$key} );
    }

    return 1;
}

#   fetch users
sub fetch_users {
    my ( $self, $args ) = @_;

    if ( exists $args->{'-user_id'} && !defined $args->{'-user_id'} ) {
        $self->debug( { '-text' => 'Have -user_id arg but its undefined' } );
        return;
    }

    #   user view db rec util
    my $user_view_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER_VIEW() };

    #   lc user args
    $self->lc_user_args($args);

    #   fetch
    my $data = $user_view_db_rec_util->fetch($args);

    return $data;
}

#   fetch one user
sub fetch_one_user {
    my ( $self, $args ) = @_;

    #   user view db rec util
    my $user_view_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER_VIEW() };

    #   lc user args
    $self->lc_user_args($args);

    #   user
    my $user;

    #   user id
    my $user_id = $args->{'-user_id'};

    #   get cached
    if ( defined $user_id ) {
        $user = $self->get_cached(
            {   '-key'      => $self->get_cache_key( { '-cache_key_args' => { '-user_id' => $user_id } } ),
                '-category' => CACHE_CATEGORY_USER
            } );
        if ( defined $user ) {
            $self->debug( { '-text' => 'Returning cached -user_id ' . $user_id } );
            return $user;
        }
    }

    $self->debug( { '-text' => 'Fetching -user_id ' . $self->dbg_value($user_id) } );

    #   fetch
    $user = $user_view_db_rec_util->fetch_one($args);

    #   set cached
    if ( defined $user_id ) {
        $self->set_cached(
            {   '-key'      => $self->get_cache_key( { '-cache_key_args' => { '-user_id' => $user_id } } ),
                '-value'    => $user,
                '-category' => CACHE_CATEGORY_USER,
            } );
    }

    return $user;
} ## end sub fetch_one_user

#   fetch current user
sub fetch_current_user {
    my ( $self, $args ) = @_;

    my $user_id = $self->user_id;
    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'Missing -user_id, cannot fetch current user' } );
        return;
    }

    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );

    return $user;
}

#   fetch oauth user
sub fetch_oauth_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-oauth_user', '-oauth_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   oauth user
    my $oauth_user = $args->{'-oauth_user'};

    #   oauth id
    my $oauth_id = $args->{'-oauth_id'};

    #   identifier
    my $identifier = $oauth_user->{'-identifier'};
    if ( !defined $identifier ) {
        $self->error( { '-text' => 'Missing identifier with -args ' . $self->dbg_value($args) } );
        return;
    }

    my ( $user_id, $user_status_id );

    my $statement = <<'STMT';
SELECT `u`.`user_id`, `u`.`user_status_id`
FROM   `as_user` u
JOIN   `as_user_oauth` uo 
    ON  `uo`.`user_id`    = `u`.`user_id`
    AND `uo`.`identifier` = ?
    AND `uo`.`oauth_id`   = ?
WHERE  `u`.`user_status_id` IN ( ?, ?, ?, ?)
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $identifier );
        $sth->bind_param( ++$bind, $oauth_id );
        $sth->bind_param( ++$bind, USER_STATUS_ACTIVE );
        $sth->bind_param( ++$bind, USER_STATUS_CANCELLED );
        $sth->bind_param( ++$bind, USER_STATUS_BANNED );
        $sth->bind_param( ++$bind, USER_STATUS_PENDING_ACTIVATION );
        $sth->execute();

        ( $user_id, $user_status_id ) = $sth->fetchrow_array();
        $sth->finish();
        1;
    };

    if ( !$success ) {
        $self->error( { '-text' => 'Error fetching user with -args ' . $self->dbg_value($args) . ' :' . $EVAL_ERROR } );
        return;
    }

    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'No user found with -args ' . $self->dbg_value($args) } );
        return;
    }

    #   user
    my $user;
    if ( defined $user_id ) {
        $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    }

    return $user;
} ## end sub fetch_oauth_user

#   fetch business user
sub fetch_business_user {
    my ( $self, $args ) = @_;

    #   fetch
    my $data = $self->fetch_users($args);

    return $data;
}

#   fetch one business user
sub fetch_one_business_user {
    my ( $self, $args ) = @_;

    #   fetch
    my $data = $self->fetch_one_user($args);

    return $data;
}

#   set current business user
sub set_current_business_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   business user id
    my $business_user_id = $args->{'-business_user_id'};

    $session_util->set_session( { '-key' => '-current_business_user_id', '-value' => $business_user_id } );

    return 1;
}

#   fetch current business user
sub fetch_current_business_user {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   business user id
    my $business_user_id = $session_util->get_session( { '-key' => '-current_business_user_id' } );

    my $business_user = $self->fetch_one_business_user( { '-business_user_id' => $business_user_id } );
    if ( !defined $business_user ) {
        $self->error( { '-text' => 'Failed to fetch business user for -business_user_id ' . $self->dbg_value($business_user_id) } );
        return;
    }

    return $business_user;
}

#   fetch application user
sub fetch_application_user {
    my ( $self, $args ) = @_;

    #   fetch
    my $data = $self->fetch_users($args);

    return $data;
}

#   fetch one application user
sub fetch_one_application_user {
    my ( $self, $args ) = @_;

    #   application user db rec util
    my $application_user_db_rec_util = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER() };

    #   fetch
    my $data = $self->fetch_one_user($args);

    return $data;
}

#   set current application user
sub set_current_application_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user id
    my $user_id = $args->{'-user_id'};

    $session_util->set_session( { '-key' => '-current_application_user_id', '-value' => $user_id } );

    return 1;
}

#   fetch current application user
sub fetch_current_application_user {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user id
    my $user_id = $session_util->get_session( { '-key' => '-current_application_user_id' } );

    my $application_user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $application_user ) {
        $self->error( { '-text' => 'Failed to fetch application user for -application_user_id ' . $self->dbg_value($user_id) } );
        return;
    }

    return $application_user;
}

#   fetch user config
sub fetch_user_config {
    my ( $self, $args ) = @_;

    #   user config db rec util
    my $user_config_db_rec_util = $self->utils->{ UTIL_DB_RECORD_R_USER_CONFIG() };

    #   user id
    my $user_id = delete $args->{'-user_id'};
    if ( defined $user_id ) {
        my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
        if ( !defined $user ) {
            $self->error( { '-text' => 'Failed to fetch user -user_id ' . $user_id . ', cannot retrieve user configuration' } );
            return;
        }

        #   user config id
        $args->{'-user_config_id'} = $user->{'-user_config_id'};
    }

    #   default
    if ( !defined $args->{'-user_config_id'} ) {
        $self->debug( { '-text' => 'No -user_config_id defined, returning default' } );
        $args->{'-default'} = 1;
    }

    #   data
    my $data = $user_config_db_rec_util->fetch_one($args);

    $self->debug( { '-text' => 'Have configuration -data ' . $self->dbg_value($data) } );

    return $data;
} ## end sub fetch_user_config

#   fetch user type
sub fetch_user_type {
    my ( $self, $args ) = @_;

    #   user type db rec util
    my $user_type_db_rec_util = $self->utils->{ UTIL_DB_RECORD_R_USER_TYPE() };

    #   user id
    my $user_id = delete $args->{'-user_id'};
    if ( defined $user_id ) {
        my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
        if ( !defined $user ) {
            $self->error( { '-text' => 'Failed to fetch user -user_id ' . $user_id . ', cannot retrieve user type' } );
            return;
        }

        #   user type id
        $args->{'-user_type_id'} = $user->{'-user_type_id'};

        #   default
        if ( !defined $args->{'-user_type_id'} ) {
            $self->debug( { '-text' => 'User -user_id ' . $user_id . ' does not have a user type' } );
            return;
        }
    }

    #   data
    my $data = $user_type_db_rec_util->fetch_one($args);

    $self->debug( { '-text' => 'Returning user type -data ' . $self->dbg_value($data) } );

    return $data;
} ## end sub fetch_user_type

#   fetch business user type
sub fetch_business_user_type {
    my ( $self, $args ) = @_;

    #   business user type db rec util
    my $business_user_type_db_rec_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_USER_TYPE() };

    #   user id
    my $user_id = delete $args->{'-user_id'};
    if ( defined $user_id ) {
        my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
        if ( !defined $user ) {
            $self->error( { '-text' => 'Failed to fetch user -user_id ' . $user_id . ', cannot retrieve user type' } );
            return;
        }

        #   business user type id
        $args->{'-business_user_type_id'} = $user->{'-business_user_type_id'};

        #   default
        if ( !defined $args->{'-business_user_type_id'} && !defined $args->{'-default'} ) {
            $self->debug( { '-text' => 'User -user_id ' . $user_id . ' does not have a business user type' } );
            return;
        }
    }

    #   data
    my $data = $business_user_type_db_rec_util->fetch_one($args);

    $self->debug( { '-text' => 'Returning business user type -data ' . $self->dbg_value($data) } );

    return $data;
} ## end sub fetch_business_user_type

#   fetch user tasks
sub fetch_user_tasks {
    my ( $self, $args ) = @_;

    #   user task view db rec util
    my $user_task_view_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER_TASK_VIEW() };

    #   user id
    my $user_id = $args->{'-user_id'};
    if ( !defined $user_id ) {
        $user_id = $self->user_id;
    }

    #   user
    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch user -user_id ' . $self->dbg_value($user_id) . ', cannot fetch user tasks' } );
        return;
    }

    #   user tasks
    my $user_tasks = $user_task_view_db_rec_util->fetch( { '-user_id' => $user_id } );

    $self->debug( { '-text' => 'Have -user_tasks ' . $self->dbg_value($user_tasks) } );

    return $user_tasks;
}

#   insert user
sub insert_user {
    my ( $self, $args ) = @_;

    #   user db rec util
    my $user_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER() };

    #   lc user args
    $self->lc_user_args($args);

    #   handle 'handle' LOL
    my $handle = $args->{'-handle'};
    if ( !defined $handle ) {

        #   if we have no handle use the first part of the email address
        $handle = $args->{'-email_address'};
        if ( defined $handle ) {
            $handle =~ s/^(.*)\@.*$/$1/sxmg;
            $args->{'-handle'} = $handle;
        }
    }

    #   validate user
    if ( !$self->validate_user_insert($args) ) {
        $self->error( { '-text' => 'Failed to validate user, not adding user' } );
        return;
    }

    #   language id
    if ( !defined $args->{'-language_id'} ) {
        $args->{'-language_id'} = DEFAULT_LANGUAGE;
    }

    #   time zone id
    if ( !defined $args->{'-time_zone_id'} ) {
        $args->{'-time_zone_id'} = DEFAULT_TIME_ZONE;
    }

    #   user status id
    if ( !defined $args->{'-user_status_id'} ) {
        $args->{'-user_status_id'} = USER_STATUS_PENDING_ACTIVATION;
    }

    #   password
    if ( exists $args->{'-password'} ) {
        $args->{'-password'} = $self->encrypt_string(
            { '-string' => $self->encrypt_string( { '-string' => $args->{'-email_address'} } ) . $args->{'-password'} } );
    }

    #   user configuration id
    if ( !defined $args->{'-user_config_id'} ) {

        #   fetch default user configuration
        my $user_config = $self->fetch_user_config( { '-default' => 1 } );
        if ( !defined $user_config ) {
            $self->error( { '-text' => 'Failed to fetch user config' } );
            return;
        }
        $args->{'-user_config_id'} = $user_config->{'-user_config_id'};
    }

    if ( !defined $args->{'-user_config_id'} ) {
        $self->error( { '-text' => 'Unable to indentify -user_config_id, cannot add user' } );
        return;
    }

    #   user type id
    if ( !defined $args->{'-user_type_id'} ) {

        #   fetch default user type
        my $user_type = $self->fetch_user_type( { '-default' => 1 } );
        if ( !defined $user_type ) {
            $self->error( { '-text' => 'Failed to fetch user type' } );
            return;
        }
        $args->{'-user_type_id'} = $user_type->{'-user_type_id'};
    }

    if ( !defined $args->{'-user_type_id'} ) {
        $self->error( { '-text' => 'Unable to indentify -user_type_id, cannot add user' } );
        return;
    }

    #   create activate key
    $args->{'-activate_key'} = $self->random_key( { '-length' => ACTIVATE_KEY_LENGTH } );

    #   user id
    my $user_id = $user_db_rec_util->insert($args);

    $self->debug( { '-text' => 'Added -user_id ' . $user_id } );

    return $user_id;
} ## end sub insert_user

#   insert business user
sub insert_business_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-user_id', '-business_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user id
    my $user_id = $args->{'-user_id'};

    #   business id
    my $business_id = $args->{'-business_id'};

    #   business user type id
    my $business_user_type_id = $args->{'-business_user_type_id'};

    #   business user status id
    my $business_user_status_id = $args->{'-business_user_status_id'};

    #   business user db rec util
    my $business_user_db_rec_util = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER() };

    #   business user type id
    if ( !defined $business_user_type_id ) {

        #   fetch default business user type
        my $business_user_type = $self->fetch_business_user_type( { '-default' => 1 } );
        if ( !defined $business_user_type ) {
            $self->error( { '-text' => 'Failed to fetch business user type' } );
            return;
        }
        $business_user_type_id = $business_user_type->{'-business_user_type_id'};
    }

    if ( !defined $business_user_type_id ) {
        $self->error( { '-text' => 'Unable to indentify -business_user_type_id, cannot add user' } );
        return;
    }

    #   business user type id
    if ( !defined $business_user_status_id ) {
        $business_user_status_id = BUSINESS_USER_STATUS_ACTIVE;
    }

    #   business user id
    my $business_user_id = $business_user_db_rec_util->insert(
        {   '-user_id'                 => $user_id,
            '-business_id'             => $business_id,
            '-business_user_type_id'   => $business_user_type_id,
            '-business_user_status_id' => $business_user_status_id,
        } );

    $self->debug( { '-text' => 'Added -business_user_id ' . $business_user_id } );

    return $business_user_id;
} ## end sub insert_business_user

#   update user
sub update_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user db rec util
    my $user_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER() };

    #   lc user args
    $self->lc_user_args($args);

    #   validate user
    if ( !$self->validate_user_update($args) ) {
        return;
    }

    if ( defined $args->{'-email_address'} && defined $args->{'-password'} ) {

        #   encrypt password with new email address/password combination
        $args->{'-password'} = $self->encrypt_string(
            { '-string' => $self->encrypt_string( { '-string' => $args->{'-email_address'} } ) . $args->{'-password'} } );
    }

    #   update
    if ( !$user_db_rec_util->update($args) ) {
        $self->error( { '-text' => 'Failed to update user record' } );
        return;
    }

    #   user id
    my $user_id = $args->{'-user_id'};
    if ( defined $user_id ) {
        $self->clear_cached(
            {   '-key'      => $self->get_cache_key( { '-cache_key_args' => { '-user_id' => $user_id } } ),
                '-category' => CACHE_CATEGORY_USER,
            } );

    }

    $self->debug( { '-text' => 'Updated -user_id ' . $args->{'-user_id'} } );

    return 1;
} ## end sub update_user

#   update business user
sub update_business_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business user db rec util
    my $business_user_db_rec_util = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER() };

    #   update
    if ( !$business_user_db_rec_util->update($args) ) {
        $self->error( { '-text' => 'Failed to update business user record' } );
        return;
    }

    $self->debug( { '-text' => 'Updated -business_user_id ' . $args->{'-business_user_id'} } );

    return 1;
}

#   update user password
sub update_user_password {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-user_id', '-password' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user db rec util
    my $user_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER() };

    #   user id
    my $user_id = $args->{'-user_id'};

    #   password
    my $password = $args->{'-password'};

    #   user
    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch user for -user_id ' . $user_id . ', cannot update password' } );
        return;
    }

    #   email address
    my $email_address = $user->{'-email_address'};
    if ( !defined $email_address ) {
        $self->error(
            {   '-text' => 'Record for -user ' . $self->dbg_value($user) . ' does not have an email address, cannot update password'
            } );
        return;
    }

    #   encrypt password with new email address/password combination
    $password = $self->encrypt_string( { '-string' => $self->encrypt_string( { '-string' => $email_address } ) . $password } );

    #   update
    if ( !$user_db_rec_util->update( { '-user_id' => $user_id, '-password' => $password } ) ) {
        $self->error( { '-text' => 'Failed to update user record' } );
        return;
    }

    return 1;
} ## end sub update_user_password

my $uc_letters = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
    'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];
my $lc_letters = [
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
    'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
];
my $numbers = [ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ];
my $characters = [
    '`', '~', '@',  '#', '%', '^', '&',  '*', '(', ')', '-', '_', '=', '+', '[', ']',
    '{', '}', '\\', '|', ';', ':', '\'', '"', ',', '.', '<', '>', '/', '?',
];

#   get user password rating
sub get_user_password_rating {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-password'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   rating
    my $rating;

    #   password
    my $password = $args->{'-password'};

    my $uc_count         = $self->_get_match_count( $password, $uc_letters, 0 );
    my $lc_count         = $self->_get_match_count( $password, $lc_letters, 0 );
    my $numbers_count    = $self->_get_match_count( $password, $numbers,    0 );
    my $characters_count = $self->_get_match_count( $password, $characters, 1 );

    foreach my $count ( $uc_count, $lc_count, $numbers_count, $characters_count ) {
        foreach my $count_check ( 3 .. 6 ) {
            if ( $count > $count_check ) {
                $rating++;
            }
        }
    }

    $self->debug(
        {   '-text' => 'Returning -rating ' . $self->dbg_value($rating) .
                ' based on -uc_count ' . $self->dbg_value($uc_count) . ' -lc_count ' . $self->dbg_value($lc_count) .
                ' -numbers_count ' . $self->dbg_value($numbers_count) . ' -characters_count ' . $self->dbg_value($characters_count)
        } );

    return $rating;
} ## end sub get_user_password_rating

sub _get_match_count {
    my ( $self, $string, $array, $escape ) = @_;

    my $count = 0;

    foreach my $c ( @{$array} ) {
        my $regexp_c = $c;
        if ($escape) {
            $regexp_c = '\\' . $c;
        }

        while ( $string =~ /[$regexp_c]/sxmg ) {
            $count++;
        }
    }

    return $count;
}

#   validate password
sub validate_password {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-password'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   password
    my $password = $args->{'-password'};

    #   rating
    my $rating = $self->get_user_password_rating( { '-password' => $password } );
    if ( $rating < MIN_PASSWORD_RATING ) {
        $self->debug( { '-text' => 'Password -rating ' . $self->dbg_value($rating) . ' < ' . MIN_PASSWORD_RATING . ', too weak' } );
        return;
    }

    $self->debug( { '-text' => 'Password -rating ' . $self->dbg_value($rating) . ' is good enough' } );

    return 1;
}

#   authen expiry minutes
sub get_authen_expiry_minutes {
    my ( $self, $args ) = @_;

    return USER_AUTHEN_EXPIRY_MINUTES();
}

sub verify_user_login {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   authen expiry minutes
    my $authen_expiry_minutes = $self->get_authen_expiry_minutes();

    $self->debug(
        { '-text' => 'Testing user authentication expiry -authen_expiry_minutes ' . $self->dbg_value($authen_expiry_minutes) } );

    my $statement = <<'STMT';
SELECT  `login_active`, 
        `touch_ts`
FROM    `as_user`
WHERE   `user_id` = ?
AND     CURRENT_TIMESTAMP() < TIMESTAMPADD(MINUTE, ?, `touch_ts`)
AND     `user_status_id` = ?
STMT
    my ( $login_active, $touch_ts );
    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $args->{'-user_id'} );
        $sth->bind_param( ++$bind, $authen_expiry_minutes );
        $sth->bind_param( ++$bind, USER_STATUS_ACTIVE() );

        $sth->execute();

        ( $login_active, $touch_ts ) = $sth->fetchrow_array();

        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to retrieve -login_active from user account :' . $EVAL_ERROR } );
        return;
    }

    $self->debug(
        {   '-text' => 'Fetched -login_active ' . ( defined $login_active ? $login_active : 'undefined' ) . ' -touch_ts ' .
                ( defined $touch_ts ? $touch_ts : 'undefined' ) . ' from user account for -user_id ' . $args->{'-user_id'} } );

    #   if active update touch ts
    if ( ( defined $login_active ) && ( $login_active == USER_LOGIN_ACTIVE ) ) {

        $statement = <<'STMT';
UPDATE `as_user`
SET    `touch_ts` = CURRENT_TIMESTAMP()
WHERE  `user_id` = ?
STMT
        $success = eval {
            my $sth = $self->dbh->prepare($statement);
            $sth->bind_param( 1, $args->{'-user_id'} );
            $sth->execute();
            $sth->finish();
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to update user account touch ts :' . $EVAL_ERROR } );
            return;
        }
        $self->debug( { '-text' => 'Updated user account touch ts for -user_id ' . $args->{'-user_id'} } );

    }
    else {

        #   log user out
        $self->logout_user( { '-user_id' => $args->{'-user_id'} } );

        return;
    }

    return 1;
} ## end sub verify_user_login

#   user id
sub user_id {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user id
    my $user_id = $session_util->get_session( { '-key' => '-user_id' } );

    return $user_id;
}

#   user language
sub user_language {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   language id
    my $language_id = $session_util->get_session( { '-key' => '-language_id' } );

    return $language_id;
}

#   user session id
sub user_session_id {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    return $session_util->get_session_id;
}

#   check user logged in
sub check_user_logged_in {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   logged in
    my $logged_in = $session_util->get_session( { '-key' => '-logged_in' } );
    if ( !$logged_in ) {
        $self->debug( { '-text' => 'User not logged in' } );
        return;
    }

    #   user status active
    my $user = $self->fetch_one_user( { '-user_id' => $self->user_id } );
    if ( $user->{'-user_status_id'} != USER_STATUS_ACTIVE ) {
        $self->debug( { '-text' => 'User is logged in but account is no longer active' } );

        #   logout user
        $self->logout_user();

        return;
    }

    return 1;
}

#   check user account admin rank
#   - checks to see whether current user can administrate another specific user
sub check_user_account_admin_rank {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-check_user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user
    my $user = $self->fetch_current_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current user' } );
        return;
    }

    my @user_types = ();

    #   business user type
    push @user_types, $self->fetch_business_user_type( { '-user_id' => $user->{'-user_id'} } );

    #   user type
    push @user_types, $self->fetch_user_type( { '-user_id' => $user->{'-user_id'} } );

    #   check user
    my @check_user_types = ();

    my $check_user_id = $args->{'-check_user_id'};
    $self->debug( { '-text' => 'Checking -check_user_id ' . $check_user_id } );

    #   business user type
    push @check_user_types, $self->fetch_business_user_type( { '-user_id' => $check_user_id } );

    #   user type
    push @check_user_types, $self->fetch_user_type( { '-user_id' => $check_user_id } );

    if ( !scalar @check_user_types ) {
        $self->error( { '-text' => 'Failed to fetch check user types for -user_id ' . $check_user_id } );
        return;
    }

    my ( $can_admin_count, $cannot_admin_count ) = ( 0, 0 );

CHECK_ADMIN:
    foreach my $user_type (@user_types) {
        if ( !$user_type->{'-user_account_admin'} ) {
            $self->debug( { '-text' => 'User with -user_type ' . $self->dbg_value($user_type) . ' cannot admin, skipping' } );
            next CHECK_ADMIN;
        }

    CHECK_ADMIN_RANK:
        foreach my $check_user_type (@check_user_types) {

            #   check user account admin rank
            #   - any failure means cannot admin other user
            if ( $user_type->{'-user_account_admin_rank'} >= $check_user_type->{'-user_account_admin_rank'} ) {
                $self->debug(
                    {   '-text' => 'User -user_id ' . $self->dbg_value( $user->{'-user_id'} ) .
                            ' -user_type ' . $self->dbg_value($user_type) . ' cannot admin -user_id ' .
                            $self->dbg_value( $args->{'-user_id'} ) . ' -check_user_type ' . $self->dbg_value($check_user_type) } );

                #   cannot admin count
                $cannot_admin_count++;

                next CHECK_ADMIN_RANK;
            }

            #   can admin count
            $can_admin_count++;
        }
    }

    $self->debug(
        {   '-text' => 'User -user_id ' .
                $self->dbg_value( $user->{'-user_id'} ) . ' -can_admin_count ' . $self->dbg_value($can_admin_count) .
                ' -cannot_admin_count ' . $self->dbg_value($cannot_admin_count) . ' -user_id ' .
                $self->dbg_value( $args->{'-user_id'} ) . ' -check_user_types ' . $self->dbg_value( \@check_user_types ) } );

    return ( ( $can_admin_count > 0 ) && ( $cannot_admin_count == 0 ) );
} ## end sub check_user_account_admin_rank

#   check user account admin
#   - checks to see whether current user can administrate user accounts at all
sub check_user_account_admin {
    my ( $self, $args ) = @_;

    #   user
    my $user = $self->fetch_current_user();
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch current user' } );
        return;
    }

    my @user_types = ();

    #   business user type
    push @user_types, $self->fetch_business_user_type( { '-user_id' => $user->{'-user_id'} } );

    #   user type
    push @user_types, $self->fetch_user_type( { '-user_id' => $user->{'-user_id'} } );

CHECK_ADMIN:
    foreach my $user_type (@user_types) {
        if ( !$user_type->{'-user_account_admin'} ) {
            $self->debug( { '-text' => 'User with -user_type ' . $self->dbg_value($user_type) . ' cannot admin, skipping' } );
            next CHECK_ADMIN;
        }

        $self->debug( { '-text' => 'User with -user_type ' . $self->dbg_value($user_type) . ' can admin' } );
        return 1;
    }

    $self->debug( { '-text' => 'User cannot admin' } );

    return;
} ## end sub check_user_account_admin

#   check user task
#   - checks to see whether a user has a specific
#   task, defaults to current user
sub check_user_task {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-task_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   task id
    my $task_id = $args->{'-task_id'};

    #   user id
    my $user_id = $args->{'-user_id'} || $self->user_id;
    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'No user id available, cannot check user task' } );
        return;
    }

    #   cache
    my $user_tasks = $self->get_cached(
        {   '-key'      => $self->get_cache_key( { '-cache_key_args' => { '-user_id' => $user_id } } ),
            '-category' => CACHE_CATEGORY_USER_TASK
        } );
    if ( defined $user_tasks ) {
        $self->debug( { '-text' => 'Fetched cached -user_id ' . $user_id } );
    }

    if ( !defined $user_tasks ) {
        my $_user_tasks = $self->fetch_user_tasks( { '-user_id' => $user_id } );
        foreach my $_user_task ( @{$_user_tasks} ) {
            my $_task_id = $_user_task->{'-task_id'};
            $user_tasks->{$_task_id} = 1;
        }

        #   set cached
        $self->set_cached(
            {   '-key'      => $self->get_cache_key( { '-cache_key_args' => { '-user_id' => $user_id } } ),
                '-value'    => $user_tasks,
                '-category' => CACHE_CATEGORY_USER_TASK,
            } );
    }

    if ( $user_tasks->{$task_id} ) {
        $self->debug( { '-text' => 'User -user_id ' . $user_id . ' has -task_id ' . $task_id } );
        return 1;
    }

    $self->debug( { '-text' => 'User -user_id ' . $user_id . ' does not have -task_id ' . $task_id } );

    return;
} ## end sub check_user_task

#   validate user insert
sub validate_user_insert {
    my ( $self, $args ) = @_;

    my $valid = 1;

TEST_UNIQUE:
    foreach my $k ( '-handle', '-email_address' ) {

        #   allow duplicate users for acccounts
        #   that are not activated yet
        my $check_user = $self->fetch_users(
            {   $k       => $args->{$k},
                '-where' => {
                    '-clause' => 'user_status_id != ?',
                    '-bind'   => [USER_STATUS_PENDING_ACTIVATION],
                },
            } );

        if ( defined $check_user ) {
            $self->error( { '-text' => 'Argument ' . $k . ' is not unique for a user, invalid' } );

            if ( $k eq '-handle' ) {
                $self->set_error( { '-error_code' => ERROR_CODE_USER_HANDLE_NOT_UNIQUE } );
            }
            elsif ( $k eq '-email_address' ) {
                $self->set_error( { '-error_code' => ERROR_CODE_USER_EMAIL_ADDRESS_NOT_UNIQUE } );
            }
            $valid = 0;
            last TEST_UNIQUE;
        }
    }

    if ( !defined $args->{'-email_address'} ) {
        $self->error( { '-text' => 'Missing email address, invalid' } );
        $valid = 0;
    }

    return $valid;
} ## end sub validate_user_insert

#   validate user update
sub validate_user_update {
    my ( $self, $args ) = @_;

    my $valid = 1;

    #   fetch existing user
    my $user = $self->fetch_one_user( { '-user_id' => $args->{'-user_id'} } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch existing user for -user_id ' . $args->{'-user_id'} . ', invalid' } );
        $valid = 0;
    }

    #   check email address / password combination
    if ( ( defined $args->{'-email_address'} || defined $args->{'-password'} ) &&
        ( !defined $args->{'-email_address'} || !defined $args->{'-password'} ) ) {
        $self->error( { '-text' => 'Cannot change email address without changing password, invalid' } );
        $valid = 0;
    }

    return $valid;
}

#   authenticate local user
#   - authenticate email address and password against a local
#     user account
#   - returns user_id and user_status_id
sub authenticate_local_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-email_address', '-password' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $email_address = $args->{'-email_address'};
    my $password      = $args->{'-password'};

    #   (security) Do not log password
    $self->debug( { '-text' => 'User (login) Attempting log in -email_address ' . $self->dbg_value($email_address) } );

    $password = $self->encrypt_user_password( { '-email_address' => $email_address, '-password' => $password } );

    if ( ( !defined $email_address ) || ( !defined $password ) ) {

        #   (security) Do not log password
        $self->error(
            {   '-text' => 'User -email_address ' . $self->dbg_value($email_address) .
                    ' or -password ' . ( defined $password ? '*****' : 'undef' ) . ' undefined, cannot log user in'
            } );
        return;
    }

    my ( $user_id, $user_status_id );

    my $statement = <<'STMT';
SELECT `user_id`, `user_status_id`
FROM   `as_user` 
WHERE  `email_address` = ?
AND    `password` = ?
AND    `user_status_id` IN ( ?, ?, ?, ?)
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $email_address );
        $sth->bind_param( ++$bind, $password );
        $sth->bind_param( ++$bind, USER_STATUS_ACTIVE );
        $sth->bind_param( ++$bind, USER_STATUS_CANCELLED );
        $sth->execute();

        ( $user_id, $user_status_id ) = $sth->fetchrow_array();
        $sth->finish();
        1;
    };

    if ( !$success ) {

        #   (security) Do not log password
        $self->error( { '-text' => 'Error logging in user -email_address ' . $email_address . ' :' . $EVAL_ERROR } );
        return;
    }

    if ( !defined $user_id ) {
        $self->debug( { '-text' => 'No user found' } );
        return;
    }

    $self->debug( { '-text' => 'Have -user_id ' . $self->dbg_value($user_id) } );

    return ( $user_id, $user_status_id );
} ## end sub authenticate_local_user

sub encrypt_user_password {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-email_address', '-password' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    my $email_address = $args->{'-email_address'};
    my $password      = $args->{'-password'};

    $password = $self->encrypt_string( { '-string' => $self->encrypt_string( { '-string' => $email_address } ) . $password } );

    return $password;
}

#   login user
sub login_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-email_address', '-password' ], $args ) ) {
        return;
    }

    my ( $user_id, $user_status_id ) = $self->authenticate_local_user($args);

    #   _login user
    if ( !$self->_login_user( { '-user_id' => $user_id, '-user_status_id' => $user_status_id } ) ) {
        $self->error( { '-text' => 'Failed to login -user_id ' . $self->dbg_value($user_id) } );
        return;
    }

    return $user_id;
}

#   login oauth user
sub login_oauth_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-oauth_user', '-oauth_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user
    my $user = $self->fetch_oauth_user($args);
    if ( !defined $user ) {
        $self->debug( { '-text' => 'Failed to find user with -args ' . $self->dbg_value($args) } );
        return;
    }

    #   user id
    my $user_id = $user->{'-user_id'};

    #   user status id
    my $user_status_id = $user->{'-user_status_id'};

    $self->debug( { '-text' => 'Have -user_id ' . $self->dbg_value($user_id) } );

    #   _login user
    if ( !$self->_login_user( { '-user_id' => $user_id, '-user_status_id' => $user_status_id } ) ) {
        $self->error( { '-text' => 'Failed to login -user_id ' . $self->dbg_value($user_id) } );
        return;
    }

    return $user_id;
} ## end sub login_oauth_user

#   _login_user
sub _login_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-user_id', '-user_status_id' ], $args ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user id
    my $user_id = $args->{'-user_id'};

    #   user status id
    my $user_status_id = $args->{'-user_status_id'};

    #   user account banned
    if ( $user_status_id == USER_STATUS_BANNED ) {

        $self->debug( { '-text' => 'User -user_id ' . $user_id . ' banned' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_BANNED } );
        return;
    }

    #   user account pending activation
    if ( $user_status_id == USER_STATUS_PENDING_ACTIVATION ) {
        $self->debug( { '-text' => 'User -user_id ' . $user_id . ' pending activation' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_PENDING_ACTIVATION } );
        return;
    }

    my $success;

    #   user account cancelled
    if ( $user_status_id == USER_STATUS_CANCELLED ) {

        #   set user status
        $success = $self->update_user(
            {   '-user_id'        => $user_id,
                '-user_status_id' => USER_STATUS_ACTIVE,
            } );

        if ( !$success ) {
            $self->error( { '-text' => 'Failed to reactivate account -user_id ' . $user_id . ' :' . $EVAL_ERROR } );
            return;
        }

        $self->debug( { '-text' => 'Reactivated user account -user_id ' . $user_id } );
    }

    #   login active
    my $statement = <<'STMT';
UPDATE `as_user`
SET    `touch_ts`     = CURRENT_TIMESTAMP(),
       `login_active` = ?
WHERE  `user_id` = ?            
STMT
    $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, USER_LOGIN_ACTIVE );
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to set login key and timestamp for -user_id ' . $user_id . ':' . $EVAL_ERROR } );
        return;
    }

    #   set logged in
    $session_util->set_session( { '-key' => '-logged_in', '-value' => 1 } );

    return 1;
} ## end sub _login_user

#   set user session values
sub set_user_session_values {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   user id
    my $user_id = $args->{'-user_id'};

    #   user
    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $user_id . ', cannot set user session values' } );
        return;
    }

    my @session_keys = qw(-user_id -handle -first_name -last_name -language_id -user_status_id -time_zone_id);
    foreach my $key (@session_keys) {
        $session_util->set_session( { '-key' => $key, '-value' => $user->{$key} } );
        $self->debug( { '-text' => 'Set session -key ' . $key . ' -value ' . $self->dbg_value( $user->{$key} ) } );
    }

    return 1;
}

#   logout user
sub logout_user {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    my $user_id = $args->{'-user_id'};
    if ( !defined $user_id ) {
        $user_id = $self->user_id();
    }
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Unable to identify -user_id. Cannot log user out.' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_NOT_LOGGED_IN } );
        return;
    }

    #   update login_active / touch_ts
    my $statement = <<'STMT';
UPDATE `as_user`
SET    `login_active` = ?,
       `touch_ts`     = NULL
WHERE  `user_id`      = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, USER_LOGIN_INACTIVE );
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to set login_action / clear touch_ts :' . $EVAL_ERROR } );
        return;
    }

    #   init new user session
    $session_util->init_new_user_session();

    $self->debug( { '-text' => 'Logged user out' } );

    return 1;
} ## end sub logout_user

#   register user
sub register_user {
    my ( $self, $args ) = @_;

    #   add user
    my $user_id = $self->insert_user($args);

    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Failed to add a new user ' . $self->dbg_value($args) } );
        return;
    }

    $self->debug( { '-text' => 'Registered user account -user_id [' . $user_id . ']' } );

    return $user_id;
}

#   register oauth user
sub register_oauth_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-oauth_id', '-oauth_user' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user db rec util
    my $user_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER() };

    #   user oauth db rec util
    my $user_oauth_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER_OAUTH() };

    #   oauth user
    my $oauth_user = delete $args->{'-oauth_user'};

    #   oauth id
    my $oauth_id = delete $args->{'-oauth_id'};

    #   identifier
    my $identifier = $oauth_user->{'-identifier'} || $oauth_user->{'-id'};
    if ( !defined $identifier ) {
        $self->error( { '-text' => 'Missing identifier with -args ' . $self->dbg_value($args) } );
        return;
    }

    #	handle
    my $handle = $oauth_user->{'-handle'};
    if ( defined $handle ) {
        $args->{'-handle'} = $handle;
    }

    #	email address
    my $email_address = $oauth_user->{'-email_address'};
    if ( defined $email_address ) {
        $args->{'-email_address'} = $email_address;
    }

    #	first name
    my $first_name = $oauth_user->{'-first_name'};
    if ( defined $first_name ) {
        $args->{'-first_name'} = $first_name;
    }

    #	last name
    my $last_name = $oauth_user->{'-last_name'};
    if ( defined $last_name ) {
        $args->{'-last_name'} = $last_name;
    }

    #   language id
    if ( !defined $args->{'-language_id'} ) {
        $args->{'-language_id'} = DEFAULT_LANGUAGE;
    }

    #   time zone id
    if ( !defined $args->{'-time_zone_id'} ) {
        $args->{'-time_zone_id'} = DEFAULT_TIME_ZONE;
    }

    #   user status id
    if ( !defined $args->{'-user_status_id'} ) {
        $args->{'-user_status_id'} = USER_STATUS_ACTIVE;
    }

    #   validate user
    if ( !$self->validate_user_insert($args) ) {
        $self->error( { '-text' => 'Failed to validate user, not adding user' } );
        return;
    }

    #   user configuration id
    if ( !defined $args->{'-user_config_id'} ) {

        #   fetch default user configuration
        my $user_config = $self->fetch_user_config( { '-default' => 1 } );
        if ( !defined $user_config ) {
            $self->error( { '-text' => 'Failed to fetch user config' } );
            return;
        }
        $args->{'-user_config_id'} = $user_config->{'-user_config_id'};
    }

    if ( !defined $args->{'-user_config_id'} ) {
        $self->error( { '-text' => 'Unable to indentify -user_config_id, cannot add user' } );
        return;
    }

    #   user id
    my $user_id = $user_db_rec_util->insert($args);

    $self->debug( { '-text' => 'Added -user_id ' . $user_id } );

    #   user
    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch -user_id ' . $self->dbg_value($user_id) } );
        return;
    }

    #   oauth user id
    my $oauth_user_id = $user_oauth_db_rec_util->insert(
        {   '-user_id'    => $user_id,
            '-identifier' => $identifier,
            '-oauth_id'   => $oauth_id,
        } );

    $user->{'-identifier'}    = $identifier;
    $user->{'-oauth_user_id'} = $oauth_user_id;

    return $user;
} ## end sub register_oauth_user

#   parse oath user data
#   - given an -oauth_user structure return equivalent -user data
sub parse_oauth_user_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-oauth_user'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   oauth user
    my $oauth_user = $args->{'-oauth_user'};

    #   user
    my $user = {};

    #   handle
    $user->{'-handle'} = $oauth_user->{'-name'} || $oauth_user->{'-username'};

    #   identifier
    $user->{'-identifier'} = $oauth_user->{'-id'} || $oauth_user->{'-identifier'};

    #   handle
    $user->{'-email_address'} = $oauth_user->{'-email'} || $oauth_user->{'-email_address'};

    #   first name
    $user->{'-first_name'} = $oauth_user->{'-first_name'} ||
        ( split /\_/sxm, $oauth_user->{'-name'} )[0] ||
        ( split /\_/sxm, $oauth_user->{'-full_name'} )[0];

    #   last name
    $user->{'-last_name'} = $oauth_user->{'-last_name'} ||
        ( split /\_/sxm, $oauth_user->{'-name'} )[1] ||
        ( split /\_/sxm, $oauth_user->{'-full_name'} )[1];

    return $user;
} ## end sub parse_oauth_user_data

#   register business user
sub register_business_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business id
    my $business_id = $args->{'-business_id'};

    #   insert user
    my $user_id = $self->insert_user($args);
    if ( !defined $user_id ) {
        $self->error( { '-text' => 'Failed to add a new user ' . $self->dbg_value($args) } );
        return;
    }

    #   insert business user
    my $business_user_id = $self->insert_business_user( { '-business_id' => $business_id, '-user_id' => $user_id } );
    if ( !defined $business_user_id ) {
        $self->error( { '-text' => 'Failed to add a new user ' . $self->dbg_value($args) } );
        return;
    }

    $self->debug(
        { '-text' => 'Registered business user account -user_id ' . $user_id . ' -business_user id ' . $business_user_id } );

    return $user_id;
}

#   activate user
sub activate_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-activate_key', '-user_id' ], $args ) ) {
        return;
    }

    #   user id
    my $user_id = $args->{'-user_id'};

    #   activate key
    my $activate_key = $args->{'-activate_key'};

    my $statement = <<'STMT';
SELECT `user_id` 
FROM   `as_user`
WHERE  `user_id`      = ?
AND    `activate_key` = ?
AND    CURRENT_TIMESTAMP() < TIMESTAMPADD(HOUR, 2, `update_ts`)
STMT

    my ($activate_user_id);
    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->bind_param( ++$bind, $activate_key );
        $sth->execute();
        ($activate_user_id) = $sth->fetchrow_array();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error(
            {   '-text' => 'Failed to activate -user_id ' .
                    $self->dbg_value($user_id) . ' -activate_key ' . $self->dbg_value($activate_key) . ' :' . $EVAL_ERROR
            } );
        return;
    }

    if ( !defined $activate_user_id ) {
        $self->error(
            {   '-text' => 'Failed to activate -user_id ' .
                    $self->dbg_value($user_id) . ' -activate_key ' . $self->dbg_value($activate_key) } );
        return;
    }

    #   set user status
    $success = $self->update_user(
        {   '-user_id'        => $user_id,
            '-user_status_id' => USER_STATUS_ACTIVE,
        } );

    if ( !$success ) {
        $self->error(
            {   '-text' => 'Failed to activate -user_id ' .
                    $self->dbg_value($user_id) . ' -activate_key ' . $self->dbg_value($activate_key) } );
        return;
    }

    $self->debug( { '-text' => 'Activated user account -user_id [' . $user_id . ']' } );

    #   set user session values
    $self->set_user_session_values( { '-user_id' => $user_id } );

    return 1;
} ## end sub activate_user

#   cancel user
sub cancel_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args ) ) {
        return;
    }

    #   user id
    my $user_id = $args->{'-user_id'};

    #   user
    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Unknown -user_id ' . $user_id } );
        return;
    }

    #   user status must be active to cancel
    if ( $user->{'-user_status_id'} != USER_STATUS_ACTIVE ) {
        $self->error( { '-text' => 'User -user_id ' . $user_id . ' is not active, cannot cancel account' } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_STATUS_CHANGE_INVALID } );
        return;
    }

    #   fetch user configuration
    my $user_config = $self->fetch_user_config( { '-user_config_id' => $user->{'-user_config_id'} } );
    if ( !defined $user_config ) {
        $self->error( { '-text' => 'Failed to fetch user config' } );
        return;
    }
    if ( !$user_config->{'-cancel_account_allowed'} ) {
        $self->error(
            {   '-text' => 'User -user_id ' .
                    $user_id . ' -user_config_id ' . $user->{'-user_config_id'} . ', cancel not allowed by configuration'
            } );

        #   set error
        $self->set_error( { '-error_code' => ERROR_CODE_USER_STATUS_CHANGE_DISALLOWED } );
        return;
    }

    #   set user status
    my $success = $self->update_user(
        {   '-user_id'        => $user_id,
            '-user_status_id' => USER_STATUS_CANCELLED,
        } );

    if ( !$success ) {
        $self->error( { '-text' => 'Failed to cancel account -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    #   logout user
    $self->logout_user();

    $self->debug( { '-text' => 'User -user_id ' . $user_id . ' account cancelled' } );

    return 1;
} ## end sub cancel_user

#   delete user
sub delete_user {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #	session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #	user id
    my $user_id = $args->{'-user_id'};

    #   user
    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Unknown user for -user_id ' . $user_id } );
        return;
    }

    #   logged in
    my $logged_in = $session_util->get_session( { '-key' => '-logged_in' } );
    if ($logged_in) {
        my $logged_in_user_id = $session_util->delete_session( { '-key' => '-user_id' } );
        if ( $logged_in_user_id == $user_id ) {

            #   logout user
            $self->logout_user();
        }
    }

    #	delete user oauth
    if ( !$self->delete_user_oauth( { '-user_id' => $user_id } ) ) {
        $self->error( { '-text' => 'Failed to delete user oauth records' } );
        return;
    }

    #   delete request records
    my $statement = <<'STMT';
DELETE
FROM as_request_status_history 
WHERE request_id IN (
    SELECT request_id 
    FROM   as_request
    WHERE user_id = ?
)    
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error(
            { '-text' => 'Failed to delete as_request_status_history records -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    $statement = <<'STMT';
DELETE
FROM as_request_data
WHERE request_id IN (
    SELECT request_id 
    FROM   as_request
    WHERE user_id = ?
)    
STMT

    $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete as_request_data records -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    $statement = <<'STMT';
DELETE
FROM as_request 
WHERE user_id = ?
STMT

    $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete as_request records -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    #   delete user contact
    $statement = <<'STMT';
DELETE
FROM as_user_contact
WHERE user_id = ?
STMT

    $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete as_user_contact records -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    #   delete business user
    $statement = <<'STMT';
DELETE
FROM as_business_user
WHERE user_id = ?
STMT

    $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete as_business_user records -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    #   delete user
    $statement = <<'STMT';
DELETE 
FROM   `as_user`
WHERE  `user_id` = ?
STMT

    $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete account -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    $self->debug( { '-text' => 'User -user_id ' . $user_id . ' account deleted' } );

    return 1;
} ## end sub delete_user

#   delete user oauth
sub delete_user_oauth {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #	user id
    my $user_id = $args->{'-user_id'};

    #   user
    my $user = $self->fetch_one_user( { '-user_id' => $user_id } );
    if ( !defined $user ) {
        $self->error( { '-text' => 'Unknown user for -user_id ' . $user_id } );
        return;
    }

    my $statement = <<'STMT';
DELETE 
FROM   `as_user_oauth`
WHERE  `user_id` = ?
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete user oauth records for -user_id [' . $user_id . ']:' . $EVAL_ERROR } );
        return;
    }

    $self->debug( { '-text' => 'User -user_id ' . $user_id . ' oauth records deleted' } );

    return 1;
} ## end sub delete_user_oauth

1;

