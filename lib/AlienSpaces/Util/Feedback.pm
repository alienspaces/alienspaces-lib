package AlienSpaces::Util::Feedback;

#
#   $Revision: 126 $
#   $Author: bwwallin $
#   $Date: 2010-05-29 16:41:02 +1000 (Sat, 29 May 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :language);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use constant {
    DEFAULT_FEEDBACK_TYPE   => 0,   #   Question
};
    
#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },
        UTIL_PHRASE(),    # phrase
        UTIL_USER(),      # user
    ];

    return $util_list;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed super init' } );
        return;
    }

    return 1;
}

#   add feedback
#   - insert into as_feedback 
sub add_feedback {
    my ( $self, $args ) = @_;

    if (! $self->check_arguments( [ '-text' ], $args ) ) {
        return;
    }

    my $user_id       = $args->{'-user_id'};
    my $name          = $args->{'-name'};
    my $email         = $args->{'-email'};
    my $text          = $args->{'-text'};

    my $feedback_type = $args->{'-feedback_type'};
    if (!defined $feedback_type) {
        $feedback_type = DEFAULT_FEEDBACK_TYPE;
    }
                   
    my $statement = <<'STMT';
INSERT INTO `as_feedback` (
    `user_id`,
    `feedback_type_id`,
    `name`,
    `email_address`,
    `text`
) VALUES (
    ?,
    ?,
    ?,
    ?,
    ?
)    
STMT

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        $sth->bind_param( ++$bind, $user_id );
        $sth->bind_param( ++$bind, $feedback_type );
        $sth->bind_param( ++$bind, $name );
        $sth->bind_param( ++$bind, $email );
        $sth->bind_param( ++$bind, $text );

        $sth->execute();
        $sth->finish();
        1;
    };

    if ( !$success ) {
        $self->error( { '-text' => 'Failed to insert feedback :' . $EVAL_ERROR } );
        return;
    }

    $self->debug(
        {   '-text' => 'Inserted new feedback -user_id ' . $self->dbg_value($user_id) . ' -name ' . $self->dbg_value($name) .
                ' -email ' . $self->dbg_value($email) . ' -text ' . $self->dbg_value($text) } );

    return 1;
} ## end sub add_feedback

1;
