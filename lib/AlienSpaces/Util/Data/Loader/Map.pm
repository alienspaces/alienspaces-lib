package AlienSpaces::Util::Loader::Map;

#
#   $Revision: 126 $
#   $Author: bwwallin $
#   $Date: 2010-05-29 16:41:02 +1000 (Sat, 29 May 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw();

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use constant { NOT_FOUND => '-999999', };

sub yes_no_map {
    my ( $self, $args ) = @_;

    my $data = lc $args->{'-data'};

    my $map = {
        'yes' => 1,
        'no'  => 0,
        'disabled' => 0,
        'enabled'  => 1,
    };

    if ( !exists $map->{$data} ) {
        return $self->NOT_FOUND;
    }

    return $map->{$data};
}

1;
