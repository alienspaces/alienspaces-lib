package AlienSpaces::Util::Data::Config::XML;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
    ];

    return $util_list;
}

#   load file config
sub load_file_config {
    my ( $self, $args ) = @_;

    #   xml libxml util
    my $xml_libxml_util = $self->utils->{ UTIL_XML_LIBXML() };

    #   config file
    my $config_file = $args->{'-file'};

    if ( !defined $config_file ) {
        $self->info( { '-text' => 'Missing config file, will attempt to derive table / column configuration from database' } );
        return;
    }

    #   raw config
    my $raw_config = $xml_libxml_util->parse( { '-file' => $config_file, '-validate' => 1 } );

    if ( !defined $raw_config->{'-elements'} || !scalar @{ $raw_config->{'-elements'} } ) {
        $self->error( { '-text' => 'Config appears to be empty' } );
        return;
    }

    #   config
    my $config = [];

    foreach my $element ( @{ $raw_config->{'-elements'} } ) {

        #   table config
        my $table_config = {};
        if ( $element->{'-name'} eq 'Table' ) {

            #   table name
            $table_config->{'-table'} = $element->{'-attributes'}->{'-name'};

            #   elements
            my $elements = $element->{'-elements'};

            foreach my $table_element ( @{$elements} ) {

                #   truncate
                if ( $table_element->{'-name'} eq 'Truncate' ) {
                    $table_config->{'-truncate'} = $table_element->{'-attributes'}->{'-data'};
                }

                #   auto increment
                if ( $table_element->{'-name'} eq 'AutoIncrement' ) {
                    $table_config->{'-auto_increment'} = { '-minimum' => $table_element->{'-attributes'}->{'-minimum'} };
                }

                #   columns
                if ( $table_element->{'-name'} eq 'Column' ) {
                    if ( !defined $table_config->{'-columns'} ) {
                        $table_config->{'-columns'} = [];
                    }
                    push @{ $table_config->{'-columns'} }, { '-name' => $table_element->{'-attributes'}->{'-name'}, };
                }
            }
        }

        push @{$config}, $table_config;
    } ## end foreach my $element ( @{ $raw_config...})

    return $config;
} ## end sub load_file_config

1;

__END__
