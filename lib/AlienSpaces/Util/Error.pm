package AlienSpaces::Util::Error;

#
#   $Revision: 126 $
#   $Author: bwwallin $
#   $Date: 2010-05-29 16:41:02 +1000 (Sat, 29 May 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :language);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },
        UTIL_PHRASE(),    # phrase
    ];

    return $util_list;
}

#   registered errors
my $ERRORS = [];

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed super init' } );
        return;
    }

    #   clear out errors
    $ERRORS = [];

    return 1;
}

sub set_error {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-error_code', '-error_text' ], $args ) ) {
        return;
    }

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   error code
    my $error_code = $args->{'-error_code'};

    #   error text
    my $error_text = $args->{'-error_text'};

    my $found = 0;

FIND_EXISTING_ERROR:
    foreach my $error ( @{$ERRORS} ) {
        if ( $error->{'-error_code'} == $error_code ) {

            #   add to existing error
            push @{ $error->{'-error_text'} }, $error_text;

            $self->debug( { '-text' => 'Registered -error_code ' . $error_code . ' -error_text ' . $error_text } );

            return 1;
        }
    }
    
    if ( !$found ) {

        #   register new error
        push @{$ERRORS},
            {
            '-error_code' => $error_code,
            '-error_text' => [$error_text],
            };
    }

    $self->debug( { '-text' => 'Registered -error_code ' . $error_code . ' -error_text ' . $error_text } );

    return 1;
} ## end sub set_error

#   return all errors
sub get_errors {
    my ( $self, $args ) = @_;

    return $ERRORS;
}

1;
