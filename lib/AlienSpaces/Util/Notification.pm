package AlienSpaces::Util::Notification;

#
#   $Revision: 126 $
#   $Author: bwwallin $
#   $Date: 2010-05-29 16:41:02 +1000 (Sat, 29 May 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :language :message-types :notification-statuses :notification-intervals);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },
        UTIL_PHRASE(),                        # phrase
        UTIL_USER(),                          # user
        UTIL_EMAIL(),                         # email
        UTIL_TEMPLATE(),                      # template
        UTIL_DB_RECORD_R_NOTIFICATION(),      # r_notifcation db rec
        UTIL_DB_RECORD_USER_NOTIFICATION()    # user_notifcation db rec
    ];

    return $util_list;
}

sub init {
    my ( $self, $args ) = @_;

    return $self->SUPER::init($args);
}

sub notification_config {
    my ( $self, $args ) = @_;

    $self->error( { '-text' => 'No configuration implemented' } );

    #
    #   sample config
    #
    #    my $config = {
    #        1  => {                                #   as_r_notification.notification_id = 1
    #            '-template_name'       => 'xxxx',  #   present in /templates/notification/xxxx.en.tt
    #            '-subject_text_method' => undef,   #   callback method to return email subject text
    #            '-data_method'         => undef,   #   callback method to return template vars
    #        },

    return;
}

#    NOTIFICATION_INTERVAL_HOURLY()
#    NOTIFICATION_INTERVAL_DAILY()
#    NOTIFICATION_INTERVAL_TWICE_DAILY()
#    NOTIFICATION_INTERVAL_WEEKLY()
#    NOTIFICATION_INTERVAL_TWICE_WEEKLY()
#    NOTIFICATION_INTERVAL_MONTHLY()
#    NOTIFICATION_INTERVAL_TWICE_MONTHLY()

sub fetch_notifications {
    my ( $self, $args ) = @_;

    my $notification_util_db_rec = $self->utils->{ UTIL_DB_RECORD_R_NOTIFICATION() };

    #   notifications
    my $notifications = $notification_util_db_rec->fetch( { '-notifcation_status_id' => NOTIFICATION_STATUS_ENABLED, } );

    foreach my $notification ( @{$notifications} ) {

        my $sql = <<'SQL';
SELECT 
      `u`.`user_id`,
      `u`.`email_address`
FROM  `as_user` u
WHERE `u`.`user_status_id` = 1
AND NOT EXISTS (
    SELECT `un`.`user_notification_id`
    FROM   `as_user_notification` un
    WHERE  `un`.`user_id` = `u`.`user_id`
    AND    `un`.`notification_id` = ?
SQL

        #   hourly
        if ( $notification->{'-notification_interval_id'} == NOTIFICATION_INTERVAL_HOURLY() ) {
            $sql .= <<'SQL';
AND  DAYOFYEAR(`un`.`entry_ts`) = DAYOFYEAR(NOW())
AND  HOUR(`un`.`entry_ts`)      = HOUR(NOW())            
SQL
        }

        #   daily
        elsif ( $notification->{'-notification_interval_id'} == NOTIFICATION_INTERVAL_DAILY() ) {
            $sql .= <<'SQL';
AND  DAYOFYEAR(`un`.`entry_ts`) = DAYOFYEAR(NOW())
SQL
        }

        #   weekly
        elsif ( $notification->{'-notification_interval_id'} == NOTIFICATION_INTERVAL_WEEKLY() ) {
            $sql .= <<'SQL';
AND  WEEKOFYEAR(`un`.`entry_ts`) = WEEKOFYEAR(NOW())
SQL
        }

        #   monthly
        elsif ( $notification->{'-notification_interval_id'} == NOTIFICATION_INTERVAL_MONTHLY() ) {
            $sql .= <<'SQL';
AND  MONTHOFYEAR(`un`.`entry_ts`) = MONTHOFYEAR(NOW())
SQL
        }

        $sql .= <<'SQL';
)
SQL

        #   user notifications
        my $user_notifications = $self->fetch_data( { '-statement' => $sql, '-bind' => [ $notification->{'-notification_id'} ] } );

        $notification->{'-user_notifications'} = $user_notifications;
    } ## end foreach my $notification ( ...)

    $self->debug( { '-text' => 'Fetched -notifications ' . $self->dbg_value($notifications) } );

    return $notifications;
} ## end sub fetch_notifications

#   send notifications
sub send_notifications {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   email util
    my $email_util = $self->utils->{ UTIL_EMAIL() };

    #   template util
    my $template_util = $self->utils->{ UTIL_TEMPLATE() };

    #   notifications
    my $notifications = $self->fetch_notifications();

    #   user notification db rec
    my $user_notification_db_rec_util = $self->utils->{ UTIL_DB_RECORD_USER_NOTIFICATION() };

    #   notification config
    my $notification_config = $self->notification_config();
    if ( !defined $notification_config ) {
        $self->error( { '-text' => 'Missing notification config' } );
        return;
    }

NOTIFICATIONS:
    foreach my $notification ( @{$notifications} ) {

        my $user_notifications = delete $notification->{'-user_notifications'};
        if ( !defined $user_notifications ) {
            $self->debug( { '-text' => 'Notification -notification_id ' . $self->dbg_value( $notification->{'-notification_id'} ) . ' has no users to send to at this time' } );
            next NOTIFICATIONS;
        }

        #   config
        my $config = $notification_config->{ $notification->{'-notification_id'} };
        if ( !defined $config ) {
            $self->error( { '-text' => 'Missing notification config for -notification_id ' . $self->dbg_value( $notification->{'-notification_id'} ) } );
            next NOTIFICATIONS;
        }

        #   data method
        my $data_method = $config->{'-data_method'};
        if ( !defined $data_method ) {
            $self->error( { '-text' => 'Missing -data_method in config for -notification_id ' . $self->dbg_value( $notification->{'-notification_id'} ) } );
            next NOTIFICATIONS;
        }

        #   template name
        my $template_name = $config->{'-template_name'};
        if ( !defined $template_name ) {
            $self->error( { '-text' => 'Missing -template_name in config for -notification_id ' . $self->dbg_value( $notification->{'-notification_id'} ) } );
            next NOTIFICATIONS;
        }

        #   subject text method
        my $subject_text_method = $config->{'-subject_text_method'};
        if ( !defined $subject_text_method ) {
            $self->error( { '-text' => 'Missing -subject_text in config for -notification_id ' . $self->dbg_value( $notification->{'-notification_id'} ) } );
            next NOTIFICATIONS;
        }

    NOTIFICATION_USERS:
        foreach my $user_notification ( @{$user_notifications} ) {

            #   user notifcation
            my $user_notification_id = $user_notification_db_rec_util->insert(
                {   '-user_id'         => $user_notification->{'-user_id'},
                    '-notification_id' => $notification->{'-notification_id'},
                } );
            if ( !$user_notification_id ) {
                $self->error( { '-text' => 'Failed to insert user notifcation record' } );
                next NOTIFICATION_USERS;
            }

            my $template_vars;
            my $success = eval {

                $self->debug( { '-text' => 'Getting data with -data_method ' . $self->dbg_value($data_method) } );

                #   template vars
                $template_vars = $self->$data_method(
                    {   '-notification'         => $notification,
                        '-user_notification'    => $user_notification,
                        '-user_notification_id' => $user_notification_id,
                    } );
            };
            if ( !$template_vars ) {
                $self->debug( { '-text' => 'No template vars returned, skipping' } );

                #   delete user notification record
                if ( !$user_notification_db_rec_util->delete( { '-user_notification_id' => $user_notification_id } ) ) {
                    $self->error( { '-text' => 'Failed to remove user notification record for -user_notification_id ' . $user_notification_id } );
                }

                next NOTIFICATION_USERS;
            }

            my $subject_text;
            $success = eval {

                $self->debug( { '-text' => 'Getting subject text with -subject_text_method ' . $self->dbg_value($subject_text_method) } );

                #   subject text
                $subject_text = $self->$subject_text_method(
                    {   '-notification'         => $notification,
                        '-user_notification'    => $user_notification,
                        '-user_notification_id' => $user_notification_id,
                    } );
            };
            if ( !$subject_text ) {
                $self->debug( { '-text' => 'No subject text returned, skipping' } );

                #   delete user notification record
                if ( !$user_notification_db_rec_util->delete( { '-user_notification_id' => $user_notification_id } ) ) {
                    $self->error( { '-text' => 'Failed to remove user notification record for -user_notification_id ' . $user_notification_id } );
                }

                next NOTIFICATION_USERS;
            }

            #   user
            my $user = $user_util->fetch_one_user( { '-user_id' => $user_notification->{'-user_id'} } );

            #   add user to template vars
            while ( my ( $k, $v ) = each %{$user} ) {
                my $var_key = lc($k);
                $var_key =~ s/^\-//sxm;
                $template_vars->{$var_key} = $user->{$k};
            }

            #   template
            #   - use Util::Template to form email body with vars
            my $body_text = $template_util->process(
                {   '-file' => 'notification/' . $template_name,
                    '-vars' => $template_vars,
                } );

            $self->debug( { '-text' => 'Sending -notification_id ' . $notification->{'-notification_id'} . ' to -user_id ' . $self->dbg_value( $user->{'-user_id'} ) } );

            if ( !defined $user->{'-email_address'} ) {
                $self->error( { '-text' => 'Notification -user_id ' . $self->dbg_value( $user->{'-user_id'} ) . ' missing email address' } );
                next EMAIL_USERS;
            }

            if (!$email_util->create_email(
                    {   '-sender'    => $self->environment->{'APP_NOREPLY_EMAIL'},
                        '-reply_to'  => $self->environment->{'APP_NOREPLY_EMAIL'},
                        '-subject'   => $subject_text,
                        '-body'      => $body_text,
                        '-recipient' => {
                            '-type'    => 'to',
                            '-address' => $user->{'-email_address'},
                        } } )
                ) {
                $self->error( { '-text' => 'Failed creating email' } );
                next NOTIFICATION_USERS;
            }

        } ## end NOTIFICATION_USERS: foreach my $user_notification...

    } ## end NOTIFICATIONS: foreach my $notification ( ...)

    return 1;
} ## end sub send_notifications

1;
