package AlienSpaces::Util::Bugzilla;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use JSON::RPC::Client;
use HTTP::Cookies;
use JSON;

use constant {
    JSON_RPC_PROGRAM => 'jsonrpc.cgi',
    JSON_LOCAL_DATA  => '/data/bugzilla.json',
};

#   cookie jar
my $cookie_jar;

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

#   request
sub request {
    my ( $self, $args ) = @_;

    #   client
    my $client = $self->get_client();

    #   url
    my $url = $args->{'-url'};

    #   data
    my $data = $args->{'-data'};

    $self->debug( { '-text' => 'Calling -url ' . $self->dbg_value($url) . ' -data ' . $self->dbg_value($data) } );

    #   call
    my $response = $client->call( $url, $data );

    if ($response) {
        if ( $response->is_error ) {
            $self->error( { '-text' => 'Response error ' . $response->error_message } );
        }
        else {
            $self->debug( { '-text' => 'Response result ' . $self->dbg_value( $response->result ) } );
        }
    }
    else {
        print $client->status_line;
    }

    return $response->result;
} ## end sub request

#   login
sub login {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-hostname', '-username', '-password' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   client
    my $client = $self->get_client();

    #   url
    my $url = $self->get_url($args);

    #   username
    my $username = $args->{'-username'};

    #   password
    my $password = $args->{'-password'};

    #   method
    my $method = 'User.login';

    #   params
    my $params = $self->get_params(
        {   'login'    => $username,
            'password' => $password,
        } );

    #   data
    my $data = {
        'method' => 'User.login',
        'params' => $params
    };

    #   response
    my $response = $self->request( { '-url' => $url, '-data' => $data } );

    return $response;
} ## end sub login

#   fetch
sub fetch {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-hostname', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   url
    my $url = $self->get_url($args);

    #   store
    my $store = delete $args->{'-store'};

    #   params
    my $params = $self->get_params( $args->{'-params'} );

    my $data = {
        'method' => 'Bug.search',
        'params' => $params,
    };

    #   response
    my $response = $self->request( { '-url' => $url, '-data' => $data } );

    #   store local
    if ($store) {
        $self->store_local( { '-data' => $response } );
    }

    return $data;
} ## end sub fetch

#   create
sub create {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-hostname', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   url
    my $url = $self->get_url($args);

    #   store
    my $store = delete $args->{'-store'};

    #   params
    my $params = $args->{'-params'};

    #   version
    if ( !defined $params->{'-version'} ) {
        $params->{'-version'} = 'Unspecified';
    }

    #   os
    if ( !defined $params->{'-op_sys'} ) {
        $params->{'-op_sys'} = 'Linux';
    }

    #   platform
    if ( !defined $params->{'-platform'} ) {
        $params->{'-platform'} = 'PC';
    }

    $params = $self->get_params($params);

    #   data
    my $data = {
        'method' => 'Bug.create',
        'params' => $params,
    };

    #   response
    my $response = $self->request( { '-url' => $url, '-data' => $data } );
    if ( !defined $response ) {
        $self->error( { '-text' => 'Failed to create new bug' } );
        return;
    }

    return $response;
} ## end sub create

#   fetch local
sub fetch_local {
    my ( $self, $args ) = @_;

    #   data file
    my $data_file = $self->get_data_file();

    #   fh
    my $fh;
    if ( !open $fh, '<', $data_file ) {
        $self->error( { '-text' => 'Failed to open -data_file ' . $self->dbg_value($data_file) . ' for read :' . $OS_ERROR } );
        return;
    }

    my $fdata = <$fh>;

    if ( !close $fh ) {
        $self->error( { '-text' => 'Failed to close -data_file ' . $self->dbg_value($data_file) . ' for read :' . $OS_ERROR } );
        return;
    }

    #   json
    my $json = JSON->new->allow_nonref;

    #   data
    my $data;
    my $success = eval {
        $data = $json->decode($fdata);
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to decode local -data ' . $self->dbg_value($fdata) . ' :' . $EVAL_ERROR } );
        return;
    }

    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to decode local -data ' . $self->dbg_value($fdata) } );
        return;
    }

    #   bugs
    my $bugs = $data->{'bugs'};
    if ( !defined $bugs || !scalar @{$bugs} ) {
        $self->debug( { '-text' => 'We have no bugs in our locale storage' } );
        return;
    }

    #   return bugs
    my $return_bugs = [];

FIND_BUGS:
    foreach my $bug ( @{$bugs} ) {

        my $add_bug = 1;
        if ( defined $args ) {
            while ( my ( $k, $v ) = each %{$args} ) {
                $k =~ s/^\-//sxmg;

                $self->debug( { '-text' => 'Testing -bug ' . $self->dbg_value($bug) . ' -k ' . $self->dbg_value($k) . ' -v ' . $self->dbg_value($v) } );

                if ( exists $bug->{$k} && $bug->{$k} ne $v ) {
                    $add_bug = 0;
                }
            }
        }
        if ( !$add_bug ) {
            $self->debug( { '-text' => 'Not adding bug' } );
            next FIND_BUGS;
        }

        push @{$return_bugs}, $bug;
    }

    $self->debug( { '-text' => 'Returning local -data ' . $self->dbg_value($return_bugs) } );

    return $return_bugs;
} ## end sub fetch_local

#   store local
sub store_local {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-data', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   data file
    my $data_file = $self->get_data_file();

    #   data
    my $data = $args->{'-data'};
    if ( !ref($data) ) {
        $self->error( { '-text' => 'Expecting data to be a reference, cannot store local' } );
        return;
    }

    #   json
    my $json = JSON->new->allow_nonref;

    my $success = eval {
        $data = $json->encode($data);
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to construct JSON from -data ' . $self->dbg_value($data) . ' :' . $EVAL_ERROR } );
        return;
    }

    #   fh
    my $fh;
    if ( !open $fh, '>', $data_file ) {
        $self->error( { '-text' => 'Failed to open -data_file ' . $self->dbg_value($data_file) . ' for writing :' . $OS_ERROR } );
        return;
    }

    print {$fh} $data;

    if ( !close $fh ) {
        $self->error( { '-text' => 'Failed to close -data_file ' . $self->dbg_value($data_file) . ' for read :' . $OS_ERROR } );
        return;
    }

    $self->debug( { '-text' => 'Stored local data' } );

    return 1;
} ## end sub store_local

#   get data file
sub get_data_file {
    my ( $self, $args ) = @_;

    return $self->environment->{'APP_HOME'} . '/' . JSON_LOCAL_DATA;
}

#   get url
sub get_url {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-hostname', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   hostname
    my $hostname = delete $args->{'-hostname'};

    #   url
    my $url = $hostname . '/' . JSON_RPC_PROGRAM;

    $self->debug( { '-text' => 'Returning -url ' . $self->dbg_value($url) } );

    return $url;
}

#   get params
sub get_params {
    my ( $self, $args ) = @_;

    #   username
    if ( defined $args->{'-username'} ) {
        $args->{'Bugzilla_login'} = delete $args->{'-username'};
    }

    #   password
    if ( defined $args->{'-password'} ) {
        $args->{'Bugzilla_password'} = delete $args->{'-password'};
    }

    #   clean params
    while ( my ( $k, $v ) = each %{$args} ) {
        if ( defined $v ) {
            my $new_k = $k;
            if ( $new_k =~ s/^\-//sxmg ) {
                $args->{$new_k} = $v;
                delete $args->{$k};
            }
        }
        else {
            delete $args->{$k};
        }
    }

    return $args;
}

sub get_client {
    my ( $self, $args ) = @_;

    if ( !defined $self->{'-client'} ) {

        #   create client
        $self->{'-client'} = JSON::RPC::Client->new();

        #   create cookie jar
        if ( !defined $cookie_jar ) {
            $cookie_jar = HTTP::Cookies->new();

            #   add cookie jar
            $self->{'-client'}->ua->cookie_jar($cookie_jar);
        }
    }

    return $self->{'-client'};
}

1;
