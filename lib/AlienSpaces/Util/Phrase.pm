package AlienSpaces::Util::Phrase;

#
#   $Revision: 157 $
#   $Author: bwwallin $
#   $Date: 2010-07-05 20:51:40 +1000 (Mon, 05 Jul 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :defaults);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    return [ @{ $self->SUPER::util_list }, 
        UTIL_SESSION(), 
        UTIL_USER(), 
    ];
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed init super class' } );
        return;
    }

    return 1;
}

sub insert_phrase {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   phrase id
    my $phrase_id = $args->{'-phrase_id'};

    #   phrase text id
    my $phrase_text_id = $args->{'-phrase_text_id'};

    #   text
    my $text = $args->{'-text'};

    #   language
    my $language = $args->{'-language'};
    if ( !defined $language ) {

        #   attempt to source language from session
        $language = $session_util->get_session( { '-key' => '-language' } );
    }

    #   insert phrase / phrase text
    my $success = eval {

        my $statement = <<'STMT';
INSERT INTO as_phrase(
    phrase_id
)
VALUES  (
    ?
)
ON DUPLICATE KEY UPDATE delete_ts = NULL
STMT

        my $sth = $self->dbh->prepare($statement);
        $sth->execute($phrase_id);

        if ( !defined $phrase_id ) {
            $phrase_id = $self->dbh->last_insert_id();
        }

        $self->debug( { '-text' => 'Added -phrase_id ' . $phrase_id } );

        #   insert the phrase text row
        if ( defined $language && defined $text ) {
            $phrase_text_id = $self->add_phrase_text(
                {   '-phrase_id'      => $phrase_id,
                    '-phrase_text_id' => $phrase_text_id,
                    '-text'           => $text,
                    '-language'       => $language,
                } );
        }
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to add phrase :' . $EVAL_ERROR } );
        return;
    }

    return ( $phrase_id, $phrase_text_id );
} ## end sub insert_phrase

sub insert_phrase_text {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-phrase_id', '-text' ], $args ) ) {
        return;
    }

    #   phrase text id
    my $phrase_text_id = $args->{'-phrase_text_id'};

    #   phrase id
    my $phrase_id = $args->{'-phrase_id'};

    #   text
    my $text = $args->{'-text'};

    #   language
    my $language = $args->{'-language'};
    if ( !defined $language ) {
        $language = DEFAULT_LANGUAGE;
    }

    my $statement = <<'STMT';
INSERT INTO as_phrase_text (
    phrase_text_id,
    phrase_id,
    language_id,
    text
)
VALUES  (
    ?, 
    ?, 
    ?, 
    ?
)
ON DUPLICATE KEY UPDATE text = ?
STMT

    my $success = eval {

        #   insert the text (or update the text if it already exists)
        my $sth = $self->dbh->prepare($statement);
        $sth->execute( $phrase_text_id, $phrase_id, $language, $text, $text );
        $sth->finish();
        1;
    };

    if ( !$success ) {
        $self->error( { '-text' => 'Failed to add phrase text :' . $EVAL_ERROR } );
        return;
    }

    if ( !defined $phrase_text_id ) {
        $phrase_text_id = $self->dbh->last_insert_id();
    }

    $self->debug( { '-text' => 'Added text for -phrase_id ' . $phrase_id . ' -language ' . $language } );

    return $phrase_text_id;
} ## end sub insert_phrase_text

#   text
sub text {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{UTIL_USER()};
    
    #   language        
    if (!defined $args->{'-language'}) {
        $args->{'-language'} = $user_util->user_language();
    }
    
    return $self->fetch_phrase_text($args);
}

#   fetch phrase text
sub fetch_phrase_text {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-phrase_id', ], $args ) ) {
        return;
    }

    #   phrase id
    my $phrase_id = $args->{'-phrase_id'};

    #   language
    my $language = $args->{'-language'};

    #   tokens or args (either works..)
    my $tokens;
    if ( exists $args->{'-tokens'} ) {
        $tokens = $args->{'-tokens'};
    }
    elsif ( exists $args->{'-args'} ) {
        $tokens = $args->{'-args'};
    }

    my ( $parent_language, $text );

    #   grab pre-prepared fetch text statement handle
    my $sql = <<'SQL_TEXT';
SELECT  l.parent_language_id,
        t.text
FROM    as_r_language l
            LEFT OUTER JOIN as_phrase_text t
                ON  t.language_id   = l.language_id
                AND t.phrase_id     = ?
WHERE   l.language_id = ?
SQL_TEXT

    my $sth;
    my $success = eval {
        $sth = $self->dbh->prepare($sql);
        1;
    };
    if ( !$success ) {
        $self->fatal( { '-text' => 'Failed to prepare statement :' . $EVAL_ERROR } );
        return;
    }

FIND_PHRASE:
    while ( !defined $text ) {

        #   if there is no parent language for the requested language drop to default language
        if ( !defined $language ) {
            $language = DEFAULT_LANGUAGE;
        }

        $success = eval {
            $sth->execute( $phrase_id, $language );
            $sth->bind_columns( \( $parent_language, $text ) );
            $sth->fetch;
            $sth->finish;
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to fetch -phrase_id ' . $phrase_id . ' :' . $EVAL_ERROR } );
            return;
        }

        if ( !defined $text ) {

            #   if we are on the default language and *still* no text, bail
            if ( $language == DEFAULT_LANGUAGE ) {
                last FIND_PHRASE;
            }
            $language = $parent_language;
        }

    }

    #   substitute phrase tokens
    if ( defined $text ) {
        $text = $self->substitute_phrase_tokens(
            {   '-text'   => $text,
                '-tokens' => $tokens,
            } );
    }

    return $text;
} ## end sub fetch_phrase_text

sub substitute_phrase_tokens {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-text'], $args ) ) {
        return;
    }

    #   text
    my $text = $args->{'-text'};

    #   tokens or args (either works..)
    my $tokens;
    if ( exists $args->{'-tokens'} ) {
        $tokens = $args->{'-tokens'};
    }
    elsif ( exists $args->{'-args'} ) {
        $tokens = $args->{'-args'};
    }

    #   substitute and environment tokens
    foreach my $token_environment_key ( @{ $self->environment_token_key_list } ) {
        if ( $text =~ /\<ENV\:$token_environment_key\>/sxmg ) {
            my $value = $self->environment->{$token_environment_key};
            $text =~ s/\<ENV\:$token_environment_key\>/$value/sxmg;
        }
    }

    #   substitute additional provided tokens
    if ( defined $tokens && keys %{$tokens} ) {
        foreach my $token ( keys %{$tokens} ) {

            my $test_token = $token;

            #   remove any hyphens and make upper case
            #   NOTE: could use $self->unprefix_keys here
            $test_token =~ s/^\-//sxmg;
            $test_token = uc($test_token);

            $self->debug( { '-text' => 'Searching for -token  ' . $test_token } );

            if ( $text =~ /\<ARG\:$test_token\>/sxmg ) {
                my $value = $tokens->{$token};
                $text =~ s/\<ARG\:$test_token\>/$value/sxmg;
            }
        }
    }

    return $text;
} ## end sub substitute_phrase_tokens

sub delete_phrase {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-phrase_id', '-language' ], $args ) ) {
        return;
    }

    my $statement;
    my $success = eval {
        my $sth;
        if ( defined $args->{'-language'} ) {

            $statement = <<'STMT';
DELETE 
FROM   as_phrase_text 
WHERE  phrase_id = ? 
AND    language_id = ?
STMT

            $sth = $self->dbh->prepare($statement);

            $sth->execute( $args->{'-phrase_id'}, $args->{'-language'}, );

            $self->debug(
                { '-text' => 'deleted text for -phrase_id ' . $args->{'-phrase_id'} . ' language_id ' . $args->{'-language'} } );
        }
        else {

            $statement = <<'STMT';
DELETE 
FROM   as_phrase_text 
WHERE  phrase_id = ? 
STMT
            $sth = $self->dbh->prepare($statement);
            $sth->execute( $args->{'-phrase_id'} );

            $statement = <<'STMT';
DELETE 
FROM   as_phrase 
WHERE  phrase_id = ? 
STMT

            $sth = $self->dbh->prepare();
            $sth->execute( $args->{'-phrase_id'} );

            $self->debug( { '-text' => 'deleted -phrase_id ' . $args->{'-phrase_id'} } );
        }
        1;
    };
    if ( !$success ) {
        $self->fatal( { '-text' => 'delete failed : ' . $EVAL_ERROR } );
        return;
    }
    return 1;
} ## end sub delete_phrase

1;
