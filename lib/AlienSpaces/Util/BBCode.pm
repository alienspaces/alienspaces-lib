package AlienSpaces::Util::BBCode;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   specific
use Parse::BBCode;

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, ];

    return $util_list;
}

#   add parser
sub add_parser {
    my ( $self, $args ) = @_;

    $self->{'-parser'} = Parse::BBCode->new();

    return 1;
}

#   render
sub render {
    my ( $self, $args ) = @_;

    if ( !defined $self->{'-parser'} ) {
        $self->add_parser();
    }

    #   parser
    my $parser = $self->{'-parser'};

    return $parser->render($args);
}

#   parse
sub parse {
    my ( $self, $args ) = @_;

    if ( !defined $self->{'-parser'} ) {
        $self->add_parser();
    }

    #   parser
    my $parser = $self->{'-parser'};

    return $parser->parse($args);
}

#   parser
sub parser {
    my ( $self, $args ) = @_;

    return $self->{'-parser'};
}

1;
