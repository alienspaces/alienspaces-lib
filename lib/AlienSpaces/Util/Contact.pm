package AlienSpaces::Util::Contact;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :request :program :comp);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_USER(),
        UTIL_BUSINESS(),

        UTIL_DB_RECORD_CONTACT(),
        UTIL_DB_RECORD_USER_CONTACT(),
        UTIL_DB_RECORD_BUSINESS_CONTACT(),
        UTIL_DB_RECORD_BUSINESS_USER_CONTACT(),
        UTIL_DB_RECORD_SYSTEM_CONTACT(),

        UTIL_DB_RECORD_R_USER_CONTACT_TYPE(),
        UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE(),
        UTIL_DB_RECORD_R_BUSINESS_USER_CONTACT_TYPE(),
    ];

    return $util_list;
}

sub fetch {
    my ( $self, $args ) = @_;

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   contacts
    my $contacts = $db_rec_contact->fetch($args);

    return $contacts;
}

sub fetch_system_contacts {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-system_contact_type_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   system contact type id
    my $system_contact_type_id = $args->{'-system_contact_type_id'};

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec system contact
    my $db_rec_system_contact = $self->utils->{ UTIL_DB_RECORD_SYSTEM_CONTACT() };

    my $contacts = [];

    #   system contact recs
    my $system_contact_recs = $db_rec_system_contact->fetch( { '-system_contact_type_id' => $system_contact_type_id } );

    if ( !defined $system_contact_recs || !scalar @{$system_contact_recs} ) {
        $self->debug( { '-text' => 'No system contacts for -system_contact_type_id ' . $system_contact_type_id } );
        return;
    }

    foreach my $system_contact_rec ( @{$system_contact_recs} ) {

        #   contact rec
        my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $system_contact_rec->{'-contact_id'}, } );
        if ( defined $contact_rec ) {
            $self->debug( { '-text' => 'Adding -contact ' . $self->dbg_value($contact_rec) } );
            push @{$contacts}, $contact_rec;
        }
    }

    return $contacts;
} ## end sub fetch_system_contacts

#   fetch user contacts
sub fetch_user_contacts {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec user contact type
    my $db_rec_user_contact_type = $self->utils->{ UTIL_DB_RECORD_R_USER_CONTACT_TYPE() };

    #   db rec user contact
    my $db_rec_user_contact = $self->utils->{ UTIL_DB_RECORD_USER_CONTACT() };

    my $contacts = [];

    #   user contact recs
    my $user_contact_recs = $db_rec_user_contact->fetch($args);

    if ( !defined $user_contact_recs || !scalar @{$user_contact_recs} ) {
        $self->debug( { '-text' => 'No user contacts for -user_contact_type_id ' . $args->{'-user_contact_type_id'} } );
        return;
    }

    foreach my $user_contact_rec ( @{$user_contact_recs} ) {

        #   user contact type
        my $user_contact_type = $self->fetch_one_user_contact_type( { '-user_contact_type_id' => $user_contact_rec->{'-user_contact_type_id'}, '-active' => 1 } );
        if ( !defined $user_contact_type ) {
            $self->debug( { '-text' => 'Failed to fetch -user_contact_type_id ' . $self->dbg_value( $user_contact_rec->{'-user_contact_type_id'} ) } );
            next;
        }

        #   contact rec
        my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $user_contact_rec->{'-contact_id'}, } );

        #   add user contact rec data to user contact rec
        while ( my ( $k, $v ) = each %{$user_contact_rec} ) {
            $contact_rec->{$k} = $v;
        }

        #   add user contact type rec data to user contact rec
        while ( my ( $k, $v ) = each %{$user_contact_type} ) {
            $contact_rec->{$k} = $v;
        }

        if ( defined $contact_rec ) {
            $self->debug( { '-text' => 'Adding -contact ' . $self->dbg_value($contact_rec) } );
            push @{$contacts}, $contact_rec;
        }
    }

    return $contacts;
} ## end sub fetch_user_contacts

sub fetch_one_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-user_contact_id', '-user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec user contact type
    my $db_rec_user_contact_type = $self->utils->{ UTIL_DB_RECORD_R_USER_CONTACT_TYPE() };

    #   db rec user contact
    my $db_rec_user_contact = $self->utils->{ UTIL_DB_RECORD_USER_CONTACT() };

    #   user contact rec
    my $user_contact_rec = $db_rec_user_contact->fetch_one($args);

    if ( !defined $user_contact_rec ) {
        $self->debug( { '-text' => 'No user contacts for -user_contact_id ' . $args->{'-user_contact_id'} . ' -user_id ' . $args->{'-user_id'} } );
        return;
    }

    #   user contact type rec
    my $user_contact_type_rec = $self->fetch_one_user_contact_type( { '-user_contact_type_id' => $user_contact_rec->{'-user_contact_type_id'}, '-active' => 1 } );
    if ( !defined $user_contact_type_rec ) {
        $self->debug( { '-text' => 'Failed to fetch -user_contact_type_id ' . $self->dbg_value( $user_contact_rec->{'-user_contact_type_id'} ) } );
        return;
    }

    #   contact rec
    my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $user_contact_rec->{'-contact_id'}, } );

    #   add user contact rec data to user contact rec
    while ( my ( $k, $v ) = each %{$user_contact_rec} ) {
        $contact_rec->{$k} = $v;
    }

    #   add user contact type rec data to user contact rec
    while ( my ( $k, $v ) = each %{$user_contact_type_rec} ) {
        $contact_rec->{$k} = $v;
    }

    return $contact_rec;
} ## end sub fetch_one_user_contact

#   fetch user contact types
sub fetch_user_contact_types {
    my ( $self, $args ) = @_;

    #   db rec user contact type util
    my $db_rec_user_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_USER_CONTACT_TYPE() };

    my $user_contact_types = $db_rec_user_contact_type_util->fetch($args);
    if ( !defined $user_contact_types || !scalar @{$user_contact_types} ) {
        $self->debug( { '-text' => 'No user contact types found' } );
        return;
    }

    return $user_contact_types;
}

#   fetch one user contact type
sub fetch_one_user_contact_type {
    my ( $self, $args ) = @_;

    #   db rec user contact type util
    my $db_rec_user_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_USER_CONTACT_TYPE() };

    my $user_contact_type = $db_rec_user_contact_type_util->fetch_one($args);
    if ( !defined $user_contact_type ) {
        $self->debug( { '-text' => 'No user contact type found' } );
        return;
    }

    return $user_contact_type;
}

#   fetch business contacts
sub fetch_business_contacts {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_CONTACT() };

    #   db rec business contact type
    my $db_rec_business_contact_type = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE() };

    my $contacts = [];

    #   business contact recs
    my $business_contact_recs = $db_rec_business_contact->fetch($args);

    if ( !defined $business_contact_recs || !scalar @{$business_contact_recs} ) {
        $self->debug( { '-text' => 'No business contacts for -business_id ' . $args->{'-business_id'} } );
        return;
    }

    foreach my $business_contact_rec ( @{$business_contact_recs} ) {

        #   business contact type
        my $business_contact_type = $self->fetch_one_business_contact_type( { '-business_contact_type_id' => $business_contact_rec->{'-business_contact_type_id'}, '-active' => 1 } );
        if ( !defined $business_contact_type ) {
            $self->debug( { '-text' => 'Failed to fetch -business_contact_type_id ' . $self->dbg_value( $business_contact_rec->{'-business_contact_type_id'} ) } );
            next;
        }

        #   contact rec
        my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $business_contact_rec->{'-contact_id'}, } );

        #   add business contact rec data to business contact rec
        while ( my ( $k, $v ) = each %{$business_contact_rec} ) {
            $contact_rec->{$k} = $v;
        }

        #   add business contact type rec data to business contact rec
        while ( my ( $k, $v ) = each %{$business_contact_type} ) {
            $contact_rec->{$k} = $v;
        }

        if ( defined $contact_rec ) {
            $self->debug( { '-text' => 'Adding -contact ' . $self->dbg_value($contact_rec) } );
            push @{$contacts}, $contact_rec;
        }
    }

    return $contacts;
} ## end sub fetch_business_contacts

sub fetch_one_business_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_contact_id', '-business_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact type
    my $db_rec_business_contact_type = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE() };

    #   db rec business contact
    my $db_rec_business_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_CONTACT() };

    #   business contact rec
    my $business_contact_rec = $db_rec_business_contact->fetch_one($args);

    if ( !defined $business_contact_rec ) {
        $self->debug( { '-text' => 'No business contacts for -business_contact_id ' . $args->{'-business_contact_id'} . ' -business_id ' . $args->{'-business_id'} } );
        return;
    }

    #   business contact type rec
    my $business_contact_type_rec = $self->fetch_one_business_contact_type( { '-business_contact_type_id' => $business_contact_rec->{'-business_contact_type_id'}, '-active' => 1 } );
    if ( !defined $business_contact_type_rec ) {
        $self->debug( { '-text' => 'Failed to fetch -business_contact_type_id ' . $self->dbg_value( $business_contact_rec->{'-business_contact_type_id'} ) } );
        return;
    }

    #   contact rec
    my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $business_contact_rec->{'-contact_id'}, } );

    #   add business contact rec data to business contact rec
    while ( my ( $k, $v ) = each %{$business_contact_rec} ) {
        $contact_rec->{$k} = $v;
    }

    #   add business contact type rec data to business contact rec
    while ( my ( $k, $v ) = each %{$business_contact_type_rec} ) {
        $contact_rec->{$k} = $v;
    }

    return $contact_rec;
} ## end sub fetch_one_business_contact

#   fetch business contact types
sub fetch_business_contact_types {
    my ( $self, $args ) = @_;

    #   db rec business contact type util
    my $db_rec_business_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE() };

    my $business_contact_types = $db_rec_business_contact_type_util->fetch($args);
    if ( !defined $business_contact_types || !scalar @{$business_contact_types} ) {
        $self->debug( { '-text' => 'No business contact types found' } );
        return;
    }

    return $business_contact_types;
}

#   fetch one business contact type
sub fetch_one_business_contact_type {
    my ( $self, $args ) = @_;

    #   db rec business contact type util
    my $db_rec_business_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE() };

    my $business_contact_type = $db_rec_business_contact_type_util->fetch_one($args);
    if ( !defined $business_contact_type ) {
        $self->debug( { '-text' => 'No business contact type found' } );
        return;
    }

    return $business_contact_type;
}

#   fetch business contacts
sub fetch_business_user_contacts {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_user_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_user_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER_CONTACT() };

    #   db rec business contact type
    my $db_rec_business_user_contact_type = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE() };

    my $contacts = [];

    #   business contact recs
    my $business_user_contact_recs = $db_rec_business_user_contact->fetch($args);

    if ( !defined $business_user_contact_recs || !scalar @{$business_user_contact_recs} ) {
        $self->debug( { '-text' => 'No business contacts for -business_user_id ' . $args->{'-business_user_id'} } );
        return;
    }

    foreach my $business_user_contact_rec ( @{$business_user_contact_recs} ) {

        #   business contact type
        my $business_user_contact_type
            = $self->fetch_one_business_user_contact_type( { '-business_user_contact_type_id' => $business_user_contact_rec->{'-business_user_contact_type_id'}, '-active' => 1 } );
        if ( !defined $business_user_contact_type ) {
            $self->debug( { '-text' => 'Failed to fetch -business_user_contact_type_id ' . $self->dbg_value( $business_user_contact_rec->{'-business_user_contact_type_id'} ) } );
            next;
        }

        #   contact rec
        my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $business_user_contact_rec->{'-contact_id'}, } );

        #   add business contact rec data to business contact rec
        while ( my ( $k, $v ) = each %{$business_user_contact_rec} ) {
            $contact_rec->{$k} = $v;
        }

        #   add business contact type rec data to business contact rec
        while ( my ( $k, $v ) = each %{$business_user_contact_type} ) {
            $contact_rec->{$k} = $v;
        }

        if ( defined $contact_rec ) {
            $self->debug( { '-text' => 'Adding -contact ' . $self->dbg_value($contact_rec) } );
            push @{$contacts}, $contact_rec;
        }
    }

    return $contacts;
} ## end sub fetch_business_user_contacts

sub fetch_one_business_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_user_contact_id', '-business_user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_user_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER_CONTACT() };

    #   db rec business contact type
    my $db_rec_business_user_contact_type = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_CONTACT_TYPE() };

    #   business contact rec
    my $business_user_contact_rec = $db_rec_business_user_contact->fetch_one($args);

    if ( !defined $business_user_contact_rec ) {
        $self->debug( { '-text' => 'No business contacts for -business_user_contact_id ' . $args->{'-business_user_contact_id'} . ' -business_user_id ' . $args->{'-business_user_id'} } );
        return;
    }

    #   business contact type rec
    my $business_user_contact_type_rec
        = $self->fetch_one_business_user_contact_type( { '-business_user_contact_type_id' => $business_user_contact_rec->{'-business_user_contact_type_id'}, '-active' => 1 } );
    if ( !defined $business_user_contact_type_rec ) {
        $self->debug( { '-text' => 'Failed to fetch -business_user_contact_type_id ' . $self->dbg_value( $business_user_contact_rec->{'-business_user_contact_type_id'} ) } );
        return;
    }

    #   contact rec
    my $contact_rec = $db_rec_contact->fetch_one( { '-contact_id' => $business_user_contact_rec->{'-contact_id'}, } );

    #   add business contact rec data to business contact rec
    while ( my ( $k, $v ) = each %{$business_user_contact_rec} ) {
        $contact_rec->{$k} = $v;
    }

    #   add business contact type rec data to business contact rec
    while ( my ( $k, $v ) = each %{$business_user_contact_type_rec} ) {
        $contact_rec->{$k} = $v;
    }

    return $contact_rec;
} ## end sub fetch_one_business_user_contact

#   fetch business contact types
sub fetch_business_user_contact_types {
    my ( $self, $args ) = @_;

    #   db rec business contact type util
    my $db_rec_business_user_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_USER_CONTACT_TYPE() };

    my $business_user_contact_types = $db_rec_business_user_contact_type_util->fetch($args);
    if ( !defined $business_user_contact_types || !scalar @{$business_user_contact_types} ) {
        $self->debug( { '-text' => 'No business contact types found' } );
        return;
    }

    return $business_user_contact_types;
}

#   fetch one business contact type
sub fetch_one_business_user_contact_type {
    my ( $self, $args ) = @_;

    #   db rec business contact type util
    my $db_rec_business_user_contact_type_util = $self->utils->{ UTIL_DB_RECORD_R_BUSINESS_USER_CONTACT_TYPE() };

    my $business_user_contact_type = $db_rec_business_user_contact_type_util->fetch_one($args);
    if ( !defined $business_user_contact_type ) {
        $self->debug( { '-text' => 'No business contact type found' } );
        return;
    }

    return $business_user_contact_type;
}

#   insert user contact
sub insert_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-user_contact_type_id', '-user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec user contact
    my $db_rec_user_contact = $self->utils->{ UTIL_DB_RECORD_USER_CONTACT() };

    #   user id
    my $user_id = $args->{'-user_id'};

    #   user contact type id
    my $user_contact_type_id = $args->{'-user_contact_type_id'};

    #   insert contact
    my $contact_id = $db_rec_contact->insert($args);
    if ( !defined $contact_id ) {
        $self->error( { '-text' => 'Failed to insert contact' } );
        return;
    }

    #   insert user contact
    my $user_contact_id = $db_rec_user_contact->insert(
        {   '-user_id'              => $user_id,
            '-user_contact_type_id' => $user_contact_type_id,
            '-contact_id'           => $contact_id,
        } );

    if ( !defined $user_contact_id ) {
        $self->error( { '-text' => 'Failed to insert user contact' } );
        return;
    }

    return $user_contact_id;
} ## end sub insert_user_contact

#   update user contact
sub update_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-user_contact_id', '-user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user contact id
    my $user_contact_id = delete $args->{'-user_contact_id'};

    #   user id
    my $user_id = delete $args->{'-user_id'};

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec user contact
    my $db_rec_user_contact = $self->utils->{ UTIL_DB_RECORD_USER_CONTACT() };

    #   user contact
    my $user_contact = $db_rec_user_contact->fetch_one( { '-user_contact_id' => $user_contact_id, '-user_id' => $user_id } );
    if ( !defined $user_contact ) {
        $self->error( { '-text' => 'Failed to fetch -user_contact_id ' . $self->dbg_value($user_contact_id) } );
        return;
    }

    #   contact id
    my $contact_id = $user_contact->{'-contact_id'};

    $args->{'-contact_id'} = $contact_id;

    #   update contact
    if ( !$db_rec_contact->update($args) ) {
        $self->error( { '-text' => 'Failed to update contact' } );
        return;
    }

    $self->debug( { '-text' => 'Updated -user_contact_id ' . $self->dbg_value($user_contact_id) . ' -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub update_user_contact

#   delete user contact
sub delete_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-user_contact_id', '-user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user contact id
    my $user_contact_id = delete $args->{'-user_contact_id'};

    #   user id
    my $user_id = delete $args->{'-user_id'};

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec user contact
    my $db_rec_user_contact = $self->utils->{ UTIL_DB_RECORD_USER_CONTACT() };

    #   user contact
    my $user_contact = $db_rec_user_contact->fetch_one( { '-user_contact_id' => $user_contact_id, '-user_id' => $user_id } );
    if ( !defined $user_contact ) {
        $self->error( { '-text' => 'Failed to fetch -user_contact_id ' . $self->dbg_value($user_contact_id) } );
        return;
    }

    #   contact id
    my $contact_id = $user_contact->{'-contact_id'};

    #   delete user contact record
    if ( !$db_rec_user_contact->delete( { '-user_contact_id' => $user_contact_id } ) ) {
        $self->error( { '-text' => 'Failed to delete -user_contact_id ' . $self->dbg_value($user_contact_id) } );
        return;
    }
    
    #   delete contact record
    if ( !$db_rec_contact->delete( { '-contact_id' => $contact_id } ) ) {
        $self->error( { '-text' => 'Failed to delete -contact_id ' . $self->dbg_value($contact_id) } );
        return;
    }

    $self->debug( { '-text' => 'Deleted -user_contact_id ' . $self->dbg_value($user_contact_id) . ' -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub delete_user_contact

#   fetch user for contact
sub fetch_user_for_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-contact_id', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec user contact
    my $db_rec_user_contact = $self->utils->{ UTIL_DB_RECORD_USER_CONTACT() };

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   user contact rec
    my $user_contact_rec = $db_rec_user_contact->fetch_one($args);

    if ( !defined $user_contact_rec ) {
        $self->debug( { '-text' => 'No user contact record found for -contact_id ' . $args->{'-contact_id'} } );
        return;
    }

    #   user id
    $args->{'-user_id'} = $user_contact_rec->{'-user_id'};

    #   user
    my $user = $user_util->fetch_one_user($args);
    if ( !defined $user ) {
        $self->error( { '-text' => 'Failed to fetch user with -args ' . $self->dbg_value($args) } );
        return;
    }

    return $user;
} ## end sub fetch_user_for_contact

#   insert business contact
sub insert_business_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_contact_type_id', '-business_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_CONTACT() };

    #   business id
    my $business_id = $args->{'-business_id'};

    #   business contact type id
    my $business_contact_type_id = $args->{'-business_contact_type_id'};

    #   insert contact
    my $contact_id = $db_rec_contact->insert($args);
    if ( !defined $contact_id ) {
        $self->error( { '-text' => 'Failed to insert contact' } );
        return;
    }

    #   insert business contact
    my $business_contact_id = $db_rec_business_contact->insert(
        {   '-business_id'              => $business_id,
            '-business_contact_type_id' => $business_contact_type_id,
            '-contact_id'               => $contact_id,
        } );

    if ( !defined $business_contact_id ) {
        $self->error( { '-text' => 'Failed to insert business contact' } );
        return;
    }

    return $business_contact_id;
} ## end sub insert_business_contact

#   update business contact
sub update_business_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_contact_id', '-business_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business contact id
    my $business_contact_id = delete $args->{'-business_contact_id'};

    #   business id
    my $business_id = delete $args->{'-business_id'};

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_CONTACT() };

    #   business contact
    my $business_contact = $db_rec_business_contact->fetch_one( { '-business_contact_id' => $business_contact_id, '-business_id' => $business_id } );
    if ( !defined $business_contact ) {
        $self->error( { '-text' => 'Failed to fetch -business_contact_id ' . $self->dbg_value($business_contact_id) } );
        return;
    }

    #   contact id
    my $contact_id = $business_contact->{'-contact_id'};

    $args->{'-contact_id'} = $contact_id;

    #   update contact
    if ( !$db_rec_contact->update($args) ) {
        $self->error( { '-text' => 'Failed to update contact' } );
        return;
    }

    $self->debug( { '-text' => 'Updated -business_contact_id ' . $self->dbg_value($business_contact_id) . ' -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub update_business_contact

#   delete business contact
sub delete_business_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_contact_id', '-business_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business contact id
    my $business_contact_id = delete $args->{'-business_contact_id'};

    #   business id
    my $business_id = delete $args->{'-business_id'};

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_CONTACT() };

    #   business contact
    my $business_contact = $db_rec_business_contact->fetch_one( { '-business_contact_id' => $business_contact_id, '-business_id' => $business_id } );
    if ( !defined $business_contact ) {
        $self->error( { '-text' => 'Failed to fetch -business_contact_id ' . $self->dbg_value($business_contact_id) } );
        return;
    }

    #   contact id
    my $contact_id = $business_contact->{'-contact_id'};

    #   delete business contact
    if ( !$db_rec_business_contact->delete( { '-business_contact_id' => $business_contact_id } ) ) {
        $self->error( { '-text' => 'Failed to delete -business_contact_id ' . $self->dbg_value($business_contact_id) } );
        return;
    }

    #   delete contact
    if ( !$db_rec_contact->delete( { '-contact_id' => $contact_id } ) ) {
        $self->error( { '-text' => 'Failed to delete -contact_id ' . $self->dbg_value($contact_id) } );
        return;
    }


    $self->debug( { '-text' => 'Deleted -business_contact_id ' . $self->dbg_value($business_contact_id) . ' -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub delete_business_contact

#   fetch business for contact
sub fetch_business_for_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-contact_id', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec business contact
    my $db_rec_business_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_CONTACT() };

    #   business util
    my $business_util = $self->utils->{ UTIL_BUSINESS() };

    #   business contact rec
    my $business_contact_rec = $db_rec_business_contact->fetch_one($args);

    if ( !defined $business_contact_rec ) {
        $self->debug( { '-text' => 'No business contact record found for -contact_id ' . $args->{'-contact_id'} } );
        return;
    }

    #   business id
    $args->{'-business_id'} = $business_contact_rec->{'-business_id'};

    #   business
    my $business = $business_util->fetch_one_business($args);
    if ( !defined $business ) {
        $self->error( { '-text' => 'Failed to fetch business with -args ' . $self->dbg_value($args) } );
        return;
    }

    return $business;
} ## end sub fetch_business_for_contact

#   insert business user contact
sub insert_business_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_user_contact_type_id', '-business_user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_user_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER_CONTACT() };

    #   business user id
    my $business_user_id = $args->{'-business_user_id'};

    #   business user contact type id
    my $business_user_contact_type_id = $args->{'-business_user_contact_type_id'};

    #   insert contact
    my $contact_id = $db_rec_contact->insert($args);
    if ( !defined $contact_id ) {
        $self->error( { '-text' => 'Failed to insert contact' } );
        return;
    }

    #   insert business contact
    my $business_user_contact_id = $db_rec_business_user_contact->insert(
        {   '-business_user_id'              => $business_user_id,
            '-business_user_contact_type_id' => $business_user_contact_type_id,
            '-contact_id'                    => $contact_id,
        } );

    if ( !defined $business_user_contact_id ) {
        $self->error( { '-text' => 'Failed to insert business user contact' } );
        return;
    }

    return $business_user_contact_id;
} ## end sub insert_business_user_contact

#   update business user contact
sub update_business_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_user_contact_id', '-business_user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business user contact id
    my $business_user_contact_id = delete $args->{'-business_user_contact_id'};

    #   business user id
    my $business_user_id = delete $args->{'-business_user_id'};

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business user contact
    my $db_rec_business_user_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER_CONTACT() };

    #   business contact
    my $business_user_contact = $db_rec_business_user_contact->fetch_one( { '-business_user_contact_id' => $business_user_contact_id, '-business_user_id' => $business_user_id } );
    if ( !defined $business_user_contact ) {
        $self->error( { '-text' => 'Failed to fetch -business_user_contact_id ' . $self->dbg_value($business_user_contact_id) } );
        return;
    }

    #   contact id
    my $contact_id = $business_user_contact->{'-contact_id'};

    $args->{'-contact_id'} = $contact_id;

    #   update contact
    if ( !$db_rec_contact->update($args) ) {
        $self->error( { '-text' => 'Failed to update contact' } );
        return;
    }

    $self->debug( { '-text' => 'Updated -business_user_contact_id ' . $self->dbg_value($business_user_contact_id) . ' -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub update_business_user_contact

#   delete business user contact
sub delete_business_user_contact {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-business_user_contact_id', '-business_user_id' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business user contact id
    my $business_user_contact_id = delete $args->{'-business_user_contact_id'};

    #   business user id
    my $business_user_id = delete $args->{'-business_user_id'};

    #   db rec contact
    my $db_rec_contact = $self->utils->{ UTIL_DB_RECORD_CONTACT() };

    #   db rec business contact
    my $db_rec_business_user_contact = $self->utils->{ UTIL_DB_RECORD_BUSINESS_USER_CONTACT() };

    #   business contact
    my $business_user_contact = $db_rec_business_user_contact->fetch_one( { '-business_user_contact_id' => $business_user_contact_id, '-business_user_id' => $business_user_id } );
    if ( !defined $business_user_contact ) {
        $self->error( { '-text' => 'Failed to fetch -business_user_contact_id ' . $self->dbg_value($business_user_contact_id) } );
        return;
    }

    #   contact id
    my $contact_id = $business_user_contact->{'-contact_id'};

    #   delete business user contact
    if ( !$db_rec_business_user_contact->delete( { '-business_user_contact_id' => $business_user_contact_id } ) ) {
        $self->error( { '-text' => 'Failed to delete -business_user_contact_id ' . $self->dbg_value($business_user_contact_id) } );
        return;
    }

    #   delete contact
    if ( !$db_rec_contact->delete( { '-contact_id' => $contact_id } ) ) {
        $self->error( { '-text' => 'Failed to delete -contact_id ' . $self->dbg_value($contact_id) } );
        return;
    }

    $self->debug( { '-text' => 'Deleted -business_user_contact_id ' . $self->dbg_value($business_user_contact_id) . ' -contact_id ' . $self->dbg_value($contact_id) } );

    return 1;
} ## end sub delete_business_user_contact

1;
