package AlienSpaces::Util::CGI;

#
#   $Revision: 155 $
#   $Author: bwwallin $
#   $Date: 2010-07-01 18:43:31 +1000 (Thu, 01 Jul 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

use base qw(CGI);

our $VERSION = (qw$Revision: $)[1];

AlienSpaces::Util::CGI->compile();

sub new {
    my ($class, $args) = @_;
 
    my $self = $class->SUPER::new($args->{'-apache_request'});
    
    return $self;
}

sub init {
    my ($self, $args) = @_;
    
    if (!$self->SUPER::init($args)) {
        return;
    }
        
    $CGI::XHTML = 0;

    return 1;
}
        
sub finished {
    my ($self, $value) = @_;
    
    if (defined $value) {
        $self->{'-finished'} = $value;
    }
    
    return $self->{'-finished'};
}    
        
sub finish {
    my ($self, $args) = @_;
    
    return;
}
    
1;
