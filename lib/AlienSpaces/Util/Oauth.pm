package AlienSpaces::Util::Oauth;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :comp :program :oauth);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use LWP::UserAgent;
use JSON;

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },

        #   oauth db rec util
        UTIL_DB_RECORD_R_OAUTH(),

        #   oauth config db rec util
        UTIL_DB_RECORD_R_OAUTH_CONFIG(),

        #   util user
        UTIL_USER(),

        #   util session
        UTIL_SESSION(),

        #   util output html navigation
        UTIL_OUTPUT_HTML_NAVIGATION(),
    ];

    return $util_list;
}

my $local_config = {
    OAUTH_FACEBOOK() => {
        '-user_path'           => '/me',
        '-auth_args'           => { 
            'response_type' => 'code', 
            'scope'         => 'email',
        },
        '-token_data_user_key' => undef,
    },
    OAUTH_GOOGLE() => {
        '-user_path' => '/userinfo',
        '-auth_args' => {
            'scope'         => 'https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/userinfo.profile',
            'response_type' => 'code',
        },
        '-token_data_user_key' => undef,
    },
    OAUTH_INSTAGRAM() => {
        '-user_path' => undef,
        '-auth_args' => {
            'scope'      => 'basic',
            'grant_type' => 'authorization_code',
        },
        '-token_data_user_key' => 'user',
    },
};

sub config {
    my ( $self, $args ) = @_;

    return $local_config;
}

sub fetch_oauths {
    my ( $self, $args ) = @_;

    #   oauth config db rec util
    my $oauth_config_db_rec_util = $self->utils->{ UTIL_DB_RECORD_R_OAUTH_CONFIG() };

    #   oauth db rec util
    my $oauth_db_rec_util = $self->utils->{ UTIL_DB_RECORD_R_OAUTH() };

    my $oauth_config_recs = $oauth_config_db_rec_util->fetch($args);
    if ( defined $oauth_config_recs && scalar @{$oauth_config_recs} ) {

        foreach my $oauth_config_rec ( @{$oauth_config_recs} ) {

            my $oauth_rec = $oauth_db_rec_util->fetch_one( { '-oauth_id' => $oauth_config_rec->{'-oauth_id'} } );
            if ( defined $oauth_rec ) {
                $oauth_config_rec = { %{$oauth_config_rec}, %{$oauth_rec} };
            }
        }
    }

    $self->debug( { '-text' => 'Have -oauth_config_recs ' . $self->dbg_value($oauth_config_recs) } );

    return $oauth_config_recs;
}

sub fetch_oauth {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-oauth_id', '-local_domain' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   oauth rec
    my $oauth_rec = $self->fetch_oauths($args);
    if ( ref($oauth_rec) eq 'ARRAY' && scalar @{$oauth_rec} == 1 ) {
        $oauth_rec = $oauth_rec->[0];
    }

    return $oauth_rec;
}

sub get_oauth_login_uri {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-oauth_id', '-local_domain' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    #   oauth id
    my $oauth_id = $args->{'-oauth_id'};

    #   config
    my $config = $self->config->{$oauth_id};
    if ( !defined $config ) {
        $self->error( { '-text' => 'Missing config for -auth_id ' . $self->dbg_value($oauth_id) } );
        return;
    }

    #   oauth rec
    my $oauth_rec = $self->fetch_oauth($args);
    if ( !defined $oauth_rec ) {
        $self->error( { '-text' => 'Failed to fetch oauth record for -oauth_id ' . $self->dbg_value($oauth_id) } );
        return;
    }

    #   oauth app id
    my $oauth_app_id = $oauth_rec->{'-oauth_app_id'};

    #   auth uri
    my $auth_uri = $oauth_rec->{'-auth_uri'};

    if ( !defined $oauth_app_id || !defined $auth_uri ) {
        $self->error( { '-text' => 'Not all required oauth configuration data available' } );
        return;
    }

    #   oauth redirect uri
    my $oauth_redirect_uri = $self->html_escape( $self->get_oauth_redirect_uri() );

    #   auth args
    my $auth_args = $config->{'-auth_args'};

    my $oauth_login_url = $self->make_program_link(
        {   '-url'          => $auth_uri,
            '-client_id'    => $oauth_app_id,
            '-redirect_uri' => $oauth_redirect_uri,
            '-state'        => $user_util->user_session_id,
            %{$auth_args},
        } );

    return $oauth_login_url;
} ## end sub get_oauth_login_uri

#   get oauth redirect uri
sub get_oauth_redirect_uri {
    my ( $self, $args ) = @_;

    #   user util
    my $user_util = $self->utils->{ UTIL_USER() };

    my $oauth_redirect_uri = $self->make_program_link( { '-program' => PROGRAM_SITE_USER_OAUTH_REGISTER(), } );

    return $oauth_redirect_uri;
}

#   get access token
sub get_access_token_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-oauth_id', '-local_domain' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   oauth id
    my $oauth_id = $args->{'-oauth_id'};

    #   config
    my $config = $self->config->{$oauth_id};
    if ( !defined $config ) {
        $self->error( { '-text' => 'Missing config for -auth_id ' . $self->dbg_value($oauth_id) } );
        return;
    }

    #   local domain
    my $local_domain = $args->{'-local_domain'};

    #   code
    my $code = $args->{'-code'};

    #   data
    my $data;

    if ( !defined $code ) {

        #   data
        $data = $session_util->get_session( { '-key' => '-oauth_token_data' } );

        #   no token data in session
        if ( !defined $data ) {
            $self->error( { '-text' => 'Failed to fetch session stored oauth token data' } );
            return;
        }

        #   token data does not match requested oauth id token data
        if ( defined $oauth_id && $oauth_id != $data->{'-oauth_id'} ) {
            $self->error( { '-text' => 'Session stored -data ' . $self->dbg_value($data) . ' does not match requested -oauth_id ' . $self->dbg_value($oauth_id) } );
            return;
        }

        $self->debug( { '-text' => 'Returning -data ' . $self->dbg_value($data) } );

        return $data;
    }

    #   oauth rec
    my $oauth_rec = $self->fetch_oauth( { '-oauth_id' => $oauth_id, '-local_domain' => $local_domain } );

    #   oauth app id
    my $oauth_app_id = $oauth_rec->{'-oauth_app_id'};

    #   oauth app secret
    my $oauth_app_secret = $oauth_rec->{'-oauth_app_secret'};

    #   oauth token uri
    my $token_uri = $oauth_rec->{'-token_uri'};

    #   oauth redirect uri
    my $oauth_redirect_uri = $self->get_oauth_redirect_uri();

    $data = $self->make_oauth_request(
        {   '-uri'    => $token_uri,
            '-params' => {
                'client_id'     => $oauth_app_id,
                'client_secret' => $oauth_app_secret,
                'code'          => $code,
                'redirect_uri'  => $oauth_redirect_uri,
                'grant_type'    => 'authorization_code',
            } } );

    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to get access token data' } );
        return;
    }

    #   add oauth id for reference
    $data->{'-oauth_id'} = $oauth_id;

    $self->debug( { '-text' => 'Returning token -data ' . $self->dbg_value($data) } );

    #   set session oauth token
    $session_util->set_session( { '-key' => '-oauth_token_data', '-value' => $data } );

    return $data;
} ## end sub get_access_token_data

#   get user data
sub get_user_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-oauth_id', '-local_domain' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   auth id
    my $oauth_id = $args->{'-oauth_id'};

    #   config
    my $config = $self->config->{$oauth_id};
    if ( !defined $config ) {
        $self->error( { '-text' => 'Missing config for -auth_id ' . $self->dbg_value($oauth_id) } );
        return;
    }

    #   local domain
    my $local_domain = $args->{'-local_domain'};

    #   code
    my $code = $args->{'-code'};

    #   access token data
    my $access_token_data = $self->get_access_token_data( { '-oauth_id' => $oauth_id, '-code' => $code, '-local_domain' => $local_domain } );
    if ( !defined $access_token_data || !defined $access_token_data->{'-access_token'} ) {
        $self->error( { '-text' => 'Failed to get token data' } );
        return;
    }

    $self->debug( { '-text' => 'Have -access_token_data ' . $self->dbg_value($access_token_data) } );

    #   data
    my $data;
    if ( $config->{'-token_data_user_key'} ) {
        $data = $access_token_data->{ $config->{'-token_data_user_key'} };
        if ( defined $data ) {
            $self->debug( { '-text' => 'Token data contains user -data ' . $self->dbg_value($data) } );
            return $data;
        }
    }

    #   oauth id
    $oauth_id = $access_token_data->{'-oauth_id'};

    #   oauth rec
    my $oauth_rec = $self->fetch_oauth( { '-oauth_id' => $oauth_id, '-local_domain' => $local_domain } );

    #   oauth app id
    my $oauth_app_id = $oauth_rec->{'-oauth_app_id'};

    #   oauth app secret
    my $oauth_app_secret = $oauth_rec->{'-oauth_app_secret'};

    #   oauth token uri
    my $token_uri = $oauth_rec->{'-token_uri'};

    #   oauth redirect uri
    my $oauth_redirect_uri = $self->get_oauth_redirect_uri();

    #   oauth api uri
    my $oauth_api_uri = $oauth_rec->{'-api_uri'};

    #   oauth user path
    my $oauth_user_path = $self->config->{$oauth_id}->{'-user_path'};

    $data = $self->make_oauth_request(
        {   '-uri'    => $oauth_api_uri . $oauth_user_path,
            '-params' => { 'access_token' => $access_token_data->{'-access_token'}, },
            '-method' => 'get'
        } );

    if ( !defined $data ) {
        $self->error( { '-text' => 'Failed to get user data' } );
        return;
    }

    
    #   oauth id
    $data->{'-oauth_id'} = $oauth_id;

    $self->debug( { '-text' => 'User data requests returned -data ' . $self->dbg_value($data) } );

    return $data;
} ## end sub get_user_data

#   make oauth request
sub make_oauth_request {
    my ( $self, $args ) = @_;

    my $uri    = $args->{'-uri'};
    my $params = $args->{'-params'};
    my $method = $args->{'-method'} || 'post';

    #   user agen
    my $ua = LWP::UserAgent->new;
    $ua->timeout(10);

    #   json
    my $json = JSON->new->allow_nonref;

    #   response
    my $response;

    if ( $method eq 'post' ) {
        $response = $ua->post( $uri, $params );
    }
    else {
        if ( defined $params ) {
            $uri .= '?';
            while ( my ( $k, $v ) = each %{$params} ) {
                $k =~ s/^\-//sxmg;
                $uri .= $k . '=' . $v . '&';
            }
        }
        $response = $ua->get($uri);
    }

    if ( !$response->is_success ) {
        my $request = $response->request;
        $self->error( { '-text' => 'Failure -status_line ' . $response->status_line . ' -response ' . $response->as_string . ' -request ' . ( defined $request ? $request->as_string : '' ) } );
        return;
    }

    #   content
    my $content = $response->content;

    my $data = {};
    if ( $content =~ /^[\[\{]/sxmg ) {

        my $success = eval {
            $data = $json->decode($content);
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed parsing -content ' . $self->dbg_value($content) . ' :' . $EVAL_ERROR } );
            return COMP_ACTION_FAILURE();
        }

        $self->debug( { '-text' => 'Success ' . $self->dbg_value($data) } );
    }
    else {
        my @pairs = split /\&/sxm, $content;
        foreach my $pair (@pairs) {
            my ( $k, $v ) = split /\=/sxm, $pair;
            $data->{$k} = $v;
        }
    }

    #   prefix keys
    $data = $self->prefix_keys($data);

    return $data;
} ## end sub make_oauth_request

1;
