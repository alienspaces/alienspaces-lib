package AlienSpaces::Util::DB::View;

#
#   $Revision: 142 $
#   $Author: bwwallin $
#   $Date: 2010-06-15 21:44:39 +1000 (Tue, 15 Jun 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use constant {
    TYPE_COLUMNS     => 1,
    TYPE_CONSTRAINTS => 2,
};

use AlienSpaces::Constants qw(:util :reference);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_DB_MYSQL(),
        UTIL_DB_BASIC(),
        UTIL_PHRASE(),
    ];

    return $util_list;
}

sub fetch_columns {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-view_name'], $args ) ) {
        return;
    }

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };
    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };

    #   view name
    my $view_name = $args->{'-view_name'};

    #   cache key
    my $cache_key = UTIL_DB_VIEW() . '-columns-' . $view_name;

    #   columns
    my $columns = $self->get_cached( { '-key' => $cache_key } );
    if ( defined $columns ) {
        return $columns;
    }

    #   database
    my $database = $db_mysql_util->database();

    #   bind
    my @bind = ( $database, $view_name );

    my $statement = <<'STMT';
SELECT `c`.`column_name`, `c`.`data_type`, `c`.`is_nullable`, 
       `c`.`character_maximum_length`, `c`.`numeric_precision`, 
       `c`.`extra`, `c`.`column_comment`
FROM   `information_schema`.`columns` c
WHERE  `c`.`table_schema` = ?
AND    `c`.`table_name` = ?
ORDER BY `c`.`ordinal_position` ASC
STMT

    #   data
    my $data = $db_basic_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    #   set cached
    $self->set_cached( { '-key' => $cache_key, '-value' => $data, '-clear' => 0 } );

    return $data;
} ## end sub fetch_columns

sub fetch_constraints {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-view_name'], $args ) ) {
        return;
    }

    return;
}

sub fetch_primary_key_columns {
    my ( $self, $args ) = @_;

    #   view_name
    my $view_name = $args->{'-view_name'};

    my $constraints = $self->fetch_constraints(
        {   '-view_name'       => $view_name,
            '-constraint_type' => 'PRIMARY KEY',
        } );

    return $constraints;
}

1;

