package AlienSpaces::Util::DB::Record::R::UserStatus;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util::DB::Record);

our $VERSION = (qw$Revision: $)[1];

#   config
sub config {
    my ( $self, $args ) = @_;

    my $type = {
        '-table_name' => 'as_r_user_status',
        '-key'        => 'user_status',
    };

    return $type;
}

1;
