package AlienSpaces::Util::DB::Record::R::BusinessConfig;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util::DB::Record);

our $VERSION = (qw$Revision: $)[1];

#   config
sub config {
    my ( $self, $args ) = @_;

    my $config = {
        '-table_name' => 'as_r_business_config',
        '-key'        => 'business_configs',
    };

    return $config;
}

1;
