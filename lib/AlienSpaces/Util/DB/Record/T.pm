package AlienSpaces::Util::DB::Record::T;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util::DB::Record);

our $VERSION = (qw$Revision: $)[1];

#   config
sub config {
    my ( $self, $args ) = @_;

    my $config = {
        '-table_name' => 'xxxx',
        '-key'        => 'xxxx',
        '-children'   => [
            {   '-class'        => 'DB::Record::XXXX',
                '-join_columns' => [ 'xxxxxx', ] }
        ],
        '-order_by' => 'xxxx ASC|DESC',
    };

    return $config;
}

1;
