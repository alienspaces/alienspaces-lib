package AlienSpaces::Util::DB::Record::UserView;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util::DB::Record);

our $VERSION = (qw$Revision: $)[1];

#   config
sub config {
    my ( $self, $args ) = @_;

    my $config = {
        '-view_name' => 'as_user_view',
        '-key'        => 'users',
        '-children'   => [
        ],
    };

    return $config;
}

1;
