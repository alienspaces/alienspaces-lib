package AlienSpaces::Util::DB::Data;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [ @{ $self->SUPER::util_list }, UTIL_PHRASE(), UTIL_DB_TABLE(), ];

    return $util_list;
}

sub new {
    my ( $class, $args ) = @_;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->fatal( { '-text' => 'Failed to initialise super class' } );
    }

    return 1;
}

sub fetch_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-tables', ], $args ) ) {
        return;
    }

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   tables
    my $tables = $args->{'-tables'};
    if ( !defined $tables ) {
        $self->error( { '-text' => 'Missing -tables, cannot fetch data' } );
        return;
    }
    if ( ref($tables) ne 'ARRAY' ) {
        $tables = [$tables];
    }

    #   select
    my $select_clause = <<'STMT';
SELECT
STMT

    #    from
    my $from_clause = <<'STMT';
FROM
STMT

    #   build table config
    my $table_config = {};

BUILD_SELECT:
    foreach my $table ( @{$tables} ) {
        my $table_name = $table->{'-table_name'};
        if ( !defined $table_name ) {
            $self->error( { '-text' => 'Missing -table_name from config, cannot process table' } );
            next BUILD_SELECT;
        }
        my $table_columns = $db_table_util->fetch_columns( { '-table_name' => $table_name } );
        my $table_constraints = $db_table_util->fetch_constraints( { '-table_name' => $table_name } );

        $table_config->{$table_name} = {
            '-columns'     => $table_columns,
            '-constraints' => $table_constraints,
        };

        #   table alias
        my $table_alias = $table->{'-table_alias'};

        #   add to select clause
        my $columns = $table->{'-columns'};
        foreach my $column ( @{$columns} ) {
            $select_clause .= <<"STMT";
    `$table_alias`.`$column`,
STMT
        }

        #    add to from clause
        $from_clause .= <<"STMT";
    `$table_name` $table_alias,
STMT

    } ## end foreach my $table ( @{$tables...})

    #    where
    my $where_clause = <<'STMT';
WHERE 1 = 1
STMT

    #   join columns
    my $join_columns = {};

BUILD_WHERE:
    foreach my $table ( @{$tables} ) {
        my $table_name = $table->{'-table_name'};
        if ( !defined $table_name ) {
            $self->error( { '-text' => 'Missing -table_name from config, cannot process table' } );
            next BUILD_SELECT;
        }

        #   table columns
        my $columns = $table_config->{$table_name}->{'-columns'};

        #   table alias
        my $table_alias = $table->{'-table_alias'};

        FIND_JOIN:
        foreach my $other_table ( @{$tables} ) {

            #   other table name
            my $other_table_name = $other_table->{'-table_name'};
            if ($other_table_name eq $table_name) {
                next FIND_JOIN;
            }

            #   other table alias
            my $other_table_alias = $other_table->{'-table_alias'};

            #   other table columns
            my $other_table_columns = $table_config->{$other_table_name}->{'-columns'};

            foreach my $column ( @{$columns} ) {
            
                #   column_name;
                my $column_name = $column->{'-name'};
                
                foreach my $other_table_column ( @{$other_table_columns} ) {
                
                    #   other column name
                    my $other_column_name = $other_table_column->{'-name'};

                    if ( $other_column_name eq $column_name && !exists $join_columns->{$column_name} ) {
                        $where_clause .= <<"STMT";
AND `$table_alias`.`$column_name` = `$other_table_alias`.`$column_name`
STMT
                        $join_columns->{$column_name} = 1;
                    }
                }
            }
        }
    } ## end foreach my $table ( @{$tables...})

    chomp $select_clause;
    chomp $from_clause;
    chop $select_clause;
    chop $from_clause;

    my $statement = <<"STMT";
$select_clause
$from_clause
$where_clause
STMT

    $self->debug( { '-text' => 'Have resulting -statement ' . $statement } );

    my $data = $self->SUPER::fetch_data( { '-statement' => $statement } );

    return $data;
} ## end sub fetch_data

1;
