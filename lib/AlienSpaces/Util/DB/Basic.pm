package AlienSpaces::Util::DB::Basic;

#
#   $Revision: 142 $
#   $Author: bwwallin $
#   $Date: 2010-06-15 21:44:39 +1000 (Tue, 15 Jun 2010) $
#
#   Note: This class is mostly redundant, you should really
#   be using sub-classes of DB::Record

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :reference);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_DB_MYSQL(),
    ];

    return $util_list;
}

#   fetch data
sub fetch_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-statement'], $args ) ) {
        return;
    }

    my $bind = $args->{'-bind'} || [];
    my $statement = $args->{'-statement'};

    #   cache
    my $cache = $args->{'-cache'};

    #   cache key
    my $cache_key = $args->{'-cache_key'};

    my $data;

    #   cache
    if ( $cache && !defined $cache_key ) {
        $cache_key = $self->get_cache_key( { '-cache_key_args' => [ $statement, @{$bind} ] } );
    }

    if ($cache) {

        #   data
        $data = $self->get_cached( { '-key' => $cache_key } );
        if ( defined $data ) {
            $self->debug( { '-text' => 'Returning cached for -key ' . $cache_key } );
            return $data;
        }
    }

    #   fetch
    my $names;

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        if ( defined $bind && scalar @{$bind} ) {
            $sth->execute( @{$bind} );
        }
        else {
            $sth->execute();
        }
        $names = $sth->{'NAME_lc'};
        $data  = $sth->fetchall_arrayref;

        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to fetch data -statement ' . $statement . ' :' . $EVAL_ERROR } );
        $self->stack( { '-text' => ' -binds ' . scalar @{$bind} . ' -bind_values ' . join( ' ', @{$bind} ) } );
        return;
    }

    #   cant handle fetching column names for this statement for some reason
    if ( !defined $names || !scalar @{$names} ) {
        $self->error( { '-text' => 'Unable to source names -statement ' . $args->{'-statement'} } );
        return;
    }

    #   not necessarily a bad thing if no data
    if ( !defined $data || !scalar @{$data} ) {
        $self->debug(
            {   '-text' => 'No data -statement ' .
                    $self->dbg_value( $args->{'-statement'} ) . ' -bind_values ' . $self->dbg_value($bind) } );
        return;
    }

    #   convert column names to hash keys
    my $key = [];

    push @{$key}, map { '-' . $_; } @{$names};

    my $dataset = [];
    my $max_col = scalar @{$names} - 1;
    foreach my $row ( @{$data} ) {
        my %row_h = map { $key->[$_] => $row->[$_] } ( 0 .. $max_col );
        push @{$dataset}, \%row_h;
    }

    if ($cache) {
        $self->trace( { '-text' => 'Storing cache for -key ' . $cache_key } );

        $self->set_cached(
            {   '-key'   => $cache_key,
                '-value' => $dataset,
                '-clear' => 0,
            } );
    }

    return $dataset;
} ## end sub fetch_data

#   update data
sub update_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-table_name', '-update_columns', '-where_columns', '-bind' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   table name
    my $table_name = $args->{'-table_name'};

    #   update columns
    my $update_columns = $args->{'-update_columns'};

    #   where columns
    my $where_columns = $args->{'-where_columns'};

    #   bind args
    my $bind_args = $args->{'-bind'};

    #   verify no where column names exist in update columns
    #   - the other alternative is to reimplement this method
    #     to accept $update_args and $where_args instead of just
    #     passing through $args but then its probably easier to
    #     write your ownn update statement and this method
    #     becomes less convenient..
    #
    #   - Cannot do:
    #       update `y`
    #       set    `x` = ?
    #       where  `x` = ?
    #
    my $update_columns_hash = {};
    foreach my $update_column ( @{$update_columns} ) {
        $update_columns_hash->{$update_column} = 1;
    }
    foreach my $where_column ( @{$where_columns} ) {
        if ( exists $update_columns_hash->{$where_column} ) {
            $self->error( { '-text' => 'Where -column ' . $where_column . ' exists in update column list, cannot update data' } );
            return;
        }
    }

    my $statement = <<"STMT";
UPDATE `$table_name`
SET
STMT

    #   bind values
    my @bind_values;

    #   update columns
    my $added_update = 0;
    foreach my $update_column ( @{$update_columns} ) {

        my $do_bind = 0;
        my $bind_value;

        #   try to find bind argument -xxxx
        if ( exists $bind_args->{ '-' . $update_column } ) {
            $bind_value = $bind_args->{ '-' . $update_column };
            $do_bind    = 1;
        }

        #   try to find bind argument xxxx
        elsif ( exists $bind_args->{$update_column} ) {
            $bind_value = $bind_args->{$update_column};
            $do_bind    = 1;
        }

        #   append bind to statement
        if ($do_bind) {
            $statement .= <<"STMT";
`$update_column` = ?,
STMT
            push @bind_values, $bind_value;
            $added_update = 1;
        }
    }

    #   get rid of trailing comma
    if ($added_update) {
        chomp $statement;
        chop $statement;
    }

    $statement .= <<'STMT';

WHERE 1 = 1
STMT

    #   where columns
    my $added_where = 0;
    foreach my $where_column ( @{$where_columns} ) {

        my $do_bind = 0;
        my $bind_value;

        #   try to find bind argument -xxxx
        if ( exists $bind_args->{ '-' . $where_column } ) {
            $bind_value = $bind_args->{ '-' . $where_column };
            $do_bind    = 1;
        }

        #   try to find bind argument xxxx
        elsif ( exists $bind_args->{$where_column} ) {
            $bind_value = $bind_args->{$where_column};
            $do_bind    = 1;
        }

        #   append bind to statement
        if ($do_bind) {
            $statement .= <<"STMT";
AND `$where_column` = ?
STMT
            push @bind_values, $bind_value;
            $added_where = 1;
        }
    }

    #   get rid of trailing comma
    if ($added_where) {
        chomp $statement;
    }

    $self->trace(
        { '-text' => 'Update -statement ' . $self->dbg_value($statement) . ' -bind_values ' . $self->dbg_value( \@bind_values ) } );

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        foreach my $bind_value (@bind_values) {
            $sth->bind_param( ++$bind, $bind_value );
        }

        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to update data :' . $EVAL_ERROR } );
        return;
    }

    return 1;
} ## end sub update_data

#   insert data
sub insert_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-table_name', '-insert_columns', '-bind' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   table name
    my $table_name = $args->{'-table_name'};

    #   insert columns
    my $insert_columns = $args->{'-insert_columns'};

    #   bind args
    my $bind_args = $args->{'-bind'};

    my $statement = <<"STMT";
INSERT INTO `$table_name` (
STMT

    #   bind values
    my @bind_values;

    #   insert columns
    my $added_insert = 0;
    foreach my $insert_column ( @{$insert_columns} ) {

        my $do_bind = 0;
        my $bind_value;

        #   try to find bind argument -xxxx
        if ( exists $bind_args->{ '-' . $insert_column } ) {
            $bind_value   = $bind_args->{ '-' . $insert_column };
            $do_bind      = 1;
            $added_insert = 1;
        }

        #   try to find bind argument xxxx
        elsif ( exists $bind_args->{$insert_column} ) {
            $bind_value   = $bind_args->{$insert_column};
            $do_bind      = 1;
            $added_insert = 1;
        }

        #   append bind to statement
        if ($do_bind) {
            $statement .= <<"STMT";
`$insert_column`,
STMT
            push @bind_values, $bind_value;
        }
    }
    if ($added_insert) {
        chomp $statement;
        chop $statement;
    }

    $statement .= <<'STMT';
) VALUES (
STMT

    foreach (@bind_values) {
        $statement .= <<'STMT';
?,
STMT
    }
    chomp $statement;
    chop $statement;

    $statement .= <<'STMT';
)
STMT

    $self->trace( { '-text' => 'Insert -statement ' . $statement . ' -bind_values ' . $self->dbg_value( \@bind_values ) } );

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);

        my $bind = 0;
        foreach my $bind_value (@bind_values) {
            $sth->bind_param( ++$bind, $bind_value );
        }

        $sth->execute();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to insert data :' . $EVAL_ERROR } );
        return;
    }

    #   last insert id
    my $last_insert_id = $self->dbh->last_insert_id();

    return $last_insert_id;
} ## end sub insert_data

1;

