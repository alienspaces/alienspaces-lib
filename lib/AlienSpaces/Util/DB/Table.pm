package AlienSpaces::Util::DB::Table;

#
#   $Revision: 142 $
#   $Author: bwwallin $
#   $Date: 2010-06-15 21:44:39 +1000 (Tue, 15 Jun 2010) $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use constant {
    TYPE_COLUMNS     => 1,
    TYPE_CONSTRAINTS => 2,
};

use AlienSpaces::Constants qw(:util :reference);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [

        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_DB_MYSQL(),
        UTIL_DB_BASIC(),
        UTIL_PHRASE(),
    ];

    return $util_list;
}

sub fetch_columns {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-table_name'], $args ) ) {
        return;
    }

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };
    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };

    #   table name
    my $table_name = $args->{'-table_name'};

    #   cache key
    my $cache_key = UTIL_DB_TABLE() . '-columns-' . $table_name;

    #   columns
    my $columns = $self->get_cached( { '-key' => $cache_key } );
    if ( defined $columns ) {
        return $columns;
    }

    #   database
    my $database = $db_mysql_util->database();

    #   bind
    my @bind = ( $database, $table_name );

    my $statement = <<'STMT';
SELECT `c`.`column_name`, `c`.`data_type`, `c`.`is_nullable`, 
       `c`.`character_maximum_length`, `c`.`numeric_precision`, 
       `c`.`extra`, `c`.`column_comment`
FROM   `information_schema`.`columns` c
WHERE  `c`.`table_schema` = ?
AND    `c`.`table_name` = ?
ORDER BY `c`.`ordinal_position` ASC
STMT

    #   data
    my $data = $db_basic_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );
    if ( !defined $data ) {
        $self->error(
            {   '-text' => 'Failed to fetch columns for -database ' .
                    $database . ' -table_name ' . $table_name . ' with -bind ' . $self->dbg_value( \@bind ) } );
        return;
    }

    #   set cached
    $self->set_cached( { '-key' => $cache_key, '-value' => $data, '-clear' => 0 } );

    return $data;
} ## end sub fetch_columns

sub fetch_constraints {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-table_name'], $args ) ) {
        return;
    }

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };
    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };

    #   table name
    my $table_name = $args->{'-table_name'};

    #   cache key
    my $cache_key = UTIL_DB_TABLE() . '-constraints-' . $table_name;

    #   columns
    my $columns = $self->get_cached( { '-key' => $cache_key } );
    if ( defined $columns ) {
        return $columns;
    }

    #   database
    my $database = $db_mysql_util->database();

    #   statement
    my $statement = <<'STMT';
SELECT `c`.`column_name`, `kc`.`referenced_table_name`,`kc`.`referenced_column_name`, `tc`.`constraint_type`
FROM   `information_schema`.`columns` c,
       `information_schema`.`key_column_usage` kc,
       `information_schema`.`table_constraints` tc
WHERE  `c`.`table_schema` = ?
AND    `c`.`table_name` = ?
AND    `kc`.`table_schema` = `c`.`table_schema`
AND    `kc`.`table_name` = `c`.`table_name`
AND    `kc`.`column_name` = `c`.`column_name`
AND    `tc`.`table_schema` = `c`.`table_schema`
AND    `tc`.`table_name` = `c`.`table_name`
AND    `tc`.`constraint_name` = `kc`.`constraint_name`
STMT

    #   bind
    my @bind = ( $database, $table_name );

    #   constraint type
    my $constraint_type = $args->{'-constraint_type'};
    if ( defined $constraint_type ) {
        $statement .= <<'STMT';
AND `tc`.`constraint_type` = ?
STMT
        push @bind, $constraint_type;
    }

    $statement .= <<'STMT';
ORDER BY `c`.`ordinal_position` ASC
STMT

    #   data
    my $data = $db_basic_util->fetch_data( { '-statement' => $statement, '-bind' => \@bind } );

    #   columns
    $columns = {};
    foreach my $constraint ( @{$data} ) {

        $columns->{ $constraint->{'-column_name'} } = {
            '-referenced_table_name'  => $constraint->{'-referenced_table_name'},
            '-referenced_column_name' => $constraint->{'-referenced_column_name'},
            '-constraint_type'        => $constraint->{'-constraint_type'},
        };
    }

    #   set cached
    $self->set_cached( { '-key' => $cache_key, '-value' => $columns, '-clear' => 0 } );

    return $columns;
} ## end sub fetch_constraints

sub fetch_primary_key_columns {
    my ( $self, $args ) = @_;

    #   table name
    my $table_name = $args->{'-table_name'};

    my $constraints = $self->fetch_constraints(
        {   '-table_name'      => $table_name,
            '-constraint_type' => 'PRIMARY KEY',
        } );

    return $constraints;
}

#
#   do what we can to try and generate a reasonable list of data from a table
#   that is reference by another table
#
sub fetch_reference_table_data {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-table_name'], $args ) ) {
        return;
    }

    #   phrase util
    my $phrase_util = $self->utils->{ UTIL_PHRASE() };

    #   table name
    my $table_name = $args->{'-table_name'};

    #   max rows
    my $max_rows = $args->{'-max_rows'};
    if ( !defined $max_rows ) {
        $max_rows = DEFAULT_MAX_REFERENCE_DATA_ROWS;
    }

    #   column
    my $columns = $self->fetch_columns( { '-table_name' => $table_name } );

    #   display / active column
    my ( $display_column, $active_column );

    $self->trace( { '-text' => 'Have -columns ' . $self->dbg_value($columns) } );

    my $valid_display_columns = {
        'phrase_id' => 1,
        'name'      => 1,
        'text'      => 1,
    };

    my $valid_active_columns = {
        'active'  => 1,
        'enabled' => 1,
    };

    foreach my $column ( @{$columns} ) {

        #   display column
        if ( exists $valid_display_columns->{ $column->{'-column_name'} } ) {
            $self->trace(
                { '-text' => 'Table -table_name ' . $table_name . ' has display -column_name ' . $column->{'-column_name'} } );
            $display_column = $column->{'-column_name'};
        }

        #   display column
        if ( exists $valid_active_columns->{ $column->{'-column_name'} } ) {
            $self->trace(
                { '-text' => 'Table -table_name ' . $table_name . ' has active -column_name ' . $column->{'-column_name'} } );
            $active_column = $column->{'-column_name'};
        }
    }

    if ( !defined $display_column ) {
        $self->error(
            { '-text' => 'Couldnt find a -phrase_id, -name or -text column to use as a reference list, not returning data' } );
        return;
    }

    #   find primary key column
    my $table_constraints = $self->fetch_constraints( { '-table_name' => $table_name } );
    if ( !defined $table_constraints ) {
        $self->error( { '-text' => 'Failed to fetch -table_name ' . $table_name . ' contraints, cannot generate reference list' } );
        return;
    }

    my $primary_key_column;
FIND_PRIMARY_KEY:
    foreach my $column_name ( keys %{$table_constraints} ) {
        if ( $table_constraints->{$column_name}->{'-constraint_type'} eq 'PRIMARY KEY' ) {
            $primary_key_column = $column_name;
            last FIND_PRIMARY_KEY;
        }
    }

    if ( !defined $primary_key_column ) {
        $self->error(
            { '-text' => 'Could not find primary key column in -table_name ' . $table_name . ', cannot generate reference list' } );
        return;
    }

    $self->trace( { '-text' => 'Table -table_name ' . $table_name . ' has -primary_key_column ' . $primary_key_column } );

    #   check count against max rows
    my $statement = <<"STMT";
SELECT COUNT(`$primary_key_column`) FROM `$table_name`
STMT

    if ( defined $active_column ) {
        $statement .= <<"STMT";
WHERE `$active_column` = 1
STMT
    }

    my $count;
    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute();
        ($count) = $sth->fetchrow_array();
        $sth->finish();
        1;
    };

    if ( !$success ) {
        $self->error(
            { '-text' => '-text' => 'Failed to fetch record count for -table_name ' . $table_name . ': ' . $EVAL_ERROR } );
        return;
    }

    if ( $count > $max_rows ) {
        $self->error(
            { '-text' => 'Row -count ' . $count . ' greater than -max_rows ' . $max_rows . ', cannnot generate reference list' } );
        return;
    }

    #   fetch the data
    $statement = <<"STMT";
SELECT 
    `$primary_key_column`, 
    `$display_column` 
FROM 
    `$table_name` 
STMT

    if ( defined $active_column ) {
        $statement .= <<"STMT";
WHERE `$active_column` = 1
STMT
    }

    $statement .= <<'STMT';
ORDER BY 1
STMT

    my $data;
    $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute();
        $data = $sth->fetchall_arrayref();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to fetch reference data from -table_name ' . $table_name . ' :' . $EVAL_ERROR } );
        return;
    }

    if ( !defined $data || !scalar @{$data} ) {
        $self->trace( { '-text' => 'No data returned from reference -table_name ' . $table_name } );
        return;
    }

    my $values = [];
    my $labels = {};

    foreach my $row ( @{$data} ) {
        my ( $value, $display_value ) = @{$row};

        #   values
        push @{$values}, $value;

        #   labels
        $labels->{$value} =
            ( $display_column eq 'phrase_id' ? $phrase_util->text( { '-phrase_id' => $display_value } ) : $display_value );
    }

    $self->trace( { '-text' => 'Returning -values ' . $self->dbg_value($values) . ' -labels ' . $self->dbg_value($labels) } );

    return ( $values, $labels );
} ## end sub fetch_reference_table_data

1;

