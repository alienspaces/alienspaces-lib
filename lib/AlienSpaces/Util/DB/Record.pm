package AlienSpaces::Util::DB::Record;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(:util :db-record);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use constant { DEFAULT_PAGE_SIZE => 10, };

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        
        #   super
        @{ $self->SUPER::util_list },

        #   local
        UTIL_DB_MYSQL(),
        UTIL_DB_BASIC(),
        UTIL_DB_TABLE(),
        UTIL_DB_VIEW(),
    ];

    #   children
    my $children = $self->_children();

    foreach my $child ( @{$children} ) {
        push @{$util_list}, $child->{'-class'};
    }

    return $util_list;
}

##############################
#
#   config
#
##############################

#   config  (optional)
sub config {
    my ( $self, $args ) = @_;

    my $config = {

        #   The name of the table data is sourced from
        '-table_name' => 'xxxx',

        #   OR the name of a view
        '-view_name' => 'xxxx',

        #   Primary key columns
        #   - Only required when based on a view
        '-primary_key_columns' => [ 'xxxx', 'xxxx' ],

        #   Key to use when returning records as children
        '-key' => 'xxxx',

        #   If the data is never modified caching query results
        #   will improve request times for records that are
        #   queried multiple time
        '-cache' => 1 | 0,

        #   Is there related child data you would also like to
        #   fetch, insert or update by default
        '-children' => [
            {   '-class'        => 'Util::DB::Record::XXXX',
                '-join_columns' => [ 'xxxxxx', ] }
        ],

        #   Order the data
        '-order_by' => 'xxxx ASC|DESC',

        #   Page size
        #   - Only used when calling the fetch method with a -page_number argument
        '-page_size' => 'xxxx',
    };

    return $config;
} ## end sub config

##############################
#
#   public methods
#
##############################

#   fetch
#   - expected to return an arrayref of 1 or more records
sub fetch {
    my ( $self, $args ) = @_;

    #   children
    my $children = $args->{'-children'};
    if ( !defined $children ) {
        $children = 1;
    }

    #   case insensitive
    my $case_insensitive = delete $args->{'-case_insensitive'};

    #   table name
    my $table_name = $self->_table_name($args);

    #   view name
    my $view_name = $self->_view_name($args);
    
    if (!defined $table_name && !defined $view_name) {
        $self->stack({'-text' => 'No table name or view name'});
        return;
    }
        
    #   primary key columns
    my $primary_key_columns = $self->_primary_key_columns($args);

    #   columns
    my $columns = $self->_columns($args);

    #   cache
    my $cache = $self->_cache();

    #   data
    my $data;

    #   cache key
    my $cache_key;

    my @ck;

VERIFY_PRIMARY_KEY_ARGUMENTS:
    foreach my $primary_key_column ( keys %{$primary_key_columns} ) {
        my $arg_key = '-' . lc($primary_key_column);

        $self->trace( { '-text' => 'Testing -args ' . $self->dbg_value($args) . ' -primary_key ' . $self->dbg_value($arg_key) } );

        if ( !defined $args->{$arg_key} ) {
            $self->trace( { '-text' => 'Missing primary key argument, not caching' } );
            last VERIFY_PRIMARY_KEY_ARGUMENTS;
        }
        push @ck, ( defined $table_name ? $table_name : $view_name ), $primary_key_column, $args->{$arg_key};
    }

    if ( $cache && @ck ) {
        $cache_key = join '-', @ck;
        $data = $self->get_cached( { '-key' => $cache_key } );
    }

    if ( !defined $data ) {

        #   page number
        my $page_number = delete $args->{'-page_number'};
        if ( defined $page_number && $page_number <= 0 ) {
            $page_number = 1;
        }

        #   page size
        my $page_size = delete $args->{'-page_size'};
        if ( defined $page_number ) {
            if ( !defined $page_size ) {
                $page_size = $self->_page_size();
            }
        }

        #   statement
        my $statement;

        if ( defined $table_name ) {
            $statement = <<"STMT";
SELECT *  FROM `$table_name` t WHERE 1 = 1
STMT
        }
        elsif ( defined $view_name ) {
            $statement = <<"STMT";
SELECT *  FROM `$view_name` t WHERE 1 = 1
STMT
        }

        #   bind
        my $bind = [];

        ( $statement, $bind ) = $self->_build_fetch(
            {   '-statement'           => $statement,
                '-primary_key_columns' => $primary_key_columns,
                '-columns'             => $columns,
                '-fetch_args'          => $args,
                '-case_insensitive'    => $case_insensitive,
            } );

        #   order by
        my $order_by = $self->_order_by();
        if ( defined $order_by ) {
            $statement .= <<"STMT";
    ORDER BY $order_by
STMT
        }

        #   page
        if ( defined $page_number && defined $page_size ) {
            $statement .= <<"STMT";
    LIMIT ?, ?
STMT
            push @{$bind}, ( ( $page_number - 1 ) * $page_size, $page_size );
        }

        $self->trace( { '-text' => 'Fetching with -statement ' . $self->dbg_value($statement) . ' -bind ' . $self->dbg_value($bind) } );

        #   fetch
        $data = $self->fetch_data( { '-statement' => $statement, '-bind' => $bind, '-cache' => $cache, '-cache_key' => $cache_key } );

        #   no data
        if ( !defined $data ) {
            $self->trace( { '-text' => 'No records found for -args ' . $self->dbg_value($args) } );
            return;
        }
    } ## end if ( !defined $data )

    #   children
    if ($children) {
        if ( defined $data && scalar @{$data} ) {
            foreach my $data_rec ( @{$data} ) {

                #   fetch children
                $self->fetch_children( { '-data' => $data_rec } );
            }
        }
    }

    return $data;
} ## end sub fetch

#   fetch one
#   - expected to return a single record as a hashref
sub fetch_one {
    my ( $self, $args ) = @_;

    #   data
    my $data = $self->fetch($args);

    #   when arrayref
    if ( defined $data && ref($data) eq 'ARRAY' ) {
        my $count = scalar @{$data};
        if ( $count == 0 ) {
            $self->error( { '-text' => 'Fetch one expecting 1 record, fetched -count ' . $self->dbg_value($count) } );
            undef $data;
        }
        if ( $count == 1 ) {
            $data = $data->[0];
        }
        if ( $count > 1 ) {
            $self->error( { '-text' => 'Fetch one expecting 1 record, fetched -count ' . $self->dbg_value($count) } );
            undef $data;
        }
    }

    #   when not hashref
    if ( defined $data && ref($data) ne 'HASH' ) {
        $self->error( { '-text' => 'Fetch one expecting an hashref, got -data ' . $self->dbg_value($data) } );
        undef $data;
    }

    return $data;
} ## end sub fetch_one

#   fetch count
sub fetch_count {
    my ( $self, $args ) = @_;

    #   case insensitive
    my $case_insensitive = delete $args->{'-case_insensitive'};

    #   table name
    my $table_name = $self->_table_name($args);

    #   view name
    my $view_name = $self->_view_name($args);

    #   primary key columns
    my $primary_key_columns = $self->_primary_key_columns($args);

    #   columns
    my $columns = $self->_columns($args);

    #   statement
    my $statement;
    if ( defined $table_name ) {
        $statement = <<"STMT";
SELECT count(*) AS count FROM `$table_name` t WHERE 1 = 1
STMT
    }
    elsif ( defined $view_name ) {
        $statement = <<"STMT";
SELECT count(*) AS count FROM `$view_name` t WHERE 1 = 1
STMT
    }

    my $bind = [];

    ( $statement, $bind ) = $self->_build_fetch(
        {   '-statement'           => $statement,
            '-primary_key_columns' => $primary_key_columns,
            '-columns'             => $columns,
            '-fetch_args'          => $args,
            '-case_insensitive'    => $case_insensitive,
        } );

    $self->trace( { '-text' => 'Fetching with -statement ' . $self->dbg_value($statement) . ' -bind ' . $self->dbg_value($bind) } );

    #   fetch
    my $data = $self->fetch_data( { '-statement' => $statement, '-bind' => $bind } );

    #   count
    my $count = 0;

    #   no data
    if ( !defined $data ) {
        $self->error( { '-text' => 'No records found for -args ' . $self->dbg_value($args) } );
        return $count;
    }

    #   count
    if ( defined $data && scalar @{$data} == 1 ) {
        $count = $data->[0]->{'-count'};
    }

    $self->trace( { '-text' => 'Returning -count ' . $self->dbg_value($count) } );

    return $count;
} ## end sub fetch_count

#   insert
sub insert {
    my ( $self, $args ) = @_;

    #   children
    my $children = $args->{'-children'};
    if ( !defined $children ) {
        $children = 1;
    }

    #   table name
    my $table_name = $self->_table_name($args);
    if ( !defined $table_name ) {
        $self->error( { '-text' => 'No -table_name defined in config, cannot insert records' } );
        return;
    }

    #   primary key columns
    my $primary_key_columns = $self->_primary_key_columns($args);

    #   columns
    my $columns = $self->_columns($args);

    #   statement
    my $statement = <<"STMT";
INSERT INTO `$table_name` (
STMT

    my @bind = ();

ADD_COLUMNS:
    foreach my $column ( @{$columns} ) {

        #   column name
        my $column_name = $column->{'-column_name'};

        #   arg key
        my $arg_key = '-' . lc($column_name);

        #   primary key
        if ( exists $primary_key_columns->{$column_name} ) {
            $statement .= <<"STMT";
    `$column_name`,
STMT
            push @bind, $args->{$arg_key};
            next ADD_COLUMNS;
        }

        #   other columns
        if ( exists $args->{$arg_key} ) {
            $statement .= <<"STMT";
    `$column_name`,
STMT
            push @bind, $args->{$arg_key};
            next ADD_COLUMNS;
        }
    }

    chomp($statement);
    chop($statement);

    if ( scalar @bind ) {
        $statement .= <<'STMT';
) VALUES (
STMT

        $statement .= ' ?,' x scalar(@bind);

        chop($statement);

        $statement .= ' )';
    }

    $self->trace( { '-text' => 'Insert -statement ' . $self->dbg_value($statement) . ' -bind ' . $self->dbg_value( \@bind ) } );

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute(@bind);
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to insert data with -args ' . $self->dbg_value($args) . ' :' . $EVAL_ERROR . ' -statement ' . $self->dbg_value($statement) } );
        return;
    }

    #   pk
    my $pk = $self->dbh->last_insert_id();

    #   add primary key
    my $keys_added = [];
    foreach my $primary_key_column ( keys %{$primary_key_columns} ) {
        my $arg_key = '-' . lc($primary_key_column);
        if ( !exists $args->{$arg_key} ) {
            $args->{$arg_key} = $pk;
            push @{$keys_added}, $arg_key;
        }
    }

    #   children
    if ($children) {

        $self->trace( { '-text' => 'Inserting children -args ' . $self->dbg_value($args) } );

        #   insert children
        $self->insert_children($args);
    }

    return $pk;
} ## end sub insert

#   update
sub update {
    my ( $self, $args ) = @_;

    #   children
    my $children = $args->{'-children'};
    if ( !defined $children ) {
        $children = 1;
    }

    #   table name
    my $table_name = $self->_table_name($args);
    if ( !defined $table_name ) {
        $self->error( { '-text' => 'No -table_name defined in config, cannot insert records' } );
        return;
    }

    #   primary key columns
    my $primary_key_columns = $self->_primary_key_columns($args);

    #   columns
    my $columns = $self->_columns($args);

    #   statement
    my $statement = <<"STMT";
UPDATE `$table_name` SET
STMT

    #   bind
    my @bind = ();

    #   update statement
ADD_COLUMNS:
    foreach my $column ( @{$columns} ) {

        #   column name
        my $column_name = $column->{'-column_name'};

        #   arg key
        my $arg_key = '-' . lc($column_name);

        #   primary key
        if ( exists $primary_key_columns->{$column_name} ) {
            next ADD_COLUMNS;
        }

        #   update columns
        if ( exists $args->{$arg_key} ) {
            $statement .= <<"STMT";
`$column_name` = ?,
STMT
            push @bind, $args->{$arg_key};
            next ADD_COLUMNS;
        }

    }

    if ( scalar @bind ) {

        chomp($statement);
        chop($statement);

        #   where clause
        $statement .= <<'STMT';

WHERE 1 = 1
STMT

        foreach my $column ( @{$columns} ) {

            #   column name
            my $column_name = $column->{'-column_name'};

            #   arg key
            my $arg_key = '-' . lc($column_name);

            #   primary key
            if ( exists $primary_key_columns->{$column_name} ) {

                if ( !defined $args->{$arg_key} ) {
                    $self->error( { '-text' => 'Missing primary key argument -column_name ' . $column_name . ', cannot update record' } );
                    return;
                }

                $statement .= <<"STMT";
AND `$column_name` = ?
STMT
                push @bind, $args->{$arg_key};
            }
        }

        $self->trace( { '-text' => 'Update -statement ' . $self->dbg_value($statement) . ' -bind ' . $self->dbg_value( \@bind ) } );

        my $success = eval {
            my $sth = $self->dbh->prepare($statement);
            $sth->execute(@bind);
            $sth->finish();
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Failed to update data with -args ' . $self->dbg_value($args) . ' :' . $EVAL_ERROR . ' -statement ' . $self->dbg_value($statement) } );
            return;
        }
    } ## end if ( scalar @bind )

    #   children
    if ($children) {

        #   update children
        $self->update_children($args);
    }

    return 1;
} ## end sub update

#   delete
sub delete {
    my ( $self, $args ) = @_;

    #   table name
    my $table_name = $self->_table_name($args);
    if ( !defined $table_name ) {
        $self->error( { '-text' => 'No -table_name defined in config, cannot insert records' } );
        return;
    }

    #   primary key columns
    my $primary_key_columns = $self->_primary_key_columns($args);

    my $statement = <<"STMT";
DELETE FROM `$table_name`
WHERE 1 = 1
STMT

    my @bind = ();
    foreach my $primary_key_column ( keys %{$primary_key_columns} ) {
        my $arg_key = '-' . lc($primary_key_column);
        if ( !defined $args->{$arg_key} ) {
            $self->error( { '-text' => 'Missing primary key column -args ' . $self->dbg_value($args) . ', cannot delete record' } );
            return;
        }

        $statement .= <<"STMT";
AND `$primary_key_column` = ?
STMT
        push @bind, $args->{$arg_key};
    }

    my $success = eval {
        my $sth = $self->dbh->prepare($statement);
        $sth->execute(@bind);
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Failed to delete record -args ' . $self->dbg_value($args) . ' :' . $EVAL_ERROR . ' -statement ' . $self->dbg_value($statement) } );
        return;
    }

    return 1;
} ## end sub delete

##############################
#
#   private methods
#
##############################

#   _build_fetch
sub _build_fetch {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-statement', '-columns' ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   statement
    my $statement = $args->{'-statement'};

    #   primary key columns
    my $primary_key_columns = $args->{'-primary_key_columns'};

    #   columns
    my $columns = $args->{'-columns'};

    #   fetch args
    my $fetch_args = $args->{'-fetch_args'};
    if ( !defined $fetch_args ) {
        $fetch_args = {};
    }

    #   case insensitive
    my $case_insensitive = $args->{'-case_insensitive'};

    #   where
    #   - special additional where clause
    my $where = $fetch_args->{'-where'};

    my ( $where_clause, $where_bind );
    if ( defined $where ) {
        $where_clause = $where->{'-clause'};
        $where_bind   = $where->{'-bind'};
    }

    #   bind
    my @bind = ();

PROCESS_COLUMNS:
    foreach my $column ( @{$columns} ) {

        #   column name
        my $column_name = $column->{'-column_name'};

        my $arg_key = '-' . lc($column_name);
        if ( exists $fetch_args->{$arg_key} ) {

            my ( $value, $operator );

            #                warn 'Have - ' . $self->dbg_value($fetch_args->{$arg_key});

            #   fetch arg contains more than just a value
            if ( ref( $fetch_args->{$arg_key} ) eq 'HASH' ) {

                $value    = $fetch_args->{$arg_key}->{'-value'};
                $operator = $fetch_args->{$arg_key}->{'-operator'};

            }
            elsif ( ref( $fetch_args->{$arg_key} ) eq 'ARRAY' ) {

                $value    = $fetch_args->{$arg_key};
                $operator = SQL_OPERATOR_IN();

            }
            else {

                $value = $fetch_args->{$arg_key};

                if ($case_insensitive) {
                    $operator = SQL_OPERATOR_LIKE();
                }
                else {
                    $operator = SQL_OPERATOR_EQUALS();
                }
            }

            #   operator in  / operator not in
            if ( $operator eq SQL_OPERATOR_IN() || $operator eq SQL_OPERATOR_NOT_IN() ) {

                #   check value
                if ( !defined $value || !scalar @{$value} ) {
                    next PROCESS_COLUMNS;
                }

                if ( $column->{'-data_type'} eq 'varchar' && $case_insensitive ) {
                    $statement .= <<"STMT";
AND LOWER(`t`.`$column_name`) $operator (
STMT
                }
                else {
                    $statement .= <<"STMT";
AND `t`.`$column_name` $operator (
STMT
                }

                if ( $column->{'-data_type'} eq 'varchar' && $case_insensitive ) {
                    $statement .= join ', ', split //sxm, ( 'CONCAT(\'%\', LOWER(?), \'%\')' x scalar @{$value} );
                }
                else {
                    $statement .= join ', ', split //sxm, ( '?' x scalar @{$value} );
                }

                $statement .= <<"STMT";
)
STMT

                push @bind, @{$value};

            } ## end if ( $operator eq SQL_OPERATOR_IN...)

            #   all other operators
            else {

                #   check value
                if ( !defined $value ) {
                    next PROCESS_COLUMNS;
                }

                if ( $column->{'-data_type'} eq 'varchar' && $case_insensitive ) {
                    $statement .= <<"STMT";
AND LOWER(`t`.`$column_name`) $operator CONCAT('%', LOWER(?), '%')
STMT
                }
                else {
                    $statement .= <<"STMT";
AND `t`.`$column_name` $operator ?
STMT
                }
                push @bind, $value;
            }
        } ## end if ( exists $fetch_args...)
    } ## end PROCESS_COLUMNS: foreach my $column ( @{$columns...})

    #   where
    if ( defined $where_clause ) {

        $statement .= <<"STMT";
AND $where_clause
STMT

        if ( defined $where_bind && scalar @{$where_bind} ) {
            push @bind, @{$where_bind};
        }
    }

    $self->trace( { '-text' => 'Resulting -statement ' . $self->dbg_value($statement) . ' -bind ' . $self->dbg_value( \@bind ) } );

    return ( $statement, \@bind );
} ## end sub _build_fetch

#   fetch children
sub fetch_children {
    my ( $self, $args ) = @_;

    #   children
    my $children = $self->_children();
    if ( !defined $children || ref( $children ne 'ARRAY' ) || !scalar @{$children} ) {
        return;
    }

    #   data
    my $data = $args->{'-data'};
    if ( !defined $data ) {
        $data = {};
    }

    #   primary key columns
    my $primary_key_columns = $self->_primary_key_columns($args);

    #   child args
    my $child_args = {};

    foreach my $primary_key_column ( keys %{$primary_key_columns} ) {
        my $arg_key = '-' . lc($primary_key_column);
        if ( !defined $data->{$arg_key} ) {
            $self->error( { '-text' => 'Missing primary key in data, cannot fetch children' } );
            return;
        }

        $child_args->{$arg_key} = $data->{$arg_key};
    }

FETCH_CHILD_DATA:
    foreach my $child ( @{$children} ) {

        #   pass join columns
        my $join_columns = $child->{'-join_columns'};
        if ( defined $join_columns && scalar @{$join_columns} ) {
            foreach my $join_column ( @{$join_columns} ) {
                if ( $join_column !~ /^\-/sxmg ) {
                    $join_column = '-' . $join_column;
                }
                if ( defined $data->{$join_column} ) {
                    $child_args->{$join_column} = $data->{$join_column};
                }
            }
        }

        #   child util
        my $child_util = $self->utils->{ $child->{'-class'} };
        if ( !defined $child_util ) {
            $self->error( { '-text' => 'Failed to load child util -class ' . $child->{'-class'} . ', cannot fetch children' } );
            next FETCH_CHILD_DATA;
        }

        $self->debug( { '-text' => 'Fetching child data -child_class ' . $self->dbg_value($child->{'-class'}) } );

        #   child key
        my $key = $child_util->_key();
        if ( !defined $key ) {
            $self->error( { '-text' => 'Child util -class ' . $child->{'-class'} . ' missing child key, cannot fetch children' } );
            next FETCH_CHILD_DATA;
        }

        $self->trace( { '-text' => 'Fetching children with -child_args ' . $self->dbg_value($child_args) } );

        #   child data
        $data->{$key} = $child_util->fetch($child_args);

        $self->trace( { '-text' => 'Now have -data ' . $self->dbg_value($args) } );

    } ## end FETCH_CHILD_DATA: foreach my $child ( @{$children...})

    return;
} ## end sub fetch_children

#   insert children
sub insert_children {
    my ( $self, $args ) = @_;

    #   children
    my $children = $self->_children();
    if ( !defined $children || ref( $children ne 'ARRAY' ) || !scalar @{$children} ) {
        $self->debug( { '-text' => 'No children defined in configuration' } );
        return;
    }

    #   primary key columns
    my $primary_key_columns = $self->_primary_key_columns($args);

    #   common child args
    my $common_child_args = {};
    foreach my $primary_key_column ( keys %{$primary_key_columns} ) {
        my $arg_key = '-' . lc($primary_key_column);
        if ( !defined $args->{$arg_key} ) {
            $self->error( { '-text' => 'Missing primary key in data, cannot insert children' } );
            return;
        }

        $common_child_args->{$arg_key} = $args->{$arg_key};
    }

INSERT_CHILD_DATA:
    foreach my $child ( @{$children} ) {

        $self->debug( { '-text' => 'Processing configuration -child ' . $self->dbg_value($child) } );

        #   child util
        my $child_util = $self->utils->{ $child->{'-class'} };
        if ( !defined $child_util ) {
            $self->error( { '-text' => 'Failed to load child util -class ' . $child->{'-class'} . ', cannot fetch children' } );
            next INSERT_CHILD_DATA;
        }

        #   child key
        my $key = $child_util->_key();
        if ( !defined $key ) {
            $self->error( { '-text' => 'Child util -class ' . $child->{'-class'} . ' missing child key, cannot fetch children' } );
            next INSERT_CHILD_DATA;
        }

        #   child data
        my $child_data = $args->{$key};
        if ( !defined $child_data || !scalar @{$child_data} ) {
            $self->debug( { '-text' => 'Missing -key ' . $self->dbg_value($key) . ' from -args ' . $self->dbg_value($args) . ', cannot insert children' } );
            next INSERT_CHILD_DATA;
        }

    INSERT_CHILDREN:
        foreach my $c_data ( @{$child_data} ) {

            $self->trace( { '-text' => 'Processing child -c_data ' . $self->dbg_value($c_data) } );

            if ( ref($c_data) eq 'HASH' ) {

                #   child args
                my $child_args = {};
                %{$child_args} = %{$common_child_args};

                while ( my ( $k, $v ) = each %{$c_data} ) {
                    $child_args->{$k} = $v;
                }

                $self->trace( { '-text' => 'Insert -child_args ' . $self->dbg_value($child_args) } );

                my $child_rec = $child_util->insert($child_args);
                if ( !defined $child_rec ) {
                    $self->error( { '-text' => 'Failed to insert child record' } );
                    next INSERT_CHILDREN;
                }
            }
        }
    } ## end INSERT_CHILD_DATA: foreach my $child ( @{$children...})

    return 1;
} ## end sub insert_children

#   update children
sub update_children {
    my ( $self, $args ) = @_;

    #   children
    my $children = $self->_children();
    if ( !defined $children || ref( $children ne 'ARRAY' ) || !scalar @{$children} ) {
        return;
    }

UPDATE_CHILD_DATA:
    foreach my $child ( @{$children} ) {

        #   child util
        my $child_util = $self->utils->{ $child->{'-class'} };
        if ( !defined $child_util ) {
            $self->error( { '-text' => 'Failed to load child util -class ' . $child->{'-class'} . ', cannot update children' } );
            next UPDATE_CHILD_DATA;
        }

        #   child key
        my $key = $child_util->_key();
        if ( !defined $key ) {
            $self->error( { '-text' => 'Child util -class ' . $child->{'-class'} . ' missing child key, cannot update children' } );
            next UPDATE_CHILD_DATA;
        }

        #   child data
        my $child_data = $args->{$key};
        if ( defined $child_data && scalar @{$child_data} ) {

        UPDATE_CHILDREN:
            foreach my $c_data ( @{$child_data} ) {
                if ( !$child_util->update($c_data) ) {
                    $self->error( { '-text' => 'Failed to update child record' } );
                    next UPDATE_CHILDREN;
                }
            }
        }
    }

    return 1;
} ## end sub update_children

#   _table_name
sub _table_name {
    my ( $self, $args ) = @_;

    #   table name
    my $table_name = delete $args->{'-table_name'};

    if ( !defined $table_name ) {
        my $config = $self->config;
        if ( defined $config ) {
            $table_name = $config->{'-table_name'};
        }
    }

    return $table_name;
}

#   _view_name
sub _view_name {
    my ( $self, $args ) = @_;

    #   table name
    my $view_name = delete $args->{'-view_name'};

    if ( !defined $view_name ) {
        my $config = $self->config;
        if ( defined $config ) {
            $view_name = $config->{'-view_name'};
        }
    }

    return $view_name;
}

#   _key
sub _key {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->config;

    #   child key
    my $key;
    if ( defined $config ) {
        $key = $config->{'-key'};
        if ( $key !~ /^\-/sxmg ) {
            $key = '-' . $key;
        }
    }

    return $key;
}

#   _children
sub _children {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->config;

    #   children
    my $children;
    if ( defined $config ) {
        $children = $config->{'-children'};
    }

    return $children;
}

#   _order_by
sub _order_by {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->config;

    #   order_by
    my $order_by;
    if ( defined $config ) {
        $order_by = $config->{'-order_by'};
    }

    return $order_by;
}

#   _page_size
sub _page_size {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->config;

    #   page_size
    my $page_size;
    if ( defined $config ) {
        $page_size = $config->{'-page_size'};
        if ( !defined $page_size ) {
            $page_size = DEFAULT_PAGE_SIZE;
        }
    }

    return $page_size;
}

#   _cache
sub _cache {
    my ( $self, $args ) = @_;

    #   config
    my $config = $self->config;

    #   cache
    my $cache;
    if ( defined $config ) {
        $cache = $config->{'-cache'};
    }

    if ( !defined $cache ) {
        $cache = 0;
    }

    return $cache;
}

#   _primary_key_columns
sub _primary_key_columns {
    my ( $self, $args ) = @_;

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   db view util
    my $db_view_util = $self->utils->{ UTIL_DB_VIEW() };

    #   table name
    my $table_name = $args->{'-table_name'};

    #   view name
    my $view_name = $args->{'-view_name'};

    if ( !defined $table_name && !defined $view_name ) {
        $table_name = $self->_table_name();
        $view_name  = $self->_view_name();
    }

    #   primary key columns
    my $primary_key_columns = $self->{'_primary_key_columns'};
    if ( !defined $primary_key_columns ) {

        #   config
        my $config = $self->config;

        if ( defined $config ) {
            $primary_key_columns = $config->{'-primary_key_columns'};
            if ( ref($primary_key_columns) eq 'ARRAY' ) {
                my @temp_primary_key_columns = @{$primary_key_columns};
                $primary_key_columns = {};
                %{$primary_key_columns} = map { $_ => { '-constraint_type' => 'PRIMARY KEY' }; } @temp_primary_key_columns;
            }
        }

        if ( !defined $primary_key_columns ) {

            #   table
            if ( defined $table_name ) {
                $primary_key_columns = $db_table_util->fetch_primary_key_columns( { '-table_name' => $table_name } );
            }

            #   view
            elsif ( defined $view_name ) {
                $primary_key_columns = $db_view_util->fetch_primary_key_columns( { '-view_name' => $view_name } );
            }
        }
        $self->{'_primary_key_columns'} = $primary_key_columns;
    }

    return $primary_key_columns;
} ## end sub _primary_key_columns

#   _columns
sub _columns {
    my ( $self, $args ) = @_;

    #   db table util
    my $db_table_util = $self->utils->{ UTIL_DB_TABLE() };

    #   db view util
    my $db_view_util = $self->utils->{ UTIL_DB_VIEW() };

    #   table name
    my $table_name = $args->{'-table_name'};

    #   view name
    my $view_name = $args->{'-view_name'};

    my $config;
    if ( !defined $table_name && !defined $view_name ) {
        $table_name = $self->_table_name();
        $view_name  = $self->_view_name();
    }

    #   columns
    my $columns = $self->{'_columns'};
    if ( !defined $columns ) {

        #   table
        if ( defined $table_name ) {
            $columns = $db_table_util->fetch_columns( { '-table_name' => $table_name } );
        }

        #   view
        elsif ( defined $view_name ) {
            $columns = $db_view_util->fetch_columns( { '-view_name' => $view_name } );
        }
        $self->{'_columns'} = $columns;
    }

    $self->trace({'-text' => 'Returning -columns ' . $self->dbg_value($columns)});
    
    return $columns;
} ## end sub _columns

1;
