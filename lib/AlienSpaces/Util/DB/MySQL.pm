package AlienSpaces::Util::DB::MySQL;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use warnings;
use strict;
use Carp qw(croak carp);
use English qw(-no_match_vars);

use AlienSpaces::Constants qw();

use constant { MAX_CONNECT_ATTEMPTS => 3, };

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use AlienSpaces::Properties {

    #   database handle
    'dbh'      => { '-type' => 's' },
    'database' => { '-type' => 's' },
};

use DBI;

sub init {
    my ( $self, $args ) = @_;

    #   SUPER init
    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Failed SUPER init' } );
        return;
    }

    $self->debug( { '-text' => 'Init' } );

    return 1;
}

#   init connection
sub init_connection {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( [ '-username', '-password', '-database', ], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   username
    my $username = $args->{'-username'};

    #   password
    my $password = $args->{'-password'};

    #   database
    my $database = $args->{'-database'};

    my $connect_attempts = $args->{'-connect_attempts'};
    $connect_attempts = 0;

    if ( $self->dbh_connected ) {
        $self->info( { '-text' => 'Already connected' } );
        return 1;
    }

    my $dbh;
    if ( ( defined $database ) &&
        ( defined $username ) &&
        ( defined $password ) ) {

        $self->info( { '-text' => 'Connecting to ' . $database } );

        my $success = eval {
            $dbh = DBI->connect( 'DBI:mysql:database=' . $database . ';host=localhost',
                $username, $password, { 'RaiseError' => 1, 'AutoCommit' => 0 } );
            1;
        };
        if ( !$success ) {
            $self->error( { '-text' => 'Connection failed :' . $EVAL_ERROR } );
            return;
        }

        if ( !defined $dbh ) {
            $self->error( { '-text' => 'Connection failed, no dbh' } );
            return;
        }

        $self->info( { '-text' => 'Connected' } );

        $dbh->{mysql_auto_reconnect} = 1;

        #   dbh
        $self->dbh($dbh);

        #   database
        $self->database($database);

        if ( !$self->dbh_connected ) {
            if ( ++$connect_attempts > MAX_CONNECT_ATTEMPTS ) {
                $self->fatal( { '-text' => 'Surpassed -max_connect_attempts ' . $connect_attempts } );
            }
            return $self->init_connection(
                {   '-connect_attempts' => $connect_attempts,
                    '-username'         => $username,
                    '-password'         => $password,
                    '-database'         => $database,
                } );
        }

        return 1;

    } ## end if ( ( defined $database...))

    $self->error( { '-text' => 'Missing connection parameters' } );

    return;
} ## end sub init_connection

sub dbh_connected {
    my ( $self, $args ) = @_;

    my ($date);

    if ( !defined $self->dbh ) {
        $self->debug( { '-text' => 'No dbh yet, not connected' } );
        return;
    }

    my $success = eval {
        my $sth = $self->dbh->prepare('SELECT CURRENT_DATE');
        $sth->execute();
        ($date) = $sth->fetchrow_array();
        $sth->finish();
        1;
    };
    if ( !$success ) {
        $self->error( { '-text' => 'Database error :' . $EVAL_ERROR } );
        return;
    }

    $self->debug( { '-text' => 'Current -date ' . $date } );

    return 1;
}

sub prepare {
    my ( $self, $statement ) = @_;

    my $sth = $self->dbh->prepare($statement);

    return $sth;
}

sub do {
    my ( $self, $statement ) = @_;

    return $self->dbh->do($statement);
}

sub last_insert_id {
    my $self = $_[0];

    return $self->dbh->{q{mysql_insertid}};
}

sub disconnect {
    my $self = $_[0];

    return $self->dbh->disconnect();
}

sub commit {
    my $self = $_[0];

    if ( defined $self->dbh ) {
        return $self->dbh->commit();
    }
    return;
}

sub rollback {
    my $self = $_[0];

    if ( defined $self->dbh ) {
        return $self->dbh->rollback();
    }
    return;
}

1;
