package AlienSpaces::Util::Business;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

use AlienSpaces::Constants qw(
    :util
    :comp
    :program
    :defaults
    :business
    :contact-type
    :mode
    :error-codes-business
);

our @DYNISA = qw(Util);

our $VERSION = (qw$Revision: $)[1];

use constant {

};

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        return;
    }

    return 1;
}

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        @{ $self->SUPER::util_list },
        UTIL_EMAIL(),      # email
        UTIL_SESSION(),    # session
        UTIL_MESSAGE(),    # message
        UTIL_PHRASE(),     # phrase
        UTIL_ERROR(),      # error

        #   util db record business
        UTIL_DB_RECORD_BUSINESS(),

    ];

    return $util_list;
}

#   fetch business
sub fetch_business {
    my ( $self, $args ) = @_;

    #   business db rec util
    my $business_db_rec_util = $self->utils->{ UTIL_DB_RECORD_BUSINESS() };

    #   fetch
    my $data = $business_db_rec_util->fetch($args);

    return $data;
}

#   fetch one business
sub fetch_one_business {
    my ( $self, $args ) = @_;

    #   business db rec util
    my $business_db_rec_util = $self->utils->{ UTIL_DB_RECORD_BUSINESS() };

    #   fetch
    my $data = $business_db_rec_util->fetch_one($args);

    return $data;
}


#   fetch current business
sub fetch_current_business {
    my ( $self, $args ) = @_;

    my $business_id = $self->business_id;
    if ( !defined $business_id ) {
        $self->debug( { '-text' => 'Missing -business_id, cannot fetch current business' } );
        return;
    }

    my $business = $self->fetch_one_business( { '-business_id' => $business_id } );

    return $business;
}

#   set business session values
sub set_business_session_values {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   business id
    my $business_id = $args->{'-business_id'};

    #   business
    my $business = $self->fetch_one_business( { '-business_id' => $business_id } );
    if ( !defined $business ) {
        $self->error( { '-text' => 'Failed to fetch -business_id ' . $business_id . ', cannot set business session values' } );
        return;
    }

    my @session_keys = qw(-business_id -business_status_id );
    foreach my $key (@session_keys) {
        $session_util->set_session( { '-key' => $key, '-value' => $business->{$key} } );
        $self->debug( { '-text' => 'Set session -key ' . $key . ' -value ' . $self->dbg_value( $business->{$key} ) } );
    }

    return 1;
}

#   insert business
sub insert_business {
    my ( $self, $args ) = @_;

    #   business db rec util
    my $business_db_rec_util = $self->utils->{ UTIL_DB_RECORD_BUSINESS() };

    #   validate business
    if ( !$self->validate_business_insert($args) ) {
        $self->debug( { '-text' => 'Failed validation with -args ' . $self->dbg_value($args) } );
        return;
    }

    #   business id
    my $business_id = $business_db_rec_util->insert($args);

    $self->debug( { '-text' => 'Added -business_id ' . $business_id } );

    return $business_id;
}

#   update business
sub update_business {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #   business db rec util
    my $business_db_rec_util = $self->utils->{ UTIL_DB_RECORD_BUSINESS() };

    #   business id
    my $business_id = $args->{'-business_id'};

    #   business
    my $business = $self->fetch_one_business( { '-business_id' => $business_id } );
    if ( !defined $business ) {
        $self->error( { '-text' => 'Failed to fetch one business -args ' . $self->dbg_value($args) . ', cannot update business' } );
        return;
    }

    #   validate business
    if ( !$self->validate_business_update($args) ) {
        $self->debug( { '-text' => 'Failed validation with -args ' . $self->dbg_value($args) } );
        return;
    }

    #   update
    if ( !$business_db_rec_util->update($args) ) {
        $self->error( { '-text' => 'Failed to update business record' } );
        return;
    }

    $self->debug( { '-text' => 'Updated -business_id ' . $args->{'-business_id'} } );

    return 1;
} ## end sub update_business

#   validate business insert
sub validate_business_insert {
    my ( $self, $args ) = @_;

    my $valid = 1;

TEST_UNIQUE:
    foreach my $k ('-name') {
        my $check_business = $self->fetch_business( { $k => $args->{$k} } );
        if ( defined $check_business ) {
            $self->error( { '-text' => 'Argument ' . $k . ' is not unique for a business, invalid' } );

            if ( $k eq '-name' ) {
                $self->set_error( { '-error_code' => ERROR_CODE_BUSINESS_NAME_NOT_UNIQUE } );
            }
            $valid = 0;
            last TEST_UNIQUE;
        }
    }

    if ( !defined $args->{'-name'} ) {
        $self->error( { '-text' => 'Missing name address, invalid' } );
        $valid = 0;
    }

    return $valid;
}

#   validate business update
sub validate_business_update {
    my ( $self, $args ) = @_;

    my $valid = 1;

TEST_UNIQUE:
    foreach my $k ('-name') {
        if ( !defined $args->{$k} ) {
            next TEST_UNIQUE;
        }
        my $check_business = $self->fetch_business( { $k => $args->{$k} } );
        if ( defined $check_business ) {
            $self->error( { '-text' => 'Argument ' . $k . ' is not unique for a business, invalid' } );

            if ( $k eq '-name' ) {
                $self->set_error( { '-error_code' => ERROR_CODE_BUSINESS_NAME_NOT_UNIQUE } );
            }
            $valid = 0;
            last TEST_UNIQUE;
        }
    }

    return $valid;
}

#   delete business
sub delete_business {
    my ( $self, $args ) = @_;

    if ( !$self->check_arguments( ['-business_id'], $args, { '-defined' => 1 } ) ) {
        return;
    }

    #	business id
    my $business_id = $args->{'-business_id'};

    #   business
    my $business = $self->fetch_business( { '-business_id' => $business_id } );
    if ( !defined $business ) {
        $self->error( { '-text' => 'Unknown business for -business_id ' . $business_id } );
        return;
    }

    $self->debug( { '-text' => 'Business -business_id ' . $business_id . ' account deleted' } );

    return 1;
}

#   business id
sub business_id {
    my ( $self, $args ) = @_;

    #   session util
    my $session_util = $self->utils->{ UTIL_SESSION() };

    #   business id
    my $business_id = $session_util->get_session( { '-key' => '-business_id' } );

    return $business_id;
}

1;

