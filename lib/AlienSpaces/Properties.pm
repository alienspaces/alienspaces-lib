package AlienSpaces::Properties;

#
#   $Revision: 122 $
#   $Author: bwwallin $
#   $Date: 2010-05-23 21:45:30 +1000 (Sun, 23 May 2010) $
#

use strict;
use warnings;
use English qw(-no_match_vars);
use Carp qw(carp croak);

our $VERSION = '$Revision: 122 $';

#
#   dont allow the following function names to be used as properties
#
my %keywords = map { $_ => 1 } qw{ BEGIN INIT CHECK END DESTROY AUTOLOAD };

my %forced_into_main = map { $_ => 1 } qw{ STDIN STDOUT STDERR ARGV ARGVOUT ENV INC SIG };

my %forbidden = ( %keywords, %forced_into_main );

my $CLASS_PROPS = {};

sub import {
    my $class = shift;
    return if !@_;
    my $props;
    my $multiple = ref $_[0];

    if ($multiple) {
        if ( ref $_[0] ne 'HASH' ) {
            croak 'Invalid reference type ', ref( $_[0] ), ' not "HASH"';
        }
        $props = $_[0];
    }
    else {
        %{$props} = @_;
    }

    my $pkg = caller;

    create_props( $pkg, $props );

    return 1;
}

#
#   install an accessor for each property
#
sub create_props {
    my $package = shift;
    my $props   = shift;

    foreach my $name ( keys %{$props} ) {
        if ( !defined $name ) {
            croak 'Cant use undef as a constant name';
        }

        if ($forbidden{$name} ||
            $name !~ m{\A        # Beginning of string
                       _?           # Optional leading underscore
                       [[:alpha:]]  # make sure the function name starts with a letter
                       \w*          # any number of letters, numbers, and underscores
                       \z           # nothing else, not even newlines
                       }msx
            ) {
            croak $name, ' is not a valid function name';
        }

        my ( $full_name, $prop_class );

        #
        # Properties can be defined with a scope of either :
        #  object (default) - Each object gets its own copy of this property
        #  class - Each object of this class or any sub-classes share this property
        #  global - Every object shares this property
        #
        # In the case of scope 'class' or 'global', the value is stored in a
        # hash 'owned' by this package.  Properties that are 'object' scoped
        # are stored in the hash that *is* the object.
        #
        my $scope = $props->{$name}->{'-scope'};
        if ( defined $scope ) {
            if ( $scope eq 'class' ) {
                if ( !exists $CLASS_PROPS->{$package} ) {
                    $CLASS_PROPS->{$package} = {};
                }
                $prop_class = 1;
            }
        }

        my $prop_name = '--' . $name;

        my $prop_type = $props->{$name}->{'-type'};
        if ( !defined $prop_type ) {
            $prop_type = 's';
        }
        my $prop_default = (defined $props->{$name}->{'-dflt'} ? $props->{$name}->{'-dflt'} : $props->{$name}->{'-default'});
        
        
        my $sub_def = <<'EOS';
sub {
    my ($p_package, $p_prop_class, $p_prop_name, $p_prop_default) = @_;
    
    return sub {
        my $self = $_[0];
        if ($p_prop_class) {
            $self = $CLASS_PROPS->{$p_package};
        }
EOS

        #   hash
        if ( $prop_type eq 'h' ) {

            if (!defined $prop_default) {
                $prop_default = {};
            }

            $sub_def .= <<'EOS';
        if ( $#_ == 1 ) {
            $self->{$p_prop_name} = $_[1];
        }
        if ( !defined $self->{$p_prop_name} ) {
            $self->{$p_prop_name} = $p_prop_default;
        }
        return $self->{$p_prop_name};
    };
}
EOS
        }

        #   array
        elsif ( $prop_type eq 'a' ) {

            if (!defined $prop_default) {
                $prop_default = [];
            }

            $sub_def .= <<'EOS';
        if ( $#_ == 1 ) {
            $self->{$p_prop_name} = $_[1];
        }
        if ( !defined $self->{$p_prop_name} ) {
            $self->{$p_prop_name} = $p_prop_default;
        }
        return $self->{$p_prop_name};
    };
}
EOS
        }

        #   scalar
        else {

            $sub_def .= <<'EOS';
        if ( $#_ == 1 ) {
            $self->{$p_prop_name} = $_[1];
        }
        if ( !defined $self->{$p_prop_name} ) {
            $self->{$p_prop_name} = $p_prop_default;
        }
        return $self->{$p_prop_name};
    };
}
EOS
        }

        my $result = eval $sub_def;    ## no critic qw(Perl::Critic::Policy::BuiltinFunctions::ProhibitStringyEval)
        if ( !$result ) {
            croak 'Failed to build function ' . $prop_name . ': ' . $EVAL_ERROR . ' as ' . $sub_def;
        }

        $full_name = $package . '::' . $name;

        no strict 'refs';
        *{$full_name} = $result->( $package, $prop_class, $prop_name, $prop_default );
        use strict 'refs';

    } ## end foreach my $name ( keys %{$props...})

    return 1;
} ## end sub create_props

1;
