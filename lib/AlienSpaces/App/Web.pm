package AlienSpaces::App::Web;
use parent qw(Plack::Component);

use strict;
use warnings;
use Carp qw(croak carp);
use English qw(-no_match_vars);

use Plack::Response;
use Plack::Request;

our $VERSION = (qw$Revision:$)[1];

use AlienSpaces::Base;

use constant {
    HTTP_NOT_FOUND => 404,
    HTTP_OK        => 200,
};

sub call {
    my ( $self, $env ) = @_;

    AlienSpaces::Base->init_environment( { '-env' => $env } );

    #   response
    my $res = Plack::Response->new();

    #   request
    my $req = Plack::Request->new($env);

    my $program_name = $req->path_info;
    $program_name =~ s/^\///sxm;

    #   program interface / program class
    my ( $interface, $interface_class ) = $self->get_interface_class(
        {   '-request_uri' => $req->request_uri,
            '-request'     => $req,
        } );

    if ( !defined $interface_class ) {
        carp ' Failed to identify -interface_class ' . ( defined $interface_class ? $interface_class : 'undef' );

        $res->status(HTTP_NOT_FOUND);
        return $res->finalize();
    }

    carp ' Executing -interface_class ' . $interface_class;

    #   instantiate program object
    my $interface_object;
    my $success = eval {
        $interface_object = $interface_class->new(
            {   '-request'      => $req,
                '-program_name' => $program_name,

                #                '-hostname'       => $apache_request->hostname,
                '-interface' => $interface,
            } );
        1;
    };

    if ( !$success ) {
        carp ' Failed instantiating program class :' . $EVAL_ERROR;

        $res->status(HTTP_NOT_FOUND);
        return $res->finalize();
    }

    if ( !defined $interface_object ) {
        carp 'Failed instantiating program class';

        $res->status(HTTP_NOT_FOUND);
        return $res->finalize();
    }

    #   initialise interface object
    $success = eval {

        #   init
        $interface_object->init();
        1;
    };

    if ( !$success ) {
        carp 'Failed initialising class :' . $EVAL_ERROR;

        $res->status(HTTP_NOT_FOUND);
        return $res->finalize();
    }

    #   parse input params
    if ( $interface_object->parse_input_params() ) {
        carp 'Parsed input parameters';

        #   check interface authorize user
        if ( $interface_object->check_interface_authorize_user() ) {

            carp 'User authorized';

            #   execute interface object
            $success = eval {

                #   main
                $interface_object->main();
                1;
            };

            if ( !$success ) {
                carp 'Failed executing class :' . $EVAL_ERROR;

                $res->status(HTTP_NOT_FOUND);
                return $res->finalize();
            }
        }
        else {
            carp 'User not authorized';
        }
    }
    else {
        carp 'Failed to parse input parameters';
    }

    #   check interface render
    my $content;
    if ( $interface_object->check_interface_render() ) {

        #   main render
        $content = $interface_object->main_render();
        $res->body($content);
    }

    #   finish
    $interface_object->finish();

    #   redirect url
    my $redirect_url = $interface_object->get_redirect_url();
    if ( defined $redirect_url ) {
        $res->redirect($redirect_url);
    }

    my $session_string = $interface_object->get_session_cookie_string();
    if ( defined $session_string ) {
        $res->header( 'Set-Cookie' => $session_string );
    }

    my $content_type = $interface_object->get_content_type();
    if ( defined $content_type ) {
        $res->header( 'Content-Type' => $content_type );
    }

    $res->status(HTTP_OK);
    return $res->finalize();
} ## end sub call

sub get_interface_class {
    my ( $class, $args ) = @_;

    #   request
    my $req = $args->{'-request'};

    my $path = $args->{'-request_uri'};
    $path =~ s/[?].*$//sxmg;

    my $interface;
    if ( $path =~ /\/(.*?)\/(.*)?$/sxmg ) {
        $interface = $1;
    }
    else {
        return;
    }

    carp 'Unparsed URI is ' . $args->{'-request_uri'};
    carp 'IFace is ' . $interface;
    carp 'Path is ' . $path;

    $interface = uc($interface);

    #   class name
    my $class_name = AlienSpaces::Base->_make_class_from_path( { '-path' => $path, '-interface' => $interface } );

    #   namespaces
    my @namespaces =
        ( AlienSpaces::Base->environment->{'APP_PERL_NAMESPACE'}, AlienSpaces::Base->environment->{'APP_SUPER_PERL_NAMESPACE'}, );

    my $load_class_name;

FIND_CLASS:
    foreach my $namespace (@namespaces) {
        if ( !defined $namespace ) { next FIND_CLASS; }

        carp 'Program namespace ' . $namespace;

        #   load class name
        $load_class_name = $namespace . '::IFace::Web::' . $class_name;

        #   load class
        if ( AlienSpaces::Base->load_class( { '-class_name' => $load_class_name } ) ) {
            carp 'Success load class -interface ' . $interface . ' -load_class_name ' . $load_class_name;
            last FIND_CLASS;
        }

        carp 'Failed load class -interface ' . $interface . ' -load_class_name ' . $load_class_name;
        undef $load_class_name;
    }

    return ( $interface, $load_class_name );
} ## end sub get_interface_class

1;
