package AlienSpaces::App::Shell;

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

our $VERSION = '$Revision:$';

my $errors = [];

sub new {
    my ( $class, $args ) = @_;

    my $self = {};
    bless $self, $class;

    my $class_name = $args->{'-class_name'};
    if ( defined $class_name ) {
        $self->{'-class_name'} = $class_name;
    }

    return $self;
}

sub call {
    my ( $self, $args ) = @_;

    my $class_name = $args->{'-class_name'};
    if ( defined $class_name ) {
        $self->{'-class_name'} = $class_name;
    }

    if ( !defined $self->{'-class_name'} ) {
        croak 'Must provide -class_name argument, cannot run app';
    }

    my $ok = 0;

RUN_CLASS:
    foreach my $namespace ( $ENV{'APP_PERL_NAMESPACE'}, $ENV{'APP_PERL_SUPER_NAMESPACE'} ) {
        if ( !defined $namespace ) {
            next RUN_CLASS;
        }
        if ( $self->run_class( { '-namespace' => $namespace } ) ) {
            $ok = 1;
            last RUN_CLASS;
        }
    }

    if ( !$ok ) {
        croak 'Run failed :' . ( join "\n", @{$errors} ) . ' :' . $EVAL_ERROR;
    }

    return 1;
} ## end sub call

sub errors {
    return $errors;
}
    
sub run_class {
    my ( $self, $args ) = @_;

    if ( !defined $self->{'-class_name'} ) {
        croak 'Must provide -class_name argument, cannot run app';
    }

    my $namespace = $args->{'-namespace'};

    my ( $class_file, $class_name, $object );
    my $success = eval {
        $class_file = $namespace . '::' . 'IFace::Shell::' . $self->{'-class_name'};
        $class_file =~ s/\:\:/\//sxmg;
        $class_file .= '.pm';

        require $class_file;

        $class_name = $namespace . '::' . 'IFace::Shell::' . $self->{'-class_name'};
        $object     = $class_name->new();
        1;
    };

    if ( !$success ) {
        push @{$errors}, $EVAL_ERROR;
    }

    if ( !defined $object ) {
        push @{$errors}, 'Failed to load ' . $self->{'-class_name'};
        return;
    }

    #   init
    $object->init();

    #   main
    $object->main();

    #   finish
    $object->finish();

    return 1;
} ## end sub run_class

1;

