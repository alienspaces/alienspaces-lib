package AlienSpaces::Util;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(croak);

our $VERSION = '$Revision:$';

use base qw (AlienSpaces::Base);

use AlienSpaces::Constants qw(:util);

use AlienSpaces::Properties {
    'name'     => { '-type' => 's' },
    'finished' => { '-type' => 's', 'dflt' => 0 },
};

sub new {
    my ( $class, $args ) = @_;

    my $self = $class->SUPER::new($args);

    return $self;
}

sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->error( { '-text' => 'Super init failed' } );
        return;
    }

    #   load panel object utils
    $self->load_utils();

    return 1;
}

#   util object list
sub util_list {
    my ( $self, $args ) = @_;

    my $util_list = [
        UTIL_DB_MYSQL(),    # default mysql database handle
    ];

    return $util_list;
}

#   util object clear list
sub util_clear_list {
    my ( $self, $args ) = @_;

    my $clear_list = [];

    return $clear_list;
}

sub util_new_args {
    my ( $self, $args ) = @_;

    #   super args
    my $util_args = $self->SUPER::util_new_args($args);

    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

sub util_init_args {
    my ( $self, $args ) = @_;

    my $util_args = $self->SUPER::util_init_args($args);

    #   global
    $util_args->{'-program_name'} = $self->program_name;

    #   specific
    if ( defined $args->{'-class'} ) {

    }

    return $util_args;
}

sub load_utils {
    my ( $self, $args ) = @_;

    return $self->SUPER::load_utils( { '-scope' => 'global', } );
}

###########################################
#
#   dbh
#   session
#   cgi
#
#   short cuts to utility classes that are
#   always loaded
#
###########################################

#   dbh
sub dbh {
    my ( $self, $args ) = @_;

    my $db_mysql_util = $self->utils->{ UTIL_DB_MYSQL() };

    return $db_mysql_util;
}

#   session
sub session {
    my ( $self, $args ) = @_;

    my $session_util = $self->utils->{ UTIL_SESSION() };

    return $session_util;
}

#   cgi
sub cgi {
    my ( $self, $args ) = @_;

    my $cgi_util = $self->utils->{ UTIL_CGI() };

    return $cgi_util;
}

###########################################
#
#   fetch, insert, update methods
#
#   these methods aren't very smart
#   you are better off implementing a set
#   of sub-class Util::DB::Record classes
#
###########################################

#   fetch data
sub fetch_data {
    my ( $self, $args ) = @_;

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };
    if ( !defined $db_basic_util ) {
        $self->stack( { '-text' => 'Missing UTIL_DB_BASIC, cannot fetch data' } );
        $self->show_utils();
        return;
    }

    return $db_basic_util->fetch_data($args);
}

#   update data
sub update_data {
    my ( $self, $args ) = @_;

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };
    if ( !defined $db_basic_util ) {
        $self->stack( { '-text' => 'Missing UTIL_DB_BASIC, cannot update data ' } );
        return;
    }

    return $db_basic_util->update_data($args);
}

#   insert data
sub insert_data {
    my ( $self, $args ) = @_;

    my $db_basic_util = $self->utils->{ UTIL_DB_BASIC() };
    if ( !defined $db_basic_util ) {
        $self->stack( { '-text' => 'Missing UTIL_DB_BASIC, cannot insert data ' } );
        return;
    }

    return $db_basic_util->insert_data($args);
}

###########################################
#
#   finish
#   cleanup
#
###########################################

#   finish
sub finish {
    my ( $self, $args ) = @_;

    $self->finished(1);

    return 1;
}

#   cleanup
sub cleanup {
    my ( $self, $args ) = @_;

    #
    #   if we have never been finished finish us off
    #
    if ( !$self->finished() ) {
        $self->debug( { '-text' => 'Finishing myself off ' . ref($self) } );
        $self->finish();
    }

    #   drop any references we have to util objects
    $self->utils(undef);

    #
    #   do not call super cleanup here as we only
    #   want Base::cleanup to be called once on
    #   cleanup of Display or Bin  object
    #

    return;
}

1;
