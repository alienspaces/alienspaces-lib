package AlienSpaces::IFace;

#
#   $Revision: $
#   $Author: $
#   $Date: $
#

use strict;
use warnings;
use English qw( -no_match_vars );
use Carp qw(carp croak);

our $VERSION = (qw$Revision: $)[1];

use base qw(AlienSpaces::Base);

sub new {
    my ( $class, $args ) = @_;

    #   super new (does minimal stuff)
    my $self = $class->SUPER::new($args);

    #   init cache
    $self->init_cache();

    #   clear ids
    $self->clear_ids();

    #   load util objects
    if ( !$self->load_utils() ) {
        $self->error( { '-text' => 'Loading utils failed' } );
        return;
    }

    #   init errors
    $self->init_errors();

    #   init profile
    $self->init_profile();

    #   init context
    $self->init_context();

    #   profile start
    $self->profile_start( { '-tag' => 'Program' } );

    return $self;
} ## end sub new

#   init
sub init {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::init($args) ) {
        $self->debug( { '-text' => 'Failed SUPER init' } );
        return;
    }

    $self->debug( { '-text' => 'Init' } );

    #   init database
    $self->init_database_connection();

    return 1;
}

#   finish
sub finish {
    my ( $self, $args ) = @_;

    if ( !$self->SUPER::finish($args) ) {
        $self->debug( { '-text' => 'Finish failed in super' } );
        return;
    }

    #   profile end
    $self->profile_end( { '-tag' => 'Program' } );

    #   profile summary
    $self->profile_summary();

    $self->finished(1);

    $self->debug( { '-text' => 'Finish' } );

    return 1;
}

1;
