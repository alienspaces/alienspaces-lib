# alienspaces-lib

A custom made web application framework written mostly in Perl for building web applications.

- Base page and component classes
- Utility classes
- Generators and other tools

## NOTE

This was a personal project and is no longer used or maintained.

Much that makes this functional has been stripped away leaving the core library and designs in place for nostalgic reference :D

Perl was fun for a while..



